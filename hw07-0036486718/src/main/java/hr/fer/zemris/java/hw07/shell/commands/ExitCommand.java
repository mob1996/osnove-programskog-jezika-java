package hr.fer.zemris.java.hw07.shell.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;

/**
 * Class represents exit command
 * @author Luka Mijić
 *
 */
public class ExitCommand implements ShellCommand {

	/**
	 * Command name
	 */
	private static final String COMMAND_NAME = "exit";
	
	/**
	 * Description of command
	 */
	private static List<String> description;
	static {
		description = new ArrayList<>();
		description.add("Command: exit");
		description.add("\t- exits console");
	}
	
	/**
	 * Method terminates program
	 * @param env is Enviroment in which help is executed
	 * @param arguments must be empty
	 * @return ShellStatus.TERMINATE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> args = CommandUtil.toArguments(arguments);
		
		if(args.size() != 0) {
			env.writeln(COMMAND_NAME + " requires zero arguments.");
			return ShellStatus.CONTINUE;
		}
		
		env.writeln("Goodbye");
		return ShellStatus.TERMINATE;
	}

	/**
	 * @return name of command
	 */
	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	/**
	 * @return description of command
	 */
	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(description);
	}

}
