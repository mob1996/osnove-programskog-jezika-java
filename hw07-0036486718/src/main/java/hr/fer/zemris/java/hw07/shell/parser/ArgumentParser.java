package hr.fer.zemris.java.hw07.shell.parser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw07.shell.lexer.ArgumentLexer;
import hr.fer.zemris.java.hw07.shell.lexer.LexerException;
import hr.fer.zemris.java.hw07.shell.lexer.TokenType;

/**
 * Class Argument parser is used for parsing input String into list of valid
 * arguments.
 * 
 * @author Luka Mijić
 *
 */
public class ArgumentParser {

	/**
	 * List of arguments
	 */
	private List<String> arguments;

	/**
	 * Lexer used for retrieval of tokens
	 */
	private ArgumentLexer lexer;

	/**
	 * Constructor for parser. Creates lexer and empty array of arguments
	 * 
	 * @param documentBody
	 *            is usef for initialization of lexer
	 */
	public ArgumentParser(String documentBody) {
		this.lexer = new ArgumentLexer(documentBody);
		arguments = new ArrayList<>();

		parse();
	}

	/**
	 * Method parses input into list of of arguments
	 */
	private void parse() {
		try {
			while (true) {
				lexer.nextToken();
				if(isTokenOfType(TokenType.EOF)) break;
				arguments.add(lexer.getToken().getValue());
			}
		} catch (LexerException e) {
			throw new ParserException(e.getMessage());
		}
	}

	/**
	 * @return unmodifiable list of arguments
	 */
	public List<String> getArguments() {
		return Collections.unmodifiableList(arguments);
	}

	/**
	 * Helper method that is used to check if current token is same type as given
	 * type
	 * 
	 * @param type
	 *            that we are checking
	 * @return true if current token's type is same as type
	 */
	private boolean isTokenOfType(TokenType type) {
		return lexer.getToken().getType() == type;
	}

}
