package hr.fer.zemris.java.hw07.shell.lexer;

/**
 * This enum class represents all types of Tokens
 * @author Luka Mijić
 * 
 */
public enum TokenType {
	/**
	 * Token represents argument
	 */
	ARGUMENT,
	
	/**
	 * This token type represents end of file.
	 * No more tokens can't be generated after this one
	 * is generated.
	 */
	EOF;
}
