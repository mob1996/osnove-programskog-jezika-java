package hr.fer.zemris.java.hw07.crypto;

import java.util.Objects;

/**
 * Util class contains some utility method
 * that are helpful for other classes.
 * @author Luka Mijić
 *
 */
public class Util {

	/**
	 * Lenght of hex string must be even
	 * to be turned into bytes
	 */
	private static int EVEN = 2;
	
	/**
	 * Each hex character can be represented as 4
	 * zeroes or ones
	 */
	private static int HEX_2_BYTE_LENGHT = 4;
	
	/**
	 * All valid hex values
	 */
	private static final String HEXES = "0123456789abcdef";
	
	/**
	 * 00001111 mask
	 */
	private static int LAST_FOUR_MASK = 0x0F;
	
	/**
	 * 10000000 mask
	 */
	private static int FIRST_FOUR_MASK = 0xF0;
	
	/**
	 * integer value of A or a hex char
	 */
	private static int HEX_A_VALUE = 10;
	
	/**
	 * lowest non digit hex value
	 */
	private static char HEX_CHAR_LOWER_BOUND = 'a';
	
	/**
	 * highest hex value
	 */
	private static char HEX_CHAR_HIGHER_BOUND = 'f';
	
	/**
	 * Method takes hex string value and transforms it
	 * into byte array
	 * @param hex string of hex values
	 * @return byte array
	 * @throws IllegalArgumentException if number of hex characters is uneven or if hex String
	 *            character that can't be interpeted as hex number
	 * @throws NullPointerException if hex is null reference
	 */
	public static byte[] hexToByte(String hex) {
		Objects.requireNonNull(hex, "Given hex String can't be null reference.");
		
		int len = hex.length();
		if(len % EVEN != 0) {
			throw new IllegalArgumentException("Bytes can only be represented with 2 hex characters." +
					" Uneven number of hex characters.");
		}
		
		byte[] bytes = new byte[len/EVEN];
		
		for(int i = 0; i < bytes.length; i++) {
			int firstIndex = i * EVEN;
			int first = hexToInt(hex.charAt(firstIndex));
			int second = hexToInt(hex.charAt(firstIndex + 1));
			bytes[i] = (byte) ((first << HEX_2_BYTE_LENGHT) | second); //shift first 4 places to left and then or between bytes
		}
		return bytes;
	}
	
	/**
	 * Transforms hex char into int that it represents
	 * @param hex 
	 * @return int value
	 * @throws new IllegalArgumentException if hex can't be transformed into int
	 */
	private static int hexToInt(char hex) {
		if(Character.isDigit(hex)) {
			return Character.getNumericValue(hex);
		} else {
			hex = Character.toLowerCase(hex);
			if(hex < HEX_CHAR_LOWER_BOUND || hex > HEX_CHAR_HIGHER_BOUND) {
				throw new IllegalArgumentException(hex + " must be in [a, f] range.");
			}
			return HEX_A_VALUE + hex - HEX_CHAR_LOWER_BOUND;
		}
	}
	
	/**
	 * Method transformes bytes array into hex String
	 * @param bytes
	 * @return bytes transformed into valid hex String
	 * @throws NullPointerException if bytes are null reference
	 */
	public static String byteToHex(byte[] bytes) {
		Objects.requireNonNull(bytes, "Given bytes array can't be null reference.");
		StringBuilder hex = new StringBuilder();
		
		for(Byte b:bytes) {
			int index1 = (int) ((b & FIRST_FOUR_MASK) >> HEX_2_BYTE_LENGHT);
			int index2 = ((int) b) & LAST_FOUR_MASK;
			hex.append(HEXES.charAt(index1));
			hex.append(HEXES.charAt(index2));
		}
		
		return hex.toString();
	}
}
