package hr.fer.zemris.java.hw07.crypto;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Objects;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Class Crypt is used for decrypting and encrypting
 * using AES256.
 * @author Luka Mijić
 *
 */
public class Crypt implements Runnable{

	/**
	 * Used for initialization of class Cipher
	 */
	private static final String INIT_CIPHER = "AES/CBC/PKCS5Padding";
	
	/**
	 * How many bytes one KB has
	 */
	private static final int KB = 1024;

	/**
	 * Cipher mode
	 */
	private int mode;
	
	/**
	 * Path to file that will be decrypted or encrypted
	 */
	private Path srcFile;
	
	/**
	 * Path to file that will store encrypted or decrypted file.
	 */
	private Path destFile;
	
	/**
	 * Creates new Crypt object.
	 * @param encrypt if it is true, Crypt will work in encrypt mode,
	 * 			otherwise it will work in decrypt mode
	 * @param srcFile 
	 * @param destFile
	 * @throws NullPointerException if srcFile or destFile are null reference
	 */
	public Crypt(boolean encrypt, Path srcFile, Path destFile) {
		this.mode = encrypt ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE;
		this.srcFile = Objects.requireNonNull(srcFile, "Source file can't be null.");
		this.destFile = Objects.requireNonNull(destFile, "Destination file can't be null.");
	}

	/**
	 * Method is used to establish communication between
	 * user and program, prompting him to enter key and initialization vector
	 * used for encrypting/decrypting file. After user enters requiered values
	 * decryption/encryption process is called. 
	 * @throws IllegalArgumentException if srcFile doesn't exist or if it isn't file
	 */
	@Override
	public void run() {
		if(!Files.isRegularFile(srcFile))
			throw new IllegalArgumentException("Given path must lead to file.");
		
		try(BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			BufferedInputStream input = new BufferedInputStream(
							Files.newInputStream(srcFile, StandardOpenOption.READ));
			BufferedOutputStream output = new BufferedOutputStream(
							Files.newOutputStream(destFile, StandardOpenOption.CREATE, StandardOpenOption.WRITE))){
			
			String keyText = promptUser(in, "Please provide password as hex-encoded text "
					+ "(16 bytes, i.e. 32 hex-digits):\n> ");
			String ivText = promptUser(in, "Please provide initialization "
					+ "vector as hex-encoded text (32 hex-digits):\n> ");
			
			SecretKeySpec keySpec = new SecretKeySpec(Util.hexToByte(keyText), "AES");
			AlgorithmParameterSpec paramSpec = new IvParameterSpec(Util.hexToByte(ivText));
			
			Cipher cipher = Cipher.getInstance(INIT_CIPHER);
			cipher.init(mode, keySpec, paramSpec);

			useCipher(input, output, cipher);
			System.out.println(cryptMessage());
		} catch (IOException | NoSuchAlgorithmException | NoSuchPaddingException | 
				InvalidKeyException | InvalidAlgorithmParameterException | 
				IllegalBlockSizeException | BadPaddingException e) {
			System.err.println(e.getClass().getSimpleName() + " happened.");
		}	
	}
	
	/**
	 * Method uses Cipher to encrypt/decrypt bytes taken from
	 * InputStream and writes encrypted/decrypted bytes using
	 * OutputStream
	 * @param input InputStream	from where bytes are read
	 * @param output OutputStream used for storing new bytes
	 * @param cipher used for encrypting/decrypting data
	 * @throws IOException 
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	private void useCipher(InputStream input, OutputStream output, Cipher cipher) throws IOException, 
																IllegalBlockSizeException, BadPaddingException {
		byte[] buff = new byte[4 * KB];
		int read = 0;
		while((read = input.read(buff)) != -1) {
			output.write(cipher.update(buff, 0, read));
		}
		output.write(cipher.doFinal());
	}
	
	/**
	 * Method writes prompt message to user and reads
	 * user's input.
	 * @param in is used for reading user input
	 * @param message that is used to prompt user
	 * @return input string
	 * @throws IOException
	 */
	private String promptUser(BufferedReader in, String message) throws IOException {
		System.out.print(message);
		return in.readLine().toLowerCase();
	}
	
	/**
	 * @return message after ending encrypting/decrypting files.
	 */
	private String cryptMessage() {
		String process = mode == Cipher.ENCRYPT_MODE ? "Encryption" : "Decryption";
		return process + " completed. Generated file "
				+ destFile.getFileName() + " based on file " + srcFile.getFileName() + ".";
	}
}
