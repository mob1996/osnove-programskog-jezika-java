package hr.fer.zemris.java.hw07.shell;

import java.util.SortedMap;

import hr.fer.zemris.java.hw07.shell.commands.CommandUtil;
import hr.fer.zemris.java.hw07.shell.commands.ShellCommand;
import hr.fer.zemris.java.hw07.shell.parser.ParserException;

/**
 * Class represents working shell. It provides option to execute various
 * commands with various parameters to user.
 * 
 * @author Luka Mijić
 *
 */
public class Shell {

	/**
	 * Method allowes communication with shell to user.
	 * @param args
	 */
	public static void main(String[] args) {
		Environment env = new EnvironmentImpl();
		env.writeln("Welcome to MyShell v 1.0");

		String input = null;
		SortedMap<String, ShellCommand> validCommands = env.commands();
		try {
			while (true) {

				input = readInput(env).trim();
				String strCommand = CommandUtil.extractCommand(input);
				if (strCommand == null) {
					env.writeln("No command given.");
					continue;
				}

				String arguments = input.substring(strCommand.length());
				ShellCommand command = validCommands.get(strCommand);

				if (command == null) {
					env.writeln("Given command doesn't exist.");
				} else {
					ShellStatus status = command.executeCommand(env, arguments);

					if (status == ShellStatus.TERMINATE) {
						System.exit(0);
					}
				}
			}			
		} catch (ShellIOException e) {
			env.write("Shell exception happened. Terminating.");
		} catch (ParserException e) {
			env.writeln(e.getMessage());
		} catch (Exception e) {
			env.writeln(e.getClass().getName() + ": " + e.getMessage());
		}

	}

	/**
	 * Method is used for reading input from environment and
	 * creating valid input from it.
	 * @param env working environment
	 * @return created input
	 */
	private static String readInput(Environment env) {
		StringBuilder inputBuilder = new StringBuilder();

		boolean nextLine = true;
		env.write(env.getPromptSymbol() + " ");
		while (nextLine) {
			nextLine = false;

			String input = env.readLine();
			if (input.endsWith(String.valueOf(env.getMoreLinesSymbol()))) {
				input = input.substring(0, input.length() - 1);
				nextLine = true;
				env.write(env.getMultiLineSymbol() + " ");
			}

			inputBuilder.append(input);
		}

		return inputBuilder.toString();
	}
}
