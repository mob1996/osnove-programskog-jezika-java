package hr.fer.zemris.java.hw07.shell.parser;

/**
 * Class used to model exceptions that can occur while
 * working with parser . 
 * @author Luka Mijić
 *
 */
public class ParserException extends RuntimeException {

	private static final long serialVersionUID = 636263890867226018L;


	/**
	 * Default constructor of ParserException
	 */
	public ParserException() {
		super();
	}
	
	
	/**
	 * Constructor of ParserException that takes
	 * message argument
	 * @param message argument
	 */
	public ParserException(String message) {
		super(message);
	}
}
