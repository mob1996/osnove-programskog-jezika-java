package hr.fer.zemris.java.hw07.shell;

/**
 * Enum represents status of the Shell
 * @author Luka Mijić
 *
 */
public enum ShellStatus {

	/**
	 * If this status is returned,
	 * shell can continue working
	 */
	CONTINUE,
	
	/**
	 * If this status is returned, shell should be
	 * terminated
	 */
	TERMINATE;
}
