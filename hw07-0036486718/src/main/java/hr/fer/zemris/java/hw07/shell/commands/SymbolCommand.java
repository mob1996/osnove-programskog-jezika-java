package hr.fer.zemris.java.hw07.shell.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;

/**
 * Class represents symbol command.
 * @author Luka Mijić
 */
public class SymbolCommand implements ShellCommand{

	/**
	 * Interfaced used for getting symbols from
	 * Environment
	 */
	private interface EnvironmentFieledGetter {
		Character getField(Environment e);
	}
	
	/**
	 * Interface used for setting symbols in 
	 * Environment
	 */
	private interface EnvironmentFieldSetter {
		void setField(Environment e, Character symbol);
	}
	
	/**
	 * Map containing needed getters
	 */
	private static Map<String, EnvironmentFieledGetter> getters;
	
	/**
	 * Map containing needed setters
	 */
	private static Map<String, EnvironmentFieldSetter> setters;
	
	static {
		getters = new HashMap<>();
		setters = new HashMap<>();
		
		getters.put("PROMPT", e -> e.getPromptSymbol());
		getters.put("MULTILINE", e -> e.getMultiLineSymbol());
		getters.put("MORELINES", e -> e.getMoreLinesSymbol());
		
		setters.put("PROMPT", (e, c) -> e.setPromptSymbol(c));
		setters.put("MULTILINE", (e, c) -> e.setMultiLineSymbol(c));
		setters.put("MORELINES", (e, c) -> e.setMoreLinesSymbol(c));
	}
	
	/**
	 * Command name
	 */
	private static final String COMMAND_NAME = "symbol";
	
	/**
	 * Description of command
	 */
	private static List<String> description;
	static {
		description = new ArrayList<>();
		description.add("Command: symbol");
		description.add("\t- method takes one or two parameters");
		description.add("\t- if there is one parameter it prints what symbol is used for that function");
		description.add("\t- valid values for first parameter are: (PROMPT, MULTILINE, MULTILINES");
		description.add("\t- second argument must be one symbol, and it's used to replace current symbol");
		description.add("\t\tfor appropriate first parameter");
	}
	
	/**
	 * Method executes command symbol.
	 * @param env is Enviroment in which help is executed
	 * @param arguments if 1 argument can be parsed from this string
	 * 				print symbol associated with given argument,
	 * 				if there are 2 arguments replace symbol for
	 * 				associated first argument with second argument
	 * @return ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> args = CommandUtil.toArguments(arguments);
		
		int numOfArgs = args.size();
		
		if(numOfArgs == 1) {
			oneArgumentSymbolCommand(args.get(0), env);
		} else if(numOfArgs == 2) {
			twoArgumentSymbolCommand(args.get(0), args.get(1), env);
		} else {
			env.writeln("Invalid number of arguments for symbol command.");
		}
		
		return ShellStatus.CONTINUE;
	}
	
	/**
	 * Method is used for executing command for 2 arguments
	 * @param firstArg 
	 * @param secondArg
	 * @param env
	 */
	private void twoArgumentSymbolCommand(String firstArg, String secondArg, Environment env) {
		EnvironmentFieldSetter setter = setters.get(firstArg);
		
		if(setter == null) {
			env.writeln("Invalid first parameter.");
			return;
		}else if(secondArg.length() != 1) {
			env.writeln("Second parameter must be one symbol.");
			return;
		}
		
		Character oldSymbol = getters.get(firstArg).getField(env);
		Character newSymbol = secondArg.charAt(0);
		
		if(Character.isAlphabetic(newSymbol) || Character.isDigit(newSymbol)) {
			env.writeln("Second parameter can't be number or a digit.");
			return;
		}
		
		setter.setField(env, newSymbol);
		
		env.writeln("Symbol for " + firstArg + " change from '" + oldSymbol + "'"
				+ " to '" + newSymbol + "'");
	}

	/**
	 * Method is used for executing command with 1 argument
	 * @param argument
	 * @param env
	 */
	private void oneArgumentSymbolCommand(String argument, Environment env) {
		EnvironmentFieledGetter getter = getters.get(argument);
		
		if(getter !=  null) {
			env.writeln("Symbol for " + argument + " is '" 
						+ getter.getField(env) + "'");
		} else {
			env.writeln("Invalid first parameter.");
		}
		
	}
	
	/**
	 * @return name of command
	 */
	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	/**
	 * @return description of command
	 */
	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(description);
	}
}
