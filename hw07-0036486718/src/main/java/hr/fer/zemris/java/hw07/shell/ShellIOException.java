package hr.fer.zemris.java.hw07.shell;

/**
 * Class represents exception that happens in Shell runtime
 * @author Luka Mijić
 *
 */
public class ShellIOException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ShellIOException() {
		super();
	}
	
	public ShellIOException(String message) {
		super(message);
	}
}
