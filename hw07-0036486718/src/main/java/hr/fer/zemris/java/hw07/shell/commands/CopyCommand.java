package hr.fer.zemris.java.hw07.shell.commands;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;

/**
 * Class represents copy command.
 * @author Luka Mijić
 *
 */
public class CopyCommand implements ShellCommand{

	/**
	 * Command name
	 */
	private static final String COMMAND_NAME = "copy";
	
	/**
	 * User should write this value if they want to
	 * continue copying
	 */
	private static final String YES = "yes";
	
	/**
	 * How many bytes one KB has
	 */
	private static final int KB = 1024;
	
	/**
	 * Description of command
	 */
	private static List<String> description;
	static {
		description = new ArrayList<>();
		description.add("Command: copy");
		description.add("\t- first argument must be path to a existing file");
		description.add("\t- second argument must be existing file or directory");
		description.add("\t\t- if second argument is same directory file is copied to c_fileName");
		description.add("\t\t- if second argument is existing directory new file has same file name");
		description.add("\t\t- if second argument is existing file, ask user permision to overwrite it");
		
	}
	
	/**
	 * Method executes command copy. It copies file from
	 * one location to another
	 * @param env is Enviroment in which help is executed
	 * @param arguments
	 * @return ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> args = CommandUtil.toArguments(arguments);
		if(args.size() != 2) {
			env.writeln("Invalid number of arguments for copy command. Expected 2.");
			return ShellStatus.CONTINUE;
		}
		Path src = CommandUtil.getPathFromString(args.get(0));
		Path dest = CommandUtil.getPathFromString(args.get(1));
		
		if(!Files.isRegularFile(src)) {
			env.writeln("Source file must exist.");
			return ShellStatus.CONTINUE;
		} 
		
		if(Files.isRegularFile(dest)) {
			if(!askForPermission(env)) {
				env.writeln("Permision denied.");
				return ShellStatus.CONTINUE;
			}
		} else if(!Files.isDirectory(dest)) {
			if(!Files.isDirectory(dest.getParent())) {
				env.writeln("Invalid destination in copy.");
				return ShellStatus.CONTINUE;
			}
		} else {
			if(src.getParent().equals(dest)) { //same directory
				dest = dest.resolve("c_" + src.getFileName());
				while(Files.exists(dest)) {
					String fileName  = "c_" + dest.getFileName();
					dest = dest.getParent().resolve(fileName);
				}
			} else {
				dest = dest.resolve(src.getFileName());
				if(Files.isRegularFile(dest)) {
					if(!askForPermission(env)) {
						env.writeln("Permision denied.");
						return ShellStatus.CONTINUE;
					}
				}
			}
		}
		
		copy(src, dest, env);
		
		return ShellStatus.CONTINUE;
	}
	
	/**
	 * Method does actual copying of files.
	 * @param src of file
	 * @param dest of file
	 * @param env used for communication
	 */
	private void copy(Path src, Path dest, Environment env) {
		try(BufferedInputStream input = new BufferedInputStream(
								Files.newInputStream(src, StandardOpenOption.READ));
			BufferedOutputStream output = new BufferedOutputStream(
								Files.newOutputStream(dest, StandardOpenOption.CREATE, StandardOpenOption.WRITE))){
			
			byte[] buff = new byte[4 * KB];
			int read = 0;
			
			double progress = 0.05;
			double step = 0.05;
			long copied = 0;
			long originalFileSize = Files.size(src);
			
			while((read = input.read(buff)) != -1) {
				output.write(buff, 0, read);
				copied += read;
				
				double currentProgess = calculateProgress(copied, originalFileSize);
				if(currentProgess >= progress) {
					progress = currentProgess + step;
					printProgess(currentProgess, env);
				}
			}
			env.writeln("Copying from " + src.toString() + " to " + dest.toString() + " completed.");
		} catch (IOException e) {
			env.writeln("Error during copying");
		}
	}
	
	/**
	 * Method prints current progress on environment.
	 * @param progess
	 * @param env
	 */
	private void printProgess(double progess, Environment env) {
		int percentage = (int) (progess * 100);
		env.write("Progess: " + percentage + "%\r");
	}
	
	/**
	 * Method calculates current progress.
	 * @param current 
	 * @param total
	 * @return current / total
	 */
	private double calculateProgress(long current, long total) {
		return ((double) current) / total;
	}
	
	/**
	 * Method asks user for permission.
	 * @param env is used for communication with user
	 * @return true if user writes 'yes' otherwise return false
	 */
	private boolean askForPermission(Environment env) {
		env.write("If you want to overrwrite current file write yes. " + env.getPromptSymbol() + " ");
		return env.readLine().equals(YES);
	}
	
	/**
	 * @return command name
	 */
	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	/**
	 * @return command description
	 */
	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(description);
	}

	
}
