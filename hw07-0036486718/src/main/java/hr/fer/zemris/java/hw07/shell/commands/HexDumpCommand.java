package hr.fer.zemris.java.hw07.shell.commands;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw07.crypto.Util;
import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;


public class HexDumpCommand implements ShellCommand {

	/**
	 * Command name
	 */
	private static final String COMMAND_NAME = "hexdump";
	
	/**
	 * How many bytes are printed per line
	 */
	private static final int BYTES_PER_LINE = 16;
	
	/**
	 * separator used for spliting printed line
	 */
	private static final Character SPLITTER = '|';
	
	/**
	 * lenght of printed adress
	 */
	private static final int ADRESS_LENGHT = 8;
	
	/**
	 * Description of command
	 */
	private static List<String> description;
	static {
		description = new ArrayList<>();
		description.add("Command: hexdump");
		description.add("\t- command takes one argument that must be path to existing file");
		description.add("\t- command dumps hex contents of file on screen in formatted fasion.");
	}
	
	/**
	 * Method executes command hexdump. It dumps all
	 * bytes on given env output. 
	 * @param env is Enviroment in which help is executed
	 * @param arguments
	 * @return ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> args = CommandUtil.toArguments(arguments);
		if(args.size() != 1) {
			env.writeln("Expected just one parameter. Was " + args.size());
			return ShellStatus.CONTINUE;
		}
		
		Path src = CommandUtil.getPathFromString(args.get(0));
		
		if(!Files.isRegularFile(src)) {
			env.writeln("Expected path to file as argument.");
			return ShellStatus.CONTINUE;
		}
		
		hexdump(src, env);
		
		return ShellStatus.CONTINUE;
	}
	
	/**
	 * Method does actual hexdump.
	 * @param src of file
	 * @param env used dumping hex
	 */
	private void hexdump(Path src, Environment env) {
		try(BufferedInputStream byteReader = new BufferedInputStream(
				Files.newInputStream(src, StandardOpenOption.READ))) {
			
			int adress = 0;
			byte[] bytes = new byte[16];
			int r = 0;
			while((r = byteReader.read(bytes)) != -1) {
				StringBuilder lineBuilder = new StringBuilder();
				StringBuilder charBuilder = new StringBuilder();
				lineBuilder.append(String.format("%0"+ADRESS_LENGHT+"d: ", adress));
				String hex = Util.byteToHex(bytes);
				
				for(int i = 0; i < BYTES_PER_LINE; i++) {
					if(i < r) {
						lineBuilder.append(hex.charAt(2 * i));
						lineBuilder.append(hex.charAt(2 * i + 1));
						charBuilder = bytes[i] >= 32 && bytes[i] <= 127 
									? charBuilder.append((char) bytes[i]) : charBuilder.append(".");
					} else {
						lineBuilder.append("  ");
					}
					lineBuilder = (i + 1) % 8 == 0 ? lineBuilder.append(" "+ SPLITTER + " ") : lineBuilder.append(" ");
				}
				adress += 10;
				env.writeln(lineBuilder.append(charBuilder.toString()).toString());
					
			}
			
		} catch (IOException e) {
			env.writeln("IOException happened in hexdump.");
		}
	}
	
	/**
	 * @return command name
	 */
	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	/**
	 * @return command description
	 */
	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(description);
	}

}
