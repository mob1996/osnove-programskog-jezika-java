package hr.fer.zemris.java.hw07.shell.commands;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;

/**
 * Class represents tree command.
 * @author Luka Mijić
 *
 */
public class TreeCommand implements ShellCommand{

	/**
	 * Inner class of TreeCommand. Visitor is used for
	 * traversing current directory and all sub directories
	 * and files.
	 * @author Luka Mijić
	 *
	 */
	private static class TreeVisitor implements FileVisitor<Path>{

		/**
		 * Current padding 
		 */
		private int padding;
		
		/**
		 * By how much is padding changed
		 */
		private final int PADDING_CHANGE = 2;
		
		/**
		 * Working environment
		 */
		private Environment env;
		
		/**
		 * Creates Visitor object using env parameter.
		 * @param env
		 */
		public TreeVisitor(Environment env) {
			super();
			this.padding = 1;
			this.env = env;
		}

		/**
		 * Action taken after visiting every current directory
		 * return FileVisitResult.CONTINUE
		 */
		@Override
		public FileVisitResult postVisitDirectory(Path dir, IOException arg1) throws IOException {
			this.padding -= PADDING_CHANGE;
			return FileVisitResult.CONTINUE;
		}

		/**
		 * Action taken before visiting every current directory
		 * return FileVisitResult.CONTINUE
		 */
		@Override
		public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes arg1) throws IOException {
			String format = "%" + String.valueOf(this.padding) + "s";
			String paddingStr = String.format(format, "");
			env.writeln(paddingStr + dir.getFileName());
			this.padding += PADDING_CHANGE;
			
			return FileVisitResult.CONTINUE;
		}

		/**
		 * Action taken when Visitor class visits file.
		 * return FileVisitResult.CONTINUE
		 */
		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes arg1) throws IOException {
			String format = "%" + String.valueOf(this.padding) + "s";
			String padding = String.format(format, "");
			env.writeln(padding + file.getFileName());
			return FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult visitFileFailed(Path arg0, IOException arg1) throws IOException {
			return FileVisitResult.SKIP_SUBTREE;
		}
		
	}
	
	/**
	 * Command name
	 */
	private static final String COMMAND_NAME = "tree";
	
	/**
	 * Description of command
	 */
	private static List<String> description;
	static {
		description = new ArrayList<>();
		description.add("Command: tree");
		description.add("\t- command takes one argument, path to directory");
		description.add("\t- command lists all directories and files that are "
				+ " descendant of given path.");
	}
	
	/**
	 * Method executes command tree. It lists every sub directories and
	 * files of those directories starting from path extracted from
	 * arguments.
	 * @param env is Enviroment in which help is executed
	 * @param arguments
	 * @return ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> args = CommandUtil.toArguments(arguments);
		
		if(args.size() != 1) {
			env.write("Expected only one argument. Was " + args.size());
			return ShellStatus.CONTINUE;
		}
		
		Path dirPath = CommandUtil.getPathFromString(args.get(0));
		if(!Files.isDirectory(dirPath)) {
			env.writeln("Given path must lead to directory.");
			return ShellStatus.CONTINUE;
		}
		
		try {
			Files.walkFileTree(dirPath, new TreeVisitor(env));
		} catch (IOException e) {
			env.write("IOexception happened while executing " + COMMAND_NAME +" command.");
		}
		
		return ShellStatus.CONTINUE;
	}
	
	/**
	 * @return command name
	 */
	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}
	
	/**
	 * @return command description
	 */
	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(description);
	}
}
