package hr.fer.zemris.java.hw07.crypto;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 * Class represents working program that
 * can get files sha digest or it can encrypt or decrypt
 * files.
 * @author Luka Mijić
 *
 */
public class Crypto {

	/**
	 * Working directory. If user gives relative path, it will be resolved usinng
	 * this path.
	 */
	private static final Path WORKING_DIR = Paths.get("src/main/resources").toAbsolutePath();
	
	/**
	 * Map contains command and associates it with how many parameters
	 * it requires. 
	 */
	private static Map<String, Integer> commandsToParamNum;

	static {
		commandsToParamNum = new HashMap<>();
		commandsToParamNum.put("checksha", 1);
		commandsToParamNum.put("encrypt", 2);
		commandsToParamNum.put("decrypt", 2);
	}

	public static void main(String[] args) {
		if (args.length <= 0) {
			System.err.println("Zero arguments recieved.");
		} else if (!isValidCommand(args[0])) {
			System.err.println(args[0] + " is not valid command.");
		} else {
			compute(args);
		}
	}

	/**
	 * Check if given string is valid command
	 * @param command 
	 * @return true if command String is valid Command, 
	 * 			otherwise return false
	 */
	private static boolean isValidCommand(String command) {
		return commandsToParamNum.containsKey(command.toLowerCase());
	}

	/**
	 * Returns resolved path.
	 * @param path
	 * @return If given string path can be turned into absolute
	 * 			path return that absolute path. Otherwise resolve
	 *          it to working directory.
	 */
	private static Path getPathFromString(String path) {
		Path p = Paths.get(path);
		return p.isAbsolute() ? p : WORKING_DIR.resolve(p);
	}

	/**
	 * Method is used to set up Runnable object and run it.
	 * @param args
	 * @throws IllegalArgumentException if command recieved unexpected
	 * 				number of parameters.
	 */
	private static void compute(String[] args) {
		String command = args[0].toLowerCase();
		int expectedParameters = commandsToParamNum.get(command);
		if (args.length != expectedParameters + 1)
			throw new IllegalArgumentException(
					args[0] + " expects " + expectedParameters + "parameters. It recieved: " + (args.length - 1));

		Runnable r = null;
		if (command.equals("checksha")) {
			Path src = getPathFromString(args[1]);
			r = new Digest(src);
		} else if (command.equals("decrypt") || command.equals("encrypt")) {
			Path src = getPathFromString(args[1]);
			Path dest = getPathFromString(args[2]);
			r = new Crypt(command.equals("encrypt"), src, dest);
		}
		r.run();
	}

}
