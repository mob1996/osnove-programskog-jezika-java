package hr.fer.zemris.java.hw07.shell.commands;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;

/**
 * Class represents ls command.
 * @author Luka Mijić
 *
 */
public class LSCommand implements ShellCommand {

	/**
	 * Command name
	 */
	private static final String COMMAND_NAME = "ls";
	
	/**
	 * Used time format
	 */
	private static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	/**
	 * Lenght used for showing attribute size
	 */
	private static final int SIZE_ATTRIBUTE_LENGHT = 10;
	
	/**
	 * Format used for formatting size
	 */
	private static final String SIZE_ATTRIBUTE_FORMAT = "%" + SIZE_ATTRIBUTE_LENGHT + "s ";
	
	/**
	 * Description of command
	 */
	private static List<String> description;
	static {
		description = new ArrayList<>();
		description.add("Command: ls");
		description.add("\t- takes single argument, path that leads to directory");
		description.add("\t- lists information about every file and directory in that directory");
		description.add("\t- first column has 4 characters. d - is directory, "
				+ "r - is readable, w - is writable, x - is executable");
		description.add("\t- second column is object size in bytes");
		description.add("\t- third column is file creation date and time. yyyy-MM-dd HH:mm:ss");
		description.add("\t- fourth column is file name");
	}
	
	/**
	 * Method executes command ls. It lists info for every
	 * directory or file in given arguments path.
	 * @param env is Enviroment in which help is executed
	 * @param arguments
	 * @return ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> args = CommandUtil.toArguments(arguments);
		
		if(args.size() != 1) {
			env.writeln("Expected only one argument. Was " + args.size());
			return ShellStatus.CONTINUE;
		}
		
		Path dirPath = CommandUtil.getPathFromString(args.get(0));
		if(!Files.isDirectory(dirPath)) {
			env.writeln("Given path must lead to directory.");
			return ShellStatus.CONTINUE;
		}
		
		try {
			DirectoryStream<Path> pathStream = 
					Files.newDirectoryStream(dirPath);
			for(Path p:pathStream) {
				env.writeln(this.listAttributes(p));
			}
		} catch (IOException e) {
			env.writeln("IOexception with given path in ls command.");
		} 

		return ShellStatus.CONTINUE;
	}

	/**
	 * @return command name
	 */
	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	/**
	 * @return command description
	 */
	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(description);
	}

	/**
	 * Method is used to create String by using
	 * file attributes.
	 * @param path of file
	 * @return formatted string with attributes
	 * @throws IOException
	 */
	private String listAttributes(Path path) throws IOException {
		StringBuilder builder = new StringBuilder();
		
		builder = Files.isDirectory(path) ? builder.append("d") : builder.append("-");
		builder = Files.isReadable(path) ? builder.append("r") : builder.append("-");
		builder = Files.isWritable(path) ? builder.append("w") : builder.append("-");
		builder = Files.isExecutable(path) ? builder.append("x ") : builder.append("- ");
		
		
		BasicFileAttributeView faView = Files.getFileAttributeView(path, 
				BasicFileAttributeView.class, LinkOption.NOFOLLOW_LINKS);
		BasicFileAttributes attributes = faView.readAttributes();
		builder.append(String.format(SIZE_ATTRIBUTE_FORMAT, String.valueOf(attributes.size())));
		String formattedDateTime = TIME_FORMAT.format(new Date((attributes.creationTime().toMillis()))); 
		builder.append(formattedDateTime).append(" " + path.getFileName());
		
		return builder.toString();
	}
}
