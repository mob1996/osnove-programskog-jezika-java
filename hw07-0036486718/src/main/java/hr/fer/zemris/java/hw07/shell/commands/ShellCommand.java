package hr.fer.zemris.java.hw07.shell.commands;

import java.util.List;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;

/**
 * Interface models basic functionalities
 * of one shell command.
 * @author Luka Mijić
 *
 */
public interface ShellCommand {

	/**
	 * Method executes command
	 * @param env
	 * @param arguments
	 * @return ShellStatus.CONTINUE if command was executed alright,
	 * 			otherwise return ShellStatus.TERMINATE
	 */
	ShellStatus executeCommand(Environment env, String arguments);
	
	/**
	 * @return name of command
	 */
	String getCommandName();
	
	/**
	 * @return list containing all lines in command description.
	 */
	List<String> getCommandDescription();
}
