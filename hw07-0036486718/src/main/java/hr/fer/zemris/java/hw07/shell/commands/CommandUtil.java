package hr.fer.zemris.java.hw07.shell.commands;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import hr.fer.zemris.java.hw07.shell.lexer.ArgumentLexer;
import hr.fer.zemris.java.hw07.shell.parser.ArgumentParser;

/**
 * Class contains some usefull utilities for working with commands
 * 
 * @author Luka Mijić
 *
 */
public class CommandUtil {

	/**
	 * Generates list of String arguments from given string
	 * @param arguments
	 * @return list of arguments
	 */
	public static List<String> toArguments(String arguments) {
		return new ArgumentParser(arguments).getArguments();
	}

	/**
	 * Returns path that resolves relative path to absolute one
	 * @param pathStr
	 * @return absolute path
	 */
	public static Path getPathFromString(String pathStr) {
		if (!Paths.get(pathStr).isAbsolute()) {
			pathStr = pathStr.startsWith("\\") || pathStr.startsWith("/") ? 
					pathStr.substring(1) : pathStr; // "/work" leads to ROOT/WORK,
													// "work" leads to CURRENT_WORKSPACE/work.
		}
		return Paths.get(pathStr).toAbsolutePath();
	}

	/**
	 * Method extracts command from given input
	 * @param input 
	 * @return string of command
	 */
	public static String extractCommand(String input) {
		return new ArgumentLexer(input).nextToken().getValue();
	}
}
