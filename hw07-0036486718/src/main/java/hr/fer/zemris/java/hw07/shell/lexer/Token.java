package hr.fer.zemris.java.hw07.shell.lexer;

/**
 * Class represents token created by lexical analysis.
 * @author Luka Mijić
 *
 */
public class Token {

	/**
	 * Token type
	 */
	private TokenType type;
	
	/**
	 * Value of tokens
	 */
	private String value;
	
	/**
	 * Constructor that takes two arguments for 
	 * creation of new Token. 
	 * @param type of created Token
	 * @param value of created Token
	 */
	public Token(TokenType type, String value) {
		if(type == null) throw new IllegalArgumentException("Type cannot be null.");
		
		this.type = type;
		this.value = value;
	}

	/**
	 * @return token type
	 */
	public TokenType getType() {
		return type;
	}

	/**
	 * @return value of token
	 */
	public String getValue() {
		return value;
	}
}
