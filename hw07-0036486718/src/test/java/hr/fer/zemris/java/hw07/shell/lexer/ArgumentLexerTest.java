package hr.fer.zemris.java.hw07.shell.lexer;

import static org.junit.Assert.*;

import org.junit.Test;

public class ArgumentLexerTest {
	
	@Test
	public void testEOF() {
		ArgumentLexer lexer = new ArgumentLexer("");
		
		Token t = lexer.nextToken();
		
		assertEquals(TokenType.EOF, t.getType());
		assertNull(t.getValue());
	}
	
	@Test (expected = LexerException.class)
	public void testExceptionAfterEOF() {
		ArgumentLexer lexer = new ArgumentLexer("");
		
		Token t = lexer.nextToken();
		
		assertEquals(TokenType.EOF, t.getType());
		assertNull(t.getValue());
		
		lexer.nextToken();
	}
	
	@Test
	public void testValidArgumentWithoutQuotes() {
		
		ArgumentLexer lexer = new ArgumentLexer("argument0  ");
		
		Token t = lexer.nextToken();
		
		assertEquals(TokenType.ARGUMENT, t.getType());
		assertEquals("argument0", t.getValue());
		
		t = lexer.nextToken();
		assertEquals(TokenType.EOF, t.getType());
		assertNull(t.getValue());
	}
	
	@Test
	public void testValidArgumentWithQuotes() {
		ArgumentLexer lexer = new ArgumentLexer("\"arg0  arg1\t\n!arg3   \"    ");
		
		Token t = lexer.nextToken();
		
		assertEquals(TokenType.ARGUMENT, t.getType());
		assertEquals("arg0  arg1\t\n!arg3   ", t.getValue());
		
		t = lexer.nextToken();
		assertEquals(TokenType.EOF, t.getType());
		assertNull(t.getValue());
	}
	
	@Test (expected = LexerException.class)
	public void testWithoutEnclosingQoute() {
		ArgumentLexer lexer = new ArgumentLexer("\"arg0  arg1\t\n!arg3       ");
		
		lexer.nextToken();
	}
	
	@Test (expected = LexerException.class)
	public void testComplicatedWithoutEnclosingQuote() {
		String text = "arg0\t\"arg2 arg3/\" \"arg4 fail here";
		ArgumentLexer lexer = new ArgumentLexer(text);
		
		Token t = lexer.nextToken();
		
		assertEquals(TokenType.ARGUMENT, t.getType());
		assertEquals("arg0", t.getValue());
		
		t = lexer.nextToken();
		
		assertEquals(TokenType.ARGUMENT, t.getType());
		assertEquals("arg2 arg3/", t.getValue());
		
		lexer.nextToken();
	}
	
	@Test
	public void testMultipleValidInputs() {
		String text = "arg0\t\"arg2 arg3/\" \"arg4\targ5\"    \"F:/Games/Installations/God of War\"";
		ArgumentLexer lexer = new ArgumentLexer(text);
		
		Token t = lexer.nextToken();
		assertEquals(TokenType.ARGUMENT, t.getType());
		assertEquals("arg0", t.getValue());
		
		t = lexer.nextToken();
		
		assertEquals(TokenType.ARGUMENT, t.getType());
		assertEquals("arg2 arg3/", t.getValue());
		
		t = lexer.nextToken();
		
		assertEquals(TokenType.ARGUMENT, t.getType());
		assertEquals("arg4\targ5", t.getValue());
		
		t = lexer.nextToken();
		
		assertEquals(TokenType.ARGUMENT, t.getType());
		assertEquals("F:/Games/Installations/God of War", t.getValue());
		
		t = lexer.nextToken();
		assertEquals(TokenType.EOF, t.getType());
		assertNull(t.getValue());
	}
}
