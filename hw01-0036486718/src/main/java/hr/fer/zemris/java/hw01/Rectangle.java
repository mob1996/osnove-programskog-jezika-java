package hr.fer.zemris.java.hw01;

import java.util.Locale;
import java.util.Scanner;

/*
 * Class for calculating area of Rectangle.
 * @author Luka Mijic
 * @version 1.0
 */
public class Rectangle {

	/*
	 * Main method is called when program starts.
	 * @param args arguments from command line used for calculating area and perimiter
	 */
	public static void main(String[] args) {
		double width;
		double height;
		switch (args.length) {
			case 0:
				Scanner sc = new Scanner(System.in);
				sc.useLocale(Locale.UK);
				width = scan("širinu", sc);
				height = scan("visinu", sc);
				
				printResult(width, height, calculateArea(width, height), calculatePerimeter(width, height));
				
				sc.close();
				break;
				
			case 2: 
				if(!checkIfValidValue(args[0]) || !checkIfValidValue(args[1])) {
					System.out.println("Dani argumenti nisu valjani.");
					return;
				}
				
				width = Double.valueOf(args[0]);
				height = Double.valueOf(args[1]);
				
				printResult(width, height, calculateArea(width, height), calculatePerimeter(width, height));
				
				break;
			default:
				System.out.println("Neispravan broj argumenata! Prekid rada.");
				break;
		}

	}
	
	/*
	 * Method is used for checking if given string can be
	 * transformed into valid double value.
	 * Value is valid if it can be interpreted as number and if
	 * interpreted number is not negative.
	 * @param str
	 */
	public static boolean checkIfValidValue(String str) {
		try {
			return Double.parseDouble(str) > 0.0 ? true: false;
		} catch(NumberFormatException exc) {
			return false;
		}
	}
	
	/*
	 * Method that checks if String can be interpreted as a number.
	 * @param str argument that method is checking.
	 */
	public static boolean isNumber(String str) {
		try {
			Double.parseDouble(str);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
	/*
	 * Method is used for calculating area of rectangle. 
	 * Formula: width * height
	 * @param a width of rectangle
	 * @param b height of rectangle
	 */
	public static double calculateArea(double width, double height) {
		return width*height;
	}
	
	/*
	 * Method is used for calculating perimeter of rectangle. 
	 * Formula: 2 * (width + height)
	 * @param a width of rectangle
	 * @param b height of rectangle
	 */
	public static double calculatePerimeter(double width, double height) {
		return 2 * (width+height);
	}
	
	/*
	 * Method is used for scaning values for width/height variables
	 * @ str string that is used to indicate what people should enter
	 * @ sc Scanner
	 */
	private static double scan(String str, Scanner sc) {
		double value = 0;
		
		while(true) {
			System.out.print("Unesite " + str +" > ");
			String line = sc.nextLine();
			
			if(checkIfValidValue(line)) {
				value = Double.valueOf(line);
				break;
			} else {
				if(isNumber(line)) {
					System.out.println("Unijeli ste ne pozitivnu vrijednost.");
				} else {
					System.out.printf("\'%s\' se ne može protumačiti kao broj.\n", line);
				}
			}
		}
		
		return value;
	}
	
	/*
	 * Method is used for printing result on standard output.
	 * Locale is set to UK so double used '.' instead of ','. 
	 */
	private static void printResult(double width, double height, double area, double perimiter) {
		String s = String.format(Locale.UK,"Pravokutnik širine %.1f i visine %.1f ima površinu %.1f te opseg %.1f.", width, height, area, perimiter);
		System.out.println(s);
	}

}
