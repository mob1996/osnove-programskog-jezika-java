package hr.fer.zemris.java.hw01;

import java.util.Scanner;

/*
 * Program for implementing and working binary tree structure.
 * @author Luka Mijic
 * @version 1.0
 */
public class UniqueNumbers {

	private static final String STOP_SCAN = "kraj";
	
	/*
	 * Class representing node of binary tree
	 */
	static class TreeNode{
		int value;
		TreeNode left;
		TreeNode right;
	}
	
	/*
	 * Main method is called when program starts.
	 * Gives user ability to add nodes to a tree.
	 * @param args arguments from command line
	 */
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		TreeNode head = null;
		
		while(true) {
			System.out.print("Unesite broj > ");
			String inputString;
			
			if((inputString = sc.nextLine()).toLowerCase().equals(STOP_SCAN)) {           
				System.out.print("Ispis od najmanjeg: " + ascended(head));
				System.out.print("\nIspis od najvećeg: " + descended(head));
				break;
			} 
			
			try {
				int inputNumber = Integer.valueOf(inputString);
				if(containsValue(head, inputNumber)) {
					System.out.println("Broj već postoji. Preskačem.");
				} else {
					head = addNode(head, inputNumber);
					System.out.println("Dodano.");
				}
				
			} catch (NumberFormatException exc){
				System.out.printf("\'%s\' nije cijeli broj.\n", inputString);
			}
		}
		
		sc.close();
	}
	
	/*
	 * Method that is used for adding a node to a binary tree.
	 * @param node current node
	 * @param value value that method wants to add
	 */
	public static TreeNode addNode(TreeNode node, int value) {
		if(node == null) {
			node = new TreeNode();
			node.value = value;
			node.left = null;
			node.right = null;
			return node;
		}
		
		if(value < node.value) {
			node.left = addNode(node.left, value);
		} else if(value > node.value){
			node.right = addNode(node.right, value);
		}
		return node;
	}
	
	/*
	 * Method for calculating number of nodes in a tree.
	 * @param node Current node in tree traversal.  
	 */
	public static int treeSize(TreeNode node) {
		if(node == null) {
			return 0;
		}
		return 1 + treeSize(node.left) + treeSize(node.right);
	}
	
	/*
	 * Method is used to check if binary tree contains given value.
	 * @param node Current node in tree traversal.
	 * @param value value that we are looking for in tree. 
	 */
	public static boolean containsValue(TreeNode node, int value) {
		if(node == null) {
			return false;
		} else if(node.value == value) {
			return true;
		} else {
			return value < node.value ? containsValue(node.left, value) : containsValue(node.right, value);
		}
	}
	
	/*
	 * Method is used to print all values inside a tree in ascending order.
	 * @param node Current node in tree traversal
	 */
	public static String ascended(TreeNode node) {
		if(node == null) {
			return "";
		}
		
		return ascended(node.left) + node.value + " " + ascended(node.right);
	}
	
	/*
	 * Method is used to print all values inside a tree in descending order.
	 * @param node Current node in tree traversal
	 */
	public static String descended(TreeNode node) {
		if(node == null) {
			return "";
		}
		
		return descended(node.right) + node.value + " " + descended(node.left);
	}

}
