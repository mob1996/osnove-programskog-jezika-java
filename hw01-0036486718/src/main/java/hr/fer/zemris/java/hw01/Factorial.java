package hr.fer.zemris.java.hw01;

import java.util.Scanner;

/*
 * Program for calculating factorial value.
 * @author Luka Mijic
 * @version 1.0
 */
public class Factorial {

	private static final String STOP_SCAN = "kraj";
	private static final int MIN_FACT_ARG = 1;
	private static final int MAX_FACT_ARG = 20;
	
	/*
	 * Main method is called when program starts.
	 * @param args arguments from command line
	 */
	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
		
		while(true) {
			System.out.print("Unesite broj > ");
			String inputString;
			if((inputString = sc.nextLine()).equals(STOP_SCAN)) {           
				System.out.println("Doviđenja");
				break;
			} 
			
			try {
				int inputNumber = Integer.valueOf(inputString);
				if(inputNumber<MIN_FACT_ARG || inputNumber>MAX_FACT_ARG) {
					System.out.printf("\'%s\' nije u dozvoljenom rasponu.\n", inputString);
					continue;
				}
				System.out.println(inputNumber + "! = " + factorial(inputNumber));
			} catch (NumberFormatException exc){
				System.out.printf("\'%s\' nije cijeli broj.\n", inputString);
			}
			
		}
		
		sc.close();	
		
	}
	
	/*
	 * Recursive function for calculating n!.
	 * Recursive formula: n! = n * (n-1)!
	 * @param n argument for n!
	 */
	
	public static long factorial(int n) {
		if(n == 1) {
			return 1L;
		} else {
			return n * factorial(n-1);
		}
	}

}
