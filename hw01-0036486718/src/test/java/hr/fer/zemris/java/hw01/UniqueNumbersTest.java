package hr.fer.zemris.java.hw01;

import org.junit.Assert;
import org.junit.Test;
import static hr.fer.zemris.java.hw01.UniqueNumbers.TreeNode;
import static hr.fer.zemris.java.hw01.UniqueNumbers.treeSize;
import static hr.fer.zemris.java.hw01.UniqueNumbers.containsValue;
import static hr.fer.zemris.java.hw01.UniqueNumbers.addNode;
import static hr.fer.zemris.java.hw01.UniqueNumbers.ascended;
import static hr.fer.zemris.java.hw01.UniqueNumbers.descended;

/*
 * Class for testing methods of UniqueNumbers class
 * @author Luka Mijic
 * @version 1.0
 */

public class UniqueNumbersTest {

	@Test
	public void checkTreeSize() {
		TreeNode head = null;
		int expected = 0;
		
		Assert.assertEquals(expected, treeSize(head));
		
		head = addNode(head, 15);
		head = addNode(head, 10);
		head = addNode(head, 18);
		
		expected = 3;
		Assert.assertEquals(expected, treeSize(head));
		
		head = addNode(head, 25);
		head = addNode(head, 16);
		head = addNode(head, 16);
		
		expected = 5;
		Assert.assertEquals(expected, treeSize(head));
		
		head = addNode(head, 30);
		head = addNode(head, 27);
		head = addNode(head, 27);
		head = addNode(head, 12);
		
		expected = 8;
		Assert.assertEquals(expected, treeSize(head));
		
	}
	
	@Test
	public void checkContainsValue() {
		TreeNode head = null;
		
		Assert.assertFalse(containsValue(head, 15));
		
		head = addNode(head, 15);
		head = addNode(head, 10);
		head = addNode(head, 18);
		
		Assert.assertTrue(containsValue(head, 15));
		Assert.assertTrue(containsValue(head, 10));
		Assert.assertTrue(containsValue(head, 18));
		
		Assert.assertFalse(containsValue(head, -15));
		Assert.assertFalse(containsValue(head, -16));
		
		head = addNode(head, -15);
		head = addNode(head, -16);
		head = addNode(head, -5);
		head = addNode(head, 0);
		
		Assert.assertTrue(containsValue(head, -15));
		Assert.assertTrue(containsValue(head, -16));
		Assert.assertTrue(containsValue(head, -5));
		Assert.assertTrue(containsValue(head, 0));
		
	}
	
	@Test
	public void checkSortAscended() {
		TreeNode head = null;
		
		head = addNode(head, 15);
		head = addNode(head, 10);
		head = addNode(head, 18);
		head = addNode(head, 25);
		head = addNode(head, 16);
		head = addNode(head, 30);
		head = addNode(head, 27);
		head = addNode(head, 6);
		head = addNode(head, 12);
		
		String[] sorted = ascended(head).split("\\s+");
		
		for(int i = 1; i < sorted.length; i++) {
			int num1 = Integer.valueOf(sorted[i-1]);
			int num2 = Integer.valueOf(sorted[i]);
			
			Assert.assertTrue(num2>num1);
		}
	}
	
	@Test
	public void checkSortDescended() {
		TreeNode head = null;
		
		head = addNode(head, 15);
		head = addNode(head, 10);
		head = addNode(head, 18);
		head = addNode(head, 25);
		head = addNode(head, 16);
		head = addNode(head, 30);
		head = addNode(head, 27);
		head = addNode(head, 6);
		head = addNode(head, 12);
		
		String[] sorted = descended(head).split("\\s+");
		
		for(int i = 1; i < sorted.length; i++) {
			int num1 = Integer.valueOf(sorted[i-1]);
			int num2 = Integer.valueOf(sorted[i]);
			
			Assert.assertTrue(num2<num1);
		}
	}
	
	
}
