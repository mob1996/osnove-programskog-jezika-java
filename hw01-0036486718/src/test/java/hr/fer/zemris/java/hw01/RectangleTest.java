package hr.fer.zemris.java.hw01;

import org.junit.Assert;
import org.junit.Test;
import static hr.fer.zemris.java.hw01.Rectangle.calculateArea;
import static hr.fer.zemris.java.hw01.Rectangle.calculatePerimeter;
import static hr.fer.zemris.java.hw01.Rectangle.isNumber;
import static hr.fer.zemris.java.hw01.Rectangle.checkIfValidValue;

public class RectangleTest {

	@Test
	public void testArea() {
		double epsilon = 0.001;
		double width = 5;
		double height = 6;
		double expected  = width * height;
		
		Assert.assertEquals(expected, calculateArea(width, height), epsilon);
		
		width = 15.042;
		height = 3.14;
		expected = width * height;
		
		Assert.assertEquals(expected, calculateArea(width, height), epsilon);
		
		width = 1534.054;
		height = 442.03;
		expected = width * height;
		
		Assert.assertEquals(expected, calculateArea(width, height), epsilon);
		
	}
	
	@Test
	public void testPerimeter() {
		double epsilon = 0.001;
		double width = 5;
		double height = 6;
		double expected = 2 * (width + height);
		
		Assert.assertEquals(expected, calculatePerimeter(width, height), epsilon);
		
		width = 15.042;
		height = 3.14;
		expected = 2 * (width + height);
		
		Assert.assertEquals(expected, calculatePerimeter(width, height), epsilon);
		
		width = 1534.054;
		height = 442.03;
		expected = 2 * (width + height);
		
		Assert.assertEquals(expected, calculatePerimeter(width, height), epsilon);
		
	}

	@Test
	public void testIsNumber() {
		String str = "stefica";
		Assert.assertFalse(isNumber(str));
		
		str = "";
		Assert.assertFalse(isNumber(str));
		
		str = "3.25";
		Assert.assertTrue(isNumber(str));
		
		str = "3";
		Assert.assertTrue(isNumber(str));
	}
	

	@Test
	public void testCheckIfValidValue() {
		String str = "stefica";
		Assert.assertFalse(checkIfValidValue(str));
		
		str = "";
		Assert.assertFalse(checkIfValidValue(str));
		
		str = "-5.25";
		Assert.assertFalse(checkIfValidValue(str));
		
		str = "0";
		Assert.assertFalse(checkIfValidValue(str));
		
		str = "-3";
		Assert.assertFalse(checkIfValidValue(str));
		
		str = "3.25";
		Assert.assertTrue(checkIfValidValue(str));
		
		str = "3";
		Assert.assertTrue(checkIfValidValue(str));
	}
}
