package hr.fer.zemris.java.hw01;

import org.junit.Assert;
import org.junit.Test;

/*
 * Class for testing correctness of Factorial class
 * @author Luka Mijic
 * @version 1.0
 */

public class FactorialTest {

	@Test
	public void forTen() {
		int n = 10;
		long expectedResult = 3628800L;
		long realResult = Factorial.factorial(n);
		
		Assert.assertEquals(expectedResult, realResult);
	}

	@Test
	public void forTwenty() {
		int n = 20;
		long expectedResult = 2432902008176640000L;
		long realResult = Factorial.factorial(n);
		
		Assert.assertEquals(expectedResult, realResult);
	}
	
	
}
