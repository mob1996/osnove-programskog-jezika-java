package hr.fer.zemris.java.hw05.db;

import static org.junit.Assert.*;
import static hr.fer.zemris.java.hw05.db.ComparisonOperators.*;
import static hr.fer.zemris.java.hw05.db.FieldValueGetters.*;

import org.junit.Test;

public class ConditionalExpressionsTest {

	@Test
	public void testLess() {
		StudentRecord record = new StudentRecord("0036486718", "Mijić", "Luka", 4);
		
		IComparisonOperator operator = LESS;
		ConditionalExpression e1 = new ConditionalExpression(operator, JMBAG, "0046486718");
		ConditionalExpression e2 = new ConditionalExpression(operator, LAST_NAME, "Topolec");
		ConditionalExpression e3 = new ConditionalExpression(operator, FIRST_NAME, "Marko");
		
		assertTrue(e1.getComparisonOperator().satisfied(e1.getFieldGetter().get(record), e1.getStringLiteral()));
		assertTrue(e2.getComparisonOperator().satisfied(e2.getFieldGetter().get(record), e2.getStringLiteral()));
		assertTrue(e3.getComparisonOperator().satisfied(e3.getFieldGetter().get(record), e3.getStringLiteral()));
		
		ConditionalExpression e4 = new ConditionalExpression(operator, JMBAG, "0026486718");
		ConditionalExpression e5 = new ConditionalExpression(operator, LAST_NAME, "Marković");
		ConditionalExpression e6 = new ConditionalExpression(operator, FIRST_NAME, "Ana");
		
		assertFalse(e4.getComparisonOperator().satisfied(e4.getFieldGetter().get(record), e4.getStringLiteral()));
		assertFalse(e5.getComparisonOperator().satisfied(e5.getFieldGetter().get(record), e5.getStringLiteral()));
		assertFalse(e6.getComparisonOperator().satisfied(e6.getFieldGetter().get(record), e6.getStringLiteral()));
	}
	
	@Test
	public void testLessOrEqual() {
		StudentRecord record = new StudentRecord("0036486718", "Mijić", "Luka", 4);
		
		IComparisonOperator operator = LESS_OR_EQUAL;
		ConditionalExpression e1 = new ConditionalExpression(operator, JMBAG, "0036486718");
		ConditionalExpression e2 = new ConditionalExpression(operator, LAST_NAME, "Topolec");
		ConditionalExpression e3 = new ConditionalExpression(operator, FIRST_NAME, "Luka");
		
		assertTrue(e1.getComparisonOperator().satisfied(e1.getFieldGetter().get(record), e1.getStringLiteral()));
		assertTrue(e2.getComparisonOperator().satisfied(e2.getFieldGetter().get(record), e2.getStringLiteral()));
		assertTrue(e3.getComparisonOperator().satisfied(e3.getFieldGetter().get(record), e3.getStringLiteral()));
		
		ConditionalExpression e4 = new ConditionalExpression(operator, JMBAG, "0026486718");
		ConditionalExpression e5 = new ConditionalExpression(operator, LAST_NAME, "Marković");
		ConditionalExpression e6 = new ConditionalExpression(operator, FIRST_NAME, "Ana");
		
		assertFalse(e4.getComparisonOperator().satisfied(e4.getFieldGetter().get(record), e4.getStringLiteral()));
		assertFalse(e5.getComparisonOperator().satisfied(e5.getFieldGetter().get(record), e5.getStringLiteral()));
		assertFalse(e6.getComparisonOperator().satisfied(e6.getFieldGetter().get(record), e6.getStringLiteral()));
	}
	
	@Test
	public void testGreater() {
		StudentRecord record = new StudentRecord("0036486718", "Mijić", "Luka", 4);
		
		IComparisonOperator operator = GREATER;
		ConditionalExpression e1 = new ConditionalExpression(operator, JMBAG, "0026486718");
		ConditionalExpression e2 = new ConditionalExpression(operator, LAST_NAME, "Marković");
		ConditionalExpression e3 = new ConditionalExpression(operator, FIRST_NAME, "Ana");
		
		assertTrue(e1.getComparisonOperator().satisfied(e1.getFieldGetter().get(record), e1.getStringLiteral()));
		assertTrue(e2.getComparisonOperator().satisfied(e2.getFieldGetter().get(record), e2.getStringLiteral()));
		assertTrue(e3.getComparisonOperator().satisfied(e3.getFieldGetter().get(record), e3.getStringLiteral()));
		
		ConditionalExpression e4 = new ConditionalExpression(operator, JMBAG, "0046486718");
		ConditionalExpression e5 = new ConditionalExpression(operator, LAST_NAME, "Topolec");
		ConditionalExpression e6 = new ConditionalExpression(operator, FIRST_NAME, "Marko");
		
		assertFalse(e4.getComparisonOperator().satisfied(e4.getFieldGetter().get(record), e4.getStringLiteral()));
		assertFalse(e5.getComparisonOperator().satisfied(e5.getFieldGetter().get(record), e5.getStringLiteral()));
		assertFalse(e6.getComparisonOperator().satisfied(e6.getFieldGetter().get(record), e6.getStringLiteral()));
	}
	
	@Test
	public void testGreaterOrEqual() {
		StudentRecord record = new StudentRecord("0036486718", "Mijić", "Luka", 4);
		
		IComparisonOperator operator = GREATER_OR_EQUAL;
		ConditionalExpression e1 = new ConditionalExpression(operator, JMBAG, "0026486718");
		ConditionalExpression e2 = new ConditionalExpression(operator, LAST_NAME, "Marković");
		ConditionalExpression e3 = new ConditionalExpression(operator, FIRST_NAME, "Luka");
		
		assertTrue(e1.getComparisonOperator().satisfied(e1.getFieldGetter().get(record), e1.getStringLiteral()));
		assertTrue(e2.getComparisonOperator().satisfied(e2.getFieldGetter().get(record), e2.getStringLiteral()));
		assertTrue(e3.getComparisonOperator().satisfied(e3.getFieldGetter().get(record), e3.getStringLiteral()));
		
		ConditionalExpression e4 = new ConditionalExpression(operator, JMBAG, "0046486718");
		ConditionalExpression e5 = new ConditionalExpression(operator, LAST_NAME, "Topolec");
		ConditionalExpression e6 = new ConditionalExpression(operator, FIRST_NAME, "Marko");
		
		assertFalse(e4.getComparisonOperator().satisfied(e4.getFieldGetter().get(record), e4.getStringLiteral()));
		assertFalse(e5.getComparisonOperator().satisfied(e5.getFieldGetter().get(record), e5.getStringLiteral()));
		assertFalse(e6.getComparisonOperator().satisfied(e6.getFieldGetter().get(record), e6.getStringLiteral()));
	}
	
	@Test
	public void testEqual() {
		StudentRecord record = new StudentRecord("0036486718", "Mijić", "Luka", 4);
		
		IComparisonOperator operator = EQUALS;
		ConditionalExpression e1 = new ConditionalExpression(operator, JMBAG, "0036486718");
		ConditionalExpression e2 = new ConditionalExpression(operator, LAST_NAME, "Mijić");
		ConditionalExpression e3 = new ConditionalExpression(operator, FIRST_NAME, "Luka");
		
		assertTrue(e1.getComparisonOperator().satisfied(e1.getFieldGetter().get(record), e1.getStringLiteral()));
		assertTrue(e2.getComparisonOperator().satisfied(e2.getFieldGetter().get(record), e2.getStringLiteral()));
		assertTrue(e3.getComparisonOperator().satisfied(e3.getFieldGetter().get(record), e3.getStringLiteral()));
		
		ConditionalExpression e4 = new ConditionalExpression(operator, JMBAG, "0046486718");
		ConditionalExpression e5 = new ConditionalExpression(operator, LAST_NAME, "Topolec");
		ConditionalExpression e6 = new ConditionalExpression(operator, FIRST_NAME, "Marko");
		
		assertFalse(e4.getComparisonOperator().satisfied(e4.getFieldGetter().get(record), e4.getStringLiteral()));
		assertFalse(e5.getComparisonOperator().satisfied(e5.getFieldGetter().get(record), e5.getStringLiteral()));
		assertFalse(e6.getComparisonOperator().satisfied(e6.getFieldGetter().get(record), e6.getStringLiteral()));
	}
	
	@Test
	public void testNotEqual() {
		StudentRecord record = new StudentRecord("0036486718", "Mijić", "Luka", 4);
		
		IComparisonOperator operator = NOT_EQUALS;
		ConditionalExpression e1 = new ConditionalExpression(operator, JMBAG, "0026486718");
		ConditionalExpression e2 = new ConditionalExpression(operator, LAST_NAME, "Mjić");
		ConditionalExpression e3 = new ConditionalExpression(operator, FIRST_NAME, "Lka");
		
		assertTrue(e1.getComparisonOperator().satisfied(e1.getFieldGetter().get(record), e1.getStringLiteral()));
		assertTrue(e2.getComparisonOperator().satisfied(e2.getFieldGetter().get(record), e2.getStringLiteral()));
		assertTrue(e3.getComparisonOperator().satisfied(e3.getFieldGetter().get(record), e3.getStringLiteral()));
		
		ConditionalExpression e4 = new ConditionalExpression(operator, JMBAG, "0036486718");
		ConditionalExpression e5 = new ConditionalExpression(operator, LAST_NAME, "Mijić");
		ConditionalExpression e6 = new ConditionalExpression(operator, FIRST_NAME, "Luka");
		
		assertFalse(e4.getComparisonOperator().satisfied(e4.getFieldGetter().get(record), e4.getStringLiteral()));
		assertFalse(e5.getComparisonOperator().satisfied(e5.getFieldGetter().get(record), e5.getStringLiteral()));
		assertFalse(e6.getComparisonOperator().satisfied(e6.getFieldGetter().get(record), e6.getStringLiteral()));
	}
	
	@Test
	public void testLike() {
		StudentRecord record = new StudentRecord("0036486718", "Mijić", "Luka", 4);
		
		IComparisonOperator operator = LIKE;
		ConditionalExpression e1 = new ConditionalExpression(operator, JMBAG, "*18");
		ConditionalExpression e2 = new ConditionalExpression(operator, LAST_NAME, "M*ć");
		ConditionalExpression e3 = new ConditionalExpression(operator, FIRST_NAME, "L*");
		
		assertTrue(e1.getComparisonOperator().satisfied(e1.getFieldGetter().get(record), e1.getStringLiteral()));
		assertTrue(e2.getComparisonOperator().satisfied(e2.getFieldGetter().get(record), e2.getStringLiteral()));
		assertTrue(e3.getComparisonOperator().satisfied(e3.getFieldGetter().get(record), e3.getStringLiteral()));
		
		ConditionalExpression e4 = new ConditionalExpression(operator, JMBAG, "*12");
		ConditionalExpression e5 = new ConditionalExpression(operator, LAST_NAME, "Mu*ć");
		ConditionalExpression e6 = new ConditionalExpression(operator, FIRST_NAME, "K*");
		
		assertFalse(e4.getComparisonOperator().satisfied(e4.getFieldGetter().get(record), e4.getStringLiteral()));
		assertFalse(e5.getComparisonOperator().satisfied(e5.getFieldGetter().get(record), e5.getStringLiteral()));
		assertFalse(e6.getComparisonOperator().satisfied(e6.getFieldGetter().get(record), e6.getStringLiteral()));
	}
}
