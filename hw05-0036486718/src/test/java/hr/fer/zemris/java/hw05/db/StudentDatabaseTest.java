package hr.fer.zemris.java.hw05.db;

import org.junit.Test;
import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
public class StudentDatabaseTest {

	private static final String RELATIVE_PATH = "src/main/resources/database.txt";
	private static final IFilter ALL_TRUE = (record) -> true;
	private static final IFilter ALL_FALSE = (record) -> false;
	private static int numberOfRecords;
 	
	private static StudentDatabase database;
	static {
		numberOfRecords = 0;
		List<String> lines = new LinkedList<>();
		try (BufferedReader reader = new BufferedReader(new FileReader(RELATIVE_PATH))) {
		    String line;
		    while ((line = reader.readLine()) != null) {
		      lines.add(line);
		      numberOfRecords++;
		    }
		} catch(FileNotFoundException exc) {
			System.err.println("Given file doesnt exist.");
		} catch(IOException exc) {
			System.err.println("IOException happened.");
		}
		
		database = new StudentDatabase(lines);
	}
	
	@Test
	public void testFilter0() {
		List<StudentRecord> allAcceptable = database.filter(ALL_TRUE);
		
		assertEquals(numberOfRecords, allAcceptable.size());
	}
	
	@Test
	public void testFilter1() {
		List<StudentRecord> allAcceptable = database.filter(ALL_FALSE);
		
		assertEquals(0, allAcceptable.size());
	}
	
	@Test
	public void testFilter2() {
		List<StudentRecord> passExam = database.filter(record  -> record.getFinalGrade() > 1);
		
		for(StudentRecord record:passExam) {
			assertNotEquals(1, record.getFinalGrade());
		}
		
		for(StudentRecord record:database.filter(ALL_TRUE)) {
			if(record.getFinalGrade() > 1) {
				assertTrue(passExam.contains(record));
			} else {
				assertFalse(passExam.contains(record));
			}
		}
	}
	
	@Test
	public void testFilter3() {
		List<StudentRecord> passExam = database.filter(record  -> record.getFirstName().startsWith("L"));
		for(StudentRecord record:passExam) {
			assertTrue(record.getFirstName().startsWith("L"));
		}
		
		for(StudentRecord record:database.filter(ALL_TRUE)) {
			if(record.getFirstName().startsWith("L")) {
				assertTrue(passExam.contains(record));
			} else {
				assertFalse(passExam.contains(record));
			}
		}
	}
	
	@Test
	public void testForJmbag() {
		StudentRecord record = database.forJmbag("0000000022");
		
		assertEquals("0000000022", record.getJmbag());
		assertEquals("Jurina", record.getLastName());
		assertEquals("Filip", record.getFirstName());
		assertEquals(3, record.getFinalGrade());
		
		record = database.forJmbag("0000000001");
		assertEquals("0000000001", record.getJmbag());
		assertEquals("Akšamović", record.getLastName());
		assertEquals("Marin", record.getFirstName());
		assertEquals(2, record.getFinalGrade());
		
		record = database.forJmbag("0000000063");
		assertEquals("0000000063", record.getJmbag());
		assertEquals("Žabčić", record.getLastName());
		assertEquals("Željko", record.getFirstName());
		assertEquals(4, record.getFinalGrade());
	}
	
	@Test
	public void testForJmbagInvalid() {
		StudentRecord record = database.forJmbag("0000000322");
		
		assertNull(record);
	}
	
}
