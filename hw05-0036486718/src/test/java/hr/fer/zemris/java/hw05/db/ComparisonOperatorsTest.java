package hr.fer.zemris.java.hw05.db;

import static org.junit.Assert.*;
import static hr.fer.zemris.java.hw05.db.ComparisonOperators.*;
import org.junit.Test;

public class ComparisonOperatorsTest {

	@Test
	public void testLess() {
		String value1 = "Andrej";
		String value2 = "Borna";
		
		assertTrue(LESS.satisfied(value1, value2));
		assertFalse(LESS.satisfied(value2, value1));
		
		value1 = "Luka";
		value2 = "Tomislav";
		
		assertTrue(LESS.satisfied(value1, value2));
		assertFalse(LESS.satisfied(value2, value1));
		
		value1 = "Luka";
		value2 = "luka";
		assertTrue(LESS.satisfied(value1, value2));
		assertFalse(LESS.satisfied(value2, value1));
		
		value1 = "Lucija";
		value2 = "Luka";
		
		assertTrue(LESS.satisfied(value1, value2));
		assertFalse(LESS.satisfied(value2, value1));
	}
	
	@Test
	public void testLessOrEqual() {
		String value1 = "Andrej";
		String value2 = "Borna";
		
		assertTrue(LESS_OR_EQUAL.satisfied(value1, value2));
		assertFalse(LESS_OR_EQUAL.satisfied(value2, value1));
		
		value1 = "Luka";
		value2 = "Tomislav";
		
		assertTrue(LESS_OR_EQUAL.satisfied(value1, value2));
		assertFalse(LESS_OR_EQUAL.satisfied(value2, value1));
		
		value1 = "Lucija";
		value2 = "Luka";
		
		assertTrue(LESS_OR_EQUAL.satisfied(value1, value2));
		assertFalse(LESS_OR_EQUAL.satisfied(value2, value1));
		
		value1 = "Luka";
		value2 = "luka";
		assertTrue(LESS_OR_EQUAL.satisfied(value1, value2));
		assertFalse(LESS_OR_EQUAL.satisfied(value2, value1));
		
		value1 = "Luka";
		value2 = "Luka";
		
		assertTrue(LESS_OR_EQUAL.satisfied(value1, value2));
		assertTrue(LESS_OR_EQUAL.satisfied(value2, value1));
	}
	
	@Test
	public void testGreater() {
		String value1 = "Borna";
		String value2 = "Andrej";
		
		assertTrue(GREATER.satisfied(value1, value2));
		assertFalse(GREATER.satisfied(value2, value1));
		
		value1 = "Tomislav";
		value2 = "Luka";
		
		assertTrue(GREATER.satisfied(value1, value2));
		assertFalse(GREATER.satisfied(value2, value1));
		
		value1 = "luka";
		value2 = "Luka";
		assertTrue(GREATER.satisfied(value1, value2));
		assertFalse(GREATER.satisfied(value2, value1));
		
		value1 = "Luka";
		value2 = "Lucija";
		
		assertTrue(GREATER.satisfied(value1, value2));
		assertFalse(GREATER.satisfied(value2, value1));
	}
	
	@Test
	public void testGreaterOrEqual() {
		String value1 = "Borna";
		String value2 = "Andrej";
		
		assertTrue(GREATER_OR_EQUAL.satisfied(value1, value2));
		assertFalse(GREATER_OR_EQUAL.satisfied(value2, value1));
		
		value1 = "Tomislav";
		value2 = "Luka";
		
		assertTrue(GREATER_OR_EQUAL.satisfied(value1, value2));
		assertFalse(GREATER_OR_EQUAL.satisfied(value2, value1));
		
		value1 = "luka";
		value2 = "Luka";
		assertTrue(GREATER_OR_EQUAL.satisfied(value1, value2));
		assertFalse(GREATER_OR_EQUAL.satisfied(value2, value1));
		
		value1 = "Luka";
		value2 = "Lucija";
		
		assertTrue(GREATER_OR_EQUAL.satisfied(value1, value2));
		assertFalse(GREATER_OR_EQUAL.satisfied(value2, value1));
		
		value1 = "Luka";
		value2 = "Luka";
		
		assertTrue(GREATER_OR_EQUAL.satisfied(value1, value2));
		assertTrue(GREATER_OR_EQUAL.satisfied(value2, value1));
	}
	
	@Test
	public void testEquals() {
		String value1 = "Andrej";
		String value2 = "Andrej";
		
		assertTrue(EQUALS.satisfied(value1, value2));
		assertTrue(EQUALS.satisfied(value2, value1));
		
		value1 = "andrej";
		value2 = "andrej";
		
		assertTrue(EQUALS.satisfied(value1, value2));
		assertTrue(EQUALS.satisfied(value2, value1));
		
		value1 = "anDrej";
		value2 = "anDrej";
		
		assertTrue(EQUALS.satisfied(value1, value2));
		assertTrue(EQUALS.satisfied(value2, value1));
		
		value1 = "anDrej";
		value2 = "andrej";
		
		assertFalse(EQUALS.satisfied(value1, value2));
		assertFalse(EQUALS.satisfied(value2, value1));
	}
	
	@Test
	public void testNotEquals() {
		String value1 = "Andrej";
		String value2 = "andrej";
		
		assertTrue(NOT_EQUALS.satisfied(value1, value2));
		assertTrue(NOT_EQUALS.satisfied(value2, value1));
		
		value1 = "andrej";
		value2 = "anDrej";
		
		assertTrue(NOT_EQUALS.satisfied(value1, value2));
		assertTrue(NOT_EQUALS.satisfied(value2, value1));
		
		value1 = "anDrej";
		value2 = "anDreJ";
		
		assertTrue(NOT_EQUALS.satisfied(value1, value2));
		assertTrue(NOT_EQUALS.satisfied(value2, value1));
		
		value1 = "anDrej";
		value2 = "anDrej";
		
		assertFalse(NOT_EQUALS.satisfied(value1, value2));
		assertFalse(NOT_EQUALS.satisfied(value2, value1));
	}
	
	@Test
	public void testLike() {
		String value = "Osnove Programskog Jezika Java";
		String pattern = "Osn*";
		
		assertTrue(LIKE.satisfied(value, pattern));
		
		pattern = "*Jezika Java";
		assertTrue(LIKE.satisfied(value, pattern));
		
		pattern = "Osnov*Jezika Java";
		assertTrue(LIKE.satisfied(value, pattern));
		
		pattern = "Osnove Programskog Jezika Java";
		assertTrue(LIKE.satisfied(value, pattern));
		
		pattern = "osnove*java";
		assertFalse(LIKE.satisfied(value, pattern));
		
		pattern = "*java";
		assertFalse(LIKE.satisfied(value, pattern));
		
		pattern = "osnove*";
		assertFalse(LIKE.satisfied(value, pattern));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testLikeMultipleWildCards0() {
		String value = "Osnove Programskog Jezika Java";
		String pattern = "O*sn*";
		
		LIKE.satisfied(value, pattern);
	}
}
