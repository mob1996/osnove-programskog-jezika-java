package hr.fer.zemris.java.hw05.db;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

public class QueryParserTest {

	@Test 
	public void testWithNoQuery() {
		@SuppressWarnings("unused")
		QueryParser parser = new QueryParser("jmbag=\"0036486718\"");
	}
	
	@Test 
	public void testDirectQuery0() {
		QueryParser parser = new QueryParser("jmbag=\"0036486718\"");
		
		assertTrue(parser.isDirectQuery());
		assertEquals(1, parser.getQuery().size());
		assertEquals("0036486718", parser.getQueriedJMBAG());
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testDirectQuery1() {
		QueryParser parser = new QueryParser("				jmbag<\"0036486718\"");
		
		assertEquals(1, parser.getQuery().size());
		assertFalse(parser.isDirectQuery());
		parser.getQueriedJMBAG();
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testDirectQuery2() {
		QueryParser parser = new QueryParser("jmbag=\"0036486718\" and firstName<\"A\"");
		
		assertEquals(2, parser.getQuery().size());
		assertFalse(parser.isDirectQuery());
		parser.getQueriedJMBAG();
	}
	
	@Test (expected = QueryParserException.class)
	public void testWithNoAnd() {
		@SuppressWarnings("unused")
		QueryParser parser = new QueryParser("jmbag=\"0036486718\"  firstName<\"A\"");
	}
	
	@Test (expected = QueryParserException.class)
	public void testInvalid0() {
		@SuppressWarnings("unused")
		QueryParser parser = new QueryParser("jmbAg=\"0036486718\" and firstName<\"A\"");
	}
	
	@Test (expected = QueryParserException.class)
	public void testInvalid1() {
		@SuppressWarnings("unused")
		QueryParser parser = new QueryParser("jmbag=!!\"0036486718\" and firstName<\"A\"");
	}
	
	@Test (expected = QueryParserException.class)
	public void testInvalid2() {
		@SuppressWarnings("unused")
		QueryParser parser = new QueryParser("jmbg=\"0036486718\" and firstName<\"A\"");
	}
	
	@Test (expected = QueryParserException.class)
	public void testInvalid3() {
		@SuppressWarnings("unused")
		QueryParser parser = new QueryParser("jmbag=\"0036486718\" and first_Name<\"A\"");
	}
	
	@Test (expected = QueryParserException.class)
	public void testInvalid4() {
		@SuppressWarnings("unused")
		QueryParser parser = new QueryParser("jmbag LiKE \"0*\" and firstName<\"A\"");
	}
	
	 @Test
	 public void testComplexQuery() {
		 QueryParser parser = new QueryParser("jmbag<\"023\" and firstName LIKE \"M*j\""
		 		+ " and lastName <= \"B\" and jmbag=\"0036486718\"");
		 
		 List<ConditionalExpression> queries = parser.getQuery();
		 
		 assertFalse(parser.isDirectQuery());
		 assertEquals(4, queries.size());
		 
		 assertTrue(FieldValueGetters.JMBAG == queries.get(0).getFieldGetter());
		 assertTrue(ComparisonOperators.LESS == queries.get(0).getComparisonOperator());
		 assertEquals("023", queries.get(0).getStringLiteral());
		 
		 assertTrue(FieldValueGetters.FIRST_NAME == queries.get(1).getFieldGetter());
		 assertTrue(ComparisonOperators.LIKE == queries.get(1).getComparisonOperator());
		 assertEquals("M*j", queries.get(1).getStringLiteral());
		 
		 assertTrue(FieldValueGetters.LAST_NAME == queries.get(2).getFieldGetter());
		 assertTrue(ComparisonOperators.LESS_OR_EQUAL == queries.get(2).getComparisonOperator());
		 assertEquals("B", queries.get(2).getStringLiteral());
		 
		 assertTrue(FieldValueGetters.JMBAG == queries.get(3).getFieldGetter());
		 assertTrue(ComparisonOperators.EQUALS == queries.get(3).getComparisonOperator());
		 assertEquals("0036486718", queries.get(3).getStringLiteral());
	 }
	
}
