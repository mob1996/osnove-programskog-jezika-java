package hr.fer.zemris.java.hw05.db;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import static org.junit.Assert.*;
import static hr.fer.zemris.java.hw05.db.ComparisonOperators.*;
import static hr.fer.zemris.java.hw05.db.FieldValueGetters.*;

public class QueryFilterTest {

	private static final String RELATIVE_PATH = "src/main/resources/filterTest.txt";
	private static StudentDatabase database;
	static {
		List<String> lines = new LinkedList<>();
		try (BufferedReader reader = new BufferedReader(new FileReader(RELATIVE_PATH))) {
		    String line;
		    while ((line = reader.readLine()) != null) {
		      lines.add(line);
		    }
		} catch(FileNotFoundException exc) {
			System.err.println("Given file doesnt exist.");
		} catch(IOException exc) {
			System.err.println("IOException happened.");
		}
		
		database = new StudentDatabase(lines);
	}
	
	@Test
	public void test0() {
		List<StudentRecord> allRecord = database.filter(r -> true);
		
		QueryParser parser = new QueryParser("jmbag LIKE \"*\"");
		List<StudentRecord> filtered = database.filter(new QueryFilter(parser.getQuery()));
		
		assertEquals(10, allRecord.size());
		assertEquals(allRecord.size(), filtered.size());
		
		for(int i = 0, size = allRecord.size(); i < size; i++) {
			assertEquals(allRecord.get(i), filtered.get(i));
		}
	}
	
	@Test
	public void test1() {
		List<StudentRecord> satisfiedRecords = 
				database.filter(r -> EQUALS.satisfied(JMBAG.get(r), "0000000001"));
		
		QueryParser parser = new QueryParser("jmbag=\"0000000001\"");
		List<StudentRecord> filtered = database.filter(new QueryFilter(parser.getQuery()));
		
		assertEquals(1, satisfiedRecords.size());
		assertEquals(satisfiedRecords.size(), filtered.size());
		
		for(int i = 0, size = satisfiedRecords.size(); i < size; i++) {
			assertEquals(satisfiedRecords.get(i), filtered.get(i));
		}
	}
	
	@Test
	public void test2() {
		List<StudentRecord> satisfiedRecords = 
				database.filter(r -> LIKE.satisfied(LAST_NAME.get(r), "Bo*"));
		
		QueryParser parser = new QueryParser("lastName LIKE \"Bo*\"");
		List<StudentRecord> filtered = database.filter(new QueryFilter(parser.getQuery()));
		
		assertEquals(2, satisfiedRecords.size());
		assertEquals(satisfiedRecords.size(), filtered.size());
		
		for(int i = 0, size = satisfiedRecords.size(); i < size; i++) {
			assertEquals(satisfiedRecords.get(i), filtered.get(i));
		}
	}
	
	@Test
	public void test3() {
		List<StudentRecord> satisfiedRecords = 
				database.filter(r -> GREATER.satisfied(LAST_NAME.get(r), "Az")
						&& LIKE.satisfied(LAST_NAME.get(r), "*ić"));
		QueryParser parser = new QueryParser("lastName>\"Az\" and lastName LIKE \"*ić\"");
		List<StudentRecord> filtered = database.filter(new QueryFilter(parser.getQuery()));
		
		assertEquals(5, satisfiedRecords.size());
		assertEquals(satisfiedRecords.size(), filtered.size());
		
		for(int i = 0, size = satisfiedRecords.size(); i < size; i++) {
			assertEquals(satisfiedRecords.get(i), filtered.get(i));
		}
	}
	
	@Test
	public void test4() {
		List<StudentRecord> satisfiedRecords = 
				database.filter(r -> NOT_EQUALS.satisfied(FIRST_NAME.get(r), "Marin")
						&& GREATER.satisfied(LAST_NAME.get(r), "B"));
		QueryParser parser = new QueryParser("firstName!=\"Marin\" and lastName>\"B\"");
		List<StudentRecord> filtered = database.filter(new QueryFilter(parser.getQuery()));
		
		assertEquals(8, satisfiedRecords.size());
		assertEquals(satisfiedRecords.size(), filtered.size());
		
		for(int i = 0, size = satisfiedRecords.size(); i < size; i++) {
			assertEquals(satisfiedRecords.get(i), filtered.get(i));
		}
	}
		
}
