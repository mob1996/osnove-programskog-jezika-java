package hr.fer.zemris.java.hw05.db;

import static org.junit.Assert.*;
import static hr.fer.zemris.java.hw05.db.FieldValueGetters.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Test;

public class FieldValueGettersTest {

	@Test
	public void test(){
		String[] jmbags = {"0036486718",
				"0000000001",
				"0000000002",
				"0000000003",
				"0000000004"
		};
		
		String[] firstNames = {"Luka",
				"Marko",
				"Ana",
				"Lovro",
				"Nikola"
		};
		
		String[] lastNames = {"Mijić",
				"Janković",
				"Vlahović",
				"Šimunović",
				"Jurić"
		};
		
		List<StudentRecord> students = new ArrayList<>();
		
		for(int i = 0; i < 5; i++) {
			int grade = new Random().nextInt(5)+1;
			students.add(new StudentRecord(jmbags[i], lastNames[i], firstNames[i], grade));
		}
		
		for(int i = 0; i < 5; i++) {
			StudentRecord record = students.get(i);
			assertEquals(record.getJmbag(), JMBAG.get(record));
			assertEquals(record.getFirstName(), FIRST_NAME.get(record));
			assertEquals(record.getLastName(), LAST_NAME.get(record));
		}
	}
}
