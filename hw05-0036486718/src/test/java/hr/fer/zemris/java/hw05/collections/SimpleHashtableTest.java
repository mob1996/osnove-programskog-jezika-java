package hr.fer.zemris.java.hw05.collections;

import static org.junit.Assert.*;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

import org.junit.Test;

import hr.fer.zemris.java.hw05.collections.SimpleHashtable.TableEntry;

public class SimpleHashtableTest {

	public SimpleHashtable<String, Integer> initHash(int capacity){
		SimpleHashtable<String, Integer> table = new SimpleHashtable<>(capacity);
		
		for(int i = 0; i < 100; i++) {
			table.put("test" + i, i);
		}
		
		return table;
	}
	
	@SuppressWarnings("unused")
	@Test (expected = IllegalArgumentException.class)
	public void testConstructor0() {
		SimpleHashtable<Object, Object> table =  new SimpleHashtable<>(0);
	}
	
	@SuppressWarnings("unused")
	@Test (expected = IllegalArgumentException.class)
	public void testConstructor1() {
		SimpleHashtable<Object, Object> table =  new SimpleHashtable<>(-5);
	}
	
	@Test
	public void testIsEmpty() {
		SimpleHashtable<String, Integer> table = initHash(50);
		
		assertFalse(table.isEmpty());
		
		for(int i = 0; i < 100; i++) {
			table.remove("test"+i);
			if(i != 99) {
				assertFalse(table.isEmpty());
			} else {
				assertTrue(table.isEmpty());
			}
		}	
	}
	
	@Test
	public void testSize() {
		SimpleHashtable<String, Integer> table = initHash(50);
		
		assertEquals(100, table.size());
		
		for(int i = 0; i < 100; i++) {
			table.remove("test"+i);
			assertEquals(100 - i - 1, table.size());
		}	
	}
	
	@Test (expected = NullPointerException.class)
	public void testInvalidPut() {
		SimpleHashtable<String, Integer> table = new SimpleHashtable<>();
		
		table.put(null, 2);
	}
	@Test
	public void testContainsKeyAndPut() {
		SimpleHashtable<String, Integer> table = initHash(50);

		assertEquals(100, table.size());
		
		
		for(int i = 0; i < 200; i++) {
			boolean expected = i < 100 ? true : false;
			assertEquals(expected, table.containsKey("test"+i));
		}
		
		assertFalse(table.containsKey(null));
		assertFalse(table.containsKey(5));
		assertFalse(table.containsKey(2.2f));
		assertFalse(table.containsKey(new Object()));
	}
	
	@Test
	public void testGet() {
		SimpleHashtable<String, Integer> table = initHash(50);
		
		for(int i = 0; i < 200; i++) {
			if(i < 100) {
				assertEquals(Integer.valueOf(i), table.get("test"+i));
			} else {
				assertNull(table.get("test"+i));
			}
		}
		
		assertNull(null);
		assertNull(table.get(-2));
		assertNull(table.get(-2.2f));
		assertNull(table.get("f"));
		assertNull(table.get('a'));
	}
	
	@Test
	public void testContainsValue() {
		SimpleHashtable<String, Integer> table = initHash(50);
		
		for(int i = 0; i < 200; i++) {
			if(i < 100) {
				assertTrue(table.containsValue(Integer.valueOf(i)));
			} else {
				assertFalse(table.containsValue(Integer.valueOf(i)));
			}
		}
		
		assertFalse(table.containsValue(null));
		table.put("null", null);
		assertTrue(table.containsValue(null));
		
		assertFalse(table.containsValue("f"));
		assertFalse(table.containsValue(2.2));
		assertFalse(table.containsValue('k'));
		
	}
	
	@Test
	public void testClear() {
		SimpleHashtable<String, Integer> table = initHash(50);
		assertFalse(table.isEmpty());
		table.clear();
		assertTrue(table.isEmpty());
	}
	
	@Test
	public void testIterator0() {
		SimpleHashtable<String, Integer> table = initHash(1);
		
		int i = 0;
		for(TableEntry<String, Integer> entry:table) {
			if(entry == null) fail();
			i++;
		}
		assertEquals(100, i);
	}
	
	@Test
	public void testIterator1() {
		SimpleHashtable<String, Integer> table = initHash(1);
		
		Iterator<TableEntry<String, Integer>> it = table.iterator();
		
		int i = 0;
		while(it.hasNext()) {
			if(it.next() == null) fail();
			
			i++;
		}
		
		assertEquals(100, i);
	}
	
	@Test (expected = NoSuchElementException.class)
	public void testIterator2() {
		SimpleHashtable<String, Integer> table = initHash(16);
		
		Iterator<TableEntry<String, Integer>> it = table.iterator();
		
		int i = 0;
		while(it.hasNext()) {
			if(it.next() == null) fail();
			
			i++;
		}
		
		assertEquals(100, i);
		it.next();
	}
	
	@Test (expected = ConcurrentModificationException.class)
	public void testIterator3() {
		SimpleHashtable<String, Integer> table = initHash(1);
		
		Iterator<TableEntry<String, Integer>> it = table.iterator();
		
		while(it.hasNext()) {
			if(it.next() == null) fail();
			table.put("error", 3);
		}
	}
	
	@Test (expected = IllegalStateException.class)
	public void testIterator4() {
		SimpleHashtable<String, Integer> table = initHash(1);
		
		Iterator<TableEntry<String, Integer>> it = table.iterator();
		it.remove();
		
	}
	
	@Test (expected = IllegalStateException.class)
	public void testIterator5() {
		SimpleHashtable<String, Integer> table = initHash(1);
		
		Iterator<TableEntry<String, Integer>> it = table.iterator();
		
		it.next();
		it.next();
		it.next();
		it.next();
		it.next();
		
		it.remove();
		it.remove();
		
	}
	
	@Test
	public void testIterator6() {
		SimpleHashtable<String, Integer> table = initHash(1);
		
		Iterator<TableEntry<String, Integer>> it = table.iterator();
		int i = 0;
		while(it.hasNext()) {
			if(it.next() == null) fail();
			
			it.remove();
			i++;
			if(i < 100) {
				assertFalse(table.isEmpty());
			}
		}
		
		assertTrue(table.isEmpty());
	}
	
	
}
