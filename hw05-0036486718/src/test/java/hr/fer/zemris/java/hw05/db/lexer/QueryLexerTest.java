package hr.fer.zemris.java.hw05.db.lexer;

import static org.junit.Assert.*;

import org.junit.Test;

public class QueryLexerTest {

	@Test
	public void testEOF() {
		QueryLexer lexer = new QueryLexer("");
		
		lexer.nextToken();
		assertNull(lexer.getToken().getValue());
		assertEquals(QueryTokenType.EOF, lexer.getToken().getType());
	}
	
	@Test (expected = QueryLexerException.class)
	public void testNextAfterEOF() {
		QueryLexer lexer = new QueryLexer("");
		
		lexer.nextToken();
		lexer.nextToken();
	}
	
	@Test
	public void testValidStrings() {
		QueryLexer lexer = new QueryLexer("\"osn*\" \"progr  	amskog\" \"*jezika\" \"java\"");
		
		QueryToken token = lexer.nextToken();
		assertEquals(QueryTokenType.STRING_LITERAL, token.getType());
		assertEquals("\"osn*\"", token.getValue());
		
		token = lexer.nextToken();
		assertEquals(QueryTokenType.STRING_LITERAL, token.getType());
		assertEquals("\"progr  	amskog\"", token.getValue());
		
		token = lexer.nextToken();
		assertEquals(QueryTokenType.STRING_LITERAL, token.getType());
		assertEquals("\"*jezika\"", token.getValue());
		
		token = lexer.nextToken();
		assertEquals(QueryTokenType.STRING_LITERAL, token.getType());
		assertEquals("\"java\"", token.getValue());
		
		lexer.nextToken();
		assertNull(lexer.getToken().getValue());
		assertEquals(QueryTokenType.EOF, lexer.getToken().getType());
	}
	
	@Test (expected = QueryLexerException.class)
	public void testInvalidString0() {
		QueryLexer lexer = new QueryLexer("\"osn*\" \"programskog");
		
		QueryToken token = lexer.nextToken();
		assertEquals(QueryTokenType.STRING_LITERAL, token.getType());
		assertEquals("\"osn*\"", token.getValue());
		
		token = lexer.nextToken();
	}
	
	@Test (expected = QueryLexerException.class)
	public void testInvalidString1() {
		QueryLexer lexer = new QueryLexer("\"osn*\" \"programs?kog\"");
		
		QueryToken token = lexer.nextToken();
		assertEquals(QueryTokenType.STRING_LITERAL, token.getType());
		assertEquals("\"osn*\"", token.getValue());
		
		token = lexer.nextToken();
	}
	
	@Test (expected = QueryLexerException.class)
	public void testInvalidString2() {
		QueryLexer lexer = new QueryLexer("\"osn*\" \"programs_kog\"");
		
		QueryToken token = lexer.nextToken();
		assertEquals(QueryTokenType.STRING_LITERAL, token.getType());
		assertEquals("\"osn*\"", token.getValue());
		
		token = lexer.nextToken();
	}
	
	@Test
	public void testOperators() {
		QueryLexer lexer = new QueryLexer("< > << >> <= => !!>!=");
		
		QueryToken token = lexer.nextToken();
		assertEquals(QueryTokenType.OPERATOR, token.getType());
		assertEquals("<", token.getValue());
		
		token = lexer.nextToken();
		assertEquals(QueryTokenType.OPERATOR, token.getType());
		assertEquals(">", token.getValue());
		
		token = lexer.nextToken();
		assertEquals(QueryTokenType.OPERATOR, token.getType());
		assertEquals("<<", token.getValue());
		
		token = lexer.nextToken();
		assertEquals(QueryTokenType.OPERATOR, token.getType());
		assertEquals(">>", token.getValue());
		
		token = lexer.nextToken();
		assertEquals(QueryTokenType.OPERATOR, token.getType());
		assertEquals("<=", token.getValue());
		
		token = lexer.nextToken();
		assertEquals(QueryTokenType.OPERATOR, token.getType());
		assertEquals("=>", token.getValue());
		
		token = lexer.nextToken();
		assertEquals(QueryTokenType.OPERATOR, token.getType());
		assertEquals("!!>!=", token.getValue());
		
		lexer.nextToken();
		assertNull(lexer.getToken().getValue());
		assertEquals(QueryTokenType.EOF, lexer.getToken().getType());
	}
	
	@Test
	public void testValidFields() {
		QueryLexer lexer = new QueryLexer("jmbag firstName lastName grade name ");
		
		QueryToken token = lexer.nextToken();
		assertEquals(QueryTokenType.WORD, token.getType());
		assertEquals("jmbag", token.getValue());
		
		token = lexer.nextToken();
		assertEquals(QueryTokenType.WORD, token.getType());
		assertEquals("firstName", token.getValue());
		
		token = lexer.nextToken();
		assertEquals(QueryTokenType.WORD, token.getType());
		assertEquals("lastName", token.getValue());
		
		token = lexer.nextToken();
		assertEquals(QueryTokenType.WORD, token.getType());
		assertEquals("grade", token.getValue());
		
		token = lexer.nextToken();
		assertEquals(QueryTokenType.WORD, token.getType());
		assertEquals("name", token.getValue());
		
		lexer.nextToken();
		assertNull(lexer.getToken().getValue());
		assertEquals(QueryTokenType.EOF, lexer.getToken().getType());
	}
	
	@Test (expected = QueryLexerException.class)
	public void testInvalidFields0() {
		QueryLexer lexer = new QueryLexer("jmbag firs*tName lastName grade name ");
		
		QueryToken token = lexer.nextToken();
		assertEquals(QueryTokenType.WORD, token.getType());
		assertEquals("jmbag", token.getValue());
		
		token = lexer.nextToken();
		assertEquals(QueryTokenType.WORD, token.getType());
		assertEquals("firs", token.getValue());
		
		lexer.nextToken();
	}
	
	@Test (expected = QueryLexerException.class)
	public void testInvalidFields1() {
		QueryLexer lexer = new QueryLexer("jmbag firs_tName lastName grade name ");
		
		QueryToken token = lexer.nextToken();
		assertEquals(QueryTokenType.WORD, token.getType());
		assertEquals("jmbag", token.getValue());
		
		token = lexer.nextToken();
		assertEquals(QueryTokenType.WORD, token.getType());
		assertEquals("firs", token.getValue());
		
		lexer.nextToken();
	}
	
	@Test (expected = QueryLexerException.class)
	public void testInvalidFields2() {
		QueryLexer lexer = new QueryLexer("jmbag firstName? lastName grade name ");
		
		QueryToken token = lexer.nextToken();
		assertEquals(QueryTokenType.WORD, token.getType());
		assertEquals("jmbag", token.getValue());
		
		token = lexer.nextToken();
		assertEquals(QueryTokenType.WORD, token.getType());
		assertEquals("firstName", token.getValue());
		
		lexer.nextToken();
	}
	
	@Test (expected = QueryLexerException.class)
	public void testInvalidFields3() {
		QueryLexer lexer = new QueryLexer("jmbag ?firstName lastName grade name ");
		
		QueryToken token = lexer.nextToken();
		assertEquals(QueryTokenType.WORD, token.getType());
		assertEquals("jmbag", token.getValue());
		
		token = lexer.nextToken();
	}
	
	@Test
	public void testMultiple() {
		QueryLexer lexer = new QueryLexer("jmbag=\"000\" and firstName<=\"L*ja\"");
		
		QueryToken token = lexer.nextToken();
		assertEquals(QueryTokenType.WORD, token.getType());
		assertEquals("jmbag", token.getValue());
		
		token = lexer.nextToken();
		assertEquals(QueryTokenType.OPERATOR, token.getType());
		assertEquals("=", token.getValue());
		
		token = lexer.nextToken();
		assertEquals(QueryTokenType.STRING_LITERAL, token.getType());
		assertEquals("\"000\"", token.getValue());
		
		token = lexer.nextToken();
		assertEquals(QueryTokenType.WORD, token.getType());
		assertEquals("and", token.getValue());
		
		
		token = lexer.nextToken();
		assertEquals(QueryTokenType.WORD, token.getType());
		assertEquals("firstName", token.getValue());
		
		token = lexer.nextToken();
		assertEquals(QueryTokenType.OPERATOR, token.getType());
		assertEquals("<=", token.getValue());
		
		token = lexer.nextToken();
		assertEquals(QueryTokenType.STRING_LITERAL, token.getType());
		assertEquals("\"L*ja\"", token.getValue());
		
		lexer.nextToken();
		assertNull(lexer.getToken().getValue());
		assertEquals(QueryTokenType.EOF, lexer.getToken().getType());
	}
}
