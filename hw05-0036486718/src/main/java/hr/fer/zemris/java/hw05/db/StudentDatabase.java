package hr.fer.zemris.java.hw05.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import hr.fer.zemris.java.hw05.collections.SimpleHashtable;

/**
 * Class is used for storage of student
 * records. 
 * @author Luka Mijić
 *
 */
public class StudentDatabase {

	/**
	 * List stores all student records.
	 */
	private List<StudentRecord> recordList;
	
	/**
	 * Makes connection between jmbag and record allowing
	 * for fast retrieval of records using jmbags.
	 */
	private SimpleHashtable<String, StudentRecord> jmbagIndex;
	
	/**
	 * Creates database from list of string record rows
	 * @param recordRows list of string records
	 */
	public StudentDatabase(List<String> recordRows) {
		Objects.requireNonNull(recordRows, "Records can't be null.");
		
		this.recordList = new ArrayList<>();
		this.jmbagIndex = new SimpleHashtable<>();
		
		for(String row : recordRows) {
			StudentRecord record = parseToRecord(row);
			
			recordList.add(record);
			jmbagIndex.put(record.getJmbag(), record);
		}
	}
	
	/**
	 * Method is used to parse row into a student record
	 * @param row that is parsed
	 * @return new StudentRecord
	 * @throws IllegalArgumentException if number of parameters
	 * 					invalid or if grade can't be parsed into int
	 */
	private StudentRecord parseToRecord(String row) {
		String[] columns = row.trim().split("\t");
		
		if(columns.length != 4) {
			throw new IllegalArgumentException("Can't parse student");
		}
		
		int grade = 0;
		try {
			grade = Integer.valueOf(columns[3]);
		} catch (NumberFormatException exc) {
			throw new IllegalArgumentException("Can't parse grade. Was " + columns[3]);
		}
	
		return new StudentRecord(columns[0], columns[1], columns[2], grade);
	}
	
	/**
	 * Method is used to get record of student with
	 * given jmbag. Complexity of getting record is O(1)
	 * @param jmbag of student
	 * @return StudentRecord with given jmbag or null if
	 * 			that record doesn't exist.
	 */
	public StudentRecord forJmbag(String jmbag) {
		return jmbagIndex.get(jmbag);
	}
	
	/**
	 * Method is used to filter records.
	 * @param filter that is used to determine which
	 * 			record is acceptable and which one is not
	 * @return list of records that pass filter
	 */
	public List<StudentRecord> filter(IFilter filter){
		List<StudentRecord> acceptableRecords = new ArrayList<>();
		
		for(StudentRecord record:recordList) {
			if(filter.accepts(record)) {
				acceptableRecords.add(record);
			}
		}
		return acceptableRecords;
	}
}
