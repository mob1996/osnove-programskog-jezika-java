package hr.fer.zemris.java.hw05.db;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Class is used as demonstration
 * 
 * @author Luka Mijić
 *
 */
public class StudentDB {

	/**
	 * Relative path to database
	 */
	private static final String RELATIVE_PATH = "src/main/resources/database.txt";

	/**
	 * When exit is read exit program
	 */
	private static final String STOP_SCAN = "exit";
	
	/**
	 * Query keyword
	 */
	
	private static final String QUERY = "query";
	/**
	 * Number of spaces
	 */
	private static final int PADDING = 1;
	
	/**
	 * Database of students
	 */
	private static StudentDatabase db;
	
	public static void main(String[] args) {
		try {
			List<String> lines = Files.readAllLines(Paths.get(RELATIVE_PATH), StandardCharsets.UTF_8);
			db = new StudentDatabase(lines);
			interaction();
		}  catch(FileNotFoundException exc) {
			System.err.println("Given file doesnt exist.");
		} catch(IOException exc) {
			System.err.println("IOException happened.");
		}
	}
	
	/**
	 * This method provides interaction between user and program.
	 */
	private static void interaction() {
		Scanner sc = new Scanner(System.in);
		
		String query;
		while(true) {
			try{
				query = sc.nextLine();
				query = query.trim();
				if(query.toLowerCase().equals(STOP_SCAN)) {
					System.out.println("Goodbye");
					break;
				} else if(!query.startsWith(QUERY)) {
					throw new IllegalArgumentException("Command must start with 'query'.");
				}
				
				query = query.substring(QUERY.length());
				QueryParser parser = new QueryParser(query);
				
				if(parser.isDirectQuery()) {
					List<StudentRecord> records = new ArrayList<>();
					StudentRecord record = db.forJmbag(parser.getQueriedJMBAG()); 
					if(record != null) {
						records.add(record);
					}
					printQuery(records, true);
				} else {
					QueryFilter filter = new QueryFilter(parser.getQuery());
					printQuery(db.filter(filter), false);
				}
				
			} catch(RuntimeException exc) {
				System.err.println(exc.getMessage());
			}
		}
		
		sc.close();
	}
	
	/**
	 * This method formats and prints result of query
	 * @param records that need to be printed
	 * @param isIndex tells if records were retrieved using fast index or not
	 */
	private static void printQuery(List<StudentRecord> records, boolean isIndex) {
		int size = records.size();
	
		if(isIndex) {
			System.out.print("Using index for record retrieval.\n");
		}
		
		String selected = "Records selected: " + size + "\n";
		if(size == 0) {
			System.out.print(selected);
			return; 
		}
		
		int neededJmbagLength = Integer.MIN_VALUE;
		int neededFirstNameLength = Integer.MIN_VALUE;
		int neededLastNameLength = Integer.MIN_VALUE;
		int neededGradeLength = 1;
		
		for(StudentRecord r : records) {
			neededJmbagLength = Integer.max(neededJmbagLength, FieldValueGetters.JMBAG.get(r).length());
			neededFirstNameLength = Integer.max(neededFirstNameLength, FieldValueGetters.FIRST_NAME.get(r).length());
			neededLastNameLength = Integer.max(neededLastNameLength, FieldValueGetters.LAST_NAME.get(r).length());
		}
			
		StringBuilder builder = new StringBuilder();
		
		String paddingFormat = "%" + PADDING +"s";
		String jmbagFormat = "%-" + neededJmbagLength +"s";
		String firstNameFormat = "%-"+ neededFirstNameLength + "s";
		String lastNameFormat = "%-" + neededLastNameLength + "s";
		String gradeFormat = "%-" + neededGradeLength + "s";
		
		
		for(StudentRecord r:records) {
			builder.append("|")
				.append(String.format(paddingFormat, " "))
				.append(String.format(jmbagFormat, r.getJmbag()))
				.append(String.format(paddingFormat, " "))
				.append("|").append(String.format(paddingFormat, " "))
				.append(String.format(lastNameFormat, r.getLastName()))
				.append(String.format(paddingFormat, " "))
				.append("|").append(String.format(paddingFormat, " "))
				.append(String.format(firstNameFormat, r.getFirstName()))
				.append(String.format(paddingFormat, " "))
				.append("|").append(String.format(paddingFormat, " "))
				.append(String.format(gradeFormat, r.getFinalGrade()))
				.append(String.format(paddingFormat, " "))
				.append("|\n");
		}
		
		StringBuilder header = new StringBuilder();
		int headerSize = PADDING * 8 + 4 + 
				neededFirstNameLength + neededGradeLength + neededJmbagLength + neededLastNameLength;
		ArrayList<Integer> plusSpots = new ArrayList<>();
		plusSpots.add(0);
		plusSpots.add(2 * PADDING + neededJmbagLength + 1);
		plusSpots.add(4 * PADDING + neededJmbagLength + neededLastNameLength + 2);
		plusSpots.add(6 * PADDING + neededJmbagLength + neededLastNameLength + neededFirstNameLength + 3);
		plusSpots.add(8 * PADDING + neededJmbagLength + neededLastNameLength + neededFirstNameLength + neededGradeLength + 4);
		
		for(int i = 0; i < headerSize; i++) {
			if(plusSpots.contains(i)) {
				header.append("+");
			} else {
				header.append("=");
			}
		}
		
		
		header.append("+\n");
		builder.append(header.toString());
		header.append(builder.toString());
		header.append(selected);
		System.out.print(header.toString());
	}


}
