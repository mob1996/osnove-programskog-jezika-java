package hr.fer.zemris.java.hw05.collections;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import static java.lang.Math.abs;


/**
 * 
 * Class represents data structure that allows storage 
 * of pairs (key, value). 
 * @author Luka Mijić
 *
 * @param <K> represents type of key
 * @param <V> represents type of value
 */
public class SimpleHashtable <K, V> implements Iterable<SimpleHashtable.TableEntry<K, V>>{
	
	/**
	 * Number of legal capacities.
	 * Number 32 is chosen because of 2^31 = Integer.MAX_VALUE
	 */
	private static int NUMBER_OF_LEGAL_CAPACITIES = 31;
	
	/**
	 * Legal capacites are cached to avoid calculating powers every
	 * time SimpleHashable is created using constructor that takes capacity argument.
	 */
	private static int[] legal_capacities;
	static {
		legal_capacities = new int[NUMBER_OF_LEGAL_CAPACITIES];
		for(int i = 0; i < NUMBER_OF_LEGAL_CAPACITIES; i++) {
			legal_capacities[i] = 1 << i;
		}
	}
	
	/**
	 * Default table size
	 */
	private static final int DEFAULT_CAPACITY = 16;
	
	/**
	 * If ratio of size and capacity passes
	 * this mark, overall capacity must be doubled
	 */
	private static final double THRESHOLD = 0.75;
	
	/**
	 * EMPTY represents no entries in
	 * SimpleHashable
	 */
	private static final int EMPTY = 0;
	
	/**
	 * Maximum capacity of SimpleHashable (2^30)
	 */
	private static final int MAX_CAPACITY = 1 << 30;
	
	
	/**
	 * Class represents one entry in table.
	 * @author Luka Mijić
	 *
	 * @param <K> represents type of key
	 * @param <V> represents type of value
	 */
	public static class TableEntry<K, V>{
		
		/**
		 * Key property of TableEntry
		 */
		private K key;
		
		/**
		 * Value property of TableEntry
		 */
		private V value;
		
		/**
		 * Reference to next TableEntry
		 */
		private TableEntry<K, V> next;
		
		/**
		 * Constructor that is used for creating
		 * instance of TableEntry class
		 * @param key of TableEntry
		 * @param value of TableEntry
		 */
		public TableEntry(K key, V value, TableEntry<K, V> next) {
			this.key = Objects.requireNonNull(key, "Key Value must not be null");
			this.value = value;
			this.next = next;
		}

		/**
		 * @return value property
		 */
		public V getValue() {
			return value;
		}

		/**
		 * @param value is new value of value property
		 */
		public void setValue(V value) {
			this.value = value;
		}

		/**
		 * @return key property
		 */
		public K getKey() {
			return key;
		}


		/**
		 * Method calculates hash code of Table Entry
		 * @return value of hash code
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((key == null) ? 0 : key.hashCode());
			return result;
		}
		

		/**
		 * Method is used to check if two objects are
		 * equal.
		 * @return true if they are equal, otherwise return false.
		 */
		@SuppressWarnings("rawtypes")
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			TableEntry other = (TableEntry) obj;
			if (key == null) {
				if (other.key != null)
					return false;
			} else if (!key.equals(other.key))
				return false;
			return true;
		}
		
		/**
		 * @return String representation of class
		 */
		@Override
		public String toString() {
			return key + "=" + value;
		}
		
	}
	
	/**
	 * Size of table
	 */
	private int capacity;
	
	/**
	 * Number of storted entries
	 */
	private int size;
	
	/**
	 * Number of modifications 
	 */
	private int modificationCount;
	
	/**
	 * Array of TableEntries
	 */
	private TableEntry<K, V>[] table;
	
	/**
	 * Default constructor of SimpleHashable.
	 * Sets table capacity to 16
	 */
	public SimpleHashtable(){
		this(DEFAULT_CAPACITY);
	}
	
	/**
	 * Constructor is used to create SimpleHashable
	 * with arbitrary capacity.
	 * Property capacity is set to the "2 to the power of n"
	 * that is closest to it. 
	 * @param capacity
	 */
	@SuppressWarnings("unchecked")
	public SimpleHashtable(int capacity) {
		if(capacity < 1) throw new IllegalArgumentException("Capacity must be 1 or higher.");
		this.capacity = calculateLegalCapacity(capacity);
		this.table = new TableEntry[this.capacity];
		this.size = EMPTY;
		this.modificationCount = 0;
	}
	
	/**
	 * Method is used to calculate 
	 * legal capacity value from given capacity
	 * @param capacity is parameter from which legal
	 * 		capacity is derived
	 * @return appropriate legal capacity
	 */
	private int calculateLegalCapacity(int capacity) {
		int i;
		for(i = 1; capacity > legal_capacities[i]; i++) {
			if(i == (NUMBER_OF_LEGAL_CAPACITIES - 1))
				return legal_capacities[i];
		}
	   
		boolean term = abs(capacity - legal_capacities[i-1]) < abs(capacity - legal_capacities[i]);
		return term ? legal_capacities[i-1] : legal_capacities[i];
	}
	
	/**
	 * Method is used to check if any entries 
	 * were added to SimpleHashable
	 * @return true if there are no entries
	 * 		otherwise return false
	 */
	public boolean isEmpty() {
		return this.size == EMPTY;
	}
	
	/**
	 * Method is used to return numbers of 
	 * entries in table
	 * @return number of entries
	 */
	public int size() {
		return this.size;
	}
	
	/**
	 * Method is used to add new entry to the
	 * hashable map. If given key is already used
	 * method overruns previous value.
	 * @param key of new entry
	 * @param value of new entry
	 */
	public void put(K key, V value) {
		Objects.requireNonNull(key, "Given Key must not be null");
		int slot = abs(key.hashCode()) % this.capacity;
		if(table[slot] == null) {
			table[slot] = new TableEntry<>(key, value, null);
			this.modificationCount++;
			if(++this.size/(double) this.capacity >= THRESHOLD) {
				resize();
			}
			return;
		}
		
		TableEntry<K, V> traversal = table[slot];
		
		while(traversal != null) {
			if(traversal.getKey().equals(key)) {
				traversal.setValue(value);
				return;
			} else if(traversal.next == null) {
				this.modificationCount++;
				traversal.next = new TableEntry<>(key, value, null);
				this.size++;
				return;
			}	
			traversal = traversal.next;
		}
		
		
	}
	
	/**
	 * Method is used to get value associated to 
	 * given key.
	 * @param key of value method looks for
	 * @return value if given key exists,
	 *            null if key doesn't exist
	 *            (return value can also be null)
	 */
	public V get(Object key) {
		if(key == null) return null;
		
		int slot = abs(key.hashCode()) % this.capacity;
		TableEntry<K, V> traversal = table[slot];
		
		while(traversal != null) {
			if(key.equals(traversal.getKey()))
				return traversal.getValue();
			
			traversal = traversal.next;
		}
		
		return null;
	}
	
	/**
	 * Method is used to remove entry from SimpleHashable
	 * @param key of entry method tries to remove
	 */
	public void remove(Object key) {
		if(key == null) return;
		
		int slot = abs(key.hashCode()) % capacity;
		TableEntry<K, V> traversal = table[slot];
		
		if(traversal.getKey().equals(key)) {
			table[slot] = table[slot].next;
			
			this.size--;
			this.modificationCount++;
			return;
		}
		
		while(traversal.next != null) {
			if(traversal.next.getKey().equals(key)) {
				traversal.next = traversal.next.next;
				size--;
				this.modificationCount++;
				return;
			}
			
			traversal = traversal.next;
		}
	}
	
	/**
	 * Method is used to check if SimpleHashable
	 * has entry with given key.
	 * @param key that method looks for
	 * @return true if SimpleHashable has entry
	 * 			with given key. Otherwise return false.
	 */
	public boolean containsKey(Object key) {
		if(key == null) return false;
		
		int slot = abs(key.hashCode()) % capacity;
		TableEntry<K, V> traversal = table[slot];
		
		while(traversal != null) {
			if(traversal.getKey().equals(key)) {
				return true;
			}
			traversal = traversal.next;
		}
		return false;
	}
	
	/**
	 * Method is used to check if SimpleHashable contains 
	 * given value.
	 * @param value that method searches for
	 * @return true if SimpleHashable contains atleast 
	 * 			one given value, otherwise return false
	 */
	public boolean containsValue(Object value) {
		for(int i = 0; i < this.capacity; i++) {
			TableEntry<K, V> traversal = table[i];
			
			while(traversal != null) {
				
				if(traversal.getValue() == null) {
					if(value == null) return true;
				} else if(value != null){
					if(traversal.getValue().equals(value)) return true;
				}
				
				traversal = traversal.next;
			}
		}
		
		return false;
	}
	
	/**
	 * Method is used to clear SimpleHashable from
	 * all elements
	 */
	public void clear() {
		size = 0;
		for(int i = 0; i < this.capacity; i++) {
			table[i] = null;
		}
		this.modificationCount++;
	}
	
	/**
	 * Method is used to resize curent SimpleHashable
	 * to the size that is two times higher than current one. 
	 */
	private void resize() {
		if(this.capacity == MAX_CAPACITY) {
			return;
		}
		int size = EMPTY;
		int oldCapacity = this.capacity;
		int newCapacity = this.capacity << 1;
		TableEntry<K, V>[] oldTable = table;
		@SuppressWarnings("unchecked")
		TableEntry<K, V>[] newTable = new TableEntry[newCapacity]; // x << 1 == x * 2 but faster
		
		for(int i = 0; i < oldCapacity; i++) {
			TableEntry<K, V> traversal = oldTable[i];
			
			while(traversal != null) {
				TableEntry<K, V> current = traversal;
				traversal = traversal.next;
				
				int slot = abs(current.getKey().hashCode()) % newCapacity; // Using put() makes this algorith less effective.                                                 // O(1) instead of O(n). This data structure doesn't guarantee 
				current.next = newTable[slot];                        // any order of data inside of it. So it's okay.         
				newTable[slot] = current;                             // Put() has to place TableEntry at the end of the list because it has to check
				size++;                                               // if list already contains key of given TableEntry. In this method duplicate key
				                                                      // can't happen.
			}
		}
		clear();
		
		this.capacity = newCapacity;
		this.size = size;
		table = newTable;
	}
	
	
	/**
	 * @return String representation on SimpleHashable
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for(int i = 0; i < this.capacity; i++) {
			TableEntry<K, V> traversal = table[i];
			while(traversal != null) {
				sb.append(traversal);
				if(i != (this.capacity-1) || traversal.next != null) {
					sb.append(", ");
				}
				traversal = traversal.next;
			}
		}
		
		return sb.append("]").toString();
	}

	/**
	 * Method creates new Iterator for hash table
	 */
	@Override
	public Iterator<TableEntry<K, V>> iterator() {
		return new IteratorImpl(this.modificationCount);
	}
	
	private class IteratorImpl implements Iterator<SimpleHashtable.TableEntry<K, V>>{

		/**
		 * Value of modificationCount inside of iterator implementation
		 */
		private int modificationCount;
		
		/**
		 * Current table slot
		 */
		private int currentSlot;
		
		/**
		 * Variable is used to signalize
		 * that .remove() was called
		 */
		private boolean usedRemove;
		
		/**
		 * Current TableEntry
		 */
		private TableEntry<K, V> current;
		
		/**
		 * Constructor of Iterator
		 * @param modificationCount represents current modification
		 * 			count of SimpleHashable 
		 */
		private IteratorImpl(int modificationCount) {
			this.modificationCount = modificationCount;
			this.usedRemove = false;
			this.currentSlot = -1;
		}

		
		@Override
		public boolean hasNext() {
			checkForConcurrentModification();
			
			if(current == null) { //when next() hasn't been called
				if(SimpleHashtable.this.table[0] != null) return true;
				return searchForElemFromCurrentSlot();
			} 
			if(current.next != null) return true;
				
			return searchForElemFromCurrentSlot();

		}
		
		/**
		 * Method is used to check ahead if any reference
		 * in table isn't null reference
		 * @return true if there is element ahead,
		 *     		false if there isn't
		 */
		private boolean searchForElemFromCurrentSlot() {
			for(int i = currentSlot + 1; i < SimpleHashtable.this.capacity; i++) {
				if(SimpleHashtable.this.table[i] != null) {
					return true;
				}
			}
			
			return false;
		}

		/**
		 * Method returns next entry in hash map
		 * @return next entry in hash map
		 * @throws NoSuchElementException if there are no more
		 * 			entries to return
		 */
		@Override
		public TableEntry<K, V> next() {
			checkForConcurrentModification();
			usedRemove = false;
			
			if(current != null) {
				current = current.next;
			}

			if(current == null) { 
				for(++currentSlot;  currentSlot < SimpleHashtable.this.capacity; currentSlot++) {
					if(SimpleHashtable.this.table[currentSlot] != null) {
						current = SimpleHashtable.this.table[currentSlot];
						break;
					}
				}
				if(currentSlot >= SimpleHashtable.this.capacity)
					throw new NoSuchElementException("No more elements.");
			}
			return current;
		}
		
		/**
		 * Method removes current entry in hash table
		 */
		@Override
		public void remove() {
			if(usedRemove) throw new IllegalStateException("remove() can only be called once after calling next().");
			if(current == null) throw new IllegalStateException("Remove can only be called if next() was called atleast once.");
			
			SimpleHashtable.this.remove(current.getKey());
			this.modificationCount++;
			usedRemove = true;
		}
		
		/**
		 * Method is used to check if modification outside of
		 * Iterator implementation happened. 
		 * @throws ConcurrentModificationException if modification happened outside of iterator
		 */
		private void checkForConcurrentModification() {
			if(this.modificationCount != SimpleHashtable.this.modificationCount) {
				throw new ConcurrentModificationException("Concurrent modificaion happened.");
			}
		}
	}
	
	
}
