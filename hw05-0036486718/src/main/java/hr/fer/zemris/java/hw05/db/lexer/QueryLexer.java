package hr.fer.zemris.java.hw05.db.lexer;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;


/**
 * Lexical analyser for simple query language.
 * @author Luka Mijić
 *
 */
public class QueryLexer {

	/**
	 * Character that encloses token value of 
	 * STRING_LITERAL type
	 */
	private static final char ENCLOSING_STRING = '"';
	
	/**
	 * Wildcard
	 */
	private static final char WILDCARD = '*';
	
	/**
	 * Predicates used to check certain conditions in generating tokens
	 */
	private static final Predicate<Character> LITERAL_PREDICATE;
	private static final Predicate<Character> OPERATORS_PREDICATE;
	private static final Predicate<Character> WORD_PREDICATE;
	
	/**
	 * List of operators
	 */
	private static List<Character> operators;
	static {
		operators = new ArrayList<>();
		operators.add('>');
		operators.add('<');
		operators.add('=');
		operators.add('!');
		
		LITERAL_PREDICATE = (c) -> c == ENCLOSING_STRING 
				|| (!Character.isLetterOrDigit(c) && c != WILDCARD && !Character.isWhitespace(c));
		OPERATORS_PREDICATE = (c) -> !operators.contains(c);
		WORD_PREDICATE = (c) -> !Character.isLetter(c);
	}
	
	/**
	 * Input text is stored into char array
	 */
	private char[] data;
	
	/**
	 * Current token
	 */
	private QueryToken token;
	
	/**
	 * Index of first index that is not used.
	 * Initial value is 0.
	 */
	private int currentIndex;
	
	public QueryLexer(String text) {
		this.data = Objects.requireNonNull(text, "Text given to lexer must not be null reference.").toCharArray();
		this.currentIndex = 0;
	}
	
	/**
     * @return token that was generated last
     */
	public QueryToken getToken() {
		return token;
	}
	
	/**
	 * Method is used to generate new token
	 * from input string.
	 * @return new QueryToken
	 */
	public QueryToken nextToken() {
		if(token != null && token.getType() == QueryTokenType.EOF) 
			throw new QueryLexerException("No more tokens avaliable");
		
		skipWhiteSpace();
		
		if(currentIndex >= data.length) {
			token = new QueryToken(QueryTokenType.EOF, null);
			return token;
		}
		
		String build;
		if(data[currentIndex] == ENCLOSING_STRING) {
			build = generateStringOfToken(LITERAL_PREDICATE);
			
			if(currentIndex < data.length) {
				build = data[currentIndex++] == '"' ? build + "\"" : build; 
			}
			if(!build.endsWith(String.valueOf(ENCLOSING_STRING)))
				throw new QueryLexerException("String must be enclosed with '\"'"
						+ " and all characters must be letter, digit or '" + WILDCARD +"'.");
			
			token = new QueryToken(QueryTokenType.STRING_LITERAL, build);
		} else if(operators.contains(data[currentIndex])) {
			build = generateStringOfToken(OPERATORS_PREDICATE);
			token = new QueryToken(QueryTokenType.OPERATOR, build);
		} else if(Character.isLetter(data[currentIndex])) {
			build = generateStringOfToken(WORD_PREDICATE);
			token = new QueryToken(QueryTokenType.WORD, build);
		} else {
			throw new QueryLexerException("Unable to do lexical analysis.");
		}
		
		return token;
	}
	
	/**
	 * Method adds characters to new Token as long as predicate
	 * is satisfied
	 * @param predicate
	 * @return new String token
	 */
	private String generateStringOfToken(Predicate<Character> predicate) {
		StringBuilder builder = new StringBuilder();
		builder.append(data[currentIndex++]);
		
		while(currentIndex < data.length) {
			if(predicate.test(data[currentIndex])) break;
			builder.append(data[currentIndex++]);
		}
		
		return builder.toString();
	}
	
	/**
	 * Method is used to skip all white space from current index 
	 * onwards. After calling this method index is at location of next
	 * non whitespace character or it is out of bounds. 
	 */
	private void skipWhiteSpace() {
		while(currentIndex < data.length) {
			if(!Character.isWhitespace(data[currentIndex])) break;
			currentIndex++;
		}
	}
}
