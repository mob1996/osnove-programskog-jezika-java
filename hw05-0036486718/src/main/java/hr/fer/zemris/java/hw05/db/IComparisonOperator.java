package hr.fer.zemris.java.hw05.db;

/**
 * Interface is used as comparison tool 
 * for two strings.
 * @author Luka Mijić
 *
 */
public interface IComparisonOperator {

	/**
	 * Compares two strings satisfy operation. 
	 * @param value1 of first String
	 * @param value2 of second String
	 * @return true if requirements are satisfied
	 */
	public boolean satisfied(String value1, String value2);
}
