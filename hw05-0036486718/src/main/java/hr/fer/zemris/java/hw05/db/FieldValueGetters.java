package hr.fer.zemris.java.hw05.db;

/**
 * Class stores multiple implementations of IFieldValueGetter interface.
 * 
 * @author Luka Mijić
 *
 */
public class FieldValueGetters {

	/**
	 * Get first name from student record.
	 */
	public static final IFieldValueGetter FIRST_NAME = (record) -> record.getFirstName();
	
	/**
	 * Get last name from student record.
	 */
	public static final IFieldValueGetter LAST_NAME = (record) -> record.getLastName();
	
	/**
	 * Get jmbag from student record.
	 */
	public static final IFieldValueGetter JMBAG = (record) -> record.getJmbag();
}
