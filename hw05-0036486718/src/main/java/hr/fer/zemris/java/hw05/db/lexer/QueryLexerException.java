package hr.fer.zemris.java.hw05.db.lexer;

/**
 * Class used to model exceptions that can occur while
 * working with lexical analyser. 
 * @author Luka Mijić
 */
public class QueryLexerException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor
	 */
	public QueryLexerException() {
		super();
	}

	/**
	 * Constructor that takes message argument.
	 * @param message is used to give better feedback
	 */
	public QueryLexerException(String message) {
		super(message);
	}
}
