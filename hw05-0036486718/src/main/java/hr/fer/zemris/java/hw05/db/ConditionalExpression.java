package hr.fer.zemris.java.hw05.db;

/**
 * Class represents one conditional expression.
 * @author Luka Mijić
 *
 */
public class ConditionalExpression {

	/**
	 * Comparison operator for expresion
	 */
	private IComparisonOperator comparisonOperator;
	
	/**
	 * Field getter for records
	 */
	private IFieldValueGetter fieldGetter;
	
	/**
	 * Literal used in comparison
	 */
	private String stringLiteral;

	/**
	 * Constructor used for creating ConditionalExpression
	 * @param comparisonOperator of conditional expression
	 * @param fieldGetter of conditional expression
	 * @param stringLiteral of conditional expression
	 */
	public ConditionalExpression(IComparisonOperator comparisonOperator, IFieldValueGetter fieldGetter,
			String stringLiteral) {
		this.comparisonOperator = comparisonOperator;
		this.fieldGetter = fieldGetter;
		this.stringLiteral = stringLiteral;
	}

	/**
	 * @return comparison operator of this conditional expression
	 */
	public IComparisonOperator getComparisonOperator() {
		return comparisonOperator;
	}

	/**
	 * @return field getter operator of this conditional expression
	 */
	public IFieldValueGetter getFieldGetter() {
		return fieldGetter;
	}

	/**
	 * @return string literal operator of this conditional expression
	 */
	public String getStringLiteral() {
		return stringLiteral;
	}
	
}
