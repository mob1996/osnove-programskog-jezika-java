package hr.fer.zemris.java.hw05.db;

/**
 * Class used to model exceptions that can occur while
 * working with parser . 
 * @author Luka Mijić
 *
 */
public class QueryParserException extends RuntimeException{

	private static final long serialVersionUID = 636263890867226018L;


	/**
	 * Default constructor of ParserException
	 */
	public QueryParserException() {
		super();
	}
	
	
	/**
	 * Constructor of ParserException that takes
	 * message argument
	 * @param message argument
	 */
	public QueryParserException(String message) {
		super(message);
	}
}
