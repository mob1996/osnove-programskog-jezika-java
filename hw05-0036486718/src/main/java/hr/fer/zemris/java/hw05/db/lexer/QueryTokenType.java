package hr.fer.zemris.java.hw05.db.lexer;

/**
* This enum class represents all types of QueryToken
* @author Luka Mijić
* 
*/
public enum QueryTokenType {
	/**
	 * This type represents end of file
	 */
	EOF,
	/**
	 * This type represents field name
	 */
	WORD,
	/**
	 * This type represents operators
	 */
	OPERATOR,
	/**
	 * This type represents string literal.
	 * String literal is enclosed by "
	 */
	STRING_LITERAL
}
