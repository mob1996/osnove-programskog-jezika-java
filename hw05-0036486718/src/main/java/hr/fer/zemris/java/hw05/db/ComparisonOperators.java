package hr.fer.zemris.java.hw05.db;

/**
 * Class stores multiple implementations of IComparisonOperator interface.
 * 
 * @author Luka Mijić
 *
 */
public class ComparisonOperators {

	/**
	 * Number of allowed wildcards
	 */
	private static final int ALLOWED_WILDCARDS = 1;
	
	/**
	 * Wildcard
	 */
	private static final String WILDCARD = "*";
	
	/**
	 * Check if value1 is lesser than value2
	 */
	public static final IComparisonOperator LESS = (value1, value2) -> value1.compareTo(value2) < 0;

	/**
	 * Check if value1 is lesser or equal to value2
	 */
	public static final IComparisonOperator LESS_OR_EQUAL = (value1, value2) -> value1.compareTo(value2) <= 0;

	/**
	 * Check if value1 is greater than value2.
	 */
	public static final IComparisonOperator GREATER = (value1, value2) -> value1.compareTo(value2) > 0;

	/**
	 * Check if value1 is greater or equal to value2.
	 */
	public static final IComparisonOperator GREATER_OR_EQUAL = (value1, value2) -> value1.compareTo(value2) >= 0;

	/**
	 * Check if value1 is equal to value2
	 */
	public static final IComparisonOperator EQUALS = (value1, value2) -> value1.equals(value2);

	/**
	 * Check if value1 is not equal to value2
	 */
	public static final IComparisonOperator NOT_EQUALS = (value1, value2) -> !value1.equals(value2);

	/**
	 * Check if value1 is like value2
	 */
	public static final IComparisonOperator LIKE = (value1, pattern) -> like(value1, pattern);

	/**
	 * Method is used to check if value is like pattern
	 * @param value
	 * @param pattern
	 * @return true if value is like pattern, false if it isn't.
	 * @throws IllegalArgumentException if number of wildcards is higher than 1
	 */
	private static boolean like(String value, String pattern) {
		int numOfWildCards = pattern.length() - pattern.replace(WILDCARD, "").length();

		if (numOfWildCards > ALLOWED_WILDCARDS) {
			throw new IllegalArgumentException("Only one wildcard '"+ WILDCARD + "' allowed.");
		}

		return value.matches(pattern.replace("*", "(.*)"));
	}
}
