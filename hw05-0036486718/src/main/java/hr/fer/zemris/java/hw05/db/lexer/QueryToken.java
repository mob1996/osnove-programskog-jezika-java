package hr.fer.zemris.java.hw05.db.lexer;

/** 
 * Class QueryToken represents combination of
 * TokenType and value created by lexical analyser.
 * @author Luka Mijić
 * 
 */
public class QueryToken {
	
	/**
	 * Type of token
	 */
	private QueryTokenType type;
	
	/**
	 * Value of token
	 */
	private Object value;

	/**
	 * Constructor that takes two arguments for 
	 * creation of new QueryToken. 
	 * @param type of created QueryToken
	 * @param value of created QueryToken
	 */
	public QueryToken(QueryTokenType type, Object value) {
		super();
		this.type = type;
		this.value = value;
	}

	/**
	 * @return type of token
	 */
	public QueryTokenType getType() {
		return type;
	}

	/**
	 * @return value of token
	 */
	public Object getValue() {
		return value;
	}
	
	
	
	
}
