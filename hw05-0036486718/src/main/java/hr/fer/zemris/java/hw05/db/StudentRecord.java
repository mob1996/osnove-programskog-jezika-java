package hr.fer.zemris.java.hw05.db;

/**
 * Class is used to represent record
 * of one Student.
 * @author Luka Mijić
 *
 */
public class StudentRecord {

	/**
	 * jmbag of student
	 */
	private String jmbag;
	
	/**
	 * First name of student
	 */
	private String firstName;
	
	/**
	 * Last name of student
	 */
	private String lastName;
	
	/**
	 * Grade that student recieved on exam.
	 */
	private int finalGrade;

	/**
	 * Constructor used for creating student record
	 * @param jmbag of student
	 * @param firstName of student
	 * @param lastName of student
	 */
	public StudentRecord(String jmbag, String lastName, String firstName, int finalGrade) {
		super();
		this.jmbag = jmbag;
		this.firstName = firstName;
		this.lastName = lastName;
		this.finalGrade = finalGrade;
	}

	/**
	 * @return jmbag of student
	 */
	public String getJmbag() {
		return jmbag;
	}

	/**
	 * @return first name of student
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return last name of student
	 */
	public String getLastName() {
		return lastName;
	}
	
	
	/**
	 * @return final grade of student
	 */
	public int getFinalGrade() {
		return finalGrade;
	}

	/**
	 * Method is used to calculate hashcode
	 * @return calculated hashcode
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((jmbag == null) ? 0 : jmbag.hashCode());
		return result;
	}

	/**
	 * Method is used to check if two 
	 * objects are equal
	 * @return true if objects are equal
	 * 		false if they are not
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StudentRecord other = (StudentRecord) obj;
		if (jmbag == null) {
			if (other.jmbag != null)
				return false;
		} else if (!jmbag.equals(other.jmbag))
			return false;
		return true;
	}
	
}
