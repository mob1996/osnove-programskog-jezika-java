package hr.fer.zemris.java.hw05.db;

/**
 * Interface that is used 
 * as an filter for student records.
 * @author Luka Mijić
 *
 */
public interface IFilter {
 
	/**
	 * Method used for filtering of student records
	 * @param record that is being filtered
	 * @return true if record is acceptable, 
	 * 		false if it is not.
	 */
	public boolean accepts(StudentRecord record);
}
