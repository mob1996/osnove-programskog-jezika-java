package hr.fer.zemris.java.hw05.db;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import hr.fer.zemris.java.hw05.collections.SimpleHashtable;
import hr.fer.zemris.java.hw05.db.lexer.QueryLexer;
import hr.fer.zemris.java.hw05.db.lexer.QueryTokenType;

/**
 * Class QueryParser is used for parsing input text
 * into syntax tree of Nodes. It uses Lexer for generating
 * tokens.
 * @author Luka Mijić
 *
 */
public class QueryParser {

	/**
	 * Simple hashtable that makes connection between string of operators
	 * and IComparsionOperators
	 */
	private static SimpleHashtable<String, IComparisonOperator> comparisonOperators;
	
	/**
	 * Hashtable that makes connection with name of fields with IFieldGetter
	 */
	private static SimpleHashtable<String, IFieldValueGetter> fieldGetters;
	
	static {
		comparisonOperators = new SimpleHashtable<>();
		fieldGetters = new SimpleHashtable<>(8);
		
		comparisonOperators.put("<", ComparisonOperators.LESS);
		comparisonOperators.put("<=", ComparisonOperators.LESS_OR_EQUAL);
		comparisonOperators.put(">", ComparisonOperators.GREATER);
		comparisonOperators.put(">=", ComparisonOperators.GREATER_OR_EQUAL);
		comparisonOperators.put("=", ComparisonOperators.EQUALS);
		comparisonOperators.put("!=", ComparisonOperators.NOT_EQUALS);
		comparisonOperators.put("LIKE", ComparisonOperators.LIKE);
		
		
		fieldGetters.put("jmbag", FieldValueGetters.JMBAG);
		fieldGetters.put("firstName", FieldValueGetters.FIRST_NAME);
		fieldGetters.put("lastName", FieldValueGetters.LAST_NAME);
		
	}
	
	/**
	 * Used for multiple commands
	 */
	private static final String AND = "and";
	
	/**
	 * List of parsed expressions
	 */
	private List<ConditionalExpression> expressions;
	
	/**
	 * Lexer 
	 */
	private QueryLexer lexer;
	
	/**
	 * Constructor used for creating Query parser.
	 * It also creates QueryLexer.
	 * @param query
	 */
	public QueryParser(String query) {
		Objects.requireNonNull(query, "Query must not be null");
		this.lexer = new QueryLexer(query);
		this.expressions = new LinkedList<>();
		
		parse();
	}
	
	/**
	 * Method is used to parsing query into list of
	 * ConditionalExpressions
	 */
	private void parse() {
		lexer.nextToken();
		parseConditionalExpression();
		
		while(true) {
			lexer.nextToken();
			if(isTokenOfType(QueryTokenType.EOF)) {
				break;
			}
			
			if(!isTokenOfType(QueryTokenType.WORD) || 
					!lexer.getToken().getValue().toString().toLowerCase().equals(AND)) {
				throw new QueryParserException("Expected 'and'. Was " + lexer.getToken().getValue());
			}
			lexer.nextToken();
			
			parseConditionalExpression();
			
		}
	}
	
	/**
	 * Method creates ConditionalExpression and adds it to expressions list
	 */
	private void parseConditionalExpression() {
		IFieldValueGetter fieldGetter = fieldGetters.get(lexer.getToken().getValue());
		checkIfExpectedToken(QueryTokenType.WORD, fieldGetter, 
				"Expected legal field name. Was " + lexer.getToken().getValue());
		
		lexer.nextToken();
		IComparisonOperator operator = comparisonOperators.get(lexer.getToken().getValue());
		try {
			checkIfExpectedToken(QueryTokenType.OPERATOR, operator, 
					"Expected legal operator. Was " + lexer.getToken().getValue());
		} catch(QueryParserException exc) {
			checkIfExpectedToken(QueryTokenType.WORD, operator, //if operator is LIKE
					"Expected legal operator. Was " + lexer.getToken().getValue());
		}
		
		
		lexer.nextToken();
		String literal = (String) lexer.getToken().getValue();
		checkIfExpectedToken(QueryTokenType.STRING_LITERAL, literal, 
				"Expected string literal. Was + " + lexer.getToken().getValue());
		
		literal = literal.replace("\"", "");
		
		expressions.add(new ConditionalExpression(operator, fieldGetter, literal));
		
	}
	
	/**
	 * Method checks if current token is expected one. 
	 * @param type that is being checked
	 * @param isNull is check if it references null reference
	 * @param message that is thrown if conditions are not satisfied
	 * @throws QueryParserException if conditions are not satisfied
	 */
	private void checkIfExpectedToken(QueryTokenType type, Object isNull, String message) {
		if(!isTokenOfType(type) || isNull == null) {
			throw new QueryParserException(message);
		}
	}
	
	/**
	 * Helper method that is used to check if current token is
	 * same type as given type
	 * @param type that we are checking
	 * @return true if current token's type is same as type
	 */
	private boolean isTokenOfType(QueryTokenType type) {
		return lexer.getToken().getType() == type;
	}

	/**
	 * Method check if list of expressions can be interpeted as
	 * DirectQuery. Direct query is 1 query in form:
	 * jmbag="literal".
	 * @return true if it can be interpeted as direct query,
	 * 			otherwise return false
	 */
	public boolean isDirectQuery() {
		if(expressions.size() != 1) return false;
		
		ConditionalExpression condExpression = expressions.get(0);
		
		if(condExpression.getFieldGetter() != FieldValueGetters.JMBAG) return false;
		if(condExpression.getComparisonOperator() != ComparisonOperators.EQUALS) return false;
		
		return true;
	}
	
	/**
	 * Method is used to return jmbag from direct query.
	 * @return jmbag
	 * @throws IllegalArgumentException if query is not direct query		
	 */
	public String getQueriedJMBAG() {
		if(!isDirectQuery()) {
			throw new IllegalArgumentException("Can't call getQueriedJMBAG() on non direct query.");
		}
		
		return expressions.get(0).getStringLiteral();
	}
	
	/**
	 * Method is used to return all expressions
	 * @return expressions
	 */
	public List<ConditionalExpression> getQuery(){
		return expressions;
	}
}
