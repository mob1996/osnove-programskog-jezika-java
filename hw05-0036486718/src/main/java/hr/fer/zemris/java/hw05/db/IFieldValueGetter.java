package hr.fer.zemris.java.hw05.db;

/**
 * Interface that is used for getting
 * values from StudentRecord.
 * @author Luka Mijić
 *
 */
public interface IFieldValueGetter {

	/**
	 * Method is used to get certain field from
	 * student record.
	 * @param record of student
	 * @return String field from student.
	 */
	public String get(StudentRecord record);
}
