package hr.fer.zemris.java.hw05.db;

import java.util.List;

/**
 * Class implements IFilter.
 * Class is used to filter records on multiple
 * expressions. 
 * @author Luka Mijić
 *
 */
public class QueryFilter implements IFilter {

	/**
	 * List of queries
	 */
	List<ConditionalExpression> expressions;
	
	/**
	 * Constructor that creates QueryFilter
	 * @param expressions are used as conditions for accepts method
	 */
	public QueryFilter(List<ConditionalExpression> expressions) {
		super();
		this.expressions = expressions;
	}

	/**
	 * Method is used to check if record satisfies conditions.
	 * @param record that is being checked
	 * @return true if record satisfies conditions,
	 * 			otherwise return false
	 */
	@Override
	public boolean accepts(StudentRecord record) {
		for(ConditionalExpression expr: expressions) {
			boolean recordSatisfied = expr.getComparisonOperator()
					.satisfied(expr.getFieldGetter().get(record), expr.getStringLiteral());
			
			if(!recordSatisfied) return recordSatisfied;
		}
		
		return true;
	}

}
