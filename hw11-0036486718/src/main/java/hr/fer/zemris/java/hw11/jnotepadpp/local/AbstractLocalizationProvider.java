package hr.fer.zemris.java.hw11.jnotepadpp.local;

import java.util.ArrayList;
import java.util.List;

/**
 * Class provides skeleton for implementation of {@link ILocalizationProvider}.
 * It provides some basic functionalities like storage of {@link ILocalizationListener}s,
 * their adding and removal. It also can notify all {@link ILocalizationListener} of changes.
 * @author Luka Mijić
 *
 */
public abstract class AbstractLocalizationProvider implements ILocalizationProvider {

	/**
	 * Stores listener
	 */
	private List<ILocalizationListener> listeners;
	
	/**
	 * Creates {@link List} that stores listener.
	 */
	public AbstractLocalizationProvider() {
		listeners = new ArrayList<>();
	}
	
	/**
	 * Adds given listener to {@link List} of listeners.
	 * @param listener that is being added
	 */
	@Override
	public void addLocalizationListener(ILocalizationListener listener) {
		listeners.add(listener);
	}

	/**
	 * Removes given listener from {@link List} of listeners.
	 */
	@Override
	public void removeLocalizationListener(ILocalizationListener listener) {
		listeners.add(listener);
	}
	
	/**
	 * Calls {@link ILocalizationListener#localizationChanged()}
	 * for each stored {@link ILocalizationListener}.
	 */
	public void fire() {
		listeners.forEach(l -> l.localizationChanged());
	}
}
