package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;

import javax.swing.Action;
import javax.swing.JFrame;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.models.impl.DefaultMultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.util.JNotepadPPUtilities;

/**
 * {@link ExitAction} extends {@link LocalizableAction}.
 * This {@link Action} calls {@link JNotepadPPUtilities#exit(JFrame, DefaultMultipleDocumentModel)}.
 * @author Luka Mijić
 *
 */
public class ExitAction extends LocalizableAction {

	private static final long serialVersionUID = 1L;

	/*
	 * Frame that is closed
	 */
	private JFrame frame;
	
	/**
	 * Creates {@link ExitAction}.
	 * @param key for localisation 
	 * @param lp provider of localisation
	 * @param frame that is closed
	 */
	public ExitAction(String key, ILocalizationProvider lp, JFrame frame) {
		super(key, lp);
		this.frame = frame;
	}

	/**
	 * When triggered method calls {@link JNotepadPPUtilities#exit(JFrame, DefaultMultipleDocumentModel)}
	 * that tries to dispose given {@link JFrame}, but first asks user if he wants to save edited documents.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		JNotepadPPUtilities.exit(frame, JNotepadPP.multipleModel);
	}

}
