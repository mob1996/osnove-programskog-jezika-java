package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;

import javax.swing.Action;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizationProvider;

/**
 * {@link ChangeLangAction} extends {@link LocalizableAction}.
 * This {@link Action} changes language for {@link LocalizationProvider}.
 * @author Luka Mijić
 *
 */
public class ChangeLangAction extends LocalizableAction {

	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Language code for croatian
	 */
	public static final String CROATIAN = "hr";
	
	/**
	 * Language code for english
	 */
	public static final String ENGLISH = "en";
	
	/**
	 * Language code for german
	 */
	public static final String GERMAN = "de";

	/**
	 * Language that is being set
	 */
	private String lang;
	
	/**
	 * Constructor of {@link ChangeLangAction}.
	 * @param key for localisation 
	 * @param lp provider of localisation
	 * @param lang language that is being set
	 */
	public ChangeLangAction(String key, ILocalizationProvider lp, String lang) {
		super(key, lp);
		this.lang = lang;
	}

	/**
	 * Sets {@link LocalizationProvider}s language to <code>lang</code>.
	 * @param e action event
 	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		LocalizationProvider.getInstance().setLanguage(lang);
		JNotepadPP.currentLang = lang;
	}

}
