package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.models.SingleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.models.impl.DefaultMultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.util.JNotepadPPUtilities;

/**
 * {@link CloseAction} extends {@link LocalizableAction}.
 * This action closes current {@link SingleDocumentModel} of {@link DefaultMultipleDocumentModel}.
 * @author Luka Mijić
 *
 */
public class CloseAction extends LocalizableAction {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor of {@link CaseAction}.
	 * @param key for localisation 
	 * @param lp provider of localisation
	 */
	public CloseAction(String key, ILocalizationProvider lp) {
		super(key, lp);
	}
	
	/**
	 * When triggered method closes down current {@link SingleDocumentModel}
	 * of {@link JNotepadPP}s {@link DefaultMultipleDocumentModel}.
	 * @param e action event
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		SingleDocumentModel model = JNotepadPP.multipleModel.getCurrentDocument();
		if(model == null) {
			return;
		}
		
		if(model.isModified()) {
			int result = JOptionPane.showConfirmDialog(JNotepadPP.multipleModel
					, "Do you want to save this document?"
					, "Save"
					, JOptionPane.YES_NO_CANCEL_OPTION
					);
			if(result == JOptionPane.CANCEL_OPTION) {
				return;
			} else if(result == JOptionPane.YES_OPTION) {
					JNotepadPPUtilities.save(JNotepadPP.multipleModel, model, JNotepadPPUtilities.GET_PATH);
			}
		}
		
		JNotepadPP.multipleModel.closeDocument(model);
	}
}
