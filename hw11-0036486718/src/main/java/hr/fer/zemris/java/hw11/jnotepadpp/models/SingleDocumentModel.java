package hr.fer.zemris.java.hw11.jnotepadpp.models;

import java.nio.file.Path;

import javax.swing.JTextArea;

/**
 * Inteface models {@link SingleDocumentModel}.
 * @author Luka Mijić
 *
 */
public interface SingleDocumentModel {
	
	/**
	 * @return {@link JTextArea} associated with {@link SingleDocumentModel}
	 */
	JTextArea getTextComponent();

	/**
	 * @return {@link Path} of {@link SingleDocumentModel}
	 */
	Path getFilePath();

	/**
	 * Sets {@link Path} of {@link SingleDocumentModel} to given {@link Path}.
	 * @param path is new path of {@link SingleDocumentModel}
	 */
	void setFilePath(Path path);

	/**
	 * @return true if model was modified, otherwise return false
 	 */
	boolean isModified();

	/**
	 * Sets modified flag to given boolean.
	 * @param modified is new value of modified flag
	 */
	void setModified(boolean modified);

	/**
	 * Adds {@link SingleDocumentListener} to {@link SingleDocumentModel}.
	 * @param l is {@link SingleDocumentListener} that is being added.
	 */
	void addSingleDocumentListener(SingleDocumentListener l);

	/**
	 * Remove {@link SingleDocumentListener} from {@link SingleDocumentModel}.
	 * @param l is {@link SingleDocumentListener} that is being removed.
	 */
	void removeSingleDocumentListener(SingleDocumentListener l);
}
