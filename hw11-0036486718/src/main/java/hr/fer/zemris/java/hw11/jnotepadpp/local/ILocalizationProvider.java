package hr.fer.zemris.java.hw11.jnotepadpp.local;

/**
 * Class provides contract for implementation of 
 * {@link ILocalizationProvider}s. They provide way of accessing
 * translations for different languages. It also enables adding
 * of various {@link ILocalizationListener}s.
 * @author Luka Mijić
 *
 */
public interface ILocalizationProvider {

	/**
	 * Adds given listeners to {@link ILocalizationProvider}.
	 * @param listener that is added
	 */
	void addLocalizationListener(ILocalizationListener listener);
	
	/**
	 * Removes given listener from {@link LocalizationProvider}.
	 * @param listener that is removed
	 */
	void removeLocalizationListener(ILocalizationListener listener);
	
	/**
	 * @param key for accessing map value
	 * @return value associated with key.
	 */
	String getString(String key);
}
