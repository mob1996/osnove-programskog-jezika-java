package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.models.SingleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.util.JNotepadPPUtilities;

/**
 * {@link SaveAsAction} extends {@link LocalizableAction}.
 * It always asks user for saving destination before saving.
 * @author Luka Mijić
 *
 */
public class SaveAsAction extends LocalizableAction {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Creates {@link SaveAsAction}.
	 * @param key for localisation 
	 * @param lp provider of localisation
	 */
	public SaveAsAction(String key, ILocalizationProvider lp) {
		super(key, lp);
	}
	
	/**
	 * When method is triggered it asks user to provide disk location
	 * for saving file using {@link JFileChooser}.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		SingleDocumentModel model = JNotepadPP.multipleModel.getCurrentDocument();
		if(model == null) {
			return;
		}
		
		if(model.getFilePath() != null) {
			JOptionPane.showMessageDialog(JNotepadPP.multipleModel
					, "File already exists!" , "Warning", JOptionPane.WARNING_MESSAGE);
		}
		
		JNotepadPPUtilities.save(JNotepadPP.multipleModel, model, JNotepadPPUtilities.GET_NULL);
	}

}
