package hr.fer.zemris.java.hw11.jnotepadpp.util;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * {@link ResourcePaths} is utility class that stores {@link Paths}
 * to various resources in src/main/resources.
 * It increases code's resistance to errors and if error happens
 * it can be fix at one place. 
 * @author Luka Mijić
 *
 */
public class ResourcePaths {

	/**
	 * Various paths, names are self explanatory.
	 */
	
	public static final Path MODIFIED_ICON_PATH = Paths.get("src/main/resources/hr/fer"
			+ "/zemris/java/hw11/jnotepadpp/util/icons/modified.png");
	
	public static final Path UNMODIFIED_ICON_PATH = Paths.get("src/main/resources/hr/fer"
			+ "/zemris/java/hw11/jnotepadpp/util/icons/unmodified.png");
	
	public static final Path CLOSE_ICON_PATH = Paths.get("src/main/resources/hr/fer" 
			+ "/zemris/java/hw11/jnotepadpp/util/icons/close.png");
	
	public static final Path COPY_ICON_PATH = Paths.get("src/main/resources/hr/fer" + 
			"/zemris/java/hw11/jnotepadpp/util/icons/copy.png");
	
	public static final Path CUT_ICON_PATH = Paths.get("src/main/resources/hr/fer" + 
			"/zemris/java/hw11/jnotepadpp/util/icons/cut.png");
	
	public static final Path EXIT_ICON_PATH = Paths.get("src/main/resources/hr/fer" + 
			"/zemris/java/hw11/jnotepadpp/util/icons/exit.png");
	
	public static final Path NEW_ICON_PATH = Paths.get("src/main/resources/hr/fer" + 
			"/zemris/java/hw11/jnotepadpp/util/icons/new_document.png");
	
	public static final Path OPEN_ICON_PATH = Paths.get("src/main/resources/hr/fer" + 
			"/zemris/java/hw11/jnotepadpp/util/icons/open_file.png");
	
	public static final Path PASTE_ICON_PATH = Paths.get("src/main/resources/hr/fer" + 
			"/zemris/java/hw11/jnotepadpp/util/icons/paste.png");
	
	public static final Path SAVE_AS_ICON_PATH = Paths.get("src/main/resources/hr/fer" + 
			"/zemris/java/hw11/jnotepadpp/util/icons/save_as.png");
	
	public static final Path SAVE_ICON_PATH = Paths.get("src/main/resources/hr/fer" + 
			"/zemris/java/hw11/jnotepadpp/util/icons/save.png");
	
	public static final Path STATS_ICON_PATH = Paths.get("src/main/resources/hr/fer" + 
			"/zemris/java/hw11/jnotepadpp/util/icons/stats.png");
	
	public static final Path NOTEPAD_ICON_PATH = Paths.get("src/main/resources/hr/fer" + 
			"/zemris/java/hw11/jnotepadpp/util/icons/jnotepad++.png");
	
	public static final Path LOWER_CASE_ICON_PATH = Paths.get("src/main/resources/hr/fer" + 
			"/zemris/java/hw11/jnotepadpp/util/icons/lower_case.png");
	
	public static final Path UPPER_CASE_ICON_PATH = Paths.get("src/main/resources/hr/fer" + 
			"/zemris/java/hw11/jnotepadpp/util/icons/upper_case.png");
	
	public static final Path INVERSE_CASE_ICON_PATH = Paths.get("src/main/resources/hr/fer" + 
			"/zemris/java/hw11/jnotepadpp/util/icons/inverse_case.png");
	
	public static final Path UK_ICON_PATH = Paths.get("src/main/resources/hr/fer" + 
			"/zemris/java/hw11/jnotepadpp/util/icons/united_kingdom.png");
	
	public static final Path CROATIA_ICON_PATH = Paths.get("src/main/resources/hr/fer" + 
			"/zemris/java/hw11/jnotepadpp/util/icons/croatia.png");
	
	public static final Path GERMANY_ICON_PATH = Paths.get("src/main/resources/hr/fer" + 
			"/zemris/java/hw11/jnotepadpp/util/icons/germany.png");
}
