package hr.fer.zemris.java.hw11.jnotepadpp.models.impl;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;

import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;

import hr.fer.zemris.java.hw11.jnotepadpp.models.SingleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.models.SingleDocumentModel;

/**
 * Implementation of {@link SingleDocumentModel}.
 * It stores {@link JTextArea}, {@link Path}, {@link List} of {@link SingleDocumentListener}s,
 * and <code>boolean</code> modified properties.
 * @author Luka Mijić
 *
 */
public class DefaultSingleDocumentModel implements SingleDocumentModel {

	/**
	 * BiConsumer that calls {@link SingleDocumentListener#documentModifyStatusUpdated(SingleDocumentModel)}
	 * on given {@link SingleDocumentListener} and provides given {@link SingleDocumentModel} as argument.
	 */
	public static final BiConsumer<SingleDocumentListener, SingleDocumentModel>
								MODIFIY_UPDATE = (l, s) -> l.documentModifyStatusUpdated(s);
		
	/**
	 * BiConsumer that calls {@link SingleDocumentListener#documentFilePathUpdated(SingleDocumentModel)}
	 * on given {@link SingleDocumentListener} and provides given {@link SingleDocumentModel} as argument.
	 */
	public static final BiConsumer<SingleDocumentListener, SingleDocumentModel>
								PATH_UPDATE = (l, s) -> l.documentFilePathUpdated(s);
	
	/**
	 * BiConsumer that calls {@link SingleDocumentListener#documentFilePathUpdated(SingleDocumentModel)}
	 * and {@link SingleDocumentListener#documentFilePathUpdated(SingleDocumentModel)}
	 * on given {@link SingleDocumentListener} and provides given {@link SingleDocumentModel} as argument.
	 */
	public static final BiConsumer<SingleDocumentListener, SingleDocumentModel>
								UPDATE = (l, s) -> {
									l.documentModifyStatusUpdated(s);
									l.documentFilePathUpdated(s);
								};

	/**
	 * Path of {@link SingleDocumentModel}
	 */
	private Path filePath;
	
	/**
	 * If flag is true, then {@link SingleDocumentModel} was modified.
	 */
	private boolean modified;
	
	/**
	 * Provides way for users to modify {@link SingleDocumentModel}
	 */
	private JTextArea holder;
	
	/**
	 * List of {@link SingleDocumentListener}s
	 */
	private List<SingleDocumentListener> listeners;
	
	/**
	 * Creates {@link SingleDocumentModel}.
	 * Initialises all properties and {@link DocumentListener}
	 * to {@link DefaultSingleDocumentModel#holder#getDocument}.
	 * Whenever change on {@link Document} occurs, modified flag is
	 * set to true, and interested {@link SingleDocumentListener}
	 * are notified.
	 * @param filePath of {@link SingleDocumentListener}
	 * @param textContent {@link SingleDocumentModel#holder} is filled with this text
	 */
	public DefaultSingleDocumentModel(Path filePath, String textContent) {
		this.filePath = filePath;
		this.modified = false;
		this.holder = new JTextArea(textContent);
		this.listeners = new ArrayList<>();
		
		this.holder.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				modified = true;
				fire(MODIFIY_UPDATE);
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				modified = true;
				fire(MODIFIY_UPDATE);
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				modified = true;
				fire(MODIFIY_UPDATE);
			}
		});
	}
	
	/**
	 * @return {@link JTextArea} property.
	 */
	@Override
	public JTextArea getTextComponent() {
		return holder;
	}

	/**
	 * @return {@link Path} property.
	 */
	@Override
	public Path getFilePath() {
		return filePath;
	}

	/**
	 * Sets {@link Path} property to given path.
	 * It also notifies {@link SingleDocumentListener} of this change.
	 * @param path new value of {@link Path} property.
	 * @throws NullPointerException if <code>path</code> is null reference
	 */
	@Override
	public void setFilePath(Path path) {
		this.filePath = Objects.requireNonNull(path, "Given path must not be null.");
		fire(PATH_UPDATE);
	}

	/**
	 * @return value of <code>modified</code> flag
	 */
	@Override
	public boolean isModified() {
		return modified;
	}

	/**
	 * Sets <code>modified</code> flag to given value.
	 * It also notifies {@link SingleDocumentListener} of this change.
	 * @param modified is new value of modified property.
	 */
	@Override
	public void setModified(boolean modified) {
		this.modified = modified;
		fire(MODIFIY_UPDATE);
	}

	/**
	 * Adds given {@link SingleDocumentListener} to {@link List}.
	 * @param l is {@link SingleDocumentListener} that is being added.
	 */
	@Override
	public void addSingleDocumentListener(SingleDocumentListener l) {
		listeners.add(l);
	}

	/**
	 * Removes given {@link SingleDocumentListener} from {@link List}.
	 * @param l is {@link SingleDocumentListener} that is being removed.
	 */
	@Override
	public void removeSingleDocumentListener(SingleDocumentListener l) {
		listeners.remove(l);
	}
	
	/**
	 * Notifies all {@link SingleDocumentListener} that are added of change.
	 * @param biConsumer that is given calls {@link BiConsumer#accept(SingleDocumentListener, SingleDocumentModel)}.
	 * 					 Usual {@link BiConsumer} would look like (l, doc) -> l.documentModifyStatusUpdated(doc)
	 */
	private void fire(BiConsumer<SingleDocumentListener, SingleDocumentModel> biConsumer) {
		listeners.forEach(l -> biConsumer.accept(l, this));
	}

}
