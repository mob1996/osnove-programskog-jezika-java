package hr.fer.zemris.java.hw11.jnotepadpp.models;

import java.nio.file.Path;

/**
 * Inteface models {@link MultipleDocumentModel}.
 * @author Luka Mijić
 *
 */
public interface MultipleDocumentModel extends Iterable<SingleDocumentModel>{
	
	/**
	 * Creates new, empty {@link SingleDocumentModel}
	 * @return created {@link SingleDocumentModel}
	 */
	SingleDocumentModel createNewDocument();

	/**
	 * @return current {@link SingleDocumentModel}.
	 */
	SingleDocumentModel getCurrentDocument();

	/**
	 * Loads new {@link SingleDocumentModel} using given {@link Path}.
	 * @param path to file that is loaded
	 * @return loaded {@link SingleDocumentModel}
	 */
	SingleDocumentModel loadDocument(Path path);

	/**
	 * Saves {@link SingleDocumentModel} to given path.
	 * @param model that is saved
	 * @param newPath is location of save point
	 */
	void saveDocument(SingleDocumentModel model, Path newPath);

	/**
	 * Closes given {@link SingleDocumentModel}.
	 * @param model that is being closed
	 */
	void closeDocument(SingleDocumentModel model);

	/**
	 * Registers {@link MultipleDocumentListener} to model.
	 * @param l is listener that is being registered
	 */
	void addMultipleDocumentListener(MultipleDocumentListener l);

	/**
	 * Deregisteres given {@link MultipleDocumentListener} from model.
	 * @param l is listener thet is being deregistered.
	 */
	void removeMultipleDocumentListener(MultipleDocumentListener l);

	/**
	 * @return number of stored documents
	 */
	int getNumberOfDocuments();

	/**
	 * Gets {@link SingleDocumentModel} stored at given index.
	 * @param index of {@link SingleDocumentModel}.
	 * @return retrieved {@link SingleDocumentModel}
	 */
	SingleDocumentModel getDocument(int index);
}
