package hr.fer.zemris.java.hw11.jnotepadpp.local;

/**
 * {@link LocalizationProviderBridge} extends {@link AbstractLocalizationProvider}.
 * It acts as a bridge between real implementation of {@link ILocalizationProvider}
 * and interested listeners. It simply passes translations requests to
 * implementation of {@link ILocalizationProvider} and returns results to user. 
 * @author Luka Mijić
 *
 */
public class LocalizationProviderBridge extends AbstractLocalizationProvider {

	/**
	 * Implementation of {@link ILocalizationProvider}
	 */
	private ILocalizationProvider provider;
	
	/**
	 * {@link ILocalizationListener} that is connected to {@link ILocalizationProvider}
	 */
	private ILocalizationListener localization_listener = () -> this.fire();
	
	/**
	 * State of connection of {@link LocalizationProviderBridge#localization_listener}
	 */
	private boolean connected = false;
	
	/**
	 * Creates {@link LocalizationProviderBridge}
	 * @param provider is used implementation of {@link ILocalizationProvider}
	 */
	public LocalizationProviderBridge(ILocalizationProvider provider) {
		super();
		this.provider = provider;
	}

	/**
	 * Registers listener to implementation of {@link ILocalizationProvider}.
	 */
	public void connect() {
		if(connected) return;
		provider.addLocalizationListener(this.localization_listener);
		connected = true;
	}
	
	/**
	 * Removes listener from {@link ILocalizationProvider}
	 */
	public void disconnect() {
		if(!connected) return;
		provider.removeLocalizationListener(this.localization_listener);
		connected = false;
	}
	
	/**
	 * Delegates to {@link ILocalizationProvider#getString(String)}.
	 * @param key that is delegated 
	 */
	@Override
	public String getString(String key) {
		return provider.getString(key);
	}

}
