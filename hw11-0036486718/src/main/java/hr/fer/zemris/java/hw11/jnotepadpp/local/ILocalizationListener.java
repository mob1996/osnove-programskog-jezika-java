package hr.fer.zemris.java.hw11.jnotepadpp.local;

/**
 * Interface {@link ILocalizationListener} provides contract
 * for implementation  of {@link ILocalizationListener}s.
 * @author Luka Mijić
 *
 */
public interface ILocalizationListener {
	/**
	 * Method is called when localizstion is changed.
	 */
	void localizationChanged();
}
