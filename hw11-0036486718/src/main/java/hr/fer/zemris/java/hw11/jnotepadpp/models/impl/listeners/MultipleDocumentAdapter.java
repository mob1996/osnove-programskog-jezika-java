package hr.fer.zemris.java.hw11.jnotepadpp.models.impl.listeners;

import hr.fer.zemris.java.hw11.jnotepadpp.models.MultipleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.models.SingleDocumentModel;

/**
 * Adapter for {@link MultipleDocumentListener}.
 * Only purpose for this class is to enable creation of anonymous classes
 * that don't have to @Override all provided methods because their implementation is
 * empty.
 * @author Luka Mijić
 *
 */
public abstract class MultipleDocumentAdapter implements MultipleDocumentListener {

	@Override
	public void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel) {
	}

	@Override
	public void documentAdded(SingleDocumentModel model) {
	}

	@Override
	public void documentRemoved(SingleDocumentModel model) {
	}

}
