package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import javax.swing.AbstractAction;
import javax.swing.Action;

import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationListener;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import static hr.fer.zemris.java.hw11.jnotepadpp.util.ResourceBundleKeys.*;

/**
 * This class is abstract and it extends {@link AbstractAction}.
 * This class provides localisation functionality to {@link Action}s
 * that extend this class.
 * @author Luka Mijić
 *
 */
public abstract class LocalizableAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Localisation key
	 */
	private String key;
	
	/**
	 * Localisation provider
	 */
	private ILocalizationProvider lp;
	
	/**
	 * This constructor sets up {@link ILocalizationListener}
	 * that listens to language changes to given {@link ILocalizationProvider}.
	 * When change happens it triggers listener and it changes {@link Action}s
	 * {@link Action#NAME} and {@link Action#SHORT_DESCRIPTION}.
	 * @param key for localisation
	 * @param lp provider of localisation
	 */
	public LocalizableAction(String key, ILocalizationProvider lp) {
		super();
		this.key = key;
		this.lp = lp;
		this.putValue(NAME, this.lp.getString(this.key));
		this.putValue(SHORT_DESCRIPTION, this.lp.getString(this.key + DESCRIPTION));
		lp.addLocalizationListener(() -> {
			LocalizableAction.this.putValue(NAME, this.lp.getString(key));
			LocalizableAction.this.putValue(SHORT_DESCRIPTION, this.lp.getString(this.key + DESCRIPTION));
		});
		
	}

}
