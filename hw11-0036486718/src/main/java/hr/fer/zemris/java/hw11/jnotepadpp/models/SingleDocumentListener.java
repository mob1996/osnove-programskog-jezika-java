package hr.fer.zemris.java.hw11.jnotepadpp.models;

/**
 * Inteface models listener to {@link SingleDocumentModel}.
 * @author Luka Mijić
 *
 */
public interface SingleDocumentListener {
	
	/**
	 * Is called when {@link SingleDocumentModel} is modified.
	 * @param model that is modified
	 */
	void documentModifyStatusUpdated(SingleDocumentModel model);

	/**
	 * Is called when {@link SingleDocumentModel}s path is being updated.
	 * @param model whose path is being updated.
	 */
	void documentFilePathUpdated(SingleDocumentModel model);
}
