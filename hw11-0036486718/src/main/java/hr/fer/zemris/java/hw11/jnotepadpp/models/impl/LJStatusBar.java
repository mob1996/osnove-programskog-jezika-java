package hr.fer.zemris.java.hw11.jnotepadpp.models.impl;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;

import static hr.fer.zemris.java.hw11.jnotepadpp.util.ResourceBundleKeys.*;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationListener;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.models.MultipleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.models.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.models.SingleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.models.impl.listeners.MultipleDocumentAdapter;

/**
 * Class extends {@link JPanel} and is used as 
 * status bar that provides real-time informations of
 * {@link MultipleDocumentModel}.
 * @author Luka Mijić
 *
 */
public class LJStatusBar extends JPanel {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Localisation provider
	 */
	private ILocalizationProvider lp;

	/**
	 * Format for giving length of document status
	 */
	private String lenFormat;
	
	/**
	 * Format giving current line, column and number of
	 * selected characters status.
	 */
	private String currFormat;
	
	/**
	 * Time format
	 */
	private String timeFormat = "yyyy/MM/dd HH:mm:ss";
	
	/**
	 * Should clock be active
	 */
	private boolean clockActive = true;
	
	/**
	 * Length of document label
	 */
	private JLabel lenLabel;
	
	/**
	 * Current line, column and number of selected chars label
	 */
	private JLabel currLabel;
	
	/**
	 * Time label
	 */
	private JLabel timeLabel;
	
	/**
	 * Thread that updates clock
	 */
	private Thread clockThread = new Thread(() -> {
		while (true) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if(!clockActive) break;
			SwingUtilities.invokeLater(() -> updateTime());
		}
	}); 
	
	/**
	 * Listener that listens for changes in document lenght
	 */
	private DocumentListener lenListener = new DocumentListener() {
		@Override
		public void removeUpdate(DocumentEvent e) {
			lenLabel.setText(String.format(lenFormat, e.getDocument().getLength()));
		}
		
		@Override
		public void insertUpdate(DocumentEvent e) {
			lenLabel.setText(String.format(lenFormat, e.getDocument().getLength()));
		}
		
		@Override
		public void changedUpdate(DocumentEvent e) {
			lenLabel.setText(String.format(lenFormat, e.getDocument().getLength()));
		}
	};
	
	/**
	 * Listener that listens for changes in documents caret position
	 */
	private ChangeListener caretChange = new ChangeListener() {
		
		@Override
		public void stateChanged(ChangeEvent e) {
			SingleDocumentModel currModel = JNotepadPP.multipleModel.getCurrentDocument();
			if(currModel == null) return;
			
			JTextArea editor = currModel.getTextComponent();
			if(editor.getCaret() != e.getSource()) return;
			
			updateCurrentInfo(editor);
		}
	};
	
	/**
	 * Creates {@link LJStatusBar}.
	 * Initialises it's {@link LayoutManager}, creates {@link Border},
	 * and adds {@link WindowListener} to given {@link JFrame}
	 * that stops clock when {@link JFrame} closes.
	 * @param parent that registers {@link WindowListener}
	 * @param lp localisation provider
	 */
	public LJStatusBar(JFrame parent, ILocalizationProvider lp) {
		this.lp = lp;
		updateFormats();
		setLayout(new GridLayout(1, 3));
		setBorder(BorderFactory.createLineBorder(Color.black));
		parent.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				clockActive = false;
			}
		});
		
		initStatusBar();
	}
	
	/**
	 * Creates status bar by adding {@link JLabel} properties.
	 * It also calls {@link LJStatusBar#initListener()} and provides
	 * style for {@link LJStatusBar}.
	 */
	private void initStatusBar() {
		lenLabel = new JLabel(String.format(lenFormat, 0));
		currLabel = new JLabel(String.format(currFormat, 0, 0, 0));
		timeLabel = new JLabel();
		
		lenLabel.setBorder(BorderFactory.createLineBorder(Color.gray));
		currLabel.setBorder(BorderFactory.createLineBorder(Color.gray));
		timeLabel.setBorder(BorderFactory.createLineBorder(Color.gray));
		timeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		updateTime();
		
		clockThread.setDaemon(true);
		clockThread.start();
		
		initListener();
		
		this.add(lenLabel);
		this.add(currLabel);
		this.add(timeLabel);
	}
	
	/**
	 * Initialises that update and reset {@link JLabel} properties.
	 * First initialised listener is {@link MultipleDocumentListener} that
	 * resets value of {@link JLabel}s if all {@link SingleDocumentModel}s are removed 
	 * from him. That listeners also updates {@link JLabel}s when current
	 * {@link SingleDocumentModel} is changed.
	 * 
	 * Other listeners is {@link ILocalizationListener} that listens for localisation changes
	 * than updates formatting properties and then updates {@link JLabel}s.
	 */
	private void initListener() {
		JNotepadPP.multipleModel.addMultipleDocumentListener(new MultipleDocumentAdapter() {
			public void documentRemoved(SingleDocumentModel model) {
				if(JNotepadPP.multipleModel.getNumberOfDocuments() <= 0) {
					reset();
				}
			};
			
			public void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel) {
				if(previousModel != null) {
					previousModel.getTextComponent().getDocument().removeDocumentListener(lenListener);
					previousModel.getTextComponent().getCaret().removeChangeListener(caretChange);
				}
				if(currentModel == null) return;
				
				currentModel.getTextComponent().getDocument().addDocumentListener(lenListener);
				currentModel.getTextComponent().getCaret().addChangeListener(caretChange);
				lenLabel.setText(String.format(lenFormat, currentModel.getTextComponent().getDocument().getLength()));
				updateCurrentInfo(currentModel.getTextComponent());
			};
		});
		
		lp.addLocalizationListener(new ILocalizationListener() {
			@Override
			public void localizationChanged() {
				updateFormats();
				SingleDocumentModel model = JNotepadPP.multipleModel.getCurrentDocument();
				if(model == null) {
					reset();
					return;
				}
				updateCurrentInfo(model.getTextComponent());
				lenLabel.setText(String.format(lenFormat, model.getTextComponent().getDocument().getLength()));
			}
		});
	}
	
	/**
	 * Update formats for different localisation options.
	 */
	private void updateFormats() {
		lenFormat = lp.getString(STATUS_BAR_KEY + LEN_STATUS) + " %d";
		currFormat = lp.getString(STATUS_BAR_KEY + LINE_STATUS) + " %d  "
				     + lp.getString(STATUS_BAR_KEY + COL_STATUS) + " %d  "
				     + lp.getString(STATUS_BAR_KEY + SELECTED_STATUS) + "%d";
  	}
	
	/**
	 * Called by clock to update time.
	 */
	private void updateTime() {
		timeLabel.setText(LocalDateTime.now().format(DateTimeFormatter.ofPattern(timeFormat)));
	}
	
	/**
	 * Resets value of {@link JLabel} properties.
	 */
	private void reset() {
		lenLabel.setText(String.format(lenFormat, 0));
		currLabel.setText(String.format(currFormat, 0, 0, 0));
	}
	
	/**
	 * Updates current line, column and number of selected characters information.
	 * @param editor {@link JTextArea} that is used to extract those numbers.
	 */
	private void updateCurrentInfo(JTextArea editor) {
		int lineNum = 1;
		int colNum = 1;
		int selection = 0;
		
		int caretPos = editor.getCaretPosition();
		try {
			lineNum += editor.getLineOfOffset(caretPos);
			colNum = caretPos - editor.getLineStartOffset(lineNum - 1); //count from zero
			Caret c = editor.getCaret();
			selection = Math.abs(c.getDot() - c.getMark());
		} catch (BadLocationException e1) {
			e1.printStackTrace();
		}
		
		currLabel.setText(String.format(currFormat, lineNum, colNum, selection));
	}
}
