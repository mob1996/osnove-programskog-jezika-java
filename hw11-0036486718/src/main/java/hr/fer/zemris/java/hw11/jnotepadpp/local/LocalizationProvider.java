package hr.fer.zemris.java.hw11.jnotepadpp.local;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * {@link LocalizationProvider} extends {@link AbstractLocalizationProvider}.
 * Class uses Singleton pattern, meaning that only one instance of this class can
 * be created during programs lifetime.
 * Class uses {@link ResourceBundle} for accessing various language translations
 * from disk.
 * @author Luka Mijić
 *
 */
public class LocalizationProvider extends AbstractLocalizationProvider{

	/**
	 * Current bundle of language translations
	 */
	private ResourceBundle bundle;
	
	/**
	 * Path to language translations
	 */
	private final static String PACKAGE = LocalizationProvider.class.getPackage().getName();
	
	/**
	 * Singleton {@link LocalizationProvider}
	 */
	private static LocalizationProvider provider = null;
	
	/**
	 * Sets starting language to english.
	 */
	private LocalizationProvider() {
		setLanguage("en");
	}
	
	/**
	 * @return If {@link LocalizationProvider#provider} is null return new {@link LocalizationProvider},
	 * otherwise return {@link LocalizationProvider#provider}.
	 */
	public static LocalizationProvider getInstance() {
		provider = provider == null ? new LocalizationProvider() : provider;
		return provider;
	}
	
	/**
	 * Sets new language and notifies all interested listeners.
	 * @param lang is language that is being set.
	 */
	public void setLanguage(String lang) {
		bundle = ResourceBundle.getBundle(PACKAGE + ".translation", Locale.forLanguageTag(lang));
		fire();
	}

	/**
	 * @return translation using given key from {@link ResourceBundle} property.
	 */
	@Override
	public String getString(String key) {
		return bundle.getString(key);
	}
}
