package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.swing.JTextArea;
import javax.swing.text.BadLocationException;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.models.SingleDocumentModel;

/**
 * {@link UniqueAction} extends {@link LocalizableAction}.
 * It removes duplicate lines from all selected lines.
 * @author Luka Mijić
 *
 */
public class UniqueAction extends LocalizableAction {

	private static final long serialVersionUID = 1L;

	/**
	 * Creates {@link UniqueAction}.
	 * @param key for localisation 
	 * @param lp provider of localisation
	 */
	public UniqueAction(String key, ILocalizationProvider lp) {
		super(key, lp);
	}

	/**
	 * When triggered removes duplicate lines from all selected lines.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		SingleDocumentModel model = JNotepadPP.multipleModel.getCurrentDocument();
		if(model == null) return;
		
		
		JTextArea editor = model.getTextComponent();
		
		int start = Math.min(editor.getCaret().getDot(), editor.getCaret().getMark());
		int end = Math.max(editor.getCaret().getDot(), editor.getCaret().getMark());
		
		if(end - start == 0) return;
		
		try {
			int startLine = editor.getLineOfOffset(start);
			int endLine = editor.getLineOfOffset(end);
			
			start = editor.getLineStartOffset(startLine);
			end = editor.getLineEndOffset(endLine);
			String[] textToUnique = editor.getDocument().getText(start, end - start).split("\n");
			editor.getDocument().remove(start, end - start);
			
			Set<String> unique = new LinkedHashSet<>();
			StringBuilder builder = new StringBuilder();
			for(int i = 0; i < textToUnique.length; i++) {
				unique.add(textToUnique[i] + (end == editor.getDocument().getLength() - 1 ? "" : "\n"));
			}
			
			unique.forEach(s -> builder.append(s));
			editor.getDocument().insertString(start, builder.toString(), null);
		} catch (BadLocationException e1) {
			e1.printStackTrace();
		}
	}

}
