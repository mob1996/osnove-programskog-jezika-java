package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;
import java.util.function.Predicate;

import javax.swing.JOptionPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.models.SingleDocumentModel;

/**
 * {@link StatisticsAction} extends {@link LocalizableAction}.
 * It shows some basic statistics of {@link SingleDocumentModel}
 * in {@link JOptionPane}.
 * @author Luka Mijić
 *
 */
public class StatisticsAction extends LocalizableAction {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Checks if characters is non blank
	 */
	private static final Predicate<Character> NON_BLANKS = c -> !Character.isWhitespace(c);
	
	/**
	 * Checks if characters is new line character
	 */
	private static final Predicate<Character> NEXT_LINE = c -> c == '\n';
	
	/**
	 * Creates {@link StatisticsAction}.
	 * @param key for localisation 
	 * @param lp provider of localisation
	 */
	public StatisticsAction(String key, ILocalizationProvider lp) {
		super(key, lp);	
	}

	/**
	 * When triggered method calculates some basic statistics for currently
	 * used {@link SingleDocumentModel} and shows it using {@link JOptionPane}.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		SingleDocumentModel model = JNotepadPP.multipleModel.getCurrentDocument();
		if(model == null) {
			JOptionPane.showMessageDialog(JNotepadPP.multipleModel 
					, "No document selected for statistic calculation."
					, "Statistics",
					JOptionPane.INFORMATION_MESSAGE
					);
			return;
		}
		
		Document doc = model.getTextComponent().getDocument();
		
		String title = model.getFilePath() == null ? "document" : model.getFilePath().getFileName().toString();
		int len = doc.getLength();
		int nonBlanks = count(doc, NON_BLANKS);
		int lines = count(doc, NEXT_LINE) + 1;
		
		String message = "File '" + title + "' has " + len + " characters, " + nonBlanks
						+ " non-blank characters and " + lines + " lines";
		
		JOptionPane.showMessageDialog(JNotepadPP.multipleModel 
				, message
				, "Statistics",
				JOptionPane.INFORMATION_MESSAGE
				);
		
	}
	
	/**
	 * For each character in document, check if <code>predicate</code> is
	 * satisfied.
	 * @param doc {@link Document} that is being checked
	 * @param predicate test for characters
	 * @return number of characters that satisfy predicate.
	 */
	private int count(Document doc, Predicate<Character> predicate) {
		int count = 0;
		char[] docChars = null;
		try {
			docChars = doc.getText(0, doc.getLength()).toCharArray();
			for(int i = 0; i < docChars.length; i++) {
				if(predicate.test(docChars[i])) {
					count++;
				}
			}
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		return count;
	}
}
