package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.models.SingleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.models.impl.DefaultMultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.models.impl.listeners.SaveIconListener;

/**
 * {@link NewAction} extends {@link LocalizableAction}.
 * It creates new empty {@link SingleDocumentModel} in {@link DefaultMultipleDocumentModel}.
 * @author Luka Mijić
 *
 */
public class NewAction extends LocalizableAction {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Creates {@link NewAction}
	 * @param key for localisation 
	 * @param lp provider of localisation
	 */
	public NewAction(String key, ILocalizationProvider lp) {
		super(key, lp);
	}


	/**
	 * Creates new empty {@link SingleDocumentModel} in {@link DefaultMultipleDocumentModel}.
	 * @param e action event
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		SingleDocumentModel newDoc = JNotepadPP.multipleModel.createNewDocument();
		newDoc.addSingleDocumentListener(new SaveIconListener());
		newDoc.setModified(true);
	}

}
