package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;
import java.util.function.Function;

import javax.swing.JTextArea;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.models.SingleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.models.impl.DefaultMultipleDocumentModel;

/**
 * {@link CaseAction} extends {@link LocalizableAction}.
 * This action transforms selected text from {@link DefaultMultipleDocumentModel}
 * using transformer {@link Function}.
 * @author Luka Mijić
 *
 */
public class CaseAction extends LocalizableAction {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Character transformer
	 */
	private Function<Character, Character> transformer;
	
	/**
	 * Constructor of {@link CaseAction}.
	 * @param key for localisation 
	 * @param lp provider of localisation
	 * @param transformer for characters transformation
	 */
	public CaseAction(String key, ILocalizationProvider lp, Function<Character, Character> transformer) {
		super(key, lp);
		this.transformer = transformer;
	}

	/**
	 * Takes selected text from {@link DefaultMultipleDocumentModel} and transforms it using
	 * transformer {@link Function}. Selected text is replaced by transformed text.
	 * @param e action event
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		SingleDocumentModel model = JNotepadPP.multipleModel.getCurrentDocument();
		if(model == null) {
			return;
		}

		JTextArea editor = model.getTextComponent();
		Document doc = editor.getDocument();
		int len = Math.abs(editor.getCaret().getDot() - editor.getCaret().getMark());

		int offset = Math.min(editor.getCaret().getDot(), editor.getCaret().getMark());
		
		try {
			String text = doc.getText(offset, len);
			text = changeCase(text);
			doc.remove(offset, len);
			doc.insertString(offset, text, null);
		} catch (BadLocationException e1) {
			e1.printStackTrace();
		}
	}
	
	/**
	 * Takes text and transforms it using given 
	 * transformation {@link Function}.
	 * @param text that is transformed
	 * @return transformed text
	 */
	private String changeCase(String text) {
		char[] transformed = text.toCharArray();
		for(int i = 0; i < transformed.length; i++) {
			transformed[i] = transformer.apply(transformed[i]);
		}
		return new String(transformed);
	}

}
