package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;
import java.text.Collator;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Locale;

import javax.swing.JTextArea;
import javax.swing.text.BadLocationException;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.models.SingleDocumentModel;

/**
 * {@link SortAction} extends {@link LocalizableAction}.
 * It sorts selected lines using provided {@link Comparator}.
 * @author Luka Mijić
 *
 */
public class SortAction extends LocalizableAction {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Collator used for sorting in different languages
	 */
	private Collator collator;
	
	/**
	 * Basic {@link Comparator} that sorts in ascending order
	 */
	private final Comparator<String> ASCENDING = (s1, s2) -> collator.compare(s1, s2);
	
	/**
	 * Basing {@link Comparator} that sorts in descending order
	 */
	private final Comparator<String> DESCENDING = ASCENDING.reversed();
	
	/**
	 * Used comparator
	 */
	private Comparator<String> comp;
	
	/**
	 * Creates {@link SortAction}.
	 * @param key for localisation 
	 * @param lp provider of localisation
	 * @param ascending if true {@link SortAction#ASCENDING} is used,
	 * 					if false {@link SortAction#DESCENDING} is used.
	 */
	public SortAction(String key, ILocalizationProvider lp, boolean ascending) {
		super(key, lp);
		this.comp = ascending == true ? ASCENDING : DESCENDING;
		collator = Collator.getInstance(Locale.forLanguageTag(JNotepadPP.currentLang));
		lp.addLocalizationListener(() -> collator = Collator.getInstance(Locale.forLanguageTag(JNotepadPP.currentLang)));
	}

	/**
	 * When triggered method sorts selected lines replaces old
	 * order in document with new one. 
	 * @param e action event
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		SingleDocumentModel model = JNotepadPP.multipleModel.getCurrentDocument();
		if(model == null) return;
		
		
		JTextArea editor = model.getTextComponent();
		
		int start = Math.min(editor.getCaret().getDot(), editor.getCaret().getMark());
		int end = Math.max(editor.getCaret().getDot(), editor.getCaret().getMark());
		
		if(end - start == 0) return;
		
		try {
			int startLine = editor.getLineOfOffset(start);
			int endLine = editor.getLineOfOffset(end);
			
			start = editor.getLineStartOffset(startLine);
			end = editor.getLineEndOffset(endLine);
			String[] textToSort = editor.getDocument().getText(start, end - start).split("\n");
			editor.getDocument().remove(start, end - start);
			Arrays.sort(textToSort, comp);
			
			StringBuilder builder = new StringBuilder();
			for(int i = 0; i < textToSort.length; i++) {
				builder.append(textToSort[i]);
				if(end != editor.getDocument().getLength() - 1) {
					builder.append("\n");
				}
			}
			
			editor.getDocument().insertString(start, builder.toString(), null);
		} catch (BadLocationException e1) {
			e1.printStackTrace();
		}
		
	}

}
