package hr.fer.zemris.java.hw11.jnotepadpp.util;

import java.nio.file.Path;
import java.util.function.Function;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;

import hr.fer.zemris.java.hw11.jnotepadpp.models.SingleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.models.impl.DefaultMultipleDocumentModel;

/**
 * Class provides some basic utilities like save and exit.
 * @author Luka Mijić
 *
 */
public class JNotepadPPUtilities {

	/**
	 * {@link Function} that returns {@link SingleDocumentModel#getFilePath()}
	 */
	public static final Function<SingleDocumentModel, Path> GET_PATH = m -> m.getFilePath();
	
	/**
	 * {@link Function} that returns null for every {@link SingleDocumentModel}
	 */
	public static final Function<SingleDocumentModel, Path> GET_NULL = m -> null;
	
	
	/**
	 * Saves given model to {@link Path} received from <code>pathGetter</code> {@link Function}.
	 * If received {@link Path} is null, {@link JFileChooser} is created using <code>multipleModel</code>
	 * as parent. FileChooser is used for picking location for saving.
	 * @param multipleModel is {@link JTabbedPane} that is parent of {@link JFileChooser}
	 * @param model that is saved
	 * @param pathGetter {@link Path} to save location
	 */
	public static void save(DefaultMultipleDocumentModel multipleModel, SingleDocumentModel model,
			Function<SingleDocumentModel, Path> pathGetter) {
		Path savePath = pathGetter.apply(model);
		if (savePath == null) {
			JFileChooser fc = new JFileChooser();
			fc.setDialogTitle("Save file");
			if (fc.showOpenDialog(multipleModel) != JFileChooser.APPROVE_OPTION) {
				return;
			}

			savePath = fc.getSelectedFile().toPath();
		}

		multipleModel.saveDocument(model, savePath);
	}
	
	/**
	 * Calls {@link JFrame#dispose()} on given {@link JFrame} if there are no
	 * modified {@link SingleDocumentModel}s in given {@link DefaultMultipleDocumentModel}.
	 * If there are modified {@link SingleDocumentModel}s, then user is asked if he wishes
	 * to save it. User can also cancel disposing of {@link JFrame}.
	 * @param frame that is being disposed
	 * @param multipleModel is used to iterate over {@link SingleDocumentModel}s and
	 * 						ask user if he wishes to save changes.
	 */
	public static void exit(JFrame frame, DefaultMultipleDocumentModel multipleModel) {
		int i = 0;
		for (SingleDocumentModel model : multipleModel) {
			i++;
			if (!model.isModified())
				continue;
			int result = JOptionPane.showConfirmDialog(multipleModel
					, "Save '" + multipleModel.getTitleAt(i - 1) + "'?",
					i + "/" + multipleModel.getNumberOfDocuments()
					, JOptionPane.YES_NO_CANCEL_OPTION);
			if (result == JOptionPane.CANCEL_OPTION) {
				return;
			} else if (result == JOptionPane.YES_OPTION) {
				JNotepadPPUtilities.save(multipleModel, model, JNotepadPPUtilities.GET_PATH);
			}
			
		}
		frame.dispose();
	}
}
