package hr.fer.zemris.java.hw11.jnotepadpp.util.icons;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import static hr.fer.zemris.java.hw11.jnotepadpp.util.ResourcePaths.*;
import javax.swing.ImageIcon;

/**
 * Utility class that stores {@link ImageIcon} used throughout
 * the application. All {@link ImageIcon} icons are static so
 * they don't have to be created whenever program requires icon.
 * @author Luka Mijić
 *
 */
public class ImageIcons {

	/**
	 * Various icons, names are self explanatory.
	 */
	public static ImageIcon MODIFIED = createIcon(MODIFIED_ICON_PATH);
	public static ImageIcon UNMODIFIED = createIcon(UNMODIFIED_ICON_PATH);
	public static ImageIcon CLOSE = createIcon(CLOSE_ICON_PATH);
	public static ImageIcon COPY = createIcon(COPY_ICON_PATH);
	public static ImageIcon CUT = createIcon(CUT_ICON_PATH);
	public static ImageIcon EXIT = createIcon(EXIT_ICON_PATH);
	public static ImageIcon NEW = createIcon(NEW_ICON_PATH);
	public static ImageIcon OPEN = createIcon(OPEN_ICON_PATH);
	public static ImageIcon PASTE = createIcon(PASTE_ICON_PATH);
	public static ImageIcon SAVE = createIcon(SAVE_ICON_PATH);
	public static ImageIcon SAVE_AS = createIcon(SAVE_AS_ICON_PATH);
	public static ImageIcon STATS = createIcon(STATS_ICON_PATH);
	public static ImageIcon NOTEPAD = createIcon(NOTEPAD_ICON_PATH);
	public static ImageIcon LOWER_CASE = createIcon(LOWER_CASE_ICON_PATH);
	public static ImageIcon UPPER_CASE = createIcon(UPPER_CASE_ICON_PATH);
	public static ImageIcon INVERSE_CASE = createIcon(INVERSE_CASE_ICON_PATH);
	public static ImageIcon UK = createIcon(UK_ICON_PATH);
	public static ImageIcon CROATIA = createIcon(CROATIA_ICON_PATH);
	public static ImageIcon GERMANY = createIcon(GERMANY_ICON_PATH);
	
	/**
	 * Creates {@link ImageIcon} from given {@link Path}
	 * while using {@link Class#getResourceAsStream(String)} method.
	 * @param path of {@link ImageIcon}
	 * @return created {@link ImageIcon}
	 */
	private static ImageIcon createIcon(Path path) {
		String pathStr = path.getFileName().toString();
		try (InputStream is = ImageIcons.class.getResourceAsStream(pathStr)){
			if(is == null) {
				throw new IllegalArgumentException("Invalid path.");
			}
			byte[] bytes = is.readAllBytes();
			
			is.close();
			return new ImageIcon(bytes);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
