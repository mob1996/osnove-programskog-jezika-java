package hr.fer.zemris.java.hw11.jnotepadpp.models.impl.listeners;

import java.nio.file.Path;

import javax.swing.ImageIcon;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.models.SingleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.util.icons.ImageIcons;

/**
 * {@link SaveIconListener} extends {@link SingleDocumentAdapter}.
 * It listens to changes in {@link SingleDocumentModel} modification
 * and in change in {@link Path} stored in {@link SingleDocumentModel}.
 * @author Luka Mijić
 *
 */
public class SaveIconListener extends SingleDocumentAdapter {

	/**
	 * Changes tab icon depending on is modified flag set on true or false.
	 * @param model that was changed
	 */
	@Override
	public void documentModifyStatusUpdated(SingleDocumentModel model) {
		ImageIcon icon = model.isModified() ? ImageIcons.MODIFIED : ImageIcons.UNMODIFIED;
		JNotepadPP.multipleModel.setIconAt(JNotepadPP.multipleModel.getSelectedIndex(), icon);
	}

	/**
	 * Changes tab tool tip to new {@link Path#toAbsolutePath()} stored in
	 * {@link SingleDocumentModel}
	 * @param model that was changed
	 */
	@Override
	public void documentFilePathUpdated(SingleDocumentModel model) {
		String title = model.getFilePath() == null ? "Document" : model.getFilePath().getFileName().toString();
		String fullPath = model.getFilePath() == null ? "" : model.getFilePath().toAbsolutePath().toString();
		JNotepadPP.multipleModel.setTitleAt(JNotepadPP.multipleModel.getSelectedIndex(), title);
		JNotepadPP.multipleModel.setToolTipTextAt(JNotepadPP.multipleModel.getSelectedIndex(), fullPath);
	}
}
