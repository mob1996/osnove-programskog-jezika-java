package hr.fer.zemris.java.hw11.jnotepadpp;

import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.DefaultEditorKit;

import hr.fer.zemris.java.hw11.jnotepadpp.actions.CaseAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.ChangeLangAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.CloseAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.ExitAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.NewAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.OpenAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.SaveAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.SaveAsAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.SortAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.StatisticsAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.UniqueAction;
import hr.fer.zemris.java.hw11.jnotepadpp.local.FormLocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationListener;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.models.SingleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.models.SingleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.models.impl.DefaultMultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.models.impl.LJStatusBar;
import hr.fer.zemris.java.hw11.jnotepadpp.models.impl.listeners.EnableListeners;
import hr.fer.zemris.java.hw11.jnotepadpp.models.impl.listeners.MultipleDocumentAdapter;
import hr.fer.zemris.java.hw11.jnotepadpp.models.impl.listeners.SingleDocumentAdapter;
import hr.fer.zemris.java.hw11.jnotepadpp.util.JNotepadPPUtilities;
import hr.fer.zemris.java.hw11.jnotepadpp.util.icons.ImageIcons;

import static hr.fer.zemris.java.hw11.jnotepadpp.util.ResourceBundleKeys.*;

/**
 * Simple editor program that provides multiple functionalities like
 * editing multiple documents at same time, saving those documents,
 * loading documents from disk, sorting lines, etc.
 * Program is also supports multiple languages.
 * Currently supported languages are croatian, english and german.
 * @author Luka Mijić
 *
 */
public class JNotepadPP extends JFrame {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Model that stores multiple document models
	 */
	public static DefaultMultipleDocumentModel multipleModel = new DefaultMultipleDocumentModel();
	
	/**
	 * Currently used language
	 */
	public static String currentLang = "en";
	
	/**
	 * Decorator fro {@link LocalizationProvider}
	 */
	private FormLocalizationProvider flp = new FormLocalizationProvider(LocalizationProvider.getInstance(), this);

	/**
	 * Action used for opening new documents
	 */
	private Action openAction;
	
	/**
	 * Action used for creating empty document
	 */
	private Action newAction;
	
	/**
	 * Action closes current document
	 */
	private Action closeAction;
	
	/**
	 * Action saves current document
	 */
	private Action saveAction;
	
	/**
	 * Action saves current document, user is
	 * prompted to provide save location
	 */
	private Action saveAsAction;
	
	/**
	 * Action shuts down application
	 */
	private Action exitAction;
	
	/**
	 * Action copies selected text
	 */
	private Action copyAction;
	
	/**
	 * Action cuts selected text
	 */
	private Action cutAction;
	
	/**
	 * Action pastes saved text
	 */
	private Action pasteAction;
	
	/**
	 * Action shows some informations about document
	 */
	private Action statsAction;
	
	/**
	 * Action transforms selected text into lower case
	 */
	private Action toLowerCaseAction;
	
	/**
	 * Action transforms selected text into upper cases
	 */
	private Action toUpperCaseAction;
	
	/**
	 * Action inverses cases of selected text
	 */
	private Action inverseCaseAction;
	
	/**
	 * Action sorts selected lines in Ascending order
	 */
	private Action sortAscAction;
	
	/**
	 * Action sorts selected lines in descending order
	 */
	private Action sortDescAction;
	
	/**
	 * Action removes duplicates from selected lines
	 */
	private Action uniqueAction;
	
	/**
	 * Action sets english as current language
	 */
	private Action ukAction;
	
	/**
	 * Action sets croatian as current language
	 */
	private Action croAction;
	
	/**
	 * Action sets german as current language
	 */
	private Action gerAction;
	
	/**
	 * Main panel that holds documents
	 */
	private JPanel mainPanel;
	
	/**
	 * Creates {@link JNotepadPP}
	 */
	public JNotepadPP() {
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setTitle("JNotepad++");
		setIconImage(ImageIcons.NOTEPAD.getImage());
		initExitSequence();
		initGUI();
	}

	/**
	 * Adds window listener that initiates {@link JNotepadPPUtilities#exit(JFrame, DefaultMultipleDocumentModel)}
	 * sequence on {@link WindowAdapter#windowClosing(WindowEvent)} event. 
	 */
	private void initExitSequence() {
		WindowAdapter closingAdapter = new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				JNotepadPPUtilities.exit(JNotepadPP.this, multipleModel);
			}
		};

		addWindowListener(closingAdapter);
	}

	/**
	 * Initialises GUI of application.
	 * Creates main panel and adds {@link DefaultMultipleDocumentModel} to {@link BorderLayout#CENTER}.
	 * It initialises actions, creates {@link JMenuBar}, and {@link JToolBar} and {@link LJStatusBar}.
	 */
	private void initGUI() {
		mainPanel = new JPanel(new BorderLayout());
		mainPanel.add(multipleModel, BorderLayout.CENTER);
		getContentPane().add(mainPanel, BorderLayout.CENTER);

		createActions();
		createMenus();
		createToolBar();
		
		mainPanel.add(new LJStatusBar(this, flp), BorderLayout.SOUTH);
	}

	/**
	 * Initialises actions and connects appropriate listeners.
	 * For example <code>uniqueAction</code> shouldn't be enabled when
	 * no text is selected.
	 */
	private void createActions() {
		this.openAction = new OpenAction(OPEN_KEY, flp);
		this.openAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control O"));
		this.openAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_O);
		this.openAction.putValue(Action.LARGE_ICON_KEY, ImageIcons.OPEN);

		this.newAction = new NewAction(NEW_KEY, flp);
		this.newAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control N"));
		this.newAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_N);
		this.newAction.putValue(Action.LARGE_ICON_KEY, ImageIcons.NEW);

		this.closeAction = new CloseAction(CLOSE_KEY, flp);
		this.closeAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control Y"));
		this.closeAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_C);
		this.closeAction.putValue(Action.LARGE_ICON_KEY, ImageIcons.CLOSE);
		this.closeAction.setEnabled(false);
		

		this.saveAction = new SaveAction(SAVE_KEY, flp);
		this.saveAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control S"));
		this.saveAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_S);
		this.saveAction.putValue(Action.LARGE_ICON_KEY, ImageIcons.SAVE);
		this.saveAction.setEnabled(false);
	
		SingleDocumentListener modifiedListener = new SingleDocumentAdapter() {
			@Override
			public void documentModifyStatusUpdated(SingleDocumentModel model) {
				JNotepadPP.this.saveAction.setEnabled(model.isModified());
			}
		};
		multipleModel.addMultipleDocumentListener(new MultipleDocumentAdapter() {
			@Override
			public void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel) {
				if (previousModel != null) {
					previousModel.removeSingleDocumentListener(modifiedListener);
				}
				if (currentModel == null) {
					JNotepadPP.this.saveAction.setEnabled(false);
					return;
				}
				currentModel.addSingleDocumentListener(modifiedListener);
				JNotepadPP.this.saveAction.setEnabled(currentModel.isModified());
			}

			@Override
			public void documentRemoved(SingleDocumentModel model) {
				if (JNotepadPP.multipleModel.getNumberOfDocuments() <= 0) {
					JNotepadPP.this.saveAction.setEnabled(false);
				}
			}
		});

		this.saveAsAction = new SaveAsAction(SAVE_AS_KEY, flp);
		this.saveAsAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control A"));
		this.saveAsAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_A);
		this.saveAsAction.setEnabled(false);
		this.saveAsAction.putValue(Action.LARGE_ICON_KEY, ImageIcons.SAVE_AS);

		this.exitAction = new ExitAction(EXIT_KEY, flp, this);
		this.exitAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control E"));
		this.exitAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_E);
		this.exitAction.putValue(Action.LARGE_ICON_KEY, ImageIcons.EXIT);

		this.copyAction = new DefaultEditorKit.CopyAction();
		this.copyAction.putValue(Action.NAME, flp.getString(COPY_KEY));
		this.copyAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control C"));
		this.copyAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_C);
		this.copyAction.putValue(Action.SHORT_DESCRIPTION, flp.getString(COPY_KEY + DESCRIPTION));
		this.copyAction.putValue(Action.LARGE_ICON_KEY, ImageIcons.COPY);
		this.copyAction.setEnabled(false);

		this.cutAction = new DefaultEditorKit.CutAction();
		this.cutAction.putValue(Action.NAME, flp.getString(CUT_KEY));
		this.cutAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control X"));
		this.cutAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_U);
		this.cutAction.putValue(Action.SHORT_DESCRIPTION, flp.getString(CUT_KEY + DESCRIPTION));
		this.cutAction.putValue(Action.LARGE_ICON_KEY, ImageIcons.CUT);
		this.cutAction.setEnabled(false);

		this.pasteAction = new DefaultEditorKit.PasteAction();
		this.pasteAction.putValue(Action.NAME, flp.getString(PASTE_KEY));
		this.pasteAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control V"));
		this.pasteAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_P);
		this.pasteAction.putValue(Action.SHORT_DESCRIPTION, flp.getString(PASTE_KEY + DESCRIPTION));
		this.pasteAction.putValue(Action.LARGE_ICON_KEY, ImageIcons.PASTE);
		this.pasteAction.setEnabled(false);
		
		flp.addLocalizationListener(new ILocalizationListener() {
			@Override
			public void localizationChanged() {
				JNotepadPP.this.copyAction.putValue(Action.NAME, flp.getString(COPY_KEY));
				JNotepadPP.this.copyAction.putValue(Action.SHORT_DESCRIPTION, flp.getString(COPY_KEY + DESCRIPTION));
				JNotepadPP.this.cutAction.putValue(Action.NAME, flp.getString(CUT_KEY));
				JNotepadPP.this.cutAction.putValue(Action.SHORT_DESCRIPTION, flp.getString(CUT_KEY + DESCRIPTION));
				JNotepadPP.this.pasteAction.putValue(Action.NAME, flp.getString(PASTE_KEY));
				JNotepadPP.this.pasteAction.putValue(Action.SHORT_DESCRIPTION, flp.getString(PASTE_KEY + DESCRIPTION));
			}
		});

		this.statsAction = new StatisticsAction(STATS_KEY, flp);
		this.statsAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control T"));
		this.statsAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_T);
		this.statsAction.putValue(Action.LARGE_ICON_KEY, ImageIcons.STATS);
		this.statsAction.setEnabled(false);
		
		this.toLowerCaseAction = new CaseAction(LOWER_CASE_KEY, flp, c -> Character.toLowerCase(c));
		this.toLowerCaseAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control L"));
		this.toLowerCaseAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_L);
		this.toLowerCaseAction.putValue(Action.LARGE_ICON_KEY, ImageIcons.LOWER_CASE);
		this.toLowerCaseAction.setEnabled(false);
		
		this.toUpperCaseAction = new CaseAction(UPPER_CASE_KEY, flp, c -> Character.toUpperCase(c));
		this.toUpperCaseAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control U"));
		this.toUpperCaseAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_U);
		this.toUpperCaseAction.putValue(Action.LARGE_ICON_KEY, ImageIcons.UPPER_CASE);
		this.toUpperCaseAction.setEnabled(false);
		
		this.inverseCaseAction = new CaseAction(INVERSE_CASE_KEY, flp, c ->{
			if(Character.isUpperCase(c)) {
				return Character.toLowerCase(c);
			} else if(Character.isLowerCase(c)) {
				return Character.toUpperCase(c);
			}
			return c;
		});
		this.inverseCaseAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control I"));
		this.inverseCaseAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_I);
		this.inverseCaseAction.putValue(Action.LARGE_ICON_KEY, ImageIcons.INVERSE_CASE);
		this.inverseCaseAction.setEnabled(false);
		
		this.sortAscAction = new SortAction(SORT_ASC_KEY, flp, true);
		this.sortAscAction.setEnabled(false);
		this.sortDescAction = new SortAction(SORT_DESC_KEY, flp, false);
		this.sortDescAction.setEnabled(false);
		this.uniqueAction = new UniqueAction(UNIQUE_KEY, flp);
		
		//Action that require selected text for them to be enabled
		Action[] selectionActions = { JNotepadPP.this.toLowerCaseAction
									, JNotepadPP.this.toUpperCaseAction
									, JNotepadPP.this.inverseCaseAction
									, JNotepadPP.this.sortAscAction
									, JNotepadPP.this.sortDescAction
									, JNotepadPP.this.uniqueAction
									};
		ChangeListener selectedListener = new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				SingleDocumentModel currModel = multipleModel.getCurrentDocument();
				if(currModel == null) return;
				
				JTextArea editor = currModel.getTextComponent();
				if(editor.getCaret() != e.getSource()) return;
				
				boolean enable;
				if(editor.getCaret().getDot() - editor.getCaret().getMark() == 0) {
					enable = false;
				} else {
					enable = true;
				}
				

				for(Action s:selectionActions) {
					s.setEnabled(enable);
				}
			}
		};
		
		this.ukAction = new ChangeLangAction(UK_KEY, flp, ChangeLangAction.ENGLISH);
		this.croAction = new ChangeLangAction(CRO_KEY, flp, ChangeLangAction.CROATIAN);
		this.gerAction = new ChangeLangAction(GER_KEY, flp, ChangeLangAction.GERMAN);
		
		this.ukAction.putValue(Action.SMALL_ICON, ImageIcons.UK);
		this.croAction.putValue(Action.SMALL_ICON, ImageIcons.CROATIA);
		this.gerAction.putValue(Action.SMALL_ICON, ImageIcons.GERMANY);
		
		multipleModel.addMultipleDocumentListener(new MultipleDocumentAdapter() {
			@Override
			public void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel) {
				if(previousModel != null) {
					previousModel.getTextComponent().getCaret().removeChangeListener(selectedListener);
				}
				boolean enable;
				if(currentModel == null) {
					enable = false;
				} else {
					currentModel.getTextComponent().getCaret().addChangeListener(selectedListener);
					enable = currentModel.getTextComponent().getSelectedText() == null ? false : true;
				}
				
				for(Action s:selectionActions) {
					s.setEnabled(enable);
				}
			}
		});
		
		multipleModel.addMultipleDocumentListener(
				new EnableListeners(this.closeAction
									, this.saveAsAction
									, this.statsAction
									, this.copyAction
									, this.cutAction
									, this.pasteAction));
	}

	/**
	 * Initialises applications menu for application.
	 * {@link JMenuBar} provides multiple {@link JMenu}s, like
	 * "File", "Edit" etc.
	 */
	private void createMenus() {
		JMenuBar menuBar = new JMenuBar();

		JMenu fileMenu = new JMenu(flp.getString(FILE_KEY));

		fileMenu.add(new JMenuItem(this.newAction));
		fileMenu.add(new JMenuItem(this.openAction));
		fileMenu.addSeparator();
		fileMenu.add(new JMenuItem(this.closeAction));
		fileMenu.addSeparator();
		fileMenu.add(new JMenuItem(this.saveAction));
		fileMenu.add(new JMenuItem(this.saveAsAction));
		fileMenu.addSeparator();
		fileMenu.add(new JMenuItem(this.exitAction));

		JMenu editMenu = new JMenu(flp.getString(EDIT_KEY));
		editMenu.add(new JMenuItem(this.copyAction));
		editMenu.add(new JMenuItem(this.cutAction));
		editMenu.add(new JMenuItem(this.pasteAction));
		editMenu.addSeparator();
		editMenu.add(new JMenuItem(this.toLowerCaseAction));
		editMenu.add(new JMenuItem(this.toUpperCaseAction));
		editMenu.add(new JMenuItem(this.inverseCaseAction));

		JMenu sortMenu = new JMenu(flp.getString(SORT_KEY));
		sortMenu.add(new JMenuItem(this.sortAscAction));
		sortMenu.add(new JMenuItem(this.sortDescAction));
		sortMenu.add(new JMenuItem(this.uniqueAction));
		
		JMenu langMenu = new JMenu(flp.getString(LANG_KEY));
		langMenu.add(new JMenuItem(this.ukAction));
		langMenu.add(new JMenuItem(this.croAction));
		langMenu.add(new JMenuItem(this.gerAction));
		
		JMenu infoMenu = new JMenu(flp.getString(INFO_KEY));
		infoMenu.add(this.statsAction);

		menuBar.add(fileMenu);
		menuBar.add(editMenu);
		menuBar.add(sortMenu);
		menuBar.add(langMenu);
		menuBar.add(infoMenu);
		
		flp.addLocalizationListener(new ILocalizationListener() {
			@Override
			public void localizationChanged() {
				fileMenu.setText(flp.getString(FILE_KEY));
				editMenu.setText(flp.getString(EDIT_KEY));
				langMenu.setText(flp.getString(LANG_KEY));
				sortMenu.setText(flp.getString(SORT_KEY));
				infoMenu.setText(flp.getString(INFO_KEY));
			}
		});

		setJMenuBar(menuBar);
	}

	/**
	 * Creates floatable {@link JToolBar}.
	 * Starting position is below {@link JMenuBar}
	 */
	private void createToolBar() {
		JToolBar toolBar = new JToolBar();
		toolBar.setFloatable(true);
		
		toolBar.add(new JButton(this.newAction));
		toolBar.add(new JButton(this.openAction));
		toolBar.add(new JButton(this.saveAction));
		toolBar.add(new JButton(this.saveAsAction));
		toolBar.add(new JButton(this.closeAction));
		toolBar.add(new JButton(this.copyAction));
		toolBar.add(new JButton(this.cutAction));
		toolBar.add(new JButton(this.pasteAction));
		toolBar.add(new JButton(this.toLowerCaseAction));
		toolBar.add(new JButton(this.toUpperCaseAction));
		toolBar.add(new JButton(this.inverseCaseAction));
		toolBar.add(new JButton(this.statsAction));
		toolBar.add(new JButton(this.exitAction));
		
		getContentPane().add(toolBar, BorderLayout.PAGE_START);
	}

	/**
	 * Starting point of application.
	 * No arguments are required.
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new JNotepadPP().setVisible(true));

	}

}
