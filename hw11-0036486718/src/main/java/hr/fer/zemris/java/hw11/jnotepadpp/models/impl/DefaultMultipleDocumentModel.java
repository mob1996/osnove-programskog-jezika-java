package hr.fer.zemris.java.hw11.jnotepadpp.models.impl;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import hr.fer.zemris.java.hw11.jnotepadpp.models.MultipleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.models.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.models.SingleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.models.SingleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.util.icons.ImageIcons;

/**
 * Class implements {@link MultipleDocumentModel} and extends {@link JTabbedPane}.
 * It stores {@link List} of {@link SingleDocumentModel}s, {@link List} of 
 * {@link MultipleDocumentListener}s. It knows which {@link SingleDocumentModel} is
 * currently showed to user.
 */
public class DefaultMultipleDocumentModel extends JTabbedPane implements MultipleDocumentModel {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * BiConsumer that calls {@link MultipleDocumentListener#currentDocumentChanged(SingleDocumentModel, SingleDocumentModel)}
	 * on given {@link MultipleDocumentListener} and provides {@link SingleDocumentModel} as argument.
	 */
	private final BiConsumer<MultipleDocumentListener, SingleDocumentModel> 
							UPDATE_CURRENT = (l, previous) -> l.currentDocumentChanged(previous, getCurrentDocument());
	
	/**
	 * BiConsumer that calls {@link MultipleDocumentListener#documentAdded(SingleDocumentModel)}
	 * on given {@link MultipleDocumentListener} and provides {@link SingleDocumentModel} as argument.
	 */
	private static final BiConsumer<MultipleDocumentListener, SingleDocumentModel> 
							ADDED = (l, doc) -> l.documentAdded(doc);
							
	/**
	 * BiConsumer that calls {@link MultipleDocumentListener#documentRemoved(SingleDocumentModel)}
	 * on given {@link MultipleDocumentListener} and provides {@link SingleDocumentModel} as argument.
	 */
	private static final BiConsumer<MultipleDocumentListener, SingleDocumentModel>
							REMOVED = (l, doc) -> l.documentRemoved(doc);
	
	/**
	 * {@link List} of stored {@link SingleDocumentModel}s.
	 */
	private List<SingleDocumentModel> documentModels;
	
	/**
	 * {@link List} of stored {@link MultipleDocumentListener}s.
	 */
	private List<MultipleDocumentListener> listeners;
	
	/**
	 * Creates {@link DefaultMultipleDocumentModel}.
	 * Initialises {@link DefaultMultipleDocumentModel#listeners}
	 * and {@link DefaultMultipleDocumentModel#documentModels}.
	 */
	public DefaultMultipleDocumentModel() {
		this.documentModels = new ArrayList<>();
		this.listeners = new ArrayList<>();
		
		this.addChangeListener(c -> fire(UPDATE_CURRENT, getCurrentDocument()));
	}
	
	/**
	 * Creates new {@link DefaultMultipleDocumentModel} and sets
	 * {@link Path} argument to null and text argument to empty {@link String}.
	 * @return creates {@link SingleDocumentModel}
	 */
	@Override
	public SingleDocumentModel createNewDocument() {
		SingleDocumentModel doc = new DefaultSingleDocumentModel(null, "");
		addAndSwapSelected(doc);
		return doc;
	}

	/**
	 * @return current {@link SingleDocumentModel}. If it doen't exist return null.
	 */
	@Override
	public SingleDocumentModel getCurrentDocument() {
		int index = this.getSelectedIndex();
		return index == -1 ? null : documentModels.get(index);
	}

	/**
	 * Loads {@link String} from given {@link Path} using {@link StandardCharsets#UTF_8}.
	 * Creates {@link SingleDocumentModel} and it uses given {@link Path} and {@link String}
	 * from reading file as arguments for constructor.
	 * @return creates {@link SingleDocumentModel}.
	 */
	@Override
	public SingleDocumentModel loadDocument(Path path) {
		Objects.requireNonNull(path, "Given path must not be null.");
		
		if(!Files.isReadable(path)) {
			JOptionPane.showMessageDialog(
					this,
					"File " + path.toAbsolutePath() + " is not readable!",
					"Error",
					JOptionPane.ERROR_MESSAGE);
			return null;
		}
		
		SingleDocumentModel prev = getCurrentDocument();
		for(int i = 0; i < documentModels.size(); i++) {
			try {
				Path otherPath = documentModels.get(i).getFilePath();
				if(otherPath == null) continue;
				
				if(Files.isSameFile(path, documentModels.get(i).getFilePath())) {
					this.setSelectedIndex(i);
					fire(UPDATE_CURRENT, prev);
					return documentModels.get(i);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		byte[] octets;
		try {
			octets = Files.readAllBytes(path);
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(
					this,
					"Error while reading file " + path.toAbsolutePath(),
					"Error",
					JOptionPane.ERROR_MESSAGE
					);
			return null;
		}
		
		String text = new String(octets, StandardCharsets.UTF_8);
		SingleDocumentModel doc = new DefaultSingleDocumentModel(path, text);
		
		addAndSwapSelected(doc);
		
		return doc;
	}

	/**
	 * Saves text from given {@link SingleDocumentModel} to given {@link Path}.
	 * If given {@link Path} is null it uses {@link SingleDocumentModel#getFilePath()}.
	 * If that is also null {@link JOptionPane} is used to convey error message.
	 */
	@Override
	public void saveDocument(SingleDocumentModel model, Path newPath) {
		newPath = newPath == null ? model.getFilePath() : newPath;
		byte[] data = model.getTextComponent().getText().getBytes(StandardCharsets.UTF_8);
		
		try {
			Files.write(newPath, data);
		} catch(IOException exc) {
			JOptionPane.showMessageDialog(this
					, "Error while saving: " + newPath.toAbsolutePath()
					, "Error", JOptionPane.ERROR_MESSAGE
					);
			return;
		}
		
		JOptionPane.showMessageDialog(this
					, "File Saved "
					,"Info"
					, JOptionPane.INFORMATION_MESSAGE
					);
		
		model.setModified(false);
		model.setFilePath(newPath);
	}

	/**
	 * Removes given {@link SingleDocumentModel} from {@link List} of
	 * models and removes tab associated with given model.
	 * If given model is not in {@link List} nothing happens.
	 * If given model is also current model, current model is changed
	 * and listeners are modified of current model change.
	 * Listeners are modified of removed model change.
	 */
	@Override
	public void closeDocument(SingleDocumentModel model) {
		int index = this.documentModels.indexOf(model);
		boolean changedCurrent = this.getSelectedIndex() == index;
		
		if(index != -1) {
			this.documentModels.remove(index);
			this.removeTabAt(index);
			
			if(changedCurrent) {
				fire(UPDATE_CURRENT, model);
			}
			fire(REMOVED, model);
		}
	}

	/**
	 * Adds {@link MultipleDocumentListener} to {@link List} of models.
	 * @param l is listener that is added.
	 */
	@Override
	public void addMultipleDocumentListener(MultipleDocumentListener l) {
		listeners.add(l);
	}

	/**
	 * Removes {@link MultipleDocumentListener} from {@link List} of models.
	 * @param l is listener that is removed.
	 */
	@Override
	public void removeMultipleDocumentListener(MultipleDocumentListener l) {
		listeners.remove(l);
	}

	/**
	 * @return number of {@link SingleDocumentModel}s in {@link List} of models
	 */
	@Override
	public int getNumberOfDocuments() {
		return this.documentModels.size();
	}

	/**
	 * @return {@link SingleDocumentModel} at given index.
	 */
	@Override
	public SingleDocumentModel getDocument(int index) {
		return this.documentModels.get(index);
	}

	/**
	 * Calls {@link BiConsumer} for each {@link SingleDocumentListener} and given {@link SingleDocumentModel}.
	 * @param biConsumer notifies {@link SingleDocumentListener} of change
	 * @param doc that was changed
	 */
	private void fire(BiConsumer<MultipleDocumentListener, SingleDocumentModel> biConsumer, SingleDocumentModel doc) {
		listeners.forEach(l -> biConsumer.accept(l, doc));
	}


	/**
	 * @return {@link List#iterator()} of list that stores models.
	 */
	@Override
	public Iterator<SingleDocumentModel> iterator() {
		return documentModels.iterator();
	}
	
	/**
	 * Adds {@link SingleDocumentModel} to {@link List}, creates new tab
	 * and connects {@link SingleDocumentModel} to given tab and sets
	 * new tab as selected tab.
	 * @param model that is added.
	 */
	private void addAndSwapSelected(SingleDocumentModel model) {
		SingleDocumentModel prev = getCurrentDocument();
		documentModels.add(model);
		
		String title = model.getFilePath() == null ? "Document" : model.getFilePath().getFileName().toString();
		this.addTab(title, new JScrollPane(model.getTextComponent()));
		this.setSelectedIndex(documentModels.size() - 1);
		ImageIcon icon = model.isModified() ? ImageIcons.MODIFIED : ImageIcons.UNMODIFIED;
		this.setIconAt(this.getSelectedIndex(), icon);
		this.setToolTipTextAt(this.getSelectedIndex(), 
				model.getFilePath() == null ? "" : model.getFilePath().toAbsolutePath().toString());
		
		fire(UPDATE_CURRENT, prev);
		fire(ADDED, getCurrentDocument());
	}
}
