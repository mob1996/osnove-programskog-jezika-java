package hr.fer.zemris.java.hw11.jnotepadpp.models.impl.listeners;

import javax.swing.Action;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.models.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.models.SingleDocumentModel;

/**
 * {@link EnableListeners} extends {@link MultipleDocumentAdapter}.
 * It is listener that disables given {@link Action}s if 
 * {@link MultipleDocumentModel} contains zero {@link SingleDocumentModel}s,
 * otherwise it enables them.
 * @author Luka Mijić
 *
 */
public class EnableListeners extends MultipleDocumentAdapter {
	
	/**
	 * List of action that are enabled/disabled
	 */
	private Action[] actions;

	/**
	 * Creates {@link EnableListeners}.
	 * @param actions that are being enabled/disable in real-time
	 */
	public EnableListeners(Action... actions) {
		super();
		this.actions = actions;
	}
	
	/**
	 * If document is added enable all actions.
	 * @param model that is added
	 */
	@Override
	public void documentAdded(SingleDocumentModel model) {
		for(Action a:actions) {
			a.setEnabled(true);
		}
	}
	
	/**
	 * If number of {@link SaveIconListener} stored in 
	 * {@link MultipleDocumentModel} is zero, disable
	 * all actions.
	 * @param model that is removed
	 */
	@Override
	public void documentRemoved(SingleDocumentModel model) {
		if(JNotepadPP.multipleModel.getNumberOfDocuments() <= 0) {
			for(Action a:actions) {
				a.setEnabled(false);
			}
		}
	}

}
