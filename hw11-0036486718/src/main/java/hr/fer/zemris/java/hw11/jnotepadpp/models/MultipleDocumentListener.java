package hr.fer.zemris.java.hw11.jnotepadpp.models;

/**
 * Inteface models listener to {@link MultipleDocumentModel}.
 * @author Luka Mijić
 *
 */
public interface MultipleDocumentListener {
	/**
	 * Is called when {@link MultipleDocumentModel} changes current {@link SingleDocumentModel}.
	 * @param previousModel
	 * @param currentModel
	 */
	void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel);

	/**
	 * Is called when {@link MultipleDocumentModel} adds new {@link SingleDocumentModel}.
	 * @param model that is added
	 */
	void documentAdded(SingleDocumentModel model);

	/**
	 * Is called when {@link MultipleDocumentModel} removes {@link SingleDocumentModel}.
	 * @param model that is removed
	 */
	void documentRemoved(SingleDocumentModel model);
}
