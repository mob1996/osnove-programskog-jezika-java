package hr.fer.zemris.java.hw11.jnotepadpp.local;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

/**
 * Class extends {@link LocalizationProviderBridge} and makes connection between 
 * {@link ILocalizationProvider} and {@link JFrame}. When {@link JFrame} is opened it 
 * calls {@link LocalizationProviderBridge#connect()} and when {@link JFrame} is closed
 * it calls {@link LocalizationProviderBridge#disconnect()}.
 * @author Luka Mijić
 *
 */
public class FormLocalizationProvider extends LocalizationProviderBridge {

	/**
	 * Creates {@link FormLocalizationProvider} and registers {@link WindowAdapter}
	 * that connects when {@link WindowAdapter#windowOpened(WindowEvent)} is called
	 * and disconnects when {@link WindowAdapter#windowClosed(WindowEvent)} is called.
	 * @param provider of localisation
	 * @param frame that registers {@link WindowAdapter}
	 */
	public FormLocalizationProvider(ILocalizationProvider provider, JFrame frame) {
		super(provider);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				connect();
			}
			
			@Override
			public void windowClosed(WindowEvent e) {
				disconnect();
			}
		});
	}

}
