package hr.fer.zemris.java.hw11.jnotepadpp.models.impl.listeners;

import hr.fer.zemris.java.hw11.jnotepadpp.models.SingleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.models.SingleDocumentModel;

/**
 * Adapter for {@link SaveIconListener}.
 * Only purpose for this class is to enable creation of anonymous classes
 * that don't have to @Override all provided methods because their implementation is
 * empty.
 * @author Luka Mijić
 *
 */
public abstract class SingleDocumentAdapter implements SingleDocumentListener {

	@Override
	public void documentModifyStatusUpdated(SingleDocumentModel model) {
	}

	@Override
	public void documentFilePathUpdated(SingleDocumentModel model) {
	}

}
