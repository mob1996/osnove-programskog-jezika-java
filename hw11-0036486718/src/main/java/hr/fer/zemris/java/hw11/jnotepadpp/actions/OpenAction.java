package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;
import java.nio.file.Path;

import javax.swing.JFileChooser;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.models.SingleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.models.impl.DefaultMultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.models.impl.listeners.SaveIconListener;

/**
 * {@link OpenAction} extends {@link LocalizableAction}.
 * It opens existing file from disk and loads it into {@link DefaultMultipleDocumentModel}
 * as new, non empty {@link SingleDocumentModel}.
 * @author Luka Mijić
 *
 */
public class OpenAction extends LocalizableAction {

	private static final long serialVersionUID = 1L;

	/**
	 * Creates {@link OpenAction}.
	 * @param key for localisation 
	 * @param lp provider of localisation
	 */
	public OpenAction(String key, ILocalizationProvider lp) {
		super(key, lp);
	}
	
	/**
	 * When triggered method creates new {@link JFileChooser} that
	 * lets use pick file that should be loaded. User can cancel file picking
	 * at any moment. If user picks file, it is loaded into {@link DefaultMultipleDocumentModel},
	 * using {@link DefaultMultipleDocumentModel#loadDocument(Path)}.
	 * @param e action even
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle("Open file");
		if(fc.showOpenDialog(JNotepadPP.multipleModel) != JFileChooser.APPROVE_OPTION) {
			return;
		}
		
		Path filePath = fc.getSelectedFile().toPath();
		System.out.println(filePath.toString());
		SingleDocumentModel opened= JNotepadPP.multipleModel.loadDocument(filePath);
		
		if(opened != null) {
			opened.addSingleDocumentListener(new SaveIconListener());
		}
	}

}
