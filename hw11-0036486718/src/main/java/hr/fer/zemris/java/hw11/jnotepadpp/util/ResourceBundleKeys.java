package hr.fer.zemris.java.hw11.jnotepadpp.util;

import java.util.ResourceBundle;

import javax.swing.Action;

/**
 * Utility class that is used for increasing codes readability.
 * It increases code's resistance to errors and if error happens
 * it can be fix at one place. 
 * It stores key that are used by {@link ResourceBundle} to retrieve values
 * using {@link ResourceBundle#getString(String)}.
 * @author Luka Mijić
 *
 */
public class ResourceBundleKeys {

	/**
	 * Added to action name keys to retrieve {@link Action#SHORT_DESCRIPTION}
	 * value.
	 */
	public static String DESCRIPTION = "_desc";
	
	/**
	 * {@link Action#NAME} keys
	 */
	public static String OPEN_KEY = "open";
	public static String NEW_KEY = "new";
	public static String CLOSE_KEY = "close";
	public static String SAVE_KEY = "save";
	public static String SAVE_AS_KEY = "save_as";
	public static String EXIT_KEY = "exit";
	public static String COPY_KEY = "copy";
	public static String CUT_KEY = "cut";
	public static String PASTE_KEY = "paste";
	public static String STATS_KEY = "stats";
	public static String LOWER_CASE_KEY = "lower_case";
	public static String UPPER_CASE_KEY = "upper_case";
	public static String INVERSE_CASE_KEY = "inverse_case";
	
	public static String STATUS_BAR_KEY = "status_bar";
	public static String LEN_STATUS = "_len";
	public static String LINE_STATUS = "_ln";
	public static String COL_STATUS = "_col";
	public static String SELECTED_STATUS = "_sel";
	
	public static String FILE_KEY = "file";
	public static String EDIT_KEY = "edit";
	public static String INFO_KEY = "info";
	public static String LANG_KEY = "lang";
	
	public static String UK_KEY = "uk";
	public static String CRO_KEY = "cro";
	public static String GER_KEY = "ger";
	
	public static String SORT_KEY = "sort";
	public static String SORT_ASC_KEY = "sort_asc";
	public static String SORT_DESC_KEY = "sort_des";
	public static String UNIQUE_KEY = "unique";
}
