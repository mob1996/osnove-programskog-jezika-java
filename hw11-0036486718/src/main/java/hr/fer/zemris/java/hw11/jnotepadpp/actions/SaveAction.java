package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;

import javax.swing.JFileChooser;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.models.SingleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.util.JNotepadPPUtilities;

/**
 * {@link SaveAction} extends {@link LocalizableAction}.
 * Save action saves currently used document to disk.
 * @author Luka Mijić
 *
 */
public class SaveAction  extends LocalizableAction {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Creates {@link SaveAction}.
	 * @param key for localisation 
	 * @param lp provider of localisation
	 */
	public SaveAction(String key, ILocalizationProvider lp) {
		super(key, lp);
	}

	/**
	 * When triggered method saves currently used {@link SingleDocumentModel}
	 * to disk. If {@link SingleDocumentModel#getFilePath()} returns null, user
	 * is prompted to provide saving location using {@link JFileChooser}.
	 * Otherwise save to location stored in {@link SingleDocumentModel}
	 * @param e action event
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		SingleDocumentModel model = JNotepadPP.multipleModel.getCurrentDocument();
		if(model == null) {
			return;
		}
		
		if(model.getFilePath() == null) {
			JNotepadPPUtilities.save(JNotepadPP.multipleModel, model, JNotepadPPUtilities.GET_PATH);
		} else {
			JNotepadPP.multipleModel.saveDocument(model, model.getFilePath());
		}
		
	}
	

}
