package hr.fer.zemris.java.custom.collections;

import static org.junit.Assert.fail;

import org.junit.Assert;
import org.junit.Test;

public class ArrayIndexedCollectionTest {

	
	public static ArrayIndexedCollection initTestArray() {
		ArrayIndexedCollection ar = new ArrayIndexedCollection();
		ar.add("Test1");
		ar.add(Integer.valueOf(5));
		ar.add(Double.valueOf(6.0));
		ar.add(Character.valueOf('a'));
		ar.add("Test2");
		ar.add("Test3");
		return ar;
	}
		
	@Test
	public void testConstructors() {
		ArrayIndexedCollection ar1 = new ArrayIndexedCollection();
		
		Assert.assertEquals(0, ar1.size());
		Assert.assertTrue(ar1.isEmpty());
		
		ArrayIndexedCollection ar2 = new ArrayIndexedCollection(4);
		
		Assert.assertEquals(0, ar2.size());
		Assert.assertTrue(ar2.isEmpty());
		
		ArrayIndexedCollection ar3 = new ArrayIndexedCollection(initTestArray());
		
		Assert.assertEquals(6, ar3.size());
		Assert.assertFalse(ar3.isEmpty());
		
		ArrayIndexedCollection ar4 = new ArrayIndexedCollection(initTestArray(), 5);
		
		Assert.assertEquals(6, ar4.size());
		Assert.assertFalse(ar4.isEmpty());
		
		try {
			ArrayIndexedCollection ar5 = new ArrayIndexedCollection(-1);
			fail("IllegalArgumentException expected!");
		} catch (IllegalArgumentException exc) {
		}
		
		try {
			ArrayIndexedCollection ar6 = new ArrayIndexedCollection(null);
			fail("NullpointerException expected!");
		} catch (NullPointerException exc) {
		}
	}
	
	@Test
	public void testSize() {
		int expected = 0;
		ArrayIndexedCollection ar = new ArrayIndexedCollection();
		
		Assert.assertEquals(expected, ar.size());
		
		ar.add("Test1");
		ar.add("Test2");
		ar.add("Test3");
		expected = 3;
		
		Assert.assertEquals(expected, ar.size());
	}
	
	@Test
	public void testIsEmpty() {
		ArrayIndexedCollection ar = new ArrayIndexedCollection(4);
		
		Assert.assertTrue(ar.isEmpty());
		
		ar.add("Test");
		Assert.assertFalse(ar.isEmpty());
	}
	
	@Test
	public void testContains() {
		ArrayIndexedCollection ar = initTestArray();
		Assert.assertTrue(ar.contains("Test1"));
		Assert.assertTrue(ar.contains(Integer.valueOf(5)));
		Assert.assertTrue(ar.contains("Test2"));
		Assert.assertTrue(ar.contains(Double.valueOf(6.0)));
		
		Assert.assertFalse(ar.contains("test1"));
		Assert.assertFalse(ar.contains(Integer.valueOf(15)));
		Assert.assertFalse(ar.contains("test2"));
		Assert.assertFalse(ar.contains(Double.valueOf(16.0)));
	}
	
	@Test
	public void testAdd() {
		ArrayIndexedCollection ar = new ArrayIndexedCollection(2);
		
		try {
			ar.add(null);
			fail("NullPointerException expected.");
		} catch (NullPointerException exc) {
		}
		
		try {
			Object expectedValue = "Test";
			int expectedSize = 1;
			ar.add("Test");
			
			Assert.assertEquals(expectedValue, ar.get(0));
			Assert.assertEquals(expectedSize, ar.size());
			
			ar.add("Test1");
			ar.add("Test2");
			ar.add("Test3");
			
			expectedValue = "Test3";
			expectedSize = 4;
			
			Assert.assertEquals(expectedValue, ar.get(3));
			Assert.assertEquals(expectedSize, ar.size());
			
			
		} catch (NullPointerException exc) {
			fail(exc.getMessage());
		}
	}
	
	@Test
	public void testGet() {
		ArrayIndexedCollection ar = initTestArray();
		try {
			Object obj = ar.get(-2);
			fail("IndexOutOfBoundException expected.");
		} catch (IndexOutOfBoundsException exc) {
		}
		
		try {
			Object obj = ar.get(ar.size());
			System.out.println(ar.size());
			fail("IndexOutOfBoundException expected.");
		} catch (IndexOutOfBoundsException exc) {
		}
		
		try {
			Assert.assertEquals("Test1", ar.get(0));
			Assert.assertEquals(Character.valueOf('a'), ar.get(3));
		} catch(IndexOutOfBoundsException exc) {
			fail(exc.getMessage());
		}
		
	}
	
	@Test
	public void testInsert() {
		ArrayIndexedCollection ar = initTestArray();
		try {
			ar.insert("Test", -1);
			fail("IndexOutOfBoundException expected.");
		} catch (IndexOutOfBoundsException exc) {
		}
		
		try {
			ar.insert("Test", ar.size()+1);
			fail("IndexOutOfBoundException expected.");
		} catch (IndexOutOfBoundsException exc) {
		}
		
		try {
		
			int startSize = ar.size();
			//insert start
			ar.insert("Test0", 0);

			Assert.assertEquals("Test0", ar.get(0));
			Assert.assertEquals("Test1", ar.get(1));
			Assert.assertEquals("Test3", ar.get(6));
			Assert.assertEquals(startSize + 1, ar.size());
			
			//insert middle
			ar.insert("TestMid", 4);
			
			Assert.assertEquals(Double.valueOf(6.0), ar.get(3));
			Assert.assertEquals("TestMid", ar.get(4));
			Assert.assertEquals(Character.valueOf('a'), ar.get(5));
			Assert.assertEquals("Test3", ar.get(7));
			Assert.assertEquals(startSize + 2, ar.size());
			
			//insert end
			ar.insert("TestEnd", 7);
			
			Assert.assertEquals("Test3", ar.get(8));
			Assert.assertEquals("TestEnd", ar.get(7));
			Assert.assertEquals("Test2", ar.get(6));
			
		} catch(IndexOutOfBoundsException exc) {
			fail(exc.getMessage());
		}
		
	}
	

	@Test
	public void testRemoveObj() {
		ArrayIndexedCollection ar = initTestArray();
		int startSize = ar.size();
		
		Assert.assertFalse(ar.contains("Test15"));
		Assert.assertFalse(ar.remove("Test15"));
		Assert.assertEquals(startSize, ar.size());
		Assert.assertEquals("Test1", ar.get(0));
		
		Assert.assertTrue(ar.contains("Test1"));
		Assert.assertTrue(ar.remove("Test1"));
		Assert.assertFalse(ar.contains("Test1"));
		Assert.assertEquals(startSize - 1, ar.size());
		Assert.assertEquals(Integer.valueOf(5), ar.get(0));
		
		Assert.assertTrue(ar.contains("Test2"));
		Assert.assertTrue(ar.remove("Test2"));
		Assert.assertFalse(ar.contains("Test2"));
		Assert.assertEquals(startSize - 2, ar.size());
		Assert.assertEquals("Test3", ar.get(3));
	}
	
	@Test
	public void testRemoveIndex() {
		ArrayIndexedCollection ar = initTestArray();
		int startSize = ar.size();
		try {
			ar.remove(-1);;
			fail("IndexOutOfBoundException expected.");
		} catch (IndexOutOfBoundsException exc) {
		}
		
		try {
			ar.remove(ar.size());
			fail("IndexOutOfBoundException expected.");
		} catch (IndexOutOfBoundsException exc) {
		}
		
		Object removed = ar.get(0);
		ar.remove(0);
		
		Assert.assertFalse(ar.contains(removed));
		Assert.assertEquals(startSize - 1, ar.size());
		
		removed = ar.get(ar.size()-1);
		ar.remove(ar.size() - 1);
		
		Assert.assertFalse(ar.contains(removed));
		Assert.assertEquals(startSize - 2, ar.size());
		
	}
	
	@Test
	public void testClear() {
		ArrayIndexedCollection ar = initTestArray();
		
		Assert.assertTrue(ar.size() != 0);
		
		ar.clear();
		
		Assert.assertTrue(ar.size() == 0);
		
	}
	
	@Test
	public void testIndexOf() {
		int expectedFail = -1;
		int[] expected = {0, 3, 5};
		ArrayIndexedCollection ar = initTestArray();
		ar.add("Test1"); //adding duplicate
		
		Assert.assertEquals(expectedFail, ar.indexOf(null));
		Assert.assertEquals(expectedFail, ar.indexOf("Test5"));
		Assert.assertEquals(expectedFail, ar.indexOf(Integer.valueOf(9)));
		Assert.assertEquals(expectedFail, ar.indexOf(Double.valueOf(5.6)));
		

		Assert.assertEquals(expected[0], ar.indexOf("Test1"));
		Assert.assertEquals(expected[1], ar.indexOf(Character.valueOf('a')));
		Assert.assertEquals(expected[2], ar.indexOf("Test3"));
		
	}
	
	@Test
	public void testToArray() {
		ArrayIndexedCollection ar = initTestArray();
		Object[] array = ar.toArray();
		
		for(int i = 0; i < ar.size(); i++) {
			Assert.assertEquals(ar.get(i), array[i]);
		}
	}
	
	@Test
	public void testForEach() {
		ArrayIndexedCollection ar = initTestArray();

		class TestProcessor extends Processor{
			private int i;
			@Override
			public void process(Object value) {
				Assert.assertEquals(value, ar.get(i));
				i++;
			}
		}
		
		ar.forEach(new TestProcessor());
		
	}
	
	@Test
	public void testAddAll() {
		ArrayIndexedCollection ar = initTestArray();
		ArrayIndexedCollection arAdd = new ArrayIndexedCollection(2);
		int startSize = ar.size();
		
		arAdd.add("Test4");
		arAdd.add(Integer.valueOf(7));
		
		ar.addAll(arAdd);
		
		Assert.assertEquals(startSize + arAdd.size(), ar.size());
		Assert.assertEquals("Test4", ar.get(startSize));
		Assert.assertEquals(Integer.valueOf(7), ar.get(startSize+1));
		
	}
}
