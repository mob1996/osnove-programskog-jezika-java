package hr.fer.zemris.java.custom.collections;

import static org.junit.Assert.fail;

import org.junit.Assert;
import org.junit.Test;

public class LinkedListIndexedCollectionTest {
	
	public static LinkedListIndexedCollection initTestList() {
		LinkedListIndexedCollection list = new LinkedListIndexedCollection();
		list.add("Test1");
		list.add(Integer.valueOf(5));
		list.add(Double.valueOf(6.0));
		list.add(Character.valueOf('a'));
		list.add("Test2");
		list.add("Test3");
		return list;
	}
		
	@Test
	public void testConstructors() {
		LinkedListIndexedCollection list1 = new LinkedListIndexedCollection();
		
		Assert.assertEquals(0, list1.size());
		Assert.assertTrue(list1.isEmpty());
		
		
		LinkedListIndexedCollection list2 = new LinkedListIndexedCollection(initTestList());
		
		Assert.assertEquals(6, list2.size());
		Assert.assertFalse(list2.isEmpty());
		
		
		try {
			LinkedListIndexedCollection list3 = new LinkedListIndexedCollection(null);
			fail("NullpointerException expected!");
		} catch (NullPointerException exc) {
		}
	}
	
	@Test
	public void testSize() {
		int expected = 0;
		LinkedListIndexedCollection list = new LinkedListIndexedCollection();
		
		Assert.assertEquals(expected, list.size());
		
		list.add("Test1");
		list.add("Test2");
		list.add("Test3");
		expected = 3;
		
		Assert.assertEquals(expected, list.size());
	}
	
	@Test
	public void testIsEmpty() {
		LinkedListIndexedCollection list = new LinkedListIndexedCollection();
		
		Assert.assertTrue(list.isEmpty());
		
		list.add("Test");
		Assert.assertFalse(list.isEmpty());
	}
	
	@Test
	public void testContains() {
		LinkedListIndexedCollection list = initTestList();
		Assert.assertTrue(list.contains("Test1"));
		Assert.assertTrue(list.contains(Integer.valueOf(5)));
		Assert.assertTrue(list.contains("Test2"));
		Assert.assertTrue(list.contains(Double.valueOf(6.0)));
		
		Assert.assertFalse(list.contains("test1"));
		Assert.assertFalse(list.contains(Integer.valueOf(15)));
		Assert.assertFalse(list.contains("test2"));
		Assert.assertFalse(list.contains(Double.valueOf(16.0)));
	}
	
	@Test
	public void testAdd() {
		LinkedListIndexedCollection list = new LinkedListIndexedCollection();
		
		try {
			list.add(null);
			fail("NullPointerException expected.");
		} catch (NullPointerException exc) {
		}
		
		try {
			Object expectedValue = "Test";
			int expectedSize = 1;
			list.add("Test");
			
			Assert.assertEquals(expectedValue, list.get(0));
			Assert.assertEquals(expectedSize, list.size());
			
			list.add("Test1");
			list.add("Test2");
			list.add("Test3");
			
			expectedValue = "Test3";
			expectedSize = 4;
			
			Assert.assertEquals(expectedValue, list.get(3));
			Assert.assertEquals(expectedSize, list.size());
			
			
		} catch (NullPointerException exc) {
			fail(exc.getMessage());
		}
	}
	
	@Test
	public void testGet() {
		LinkedListIndexedCollection list = initTestList();
		try {
			Object obj = list.get(-2);
			fail("IndexOutOfBoundException expected.");
		} catch (IndexOutOfBoundsException exc) {
		}
		
		try {
			Object obj = list.get(list.size());
			System.out.println(list.size());
			fail("IndexOutOfBoundException expected.");
		} catch (IndexOutOfBoundsException exc) {
		}
		
		try {
			Assert.assertEquals("Test1", list.get(0));
			Assert.assertEquals(Character.valueOf('a'), list.get(3));
		} catch(IndexOutOfBoundsException exc) {
			fail(exc.getMessage());
		}
		
	}
	
	@Test
	public void testInsert() {
		LinkedListIndexedCollection list = initTestList();
		try {
			list.insert("Test", -1);
			fail("IndexOutOfBoundException expected.");
		} catch (IndexOutOfBoundsException exc) {
		}
		
		try {
			list.insert("Test", list.size()+1);
			fail("IndexOutOfBoundException expected.");
		} catch (IndexOutOfBoundsException exc) {
		}
		
		try {
		
			int startSize = list.size();
			//insert start
			list.insert("Test0", 0);

			Assert.assertEquals("Test0", list.get(0));
			Assert.assertEquals("Test1", list.get(1));
			Assert.assertEquals("Test3", list.get(6));
			Assert.assertEquals(startSize + 1, list.size());
			
			//insert middle
			list.insert("TestMid", 4);
			
			Assert.assertEquals(Double.valueOf(6.0), list.get(3));
			Assert.assertEquals("TestMid", list.get(4));
			Assert.assertEquals(Character.valueOf('a'), list.get(5));
			Assert.assertEquals("Test3", list.get(7));
			Assert.assertEquals(startSize + 2, list.size());
			
			//insert end
			list.insert("TestEnd", 7);
			
			Assert.assertEquals("Test3", list.get(8));
			Assert.assertEquals("TestEnd", list.get(7));
			Assert.assertEquals("Test2", list.get(6));
			
		} catch(IndexOutOfBoundsException exc) {
			fail(exc.getMessage());
		}
		
	}
	

	@Test
	public void testRemoveObj() {
		LinkedListIndexedCollection list = initTestList();
		int startSize = list.size();
		
		Assert.assertFalse(list.contains("Test15"));
		Assert.assertFalse(list.remove("Test15"));
		Assert.assertEquals(startSize, list.size());
		Assert.assertEquals("Test1", list.get(0));
		
		Assert.assertTrue(list.contains("Test1"));
		Assert.assertTrue(list.remove("Test1"));
		Assert.assertFalse(list.contains("Test1"));
		Assert.assertEquals(startSize - 1, list.size());
		Assert.assertEquals(Integer.valueOf(5), list.get(0));
		
		Assert.assertTrue(list.contains("Test2"));
		Assert.assertTrue(list.remove("Test2"));
		Assert.assertFalse(list.contains("Test2"));
		Assert.assertEquals(startSize - 2, list.size());
		Assert.assertEquals("Test3", list.get(3));
		
		Assert.assertTrue(list.remove("Test3"));
		Assert.assertTrue(list.remove(Character.valueOf('a')));
		Assert.assertTrue(list.remove(Double.valueOf(6.0)));
		Assert.assertTrue(list.remove(Integer.valueOf(5)));
		Assert.assertEquals(0, list.size());
	}
	
	@Test
	public void testRemoveIndex() {
		LinkedListIndexedCollection list = initTestList();
		int startSize = list.size();
		try {
			list.remove(-1);;
			fail("IndexOutOfBoundException expected.");
		} catch (IndexOutOfBoundsException exc) {
		}
		
		try {
			list.remove(list.size());
			fail("IndexOutOfBoundException expected.");
		} catch (IndexOutOfBoundsException exc) {
		}
		
		Object removed = list.get(0);
		list.remove(0);
		
		Assert.assertFalse(list.contains(removed));
		Assert.assertEquals(startSize - 1, list.size());
		
		removed = list.get(list.size()-1);
		list.remove(list.size() - 1);
		
		Assert.assertFalse(list.contains(removed));
		Assert.assertEquals(startSize - 2, list.size());
		
		for(; 0 < list.size();) {
			list.remove(0);
		}
		Assert.assertEquals(0, list.size());
		
	}
	
	@Test
	public void testClelist() {
		LinkedListIndexedCollection list = initTestList();
		
		Assert.assertTrue(list.size() != 0);
		
		list.clear();
		
		Assert.assertTrue(list.size() == 0);
		
	}
	
	@Test
	public void testIndexOf() {
		int expectedFail = -1;
		int[] expected = {0, 3, 5};
		LinkedListIndexedCollection list = initTestList();
		list.add("Test1"); //adding duplicate
		
		Assert.assertEquals(expectedFail, list.indexOf(null));
		Assert.assertEquals(expectedFail, list.indexOf("Test5"));
		Assert.assertEquals(expectedFail, list.indexOf(Integer.valueOf(9)));
		Assert.assertEquals(expectedFail, list.indexOf(Double.valueOf(5.6)));
		

		Assert.assertEquals(expected[0], list.indexOf("Test1"));
		Assert.assertEquals(expected[1], list.indexOf(Character.valueOf('a')));
		Assert.assertEquals(expected[2], list.indexOf("Test3"));
		
	}
	
	@Test
	public void testToArray() {
		LinkedListIndexedCollection list = initTestList();
		Object[] array = list.toArray();
		
		for(int i = 0; i < list.size(); i++) {
			Assert.assertEquals(list.get(i), array[i]);
		}
	}
	
	@Test
	public void testForEach() {
		LinkedListIndexedCollection list = initTestList();

		class TestProcessor extends Processor{
			private int i;
			@Override
			public void process(Object value) {
				Assert.assertEquals(value, list.get(i));
				i++;
			}
		}
		
		list.forEach(new TestProcessor());
		
	}
	
	@Test
	public void testAddAll() {
		LinkedListIndexedCollection list = initTestList();
		LinkedListIndexedCollection listAdd = new LinkedListIndexedCollection();
		int startSize = list.size();
		
		listAdd.add("Test4");
		listAdd.add(Integer.valueOf(7));
		
		list.addAll(listAdd);
		
		Assert.assertEquals(startSize + listAdd.size(), list.size());
		Assert.assertEquals("Test4", list.get(startSize));
		Assert.assertEquals(Integer.valueOf(7), list.get(startSize+1));
		
	}
}

