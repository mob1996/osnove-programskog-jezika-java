package hr.fer.zemris.java.hw02;

import static org.junit.Assert.fail;

import org.junit.Assert;
import org.junit.Test;

import hr.fer.zemris.java.hw02.ComplexNumber;

import static java.lang.Math.PI;

public class ComplexNumberTest {

	private static double epsilon = 1E-4;
	
	@Test
	public void testAngleZeroToTwoPI() {
		double angle = 6.5;
		
		Assert.assertEquals(6.5 % (2 * PI), 
				ComplexNumber.angleZeroToTwoPI(angle), epsilon);
		
		angle = -3.5;
		
		Assert.assertEquals(2 * PI + (-3.5 % (2 * PI)), 
				ComplexNumber.angleZeroToTwoPI(angle), epsilon);
		
		angle = -50.25;
		
		Assert.assertEquals(2 * PI + (-50.25 % (2 * PI)), 
				ComplexNumber.angleZeroToTwoPI(angle), epsilon);
	}
	
	@Test
	public void testFromReal() {
		ComplexNumber c = ComplexNumber.fromReal(5.25);
		
		Assert.assertEquals(5.25, c.getReal(), epsilon);
		Assert.assertEquals(0, c.getImaginary(), epsilon);
		
		c = ComplexNumber.fromReal(0);
		
		Assert.assertEquals(0, c.getReal(), epsilon);
		Assert.assertEquals(0, c.getImaginary(), epsilon);
		
		c = ComplexNumber.fromReal(-25.30);
		
		Assert.assertEquals(-25.30, c.getReal(), epsilon);
		Assert.assertEquals(0, c.getImaginary(), epsilon);
	}
	
	@Test
	public void testFromImaginary() {
		ComplexNumber c = ComplexNumber.fromImaginary(5.25);
		
		Assert.assertEquals(5.25, c.getImaginary(), epsilon);
		Assert.assertEquals(0, c.getReal(), epsilon);
		
		c = ComplexNumber.fromImaginary(0);
		
		Assert.assertEquals(0, c.getImaginary(), epsilon);
		Assert.assertEquals(0, c.getReal(), epsilon);
		
		c = ComplexNumber.fromImaginary(-25.30);
		
		Assert.assertEquals(-25.30, c.getImaginary(), epsilon);
		Assert.assertEquals(0, c.getReal(), epsilon);
	}
	
	@Test
	public void testFromMagnitudeAndAngle() {
		double magnitude = 5.5;
		
		//edge cases
		double angle = PI/2;
		ComplexNumber c = ComplexNumber.fromMagnitudeAndAngle(magnitude, angle);
		
		Assert.assertEquals(0, c.getReal(), epsilon);
		Assert.assertEquals(5.5, c.getImaginary(), epsilon);
		
		angle = 3 * PI/2;
		c = ComplexNumber.fromMagnitudeAndAngle(magnitude, angle);
		
		Assert.assertEquals(0, c.getReal(), epsilon);
		Assert.assertEquals(-5.5, c.getImaginary(), epsilon);
		
		angle = 0;
		c = ComplexNumber.fromMagnitudeAndAngle(magnitude, angle);
		
		Assert.assertEquals(5.5, c.getReal(), epsilon);
		Assert.assertEquals(0, c.getImaginary(), epsilon);
		
		angle = 2 * PI;
		c = ComplexNumber.fromMagnitudeAndAngle(magnitude, angle);
		
		Assert.assertEquals(5.5, c.getReal(), epsilon);
		Assert.assertEquals(0, c.getImaginary(), epsilon);
		
		angle = PI;
		c = ComplexNumber.fromMagnitudeAndAngle(magnitude, angle);
		
		Assert.assertEquals(-5.5, c.getReal(), epsilon);
		Assert.assertEquals(0, c.getImaginary(), epsilon);
		
		//test for every quadrant
		//I. quadrant
		angle = PI/6;
		c = ComplexNumber.fromMagnitudeAndAngle(magnitude, angle);
		
		Assert.assertEquals(4.7631397, c.getReal(), epsilon);
		Assert.assertEquals(2.75, c.getImaginary(), epsilon);
		
		//II. quadrant
		angle = PI/6 + PI/2;
		c = ComplexNumber.fromMagnitudeAndAngle(magnitude, angle);
		
		Assert.assertEquals(-2.75, c.getReal(), epsilon);
		Assert.assertEquals(4.7631397, c.getImaginary(), epsilon);
		
		//III. quadrant
		angle = PI/6 + PI;
		c = ComplexNumber.fromMagnitudeAndAngle(magnitude, angle);
		
		Assert.assertEquals(-4.7631397, c.getReal(), epsilon);
		Assert.assertEquals(-2.75, c.getImaginary(), epsilon);
		
		//IV. quadrant
		angle = PI/6 + 3 * PI/2;
		c = ComplexNumber.fromMagnitudeAndAngle(magnitude, angle);
		
		Assert.assertEquals(2.75, c.getReal(), epsilon);
		Assert.assertEquals(-4.7631397, c.getImaginary(), epsilon);
		
	}
	
	@Test
	public void testParse() {
		try {
			ComplexNumber c = ComplexNumber.parse(null);
			fail("NullPointerException expected.");
		} catch (NullPointerException exc) {
		}
		
		try {
			ComplexNumber c = ComplexNumber.parse("30h");
			fail("IllegalArgumentException expected.");
		} catch (IllegalArgumentException exc) {	
		}
		
		try {
			ComplexNumber c = ComplexNumber.parse("30,2");
			fail("IllegalArgumentException expected.");
		} catch (IllegalArgumentException exc) {
		}
		
		try {
			ComplexNumber c = ComplexNumber.parse("3.5+2.6i");
			
			Assert.assertEquals(3.5, c.getReal(), epsilon);
			Assert.assertEquals(2.6, c.getImaginary(), epsilon);
			
			c = ComplexNumber.parse("3.5+2.6i+2+3i");
				
			Assert.assertEquals(5.5, c.getReal(), epsilon);
			Assert.assertEquals(5.6, c.getImaginary(), epsilon);
			
			c = ComplexNumber.parse("2.3-i+i+21i+3.5i+i+0.5i+1.7");
			
			Assert.assertEquals(4, c.getReal(), epsilon);
			Assert.assertEquals(26, c.getImaginary(), epsilon);
			
			c = ComplexNumber.parse("i");
			
			Assert.assertEquals(0, c.getReal(), epsilon);
			Assert.assertEquals(1, c.getImaginary(), epsilon);
			
			c = ComplexNumber.parse("-i");
			
			Assert.assertEquals(0, c.getReal(), epsilon);
			Assert.assertEquals(-1, c.getImaginary(), epsilon);
			
			
		} catch (IllegalArgumentException exc) {
			fail("IlleagalArgumentException not expected.");
		}
	}
	
	@Test
	public void testAdd() {
		ComplexNumber c = new ComplexNumber(2, 3.2);
		try {
		
			c = c.add(new ComplexNumber(2.5, 2.2));
		
			Assert.assertEquals(2 + 2.5, c.getReal(), epsilon);
			Assert.assertEquals(3.2 + 2.2, c.getImaginary(), epsilon);
			
			c = c.add(new ComplexNumber(1.1, 0.5));
		
			Assert.assertEquals(2 + 2.5 + 1.1, c.getReal(), epsilon);
			Assert.assertEquals(3.2 + 2.2 + 0.5, c.getImaginary(), epsilon);
		} catch (NullPointerException exc) {
			fail("NullPointerException not expected");
		}
		
		try {
			c.add(null);
			fail("NullPointerException expected");
		} catch (NullPointerException exc) {
		}
	}
	
	@Test
	public void testSub() {
		ComplexNumber c = new ComplexNumber(2, 3.2);
		
		try {
			c = c.sub(new ComplexNumber(2.5, 2.2));
		
			Assert.assertEquals(2 - 2.5, c.getReal(), epsilon);
			Assert.assertEquals(3.2 - 2.2, c.getImaginary(), epsilon);
		
			c = c.sub(new ComplexNumber(1.1, 0.5));
		
			Assert.assertEquals(2 - 2.5 - 1.1, c.getReal(), epsilon);
			Assert.assertEquals(3.2 - 2.2 - 0.5, c.getImaginary(), epsilon);
		} catch (NullPointerException exc) {
			fail("NullPointerException not expected");
		}
		
		try {
			c.sub(null);
			fail("NullPointerException expected");
		} catch (NullPointerException exc) {
		}
	
	}
	
	@Test
	public void testConjugate() {
		ComplexNumber c = new ComplexNumber(-2, -3);
		ComplexNumber cConjugate = c.conjugate();
		
		Assert.assertEquals(c.getReal(), cConjugate.getReal(), epsilon);
		Assert.assertEquals(-c.getImaginary(), cConjugate.getImaginary(), epsilon);
		
		c = new ComplexNumber(-2, 3);
		cConjugate = c.conjugate();
		
		Assert.assertEquals(c.getReal(), cConjugate.getReal(), epsilon);
		Assert.assertEquals(-c.getImaginary(), cConjugate.getImaginary(), epsilon);
	
	}
	
	@Test
	public void testPower() {
		ComplexNumber c= new ComplexNumber(2, -3);
		
		try {
			ComplexNumber cMinus = c.power(-1);
			fail("IllegalArgumentException expected.");
		} catch (IllegalArgumentException exc) {
		}
		
		try {
			ComplexNumber nZero = c.power(0);
			
			Assert.assertEquals(1, nZero.getReal(), epsilon);
			Assert.assertEquals(0, nZero.getImaginary(), epsilon);
			
			ComplexNumber nOne = c.power(1);
			
			Assert.assertEquals(c.getReal(), nOne.getReal(), epsilon);
			Assert.assertEquals(c.getImaginary(), nOne.getImaginary(), epsilon);
			
			ComplexNumber nTwo = c.power(2);
			
			Assert.assertEquals(-5, nTwo.getReal(), epsilon);
			Assert.assertEquals(-12, nTwo.getImaginary(), epsilon);
			
			ComplexNumber nThree = c.power(3);
			
			Assert.assertEquals(-46, nThree.getReal(), epsilon);
			Assert.assertEquals(-9, nThree.getImaginary(), epsilon);
			
		} catch (IllegalArgumentException exc) {
			fail("IllegalArgumentException not expected.");
		}
	}
	
	@Test
	public void testRoot() {
		ComplexNumber c = new ComplexNumber(2, -3);
		
		try {
			c.root(0);
			fail("IllegalArgumentException expected.");
		} catch (IllegalArgumentException exc) {
		}
		try {
			c.root(-1);
			fail("IllegalArgumentException expected.");
		} catch (IllegalArgumentException exc) {
		}
		
		try {
			ComplexNumber[] roots = c.root(3);
			
			Assert.assertEquals(-0.298628, roots[0].getReal(), epsilon);
			Assert.assertEquals(1.504046, roots[0].getImaginary(), epsilon);
			
			Assert.assertEquals(-1.1532, roots[1].getReal(), epsilon);
			Assert.assertEquals(-1.0106, roots[1].getImaginary(), epsilon);
			
			Assert.assertEquals(1.4519, roots[2].getReal(), epsilon);
			Assert.assertEquals(-0.4934, roots[2].getImaginary(), epsilon);
			
		} catch (IllegalArgumentException exc) {
			fail("IllegalArgumentException not expected.");
		}
	}
	
	
}
