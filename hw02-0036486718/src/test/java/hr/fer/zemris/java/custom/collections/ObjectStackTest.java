package hr.fer.zemris.java.custom.collections;

import static org.junit.Assert.fail;

import org.junit.Assert;
import org.junit.Test;

public class ObjectStackTest {

	private static ObjectStack init_stack() {
		ObjectStack os = new ObjectStack();
		os.push("Test0");
		os.push("Test1");
		os.push("Test2");
		os.push("Test3");
		os.push("Test4");
		os.push(Integer.valueOf(20));
		os.push(Double.valueOf(5.0));
		
		return os;
	}
	
	@Test
	public void testIsEmpty() {
		ObjectStack os = init_stack();
		
		Assert.assertFalse(os.isEmpty());
		
		os.pop();
		os.pop();
		
		os.pop();
		Assert.assertFalse(os.isEmpty());
		
		os.pop();
		os.pop();
		os.pop();
		os.pop();
		
		Assert.assertTrue(os.isEmpty());
	}
	
	@Test
	public void testSize() {
		ObjectStack os = init_stack();
		
		for(int expected = os.size(); !os.isEmpty(); expected--) {
			Assert.assertEquals(expected, os.size());
			os.pop();
		}
	}
	
	@Test
	public void testPush() {
		ObjectStack os = new ObjectStack();
		
		for(int i = 0; i < 5; i++) {
			String value = "Test"+i;
			os.push(value);
			Assert.assertEquals(value, os.peek());
			Assert.assertEquals(i+1, os.size());
		}
	}
	
	@Test
	public void testPop() {
		ObjectStack os = init_stack();
		int size = os.size();
		
		Assert.assertEquals(Double.valueOf(5.0), os.pop());
		Assert.assertEquals(--size, os.size());
		
		Assert.assertEquals(Integer.valueOf(20), os.pop());
		Assert.assertEquals(--size, os.size());
		
		while(!os.isEmpty()) {
			Assert.assertEquals("Test" + (size-1), os.pop());
			Assert.assertEquals(--size, os.size());
		}
		
		try {
			os.pop();
			fail("EmptyStackException expected.");
		} catch(EmptyStackException exc) {
			
		}
	}
	
	@Test
	public void testPeek() {
		ObjectStack os = init_stack();
		int size = os.size();
		
		Assert.assertEquals(Double.valueOf(5.0), os.peek());
		os.pop();
		
		Assert.assertEquals(Integer.valueOf(20), os.peek());
		os.pop();
		
		while(!os.isEmpty()) {
			Assert.assertEquals("Test" + (os.size()-1), os.peek());
			os.pop();
		}
		
		try {
			os.peek();
			fail("EmptyStackException expected.");
		} catch(EmptyStackException exc) {
			
		}
	}
	
	@Test
	public void testClear() {
		ObjectStack os = init_stack();
		
		Assert.assertFalse(os.isEmpty());
		
		os.clear();
		
		Assert.assertTrue(os.isEmpty());
	}
}
