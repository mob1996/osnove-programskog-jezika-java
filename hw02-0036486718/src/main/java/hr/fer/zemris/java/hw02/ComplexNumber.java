package hr.fer.zemris.java.hw02;

import hr.fer.zemris.java.custom.collections.ObjectStack;
import static java.lang.Math.pow;
import static java.lang.Math.PI;
import static java.lang.Math.atan;
import static java.lang.Math.abs;
import static java.lang.Math.cos;
import static java.lang.Math.sin;

/**
 * 
 * @author Luka Mijić
 * Class that is used for representation of
 * complex numbers
 */
public class ComplexNumber {
	
	private double real;
	private double imaginary;
	private double magnitude;
	private double angle;
	
	/**
	 * Constructor for creating complex numbers from 
	 * real and imaginary parts
	 * @param real part of complex number
	 * @param imaginary part of complex number
	 */
	public ComplexNumber(double real, double imaginary) {
		this.real = real;
		this.imaginary = imaginary;
		this.magnitude = pow(real * real + imaginary * imaginary, 0.5); // magnitude = root(real^2 + imaginary^2)
		
		if(abs(real) <= 1E-6 ) { //real close to 0	
			if(abs(imaginary) <= 1E-6) { //real and imaginary close to 0
				angle = 0;
			} else if(imaginary > 0) { 
				angle = PI/2;
			} else {
				angle = 3*PI/2;
			}
		} else {
			angle = atan(imaginary/real);
			angle = real > 0 ? angle : PI + angle; 
		}
		
		angle = angleZeroToTwoPI(angle);
		
	}
	
	private ComplexNumber(double real, double imaginary, double magnitude, double angle) {
		this.real = real;
		this.imaginary = imaginary;
		this.magnitude = magnitude;
		this.angle = angleZeroToTwoPI(angle);
	}
	
	/**
	 * Method is used to transform angle to be an element of
	 * [0, 2*PI]
	 * @param angle of complex number
	 * @return transformed angle
	 */
	public static double angleZeroToTwoPI(double angle) {
		angle = abs(angle) < (2 * PI) ? angle : (angle % (2 * PI));
		angle = angle >= 0 ? angle : angle + 2 * PI;
		return angle;
	}
	
	/**
	 * Factory method used for creating new complex number
	 * from only its real part. 
	 * @param real part of complex number
	 * @return new complex number
	 */
	public static ComplexNumber fromReal(double real) {
		return new ComplexNumber(real, 0);
	}
	
	/**
	 * Factory method used for creating new complex number
	 * from only its imaginary part. 
	 * @param imaginary part of complex number
	 * @return new complex number
	 */
	public static ComplexNumber fromImaginary(double imaginary) {
		return new ComplexNumber(0, imaginary);
	}
	
	/**
	 * Factory method that creates new ComplexNumber from magnitude 
	 * and angle of complex number
	 * @param magnitude of complex number
	 * @param angle of complex number
	 * @return new complex number
	 */
	public static ComplexNumber fromMagnitudeAndAngle(double magnitude, double angle) {
		angle = angleZeroToTwoPI(angle);
		double real;
		double imaginary;
		
		real = magnitude * cos(angle);
		imaginary = magnitude * sin(angle);
		
		return new ComplexNumber(real, imaginary, magnitude, angle);
	}
	
	/**
	 * Factory method creates complex number from given string.
	 * @param str
	 * @return new complex number
	 * @throws NullPointerException if string == null
	 * @throws IllegalArgumentException if String is empty or if
	 * 								string cannot be parsed as complex number
	 */
	public static ComplexNumber parse(String str) {
		if(str == null) throw new NullPointerException("Argument cannot be null.");
		if(str.isEmpty()) throw new IllegalArgumentException("Argument must not be empty String.");
		
		
		
		ObjectStack stack = new ObjectStack();
		str = str.replaceAll("\\s+", "");
		String[] tokens = str.split("");
		
		
		for(String token:tokens) {
			stack.push(token);
		}
		
		return parseComplexFromStack(stack);
	}
	
	/**
	 * Method is used to parse complex number from elements of stack.
	 * Algorithm:
	 * 		1. if character 'i' is at top of the stack. That means that 
	 * 			every character until the '+' and '-' (including them) represents imaginary
	 * 			part of the complex number.
	 * 			a) If 'i' is being followed by empty stack, '+' or '-', that means
	 * 				that complex number is either 1i or -1i
	 * 		2. if top character isn't 'i'. It is added at the start of the 
	 * 			string.
	 * 		3. When '+' or '-' is read method assumes that builded String
	 * 			represents a number and tries to parse it using Double.parse(string).
	 * 			If it doesn't throw exception, method check if isComplex flag was set to
	 * 			true, if it was number is added to imaginary part, otherwise to real part. 
	 * @param stack 
	 * @return new complex number parsed from stack
	 */
	private static ComplexNumber parseComplexFromStack(ObjectStack stack) {
		double newReal = 0.0;
		double newImaginary = 0.0;
		boolean isComplex = false;
		String parse = "";
		
		while(!stack.isEmpty()) {
			String popped = (String) stack.pop();
			
			if(popped.equals("i") && !isComplex && parse.isEmpty()) { //isComplex is checked so it can't parse 4ii;
																	  //parse.isEmpty is checked so it can't parse 4i2
				if(!stack.isEmpty()) {
					if(stack.peek().equals("+") || stack.peek().equals("-")) {
						newImaginary += Double.parseDouble(stack.pop() + "1.0");
						continue;
					}
				} else { //if last token == 'i'
					newImaginary++;    
					continue;
				}
				
				isComplex = true;
				continue;
			} else if(popped.equals("+") || popped.equals("-") || stack.isEmpty()) {
				parse = popped + parse;
				
				try {
					double number = Double.parseDouble(parse);
					
					if(isComplex) {
						newImaginary += number;
					} else {
						newReal += number;
					}
				} catch (NumberFormatException exc) {
					throw new IllegalArgumentException("String cannot be parsed as complex number.");
				} finally {
					parse = "";
					isComplex = false;
				}
			} else {
				parse = popped + parse;
			}
		}
		
		return new ComplexNumber(newReal, newImaginary);
	}
	
	public double getReal() {
		return real;
	}

	public double getImaginary() {
		return imaginary;
	}

	public double getMagnitude() {
		return magnitude;
	}

	public double getAngle() {
		return angle;
	}
	
	/**
	 * Method that creates new complex number
	 * by adding this complex number and c.
	 * @param c is complex number that is added to current complex number
	 * @return new complex number
	 * @throws NullPointerException if c == null
	 */
	public ComplexNumber add(ComplexNumber c) {
		if(c == null) throw new NullPointerException("Argument cannot be null.");
		
		return new ComplexNumber(
							this.real + c.getReal(),
							this.imaginary + c.getImaginary()
							);
	}
	
	/**
	 * Method that creates new complex number
	 * by subtracting c from current complex number
	 * @param c is complex number that is subtracted from current complex number
	 * @return new complex number
	 * @throws NullPointerException if c == null
	 */
	public ComplexNumber sub(ComplexNumber c) {
		if(c == null) throw new NullPointerException("Argument cannot be null.");
		
		return new ComplexNumber(
				this.real - c.getReal(),
				this.imaginary - c.getImaginary()
				);
	}
	
	/**
	 * Method creates new complex number by multiplying two
	 * complex numbers
	 * (a+bj) * (x+zi) = a*x + a*z*i + b*x*i+b*z*i^2
	 * 				   = a*x - b*z + i*(a*z+b*x)
	 * @param c other complex number
	 * @return new complex number
	 * @throws NullPointerException if c == null
	 */
	public ComplexNumber mul(ComplexNumber c) {
		if(c == null) throw new NullPointerException("Argument cannot be null.");
		
		double real = this.real * c.real - this.imaginary * c.imaginary;
		double imaginary = this.real * c.imaginary + c.real * this.imaginary;
		return new ComplexNumber(real, imaginary);
	}
	
	/**
	 * Method creates new complex number by dividing current
	 * complex number by c.
	 * z = a + b*i
	 * g = x + y*i
	 * 
	 * z/g = (z*conjugate(g))/(g*conjugate(g))
	 * g*conjugate(g) ==> imaginary part = 0
	 * @param c divisor
	 * @return new complex number
	 * @throws NullPointerException if c == null
	 * @throws IllegalArgumentException if magnitude of c is 0
	 */
	public ComplexNumber div(ComplexNumber c) {
		if(c == null) throw new NullPointerException("Argument cannot be null.");
		if(abs(c.getMagnitude()) <= 1E-6) 
			throw new IllegalArgumentException("Divisor cannot be 0. Was " + c);
		
		ComplexNumber cConjugate = c.conjugate();
		ComplexNumber dividend = this.mul(cConjugate);
		ComplexNumber divisor = c.mul(cConjugate); //imaginary part of divisor is 0
		
		double newReal = dividend.getReal() / divisor.getReal();
		double newImaginary = dividend.getImaginary() / divisor.getReal();
		
		return new ComplexNumber(newReal, newImaginary);
		
	}
	
	/**
	 * Method creates new complex number that is conjugate of
	 * current complex number
	 * conjugate(a+bi) = a-bi;
	 * @return conjugate of current complex number
	 */
	public ComplexNumber conjugate() {
		return new ComplexNumber(this.real, -this.imaginary);
	}
	
	/**
	 * Method is used to calculate power of current complex number
	 * and return it as new complex number.
	 * z = a + b*i
	 * z^n = magnitude^n(cos(n*angle) + i*sin(n*angle))
	 * @param n power
	 * @return current complex number to power of n
	 * @throws IllegalArgumentException if n is lower than 0
	 */
	public ComplexNumber power(int n) {
		if(n < 0) throw new IllegalArgumentException("Argument n must be 0 or higher. Was " + n + ".");
		
		double newAngle = this.angle * n;
		double newMagnitude = pow(this.magnitude, n);
		
		return fromMagnitudeAndAngle(newMagnitude, newAngle);
	}
	
	/**
	 * Method is used to calculate nth root of complex number. 
	 * @param n 
	 * @return array of result
	 * @throws IllegalArgumentException if argument n is lower or equal to 0
	 */
	public ComplexNumber[] root(int n) {
		if(n <= 0) throw new IllegalArgumentException("Argument n must be higher than 0. Was " + n + ".");
		
		ComplexNumber[] roots = new ComplexNumber[n];
		
		double newMagnitude = pow(this.magnitude, 1.0/n);
		for(int i = 0; i < n; i++) {
			double newAngle = (this.angle + 2 * i * PI)/n;
			
			roots[i] = fromMagnitudeAndAngle(newMagnitude, newAngle);
		}
		
		return roots;
	}
	

	
	@Override
	public String toString() {
		return String.format("(%.3f, %.3fi) == %.3f/_%.3f", real, imaginary, magnitude, angle).toString();
	}
	
}
