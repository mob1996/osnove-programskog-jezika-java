package hr.fer.zemris.java.custom.collections.demo;

import hr.fer.zemris.java.custom.collections.ArrayIndexedCollection;
import hr.fer.zemris.java.custom.collections.EmptyStackException;
import hr.fer.zemris.java.custom.collections.ObjectStack;

public class StackDemo {

	/**
	 * Static method is used to initialize array with
	 * allowed operators for stack calculation. 
	 * @return array with allowed operators
	 */
	private static ArrayIndexedCollection initOperators() {
		ArrayIndexedCollection operators =  new ArrayIndexedCollection(5);
		operators.add("+");
		operators.add("-");
		operators.add("/");
		operators.add("*");
		operators.add("%");
		return operators;
	}
	
	public static void main(String[] args) {
		if(args.length != 1) return;
		
		ArrayIndexedCollection operators = initOperators();
		
		ObjectStack calculateStack = new ObjectStack();
		String[] expression = args[0].split("\\s+");
		
		for(String exp:expression) {
			if(operators.contains(exp)) {
				try {
					Double secondArg = (Double) calculateStack.pop();
					Double firstArg = (Double) calculateStack.pop();
					double result = calculate(firstArg.doubleValue(), secondArg.doubleValue(), exp);
					calculateStack.push(Double.valueOf(result));
				} catch (EmptyStackException exc) {
					System.out.println("Number of arguments on stack was not enough.");
				} catch (IllegalArgumentException exc) {
					System.out.println(exc.getMessage());
				}
			} else {
				try {
					calculateStack.push(Double.valueOf(exp));
				} catch(NumberFormatException exc) {
					System.out.print("Invalid arguments. Argument " + exp + " is not supported.\nAbort!");
					return;
				}
			}
		}
		
		if(calculateStack.size() != 1) {
			System.out.println("Number of stack arguments is not 1.");
		} else {
			System.out.println(calculateStack.pop());
		}
		
	}
	
	/**
	 * Method is used to make calculations like
	 * x (operator) y
	 * @param x first argument
	 * @param y second argument
	 * @param operator
	 * @return result
	 */
	public static double calculate(double x, double y, String operator) {
		switch(operator) {
			case "+":
				return x + y;
			case "-":
				return x - y;
			case "*":
				return x * y;
			case "/":
				if(y == 0) throw new IllegalArgumentException("Cannot divide by zero. " + x + "/" + y );
				return x / y;
			case "%":
				return (int) x % (int) y;
			default:
				throw new IllegalArgumentException("Given operator argument is not allowed. Was " + operator);
		}
		
	}

}
