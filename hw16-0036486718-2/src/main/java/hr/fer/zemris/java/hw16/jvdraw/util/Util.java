package hr.fer.zemris.java.hw16.jvdraw.util;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import hr.fer.zemris.java.hw16.jvdraw.models.DrawingModel;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.visitors.GeometricalObjectVisitor;

/**
 * Class contains some utility methods.
 * @author Luka Mijić
 *
 */
public class Util {

	/**
	 * Method stores given {@link DrawingModel} to given
	 * {@link Path}.  
	 * @param model {@link DrawingModel}
	 * @throws IOException
	 */
	public static void saveDrawingModel(DrawingModel model, boolean saveAs) throws IOException {
		if(model.getSize() == 0) {
			JOptionPane.showMessageDialog(null
					, "Model is empty"
					, "Save error"
					, JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		if (SaveStorageProvider.getSaveLocation() == null || saveAs) {
			JFileChooser fc = new JFileChooser();
			fc.setDialogTitle("Save file");
			if(fc.showOpenDialog(null) != JFileChooser.APPROVE_OPTION) {
				return;
			}

			if(fc.getSelectedFile().toPath().toString().toLowerCase().endsWith(".jvd")){
				SaveStorageProvider.setSaveLocation(fc.getSelectedFile().toPath());
			} else {
				SaveStorageProvider.setSaveLocation(Paths.get(fc.getSelectedFile().toPath().toString() + ".jvd"));
			}
		}
		
		StringBuilder data = new StringBuilder();
		
		for(int i = 0; i < model.getSize(); i++) {
			if(i == model.getSize() - 1) {
				data.append(model.getObject(i).toJVD());
			} else {
				data.append(model.getObject(i).toJVD() + "\n");
			}
		}
		
		byte[] byteData = data.toString().getBytes(StandardCharsets.UTF_8);
		Files.write(SaveStorageProvider.getSaveLocation(), byteData);
	}
	
	/**
	 * Method opens ".jvd" file checks if it's not corrupted
	 * calls {@link Util#clearModel(DrawingModel)} on given model
	 * and fills it from data from file.
	 * @param model that is filled with data
	 * @throws IOException if error reading data occurs
	 */
	public static void openDrawingModel(DrawingModel model) throws IOException {
		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle("Open file");
		fc.setFileFilter(new FileNameExtensionFilter("jvd filter", "jvd"));
		if(fc.showOpenDialog(null) != JFileChooser.APPROVE_OPTION) {
			return;
		}
		Path openPath = fc.getSelectedFile().toPath();
		
		if(!openPath.toString().toLowerCase().endsWith(".jvd")) {
			throw new IllegalArgumentException("File must be like *.jvd");
		}
		
		List<String> lines = Files.readAllLines(openPath);
		List<GeometricalObject> objs = new ArrayList<>();
		for(String line:lines) {
			objs.add(JVDReader.parse(line));
		}
		
		if(objs.isEmpty()) {
			JOptionPane.showMessageDialog(null
										, "Model is empty"
										, "Open error"
										, JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		clearModel(model);
		for(GeometricalObject obj:objs) {
			model.add(obj);
		}
		
		SaveStorageProvider.setSaveLocation(openPath);
	}
	
	/**
	 * Clears all data from {@link DrawingModel}.
	 * @param model that is cleared
	 */
	public static void clearModel(DrawingModel model) {
		while(model.getSize() > 0) {
			model.remove(model.getObject(0));
		}
	}
	
	/**
	 * Visits every {@link GeometricalObject}s stored in
	 * {@link DrawingModel} and call {@link GeometricalObject#accept(GeometricalObjectVisitor)}
	 * on each of them.
	 * @param model {@link DrawingModel} that is visited
	 * @param v {@link GeometricalObjectVisitor}
	 */
	public static void modelVisits(DrawingModel model, GeometricalObjectVisitor v) {
		for(int i = 0, size = model.getSize(); i < size; i++) {
			model.getObject(i).accept(v);
		}
	}
	
	

}
