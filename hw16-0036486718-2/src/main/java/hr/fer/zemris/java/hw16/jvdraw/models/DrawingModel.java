package hr.fer.zemris.java.hw16.jvdraw.models;

import hr.fer.zemris.java.hw16.jvdraw.listeners.DrawingModelListener;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.GeometricalObject;

/**
 * This interface models simple drawing model.
 * This model stores different {@link GeometricalObject}s.
 * Several method are used for creating this model.
 * @author Luka Mijić
 *
 */
public interface DrawingModel {

	/**
	 * @return number of stored {@link GeometricalObject}.
	 */
	int getSize();
	
	/**
	 * Get {@link GeometricalObject} stored at given index.
	 * If there is no object stored at given index various method 
	 * for notifying user can be used. For example method could return
	 * null or throw an Exception.
	 * @param index of wanted object
	 * @return {@link GeometricalObject} stored at given index.
	 */
	GeometricalObject getObject(int index);
	
	/**
	 * Adds given {@link GeometricalObject} to model
	 * @param object that is stored
	 */
	void add(GeometricalObject object);
	
	/**
	 * Removes given {@link GeometricalObject} from model.
	 * @param object that is removed
	 */
	void remove(GeometricalObject object);
	
	/**
	 * Change position of {@link GeometricalObject} in
	 * model by given offset.
	 * @param object whose position is changed
	 * @param offset by how much position changes
 	 */
	void changeOrder(GeometricalObject object, int offset);
	
	/**
	 * Register {@link DrawingModelListener} listener to model.
	 * @param l registered listener
	 */
	void addDrawingModelListener(DrawingModelListener l);
	
	/**
	 * Remove given {@link DrawingModelListener} from model.
	 * @param l listener that is removed
	 */
	void removeDrawingModelListener(DrawingModelListener l);
}
