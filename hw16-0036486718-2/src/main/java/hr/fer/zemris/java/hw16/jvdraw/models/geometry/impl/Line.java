package hr.fer.zemris.java.hw16.jvdraw.models.geometry.impl;

import java.awt.Color;
import java.awt.Point;

import hr.fer.zemris.java.hw16.jvdraw.models.geometry.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.editors.GeometricalObjectEditor;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.editors.LineEditor;
import hr.fer.zemris.java.hw16.jvdraw.visitors.GeometricalObjectVisitor;

/**
 * This class extends {@link GeometricalObject} 
 * and it models a line. Line is defined by two 
 * {@link Point}s and a line {@link Color}.
 * @author Mob
 *
 */
public class Line extends GeometricalObject {

	/**
	 * Start {@link Point} of line
	 */
	private Point startPoint;
	
	/**
	 * End {@link Point} of line
	 */
	private Point endPoint;
	
	/**
	 * {@link Color} of line
	 */
	private Color lineColor;
	
	/**
	 * Create {@link Line} with given {@link Point}
	 * and {@link Color#BLACK}
	 * @param startPoint of line
	 * @param endPoint of line
	 */
	public Line(Point startPoint, Point endPoint) {
		this(startPoint, endPoint, Color.BLACK);
	}
	
	/**
	 * Create {@link Line} with given parameters.
	 * @param startPoint of line
	 * @param endPoint of line
	 * @param lineColor is colour of {@link Line}
	 */
	public Line(Point startPoint, Point endPoint, Color lineColor) {
		this.startPoint = startPoint;
		this.endPoint = endPoint;
		this.lineColor = lineColor;
	}

	/**
	 * @return line's start {@link Point}.
	 */
	public Point getStartPoint() {
		return startPoint;
	}

	/**
	 * @return line's end {@link Point}
	 */
	public Point getEndPoint() {
		return endPoint;
	}

	/**
	 * @return {@link Color} of line
	 */
	public Color getLineColor() {
		return lineColor;
	}
	
	/**
	 * Set this as new start {@link Point} of line and notify listeners.
	 * @param startPoint is new start {@link Point} of line
	 */
	public void setStartPoint(Point startPoint) {
		this.startPoint = startPoint;
		fire();
	}

	/**
	 * Set this as new end {@link Point} of line and notify listeners.
	 * @param startPoint is new end {@link Point} of line
	 */
	public void setEndPoint(Point endPoint) {
		this.endPoint = endPoint;
		fire();
	}

	/**
	 * Set this as new {@link Color} of line and notify listeners.
	 * @param lineColor is new {@link Color} of line
	 */
	public void setLineColor(Color lineColor) {
		this.lineColor = lineColor;
		fire();
	}

	@Override
	public void accept(GeometricalObjectVisitor v) {
		v.visit(this);
	}

	@Override
	public GeometricalObjectEditor createGeometricalObjectEditor() {
		return new LineEditor(this);
	}
	
	@Override
	public String toString() {
		return String.format("Line (%d, %d) - (%d, %d)" 
							, startPoint.x
							, startPoint.y
							, endPoint.x
							, endPoint.y
							);
	}
	
	@Override
	public String toJVD() {
		return String.format("LINE %d %d %d %d %d %d %d"
								, startPoint.x
								, startPoint.y
								, endPoint.x
								, endPoint.y
								, lineColor.getRed()
								, lineColor.getGreen()
								, lineColor.getBlue()
								);
	}
}
