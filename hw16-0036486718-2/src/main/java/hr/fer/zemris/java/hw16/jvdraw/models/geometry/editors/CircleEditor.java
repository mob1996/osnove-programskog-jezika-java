package hr.fer.zemris.java.hw16.jvdraw.models.geometry.editors;

import java.awt.GridLayout;
import java.awt.Point;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import hr.fer.zemris.java.hw16.jvdraw.components.JColorArea;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.impl.Circle;

/**
 * Extends {@link GeometricalObjectEditor}.
 * Creates {@link JPanel} that has components
 * for modifying stored {@link Circle}.
 * @author Luka Mijić
 *
 */
public class CircleEditor extends GeometricalObjectEditor {

	private static final long serialVersionUID = 1L;
	
	/**
	 * {@link Circle} that is edited
	 */
	private Circle circle;
	
	/**
	 * Field for editing x component of center
	 */
	private JTextField centerX;
	
	/**
	 * Field for editing y component of center
	 */
	private JTextField centerY;
	
	/**
	 * Field for editing radius
	 */
	private JTextField radius;
	
	/**
	 * {@link JColorArea} for editing circle outline colour
	 */
	private JColorArea outLineColorArea;

	/**
	 * Creates {@link CircleEditor} and
	 * calls {@link CircleEditor#initFields()} and {@link CircleEditor#createPanel()}.
	 * @param circle that is edited
	 */
	public CircleEditor(Circle circle) {
		this.circle = circle;
		initFields();
		createPanel();
	}
	
	/**
	 * Initialises editors components.
	 */
	private void initFields() {
		centerX = new JTextField(String.valueOf(circle.getCenter().x));
		centerY = new JTextField(String.valueOf(circle.getCenter().y));
		
		radius = new JTextField(String.valueOf(circle.getRadius()));
		outLineColorArea = new JColorArea(circle.getOutlineColor());
	}

	/**
	 * Adds all components to {@link JPanel}.
	 */
	private void createPanel() {
		setLayout(new GridLayout(0, 2));
		
		add(new JLabel("Center x: "));
		add(centerX);
		
		add(new JLabel("Center y:"));
		add(centerY);
		
		add(new JLabel("Radius: "));
		add(radius);
		
		add(new JLabel("Outline color:"));
		add(outLineColorArea);
	}
	
	/**
	 * Checks if values stored in components
	 * are valid.
	 */
	@Override
	public void checkEditing() {
		String message = "";
		try {
			message = "Center x input invalid. Input must be integer.";
			Integer.valueOf(centerX.getText().trim());
			message = "Center y input invalid. Input must be integer.";
			Integer.valueOf(centerY.getText().trim());
			message = "Invalid radius. Input must be integer.";
			Integer.valueOf(radius.getText().trim());
		} catch (NumberFormatException exc) {
			throw new NumberFormatException(message);
		}

	}

	/**
	 * Should be called if {@link CircleEditor#checkEditing()}
	 * doesn't throw any exceptions.
	 * This method saves made changes in {@link Circle}.
	 */
	@Override
	public void acceptEditing() {
		Point newCenter = new Point(Integer.valueOf(centerX.getText().trim())
									, Integer.valueOf(centerY.getText().trim()));
		
		int r = Integer.valueOf(radius.getText().trim());
		
		this.circle.setCenter(newCenter);
		this.circle.setRadius(r);
		this.circle.setOutlineColor(outLineColorArea.getCurrentColor());

	}

}
