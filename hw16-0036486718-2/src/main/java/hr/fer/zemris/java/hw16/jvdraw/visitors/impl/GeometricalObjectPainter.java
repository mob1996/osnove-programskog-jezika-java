package hr.fer.zemris.java.hw16.jvdraw.visitors.impl;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.Stroke;

import hr.fer.zemris.java.hw16.jvdraw.models.geometry.impl.Circle;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.impl.FilledCircle;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.impl.Line;
import hr.fer.zemris.java.hw16.jvdraw.visitors.GeometricalObjectVisitor;

/**
 * Implements {@link GeometricalObjectVisitor}.
 * On each visit drawn {@link GeometricalObjectVisitor}
 * using provided {@link Graphics2D}.
 * @author Luka Mijić
 *
 */
public class GeometricalObjectPainter implements GeometricalObjectVisitor {

	/**
	 * {@link Graphics2D} used for drawing
	 */
	private Graphics2D g2d;
	
	/**
	 * Creates {@link GeometricalObjectVisitor} with given {@link Graphics2D}.
	 * @param g2d {@link Graphics2D} used for drawing.
	 */
	public GeometricalObjectPainter(Graphics2D g2d) {
		this.g2d = g2d;
	}

	/**
	 * Draws {@link Line}.
	 * @param line is {@link Line} that is drawn.
	 */
	@Override
	public void visit(Line line) {
		g2d.setColor(line.getLineColor());
		g2d.drawLine(line.getStartPoint().x
					, line.getStartPoint().y
					, line.getEndPoint().x
					, line.getEndPoint().y);
	}

	/**
	 * Draws {@link Circle}.
	 * @param circle is {@link Circle} that is drawn.
	 */
	@Override
	public void visit(Circle circle) {
		g2d.setColor(circle.getOutlineColor());
		g2d.drawOval(circle.getCenter().x - circle.getRadius()
					, circle.getCenter().y - circle.getRadius()
					, 2 * circle.getRadius()
					, 2 * circle.getRadius());

	}

	/**
	 * Draws {@link FilledCircle}.
	 * @param filledCircle is {@link FilledCircle} that is drawn.
	 */
	@Override
	public void visit(FilledCircle filledCircle) {
		g2d.setColor(filledCircle.getFillColor());
		g2d.fillOval(filledCircle.getCenter().x - filledCircle.getRadius()
					, filledCircle.getCenter().y - filledCircle.getRadius()
					, 2 * filledCircle.getRadius()
					, 2 * filledCircle.getRadius());
		
		Stroke s = g2d.getStroke();
		g2d.setStroke(new BasicStroke(2));
		g2d.setColor(filledCircle.getOutlineColor());
		g2d.drawOval(filledCircle.getCenter().x - filledCircle.getRadius()
					, filledCircle.getCenter().y - filledCircle.getRadius()
					, 2 * filledCircle.getRadius()
					, 2 * filledCircle.getRadius());
		g2d.setStroke(s);
	}

}
