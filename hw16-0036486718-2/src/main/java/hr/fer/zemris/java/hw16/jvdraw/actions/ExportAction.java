package hr.fer.zemris.java.hw16.jvdraw.actions;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import hr.fer.zemris.java.hw16.jvdraw.models.DrawingModel;
import hr.fer.zemris.java.hw16.jvdraw.util.Util;
import hr.fer.zemris.java.hw16.jvdraw.visitors.impl.GeometricalObjectBBCalculator;
import hr.fer.zemris.java.hw16.jvdraw.visitors.impl.GeometricalObjectPainter;

/**
 * Class extends {@link AbstractAction}.
 * This class is used for exporting existing model
 * into "png", "jpg" or "gif" image.
 * @author Luka Mijić
 *
 */
public class ExportAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	/**
	 * Array of options
	 */
	private static final String[] OPTIONS = {"jpg", "png", "gif"};
	
	/**
	 * {@link DrawingModel} used for exporting
	 */
	private DrawingModel model;
	
	/**
	 * Creating {@link ExportAction} with given parameters.
	 * @param model that is used for exporting
	 */
	public ExportAction(DrawingModel model) {
		this.model = model;
	}
	
	/**
	 * If {@link DrawingModel#getSize()} returns zero exporting is canceled.
	 * Give user option to pick wanted export format.
	 * Let user choose export location.
	 * Export image.
	 * @param e {@link ActionEvent}
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if(model.getSize() == 0) {
			JOptionPane.showMessageDialog(null
					, "Nothing is drawn"
					, "Export error"
					, JOptionPane.ERROR_MESSAGE
			);
			return;
		}
		String format = (String) JOptionPane.showInputDialog(
								null
								, "Export extension?"
								, "Export"
								, JOptionPane.QUESTION_MESSAGE
								, null
								, OPTIONS
								, OPTIONS[0]);
		
		if(format == null) {
			return;
		}
		
		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle("Export file");
		if(fc.showOpenDialog(null) != JFileChooser.APPROVE_OPTION) {
			return;
		}
		
		Path imagePath;
		if(fc.getSelectedFile().toPath().toString().toLowerCase().endsWith("." + format)){
			imagePath = fc.getSelectedFile().toPath();
		} else {
			imagePath = Paths.get(fc.getSelectedFile().toPath().toString() + "." + format);
		}
		
		GeometricalObjectBBCalculator bbCalculator = new GeometricalObjectBBCalculator();
		Util.modelVisits(model, bbCalculator);

		Rectangle boundingBox = bbCalculator.boundingBox();
		BufferedImage image = new BufferedImage(boundingBox.width
												, boundingBox.height
												, BufferedImage.TYPE_3BYTE_BGR
		);
		
		Graphics2D g2d = image.createGraphics();
		g2d.setColor(Color.WHITE);
		g2d.fillRect(0, 0, boundingBox.width, boundingBox.height);
		g2d.translate(-boundingBox.x, -boundingBox.y);
		
		
		GeometricalObjectPainter painter = new GeometricalObjectPainter(g2d);
		Util.modelVisits(model, painter);
		
		g2d.dispose();
		
		try {
			ImageIO.write(image, format, imagePath.toFile());
		} catch (IOException exc) {
			JOptionPane.showMessageDialog(null
					, "Error saving image"
					, "Export error"
					, JOptionPane.ERROR_MESSAGE
			);
		}
	}

}
