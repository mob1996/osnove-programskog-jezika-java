package hr.fer.zemris.java.hw16.jvdraw.models;

import javax.swing.AbstractListModel;

import hr.fer.zemris.java.hw16.jvdraw.listeners.DrawingModelListener;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.GeometricalObject;

/**
 * Extends {@link AbstractListModel} and implements {@link DrawingModelListener}.
 * This model doesn't store any {@link GeometricalObject}s, instead
 * it acts as adapter to {@link DrawingModel}.
 * @author Luka Mijić
 *
 */
public class DrawingObjectListModel extends AbstractListModel<GeometricalObject> implements DrawingModelListener {

	private static final long serialVersionUID = 1L;

	/**
	 * Model
	 */
	private DrawingModel model;
	
	/**
	 * Creates new {@link DrawingObjectListModel} with given
	 * drawing {@link DrawingModel} and it registers itself on model
	 * @param model {@link DrawingModel}
	 */
	public DrawingObjectListModel(DrawingModel model) {
		this.model = model;
		this.model.addDrawingModelListener(this);
	}

	/**
	 * Calls {@link DrawingModel#getObject(int)} and
	 * givens received index as parameter.
	 * @param index of wanted element
	 */
	@Override
	public GeometricalObject getElementAt(int index) {
		return model.getObject(index);
	}

	/**
	 * Calls {@link DrawingModel#getSize()}
	 */
	@Override
	public int getSize() {
		return model.getSize();
	}

	/**
	 * Calls {@link DrawingObjectListModel#fireIntervalAdded(Object, int, int)}
	 * with given parameters.
	 * @param source {@link DrawingModel}
	 * @param index0 of first added object
	 * @param index1 of last added object
	 */
	@Override
	public void objectsAdded(DrawingModel source, int index0, int index1) {
		this.fireIntervalAdded(source, index0, index1);
		
	}

	/**
	 * Calls {@link DrawingObjectListModel#fireIntervalRemoved(Object, int, int)}
	 * with given parameters.
	 * @param source {@link DrawingModel}
	 * @param index0 of first removed object
	 * @param index1 of last removed object
	 */
	@Override
	public void objectsRemoved(DrawingModel source, int index0, int index1) {
		this.fireIntervalRemoved(source, index0, index1);
		
	}

	/**
	 * Calls {@link DrawingObjectListModel#fireContentsChanged(Object, int, int)}
	 * with given parameters.
	 * @param source {@link DrawingModel}
	 * @param index0 of first changed object
	 * @param index1 of last changed object
	 */
	@Override
	public void objectsChanged(DrawingModel source, int index0, int index1) {
		this.fireContentsChanged(source, index0, index1);
		
	}

}
