package hr.fer.zemris.java.hw16.jvdraw.models.geometry;

import java.util.ArrayList;
import java.util.List;

import hr.fer.zemris.java.hw16.jvdraw.listeners.GeometricalObjectListener;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.editors.GeometricalObjectEditor;
import hr.fer.zemris.java.hw16.jvdraw.visitors.GeometricalObjectVisitor;

/**
 * Abstract class models geometric object like
 * like, circle etc.
 * It stores listeners {@link GeometricalObjectListener}
 * and can accept requests from {@link GeometricalObjectVisitor}s.
 * Can create {@link GeometricalObjectEditor}.
 * @author Luka Mijić
 *
 */
public abstract class GeometricalObject {

	/**
	 * {@link List} of {@link GeometricalObjectListener}
	 */
	private List<GeometricalObjectListener> listeners = new ArrayList<>();
	
	/**
	 * Accepts request from {@link GeometricalObjectVisitor}.
	 * @param v used for calling appropriate {@link GeometricalObjectVisitor} method.
	 */
	public abstract void accept(GeometricalObjectVisitor v);
	
	/**
	 * @return appropriate {@link GeometricalObjectEditor}.
	 */
	public abstract GeometricalObjectEditor createGeometricalObjectEditor();
	
	/**
	 * @return .jdv form of {@link GeometricalObject}
	 */
	public abstract String toJVD();
	
	/**
	 * @param l {@link GeometricalObjectListener} that is stored
	 */
	public void addGeometricalObjectListener(GeometricalObjectListener l) {
		listeners.add(l);
	}
	
	/**
	 * @param l {@link GeometricalObjectListener} that is removed
	 */
	public void removeGeometricalObjectListener(GeometricalObjectListener l) {
		listeners.remove(l);
	}
	
	/**
	 * Notifies all listeners of change.
	 */
	protected void fire() {
		for(GeometricalObjectListener l:listeners) {
			l.geometricalObjectChanged(this);
		}
	}
}
