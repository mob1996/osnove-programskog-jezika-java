package hr.fer.zemris.java.hw16.jvdraw.components;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JComponent;
import javax.swing.JPanel;

import hr.fer.zemris.java.hw16.jvdraw.listeners.DrawingModelListener;
import hr.fer.zemris.java.hw16.jvdraw.models.DrawingModel;
import hr.fer.zemris.java.hw16.jvdraw.tools.ToolProvider;
import hr.fer.zemris.java.hw16.jvdraw.util.Util;
import hr.fer.zemris.java.hw16.jvdraw.visitors.GeometricalObjectVisitor;
import hr.fer.zemris.java.hw16.jvdraw.visitors.impl.GeometricalObjectPainter;

/**
 * Extends {@link JComponent} and implements {@link DrawingModelListener}.
 * Given {@link DrawingModel} is drawn on this components.
 * @author Luka Mijić
 *
 */
public class JDrawingCanvas extends JPanel implements DrawingModelListener {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Given model
	 */
	private DrawingModel model;

	/**
	 * Create new {@link JDrawingCanvas} with given {@link DrawingModel}.
	 * It creates new {@link MouseAdapter} that delegates
	 * it's method to appropriate {@link ToolProvider#getTool()} methods.
	 * @param model
	 */
	public JDrawingCanvas(DrawingModel model) {
		this.model = model;
		model.addDrawingModelListener(this);
		
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				ToolProvider.getTool().mousePressed(e);
			}
			
			@Override
			public void mouseReleased(MouseEvent e) {
				ToolProvider.getTool().mouseReleased(e);
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				ToolProvider.getTool().mouseClicked(e);
			}
		});
		
		this.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				ToolProvider.getTool().mouseMoved(e);
			}
			
			@Override
			public void mouseDragged(MouseEvent e) {
				ToolProvider.getTool().mouseDragged(e);
			}
		});
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		g2d.clearRect(0, 0, getWidth(), getHeight());
		GeometricalObjectVisitor v = new GeometricalObjectPainter(g2d);
		
		Util.modelVisits(model, v);
		
		ToolProvider.getTool().paint(g2d);
	}

	@Override
	public void objectsAdded(DrawingModel source, int index0, int index1) {
		repaint();
		
	}

	@Override
	public void objectsRemoved(DrawingModel source, int index0, int index1) {
		repaint();
		
	}

	@Override
	public void objectsChanged(DrawingModel source, int index0, int index1) {
		repaint();
		
	}
}
