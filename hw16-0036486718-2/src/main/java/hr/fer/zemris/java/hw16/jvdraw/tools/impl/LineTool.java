package hr.fer.zemris.java.hw16.jvdraw.tools.impl;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;

import hr.fer.zemris.java.hw16.jvdraw.models.DrawingModel;
import hr.fer.zemris.java.hw16.jvdraw.models.IColorProvider;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.impl.Circle;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.impl.Line;
import hr.fer.zemris.java.hw16.jvdraw.tools.Tool;

/**
 * Class extends {@link AbstractGeometricObjectTool}
 * and models tool used for drawing and storing {@link Line}s.
 * @author Luka Mijić
 *
 */
public class LineTool extends AbstractGeometricObjectTool {

	/**
	 * Model that stores created {@link Circle}s.
	 */
	private DrawingModel model;
	
	/**
	 * Provides line {@link Color}.
	 */
	private IColorProvider cProvider;
	
	/**
	 * Creates {@link LineTool} and stores given parameters.
	 * @param model that stores created {@link Line}
	 * @param cProvider provides {@link Line} colour.
	 */
	public LineTool(DrawingModel model, IColorProvider cProvider) {
		super();
		this.model = model;
		this.cProvider = cProvider;
	}

	/**
	 * @see Tool#paint(Graphics2D)
	 * Paints line. 
	 */
	@Override
	public void paint(Graphics2D g2d) {
		if(getFirstPoint() == null || getSecondPoint() == null) {
			return;
		}
		
		g2d.setColor(cProvider.getCurrentColor());
		g2d.drawLine(getFirstPoint().x, getFirstPoint().y, getSecondPoint().x, getSecondPoint().y);

	}

	/**
	 * Creates {@link Line} from given point.
	 * First {@link Point} is used as start {@link Point} of line
	 * and second {@link Point} is used as end {@link Point} of line.
	 * {@link IColorProvider} provides line colour.
	 * @param first is start {@link Point}
	 * @param second is end {@link Point} 
	 */
	@Override
	public GeometricalObject createGeometricalObject(Point first, Point second) {
		return new Line(getFirstPoint(), getSecondPoint(), cProvider.getCurrentColor());
	}

	/**
	 * Stores given {@link GeometricalObject} in {@link DrawingModel}.
	 * @param o is stored object
	 */
	@Override
	public void storeGeometricalObject(GeometricalObject o) {
		model.add(o);
	}

}
