package hr.fer.zemris.java.hw16.jvdraw.models.geometry.editors;

import javax.swing.JPanel;

import hr.fer.zemris.java.hw16.jvdraw.models.geometry.GeometricalObject;

/**
 * Class extends {@link JPanel} and
 * models a panel that is used for editing {@link GeometricalObject}s.
 * @author Luka Mijić
 *
 */
public abstract class GeometricalObjectEditor extends JPanel {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Check if editing information is valid
	 */
	public abstract void checkEditing();
	
	/**
	 * Should be called if {@link GeometricalObjectEditor#checkEditing()}
	 * didn't throw any errors. It accepts modifications and stores them.
	 */
	public abstract void acceptEditing();

}
