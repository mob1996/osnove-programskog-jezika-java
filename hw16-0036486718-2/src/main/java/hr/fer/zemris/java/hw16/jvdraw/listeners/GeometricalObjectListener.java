package hr.fer.zemris.java.hw16.jvdraw.listeners;

import hr.fer.zemris.java.hw16.jvdraw.models.geometry.GeometricalObject;

/**
 * This interface models listener to {@link GeometricalObject}.
 * @author Luka Mijić
 *
 */
public interface GeometricalObjectListener {
	
	/**
	 * This method is called when change occurs in
	 * given {@link GeometricalObject}.
	 * @param o {@link GeometricalObject}
	 */
	void geometricalObjectChanged(GeometricalObject o);
}
