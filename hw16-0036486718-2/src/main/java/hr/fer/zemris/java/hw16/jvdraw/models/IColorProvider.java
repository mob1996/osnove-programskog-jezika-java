package hr.fer.zemris.java.hw16.jvdraw.models;

import java.awt.Color;

import hr.fer.zemris.java.hw16.jvdraw.listeners.ColorChangeListener;

/**
 * This interface models object that can provide
 * {@link Color}. It also allows registration
 * of various {@link ColorChangeListener}s.
 * @author Luka Mijić
 *
 */
public interface IColorProvider {

	/**
	 * @return currently selected {@link Color}
	 */
	Color getCurrentColor();
	
	/**
	 * Saves reference to given {@link ColorChangeListener}.
	 * That reference is used for notifying listeners of changes. 
	 * @param l {@link ColorChangeListener} that is registered
	 */
	void addColorChangeListener(ColorChangeListener l);
	
	/**
	 * Removes given {@link ColorChangeListener} from 
	 * {@link IColorProvider}. If {@link IColorProvider} doesn't
	 * store reference to given listener, nothing happens.
	 * @param l {@link ColorChangeListener} that is removed
	 */
	void removeColorChangeListener(ColorChangeListener l);
}
