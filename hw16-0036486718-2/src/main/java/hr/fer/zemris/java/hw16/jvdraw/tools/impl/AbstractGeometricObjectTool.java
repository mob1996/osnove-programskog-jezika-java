package hr.fer.zemris.java.hw16.jvdraw.tools.impl;

import java.awt.Point;
import java.awt.event.MouseEvent;

import javax.swing.JComponent;

import hr.fer.zemris.java.hw16.jvdraw.models.geometry.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.tools.Tool;

/**
 * Abstract class implements all methods except
 * {@link Tool#paint(java.awt.Graphics2D)}.
 * It provides simple model used for drawing objects
 * with maximum of two {@link Point}.
 * @author Luka Mijić
 *
 */
public abstract class AbstractGeometricObjectTool implements Tool {
		
	/**
	 * First point
	 */
	private Point firstPoint = null;
	
	/**
	 * Second point
	 */
	private Point secondPoint = null;
	
	/**
	 * Flag set to true if mouse was clicked
	 */
	private boolean isClicked = false;
	
	/**
	 * @return first point
	 */
	public Point getFirstPoint() {
		return firstPoint;
	}
	
	/**
	 * @return second point
	 */
	public Point getSecondPoint() {
		return secondPoint;
	}
	
	/**
	 * Creates appropriate {@link GeometricalObject} from two {@link Point}s.
	 * @param first {@link Point}
	 * @param second {@link Point}
	 * @return created {@link GeometricalObject}.
	 */
	public abstract GeometricalObject createGeometricalObject(Point first, Point second);
	
	/**
	 * Stores {@link GeometricalObject}.
	 * @param o object {@link GeometricalObject}
	 */
	public abstract void storeGeometricalObject(GeometricalObject o);
	
	/**
	 * @see Tool#mousePressed(MouseEvent)
	 * If {@link Point}s are null references
	 * set them both to {@link MouseEvent#getPoint()}.
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		if(firstPoint == null) {
			firstPoint = e.getPoint();
			secondPoint = e.getPoint();
		}

	}

	/**
	 * @see Tool#mouseReleased(MouseEvent)
	 * If mouse was clicked this method does nothing.
	 * If mouse was pressed and dragged call
	 * {@link AbstractGeometricObjectTool#storeGeometricalObject(GeometricalObject)}
	 * on {@link AbstractGeometricObjectTool#createGeometricalObject(Point, Point)}.
	 * Set both {@link Point}s to null.
	 */
	@Override
	public void mouseReleased(MouseEvent e) {
		if(firstPoint.x == secondPoint.x && firstPoint.y == secondPoint.y) {
			return;
		}
		if(!isClicked) {
			storeGeometricalObject(createGeometricalObject(firstPoint, secondPoint));
			firstPoint = null;
			secondPoint = null;
		}
	}

	/**
	 * @see Tool#mouseClicked(MouseEvent)
	 * If <code>isClicked</code> flag is false, set it to true.
	 * Otherwise call {@link AbstractGeometricObjectTool#storeGeometricalObject(GeometricalObject)}
	 * on {@link AbstractGeometricObjectTool#createGeometricalObject(Point, Point)}.
	 * Set both {@link Point}s to null and <code>isClicked</code> to false.
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		if(!isClicked) {
			isClicked = true;
		} else {
			storeGeometricalObject(createGeometricalObject(firstPoint, secondPoint));
			firstPoint = null;
			secondPoint = null;
			isClicked = false;
		}

	}

	/**
	 * @see Tool#mouseMoved(MouseEvent)
	 * If mouse was not clicked this method does nothing.
	 * If it was update second {@link Point} with
	 * {@link MouseEvent#getPoint()} and call
	 * {@link JComponent#repaint()} on {@link MouseEvent#getComponent()}.
	 */
	@Override
	public void mouseMoved(MouseEvent e) {
		if(isClicked) {
			secondPoint = e.getPoint();
			e.getComponent().repaint();
		}
	}

	/**
	 * @see Tool#mouseDragged(MouseEvent)
	 * If mouse was clicked this method does nothing.
	 * If it was update second {@link Point} with
	 * {@link MouseEvent#getPoint()} and call
	 * {@link JComponent#repaint()} on {@link MouseEvent#getComponent()}.
	 */
	@Override
	public void mouseDragged(MouseEvent e) {
		if(!isClicked) {
			secondPoint = e.getPoint();
			e.getComponent().repaint();
		}

	}
	

}
