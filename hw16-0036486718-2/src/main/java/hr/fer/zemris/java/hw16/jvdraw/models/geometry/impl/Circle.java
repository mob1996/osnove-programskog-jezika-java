package hr.fer.zemris.java.hw16.jvdraw.models.geometry.impl;

import java.awt.Color;
import java.awt.Point;

import hr.fer.zemris.java.hw16.jvdraw.models.geometry.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.editors.CircleEditor;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.editors.GeometricalObjectEditor;
import hr.fer.zemris.java.hw16.jvdraw.visitors.GeometricalObjectVisitor;

/**
 * This class extends {@link GeometricalObject} and it
 * models circle object. This circle is defined by
 * center, radius and outline colour
 * @author Luka Mijić
 *
 */
public class Circle extends GeometricalObject {

	/**
	 * Center of circle
	 */
	private Point center;
	
	/**
	 * Radius of circle
	 */
	private int radius;
	
	/**
	 * Circle outline colour
	 */
	private Color outlineColor;
	
	/**
	 * Creates {@link Circle} with given parameters and
	 * outline colour {@link Color#BLACK}.
	 * @param center of circle
	 * @param radius of circle
	 */
	public Circle(Point center, int radius) {
		this(center, radius, Color.BLACK);
	}
	
	/**
	 * Creates {@link Circle} with given parameters.
	 * @param center of circle
	 * @param radius of circle
	 * @param outlineColor is circle outline colour
	 */
	public Circle(Point center, int radius, Color outlineColor) {
		super();
		this.center = center;
		this.radius = radius;
		this.outlineColor = outlineColor;
	}

	/**
	 * @return center
	 */
	public Point getCenter() {
		return center;
	}

	/**
	 * @return radius
	 */
	public int getRadius() {
		return radius;
	}

	/**
	 * @return circle's outline colour
	 */
	public Color getOutlineColor() {
		return outlineColor;
	}
	
	/**
	 * Set new center of circle and notify listeners.
	 * @param center new center value
	 */
	public void setCenter(Point center) {
		this.center = center;
		fire();
	}

	/**
	 * Set new radius of circle and notify listeners.
	 * @param radius is new radius value
	 */
	public void setRadius(int radius) {
		this.radius = radius;
		fire();
	}

	/**
	 * Set new outline colour of circle and notify listeners.
	 * @param outlineColor is new outlineColor value
	 */
	public void setOutlineColor(Color outlineColor) {
		this.outlineColor = outlineColor;
		fire();
	}

	@Override
	public void accept(GeometricalObjectVisitor v) {
		v.visit(this);
	}

	@Override
	public GeometricalObjectEditor createGeometricalObjectEditor() {
		return new CircleEditor(this);
	}
	
	@Override
	public String toString() {
		return String.format("Circle (%d, %d), %d"
							, center.x
							, center.y
							, radius);
	}

	@Override
	public String toJVD() {
		return String.format("CIRCLE %d %d %d %d %d %d"
							, center.x
							, center.y
							, radius
							, outlineColor.getRed()
							, outlineColor.getGreen()
							, outlineColor.getBlue()
							);
	}

	

}
