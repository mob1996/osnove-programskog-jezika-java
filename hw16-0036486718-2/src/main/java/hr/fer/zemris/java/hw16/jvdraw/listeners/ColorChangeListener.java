package hr.fer.zemris.java.hw16.jvdraw.listeners;

import java.awt.Color;

import hr.fer.zemris.java.hw16.jvdraw.models.IColorProvider;

/**
 * This listener is used to observer
 * colour changes in {@link IColorProvider}.
 * @author Luka Mijić
 *
 */
public interface ColorChangeListener {

	/**
	 * This method is called {@link IColorProvider} notifies
	 * it's {@link ColorChangeListener} of selected {@link Color} 
	 * changes.
	 * @param source of change
	 * @param oldColor previous selected {@link Color}
	 * @param newColor current selected {@link Color}
	 */
	void newColorSelected(IColorProvider source, Color oldColor, Color newColor);
}
