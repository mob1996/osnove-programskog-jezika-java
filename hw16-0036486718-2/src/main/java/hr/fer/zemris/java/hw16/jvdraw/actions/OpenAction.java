package hr.fer.zemris.java.hw16.jvdraw.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import hr.fer.zemris.java.hw16.jvdraw.models.DrawingModel;
import hr.fer.zemris.java.hw16.jvdraw.util.Util;

/**
 * Class extends {@link AbstractAction}.
 * This class is used for opening ".jvd"
 * files from disk.
 * @author Luka Mijić
 *
 */
public class OpenAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	/**
	 * {@link DrawingModel} that is loaded
	 */
	private DrawingModel model;
	
	/**
	 * Creates {@link OpenAction}.
	 * @param model that is loaded
	 */
	public OpenAction(DrawingModel model) {
		this.model = model;
	}
	
	/**
	 * Calls {@link Util#openDrawingModel(DrawingModel)}
	 * If errors occurs show dialog message.
	 * @param e {@link ActionEvent}.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			Util.openDrawingModel(model);
		} catch (Exception exc) {
			JOptionPane.showMessageDialog(null
					, exc.getMessage()
					, "Open error"
					, JOptionPane.ERROR_MESSAGE
			);
		}
		
	}

}
