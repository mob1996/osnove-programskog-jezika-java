package hr.fer.zemris.java.hw16.jvdraw;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import hr.fer.zemris.java.hw16.jvdraw.actions.ExportAction;
import hr.fer.zemris.java.hw16.jvdraw.actions.OpenAction;
import hr.fer.zemris.java.hw16.jvdraw.actions.SaveAction;
import hr.fer.zemris.java.hw16.jvdraw.actions.SaveAsAction;
import hr.fer.zemris.java.hw16.jvdraw.components.ColorLabel;
import hr.fer.zemris.java.hw16.jvdraw.components.JColorArea;
import hr.fer.zemris.java.hw16.jvdraw.components.JDrawingCanvas;
import hr.fer.zemris.java.hw16.jvdraw.listeners.DrawingModelListener;
import hr.fer.zemris.java.hw16.jvdraw.models.DrawingModel;
import hr.fer.zemris.java.hw16.jvdraw.models.DrawingModelImpl;
import hr.fer.zemris.java.hw16.jvdraw.models.DrawingObjectListModel;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.editors.GeometricalObjectEditor;
import hr.fer.zemris.java.hw16.jvdraw.tools.ToolProvider;
import hr.fer.zemris.java.hw16.jvdraw.tools.impl.CircleTool;
import hr.fer.zemris.java.hw16.jvdraw.tools.impl.FilledCircleTool;
import hr.fer.zemris.java.hw16.jvdraw.tools.impl.LineTool;
import hr.fer.zemris.java.hw16.jvdraw.util.Util;

/**
 * Class extends {@link JFrame}.
 * This frame is used for drawing various {@link GeometricalObject}s.
 * It provides various functionalities.
 * @author Luka Mijić
 *
 */
public class JVDraw extends JFrame {

	private static final long serialVersionUID = 1L;

	/**
	 * Main {@link JPanel} of application
	 */
	private JPanel mainPanel;
	
	/**
	 * {@link JColorArea} for picking foreground colour
	 */
	private JColorArea foreground;
	
	/**
	 * {@link JColorArea} for picking background colour
	 */
	private JColorArea background;
	
	/**
	 * {@link DrawingModel} that is drawn on screen
	 */
	private DrawingModel model;
	
	/**
	 * {@link SaveAction}
	 */
	private Action saveAction;
	
	/**
	 * {@link SaveAction}
	 */
	private Action saveAsAction;
	
	/**
	 * {@link OpenAction}
	 */
	private Action openAction;
	
	/***
	 * {@link ExportAction}
	 */
	private Action exportAction;
	
	/**
	 * Creates {@link JVDraw}.
	 */
	public JVDraw() {
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setSize(800, 800);
		
		
		setup();
		initGUI();
		initExitSequence();
	}
	
	/**
	 * Creates {@link WindowAdapter} with overridden
	 * {@link WindowAdapter#windowClosed(WindowEvent)}.
	 * Checks if {@link DrawingModel} was modified and if it was
	 * prompt user to save it. Otherwise just disposes this frame.
	 */
	private void initExitSequence() {
		WindowAdapter closingAdapter = new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				if(saveAction.isEnabled()) {
					int result = JOptionPane.showConfirmDialog(JVDraw.this
							, "Save drawing?"
							, "Saving"
							, JOptionPane.YES_NO_CANCEL_OPTION);
			
					if (result == JOptionPane.CANCEL_OPTION) {
						return;
					} else if(result == JOptionPane.YES_OPTION) {
						try {
							Util.saveDrawingModel(model, false);
						} catch (IOException e1) {
							JOptionPane.showMessageDialog(null
									, "Error saving drawing"
									, "Save error"
									, JOptionPane.ERROR_MESSAGE);
						}
						return;
					}
				}
				JVDraw.this.dispose();
			}
		};

		addWindowListener(closingAdapter);
	}
	
	/**
	 * Sets up {@link JColorArea}s, {@link DrawingModel},
	 * various actions etc.
	 */
	private void setup() {
		foreground = new JColorArea(Color.RED);
		background = new JColorArea(Color.BLUE);
		model = new DrawingModelImpl();
		
		this.saveAction = new SaveAction(model);
		this.saveAction.putValue(Action.NAME, "Save");
		this.saveAction.putValue(Action.SHORT_DESCRIPTION, "Saves drawing");
		this.saveAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control S"));
		this.saveAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_S);
		this.saveAction.setEnabled(false);
		
		this.saveAsAction = new SaveAsAction(model);
		this.saveAsAction.putValue(Action.NAME, "Save As");
		this.saveAsAction.putValue(Action.SHORT_DESCRIPTION, "Saves drawing to choosen location.");
		this.saveAsAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control A"));
		this.saveAsAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_A);
		
		this.openAction = new OpenAction(model);
		this.openAction.putValue(Action.NAME, "Open");
		this.openAction.putValue(Action.SHORT_DESCRIPTION, "Opens *.jvd file.");
		this.openAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control O"));
		this.openAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_O);
		
		this.exportAction = new ExportAction(model);
		this.exportAction.putValue(Action.NAME, "Export");
		this.exportAction.putValue(Action.SHORT_DESCRIPTION, "Exports drawing..");
		this.exportAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control E"));
		this.exportAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_E);
		
		DrawingModelListener modified = new DrawingModelListener() {
			
			@Override
			public void objectsRemoved(DrawingModel source, int index0, int index1) {
				JVDraw.this.saveAction.setEnabled(true);
			}
			
			@Override
			public void objectsChanged(DrawingModel source, int index0, int index1) {
				JVDraw.this.saveAction.setEnabled(true);
			}
			
			@Override
			public void objectsAdded(DrawingModel source, int index0, int index1) {
				JVDraw.this.saveAction.setEnabled(true);
			}
		};
		model.addDrawingModelListener(modified);
	}
	
	/**
	 * Initialises GUI. Fills {@link JVDraw#getContentPane()}
	 * with various components like {@link JDrawingCanvas}, {@link JMenu},
	 * {@link JToolBar}, {@link JLabel} etc.
	 */
	private void initGUI() {
		mainPanel = new JPanel(new BorderLayout());
		mainPanel.setBackground(Color.WHITE);
		getContentPane().add(mainPanel, BorderLayout.CENTER);
		
		JDrawingCanvas canvas = new JDrawingCanvas(model);
		mainPanel.add(canvas, BorderLayout.CENTER);
		
		createToolBar();
		createMenu();
		createColorLabel();
		createJList();
		
	}
	
	/**
	 * Creates applications toolbar.
	 */
	private void createToolBar() {
		JToolBar toolBar = new JToolBar();
		toolBar.setFloatable(true);
		
		toolBar.add(foreground);
		toolBar.add(background);
		
		ButtonGroup group = new ButtonGroup();
		
		JToggleButton lineToggle = new JToggleButton("Line");
		JToggleButton circleToggle = new JToggleButton("Circle");
		JToggleButton filledToggle = new JToggleButton("Filled Circle");
		
		group.add(lineToggle);
		group.add(circleToggle);
		group.add(filledToggle);
		
		lineToggle.addActionListener(e -> ToolProvider.setTool(new LineTool(model, foreground)));
		circleToggle.addActionListener(e -> ToolProvider.setTool(new CircleTool(model, foreground)));
		filledToggle.addActionListener(e -> ToolProvider.setTool(new FilledCircleTool(model, foreground, background)));
		
		lineToggle.doClick();
		
		toolBar.add(lineToggle);
		toolBar.add(circleToggle);
		toolBar.add(filledToggle);
		
		mainPanel.add(toolBar, BorderLayout.PAGE_START);
	}
	
	/**
	 * Creates applications menu bar.
	 */
	private void createMenu() {
		JMenuBar menuBar = new JMenuBar();
		
		JMenu file = new JMenu("File");
		
		file.add(new JMenuItem(openAction));
		file.addSeparator();
		file.add(new JMenuItem(saveAction));
		file.add(new JMenuItem(saveAsAction));
		file.addSeparator();
		file.add(new JMenuItem(exportAction));
		
		menuBar.add(file);
		setJMenuBar(menuBar);
	}
	
	/**
	 * Creates label that shows currently selected colours.
	 */
	private void createColorLabel() {
		ColorLabel cLabel = new ColorLabel(foreground, background);
		cLabel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
		cLabel.setHorizontalAlignment(JLabel.CENTER);
		
		getContentPane().add(cLabel, BorderLayout.PAGE_END);
	}
	
	/**
	 * Creates {@link JList} that holds contents from 
	 * {@link DrawingModel}. Adds key bindings for various operations
	 * with {@link JList}. 
	 */
	private void createJList() {
		JList<GeometricalObject> list = new JList<>(new DrawingObjectListModel(model));
		list.setBorder(BorderFactory.createLineBorder(Color.BLACK, 5));
		list.setCellRenderer(new DefaultListCellRenderer() {
			private static final long serialVersionUID = 1L;

			@Override
			public Component getListCellRendererComponent(JList<?> list,
                    Object value, int index, boolean isSelected,
                    boolean cellHasFocus) {
				JLabel listCellRendererComponent = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                listCellRendererComponent.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0,Color.BLACK));
                return listCellRendererComponent;
			}
		});
		
		list.setFocusable(true);
		list.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "DELETE");
		list.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_PLUS, 0), "SHIFT_UP");
		list.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_MINUS, 0), "SHIFT_DOWN");
		
		list.getActionMap().put("DELETE", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				int[] selectedIndexes = list.getSelectedIndices();
				
				int deleted = 0;
				for(int i = 0; i < selectedIndexes.length; i++) {
					try {
						model.remove(model.getObject(selectedIndexes[i] - deleted));
						deleted++;
					} catch (IndexOutOfBoundsException ignorable) {
					}
					
				}
			}
		});
		list.getActionMap().put("SHIFT_UP", new AbstractAction() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				int index = list.getSelectedIndex();
				if(index == -1) return;
				
				try {
					model.changeOrder(model.getObject(index), 1);
				} catch(IndexOutOfBoundsException ignorable) {
				}
			}
		});
		
		list.getActionMap().put("SHIFT_DOWN", new AbstractAction() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				int index = list.getSelectedIndex();
				if(index == -1) return;
				
				try {
					model.changeOrder(model.getObject(index), -1);
				} catch(IndexOutOfBoundsException ignorable) {
				}
			}
		});
		
		list.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount() < 2) {
					return;
				}
				
				int index = list.getSelectedIndex();
				if(index == -1) return;
				
				GeometricalObjectEditor editor = model.getObject(index).createGeometricalObjectEditor();
				int result = JOptionPane.showConfirmDialog(JVDraw.this
															, editor
															, "Edit"
															, JOptionPane.OK_CANCEL_OPTION);
				if(result == JOptionPane.OK_OPTION) {
					try {
						editor.checkEditing();
						editor.acceptEditing();
					} catch (Exception exc) {
						JOptionPane.showMessageDialog(JVDraw.this
													, exc.getMessage()
													, "Editing error"
													, JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		
		JScrollPane scrollPane = new JScrollPane(list);
		mainPanel.add(scrollPane, BorderLayout.EAST);
	}
	
	/**
	 * Starts application.
	 * @param args no arguments needed
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new JVDraw().setVisible(true));

	}

}
