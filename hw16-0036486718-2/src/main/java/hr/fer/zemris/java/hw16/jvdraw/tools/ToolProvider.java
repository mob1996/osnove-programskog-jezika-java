package hr.fer.zemris.java.hw16.jvdraw.tools;

/**
 * Class is used for providing appropriate {@link Tool}
 * across application.
 * @author Luka Mijić
 *
 */
public class ToolProvider {

	/**
	 * {@link Tool}
	 */
	private static Tool tool = null;
	
	/**
	 * @return appropriate {@link Tool}
	 * @throws IllegalStateException if {@link Tool} was not set up.
	 */
	public static Tool getTool() {
		if(tool == null) {
			throw new IllegalStateException("No tool set up.");
		}
		return tool;
	}
	
	/**
	 * @param t is new {@link Tool} value that provider returns.
	 */
	public static void setTool(Tool t) {
		tool = t;
	}
	
}
