package hr.fer.zemris.java.hw16.jvdraw.visitors.impl;

import java.awt.Point;
import java.awt.Rectangle;

import hr.fer.zemris.java.hw16.jvdraw.models.geometry.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.impl.Circle;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.impl.FilledCircle;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.impl.Line;
import hr.fer.zemris.java.hw16.jvdraw.visitors.GeometricalObjectVisitor;

/**
 * Implements {@link GeometricalObjectVisitor}. 
 * Calculates bounding box that encapsulates all
 * {@link GeometricalObject}s.
 * @author Luka Mijić
 *
 */
public class GeometricalObjectBBCalculator implements GeometricalObjectVisitor {

	/**
	 * Left top {@link Point} of bounding box
	 */
	private Point leftTop;
	
	/**
	 * Right bottom {@link Point} of bounding box
	 */
	private Point rightBot;
	
	/**
	 * Calculates bounding box based on given {@link Line}.
	 * @param line is {@link Line} used for calculating bounding box.
	 */
	@Override
	public void visit(Line line) {
		if(leftTop == null || rightBot == null) {
			leftTop = new Point(line.getStartPoint().x, line.getEndPoint().y);
			rightBot = new Point(line.getStartPoint().x, line.getEndPoint().y);
		}
		
		checkLeft(line.getStartPoint());
		checkRight(line.getStartPoint());
		
		checkLeft(line.getEndPoint());
		checkRight(line.getEndPoint());
	}

	/**
	 * Calculates bounding box based on given {@link Circle}.
	 * @param circle is {@link Circle} used for calculating bounding box.
	 */
	@Override
	public void visit(Circle circle) {
		if(leftTop == null || rightBot == null) {
			leftTop = new Point(circle.getCenter().x, circle.getCenter().y);
			rightBot = new Point(circle.getCenter().x, circle.getCenter().y);
		}
		
		int r = circle.getRadius();
		Point circleLeftTop = new Point(circle.getCenter().x - r, circle.getCenter().y - r);
		Point circleRightBot = new Point(circle.getCenter().x + r, circle.getCenter().y + r);
		
		checkLeft(circleLeftTop);
		checkRight(circleLeftTop);
		checkLeft(circleRightBot);
		checkRight(circleRightBot);
	}

	/**
	 * Calculates bounding box based on given {@link FilledCircle}.
	 * @param filledCircle is {@link FilledCircle} used for calculating bounding box.
	 */
	@Override
	public void visit(FilledCircle filledCircle) {
		if(leftTop == null || rightBot == null) {
			leftTop = new Point(filledCircle.getCenter().x, filledCircle.getCenter().y);
			rightBot = new Point(filledCircle.getCenter().x, filledCircle.getCenter().y);
		}
		
		
		int r = filledCircle.getRadius();
		Point circleLeftTop = new Point(filledCircle.getCenter().x - r, filledCircle.getCenter().y - r);
		Point circleRightBot = new Point(filledCircle.getCenter().x + r, filledCircle.getCenter().y + r);
		
		checkLeft(circleLeftTop);
		checkRight(circleLeftTop);
		checkLeft(circleRightBot);
		checkRight(circleRightBot);
	}
	
	/**
	 * @return calculated bounding box
	 * @throws IllegalStateException if bounding box was not calculated
	 */
	public Rectangle boundingBox() {
		if(leftTop == null || rightBot == null) {
			throw new IllegalStateException("No checking happened");
		}
		
		return new Rectangle(leftTop.x, leftTop.y, rightBot.x - leftTop.x, rightBot.y - leftTop.y);
	}
	
	/**
	 * Resets calculation
	 */
	public void reset() {
		leftTop = null;
		rightBot = null;
	}

	/**
	 * Check if given {@link Point} is encapsulated
	 * by bounding box and if it is change left top {@link Point}
	 * of bounding box.
	 * @param p {@link Point} for which bounding box is checked
	 */
	private void checkLeft(Point p) {
		if(leftTop.x > p.x) {
			leftTop.x = p.x;
		}
		
		if(leftTop.y > p.y) {
			leftTop.y = p.y;
		}
	}
	
	/**
	 * Check if given {@link Point} is encapsulated
	 * by bounding box and if it is change right bottom {@link Point}
	 * of bounding box.
	 * @param p {@link Point} for which bounding box is checked
	 */
	private void checkRight(Point p) {
		if(rightBot.x < p.x) {
			rightBot.x = p.x;
		}
		
		if(rightBot.y < p.y) {
			rightBot.y = p.y;
		}
	}
}
