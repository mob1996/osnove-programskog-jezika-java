package hr.fer.zemris.java.hw16.jvdraw.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.swing.JColorChooser;
import javax.swing.JComponent;

import hr.fer.zemris.java.hw16.jvdraw.listeners.ColorChangeListener;
import hr.fer.zemris.java.hw16.jvdraw.models.IColorProvider;

/**
 * Extends {@link JComponent} and implements {@link IColorProvider}.
 * This is a component whose painting area is fully painted by 
 * single colour. Colour can be changed by clicking on component area.
 * When component area is clicked {@link JColorChooser} opens.
 * @author Luka Mijić
 *
 */
public class JColorArea extends JComponent implements IColorProvider {

	/**
	 * This {@link MouseAdapter} overrides only
	 * {@link MouseAdapter#mouseClicked(MouseEvent)} method.
	 * @author Luka Mijić
	 *
	 */
	private class ColorChooser extends MouseAdapter {
		
		/**
		 * Opens {@link JColorChooser} and let's
		 * user choose wanted colour. If user quits
		 * nothing happens, otherwise call {@link JColorArea#setCurrentColor(Color)}
		 * with selected colour.
		 * @param e {@link MouseEvent}
		 */
		@Override
		public void mouseClicked(MouseEvent e) {
			Color c = JColorChooser.showDialog(JColorArea.this
											   , "Pick new color"
											   , JColorArea.this.selectedColor
											   );
			
			if(c != null) {
				JColorArea.this.setCurrentColor(c);
			}
		}
	}
	
	private static final long serialVersionUID = 1L;

	/**
	 * Currently selected colour
	 */
	private Color selectedColor;
	
	/**
	 * {@link List} of {@link ColorChangeListener} registered to 
	 * this {@link IColorProvider}.
	 */
	private List<ColorChangeListener> listeners;
	
	/**
	 * Creates new {@link JColorArea}.
	 * @param color is default colour
	 */
	public JColorArea(Color color) {
		this.selectedColor = Objects.requireNonNull(color, "Selected color must not be null ref.");
		this.listeners = new ArrayList<>();
		this.addMouseListener(new ColorChooser());
	}

	/**
	 * Given parameter is used as new colour that {@link IColorProvider}
	 * provides. It also notifies all listeners of this change.
	 * @param color is new selected colour
	 */
	public void setCurrentColor(Color color) {
		Objects.requireNonNull(color, "Given parameter must not be null.");
		Color oldColor = this.selectedColor;
		this.selectedColor = color;
		fire(oldColor);
		repaint();
	}

	/**
	 * Notifies all {@link ColorChangeListener} of change.
	 * @param oldColor is previously used colour
	 */
	private void fire(Color oldColor) {
		for(ColorChangeListener l:listeners) {
			l.newColorSelected(this, oldColor, selectedColor);
		}
	}
	
	@Override
	public void addColorChangeListener(ColorChangeListener l) {
		listeners.add(l);
	}

	@Override
	public void removeColorChangeListener(ColorChangeListener l) {
		listeners.remove(l);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		Insets ins = getInsets();
		Rectangle bounds = this.getBounds();
		
		g2d.setColor(selectedColor);
		g2d.fillRect(ins.left, ins.top, bounds.width, bounds.height);
	}
	
	@Override
	public Dimension getMaximumSize() {
		return getPreferredSize();
	}
	
	@Override
	public Dimension getMinimumSize() {
		return getPreferredSize();
	}
	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(15, 15);
	}

	@Override
	public Color getCurrentColor() {
		return selectedColor;
	}	
}
