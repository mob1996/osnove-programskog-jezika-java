package hr.fer.zemris.java.hw16.jvdraw.tools.impl;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Stroke;

import hr.fer.zemris.java.hw16.jvdraw.models.DrawingModel;
import hr.fer.zemris.java.hw16.jvdraw.models.IColorProvider;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.impl.FilledCircle;
import hr.fer.zemris.java.hw16.jvdraw.tools.Tool;

/**
 * Class extends {@link AbstractGeometricObjectTool}
 * and models tool used for drawing and storing {@link FilledCircle}s.
 * @author Luka Mijić
 *
 */
public class FilledCircleTool extends AbstractGeometricObjectTool {

	/**
	 * Model that stores created {@link FilledCircle}s.
	 */
	private DrawingModel model;
	
	/**
	 * Provides {@link Color} of {@link FilledCircle}s outline.
	 */
	private IColorProvider outlineProvider;
	
	/**
	 * Provides {@link Color} of {@link FilledCircle}s fill.
	 */
	private IColorProvider fillProvider;
	
	/**
	 * Creates {@link FilledCircleTool} and stores given parameters.
	 * @param model that stores created {@link FilledCircle}
	 * @param outlineProvider provides {@link FilledCircle} outline colour.
	 * @param fillProvider provides {@link FilledCircle} fill colour.
	 */
	public FilledCircleTool(DrawingModel model, IColorProvider outlineProvider, IColorProvider fillProvider) {
		this.model = model;
		this.outlineProvider = outlineProvider;
		this.fillProvider = fillProvider;
	}

	/**
	 * @see Tool#paint(Graphics2D)
	 * Paints filled circle. 
	 */
	@Override
	public void paint(Graphics2D g2d) {
		if(getFirstPoint() == null || getSecondPoint() == null) {
			return;
		}
		
		g2d.setColor(fillProvider.getCurrentColor());
		
		int radius = calculateRadius();
		int startX  = getFirstPoint().x - radius;
		int startY = getFirstPoint().y - radius;
		
		Stroke s = g2d.getStroke();
		g2d.fillOval(startX, startY, 2 * radius, 2 * radius);
		g2d.setColor(outlineProvider.getCurrentColor());
		g2d.setStroke(new BasicStroke(2));
		g2d.drawOval(startX, startY, 2 * radius, 2 * radius);
		g2d.setStroke(s);
	}

	/**
	 * Creates {@link FilledCircle} from given point.
	 * First point is used as center and distance between
	 * first and second point defines radius and stored
	 * One {@link IColorProvider} provides outline colour.
	 * Second One {@link IColorProvider} provides fill colour.
	 * @param first is center {@link Point}
	 * @param second is {@link Point} that together with first point defines radius
	 */
	@Override
	public GeometricalObject createGeometricalObject(Point first, Point second) {
		return new FilledCircle(getFirstPoint()
								, calculateRadius()
								, outlineProvider.getCurrentColor()
								, fillProvider.getCurrentColor()
								);
	}

	/**
	 * Stores given {@link GeometricalObject} in {@link DrawingModel}.
	 * @param o is stored object
	 */
	@Override
	public void storeGeometricalObject(GeometricalObject o) {
		model.add(o);
	}
	
	/**
	 * @return circles radius from two points.
	 */
	private int calculateRadius() {
		int xDif = getFirstPoint().x - getSecondPoint().x;
		int yDif = getFirstPoint().y - getSecondPoint().y;
		return (int) Math.sqrt(xDif * xDif + yDif * yDif);
	}

}
