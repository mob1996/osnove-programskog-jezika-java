package hr.fer.zemris.java.hw16.jvdraw.actions;

import java.awt.event.ActionEvent;
import java.io.IOException;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import hr.fer.zemris.java.hw16.jvdraw.models.DrawingModel;
import hr.fer.zemris.java.hw16.jvdraw.util.Util;

/**
 * Class extends {@link AbstractAction}.
 * This class is used for saving given {@link DrawingModel}
 * to location that user chooses.
 * @author Luka Mijić
 *
 */
public class SaveAsAction extends AbstractAction {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * {@link DrawingModel} that is saved
	 */
	private DrawingModel model;

	/**
	 * Creates {@link SaveAsAction}.
	 * @param model that is saved
	 */
	public SaveAsAction(DrawingModel model) {
		this.model = model;
	}
	
	/**
	 * Calls {@link Util#saveDrawingModel(DrawingModel, boolean)}
	 * with saveAs boolean parameter set as true.
	 * @param e {@link ActionEvent}
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			Util.saveDrawingModel(model, true);
		} catch (IOException e1) {
			JOptionPane.showMessageDialog(null
											, "Error saving drawing"
											, "Save error"
											, JOptionPane.ERROR_MESSAGE);
		}
	}

}
