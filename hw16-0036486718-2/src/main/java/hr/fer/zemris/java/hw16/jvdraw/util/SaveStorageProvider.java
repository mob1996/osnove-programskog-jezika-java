package hr.fer.zemris.java.hw16.jvdraw.util;

import java.nio.file.Path;

/**
 * Class provides save location {@link Path}.
 * @author Luka Mijić
 *
 */
public class SaveStorageProvider {

	/**
	 * Provided {@link Path}
	 */
	private static Path saveLocation = null;
	
	/**
	 * @return save location {@link Path}
	 */
	public static Path getSaveLocation() {
		return saveLocation;
	}
	
	/**
	 * @param saveLoc is new save location
	 */
	public static void setSaveLocation(Path saveLoc) {
		saveLocation = saveLoc;
	}
}
