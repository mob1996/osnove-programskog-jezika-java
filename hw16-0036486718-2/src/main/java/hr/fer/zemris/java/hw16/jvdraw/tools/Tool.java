package hr.fer.zemris.java.hw16.jvdraw.tools;

import java.awt.Graphics2D;
import java.awt.event.MouseEvent;

/**
 * Models interface used for drawing
 * with mouse.
 * @author Luka Mijić
 *
 */
public interface Tool {

	/**
	 * Method is called when mouse is pressed.
	 * @param e {@link MouseEvent}
	 */
	void mousePressed(MouseEvent e);
	
	/**
	 * Method is called when mouse is released.
	 * @param e {@link MouseEvent}
	 */
	void mouseReleased(MouseEvent e);
	
	/**
	 * Method is called when mouse is clicked.
	 * @param e {@link MouseEvent}
	 */
	void mouseClicked(MouseEvent e);
	
	/**
	 * Method is called when mouse is moved.
	 * @param e {@link MouseEvent}
	 */
	void mouseMoved(MouseEvent e);
	
	/**
	 * Method is called when mouse is dragged.
	 * @param e {@link MouseEvent}
	 */
	void mouseDragged(MouseEvent e);
	
	/**
	 * Method is used for painting current state of {@link Tool}.
	 * @param g2d {@link Graphics2D} used for drawing.
	 */
	void paint(Graphics2D g2d);
}
