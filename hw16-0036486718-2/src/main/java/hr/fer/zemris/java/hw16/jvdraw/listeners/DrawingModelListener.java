package hr.fer.zemris.java.hw16.jvdraw.listeners;

import hr.fer.zemris.java.hw16.jvdraw.models.DrawingModel;

/**
 * This interface models listener to {@link DrawingModel}.
 * @author Luka Mijić
 *
 */
public interface DrawingModelListener {
	
	/**
	 * This method is called when objects are added into 
	 * {@link DrawingModel}. 
	 * @param source {@link DrawingModel} where objects are added
	 * @param index0 index of first added object
	 * @param index1 of last added object
	 */
	void objectsAdded(DrawingModel source, int index0, int index1);
	
	/**
	 * This method is called when objects are removed from
	 * {@link DrawingModel}. 
	 * @param source {@link DrawingModel} from which objects are removed
	 * @param index0 index of first removed object
	 * @param index1 of last removed object
	 */
	void objectsRemoved(DrawingModel source, int index0, int index1);
	
	/**
	 * This method is called when objects are changed in
	 * {@link DrawingModel}. 
	 * @param source {@link DrawingModel} where change happened.
	 * @param index0 index of changed object.
	 * @param index1 of last changed object.
	 */
	void objectsChanged(DrawingModel source, int index0, int index1);
}
