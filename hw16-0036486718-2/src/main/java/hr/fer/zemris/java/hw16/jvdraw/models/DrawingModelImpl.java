package hr.fer.zemris.java.hw16.jvdraw.models;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import hr.fer.zemris.java.hw16.jvdraw.listeners.DrawingModelListener;
import hr.fer.zemris.java.hw16.jvdraw.listeners.GeometricalObjectListener;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.GeometricalObject;

/**
 * Implementation of {@link DrawingModel}.
 * {@link GeometricalObject}s are stored in {@link List}.
 * @author Luka Mijić
 *
 */
public class DrawingModelImpl implements DrawingModel, GeometricalObjectListener {
	
	/**
	 * {@link List} that stores {@link GeometricalObject}.
	 */
	private List<GeometricalObject> objects = new ArrayList<>();
	
	/**
	 * {@link List} that stores {@link DrawingModelListener}s.
	 */
	private List<DrawingModelListener> listeners = new ArrayList<>();
	
	/**
	 * @see DrawingModel#getSize()
	 */
	@Override
	public int getSize() {
		return objects.size();
	}

	/**
	 * @see DrawingModel#getObject(int)
	 * In this implementation error occurs while trying
	 * to get index that is out of bounds.
	 */
	@Override
	public GeometricalObject getObject(int index) {
		return objects.get(index);
	}

	/**
	 * @see DrawingModel#add(GeometricalObject)
	 * Also {@link DrawingModelListener} registers itself
	 * on given {@link GeometricalObject} and notifies
	 * interested {@link DrawingModelListener}s of added object.
	 */
	@Override
	public void add(GeometricalObject object) {
		objects.add(object);
		object.addGeometricalObjectListener(this);
		int index = objects.size() - 1;
		fire(l -> l.objectsAdded(this, index, index));
	}

	/**
	 * @see DrawingModel#remove(GeometricalObject)
	 * Also {@link DrawingModelListener} deregisters itself
	 * on given {@link GeometricalObject} and notifies
	 * interested {@link DrawingModelListener}s of removed object.
	 */
	@Override
	public void remove(GeometricalObject object) {
		int index = objects.indexOf(object);
		if(index == -1) {
			throw new IllegalArgumentException("No such object in model.");
		}
		
		objects.remove(object);
		object.removeGeometricalObjectListener(this);
		fire(l -> l.objectsRemoved(this, index, index));
	}

	/**
	 * @see DrawingModel#changeOrder(GeometricalObject, int)
	 * @throws IllegalArgumentException if given object is not stored
	 * @throws IndexOutOfBoundsException if new index is out of bounds
	 */
	@Override
	public void changeOrder(GeometricalObject object, int offset) {
		int objIndex = objects.indexOf(object);
		int newIndex = objIndex + offset;
		if(objIndex == -1) {
			throw new IllegalArgumentException("No such object in model.");
		} else if(newIndex < 0 || newIndex >= objects.size()) {
			throw new IndexOutOfBoundsException("New index is out of bounds.");
		}
		
		objects.remove(object);
		fire(l -> l.objectsRemoved(this, objIndex, objIndex));
		objects.add(newIndex, object);
		fire(l -> l.objectsAdded(this, newIndex, newIndex));
	}

	@Override
	public void geometricalObjectChanged(GeometricalObject o) {
		int index = objects.indexOf(o);
		if(index == -1) {
			throw new IllegalArgumentException("No such object.");
		}
		
		fire(l -> l.objectsChanged(this, index, index));
	}
	
	@Override
	public void addDrawingModelListener(DrawingModelListener l) {
		listeners.add(l);
		
	}

	@Override
	public void removeDrawingModelListener(DrawingModelListener l) {
		listeners.remove(l);
	}

	/**
	 * Notifies all stored listeners.
	 * @param consumer used for calling appropriate {@link DrawingModelListener} methods.
	 */
	private void fire(Consumer<DrawingModelListener> consumer) {
		for(DrawingModelListener l:listeners) {
			consumer.accept(l);
		}
	}

}
