package hr.fer.zemris.java.hw16.jvdraw.components;

import java.awt.Color;

import javax.swing.JLabel;

import hr.fer.zemris.java.hw16.jvdraw.listeners.ColorChangeListener;
import hr.fer.zemris.java.hw16.jvdraw.models.IColorProvider;

/**
 * This class extends {@link JLabel} and implements
 * {@link ColorChangeListener} class.
 * This class is basically label that shows
 * currently selected foreground and background colour.
 * Colours are provided with {@link IColorProvider}. 
 * @author Luka Mijić
 *
 */
public class ColorLabel extends JLabel implements ColorChangeListener {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Foreground provider
	 */
	private IColorProvider foregroundColor;
	
	/**
	 * Background provider
	 */
	private IColorProvider backgroundColor;
	
	/**
	 * Creates {@link ColorLabel}.
	 * It registers label to given providers.
	 * @param foregroundColor provider of foreground colour
	 * @param backgroundColor provider of background colour
	 */
	public ColorLabel(IColorProvider foregroundColor, IColorProvider backgroundColor) {
		this.foregroundColor = foregroundColor;
		this.backgroundColor = backgroundColor;
		
		foregroundColor.addColorChangeListener(this);
		backgroundColor.addColorChangeListener(this);
		
		newColorSelected(null, null, null);
	}

	/**
	 * Parameters don't really matter in this implementation.
	 * When change happens in any of the providers
	 * label massage is changed.
	 * @param source of change
	 * @param oldColor of provider
	 * @param newColor of provider
	 */
	@Override
	public void newColorSelected(IColorProvider source, Color oldColor, Color newColor) {
		String message = String.format(
						 	"Foreground color: %s, background color: %s"
							, colorToRGBString(foregroundColor.getCurrentColor())
							, colorToRGBString(backgroundColor.getCurrentColor())
							);
		this.setText(message);
		
	}
	
	/**
	 * Creates {@link String} from {@link Color}
	 * that is formed like this (R, G, B).
	 * @param color which is transformer
	 * @return created {@link String}
 	 */
	private String colorToRGBString(Color color) {
		return String.format("(%d, %d, %d)", color.getRed(), color.getGreen(), color.getBlue());
	}
	
	
	
	

}
