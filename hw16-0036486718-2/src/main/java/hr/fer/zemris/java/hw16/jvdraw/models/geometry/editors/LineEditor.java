package hr.fer.zemris.java.hw16.jvdraw.models.geometry.editors;

import java.awt.GridLayout;
import java.awt.Point;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import hr.fer.zemris.java.hw16.jvdraw.components.JColorArea;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.impl.Line;

/**
 * Extends {@link GeometricalObjectEditor}.
 * Creates {@link JPanel} that has components
 * for modifying stored {@link Line}.
 * @author Luka Mijić
 *
 */
public class LineEditor extends GeometricalObjectEditor {

	private static final long serialVersionUID = 1L;
	
	/**
	 * {@link Line} that is being edited
	 */
	private Line line;
	
	/**
	 * Field for editing x component of line's start point
	 */
	private JTextField startX;
	
	/**
	 * Field for editing y component of line's start point
	 */
	private JTextField startY;
	
	/**
	 * Field for editing x component of line's end point
	 */
	private JTextField endX;
	
	/**
	 * Field for editing y component of line's end point
	 */
	private JTextField endY;
	
	/**
	 * {@link JColorArea} for editing line colour
	 */
	private JColorArea lineColorArea;
	
	/**
	 * Creates {@link LineEditor} and calls
	 * {@link LineEditor#initFields()} and {@link LineEditor#createPanel()}.
	 * @param line
	 */
	public LineEditor(Line line) {
		this.line = line;
		initFields();
		createPanel();
	}
	
	/**
	 * Initialises editors components.
	 */
	private void initFields() {
		startX = new JTextField(String.valueOf(line.getStartPoint().x));
		startY = new JTextField(String.valueOf(line.getStartPoint().y));
		endX = new JTextField(String.valueOf(line.getEndPoint().x));
		endY = new JTextField(String.valueOf(line.getEndPoint().y));
		
		startX.setColumns(10);
		startY.setColumns(10);
		endX.setColumns(10);
		endY.setColumns(10);
		
		lineColorArea = new JColorArea(line.getLineColor());
	}
	
	/**
	 * Adds all components to {@link JPanel}.
	 */
	private void createPanel() {
		setLayout(new GridLayout(0, 2));
		
		add(new JLabel("Start x: "));
		add(startX);
		
		add(new JLabel("Start y: "));
		add(startY);
		
		add(new JLabel("End x: "));
		add(endX);
		
		add(new JLabel("End y: "));
		add(endY);
	
		add(new JLabel("Line Color: "));
		add(lineColorArea);
	}
	
	/**
	 * Checks if values stored in components
	 * are valid.
	 */
	@Override
	public void checkEditing() {
		String message = "";
		try {
			message = "Start x input invalid. Input must be integer.";
			Integer.valueOf(startX.getText().trim());
			message = "Start y input invalid. Input must be integer.";
			Integer.valueOf(startY.getText().trim());
			message = "End x input invalid. Input must be integer.";
			Integer.valueOf(endX.getText().trim());
			message = "End y input invalid. Input must be integer.";
			Integer.valueOf(endY.getText().trim());
		} catch (NumberFormatException exc) {
			throw new NumberFormatException(message);
		}
		
	}

	/**
	 * Should be called if {@link LineEditor}.
	 * doesn't throw any exceptions.
	 * This method saves made changes in {@link Line}.
	 */
	@Override
	public void acceptEditing() {
		Point newStart = new Point(Integer.valueOf(startX.getText().trim())
									, Integer.valueOf(startY.getText().trim()));
		
		Point newEnd = new Point(Integer.valueOf(endX.getText().trim())
				, Integer.valueOf(endY.getText().trim()));
		
		line.setStartPoint(newStart);
		line.setEndPoint(newEnd);
		line.setLineColor(lineColorArea.getCurrentColor());
		
	}

}
