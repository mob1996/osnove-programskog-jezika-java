package hr.fer.zemris.java.hw16.jvdraw.models.geometry.editors;

import java.awt.GridLayout;
import java.awt.Point;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import hr.fer.zemris.java.hw16.jvdraw.components.JColorArea;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.impl.FilledCircle;

/**
 * Extends {@link GeometricalObjectEditor}.
 * Creates {@link JPanel} that has components
 * for modifying stored {@link FilledCircle}.
 * @author Luka Mijić
 *
 */
public class FilledCircleEditor extends GeometricalObjectEditor {

	private static final long serialVersionUID = 1L;
	
	/**
	 * {@link FilledCircle} that is editied
	 */
	private FilledCircle filledCircle;
	
	/**
	 * Field for editing x component of center
	 */
	private JTextField centerX;
	
	/**
	 * Field for editing y component of center
	 */
	private JTextField centerY;
	
	/**
	 * Field for editing radius
	 */
	private JTextField radius;
	
	/**
	 * {@link JColorArea} for editing circle outline colour
	 */
	private JColorArea outLineColorArea;
	
	/**
	 * {@link JColorArea} for editing circles fill colour
	 */
	private JColorArea fillColorArea;
	
	/**
	 * Creates {@link FilledCircleEditor} and calls
	 * {@link FilledCircleEditor#initFields()} and
	 * {@link FilledCircleEditor#createPanel()}.
	 * 
	 */
	public FilledCircleEditor(FilledCircle filledCircle) {
		this.filledCircle = filledCircle;
		initFields();
		createPanel();
	}

	/**
	 * Initialises editors components.
	 */
	private void initFields() {
		centerX = new JTextField(String.valueOf(filledCircle.getCenter().x));
		centerY = new JTextField(String.valueOf(filledCircle.getCenter().y));
		
		radius = new JTextField(String.valueOf(filledCircle.getRadius()));
		outLineColorArea = new JColorArea(filledCircle.getOutlineColor());
		fillColorArea = new JColorArea(filledCircle.getFillColor());
	}
	
	/**
	 * Adds all components to {@link JPanel}.
	 */
	private void createPanel() {
		setLayout(new GridLayout(0, 2));
		
		add(new JLabel("Center x: "));
		add(centerX);
		
		add(new JLabel("Center y:"));
		add(centerY);
		
		add(new JLabel("Radius: "));
		add(radius);
		
		add(new JLabel("Outline color:"));
		add(outLineColorArea);
		
		add(new JLabel("Fill color:"));
		add(fillColorArea);
	}
	
	/**
	 * Checks if values stored in components
	 * are valid.
	 */
	@Override
	public void checkEditing() {
		String message = "";
		try {
			message = "Center x input invalid. Input must be integer.";
			Integer.valueOf(centerX.getText().trim());
			message = "Center y input invalid. Input must be integer.";
			Integer.valueOf(centerY.getText().trim());
			message = "Invalid radius. Input must be integer.";
			Integer.valueOf(radius.getText().trim());
		} catch (NumberFormatException exc) {
			throw new NumberFormatException(message);
		}
		
	}

	/**
	 * Should be called if {@link FilledCircleEditor#checkEditing()}
	 * doesn't throw any exceptions.
	 * This method saves made changes in {@link FilledCircle}.
	 */
	@Override
	public void acceptEditing() {
		Point newCenter = new Point(Integer.valueOf(centerX.getText().trim())
				, Integer.valueOf(centerY.getText().trim()));

		int r = Integer.valueOf(radius.getText().trim());

		this.filledCircle.setCenter(newCenter);
		this.filledCircle.setRadius(r);
		this.filledCircle.setOutlineColor(outLineColorArea.getCurrentColor());
		this.filledCircle.setFillColor(fillColorArea.getCurrentColor());
		
	}

}
