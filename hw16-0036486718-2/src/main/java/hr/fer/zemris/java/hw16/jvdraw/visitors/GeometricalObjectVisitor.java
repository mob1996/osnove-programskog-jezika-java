package hr.fer.zemris.java.hw16.jvdraw.visitors;

import hr.fer.zemris.java.hw16.jvdraw.models.geometry.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.impl.Circle;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.impl.FilledCircle;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.impl.Line;

/**
 * Interface models Visitor pattern for visiting
 * {@link GeometricalObject}s. 
 * @author Luka Mijić
 *
 */
public interface GeometricalObjectVisitor {
	
	/**
	 * Is called when {@link Line} is visited.
	 * @param line is visited {@link Line}
	 */
	void visit(Line line);
	
	/**
	 * Is called when {@link Circle} is visited.
	 * @param circle is visited {@link Circle}
	 */
	void visit(Circle circle);
	
	/**
	 * Is called when {@link FilledCircle} is visited
	 * @param filledCircle is visited {@link FilledCircle}
	 */
	void visit(FilledCircle filledCircle);
}
