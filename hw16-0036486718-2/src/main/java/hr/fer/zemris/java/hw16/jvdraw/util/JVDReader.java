package hr.fer.zemris.java.hw16.jvdraw.util;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import hr.fer.zemris.java.hw16.jvdraw.models.geometry.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.impl.Circle;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.impl.FilledCircle;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.impl.Line;

/**
 * This class knows how to parse 
 * lines from ".jvd" files.
 * @author Luka Mijić
 *
 */
public class JVDReader {

	/**
	 * Stores {@link Function}s that can turn {@link List} of
	 * {@link Integer}s into {@link GeometricalObject}s.
	 */
	private static Map<String, Function<List<Integer>, GeometricalObject>> objCreators;
	
	static {
		objCreators = new HashMap<>();
		
		Function<List<Integer>, GeometricalObject> lineCreator
						= (l) -> {
							if(l.size() != 7) {
								throw new IllegalArgumentException("Invalid argument number for LINE.");
							}
							Point startPoint = new Point(l.get(0), l.get(1));
							Point endPoint = new Point(l.get(2), l.get(3));
							int red = l.get(4);
							int green = l.get(5);
							int blue = l.get(6);
							
							Color color = new Color(red, green, blue);
							
							return new Line(startPoint, endPoint, color);
						};
						
		Function<List<Integer>, GeometricalObject> circleCreator
						= (c) -> {
							if(c.size() != 6) {
								throw new IllegalArgumentException("Invalid argument number for CIRCLE.");
							}
							Point centerPoint = new Point(c.get(0), c.get(1));
							int radius = c.get(2);
							
							int red = c.get(3);
							int green = c.get(4);
							int blue = c.get(5);
							
							Color color = new Color(red, green, blue);
							
							return new Circle(centerPoint, radius, color);
						};
						
		Function<List<Integer>, GeometricalObject> filledCreator
						= (c) -> {
							if(c.size() != 9) {
								throw new IllegalArgumentException("Invalid argument number for FCIRCLE.");
							}
							Point centerPoint = new Point(c.get(0), c.get(1));
							int radius = c.get(2);
							
							int red1 = c.get(3);
							int green1 = c.get(4);
							int blue1 = c.get(5);
							
							Color outLineColor = new Color(red1, green1, blue1);
							
							int red2 = c.get(6);
							int green2 = c.get(7);
							int blue2 = c.get(8);
							
							Color fillColor = new Color(red2, green2, blue2);
							
							return new FilledCircle(centerPoint, radius, outLineColor, fillColor);
						};
		objCreators.put("LINE", lineCreator);
		objCreators.put("CIRCLE", circleCreator);
		objCreators.put("FCIRCLE", filledCreator);
	}
	
	/**
	 * Parses valid ".jvd" file into appropriate {@link GeometricalObject}.
	 * @param line that is parsed
	 * @return {@link GeometricalObject}
	 */
	public static GeometricalObject parse(String line) {
		String[] params = line.split("\\s+");
		if(params.length <= 1) {
			throw new IllegalArgumentException("No parameters given.");
		}
		
		Function<List<Integer>, GeometricalObject> objCreator = objCreators.get(params[0]);
		if(objCreator == null) {
			throw new IllegalArgumentException("'" + params[0] + "'" + " is not geometrical object.");
		}
		
		List<Integer> funcParams = new ArrayList<>();
		for(int i = 1; i < params.length; i++) {
			try {
				funcParams.add(Integer.valueOf(params[i]));
			} catch (NumberFormatException exc) {
				throw new NumberFormatException("Can't parse line.");
			}
		}
		
		return objCreator.apply(funcParams);
		
	}
}
