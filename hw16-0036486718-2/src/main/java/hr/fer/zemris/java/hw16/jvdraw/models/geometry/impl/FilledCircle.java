package hr.fer.zemris.java.hw16.jvdraw.models.geometry.impl;

import java.awt.Color;
import java.awt.Point;

import hr.fer.zemris.java.hw16.jvdraw.models.geometry.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.editors.FilledCircleEditor;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.editors.GeometricalObjectEditor;
import hr.fer.zemris.java.hw16.jvdraw.visitors.GeometricalObjectVisitor;

/**
 * This class extends {@link GeometricalObject} and it
 * models filled circle object. This circle is defined by
 * center, radius, outline colour and fill colour.
 * @author Luka Mijić
 *
 */
public class FilledCircle extends GeometricalObject {

	/**
	 * Center of circle
	 */
	private Point center;
	
	/**
	 * Radius of circle
	 */
	private int radius;
	
	/**
	 * Circle outline colour
	 */
	private Color outlineColor;
	
	/**
	 * Circle fill colour
	 */
	private Color fillColor;
	
	/**
	 * Creates {@link FilledCircle} with given parameters.
	 * Fill and outline colour is {@link Color#BLACK}
	 * @param center of circle
	 * @param radius of circle
	 */
	public FilledCircle(Point center, int radius) {
		this(center, radius, Color.BLACK, Color.BLACK);
	}
	
	/**
	 * Creates {@link FilledCircle} with given parameters.
	 * @param center of circle
	 * @param radius of circle
	 * @param outlineColor is circle outline colour
	 * @param fillColor is circle's fill colour
	 */
	public FilledCircle(Point center, int radius, Color outlineColor, Color fillColor) {
		super();
		this.center = center;
		this.radius = radius;
		this.outlineColor = outlineColor;
		this.fillColor = fillColor;
	}

	/**
	 * @return center
	 */
	public Point getCenter() {
		return center;
	}

	/**
	 * @return radius
	 */
	public int getRadius() {
		return radius;
	}

	/**
	 * @return circle's outline colour
	 */
	public Color getOutlineColor() {
		return outlineColor;
	}

	/**
	 * @return circle's fill colour
	 */
	public Color getFillColor() {
		return fillColor;
	}

	/**
	 * Set new center of circle and notify listeners.
	 * @param center new center value
	 */
	public void setCenter(Point center) {
		this.center = center;
		fire();
	}

	/**
	 * Set new radius of circle and notify listeners.
	 * @param radius is new radius value
	 */
	public void setRadius(int radius) {
		this.radius = radius;
		fire();
	}

	/**
	 * Set new outline colour of circle and notify listeners.
	 * @param outlineColor is new outlineColor value
	 */
	public void setOutlineColor(Color outlineColor) {
		this.outlineColor = outlineColor;
		fire();
	}

	/**
	 * Set new fill colour of circle and notify listeners.
	 * @param outlineColor is new outlineColor value
	 */
	public void setFillColor(Color fillColor) {
		this.fillColor = fillColor;
		fire();
	}

	@Override
	public void accept(GeometricalObjectVisitor v) {
		v.visit(this);
	}

	@Override
	public GeometricalObjectEditor createGeometricalObjectEditor() {
		return new FilledCircleEditor(this);
	}

	@Override
	public String toString() {
		return String.format("Filled circle (%d, %d), %d, %s"
							, center.x
							, center.y
							, radius
							, String.format("#%06x", fillColor.getRGB() & 0x00FFFFFF)
							);
	}

	@Override
	public String toJVD() {
		return String.format("FCIRCLE %d %d %d %d %d %d %d %d %d"
				, center.x
				, center.y
				, radius
				, outlineColor.getRed()
				, outlineColor.getGreen()
				, outlineColor.getBlue()
				, fillColor.getRed()
				, fillColor.getGreen()
				, fillColor.getBlue()
				);
	}
}
