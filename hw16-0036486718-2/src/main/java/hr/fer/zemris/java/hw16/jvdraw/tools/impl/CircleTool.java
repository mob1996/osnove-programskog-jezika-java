package hr.fer.zemris.java.hw16.jvdraw.tools.impl;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;

import hr.fer.zemris.java.hw16.jvdraw.models.DrawingModel;
import hr.fer.zemris.java.hw16.jvdraw.models.IColorProvider;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.models.geometry.impl.Circle;
import hr.fer.zemris.java.hw16.jvdraw.tools.Tool;

/**
 * Class extends {@link AbstractGeometricObjectTool}
 * and models tool used for drawing and storing {@link Circle}s.
 * @author Luka Mijić
 *
 */
public class CircleTool extends AbstractGeometricObjectTool {

	/**
	 * Model that stores created {@link Circle}s.
	 */
	private DrawingModel model;
	
	/**
	 * Provides {@link Color} of {@link Circle}s outline.
	 */
	private IColorProvider cProvider;
	
	/**
	 * Creates {@link CircleTool} and stores given parameters.
	 * @param model that stores created {@link Circle}
	 * @param cProvider provides {@link Circle} colour.
	 */
	public CircleTool(DrawingModel model, IColorProvider cProvider) {
		this.model = model;
		this.cProvider = cProvider;
	}
	
	/**
	 * @see Tool#paint(Graphics2D)
	 * Paints circle. 
	 */
	@Override
	public void paint(Graphics2D g2d) {
		if(getFirstPoint() == null || getSecondPoint() == null) {
			return;
		}
		g2d.setColor(cProvider.getCurrentColor());
		
		int radius = calculateRadius();
		int startX  = getFirstPoint().x - radius;
		int startY = getFirstPoint().y - radius;
		
		g2d.drawOval(startX, startY, 2 * radius, 2 * radius);
		
	}

	/**
	 * Creates {@link Circle} from given point.
	 * First point is used as center and distance between
	 * first and second point defines radius and stored
	 * {@link IColorProvider} provides outline colour.
	 * @param first is center {@link Point}
	 * @param second is {@link Point} that together with first point defines radius
	 */
	@Override
	public GeometricalObject createGeometricalObject(Point first, Point second) {
		return new Circle(getFirstPoint(), calculateRadius(), cProvider.getCurrentColor());
	}

	/**
	 * Stores given {@link GeometricalObject} in {@link DrawingModel}.
	 * @param o is stored object
	 */
	@Override
	public void storeGeometricalObject(GeometricalObject o) {
		model.add(o);
	}
	
	/**
	 * @return circles radius from two points.
	 */
	private int calculateRadius() {
		int xDif = getFirstPoint().x - getSecondPoint().x;
		int yDif = getFirstPoint().y - getSecondPoint().y;
		return (int) Math.sqrt(xDif * xDif + yDif * yDif);
	}

}
