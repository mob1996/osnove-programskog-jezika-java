package hr.fer.zemris.java.hw16.search.shell;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * Provides interface for user
 * to communicate with program.
 * @author Luka Mijić
 *
 */
public class SimpleEnvironment {

	/**
	 * Input Stream
	 */
	private BufferedReader input;
	
	/**
	 * Output stream
	 */
	private BufferedWriter output;
	
	public SimpleEnvironment() {
		this.input = new BufferedReader(new InputStreamReader(System.in));
		this.output = new BufferedWriter(new OutputStreamWriter(System.out));
	}
	
	/**
	 * Reads line from input stream
	 */
	public String readLine() throws IOException {
		return input.readLine();
		
	}

	/**
	 * Writes text parameter to output stream
	 * @param text
	 */
	public void write(String text) throws IOException {
		output.write(text);
		output.flush();

	}

	/**
	 * Writes to output stream and adds newline at the end of the
	 * text
	 * @param text
	 */
	public void writeln(String text) throws IOException {
		this.write(text + "\n");
	}

}
