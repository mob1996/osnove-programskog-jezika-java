package hr.fer.zemris.java.hw16.search.engine;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hr.fer.zemris.java.hw16.math.Vector;

/**
 * Class that stores and builds {@link EngineVocabulary},
 * {@link EngineTFIDFVectors}, {@link EngineTFVectors}.
 * Class that can calculate similarities between documents.
 * @author Luka Mijić
 *
 */
public class SearchEngine {

	/**
	 * Visits all files in directory and sub directories.
	 * @author Luka Mijić
	 *
	 */
	private class EngineBuilder implements FileVisitor<Path> {
		
		@Override
		public FileVisitResult postVisitDirectory(Path dir, IOException arg1) throws IOException {
			return FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes arg1) throws IOException {
			return FileVisitResult.CONTINUE;
		}

		/**
		 * Read current file, find all words in in
		 * and fill idfVector and {@link EngineTFVectors}.
		 * @param file is file that is visited
		 * @param att {@link BasicFileAttributes}
		 * 
		 */
		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes att) throws IOException {
			Set<String> idfVisited = new HashSet<>();
			
			SearchEngine.this.tfVectors.addVector(file);
			Vector idfVector = SearchEngine.this.idfVector;
			
			String document = new String(Files.readAllBytes(file), charset);
			Matcher matcher = Pattern.compile("\\p{IsAlphabetic}+").matcher(document);
			
			while(matcher.find()) {
				String word = matcher.group().toLowerCase().trim();
				
				int index = SearchEngine.this.ev.getWordIndex(word);
				if(index == - 1) continue;
				
				SearchEngine.this.tfVectors.foundWord(file, word);
				if(!idfVisited.contains(word)) {
					idfVisited.add(word);
					idfVector.setComponent(idfVector.getComponent(index) + 1, index);
				}
			}
			
			
			return FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult visitFileFailed(Path file, IOException arg1) throws IOException {
			return FileVisitResult.CONTINUE;
		}
	}
	
	/**
	 * Stores stop words
	 */
	private EngineStopWords esw;
	
	/**
	 * Vocabulary of valid words
	 */
	private EngineVocabulary ev;
	
	/**
	 * Stores TF vectors of all documents
	 */
	private EngineTFVectors tfVectors;
	
	/**
	 * Stores idfVector
	 */
	private Vector idfVector;
	
	/**
	 * Character encoding used for reading files
	 */
	private Charset charset;
	
	/**
	 * Directory where documents are stored.
	 */
	private Path documentsDirectory;
	
	/**
	 * Stores TF-IDF vectors for each document
	 */
	private EngineTFIDFVectors tfIdfVectors;
	
	/**
	 * Creates {@link SearchEngine}.
	 * Default character encoding is UTF-8.
	 * @param stopWordsFile is {@link Path} to file that stores stop words
	 * @param documentsDirectory is {@link Path} to directory that stores documents
	 */
	public SearchEngine(Path stopWordsFile, Path documentsDirectory) {
		this(stopWordsFile, documentsDirectory, StandardCharsets.UTF_8);
	}
	
	/**
	 * Creates {@link SearchEngine}.
	 * @param stopWordsFile is {@link Path} to file that stores stop words
	 * @param documentsDirectory is {@link Path} to directory that stores documents
	 * @param charset is character encoding used for reading files.
	 */
	public SearchEngine(Path stopWordsFile, Path documentsDirectory, Charset charset) {
		esw = new EngineStopWords(stopWordsFile);
		ev = new EngineVocabulary(documentsDirectory, esw);
		this.documentsDirectory = documentsDirectory;
		this.charset = charset;
	}
	
	/**
	 * Builds whole {@link SearchEngine}.
	 * First builds {@link EngineStopWords} and {@link EngineVocabulary}.
	 * Calculates values in {@link EngineTFVectors} and fills idfVector.
	 * Builds {@link EngineTFIDFVectors}.
	 * @throws IOException
	 */
	public void build() throws IOException {
		esw.buildStopWordsVocabulary(this.charset);
		ev.buildVocabulary(this.charset);
		this.idfVector = new Vector(ev.getVocabularySize());
		this.tfVectors = new EngineTFVectors(ev);
		
		EngineBuilder eb = new EngineBuilder();
		Files.walkFileTree(this.documentsDirectory, eb);
		
		processIdf(tfVectors.numberOfVisitedDocuments());
		
		tfIdfVectors = new EngineTFIDFVectors(tfVectors, idfVector);
		tfIdfVectors.build();
	}
	
	/**
	 * Before this method is called idfVector components store
	 * how many different documents word appeared. This method
	 * calculates real idfVector.
	 * Each component value v is replaced by ln(filesVisited/v).
	 * @param filesVisited number of visited documents
	 */
	private void processIdf(int filesVisited) {
		for(int i = 0, size = idfVector.getDimension(); i < size; i++) {
			double comp = Math.log(filesVisited/idfVector.getComponent(i));
			idfVector.setComponent(comp, i);
		}
	}
	
	/**
	 * @return {@link EngineStopWords}
	 */
	public EngineStopWords getEngineStopWords() {
		return esw;
	}

	/**
	 * @return {@link EngineVocabulary}
	 */
	public EngineVocabulary getEngineVocabulary() {
		return ev;
	}

	/**
	 * Takes list of terms that are searched. 
	 * Creates TF-IDF vectors of those words
	 * and calculates similarities between this {@link Vector}
	 * and other TF-IDF {@link Vector}s.
	 * @param words that are search terms
	 * @return {@link Map} of {@link Path} to
	 * 			documents and similarity score. 
	 */
	public Map<Path, Double> search(List<String> words){
		Map<Path, Double> searchResults = new HashMap<>();
		
		Vector searchVector = new Vector(ev.getVocabularySize());
		for(String word:words) {
			word = word.toLowerCase().trim();
			int index = ev.getWordIndex(word);
			if(index != - 1) {
				searchVector.setComponent(searchVector.getComponent(index) + 1,  index);
			}
		}
		if(Math.abs(searchVector.norm()) < 1E-10) {
			return searchResults;
		}
		
		for(int i = 0; i < ev.getVocabularySize(); i++) {
			searchVector.setComponent(searchVector.getComponent(i) * idfVector.getComponent(i), i);
		}
		
		for(Path p:tfVectors.getDocumentPaths()) {
			Vector tfIdf = tfIdfVectors.getTfIdfVector(p);
			searchResults.put(p.toAbsolutePath(), searchVector.cos(tfIdf));
		}
		return searchResults;
	}
}
