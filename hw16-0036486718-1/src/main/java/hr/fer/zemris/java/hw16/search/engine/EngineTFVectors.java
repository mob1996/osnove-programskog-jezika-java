package hr.fer.zemris.java.hw16.search.engine;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import hr.fer.zemris.java.hw16.math.Vector;

/**
 * Class stores TF vectors associated to
 * document's {@link Path}s.
 * @author Luka MIjić
 *
 */
public class EngineTFVectors {

	/**
	 * Map that stores TF {@link Vector}s
	 * associated to {@link String} representation of paths.
	 */
	private Map<String, Vector> tfVectors;
	
	/**
	 * Vocabulary of words in {@link Vector}
	 */
	private EngineVocabulary vocabulary;
	
	/**
	 * Creates {@link EngineTFIDFVectors}.
	 * @param vocabulary of valid words
	 */
	public EngineTFVectors(EngineVocabulary vocabulary) {
		this.vocabulary = Objects.requireNonNull(vocabulary, "Vocabulary must not be null.");
		this.tfVectors = new HashMap<>();
	}
	
	/**
	 * Adds {@link Vector} whose dimension is number of words in vocabulary.
	 * Initial values of {@link Vector} components is zero.
	 * If {@link Vector} associated to given {@link Path} already exists
	 * overwrite it. 
	 * @param p document's {@link Path}
	 */
	public void addVector(Path p) {
		tfVectors.put(pathToKey(p)
					  , new Vector(vocabulary.getVocabularySize()));
	}
	
	/**
	 * To {@link Vector} associated with given {@link Path} increment
	 * component at index received by calling {@link EngineVocabulary#getWordIndex(String)}.
	 * @param p document's {@link Path}
	 * @param word that is found
	 */
	public void foundWord(Path p, String word) {
		int index = vocabulary.getWordIndex(word);
		if(index == -1) return;
		
		Vector tf = tfVectors.get(pathToKey(p));
		if(tf == null) {
			throw new IllegalArgumentException("There is no document associated with given path.");
		}
		
		tf.setComponent(tf.getComponent(index) + 1, index);
	}
	
	/**
	 * @param p document's {@link Path}
	 * @return {@link Vector} associated with given {@link Path}.
	 * 			if such {@link Vector} doesn't exist return false.
	 */
	public Vector getVector(Path p) {
		return tfVectors.get(pathToKey(p));
	}
	
	/**
	 * @return number of visited documents
	 */
	public int numberOfVisitedDocuments() {
		return tfVectors.size();
	}
	
	/**
	 * @return TF {@link Vector}s size. Size of TF vectors is
	 * 			equal to {@link EngineVocabulary#getVocabularySize()}.
	 */
	public int getVectorSize() {
		return vocabulary.getVocabularySize();
	}
	
	/**
	 * Calculates key in {@link Map} based on 
	 * given {@link Path}.
	 * @param p given {@link Path}.
	 * @return {@link String} key from {@link Path}
	 */
	private String pathToKey(Path p) {
		return p.toAbsolutePath().toString().toLowerCase();
	}
	
	/**
	 * @return all document's {@link Path}s whose {@link Vector}s 
	 * 			are created.
	 */
	public Set<Path> getDocumentPaths(){
		Set<Path> paths = new HashSet<>();
		tfVectors.keySet().forEach(k -> paths.add(Paths.get(k)));
		
		return paths;
	}
	
}
