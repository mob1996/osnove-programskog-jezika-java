package hr.fer.zemris.java.hw16.search.engine;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import hr.fer.zemris.java.hw16.math.Vector;

/**
 * Method stores TF-IDF vector of
 * each document.
 * @author Luka Mijić
 *
 */
public class EngineTFIDFVectors {

	/**
	 * TF vectors of documents
	 */
	private EngineTFVectors tfVectors;
	
	/**
	 * IDF vector
	 */
	private Vector idfVector;

	/**
	 * TF-IDF vectors mapped to document paths
	 */
	private Map<String, Vector> tfIdfVectors;
	
	/**
	 * Creates new {@link EngineTFIDFVectors}.
	 * Vectors of documents are not calculated yet.
	 * For calculating call {@link EngineTFIDFVectors#build()}. 
	 * @param tfVectors TF vectors
	 * @param idfVector IDF vector
	 */
	public EngineTFIDFVectors(EngineTFVectors tfVectors, Vector idfVector) {
		this.tfVectors = Objects.requireNonNull(tfVectors, "TF vectors must not be null");
		this.idfVector = Objects.requireNonNull(idfVector, "IDF vector must not be null");
		
		if(idfVector.getDimension() != tfVectors.getVectorSize()) {
			throw new IllegalArgumentException("idf and tf vectors must be of same size.");
		}
	}
	
	/**
	 * Calculates TF-IDF vectors for
	 * all {@link Path}s in {@link EngineTFVectors#getDocumentPaths()}
	 */
	public void build() {
		tfIdfVectors = new HashMap<>();
		
		Set<Path> paths = tfVectors.getDocumentPaths();
		
		int size = idfVector.getDimension();
		for(Path p:paths) {
			Vector tfIdfVector = new Vector(size);
			Vector tfVector= tfVectors.getVector(p);
			for(int i = 0; i < size; i++) {	
				tfIdfVector.setComponent(tfVector.getComponent(i) * idfVector.getComponent(i), i);
			}
			tfIdfVectors.put(pathToKey(p), tfIdfVector);
		}
	}
	
	/**
	 * Return TFIDF {@link Vector} associated to
	 * given {@link Path}.
	 * @param p document {@link Path}
	 * @return return TF-IDF {@link Vector} associated to given
	 * 					{@link Path} otherwise return null.
	 */
	public Vector getTfIdfVector(Path p) {
		return tfIdfVectors.get(pathToKey(p));
	}
	
	/**
	 * Calculates key in {@link Map} based on 
	 * given {@link Path}.
	 * @param p given {@link Path}.
	 * @return {@link String} key from {@link Path}
	 */
	private String pathToKey(Path p) {
		return p.toAbsolutePath().toString().toLowerCase();
	}
	
}
