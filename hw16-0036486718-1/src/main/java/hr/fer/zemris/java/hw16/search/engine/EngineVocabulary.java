package hr.fer.zemris.java.hw16.search.engine;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class is used for building and creating vocabulary.
 * @author Luka Mijić
 *
 */
public class EngineVocabulary {

	/**
	 * Class that visits all files that are children
	 * of starting directory.
	 * When it visits those files it reads them
	 * and stores their words into vocabulary.
	 * @author Luka Mijić
	 *
	 */
	private class VocabularyBuilder implements FileVisitor<Path>{
		
		/**
		 * Character encoding used for reading files
		 */
		private Charset charset;
		
		/**
		 * Index that is associated with read word
		 */
		private int index = 0;
		
		private VocabularyBuilder(Charset charset) {
			this.charset = charset;
		}

		@Override
		public FileVisitResult postVisitDirectory(Path dir, IOException arg1) throws IOException {
			return FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes arg1) throws IOException {
			return FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes arg1) throws IOException {
			String document = new String(Files.readAllBytes(file), charset);
			Matcher matcher = Pattern.compile("\\p{IsAlphabetic}+").matcher(document);
			
			while(matcher.find()) {
				String word = matcher.group().toLowerCase().trim();
				
				if(!word.isEmpty() && !EngineVocabulary.this.esw.isStopWord(word)) {
					EngineVocabulary.this.vocabulary.computeIfAbsent(word, w -> index++);
				}
			}
			
			return FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult visitFileFailed(Path file, IOException arg1) throws IOException {
			return FileVisitResult.CONTINUE;
		}
		
	}

	/**
	 * Path to directory that contains text documents
	 */
	private Path documentsDirectory;
	
	/**
	 * Vocabulary
	 */
	private Map<String, Integer> vocabulary;
	
	/**
	 * Contains stop words
	 */
	private EngineStopWords esw;

	/**
	 * Character encoding
	 */
	private Charset charset;
	
	/**
	 * Creates {@link EngineVocabulary} but doesn't fill 
	 * it's structures with words just yet.
	 * Given {@link Path} must lead to directory.
	 * Default character encoding is UTF-8.
	 * @param docDirectory {@link Path} to directory with documents
	 * @param esw {@link EngineStopWords}
	 * @throws NullPointerException if any of given parameters are null
	 * @throws IllegalArgumentException if {@link Path} does not lead to directory.
	 */
	public EngineVocabulary(Path docDirectory, EngineStopWords esw) {
		this.documentsDirectory = Objects.requireNonNull(docDirectory, "Given directory path not be null reference.");
		if(!Files.isDirectory(this.documentsDirectory)) {
			throw new IllegalArgumentException("Given path must lead to directory.");
		}
		this.charset = StandardCharsets.UTF_8;
		this.esw = Objects.requireNonNull(esw, "ESW parameter must not be null.");
	}
	
	/**
	 * @param charset is new encoding used for reading characters
	 */
	public void setCharset(Charset charset) {
		this.charset = charset;
	}
	
	/**
	 * @return {@link EngineStopWords}
	 */
	public EngineStopWords getEngineStopWords() {
		return esw;
	}

	/**
	 * @param esw is new {@link EngineStopWords}
	 */
	public void setEngineStopWords(EngineStopWords esw) {
		this.esw = esw;
	}

	/**
	 * Fills vocabulary with words from files
	 * stored into given directory.
	 * Uses stored {@link Charset} for reading, default
	 * one is UTF-8.
	 * @throws IOException if error occurs while reading
	 */
	public void buildVocabulary() throws IOException {
		buildVocabulary(charset);
	}
	
	/**
	 * Fills vocabulary with words from files
	 * stored into given directory.
	 * Uses given {@link Charset} for reading files.
	 * @param charset used for reading filesh
	 * @throws IOException if error occurs while readin
	 */
	public void buildVocabulary(Charset charset) throws IOException {
		vocabulary = new HashMap<>();
		Files.walkFileTree(documentsDirectory, new VocabularyBuilder(charset));
	}
	
	/**
	 * @return all words in vocabulary
	 * @throws IllegalStateException if vocabulary wasn't built 
	 */
	public Set<String> getVocabularyWords(){
		if(vocabulary == null) {
			throw new IllegalStateException("Build vocabulary before calling this method.");
		}
		
		return vocabulary.keySet();
	}
	
	/**
	 * @param word for which index is looked
	 * @return index of given word, if given word is not in vocabulary return -1
	 * @throws IllegalStateException if vocabulary wasn't built
	 */
	public int getWordIndex(String word) {
		if(vocabulary == null) {
			throw new IllegalStateException("Build vocabulary before calling this method.");
		}
		return vocabulary.getOrDefault(word.toLowerCase().trim(), -1);
	}
	
	/**
	 * @return size of vocabulary
	 */
	public int getVocabularySize() {
		if(vocabulary == null) {
			throw new IllegalStateException("Build vocabulary before calling this method.");
		}
		return vocabulary.size();
	}
}
