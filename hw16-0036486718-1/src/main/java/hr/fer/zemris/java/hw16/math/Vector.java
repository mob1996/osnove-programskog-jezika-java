package hr.fer.zemris.java.hw16.math;

import java.util.Arrays;
import java.util.Objects;
import java.util.function.DoubleBinaryOperator;

public class Vector {

	/**
	 * Adds two numbers
	 */
	private static final DoubleBinaryOperator ADD = (d1, d2) -> d1 + d2;
	
	/**
	 * Subs two numbers
	 */
	private static final DoubleBinaryOperator SUB = (d1, d2) -> d1 - d2;
	
	/**
	 * Default epsilon value
	 */
	private static final double DEFAULT_EPSILON = 1E-10;
	
	/**
	 * Stores component values
	 */
	private double[] components;
	
	/**
	 * Epsilon
	 */
	private double epsilon;

	/**
	 * Creates instance of {@link Vector} with
	 * n components. Default epsilon (1E-10) is used.
	 * @param components is array of components value
	 * @throws NullPointerException if components parameter is null reference
	 */
	public Vector(double[] components) {
		this.components = Objects.requireNonNull(components, "Given components must not be null reference.");
		this.epsilon = DEFAULT_EPSILON;
	}
	
	/**
	 * Creates instance of {@link Vector} with
	 * n components. Default epsilon (1E-10) is used.
	 * Default value of components is zero.
	 * @param n number of components
	 * @throws RuntimeException if n is less than 1.
	 */
	public Vector(int n) {
		if(n <= 0) {
			throw new RuntimeException("n must be higher than zero.");
		}
		components = new double[n];
		this.epsilon = DEFAULT_EPSILON;
 	}

	/**
	 * @return epsilon value
	 */
	public double getEpsilon() {
		return epsilon;
	}

	/**
	 * @param epsilon is new epsilon value
	 */
	public void setEpsilon(double epsilon) {
		this.epsilon = epsilon;
	}

	/**
	 * @return number of components 
	 */
	public int getDimension() {
		return components.length;
	}
	
	/**
	 * Retrieve component at given index parameter.
	 * Index counting starts from zero.
	 * @param compNum index of wanted components.
	 * @return value of component at given index
	 * @throws IndexOutOfBoundsException if index is not in [0, n-1] set.
	 */
	public double getComponent(int compNum) {
		return components[compNum];
	}
	
	/**
	 * Set component at given index to given value.
	 * Index counting start from zero.
	 * @param value new value of component
	 * @param comNum index of component
	 * @throws IndexOutOfBoundsException if index is not in [0, n-1] set.
	 */
	public void setComponent(double value, int comNum) {
		components[comNum] = value;
	}
	
	/**
	 * Calculates vector norm.
	 * @return norm
	 */
	public double norm() {
		return Math.sqrt(Arrays.stream(components).map(d -> d * d).sum());
	}
	
	/**
	 * Normalises current vector while modifying it.
	 * @return current {@link Vector}
	 * @throws RuntimeException if {@link Vector#norm()} is zero.
	 */
	public Vector normalize() {
		double norm = this.norm();
		if(norm <= epsilon) {
			throw new IllegalArgumentException("Norm was zero. Can't divide by zero.");
		}
		for(int i = 0; i < components.length; i++) {
			components[i] = components[i] / norm;
		}
		
		return this;
	}
	
	/**
	 * Creates and returns vector created by normalising current
	 * vector without modifying it. 
	 * @return normalised vector
	 * @throws RuntimeException if {@link Vector#norm()} is zero.
	 */
	public Vector normalized() {
		double norm = this.norm();
		if(norm <= epsilon) {
			throw new IllegalArgumentException("Norm was zero. Can't divide by zero.");
		}
		
		return new Vector(Arrays.stream(components).map(d -> d / norm).toArray());
	}
	
	/**
	 * Adds current {@link Vector} with other {@link Vector}.
	 * This method modifies current {@link Vector}.
	 * @param other {@link Vector} that is added
	 * @return current {@link Vector}
	 * @throws NullPointerException if given {@link Vector} is null reference
	 * @throws RuntimeException if {@link Vector}s dimensions are different.
	 */
	public Vector add(Vector other) {
		Objects.requireNonNull(other, "Other vector can't be null reference..");
		if(this.getDimension() != other.getDimension()) {
			throw new IllegalArgumentException("Vectors must have same dimensions.");
		}
		vectorOperations(components, other.getComponents(), components, ADD);
		
		return this;
	}
	
	/**
	 * Creates new {@link Vector} by adding current and given {@link Vector}.
	 * @param other {@link Vector} being added
	 * @return newly created {@link Vector}
	 * @throws NullPointerException if given {@link Vector} is null reference
	 * @throws RuntimeException if {@link Vector}s dimensions are different.
	 */
	public Vector added(Vector other) {
		Objects.requireNonNull(other, "Other vector can't be null reference..");
		if(this.getDimension() != other.getDimension()) {
			throw new IllegalArgumentException("Vectors must have same dimensions.");
		}
		//I understand that this is code redundancy, but it's better to check before potentially copying huge vectors
		return this.copy().add(other);
	}
	
	/**
	 * Subs current {@link Vector} with other {@link Vector}.
	 * This method modifies current {@link Vector}.
	 * @param other {@link Vector} is subtrahend
	 * @return current {@link Vector}
	 * @throws NullPointerException if given {@link Vector} is null reference
	 * @throws RuntimeException if {@link Vector}s dimensions are different.
	 */
	public Vector sub(Vector other) {
		Objects.requireNonNull(other, "Other vector can't be null reference..");
		if(this.getDimension() != other.getDimension()) {
			throw new IllegalArgumentException("Vectors must have same dimensions.");
		}
		vectorOperations(components, other.getComponents(), components, SUB);
		
		return this;
	}
	
	/**
	 * Creates new {@link Vector} by subtracting current and given {@link Vector}.
	 * @param other {@link Vector} is subtrahend
	 * @return newly created {@link Vector}
	 * @throws NullPointerException if given {@link Vector} is null reference
	 * @throws RuntimeException if {@link Vector}s dimensions are different.
	 */
	public Vector subbed(Vector other) {
		Objects.requireNonNull(other, "Other vector can't be null reference..");
		if(this.getDimension() != other.getDimension()) {
			throw new IllegalArgumentException("Vectors must have same dimensions.");
		}
		//I understand that this is code redundancy, but it's better to check before potentially copying huge vectors
		return this.copy().sub(other);
	}
	
	/**
	 * Calculates dot product. 
	 * @param other {@link Vector}
	 * @return dot product
	 * @throws NullPointerException if given {@link Vector} is null reference
	 * @throws RuntimeException if {@link Vector}s dimensions are different.
	 */
	public double dot(Vector other) {
		Objects.requireNonNull(other, "Other vector can't be null reference..");
		if(this.getDimension() != other.getDimension()) {
			throw new IllegalArgumentException("Vectors must have same dimensions.");
		}
		
		double dot = 0;
		for(int i = 0; i < components.length; i++) {
			dot += (components[i] * other.getComponent(i));
		}
		
		return dot;
	}
	
	/**
	 * Scales current vector by factor s.
	 * @param s scaling factor
	 * @return scaled current {@link Vector}.
	 */
	public Vector scale(double s) {
		for(int i = 0; i < components.length; i++) {
			components[i] = components[i] * s;
		}
		return this;
	}
	
	/**
	 * Creates new {@link Vector} and calls {@link Vector#scale(double)}
	 * on him with given parameter.
	 * @param s scaling factor
	 * @return newly created {@link Vector}.
	 */
	public Vector scaled(double s) {
		return this.copy().scale(s);
	}
	
	public double cos(Vector other) {
		Objects.requireNonNull(other, "Other vector can't be null reference..");
		if(this.getDimension() != other.getDimension()) {
			throw new IllegalArgumentException("Vectors must have same dimensions.");
		}
		
		double dot = this.dot(other);
		double div = this.norm() * other.norm();
		
		if(div <= epsilon) {
			throw new IllegalArgumentException("Multiplication of vectors norms is zero.");
		}
		
		return dot/div;
	}
	
	/**
	 * Takes binary operator and does given operation between 
	 * elements from first and second arrays and stores results into
	 * given results array. 
	 * Method requires that all arrays have equal length. That
	 * task is left to the user. 
	 * @param first array
	 * @param second array
	 * @param results stores results
	 * @param op operation between arrays elements
	 */
	private void vectorOperations(double[] first, double[] second, double[] results, DoubleBinaryOperator op) {
		for(int i = 0; i < first.length; i++) {
			results[i] = op.applyAsDouble(first[i], second[i]);
		}
	}
	
	/**
	 * Returns structure that stores {@link Vector} values.
	 * Modifying values modifies {@link Vector} components.
	 * @return inner structure that stores components. 
	 */
	public double[] getComponents() {
		return components;
	}
	
	/**
	 * Creates double array from {@link Vector}.
	 * Modifying elements in array doesn't impact current vector in
	 * any way.
	 * @return double array representation of {@link Vector}.
	 */
	public double[] toArray() {
		return Arrays.copyOf(components, components.length);
	}
	
	/**
	 * Creates deep copy of {@link Vector}.
	 * @return deep copy of current {@link Vector}.
	 */
	public Vector copy() {
		return new Vector(toArray());
	}
}
