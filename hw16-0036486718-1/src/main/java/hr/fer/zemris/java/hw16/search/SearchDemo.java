package hr.fer.zemris.java.hw16.search;

import java.nio.file.Paths;

import hr.fer.zemris.java.hw16.search.shell.SearchShell;

/**
 * Document search demonstration.
 * @author Luka Mijić
 *
 */
public class SearchDemo {

	/**
	 * Starts demonstration
	 * @param args needs at least one argument.
	 */
	public static void main(String[] args) {
		if(args.length != 1) {
			System.out.println("Expected one argument.");
			return;
		}
		SearchShell shell = new SearchShell();
		shell.start(Paths.get(args[0]));

	}

}
