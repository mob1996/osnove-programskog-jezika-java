package hr.fer.zemris.java.hw16.search.engine;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

/**
 * Class reads document that stores 
 * stop-words and add those words to inner
 * structure. 
 * @author Luka Mijić
 *
 */
public class EngineStopWords {
	
	/**
	 * Path to file that stores stop words
	 */
	private Path stopWordsPath;
	
	/**
	 * Stores all stop words
	 */
	private Set<String> stopWords;
	
	/**
	 * Character encoding
	 */
	private Charset charset;
	
	/**
	 * Creates new {@link EngineStopWords} but doesn't fill it's
	 * structure with words just yet. 
	 * Given file must be regular, readable file.
	 * It's expected that stop words are separated by white spaces.
	 * To fill vocabulary call {@link EngineStopWords#buildVocabulary()}.
	 * UTF-8 is used by default for character decoding.
	 * @param stopWordsPath path to file that contains stop words
	 * @throws NullPointerException if given {@link Path} is null reference
	 * @throws IllegalArgumentException if given path does not lead to regular or readable file.
	 */
	public EngineStopWords(Path stopWordsPath) {
		this.stopWordsPath = Objects.requireNonNull(stopWordsPath, "Given path must not be null reference.");
		if(!Files.isRegularFile(this.stopWordsPath)) {
			throw new IllegalArgumentException("Path must lead to regular file.");
		} else if(!Files.isReadable(this.stopWordsPath)) {
			throw new IllegalArgumentException("Path must lead to readable file.");
		}
		this.charset = StandardCharsets.UTF_8;
	}
	
	/**
	 * @param charset is new encoding used for reading characters
	 */
	public void setCharset(Charset charset) {
		this.charset = charset;
	}

	/**
	 * Calls {@link EngineStopWords#buildStopWordsVocabulary(Charset)}
	 * and gives stored character encoding as parameter.
	 * Default encoding is UTF-8 and it can be changed using
	 * {@link EngineStopWords#setCharset(Charset)}.
	 * @throws IOException if error occurs while reading file
	 */
	public void buildStopWordsVocabulary() throws IOException {
		buildStopWordsVocabulary(charset);
	}
	
	/**
	 * Reads stored {@link Path} using given character encoding.
	 * Stores all stop words that appear. 
	 * @param charset used for reading files
	 * @throws IOException if error occurs while reading file
	 */
	public void buildStopWordsVocabulary(Charset charset) throws IOException {
		stopWords = null;
		String document = new String(Files.readAllBytes(stopWordsPath), charset);
		stopWords = new TreeSet<>();
		for(String word:document.split("\\s+")) {
			stopWords.add(word);
		}
	}
	
	/**
	 * Check if given word is stopWord
	 * @param word that is checked
	 * @return true if given word is stop word,
	 * 			otherwise return false.
	 */
	public boolean isStopWord(String word) {
		if(stopWords == null) {
			throw new IllegalStateException("Build vocabulary before calling this method.");
		}
		Objects.requireNonNull(word, "Given word must not be null reference.");
		return stopWords.contains(word);
	}
}
