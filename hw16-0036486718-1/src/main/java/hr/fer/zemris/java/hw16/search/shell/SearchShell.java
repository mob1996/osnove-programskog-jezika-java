package hr.fer.zemris.java.hw16.search.shell;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import hr.fer.zemris.java.hw16.search.engine.EngineVocabulary;
import hr.fer.zemris.java.hw16.search.engine.SearchEngine;

/**
 * Shell that provides user interface
 * for working {@link SearchEngine}.
 * It implements serveral commands such as
 * 'exit', 'results', 'query' and 'type'
 * @author Luka Mijić
 *
 */
public class SearchShell {

	/**
	 * Path to stored stop words.
	 */
	private Path stopWords = Paths.get("src/main/resources/hrvatski_stoprijeci.txt");
	
	/**
	 * List of search results
	 */
	private List<Entry<Path, Double>> results;
	
	/**
	 * Starts {@link SearchShell}.
	 * Prompts user for input of commands.
	 * @param documentsDirectory {@link Path} to directory
	 * 			where document's are stored.
	 */
	public void start(Path documentsDirectory) {
		SearchEngine eng;
		try {
			eng = new SearchEngine(stopWords, documentsDirectory);
			eng.build();
		} catch (Exception e) {
			System.out.println("Error starting engine: " + e.getMessage());
			return;
		}
		SimpleEnvironment env = new SimpleEnvironment();
		
		try {
			env.writeln("Welcome.");
			env.writeln("Vocabulary size: " + eng.getEngineVocabulary().getVocabularySize());
			while(true) {
				env.write("> ");
				String[] inputs = env.readLine().trim().split("\\s+");
				
				if(inputs.length == 0) {
					env.writeln("Invalid input.");
					continue;
				}
				String command = inputs[0];
				
				if(command.equals("exit") && inputs.length == 1) {
					env.writeln("Goodbye.");
					return;
				} else if(command.equals("query")) {
					List<String> params = new ArrayList<>();
					
					EngineVocabulary ev = eng.getEngineVocabulary();
					env.write("Query is: [");
					for(int i = 1; i < inputs.length; i++) {
						String word = inputs[i].trim().toLowerCase();
						if(ev.getWordIndex(word) == -1) {
							if(i == inputs.length - 1) {
								env.writeln("]");
							}
							continue;
						}
						
						if(i == inputs.length - 1) {
							env.writeln(word + "]");
						} else {
							env.write(word + ", ");
						}
						params.add(word);
					}
					
					processSearch(eng.search(params));
					printResults(env);
				} else if(command.equals("results")) {
					printResults(env);
				} else if(command.equals("type")) {
					if(inputs.length != 2) {
						env.writeln("'type' commant takes 1 parameter.");
						continue;
					}
					
					if(results == null) {
						env.writeln("Search wasn't made.");
						continue;
					}
					
					int index = 0;
					try {
						index = Integer.valueOf(inputs[1]);
					} catch(NumberFormatException exc) {
						env.writeln("Parameter in 'type' must be number.");
						continue;
					}
					
					if(index < 0 || index  >= results.size()) {
						env.writeln("Index must be number in set [0, " + results.size() + "> for "
								+ "current results.");
						continue;
					}
					
					Path p = results.get(index).getKey();
					env.writeln(new String(Files.readAllBytes(p), StandardCharsets.UTF_8));
				} else {
					env.writeln("Invalid inputs.");
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Processes search results given by {@link SearchEngine}
	 * and stores them into {@link List} where they are sorted.
	 * @param searchResults {@link SearchEngine#search(List)} results
	 */
	private void processSearch(Map<Path, Double> searchResults) {
		if(results == null) {
			results = new ArrayList<>();
		}
		results.clear();
		
		searchResults.entrySet().stream().filter(e -> e.getValue().doubleValue() > 0.0000001)
					.sorted((e1, e2) -> e2.getValue().compareTo(e1.getValue()))
					.forEach(e ->{
						if(results.size() < 10) {
							results.add(e);
						}
					});
	}
	
	/**
	 * Prints first 10 results.
	 * @param env {@link SimpleEnvironment} for writing output
	 * @throws IOException if error occurs
	 */
	private void printResults(SimpleEnvironment env) throws IOException {
		if(results == null) {
			env.writeln("Search wasn't made.");
			return;
		} else if(results.isEmpty()) {
			env.writeln("No results.");
			return;
		}
		for(int i = 0; i < results.size(); i++) {
			if(i >= 10) {
				break;
			}
			Entry<Path, Double> entry = results.get(i);
			String output = String.format("[ %d] ( %.5f) %s"
					, i
					, entry.getValue().doubleValue()
					, entry.getKey().toAbsolutePath().toString()
			);
			
			env.writeln(output);
		}
	}
}
