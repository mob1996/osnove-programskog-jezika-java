package hr.fer.zemris.math;

import static org.junit.Assert.*;
import org.junit.Test;

public class Vector2DTest {

	private static double epsilon = 1E-6;
	
	@Test
	public void testConstructor() {
		Vector2D v1 = new Vector2D(4, 5);
		
		assertEquals(4, v1.getX(), epsilon);
		assertEquals(5, v1.getY(), epsilon);
		
		Vector2D v2 = new Vector2D(-5, -3);
		
		assertEquals(-5, v2.getX(), epsilon);
		assertEquals(-3, v2.getY(), epsilon);
		
		Vector2D v3 = new Vector2D(5, -3);
		
		assertEquals(5, v3.getX(), epsilon);
		assertEquals(-3, v3.getY(), epsilon);
	}
	
	@Test
	public void testTranslate() {
		Vector2D v1 = new Vector2D(3, -5);
		
		v1.translate(new Vector2D(-2, 3));
		
		assertEquals(1, v1.getX(), epsilon);
		assertEquals(-2, v1.getY(), epsilon);
		
		v1.translate(new Vector2D(4, 2));
		
		assertEquals(5, v1.getX(), epsilon);
		assertEquals(0, v1.getY(), epsilon);
	}
	
	@Test
	public void testTranslated() {
		Vector2D v1 = new Vector2D(3, -5);
		
		Vector2D v2 = v1.translated(new Vector2D(-2, 3));
		
		assertEquals(3, v1.getX(), epsilon);
		assertEquals(-5, v1.getY(), epsilon);
		
		assertEquals(1, v2.getX(), epsilon);
		assertEquals(-2, v2.getY(), epsilon);
		
		v2 = v1.translated(new Vector2D(4, 2));
		
		assertEquals(3, v1.getX(), epsilon);
		assertEquals(-5, v1.getY(), epsilon);
		
		assertEquals(7, v2.getX(), epsilon);
		assertEquals(-3, v2.getY(), epsilon);
	}
	
	@Test
	public void testRotate() {
		Vector2D v1 = new Vector2D(3, 5);
		
		v1.rotate(90);
		assertEquals(-5, v1.getX(), epsilon);
		assertEquals(3, v1.getY(), epsilon);
		
		
		v1.rotate(180);
		assertEquals(5, v1.getX(), epsilon);
		assertEquals(-3, v1.getY(), epsilon);
		
		v1.rotate(360);
		assertEquals(5, v1.getX(), epsilon);
		assertEquals(-3, v1.getY(), epsilon);
		
		v1.rotate(45);
		assertEquals(5.656854249, v1.getX(), epsilon);
		assertEquals(1.414213562, v1.getY(), epsilon);
		
		v1.rotate(-45);
		assertEquals(5, v1.getX(), epsilon);
		assertEquals(-3, v1.getY(), epsilon);
	}
	
	@Test
	public void testRotated() {
		Vector2D v1 = new Vector2D(3, 5);
		
		Vector2D v2 = v1.rotated(90);

		assertEquals(3, v1.getX(), epsilon);
		assertEquals(5, v1.getY(), epsilon);
		
		assertEquals(-5, v2.getX(), epsilon);
		assertEquals(3, v2.getY(), epsilon);
		
		v2 = v1.rotated(30);
		assertEquals(0.0980762, v2.getX(), epsilon);
		assertEquals(5.8301270, v2.getY(), epsilon);
		
		v2 = v1.rotated(-93.333333333333);
		
		assertEquals(4.817106305, v2.getX(), epsilon);
		assertEquals(-3.28564861, v2.getY(), epsilon);
	}
	
	@Test
	public void testScale() {
		
		Vector2D v1 = new Vector2D(2, 4);
		
		v1.scale(0.5);
		assertEquals(1, v1.getX(), epsilon);
		assertEquals(2, v1.getY(), epsilon);
		
		v1.scale(-4);
		assertEquals(-4, v1.getX(), epsilon);
		assertEquals(-8, v1.getY(), epsilon);
		
		v1.scale(-1.3);
		assertEquals(5.2, v1.getX(), epsilon);
		assertEquals(10.4, v1.getY(), epsilon);
	}
	
	@Test
	public void testScaled() {
		Vector2D v1 = new Vector2D(-2, 5);
		Vector2D v2 = v1.scaled(-0.5);
		
		assertEquals(-2, v1.getX(), epsilon);
		assertEquals(5, v1.getY(), epsilon);
		
		assertEquals(1, v2.getX(), epsilon);
		assertEquals(-2.5, v2.getY(), epsilon);
		
		v2 = v1.scaled(3);
		assertEquals(-6,  v2.getX(), epsilon);
		assertEquals(15, v2.getY(), epsilon);
		
		v2 = v1.scaled(-1);
		assertEquals(2,  v2.getX(), epsilon);
		assertEquals(-5, v2.getY(), epsilon);
	}
	
	@Test 
	public void testNormalize() {
		Vector2D v1 = new Vector2D(3, -5);
		v1.normalize();
		
		assertEquals(0.5144957554, v1.getX(), epsilon);
		assertEquals(-0.8574929257, v1.getY(), epsilon);
		
		v1 = new Vector2D(-2.35, 4.236);
		v1.normalize();
		
		assertEquals(-0.485117024, v1.getX(), epsilon);
		assertEquals(0.8744492399, v1.getY(), epsilon);
	}
	
	@Test
	public void testNormalized() {
		Vector2D v1 = new Vector2D(3, -5);
		Vector2D v2 = v1.normalized();
		
		assertEquals(3, v1.getX(), epsilon);
		assertEquals(-5, v1.getY(), epsilon);
		
		assertEquals(0.5144957554, v2.getX(), epsilon);
		assertEquals(-0.8574929257, v2.getY(), epsilon);
		
		v1 = new Vector2D(-2.35, 4.236);
		v2 = v1.normalized();
		
		assertEquals(-0.485117024, v2.getX(), epsilon);
		assertEquals(0.8744492399, v2.getY(), epsilon);
	}
	
	@Test
	public void testCopy() {
		Vector2D v1 = new Vector2D(3, 4);
		Vector2D v2 = v1.copy();
		
		assertEquals(v1.getX(),  v2.getX(), epsilon);
		assertEquals(v2.getY(), v2.getY(), epsilon);
		
		v1 = new Vector2D(-3.2353215321, 4.2356325325);
		v2 = v1.copy();
		
		assertEquals(v1.getX(),  v2.getX(), epsilon);
		assertEquals(v2.getY(), v2.getY(), epsilon);
		
		v1 = new Vector2D(0.00053252, 100048.14214);
		v2 = v1.copy();
		
		assertEquals(v1.getX(),  v2.getX(), epsilon);
		assertEquals(v2.getY(), v2.getY(), epsilon);
	}
	
}
