package hr.fer.zemris.java.custom.collections;

import org.junit.Test;
import static org.junit.Assert.*;

public class DictionaryTest {

	public Dictionary initDictionary() {
		Dictionary dict = new Dictionary();
		
		dict.put("0", "zero");
		dict.put(Integer.valueOf(1), "one");
		dict.put("two", Double.valueOf(2.0));
		dict.put("summer", "hot");
		dict.put("winter", "cold");
		dict.put(Character.valueOf('c'), "Letter c");
		
		return dict;
	}
	
	@Test
	public void testSize() {
		Dictionary dict = new Dictionary();
		
		assertEquals(0, dict.size());
		
		dict.put("1", "jedan");
		assertEquals(1, dict.size());
		
		dict.put("1", "one");
		assertEquals(1, dict.size());
		
		dict.put("2", "two");
		assertEquals(2, dict.size());
		
		dict.put("3", "three");
		dict.put("4", "four");
		dict.put("5", "five");
		
		assertEquals(5, dict.size());
		
	}
	
	@Test
	public void testClear() {
		Dictionary dict = initDictionary();
		
		dict.clear();
		
		assertEquals(0, dict.size());
	}
	
	@Test
	public void testIsEmpty() {
		Dictionary dict = new Dictionary();
		
		assertTrue(dict.isEmpty());
		
		dict.put("1", "jedan");
		
		assertFalse(dict.isEmpty());
		
		dict.clear();
		
		assertTrue(dict.isEmpty());
	}
	
	@Test
	public void testGet() {
		Dictionary dict = initDictionary();
		
		assertEquals("zero", dict.get("0"));
		assertEquals("one", dict.get(Integer.valueOf(1)));
		assertEquals(Double.valueOf(2.0), dict.get("two"));
		assertEquals("Letter c", dict.get(Character.valueOf('c')));
		assertNull(dict.get("ten"));
	}
	
	@Test
	public void testPut() {
		Dictionary dict = initDictionary();
		
		assertEquals(6, dict.size());
		assertEquals("hot", dict.get("summer"));
		
		dict.put("summer", "cold");
		assertEquals(6, dict.size());
		assertEquals("cold", dict.get("summer"));

	}
	
}
