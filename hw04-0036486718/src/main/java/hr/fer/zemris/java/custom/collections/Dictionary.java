package hr.fer.zemris.java.custom.collections;

/**
 * Class represents Dictionary structure that
 * makes connection between key and given value.
 * Value is stored with key and can be requested by
 * asking for given key. 
 * @author Mob
 *
 */
public class Dictionary {

	/**
	 * This class represents one entry in
	 * given dictionary. 
	 * @author Luka Mijić
	 *
	 */
	private class Entry {
		
		/**
		 * Key is used to access value.
		 */
		private Object key;
		/**
		 * Value of Entry
		 */
		private Object value;
		
		/**
		 * Constructor for Entry class that takes
		 * both key and value.
		 * @param key of the Entry
		 * @param value of the Entry
		 */
		private Entry(Object key, Object value) {
			if(key == null) throw new NullPointerException("Key cannot be null.");
			
			this.key = key;
			this.value = value;
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((key == null) ? 0 : key.hashCode());
			return result;
		}

		/**
		 * Two entrys are equal if their keys are equal.
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Entry other = (Entry) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (key == null) {
				if (other.key != null)
					return false;
			} else if (!key.equals(other.key))
				return false;
			return true;
		}

		private Dictionary getOuterType() {
			return Dictionary.this;
		}
		
	}
	
	/**
	 * Structure used for storing Entrys.
	 */
	private ArrayIndexedCollection dictionaryArray;
	
	/**
	 * Creates empty dictionary.
	 */
	public Dictionary() {
		dictionaryArray = new ArrayIndexedCollection();
	}
	
	/**
	 * Method checks if Dictionary is Empty
	 * @return true if dictionary is empty
	 *           and false if it is not.
	 */
	public boolean isEmpty() {
		return dictionaryArray.size() == 0;
	}
	
	/**
	 * Method returns current number of elements in
	 * dictionary
	 * @return size
	 */
	public int size() {
		return dictionaryArray.size();
	}
	
	/**
	 * Clears all elements from dictionary.
	 */
	public void clear() {
		dictionaryArray.clear();
	}
	
	/**
	 * Adds new Entry into dictionary.
	 * If dictionary already holds Entry with given key
	 * it replaces value with new value.
	 * @param key of the Entry
	 * @param value of the Entry
	 */
	public void put(Object key, Object value) {
		Entry entry = new Entry(key, value);
		int index = dictionaryArray.indexOf(entry);
		
		if(index == -1) {
			dictionaryArray.add(entry);
		} else {
			entry = (Entry) dictionaryArray.get(index);
			entry.value = value;
		}
	}
	
	/**
	 * Gets value from dictionary if given key
	 * exists.
	 * @param key of value method looks for
	 * @return null if Entry with given key doesn't exits,
	 * 			otherwise it returns value associated with given key
	 */
	public Object get(Object key) {
		int index = dictionaryArray.indexOf(new Entry(key, null));
		
		if(index == -1) {
			return null;
		} else {
			Entry getEntry = (Entry) dictionaryArray.get(index);
			return getEntry.value;
		}
	}
	
	
}
