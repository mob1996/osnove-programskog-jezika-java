package hr.fer.zemris.java.custom.collections;

import java.util.Arrays;

/**
 * 
 * Class that is used for storing instances of
 * Object class and classes that extend Object class in an array.
 * @author Luka Mijić
 *
 */
public class ArrayIndexedCollection extends Collection {

	private static final int DEFAULT_CAPACITY = 16;
	private static final int MIN_CAPACITY = 1;
	private static final int DOUBLE_CAPACITY = 2;
	
	private int size;
	private int capacity;
	private Object[] elements;
	
	
	/**
	 * Default constructor. 
	 * Sets capacity to its default value which is 16.
	 */
	public ArrayIndexedCollection() {
		this(DEFAULT_CAPACITY);
	}
	
	/**
	 * Constructor that takes existing collection as an argument.
	 * Size of new instance of Collection is the same as collection argument.
	 * Fills new Collection with values from argument.
	 * @param collection collection that we use to fill new collection.
	 */
	public ArrayIndexedCollection(Collection collection) {
		this(collection, DEFAULT_CAPACITY);
	}
	
	/**
	 * Constructor for creating empty collection with with 
	 * with preferable capacity. 
	 * @param initialCapacity capacity of collection will be set to this value
	 * @throws IllegalArumentException if capacity is less than MIN_CAPACITY=1
	 */
	public ArrayIndexedCollection(int initialCapacity) {
		if(initialCapacity < MIN_CAPACITY) throw new IllegalArgumentException("Capacity must be higher than 1. Was " + initialCapacity);
		
		this.capacity = initialCapacity;
		elements = new Object[this.capacity];
	}
	
	/**
	 * Constructor for creating new Collection that is filled with values
	 * from collection argument. Size of new Collection is either initialCapacity 
	 * or collection.size(), depending on which of them is higher.
	 * @param collection that we use to fill new Collection
	 * @param initialCapacity capacity that is preferable for new Collection
	 * @throws IllegalArumentException if capacity is less than MIN_CAPACITY=1
	 * @throws NullPointerException if collection references null
	 */
	public ArrayIndexedCollection(Collection collection, int initialCapacity) {
		if(collection == null) throw new NullPointerException("Collection must not reference null.");
		
		int collSize = collection.size();
		this.capacity = collSize > initialCapacity ? collSize : initialCapacity;
		
		if(this.capacity < MIN_CAPACITY) throw new IllegalArgumentException("Capacity must be higher than 1. Was " + this.capacity);
		
		this.elements = new Object[this.capacity];

		addAll(collection);
	}
	
	/**
	 * Public method is used to get size of collection
	 * @return size of collection
	 */
	@Override
	public int size() {
		return size;
	}

	/**
	 * Public method that checks if collections contains
	 * value.
	 * @param value for which we check if collection
	 * 			contains it
	 * @return true if collection contains value,
	 * 			otherwise return false
	 */
	@Override
	public boolean contains(Object value) {
		if(value == null ) return false;
		for(int i = 0; i < size; i++) {
			if(value.equals(elements[i])) return true;
		}
		return false;
	}
	
	
	/**
	 * Public method is used to add new value to
	 * collection. If capacity of collection is full
	 * double the capacity of collection. 
	 * @param value to be added into collection
	 * @throws NullPointerException if value that is supposed
	 * 			to be added is null reference
	 */
	@Override
	public void add(Object value) {
		if(value == null) throw new NullPointerException("Argument for method add cannot be null reference.");
	
		if(size == capacity) {
			this.capacity = DOUBLE_CAPACITY * this.capacity;
			elements = Arrays.copyOf(elements, this.capacity);
		}
		
		elements[size++] = value;
	}
	
	/**
	 * Public method is used to get element from collection that is at
	 * index location
	 * @param index of element 
	 * @return element at index
	 * @throws IndexOutOfBoundsException if index is not element of [0, size-1]
	 */
	public Object get(int index) {
		if(index < 0 || index > (size-1)) {
			throw new IndexOutOfBoundsException(
					String.format("Index must be element of [0, %d]. Was %d.", (size-1), index).toString()
					);
		}
		
		return elements[index];
	}
	
	/**
	 * Public method is used to insert value at given index.
	 * Algorithm used: 1. add value at the end of the collection
	 * 						using function add(Object value). 
	 * 						If collection is full, function add will
	 * 						increase its capacity.
	 * 				   2. Swap given value at index i with value at i-1
	 * 						until given value swaps with value at i-1 = index.
	 * @param value that we want to insert
	 * @param index at which method places given value without
	 * 			overwriting current value at index. If index == size, method
	 * 			acts like add()
	 * @throws IndexOutOfBoundsException if index is not element of [0, size-1]
	 * @throws NullPointerException method add(Object value) can throw this exception
	 * 								if value == null
	 */
	public void insert(Object value, int index) {
		if(index < 0 || index > size) {
			throw new IndexOutOfBoundsException(
					String.format("Index must be element of [0, %d]. Was %d.", (size-1), index).toString()
					);
		}
		
		add(value);
		if(index == size-1) return; // Size-1 is used, because add() increased size value
		
		for(int i = size - 1; i > index; i--) {
			Object pom = elements[i];
			elements[i] = elements[i-1];
			elements[i-1] = pom;
		}
	}
	
	
	/**
	 * Public method is used to remove value from collection.
	 * @param value is value that we want to remove
	 * @return true if element is removed from collection, 
	 * 			false if element is not in collection
	 */
	@Override
	public boolean remove(Object value) {
		for(int i=0; i < this.capacity && elements[i] != null; i++) {
			if(elements[i].equals(value)) {
				
				if(i != (this.capacity-1)) {
					for(int j = i + 1; j < this.capacity; j++) {  //This for loop is used to shift to left by 1 
						elements[j-1] = elements[j];              //all elements starting from i+1        
						
						if(elements[j] == null) break;
					};
				} 
				
				elements[this.capacity-1] = null;
				size--;
				
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Public method that is used to remove element from given index
	 * @param index of an element that is being removed
	 * @throws IndexOutOfBoundsException if index is not element of
	 * 			[0, size-1]
	 */
	public void remove(int index) {
		if(index < 0 || index > (size-1)) {
			throw new IndexOutOfBoundsException(
					String.format("Index must be element of [0, %d]. Was %d.", (size-1), index).toString()
					);
		}
		
		for(int i = index + 1; i < this.capacity; i++) {
			elements[i-1] = elements[i];
			
			if(elements[i] == null) break;
		}
		
		elements[this.capacity-1] = null;
		size--;
		
	}
	
	/**
	 * Public method is used to set all elements of 
	 * array to null so garbage collector can clear them
	 * from memory.
	 */
	@Override
	public void clear() {
		for(int i = 0; i < size; i++) {
			elements[i] = null;
		}
		
		size = 0;
	}
	
	/**
	 * Public method is used to return index of element value
	 * @param value of the element method is looking index for
	 * @return index of value, or -1 if value doesn't exists or
	 * 			value == null
	 */
	public int indexOf(Object value) {
		if(value == null) return -1;
		
		for(int i = 0; i < size; i++) {
			if(elements[i].equals(value)) {
				return i;
			}
		}
		
		return -1;
	}
	
	/**
	 * Public method is used to create and return new array with size equal
	 * to the size of collection and that is filled with elements from collection.
	 * @return newArray is new array filled with elements from collection
	 */
	@Override
	public Object[] toArray(){
		Object[] newArray = Arrays.copyOf(elements, size);
		return newArray;
	}
	
	/**
	 * Public method that calls method process from class Processor
	 * for each element of collection.
	 * @param processor 
	 */
	@Override
	public void forEach(Processor processor) {
		for(int i = 0; i < size; i++) {
			processor.process(elements[i]);
		}
	}
	
	

}
