package hr.fer.zemris.java.custom.collections;

/**
 * Class Processor is template
 * for creating local classes that 
 * implement method process.
 * @author Luka Mijić
 *
 */
public class Processor {

	/**
	 * Method used for processing given value.
	 * @param value value that is processed
	 */
	public void process(Object value) {
		
	}
}
