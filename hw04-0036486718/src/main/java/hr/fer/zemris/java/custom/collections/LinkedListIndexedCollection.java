package hr.fer.zemris.java.custom.collections;

/**
 * Class that is used for storing instances of
 * Object class and classes that exctend Object class in a linked list.
 * @author Luka Mijić
 *
 */
public class LinkedListIndexedCollection extends Collection{
	
	/**
	 * 
	 * @author Luka Mijić
	 * Static nested class List node represents structure
	 *  in a linked list.
	 */
	private static class ListNode{
		Object value;
		ListNode prev;
		ListNode next;
		
		/**
		 * Constructor for static nested class ListNode.
		 * Sets class variable value to argument value and
		 * sets prev and next references to null.
		 * @param value 
		 */
		private ListNode(Object value) {
			this.value = value;
			prev = next = null;
		}
		
	}
	
	private static final int EMPTY = 0;
	
	private int size;
	private ListNode head;
	private ListNode tail;
	
	/**
	 * Default constructor for this class.
	 * Creates empty list.
	 */
	public LinkedListIndexedCollection() {
		head = tail = null;
	}
	
	/**
	 * Constructor that creates linked list filled
	 * with values from collection
	 * @param collection that is used to fill the linked list
	 */
	public LinkedListIndexedCollection(Collection collection) {
		addAll(collection);
	}
	
	/**
	 * Public method is used to return size of the list.
	 * @return size of the list
	 */
	@Override
	public int size() {
		return size;
	}
	
	/**
	 * Public method is used to traverse list
	 * and check if it contains given value.
	 * @param value for which method is looking
	 * 			for
	 */
	@Override
	public boolean contains(Object value) {
		if(value == null || size == EMPTY) return false;
		
		ListNode startRef = head;
		ListNode endRef = tail;
		
		for(int startIndex = 0, endIndex = size - 1; 
				startIndex <= endIndex;
				startIndex++, endIndex--,
				startRef = startRef.next,
				endRef = endRef.prev) {
			
			if(startRef.value.equals(value) || endRef.value.equals(value)) return true;
		}

		return false;
	}
	
	/**
	 * Public method is used to add new values at the 
	 * end of a linked list. 
	 * @param value that we want to add to the end of the list.
	 */
	@Override
	public void add(Object value) {
		if(value == null) throw new NullPointerException("Argument for method add cannot be null reference.");
	
		ListNode node = new ListNode(value);
		
		if(head == null) {
			head = tail = node;
		} else {
			node.prev = tail;
			tail.next = node;
			tail = node;
		}
		
		size++;
	}
	
	/**
	 * Public method is used to insert given value at given index
	 * without overwriting value at that index.
	 * @param value that method inserts at given index
	 * @param index at which method places given value without
	 * 			overwriting current value at index. If index == size, method
	 * 			acts like add()
	 * @throws IndexOutOfBoundsException if index is not element of [0, size-1]
	 * @throws NullPointerException method can throw this exception
	 * 								if value == null
	 */
	public void insert(Object value, int index) {
		if(value == null) throw new NullPointerException("Argument for method insert cannot be null reference.");
		
		if(index < 0 || index > size) {
			throw new IndexOutOfBoundsException(
					String.format("Index must be element of [0, %d]. Was %d.", (size-1), index).toString()
					);
		}
		
		ListNode newNode = new ListNode(value);
		
		if(index == size) {
			add(value); 
			return;
		}
		
		ListNode startRef = head;
		ListNode endRef = tail;
		
		
		for(int startIndex = 0, endIndex = size - 1; 
				startIndex <= endIndex;
				startIndex++, endIndex--,
				startRef = startRef.next,
				endRef = endRef.prev) {
			
			if(startIndex == index) {
				newNode.prev = startRef.prev;
				newNode.next = startRef;
				if(startIndex == 0) {
					head = newNode;
				} else {
					startRef.prev.next = newNode;
				}
				startRef.prev = newNode;
				break;
			} else if(endIndex == index) {
				newNode.next = endRef;
				newNode.prev = endRef.prev;
				endRef.prev.next = newNode;
				endRef.prev = newNode;
				break;
			}
		}
		
		size++;
		return;
	}
	
	/**
	 * Public method is used to get value at 
	 * requested index.
	 * @param index of the value requested
	 * @return value at requested index
	 */
	public Object get(int index) {
		if(index < 0 || index > (size-1)) {
			throw new IndexOutOfBoundsException(
					String.format("Index must be element of [0, %d]. Was %d.", (size-1), index).toString()
					);
		}
		
		ListNode startRef = head;
		ListNode endRef = tail;
		Object returnValue = null;
		
		for(int startIndex = 0, endIndex = size - 1; 
				startIndex <= endIndex;
				startIndex++, endIndex--,
				startRef = startRef.next,
				endRef = endRef.prev) {
			
			if(startIndex == index) {
				returnValue = startRef.value;
				break;
			} else if(endIndex == index) {
				returnValue = endRef.value;
				break;
			}
		}
		
		return returnValue;
	}
	
	/**
	 * Public method that finds first appearance of given
	 * value.
	 * @param value that method searches for
	 * @return index of given value, or -1 if 
	 * 			value reference is null or if
	 * 		 	list doesn't cointain given value.
	 */
	public int indexOf(Object value) {
		if(value == null) return -1;
		
		ListNode startRef = head;
		ListNode endRef = tail;
		
		for(int startIndex = 0, endIndex = size - 1; 
				startIndex <= endIndex;
				startIndex++, endIndex--,
				startRef = startRef.next,
				endRef = endRef.prev) {
			
			if(startRef.value.equals(value)) return startIndex;
			else if(endRef.value.equals(value)) return endIndex;
		}
		
		return -1;
	}
	
	/**
	 * Public method is used to remove given value
	 * from the list. 
	 * @param value that method removes from list
	 * @return true if value is removed, return false
	 * 			if value was not in the list. 
	 */
	@Override
	public boolean remove(Object value) {
		if(value == null || size == EMPTY) return false;
		
		int startSize = size;
		if(head.value.equals(value) && head==tail) { //One Element
			clear();
		} else if(head.value.equals(value)) { //Element is stored in head
			removeHead();
		} else if(tail.value.equals(value)) { //Element is stored in tail
			removeTail();
		}
		
		if(startSize > size) return true; //Check if element is removed by previous if statements
		
		ListNode startRef = head;
		ListNode endRef = tail;
		
		for(int startIndex = 0, endIndex = size - 1; 
				startIndex <= endIndex;
				startIndex++, endIndex--,
				startRef = startRef.next,
				endRef = endRef.prev) {
			
			if(startRef.value.equals(value)) {
				startRef.prev.next = startRef.next;
				startRef.next.prev = startRef.prev;
				size--;
				return true;
			} else if(endRef.value.equals(value)) {
				endRef.next.prev = endRef.prev;
				endRef.prev.next = endRef.next;
				size--;
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Public method is used to remove value at given index
	 * from the list.
	 * @param index of value method removes
	 * @throws IndexOutOfBoundsException if index is not
	 * 			an element of [0, size-1]
	 */
	public void remove(int index) {
		if(index < 0 || index > (size-1)) {
			throw new IndexOutOfBoundsException(
					String.format("Index must be element of [0, %d]. Was %d.", (size-1), index).toString()
					);
		}
		int startSize = size;
		if(index == 0 && head == tail) { //Only one element
			clear();
		} else if(index == 0) {
			removeHead();
		} else if(index == (size-1)) {
			removeTail();
		}
		
		if(startSize > size) return;
		
		ListNode startRef = head;
		ListNode endRef = tail;
		
		for(int startIndex = 0, endIndex = size - 1; 
				startIndex <= endIndex;
				startIndex++, endIndex--,
				startRef = startRef.next,
				endRef = endRef.prev) {
			if(startIndex == index) {
				startRef.prev.next = startRef.next;
				startRef.next.prev = startRef.prev;
				break;
			} else if(endIndex == index) {
				endRef.next.prev = endRef.prev;
				endRef.prev.next = endRef.next;
				break;
			}
		}
		size--;
		
	}
	
	/**
	 * Private method is used to remove first element from the
	 * linked list. 
	 */
	private void removeHead() {
		head = head.next;
		head.prev = null;
		size--;
	}
	
	/**
	 * Private method is used to remove last element from the
	 * linked list. 
	 */
	private void removeTail() {
		tail = tail.prev;
		tail.next = null;
		size--;
	}
	
	/**
	 * Method is used to clear collections of all elements. 
	 */
	@Override
	public void clear() {
		head = null;
		tail = null;
		size = 0;
	}
	
	/**
	 * Public method is used to create and return new array with size equal
	 * to the size of collection and that is filled with elements from collection.
	 * @return newArray is new array filled with elements from collection
	 */
	@Override
	public Object[] toArray() {
		Object[] newArray = new Object[size];
		
		ListNode startRef = head;
		ListNode endRef = tail;
		
		for(int startIndex = 0, endIndex = size - 1; 
				startIndex <= endIndex;
				startIndex++, endIndex--,
				startRef = startRef.next,
				endRef = endRef.prev) {
		
			newArray[startIndex] = startRef.value;
			newArray[endIndex] = endRef.value;
		}
		
		return newArray;
	}
	
	/**
	 * Public method that calls method process from class Processor
	 * for each element of collection.
	 * @param processor 
	 */
	@Override
	public void forEach(Processor processor) {
		ListNode startRef = head;
		for(; startRef != null; startRef = startRef.next) {
			processor.process(startRef.value);
		}
	}
	
}
