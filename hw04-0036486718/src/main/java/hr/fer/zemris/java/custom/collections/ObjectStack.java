package hr.fer.zemris.java.custom.collections;

/**
 * 
 * Class that is used as an adapter that makes
 * ArrayIndexedCollection act like a stack.
 * @author Luka Mijić
 */
public class ObjectStack {

	private ArrayIndexedCollection array;
	
	/**
	 * Default constructor that
	 * creates empty stack
	 */
	public ObjectStack() {
		array = new ArrayIndexedCollection();
	}
	
	/**
	 * Method is used to check if stack is empty.
	 * @return true if stack is empty, false if it isn't.
	 */
	public boolean isEmpty() {
		return array.isEmpty();
	}
	
	/**
	 * Method is used to check the current
	 * size of the stack.
	 * @return the size of the stack
	 */
	public int size() {
		return array.size();
	}
	
	/**
	 * Method is used to add new value at
	 * the top of the stack.
	 * @param value that is being pushed on stack.
	 */
	public void push(Object value) {
		if(value == null) throw new NullPointerException("Argument for method push cannot be null reference.");
		
		array.add(value);
	}
	
	/**
	 * Method is used to get and remove value from top
	 * of the stack.
	 * @return value from top of the stack
	 * @throws EmptyStackException if stack is empty
	 */
	public Object pop() {
		if(isEmpty()) throw new EmptyStackException("Cannont use pop() on an empty stack.");
		
		int topIndex = size()-1;
		Object top = array.get(topIndex);
		
		array.remove(topIndex);
		
		return top;
	}
	
	/**
	 * Method is used to get value from top of the stack
	 * without removing it.
	 * @return value from top of the stack
	 * @throws EmptyStackException if stack is empty
	 */
	public Object peek() {
		if(isEmpty()) throw new EmptyStackException("Cannont use peek() on an empty stack.");
		
		return array.get(size()-1);
	}
	
	/**
	 * Method is used to clear all elements from the stack. 
	 */
	public void clear() {
		array.clear();
	}
	
}
