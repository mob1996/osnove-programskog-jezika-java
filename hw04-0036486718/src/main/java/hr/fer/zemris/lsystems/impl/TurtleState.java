package hr.fer.zemris.lsystems.impl;

import java.awt.Color;
import java.util.Objects;

import hr.fer.zemris.math.Vector2D;

/**
 * Represents turtle state in so called 
 * 'turtle graphics'.
 * @author Luka Mijić
 *
 */
public class TurtleState {

	/**
	 * Current position of turtle
	 */
	private Vector2D position;
	
	/**
	 * Direction of turtle.
	 * Unit vector. 
	 */
	private Vector2D direction;
	
	/**
	 * Colour that turtle uses to draw.
	 */
	private Color color;
	
	/**
	 * Length that turtle moves in 
	 * direction given by direction property
	 */
	private double moveLength;
	
	/**
	 * Default constructor that sets current 
	 * TurtleState to default values.
	 * Default position is (0,0)
	 * Default direction is (1,0)
	 * Default colour is black
	 * Default moveLength is 10
	 */
	public TurtleState() {
		this.position = new Vector2D(0.0, 0.0);
		this.direction = new Vector2D(1.0, 0);
		this.color = Color.BLACK;
		this.moveLength = 1;
	}

	/**
	 * Constructor is used to create new TurtleState
	 * with given values.
	 * @param position represents current position of turtle
	 * @param angle is used to define current direction.
	 *            Default direction (1, 0) is rotated by angle.
	 * @param color of lines drawn by turtle
	 * @param moveLength 
	 */
	public TurtleState(Vector2D position, Vector2D direction, Color color, double moveLength) {
		this.position = Objects.requireNonNull(position, "position argument can't be null.");
		this.direction = Objects.requireNonNull(direction, "direction argument can't be null.");
		this.direction.normalize();
		this.color = Objects.requireNonNull(color, "color argument can't be null.");
		this.moveLength = moveLength;
	}

	public Vector2D getPosition() {
		return position;
	}

	public void setPosition(Vector2D position) {
		this.position = Objects.requireNonNull(position, "position argument can't be null.");
	}

	public Vector2D getDirection() {
		return direction;
	}

	public void setDirection(Vector2D direction) {
		this.direction = Objects.requireNonNull(direction, "direction argument can't be null.");
		this.direction.normalize();
	}
	
	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = Objects.requireNonNull(color, "color argument can't be null.");
	}

	public double getMoveLength() {
		return moveLength;
	}

	public void setMoveLength(double moveLength) {
		this.moveLength = moveLength;
	}
	
	/**
	 * Method is used to create copy of current state.
	 * @return new TurtleState
	 */
	public TurtleState copy() {
		return new TurtleState(this.position.copy()
				, this.direction.copy()
				, this.color
				, this.moveLength);
	}
	
	
	
}
