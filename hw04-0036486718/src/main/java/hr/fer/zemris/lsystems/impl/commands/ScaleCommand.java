package hr.fer.zemris.lsystems.impl.commands;

import java.util.Objects;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

/**
 * Class represents commands that
 * that alters distance that turtle moves.
 * @author Luka Mijić
 *
 */
public class ScaleCommand implements Command {

	/**
	 * Factor by which distance is scaled
	 */
	private double factor;

	/**
	 * Constructor that takes one argument
	 * @param factor argument is used to set
	 * 			value of factor property
	 */
	public ScaleCommand(double factor) {
		this.factor = factor;
	}


	/**
	 * Method alters distance turtle
	 * moves for each step.
	 * @param ctx is Context
	 * @param painter provides canvas to draw on
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		Objects.requireNonNull(ctx, "Context argument can't be null.");
		Objects.requireNonNull(painter, "Painter argument can't be null.");
		
		ctx.getCurrentState().setMoveLength(
				factor * ctx.getCurrentState().getMoveLength());
		
	}
	
	
	
	
}
