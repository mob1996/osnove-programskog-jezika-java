package hr.fer.zemris.lsystems.impl;

import java.awt.Color;
import java.util.Objects;

import hr.fer.zemris.java.custom.collections.Dictionary;
import hr.fer.zemris.lsystems.LSystem;
import hr.fer.zemris.lsystems.LSystemBuilder;
import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.commands.ColorCommand;
import hr.fer.zemris.lsystems.impl.commands.DrawCommand;
import hr.fer.zemris.lsystems.impl.commands.PopCommand;
import hr.fer.zemris.lsystems.impl.commands.PushCommand;
import hr.fer.zemris.lsystems.impl.commands.RotateCommand;
import hr.fer.zemris.lsystems.impl.commands.ScaleCommand;
import hr.fer.zemris.lsystems.impl.commands.SkipCommand;
import hr.fer.zemris.math.Vector2D;

/**
 * This Builder class is used to build new
 * Lindermayer system.
 * @author Luka Mijić
 *
 */
public class LSystemBuilderImpl implements LSystemBuilder {

	/**
	 * Dictionary that stores registered commands
	 */
	private Dictionary commands;
	/**
	 * Dictionary that stores registered 
	 */
	private Dictionary productions;
	
	/**
	 * Length of unit move of turtle.
	 * For example 0.5 move to the right would pass half of screen.
	 */
	private double unitLength;
	
	/**
	 * Used to scale unitLenght so overall dimensions
	 * of fractals would stay somehow consistent
	 */
	private double unitLengthDegreeScaler;
	
	/**
	 * Starting point of turtle
	 * (0, 0) => left bottom corner
	 * (1, 1) => right top corner
	 * (0.5, 0.5) => center
	 */
	private Vector2D origin;
	
	/**
	 * Angle defines where turtle is "looking"
	 */
	private double angle;
	
	/**
	 * Starting sequence used for generating
	 * new sequence
	 */
	private String axiom;
	
	/**
	 * Default constructor that sets property values
	 * to their default values.
	 */
	public LSystemBuilderImpl() {
		this.unitLength = 0.1;
		this.unitLengthDegreeScaler = 1;
		this.origin = new Vector2D(0, 0);
		this.angle = 0;
		this.axiom = "";
		this.commands = new Dictionary();
		this.productions = new Dictionary();
	}
	
	/**
	 * Builds new LSystem from properties of Builder class
	 * @return new LSystem
	 */
	@Override
	public LSystem build() {
		return new LSystemImpl(this);
	}

	/**
	 * Method is used to configurate builder from text.
	 * @param rows is array of string.
	 * 			Each element represents one command.
	 */
	@Override
	public LSystemBuilder configureFromText(String[] rows) {
		Objects.requireNonNull(rows, "Rows argument cannot be null");
		for(int i = 0, size = rows.length; i < size; i++) {
			String row = rows[i].trim().replaceAll("\\s+", " ");
			if(row.isEmpty()) continue;
			
			setOptionFromText(row);
		}
		return this;
	}
	
	/**
	 * Helper method of configurateFromText().
	 * It parses given row and sets options in
	 * builder accordingly.
	 * @param row that contains one command
	 */
	private void setOptionFromText(String row) {
		String[] tokens = row.split("\\s+");

		String function = tokens[0];

		if (function.equals("origin") || function.equals("production")) { //commands with 2 parameters
			if (tokens.length != 3)
				throw new IllegalArgumentException(function + " takes only 2 parameters.");

			if (function.equals("origin")) {
				try {
					double x = Double.parseDouble(tokens[1]);
					double y = Double.parseDouble(tokens[2]);
					setOrigin(x, y);
				} catch (NumberFormatException exc) {
					throw new IllegalArgumentException("Origins arguments must be parsable as double numbers."
							+ " They were '" + tokens[1] + "', '" + tokens[2] + "'.");
				}
			} else {
				if(tokens[1].length() != 1) 
					throw new IllegalArgumentException(
							"'" + tokens[1] + "' in production can't be turned into character.");
				registerProduction(Character.valueOf(tokens[1].charAt(0)), tokens[2]);
			}
			return;
		} else if (function.equals("angle") || function.equals("unitLength") || function.equals("axiom")) { //commands with 1 parameter
			if (tokens.length != 2)
				throw new IllegalArgumentException(function + " takes only 1 parameter.");

			if(function.equals("axiom")) {
				setAxiom(tokens[1]);
				return;
			}
			try {
				double value = Double.parseDouble(tokens[1]);
				if (function.equals("angle")) {
					setAngle(value);
				} else {
					setUnitLength(value);
				}
			} catch (NumberFormatException exc) {
				throw new IllegalArgumentException(function + " argument must be parsable as double number."
						+ " Parameter was '" + tokens[1] + "'.");
			}
		} else if(function.equals("command")) {
			if(tokens.length == 3) {
				if(tokens[1].length() != 1) 
					throw new IllegalArgumentException(
							"'" + tokens[1] + "' in command can't be turned into character.");
				
				registerCommand(tokens[1].charAt(0), tokens[2]); 
			} else if(tokens.length == 4) {
				if(tokens[1].length() != 1) 
					throw new IllegalArgumentException(
							"'" + tokens[1] + "' in command can't be turned into character.");
				
				registerCommand(tokens[1].charAt(0), tokens[2] + " " + tokens[3]);
			} else {
				throw new IllegalArgumentException("Only valid number of arguments for command are 2 or 3 arguments.");
			}
		} else if(function.equals("unitLengthDegreeScaler")) {
			if(tokens.length == 2) {
				try {
					double value = Double.valueOf(tokens[1]);
					setUnitLength(value);
				} catch (NumberFormatException exc) {
					throw new IllegalArgumentException("Parameter for unitlengthdegreescaler must be parsable as double.");
				}
			} else {
				StringBuilder expressionBuilder = new StringBuilder();
				for(int i = 1; i < tokens.length; i++) {
					expressionBuilder.append(tokens[i]);
				}
				
				String expression = expressionBuilder.toString().trim();
				String[] expressions = expression.split("/");
				
				if(expressions.length == 2) {
					try {
						double first = Double.valueOf(expressions[0]);
						double second = Double.valueOf(expressions[1]);
						setUnitLengthDegreeScaler(first/second);
						return;
					} catch (NumberFormatException exc) {
						throw new IllegalArgumentException("Parameter for unitlengthdegreescaler must be parsable as double.");
					}
				}
				
				throw new IllegalArgumentException("Expression is not parsable as number");
			}
		}
	}

	/**
	 * Adds new command to LSystem
	 * @param sign represents command
	 * @param command that can be executed
	 * @return this object
	 */
	@Override
	public LSystemBuilder registerCommand(char sign, String strCommand) {
		Objects.requireNonNull(strCommand, "Given command to registerCommand can't be null.");
		strCommand = strCommand.trim().replaceAll("\\s+", " ");
		String[] parameters = strCommand.split("\\s+");
		
		Command newCommand = null;
		if(parameters.length == 1) {
			newCommand = oneParameterCommand(parameters[0]);
		} else if(parameters.length == 2) {
			newCommand = twoParameterCommand(parameters[0], parameters[1]);
		} else {
			throw new IllegalArgumentException("Command argument must have 1 or 2 parameters."
					+ " It had " + parameters.length + ".");
		}
		
		commands.put(sign, newCommand);
		return this;
	}
	
	/**
	 * Method is used to create command from
	 * string.
	 * @param command string value that represents
	 * 			command
	 * @return new command
	 * @throws IllegalArgumentException if strCommand is not
	 * 			equal to 'pop' or 'push'. Checking is not case sensitive
	 */
	private Command oneParameterCommand(String strCommand){
		if(strCommand.toLowerCase().equals("push")) {
			return new PushCommand();
		} else if(strCommand.toLowerCase().equals("pop")) {
			return  new PopCommand();
		}
		
		throw new IllegalArgumentException("Only valid one parameter commands are POP and PUSH.");
	}
	
	/**
	 * 
	 * Method is used to create command from
	 * two strings
	 * @param command string value that represents
	 * 			command
	 * @param strValue represents argument of that command
	 * @return new command
	 * @throws IllegalArgumentException if strCommand is not
	 * 			equal to 'pop' or 'push'. Checking is not case sensitive
	 */
	private Command twoParameterCommand(String strCommand, String strValue) {
		String lowerCommand = strCommand.toLowerCase();
		
		if(lowerCommand.equals("color")) {
			String colorCode = "#" + strValue;
			
			try {
				Color color = Color.decode(colorCode);
				return new ColorCommand(color);
			} catch(NumberFormatException exc) {
				throw new IllegalArgumentException(strValue + " can't be parsed as color code.");
			}
		}
		
		double value;
		try {
			value = Double.valueOf(strValue);
		} catch(NumberFormatException exc) {
			throw new IllegalArgumentException("Second parameter must be parsable as double value.");
		}
		
		if(lowerCommand.equals("draw")) {
			return new DrawCommand(value);
		} else if(lowerCommand.equals("skip")){
			return new SkipCommand(value);
		} else if(lowerCommand.endsWith("scale")) {
			return new ScaleCommand(value);
		} else if(lowerCommand.endsWith("rotate")) {
			return new RotateCommand(value);
		}
		
		throw new IllegalArgumentException("Not valid command given. Valid commands are 'DRAW', 'SKIP',"
				+ "'SCALE,'\n'ROTATE', 'COLOR'. Was " + strCommand + ".");
	}

	/**
	 * Adds new production to LSystem
	 * @param sign generates given production
	 * @param production
	 * @return this object 
	 */
	@Override
	public LSystemBuilder registerProduction(char sign, String production) {
		Objects.requireNonNull(production, "Given production can't be null.");
		productions.put(sign, production);
		return this;
	}

	/**
	 * Sets angle of LSystem to given angle
	 * @param angle argument is new value of
	 * 			angle property
	 * @return this object
	 */
	@Override
	public LSystemBuilder setAngle(double angle) {
		this.angle = angle;
		return this;
	}

	/**
	 * Sets axiom of LSystem to given axiom
	 * @param axiom argument is new value of 
	 * 			axiom property
	 * @return this object
	 */
	@Override
	public LSystemBuilder setAxiom(String axiom) {
		Objects.requireNonNull(axiom, "Axiom can't be null");
		this.axiom = axiom;
		return this;
	}

	/**
	 * Sets origin to new position
	 * @param x is origins location on
	 * 			x axis
	 * @param y is origins location on 
	 * 			y axis
	 * @throws IllegalArgumentException x and y must be higher
	 * 			or equal to 0 and lower or equal to 1
	 * @return this object
	 */
	@Override
	public LSystemBuilder setOrigin(double x, double y) {
		this.origin = new Vector2D(x, y);
		return this;
	}

	/**
	 * Sets unitLength to new value
	 * @param unitLength argument is used to
	 * 			set new value of unitLength property
	 * @return this object
	 */
	@Override
	public LSystemBuilder setUnitLength(double unitLength) {
		this.unitLength = unitLength;
		return this;
	}

	/**
	 * Sets unitLengthDegreeScaler to new value
	 * @param unitLengthDegreeScaler argument is used to
	 * 			set new value of unitLengthDegreeScaler property
	 * @return this object
	 */
	@Override
	public LSystemBuilder setUnitLengthDegreeScaler(double unitLengthDegreeScaler) {
		this.unitLengthDegreeScaler = unitLengthDegreeScaler;
		return this;
	}
	
	public static class LSystemImpl implements LSystem {

		/**
		 * Dictionary that stores registered commands
		 */
		private Dictionary commands;
		/**
		 * Dictionary that stores registered 
		 */
		private Dictionary productions;
		
		/**
		 * Length of unit move of turtle.
		 * For example 0.5 move to the right would pass half of screen.
		 */
		private double unitLength;
		
		/**
		 * Used to scale unitLenght so overall dimensions
		 * of fractals would stay somehow consistent
		 */
		private double unitLengthDegreeScaler;
		
		/**
		 * Starting point of turtle
		 * (0, 0) => left bottom corner
		 * (1, 1) => right top corner
		 * (0.5, 0.5) => center
		 */
		private Vector2D origin;
		
		/**
		 * Angle defines where turtle is "looking"
		 */
		private double angle;
		
		/**
		 * Starting sequence used for generating
		 * new sequence
		 */
		private String axiom;
		
		/**
		 * Constructor creates LSystemImpl from its Builder class.
		 * @param builder for LSystemImpl
		 */
		private LSystemImpl(LSystemBuilderImpl builder) {
			this.commands = builder.commands;
			this.productions = builder.productions;
			this.unitLength = builder.unitLength;
			this.unitLengthDegreeScaler = builder.unitLengthDegreeScaler;
			this.origin = builder.origin;
			this.angle = builder.angle;
			this.axiom = builder.axiom;
		}

		/**
		 * Method is used to draw on canvas given by painter.
		 * First it generates character sequence, then it reads
		 * every character and calls appropriate commands.
		 * @param level 
		 * @param painter provides canvas and functions to
		 * 			draw on it
		 */
		@Override
		public void draw(int level, Painter painter) {
			Objects.requireNonNull(painter, "Painter can't be null.");
			
			Context ctx = new Context();
			TurtleState startState = new TurtleState(
					origin.copy(), 
					new Vector2D(1, 0).rotated(angle), 
					Color.BLACK,
					unitLength * Math.pow(unitLengthDegreeScaler, level));
			
			ctx.pushState(startState);
			
			String generated = generate(level);
			for(int i = 0, size = generated.length(); i < size; i++) {
				Command currentCommand = (Command) commands.get(generated.charAt(i));
				
				if(currentCommand != null) {
					currentCommand.execute(ctx, painter);
				}
			}
			
		}

		/**
		 * Method is used to generated command
		 * sequence from starting axiom to given level.
		 * Default production is x -> x. x represents any 
		 * valid character.
		 * @param level 
		 */
		@Override
		public String generate(int level) {
			if(level == 0) {
				return axiom;
			} else {
				String generated = generate(level-1);
				
				StringBuilder generator = new StringBuilder();
					
				for(int i = 0, size = generated.length(); i < size; i++){
					String production = (String) productions.get(generated.charAt(i));
					
					if(production == null) {
						generator.append(generated.charAt(i));
					} else {
						generator.append(production);
					}
				}
				return generator.toString();
			}
				
			
		}
		
	}

}
