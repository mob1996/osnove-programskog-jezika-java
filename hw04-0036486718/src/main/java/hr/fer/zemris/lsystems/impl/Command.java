package hr.fer.zemris.lsystems.impl;

import hr.fer.zemris.lsystems.Painter;

/**
 * Interface is used to model what
 * methods commands for turtle should have
 * @author Luka Mijić
 *
 */
public interface Command {

	/**
	 * Method is called to execute command
	 * on given context and painter
	 * @param ctx is Context on which command is executed
	 * @param painter is Painter on which command is executed
	 */
	void execute(Context ctx, Painter painter);
}
