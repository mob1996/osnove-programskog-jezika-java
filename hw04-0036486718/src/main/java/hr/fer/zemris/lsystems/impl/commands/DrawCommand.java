package hr.fer.zemris.lsystems.impl.commands;

import java.util.Objects;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;
import hr.fer.zemris.lsystems.impl.TurtleState;
import hr.fer.zemris.math.Vector2D;
/**
 * Class represents command that draws
 * line.
 * @author Luka Mijić
 *
 */
public class DrawCommand implements Command {

	/**
	 * Step of line
	 */
	private double step;
	
	/**
	 * Constructor that takes one argument
	 * @param step arguments is used to 
	 * 			set value of set property
	 */
	public DrawCommand(double step) {
		this.step = step;
	}


	/**
	 * Method moves turtle to new position
	 * and draws a line connecting 2 positions.
	 * @param ctx is Context
	 * @param painter provides canvas to draw on
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		Objects.requireNonNull(ctx, "Context argument can't be null.");
		Objects.requireNonNull(painter, "Painter argument can't be null.");
		
		TurtleState current = ctx.getCurrentState();
		Vector2D position = current.getPosition();
		Vector2D direction = current.getDirection();
		
		double length = step * current.getMoveLength();
		
		double startX = position.getX();
		double startY = position.getY();
		
		double deltaX = length * direction.getX();
		double deltaY = length * direction.getY();
		
		double endX = startX + deltaX;
		double endY = startY + deltaY;
		
		painter.drawLine(startX, startY, endX, endY, current.getColor(), 1.0f);
		
		position.translate(new Vector2D(deltaX, deltaY));
		
		
	}

}
