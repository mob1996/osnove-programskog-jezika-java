package hr.fer.zemris.lsystems.impl.commands;

import java.util.Objects;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;
/**
 * Class represents command that rotates
 * direction of state at the top of the context stack.
 * @author Luka Mijić
 *
 */
public class RotateCommand implements Command {

	/**
	 * Angle that is used to rotate direction
	 * of current state from Context
	 */
	private double angle;
	
	/**
	 * Constructor that takes one argument angle
	 * @param angle argument is used
	 * 			to set angle property
	 */
	public RotateCommand(double angle) {
		this.angle = angle;
	}

	/**
	 * Method is used to rotate direction vector from
	 * current Context state. 
	 * @param ctx is Context
	 * @param painter provides canvas to draw on
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		Objects.requireNonNull(ctx, "Context argument can't be null.");
		Objects.requireNonNull(painter, "Painter argument can't be null.");
		
		ctx.getCurrentState().getDirection().rotate(angle);
	}

}
