package hr.fer.zemris.lsystems.impl.commands;

import java.util.Objects;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

/**
 * Class represents command that 
 * pop's state from stack.
 * @author Luka Mijić
 *
 */
public class PopCommand implements Command {

	/**
	 * Method is used to pop state from context.
	 * @param ctx is Context
	 * @param painter provides canvas to draw on
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		Objects.requireNonNull(ctx, "Context argument can't be null.");
		Objects.requireNonNull(painter, "Painter argument can't be null.");
		
		ctx.popState();
	}

	
}
