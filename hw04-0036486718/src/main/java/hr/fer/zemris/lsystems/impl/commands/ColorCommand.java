package hr.fer.zemris.lsystems.impl.commands;

import java.awt.Color;
import java.util.Objects;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

/**
 * Class represents commands that
 * that alters color of lines
 * that turtle draws
 * @author Luka Mijić
 *
 */
public class ColorCommand implements Command {

	/**
	 * New colour for turtle lines
	 */
	private Color color;
	
	/**
	 * Constructor that takes one argument
	 * @param color argument is used to set
	 * 			value of color property
	 */
	public ColorCommand(Color color) {
		this.color = Objects.requireNonNull(color, "color argument can't be null.");
		this.color = color;
	}


	/**
	 * Sets color property of current TurtleState
	 * to color property of this class.
	 * @param ctx is Context
	 * @param painter provides canvas to draw on
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		Objects.requireNonNull(ctx, "Context argument can't be null.");
		Objects.requireNonNull(painter, "Painter argument can't be null.");
		
		ctx.getCurrentState().setColor(color);
	}

}
