	package hr.fer.zemris.lsystems.impl.commands;

import java.util.Objects;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;


/**
 * Class represents command that 
 * pushes state to top of the stack
 * @author Luka Mijić
 *
 */
public class PushCommand implements Command {

	/**
	 * Copies current state from top of the Context stack
	 * and places that copy to the top of the stack.
	 * @param ctx is Context
	 * @param painter provides canvas to draw on
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		Objects.requireNonNull(ctx, "Context argument can't be null.");
		Objects.requireNonNull(painter, "Painter argument can't be null.");
		
		ctx.pushState(ctx.getCurrentState().copy());
	}

}
