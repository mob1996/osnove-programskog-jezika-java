package hr.fer.zemris.lsystems.impl.commands;

import java.util.Objects;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;
import hr.fer.zemris.lsystems.impl.TurtleState;
import hr.fer.zemris.math.Vector2D;

/**
 * Class represents commands that
 * teleports turtle to new position.
 * @author Mob
 *
 */
public class SkipCommand implements Command{
	
	/**
	 * Step of line
	 */
	private double step;
	
	/**
	 * Constructor that takes one argument
	 * @param step argument is used to 
	 * 			set value of set property
	 */
	public SkipCommand(double step) {
		this.step = step;
	}


	/**
	 * Method moves turtle to new position.
	 * @param ctx is Context
	 * @param painter provides canvas to draw on
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		Objects.requireNonNull(ctx, "Context argument can't be null.");
		Objects.requireNonNull(painter, "Painter argument can't be null.");
		
		TurtleState current = ctx.getCurrentState();
		
		double length = step * current.getMoveLength();
		
		double deltaX = length * current.getDirection().getX();
		double deltaY = length * current.getDirection().getY();
		
		
		current.getPosition().translate(new Vector2D(deltaX, deltaY));
		
		
	}
}
