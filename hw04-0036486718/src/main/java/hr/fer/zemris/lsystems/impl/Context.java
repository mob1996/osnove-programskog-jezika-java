package hr.fer.zemris.lsystems.impl;

import hr.fer.zemris.java.custom.collections.ObjectStack;

/**
 * Context class remembers states of turtle and 
 * gives user methods to access those states.
 * @author Mob
 *
 */
public class Context {
	
	/**
	 * Stack is used to remember TurtleStates
	 */
	private ObjectStack stateStack;

	/**
	 * Constructor is used to create new Context
	 * with no memory of states.
	 */
	public Context() {
		this.stateStack = new ObjectStack();
	}
	
	/**
	 * Method is used to return current state 
	 * without removing it from stack.
	 * @return current TurtleState
	 */
	public TurtleState getCurrentState() {
		return (TurtleState) stateStack.peek();
	}
	
	/**
	 * Method is used to push given state
	 * to the top of the stack.
	 * @param state pushed on stack
	 */
	public void pushState(TurtleState state) {
		stateStack.push(state);
	}
	
	/**
	 * Method is used to remove state
	 * from top of the stack. 
	 */
	public void popState() {
		stateStack.pop();
	}
}
