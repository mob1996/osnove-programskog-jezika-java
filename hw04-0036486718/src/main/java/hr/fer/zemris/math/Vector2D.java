package hr.fer.zemris.math;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.PI;
import static java.lang.Math.sqrt;

import java.util.Objects;

import static java.lang.Math.pow;

/**
 * Class represents vector with 2 components
 * in 2D space
 * @author Luka Mijić
 *
 */
public class Vector2D {
	
	/**
	 * Length and orientation of vector
	 * in x direction
	 */
	private double x;
	/*
	 * 
	 * Length and orientation of vector
	 * in y direction
	 */
	private double y;
	
	/**
	 * Constructor that sets values of 
	 * x and y Vector2D properties
	 * @param x value given to x property
	 * @param y value given to y property
	 */
	public Vector2D(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}
	
	/**
	 * Translates and modifies current 
	 * vector.
	 * @param offset is another Vector2D value.
	 * 			x component of offset is added to x property and
	 * 			y component of offset is added to y property
	 */
	public void translate (Vector2D offset) {
		Objects.requireNonNull(offset, "offset argument can't be null");
		
		this.x = this.x + offset.getX();
		this.y = this.y + offset.getY();
	}
	
	/**
	 * Method is used to get new Vector2D that is created
	 * by translating current Vector2D by offset Vector2D
	 * @param offset is Vector2D that defines by how
	 * 			much is new Vector2D translated from 
	 * 			current one
	 * @return new Vector2D 
	 */
	public Vector2D translated(Vector2D offset) {
		Vector2D copy = this.copy();
		copy.translate(offset);
		return copy;
	}
	
	/**
	 * Rotates and modifies current 
	 * vector. 
	 * @param angle by which current vector is rotated.
	 */
	public void rotate(double angle) {
		double x = this.x;
		double y = this.y;
		double rad = degreeToRad(angle);
		
		this.x = x * cos(rad) - y * sin(rad);
		this.y = x * sin(rad) + y * cos(rad);
	}
	
	/**
	 * Method is used to get new Vector2D that is created
	 * by rotating current Vector2D by given angle.
	 * @param angle for which method rotates current vector
	 * @return new Vector2D
	 */
	public Vector2D rotated(double angle) {
		Vector2D copy = this.copy();
		copy.rotate(angle);
		return copy;
	}
	
	/**
	 * Scales and modifies current vector.
	 * @param scaler is value by which current 
	 * 			vector is being modified.
	 */
	public void scale(double scaler) {
		this.x = scaler * this.x;
		this.y = scaler * this.y;
	}
	
	/**
	 * Method is used to get new Vector2D that is created
	 * by scaling current Vector2D by given value.
	 * @param scaler is value by which is current vector scaled
	 * @return new Vector2D
	 */
	public Vector2D scaled(double scaler) {
		Vector2D copy = this.copy();
		copy.scale(scaler);
		return copy;
	}
	
	/**
	 * Normalize current vector
	 */
	public void normalize() {
		double scaleToUnit = 1 / (sqrt(pow(this.x, 2) + pow(this.y, 2)));
		this.scale(scaleToUnit);
	}
	
	/**
	 * Returns new vector that is normalization of current vector
	 * @return new normalized vector
	 */
	public Vector2D normalized() {
		Vector2D copy = this.copy();
		copy.normalize();
		return copy;
	}
	
	/**
	 * @return new vector from values of current vector
	 */
	public Vector2D copy() {
		return new Vector2D(this.x, this.y);
	}
	
	/**
	 * Turns degrees into radians
	 * @param angle in degrees
	 * @return angle in radians
	 */
	private double degreeToRad(double angle) {
		return angle * (PI/180.0);
	}
	
}
