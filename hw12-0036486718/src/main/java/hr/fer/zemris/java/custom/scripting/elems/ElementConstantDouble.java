package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Class extends Element class and represents
 * constant double element.
 * @author Luka Mijić
 *
 */
public class ElementConstantDouble extends Element {

	/**
	 * Double value that class stores
	 */
	private double value;
	
	/**
	 * Constructor that sets value to argument 
	 * value. 
	 * @param value
	 */
	public ElementConstantDouble(double value) {
		this.value = value;
	}
	
	public double getValue() {
		return value;
	}
	
	/**
	 * @return String representation of 
	 * 			double value
	 */
	@Override
	public String asText() {
		return String.valueOf(value);
	}
}
