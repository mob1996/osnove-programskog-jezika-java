package hr.fer.zemris.java.custom.scripting.demo;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hr.fer.zemris.java.custom.scripting.exec.SmartScriptEngine;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.webserver.RequestContext;
import hr.fer.zemris.java.webserver.RequestContext.RCCookie;

/**
 * Demonstration of executing script "webroot/scripts/fibonacci_html.smscr" using
 * {@link SmartScriptEngine}.
 * 
 * @author Luka Mijić
 *
 */
public class SmartScriptEngineDemoFibonacciHtml {

	/**
	 * Sets up and executes "webroot/scripts/fibonacci_html.smscr" script.
	 * 
	 * @param args
	 *            no arguments expected
	 */
	public static void main(String[] args) {
		try {
			String documentBody = new String(Files.readAllBytes(Paths.get("webroot/scripts/fibonacci_html.smscr")),
					StandardCharsets.UTF_8);
			Map<String, String> parameters = new HashMap<String, String>();
			Map<String, String> persistentParameters = new HashMap<String, String>();
			List<RCCookie> cookies = new ArrayList<RequestContext.RCCookie>();
			// create engine and execute it
			new SmartScriptEngine(new SmartScriptParser(documentBody).getDocumentNode(),
					new RequestContext(System.out, parameters, persistentParameters, cookies)).execute();
		} catch (IOException e) {
			System.out.println("Error while reading file.");
		}
	}
}
