package hr.fer.zemris.java.webserver.workers;

import java.io.IOException;
import java.util.Map;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * Class implements {@link IWebWorker}.
 * It generates HTML body while listing are parameters in given context.
 * @author Luka Mijić
 *
 */
public class EchoParams implements IWebWorker{

	/**
	 * Generates HTML file that lists
	 * all parameters stored in parameters {@link Map} in 
	 * given {@link RequestContext}.
	 * @param context is used for dispatching information
	 */
	@Override
	public void processRequest(RequestContext context) throws Exception {
		try {
			context.write("<html><head>");
			context.write("<style>");
			context.write("table, th, td {\r\n" + 
						  "border: 1px solid black;\r\n" + 
						  "}");
			context.write("</style>\r\n" + 
						  "</head>\r\n" + 
						  "<body>");
			
			context.write("<h2>Parameter Table</h2>");
			context.write("<table style=\"width:20%\">");
			
			for(String name:context.getParameterNames()) {
				context.write("<tr>");
				context.write("<td>" + name + "</td>");
				context.write("<td>" + context.getParameter(name) + "</td>");
				context.write("</tr>");
			}
			
			context.write("</table>\r\n" +
						  "</body>\r\n" + 
						  "</html>");
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		
	}
	

}
