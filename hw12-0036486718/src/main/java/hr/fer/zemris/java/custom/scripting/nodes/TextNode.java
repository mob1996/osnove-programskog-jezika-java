package hr.fer.zemris.java.custom.scripting.nodes;


/**
 * Class represents one text node in syntax tree.
 * Class extends Node class.
 * @author Luka Mijić
 *
 */
public class TextNode extends Node{

	/**
	 * Stored text in TextNode
	 */
	private String text;
	
	/**
	 * Constructor that creates TextNode
	 * @param text argument is saved to
	 * 			text class property
	 * @throws NullPointerException if argument text == null
	 */
	public TextNode(String text) {
		super();
		if(text == null) throw new NullPointerException("Text argument in TextNode constructor can't be null.");
		this.text = text;
	}

	public String getText() {
		return text;
	}
	
	@Override
	public String toString() {
		String newText = text.replace("\\", "\\\\");	
		newText = newText.replace("{$", "\\{$");
		return newText;
	}
	
	@Override
	public void accept(INodeVisitor visitor) {
		visitor.visitTextNode(this);
	}
	
}
