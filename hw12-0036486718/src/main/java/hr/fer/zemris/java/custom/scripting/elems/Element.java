package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Base class used to make skeleton
 * for classes that inherit it.
 * @author Luka Mijić
 *
 */
public class Element {

	/**
	 * Method that returns String representation
	 * of Element class
	 * @return empty String
	 */
	public String asText() {
		return "";
	}
}
