package hr.fer.zemris.java.webserver;

/**
 * Interface models classes that can process certain {@link RequestContext}.
 * @author Luka Mijić
 *
 */
public interface IWebWorker {

	/**
	 * Process given {@link RequestContext}.
	 * @param context that is processed
	 * @throws Exception
	 */
	public void processRequest(RequestContext context) throws Exception;
}
