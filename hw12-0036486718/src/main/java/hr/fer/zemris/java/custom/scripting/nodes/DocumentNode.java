package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * Class represents one Document node in syntax tree.
 * DocumentNode is starting point in syntax tree.
 * Class extends Node class.
 * @author Mob
 *
 */
public class DocumentNode extends Node {


	@Override
	public String toString() {
		return "";
	}
	
	@Override
	public void accept(INodeVisitor visitor) {
		visitor.visitDocumentNode(this);
	}
}
