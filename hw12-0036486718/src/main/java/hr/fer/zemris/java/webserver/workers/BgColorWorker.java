package hr.fer.zemris.java.webserver.workers;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * Class implements {@link IWebWorker}.
 * It takes parameter set at "bgcolor" key using
 * {@link RequestContext#getParameter(String)}.
 * Decodes it and sets persistent parameter
 * "background" to decoded value. If value can't be decoded
 *  default color is used.
 * @author Luka Mijić
 *
 */
public class BgColorWorker implements IWebWorker{

	/**
	 * Key of color parameter
	 */
	private static final String BG_COLOR_KEY = "bgcolor";
	
	/**
	 * Default color
	 */
	private static final String DEF_COLOR = "7F7F7F";
	
	/**
	 * Letters that are valid in hex color format
	 */
	private static final char[] validLetters = {'A', 'B', 'C', 'D', 'E', 'F'};
	
	/**
	 * Takes value assigned to parameter key "bgcolor".
	 * If there are no parameters, default color is set.
	 * Otherwise method calls this{@link #decodable(String)}
	 * to see if value can be decoded to color.
	 * If it can't color is not changed, otherwise set
	 * persistent parameter "background" to decoded value.
	 * @param context is used for dispatching information and storing parameters
	 */
	@Override
	public void processRequest(RequestContext context) throws Exception {
		String color = context.getParameter(BG_COLOR_KEY);
		
		String message = "";
		if(color == null) {
			context.setPersistentParameter(BG_COLOR_KEY, DEF_COLOR);
			message = "Color reset.";
		} else {
			boolean decodable = decodable(color);
			if(decodable) {
				message = "Color was set.";
				context.setPersistentParameter(BG_COLOR_KEY, color);
			} else {
				message = "Color was not changed.";
			}
		}
		
		context.write("<html><body>");
		context.write("<h1>Color</h1>");
		context.write("<p>" + message + ":" + "<a href=\"/index2.html\">Index</a>" + "</p>");
		
		
		context.write("</body></html>");
		
	}
	
	/**
	 * @param colorStr {@link String} that is decoded
	 * @return true if given {@link String} can be decoded to color,
	 * 			otherwise return false
	 */
	private boolean decodable(String colorStr) {
		if(colorStr.length() != 6) return false;
		
		for(int i = 0; i < colorStr.length(); i++) {
			if(Character.isDigit(colorStr.charAt(i))) {
				continue;
			}
			
			char upperChar = Character.toUpperCase(colorStr.charAt(i));
			boolean validLetter = false;
			for(int j = 0; j < validLetters.length; j++) {
				if(upperChar == colorStr.charAt(i)) {
					validLetter = true;
					break;
				}
			}
			
			if(validLetter) continue;
			return false;
		}
		
		return true;
	}
}
