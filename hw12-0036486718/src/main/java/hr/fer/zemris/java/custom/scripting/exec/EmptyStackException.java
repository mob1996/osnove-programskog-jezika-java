package hr.fer.zemris.java.custom.scripting.exec;

/**
 * Class EmptyStackException exception that extends RuntimeException
 * and it is thrown when user tries to pop or peek at empty stack. 
 * @author Luka Mijić
 * 
 *
 */
public class EmptyStackException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public EmptyStackException(String message) {
		super(message);
	}
}
