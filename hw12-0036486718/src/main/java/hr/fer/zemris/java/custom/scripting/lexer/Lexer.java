package hr.fer.zemris.java.custom.scripting.lexer;

import java.math.BigDecimal;
import java.math.BigInteger;

import hr.fer.zemris.java.custom.collections.ArrayIndexedCollection;



public class Lexer {
	
	/**
	 * Array where all valid operators are stored.
	 */
	private static ArrayIndexedCollection operators;
	{
		operators = new ArrayIndexedCollection();
		operators.add(Character.valueOf('+'));
		operators.add(Character.valueOf('-'));
		operators.add(Character.valueOf('*'));
		operators.add(Character.valueOf('/'));
		operators.add(Character.valueOf('^'));
	}
	/**
	 * Input text is stored into char array
	 */
	private char[] data;
	
	/**
	 * Current token
	 */
	private Token token;
	
	/**
	 * Index of first index that is not used.
	 * Initial value is 0.
	 */
	private int currentIndex;
	
	/**
	 * Current state of lexical analyser.
	 */
	private LexerState state;	
	
	/**
	 * Constructor takes String text and transforms it
	 * into char array. 
	 * @param text that is transformed
	 * @throws IllegalArgumentException if given argument is null
	 */
	public Lexer(String text) {
		if(text == null) throw new NullPointerException("Text argument in Lexer constructor cannot be null.");
	
		data = text.toCharArray();
		state = LexerState.TEXT_MODE;
	}

    /**
     * @return token that was generated last
     */
	public Token getToken() {
		return token;
	}
	
	/**
	 * Sets parameter state to argument state.
	 * @param state is new state of lexer
	 */
	public void setState(LexerState state) {
		if(state == null) throw new IllegalArgumentException("Given state cannot be null.");
		this.state = state;
	}
	
	/**
	 * Method is used to generate next token from input string.
	 * @return next token from string
	 * @throws LexerException if EOF was reached
	 */
	public Token nextToken() {
		if(token != null && token.getType() == TokenType.EOF) throw new LexerException("No more tokens avaliable");
	
		if(currentIndex >= data.length) {
			token = new Token(TokenType.EOF, null);
			return token;
		}
		
		token = state == LexerState.TEXT_MODE ? nextTokenTextMode() : nextTokenTagMode();
		
		return token;
	}
	
	/**
	 * Method that is called from nextToken() if Lexer works
	 * in TEXT_MODE
	 * @return new generated token in text mode
	 */
	private Token nextTokenTextMode() {
		StringBuilder textModeBuilder = new StringBuilder();
		TokenType type = TokenType.TEXT;
		if (data[currentIndex] == '{') {
			textModeBuilder.append(data[currentIndex++]);

			if (currentIndex >= data.length)
				throw new LexerException("'{' cannot be at the end of the document");
			if (data[currentIndex] == '$') {
				textModeBuilder.append(data[currentIndex++]);
				type = TokenType.START_TAG;
				return new Token(type, textModeBuilder.toString());
			}
		}

		boolean escaped = false;
		while (currentIndex < data.length) {
			if (data[currentIndex] == '\\' && !escaped) {
				currentIndex++;
				if (currentIndex >= data.length)
					throw new LexerException("'\\' cannot be at the end of the document");
				escaped = true;
				continue;
			}

			if (data[currentIndex] == '{' && !escaped) {
				if (currentIndex + 1 < data.length) {
					if(data[currentIndex + 1] == '$') break;
				}
			} else if (data[currentIndex] != '\\' && data[currentIndex] != '{' && escaped) {
				// means that escaped character is read but next one isn't '\' or '{'
				throw new LexerException("'\\' must be followed by '\\' or '{'");
			}

			escaped = false;
			textModeBuilder.append(data[currentIndex++]);
		}
		return new Token(type, replaceEscapes(textModeBuilder.toString()));

	}
	
	/**
	 * Method that is called from nextToken() if Lexer works
	 * in TAG_MODE. It utilizes many helper method that are used
	 * to generate tokens of different types. 
	 * @return new generated token
	 */
	private Token nextTokenTagMode() {
		skipWhiteSpace();
		
		if(currentIndex >= data.length) {
			return new Token(TokenType.EOF, null);
		} else if(data[currentIndex] == '$') {
			currentIndex++;
			if(currentIndex >= data.length) throw new LexerException("'$' cannot be at the end of the document");
			if(data[currentIndex++] != '}') throw new LexerException("'$' must be followed by '} in TAG_MODE");
			return new Token(TokenType.END_TAG, "$}");
		} else if(data[currentIndex] == '"') {
			return nextStringTagMode();
		} else if(Character.isLetter(data[currentIndex])) {
			return nextVariableTagMode();
		} else if(data[currentIndex] == '@') {
			return nextFunctionTagMode();
		} else if(data[currentIndex] == '-') {
			currentIndex++;
			if((currentIndex) >= data.length) {
				return new Token(TokenType.OPERATOR, Character.valueOf('-'));
			} else if(!Character.isDigit(data[currentIndex])) {
				return new Token(TokenType.OPERATOR, Character.valueOf('-'));
			} else {
				return nextNumberTagMode(false);
			}
		} else if(Character.isDigit(data[currentIndex])){
			return nextNumberTagMode(true);
		} else if(operators.contains(Character.valueOf(data[currentIndex]))){
			return new Token(TokenType.OPERATOR, Character.valueOf(data[currentIndex++]));
		} else if(data[currentIndex] == '=') {
			currentIndex++;
			return new Token(TokenType.ECHO_TAG_NAME, Character.valueOf('='));
		}
		
		throw new LexerException("Cannot generate valid token.");
	}
	
	/**
	 * Method is used to generate new token of type STRING.
	 * STRING tokens are tokens generated in TAG_MODE. Characters
	 * of that token must be enclosed by ".
	 * @return new STRING token.
	 */
	private Token nextStringTagMode() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(data[currentIndex++]);
		while(currentIndex < data.length) {
			stringBuilder.append(data[currentIndex]);
			if(data[currentIndex++] == '"') break;
		}
		String build = stringBuilder.toString();
		if(!build.endsWith("\"")) throw new LexerException("String must start and end with \"");
		
		return new Token(TokenType.STRING, replaceEscapes(build));
	}
	
	/**
	 * Method is used to generate new token of type VARIABLE.
	 * VARIABLE tokens are tokens generated in TAG_MODE.
	 * @return new VARIABLE token
	 */
	private Token nextVariableTagMode() {
		StringBuilder variableBuilder = new StringBuilder();
	
		while(currentIndex < data.length) {
			char current = data[currentIndex];
			if(!Character.isLetter(current) && !Character.isDigit(current) && current != '_')
				break;
			
			variableBuilder.append(current);
			currentIndex++;
		}
		
		return new Token(TokenType.VARIABLE, variableBuilder.toString());
	}
	
	/**
	 * Method is used to generate new token of type FUNCTION.
	 * FUNCTION tokens are tokens generated in TAG_MODE.
	 * Method also uses method nextVariableTagMode() because
	 * FUNCTION token is defined as @ followed by string that can
	 * be interpreted as VARIABLE token.
	 * @return new VARIABLE token
	 */
	private Token nextFunctionTagMode() {
		StringBuilder functionBuilder = new StringBuilder();
		functionBuilder.append(data[currentIndex++]);
		
		if(currentIndex >= data.length) throw new LexerException("'@' must not be at the end of the file.");
		if(!Character.isLetter(data[currentIndex])) throw new LexerException("'@' must be followed by letter in TAG_MODE");
		
		functionBuilder.append(data[currentIndex++]);
		functionBuilder.append((String) nextVariableTagMode().getValue());
		
		return new Token(TokenType.FUNCTION, functionBuilder.toString());
	}
	
	/**
	 * Method is used to generate DOUBLE or INTEGER token
	 * while lexer works in TAG_MODE.
	 * Token is considered DOUBLE type if it contains '.' in it, otherwise
	 * it is considered INTEGER. 
	 * @param positive is true if lexer didn't read '-' before number
	 * @return new Token of INTEGER or DOUBLE TYPE
	 */
	private Token nextNumberTagMode(boolean positive) {
		StringBuilder numberBuilder = new StringBuilder();
		TokenType type = TokenType.INTEGER;
		
		while(currentIndex < data.length) {
			if(data[currentIndex] == '.') {
				if(type == TokenType.INTEGER) {
					type = TokenType.DOUBLE;
				} else {
					throw new LexerException("Number cannot be tokenized with more than one dot.");
				}
			} else if(!Character.isDigit(data[currentIndex]) || operators.contains(Character.valueOf(data[currentIndex]))) {
				break;
			}
			
			numberBuilder.append(data[currentIndex++]);
		}
		
		String number = numberBuilder.toString();

		if(type == TokenType.INTEGER) {
			BigInteger bigInt = new BigInteger(number);
			BigInteger maxInt = BigInteger.valueOf(Integer.MAX_VALUE);
			
			if(bigInt.compareTo(maxInt) > 0) throw new LexerException("Number is too big to be parsed as INT");
			
		} else if(type == TokenType.DOUBLE) {
			BigDecimal bigDec = new BigDecimal(number);
			BigDecimal maxDouble = BigDecimal.valueOf(Double.MAX_VALUE);
			BigDecimal minDouble = BigDecimal.valueOf(Double.MIN_VALUE);
			
			if(bigDec.compareTo(maxDouble) > 0) throw new LexerException("Number is too big to be parsed as Double");
			if(bigDec.compareTo(minDouble) < 0) throw new LexerException("Number is too small to be parsed as Double");
			
		} else {
			throw new LexerException("Type has to be INTEGER or DOUBLE.");
		}

		number = positive ? number : '-' + number;
		
		if(type == TokenType.INTEGER) {
			return new Token(type, Integer.valueOf(number));
		} else {
			return new Token(type, Double.valueOf(number));
		}
		
		
	}
	
	
	/**
	 * Method is used to skip all white space from current index 
	 * onwards. After calling this method index is at location of next
	 * non whitespace character or it is out of bounds. 
	 */
	private void skipWhiteSpace() {
		while(currentIndex < data.length) {
			if(!Character.isWhitespace(data[currentIndex])) break;
			currentIndex++;
		}
	}
	
	/**
	 * Method replaces Strings of two characters "\(n,r,t)" with
	 * single character '\(n,r,t)'
	 * @param string that replaces escapes
	 * @return string with replaced escapes
	 */
	private String replaceEscapes(String string) {
		string = string.replace("\\n", "\n");
		string = string.replace("\\r", "\r");
		string = string.replace("\\t", "\t");
		return string;
	}
}


