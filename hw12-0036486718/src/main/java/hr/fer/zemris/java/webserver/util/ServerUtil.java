package hr.fer.zemris.java.webserver.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * This utility class implements several static methods
 * that are useful for working with servers.
 * @author Luka Mijić
 *
 */
public class ServerUtil {

	/**
	 * Method is used for reading HTTP header from
	 * input stream.
	 * @param is that is read
	 * @return header in form of byte array
	 * @throws IOException
	 */
	public static byte[] readRequest(InputStream is) throws IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		int state = 0;
		l: while (true) {
			int b = is.read();
			if (b == -1)
				return null;
			if (b != 13) {
				bos.write(b);
			}
			switch (state) {
			case 0:
				if (b == 13) {
					state = 1;
				} else if (b == 10)
					state = 4;
				break;
			case 1:
				if (b == 10) {
					state = 2;
				} else
					state = 0;
				break;
			case 2:
				if (b == 13) {
					state = 3;
				} else
					state = 0;
				break;
			case 3:
				if (b == 10) {
					break l;
				} else
					state = 0;
				break;
			case 4:
				if (b == 10) {
					break l;
				} else
					state = 0;
				break;
			}
		}
		return bos.toByteArray();
	}

	/**
	 * Transform header into {@link List} of lines.
	 * @param requestHeader is header that is transformed
	 * @return {@link List} of header lines.
	 */
	public static List<String> extractHeaders(String requestHeader) {
		List<String> headers = new ArrayList<String>();
		String currentLine = null;
		for (String s : requestHeader.split("\n")) {
			if (s.isEmpty())
				break;
			char c = s.charAt(0);
			if (c == 9 || c == 32) {
				currentLine += s;
			} else {
				if (currentLine != null) {
					headers.add(currentLine);
				}
				currentLine = s;
			}
		}
		if (!currentLine.isEmpty()) {
			headers.add(currentLine);
		}
		return headers;
	}

	/**
	 * Send error message to given {@link OutputStream}.
	 * @param ostream error message destination
	 * @param statusCode of error
	 * @param statusText of error
	 * @throws IOException
	 */
	public static void sendError(OutputStream ostream, int statusCode, String statusText) throws IOException {
		ostream.write(
				("HTTP/1.1 "+statusCode+" "+statusText+"\r\n"+
				"Server: simple java server\r\n"+
				"Content-Type: text/plain;charset=UTF-8\r\n"+
				"Content-Length: 0\r\n"+
				"Connection: close\r\n"+
				"\r\n").getBytes(StandardCharsets.US_ASCII)
			);
		ostream.flush();
	}
	
	/**
	 * Method is used for generating Session ID.
	 * Length of ID is 20 and all characters are
	 * upper case letters.
	 * @param rand is random value generator
	 * @return SID
	 */
	public static String generateSID(Random rand) {
		char[] sidChars = new char[20];
		for(int i = 0; i < sidChars.length; i++) {
			sidChars[i] = (char) (rand.nextInt(26) + 65); 
		}
		return new String(sidChars);
	}
	
	/**
	 * Calculate future time in milliseconds by
	 * adding given milliseconds time to current milliseconds time.
	 * @param futureMilis  
	 * @return current time milliseconds + <code>futureMilis</code>.
	 */
	public static long getFutureTimeMilis(long futureMillis) {
		return System.currentTimeMillis() + futureMillis;
	}
}
