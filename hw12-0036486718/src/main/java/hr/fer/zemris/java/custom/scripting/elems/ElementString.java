package hr.fer.zemris.java.custom.scripting.elems;
/**
 * Class extends Element class and represents
 * String element. 
 * @author Luka Mijić
 *
 */
public class ElementString extends Element {
	/**
	 * String value that class stores
	 */
	private String value;
	
	/**
	 * Constructor that sets value to argument 
	 * value. 
	 * @param value
	 */
	public ElementString(String value) {
		if(value == null) throw new IllegalArgumentException("Argument value cannot be null.");
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
	/**
	 * @return String value
	 */
	@Override
	public String asText() {
		return value;
	}
}
