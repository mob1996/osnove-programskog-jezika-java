package hr.fer.zemris.java.custom.scripting.parser;

import hr.fer.zemris.java.custom.collections.ArrayIndexedCollection;
import hr.fer.zemris.java.custom.collections.ObjectStack;
import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantDouble;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantInteger;
import hr.fer.zemris.java.custom.scripting.elems.ElementFunction;
import hr.fer.zemris.java.custom.scripting.elems.ElementOperator;
import hr.fer.zemris.java.custom.scripting.elems.ElementString;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;
import hr.fer.zemris.java.custom.scripting.lexer.Lexer;
import hr.fer.zemris.java.custom.scripting.lexer.LexerState;
import hr.fer.zemris.java.custom.scripting.lexer.Token;
import hr.fer.zemris.java.custom.scripting.lexer.TokenType;
import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.Node;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParserException;

/**
 * Class SmartScriptParser is used for parsing input text
 * into syntax tree of Nodes. It uses Lexer for generating
 * tokens.
 * @author Luka Mijić
 *
 */
public class SmartScriptParser {

	/**
	 * Top of this stack is current parent node.
	 */
	private ObjectStack parentStack;
	private Lexer lexer;
	private DocumentNode documentNode;
	
	/**
	 * Constructor of SmartScriptParser.
	 * It creates new empty stack and pushed new DocumentNode on top of it.
	 * Also it creates new Lexer using documentBody as argument.
	 * @param documentBody argument is used in creation of new Lexer
	 */
	public SmartScriptParser(String documentBody) {
		if(documentBody == null) throw new IllegalArgumentException("Argument documentBody cannot be null.");
		parentStack = new ObjectStack();
		documentNode = new DocumentNode();
		parentStack.push(documentNode);
		
		lexer = new Lexer(documentBody);
		
		parse();
	}
	
	/**
	 * Method is used to parsing documentBody into
	 * syntax tree.
	 */
	private void parse() {
		
		while(true) {
			lexer.nextToken();
			if(isTokenOfType(TokenType.EOF)) {
				break;
			} else if(isTokenOfType(TokenType.START_TAG)) {
				changeLexerState();
				parseTag();
			} else if(isTokenOfType(TokenType.END_TAG)) {
				lexer.setState(LexerState.TEXT_MODE);
			} else if(isTokenOfType(TokenType.TEXT)){
				TextNode newTextNode = new TextNode((String)lexer.getToken().getValue());
				addChildToTopNode(newTextNode);
			}
		}
		
		if(parentStack.size() != 1) {
			throw new SmartScriptParserException("Only one node expected on stack.");
		}
	}
	
	/**
	 * Method is called after reading token of type START_TAG.
	 * It is used to parse content inside of tags.
	 */
	private void parseTag() {
		lexer.nextToken();
		if(isTokenOfType(TokenType.ECHO_TAG_NAME)) {
			lexer.nextToken();
			parseEcho();
		} else if(isTokenOfType(TokenType.VARIABLE)) {
			String tokenValue = (String)lexer.getToken().getValue();
			tokenValue = tokenValue.toLowerCase();
			
			if("for".equals(tokenValue)) {
				lexer.nextToken();
				parseFor();
			} else if("end".equals(tokenValue)) {
				lexer.nextToken();
				if(!isTokenOfType(TokenType.END_TAG)) {
					throw new SmartScriptParserException("Tag name 'end' must be followed by END_TAG type token.");
				}
				parentStack.pop();
				if(parentStack.size() <= 0) {
					throw new SmartScriptParserException("Stack must not be empty");
				}
				changeLexerState();
			} else {
				throw new SmartScriptParserException("Only valid tag names are 'for', 'end' and '='.");
			}
		} else {
			throw new SmartScriptParserException("Only valid types for tag name are ECHO_TAG_NAME and VARIABLE.");
		}
	}
	
	/**
	 * Method is used to parse contents of echo tags.
	 * @throws SmartScriptParserException if contents inside of echo tags cannot be parsed.
	 */
	private void parseEcho() {
		ArrayIndexedCollection elementsArray = new ArrayIndexedCollection();
		while(!isTokenOfType(TokenType.END_TAG)) {
			if(isTokenOfType(TokenType.EOF))
				throw new SmartScriptParserException("EOF shouldnt be read inside echo tag.");
			
			Element elem = tokenToElement();
			
			
			elementsArray.add(elem);
			lexer.nextToken();
		}
		changeLexerState();
		
		int size = elementsArray.size();
		Element[] elements = new Element[size];
		
		for(int i = 0; i < size; i++) {
			elements[i] = (Element) elementsArray.get(i);
		}
		
		addChildToTopNode(new EchoNode(elements));
	}
	
	/**
	 * Method is used to parse contents inside for tags.
	 * @throws SmartScriptParserException if contents inside of for tags cannot be parsed.
	 */
	private void parseFor() {
		ElementVariable variable;
		Element startExpression;
		Element endExpression;
		Element stepExpression = null;
		
		if(!isTokenOfType(TokenType.VARIABLE)) {
			throw new SmartScriptParserException("First parameter in FOR tag must be variable.");
		}
		variable = new ElementVariable((String) lexer.getToken().getValue());
		lexer.nextToken();
		
		if(!isValidForParameter()) {
			throw new SmartScriptParserException("Second parameter in for is invalid.");
		}
		
		startExpression = tokenToElement();
		lexer.nextToken();
		
		if(!isValidForParameter()) {
			throw new SmartScriptParserException("Third parameter in for is invalid.");
		}
		
		endExpression = tokenToElement();
		lexer.nextToken();
		
		if(!isValidForParameter()) {
			if(!isTokenOfType(TokenType.END_TAG))
				throw new SmartScriptParserException("Either valid forth parameter or END_TAG expected in for tag.");
		} else {
			stepExpression = tokenToElement();
			lexer.nextToken();
		}
		
		if(!isTokenOfType(TokenType.END_TAG)) {
			throw new SmartScriptParserException("END_TAG expected in for tag.");
		}
		
		ForLoopNode forLoop = new ForLoopNode(variable, startExpression, endExpression, stepExpression);
		addChildToTopNode(forLoop);
		parentStack.push(forLoop);
		changeLexerState();
	}
	
	public DocumentNode getDocumentNode() {
		return (DocumentNode) parentStack.peek();
	}

	/**
	 * Helper method that is used to check if current token is
	 * same type as given type
	 * @param type that we are checking
	 * @return true if current token's type is same as type
	 */
	private boolean isTokenOfType(TokenType type) {
		return lexer.getToken().getType() == type;
	}
	
	/**
	 * Method is used to switch lexer state.
	 */
	private void changeLexerState() {
		if(lexer.getToken().getType() == TokenType.START_TAG) {
			lexer.setState(LexerState.TAG_MODE);
		} else if(lexer.getToken().getType() == TokenType.END_TAG){
			lexer.setState(LexerState.TEXT_MODE);
		} else {
			throw new SmartScriptParserException("Lexer state can only be changed after reading token of type START/END_TAG.");
		}
	}
	
	/**
	 * Method that is used to create new object
	 * of class Element from information of current
	 * token.
	 * @return new element
	 */
	private Element tokenToElement() {
		Token token = lexer.getToken();
		Element elem;
		if(isTokenOfType(TokenType.VARIABLE)) {
			elem = new ElementVariable((String) token.getValue());
		} else if(isTokenOfType(TokenType.INTEGER)) {
			elem = new ElementConstantInteger((Integer) token.getValue());
		} else if(isTokenOfType(TokenType.DOUBLE)) {
			elem = new ElementConstantDouble((Double) token.getValue());
		} else if(isTokenOfType(TokenType.FUNCTION)) {
			elem = new ElementFunction((String) token.getValue());
		} else if(isTokenOfType(TokenType.OPERATOR)) {
			Character operator = (Character) token.getValue();
			elem = new ElementOperator(String.valueOf(operator));
		} else if(isTokenOfType(TokenType.STRING)) {
			elem = new ElementString((String) token.getValue());
		} else {
			throw new SmartScriptParserException("Token cannot be turned into element " + token.getValue());
		}
		return elem;
	}
	
	/**
	 * Check if token of given type can be
	 * valid parameter in for loop.
	 * @return true if it can be valid parameter,
	 * 			otherwise return false
	 */
	private boolean isValidForParameter() {
		if(isTokenOfType(TokenType.VARIABLE)) return true;
		if(isTokenOfType(TokenType.INTEGER)) return true;
		if(isTokenOfType(TokenType.DOUBLE)) return true;
		if(isTokenOfType(TokenType.STRING)) return true;
		return false;
	}
	
	/**
	 * Method is used to add child node to the children
	 * array in Node that is at the top of the stack.
	 * @param child that is being added.
	 */
	private void addChildToTopNode(Node child) {
		Node currentTopNode = (Node) parentStack.peek();
		currentTopNode.addChild(child);
	}
}
