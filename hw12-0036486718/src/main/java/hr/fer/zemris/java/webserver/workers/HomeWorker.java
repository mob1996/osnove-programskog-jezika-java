package hr.fer.zemris.java.webserver.workers;

import hr.fer.zemris.java.webserver.IDispatcher;
import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * Implements {@link IWebWorker}.
 * Creates and sends home html file.
 * @author Luka Mijić
 *
 */
public class HomeWorker implements IWebWorker {
	
	/**
	 * Key to color in persistent parameters map
	 */
	private static final String PP_BG_KEY = "bgcolor";
	
	/**
	 * Key to color in temporary parameters map
	 */
	private static final String TP_BG_KEY = "background";
	
	/**
	 * Default color
	 */
	private static final String DEF_COLOR = "7F7F7F";
	
	/**
	 * Takes value assigned to "bgcolor" in persistent parameter map
	 * and stores it to "background" in temporary parameter map.
	 * Dispatch request to {@link IDispatcher} stored in given
	 * {@link RequestContext} and give path of 'home'
	 * script.
	 */
	@Override
	public void processRequest(RequestContext context) throws Exception {
		String persistentColor = context.getPersistentParameter(PP_BG_KEY);
		persistentColor = persistentColor == null ? DEF_COLOR : persistentColor;
		context.setTemporaryParameter(TP_BG_KEY, persistentColor);
		
		
		context.getDispatcher().dispatchRequest("/private/home.smscr");
	}
	

}
