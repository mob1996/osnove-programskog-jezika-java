package hr.fer.zemris.java.webserver.workers;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * Class implements {@link IWebWorker}.
 * This worker creates 'png' image of filled circle.
 * @author Luka Mijić
 *
 */
public class CircleWorker implements IWebWorker {

	/**
	 * Method creates 'png' image of filled circle.
	 * Sets mime type of given context to 'image/png' and
	 * sends create image using {@link RequestContext#write(byte[])}.
	 * @param context is used for dispatching information
	 */
	@Override
	public void processRequest(RequestContext context) throws Exception {
		BufferedImage bim = new BufferedImage(200, 200, BufferedImage.TYPE_3BYTE_BGR);
		context.setMimeType("image/png");
		Graphics2D g2d = bim.createGraphics();
		g2d.fillOval(0, 0, bim.getWidth(), bim.getHeight());
		g2d.dispose();
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			ImageIO.write(bim, "png", bos);
			context.write(bos.toByteArray());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
