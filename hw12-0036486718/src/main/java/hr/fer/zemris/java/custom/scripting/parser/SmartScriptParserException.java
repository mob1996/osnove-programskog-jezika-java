package hr.fer.zemris.java.custom.scripting.parser;

/**
 * Class used to model exceptions that can occur while
 * working with parser . 
 * @author Luka Mijić
 *
 */
public class SmartScriptParserException extends RuntimeException {

	private static final long serialVersionUID = 636263890867226018L;


	/**
	 * Default constructor of ParserException
	 */
	public SmartScriptParserException() {
		super();
	}
	
	
	/**
	 * Constructor of ParserException that takes
	 * message argument
	 * @param message argument
	 */
	public SmartScriptParserException(String message) {
		super(message);
	}
}
