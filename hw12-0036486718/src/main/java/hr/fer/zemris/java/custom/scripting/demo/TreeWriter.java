package hr.fer.zemris.java.custom.scripting.demo;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.INodeVisitor;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParserException;

/**
 * Demonstration program that shows functionalities of {@link SmartScriptParser}
 * using {@link INodeVisitor} pattern for recreating original file.
 * @author Luka Mijić
 *
 */
public class TreeWriter {

	/**
	 * Implementation of {@link INodeVisitor}.
	 * It visits each node and writes its contents
	 * on standard output.
	 * @author Luka Mijić
	 *
	 */
	private static class WriterVisitor implements INodeVisitor {
		
		@Override
		public void visitTextNode(TextNode node) {
			System.out.print(node.toString());
		}

		@Override
		public void visitForLoopNode(ForLoopNode node) {
			System.out.print(node.toString());
			
			for(int i = 0, size = node.numberOfChildren(); i < size; i++) {
				node.getChild(i).accept(this);
			}
			
			System.out.println("{$END$}");
		}

		@Override
		public void visitEchoNode(EchoNode node) {
			System.out.print(node);
			
		}

		@Override
		public void visitDocumentNode(DocumentNode node) {
			for(int i = 0, size = node.numberOfChildren(); i < size; i++) {
				node.getChild(i).accept(this);
			}
			
		}
	
	}
	
	/**
	 * Starting point of demonstration program.
	 * It requires one argument, path to file that can
	 * be interpreted using {@link SmartScriptParser}.
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length != 1) {
			System.err.println("Invalid numbers of arguments");
			return;
		}
		String filepath = args[0];
		String docBody;

		SmartScriptParser parser = null;
		try {
			docBody = new String(Files.readAllBytes(Paths.get(filepath)), StandardCharsets.UTF_8);
			parser = new SmartScriptParser(docBody);
			parser.getDocumentNode().accept(new WriterVisitor());
		} catch (SmartScriptParserException e) {
			System.out.println("Unable to parse document!");
			System.exit(-1);
		}catch (IOException e) {
			System.out.println("Unable to open filepath");
			System.exit(-1);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("If this line ever executes, you have failed this class!");
			System.exit(-1);
		}
	}
}
