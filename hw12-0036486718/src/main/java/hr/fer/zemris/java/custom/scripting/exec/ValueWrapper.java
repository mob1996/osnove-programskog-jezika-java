package hr.fer.zemris.java.custom.scripting.exec;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;


/**
 * Class is used as a wrapper for objects.
 * @author Luka Mijić
 *
 */
public class ValueWrapper {
	
	/**
	 * String class needs to be parsed
	 */
	private static final Class<String> NEED_PARSING = String.class;
	
	/**
	 * Double class has bigger priority than Integer Class 
	 */
	private static final Class<Double> TOP_PRIORITY = Double.class;
	

	private static final BiFunction<Number, Number, Number> ADD = (n1, n2) -> n1.doubleValue() + n2.doubleValue();
	private static final BiFunction<Number, Number, Number> SUB = (n1, n2) -> n1.doubleValue() - n2.doubleValue();
	private static final BiFunction<Number, Number, Number> MUL = (n1, n2) -> n1.doubleValue() * n2.doubleValue();
	private static final BiFunction<Number, Number, Number> DIV = new BiFunction<>() {
		@Override
		public Number apply(Number n1, Number n2) {
			if(Math.abs(n2.doubleValue()) <= 10E-7) {
				throw new IllegalArgumentException("Division by zero not allowed.");
			}
			
			return n1.doubleValue() / n2.doubleValue();
		}
	};
	
	/**
	 * Type of classes that are valid for arithmetic 
	 * operations
	 */
	public static List<Class<?>> arithClasses;
	static {
		arithClasses = new ArrayList<>();
		
		arithClasses.add(String.class);
		arithClasses.add(Integer.class);
		arithClasses.add(Double.class);
	}
	
	/**
	 * Value that is wrapped
	 */
	private Object value;
	
	/**
	 * Creates new ValueWrapper
	 * @param value that is wrapped
	 */
	public ValueWrapper(Object value) {
		this.value = value;
	}
	
	/**
	 * @return value of wrapper
	 */
	public Object getValue() {
		return value;
	}
	
	/**
	 * Sets value property to value parameter
	 * @param value
	 */
	public void setValue(Object value) {
		this.value = value;
	}

	/**
	 * Method adds two objects if arithmetic 
	 * operetions are valid on them.
	 * @param incValue is addend
	 */
	public void add(Object incValue) {
		storeResult(this.value, incValue, ADD);
	}
	
	/**
	 * Method subtracts two objects if arithmetic 
	 * operetions are valid on them.
	 * @param decValue is subtrahend
	 */
	public void subtract(Object decValue) {
		storeResult(this.value, decValue, SUB);
	}
	
	/**
	 * Method multiplies adds two objects if arithmetic 
	 * operetions are valid on them.
	 * @param mulValue is multiplier
	 */
	public void multiply(Object mulValue) {
		storeResult(this.value, mulValue, MUL);
	}
	
	/**
	 * Method divides two objects if arithmetic 
	 * operetions are valid on them.
	 * @param divValue is divisor
	 */
	public void divide(Object divValue) {
		storeResult(this.value, divValue, DIV);
	}
	
	/**
	 * Method compares current value and withValue if 
	 * both of them can be interpreted as number.
	 * @param withValue
	 * @return less than 0 if value is lesser than withValue, 
	 * 			0 if their values are equal and greater than 0 otherwise
	 */
	public int numCompare(Object withValue) {
		Object value1 = this.value == null ? Integer.valueOf(0) : this.value;
		withValue = withValue == null ? Integer.valueOf(0) : withValue;
		testValidArithmeticClasses(value1, withValue);
		
		value1 = value1.getClass() == NEED_PARSING ? parse(String.class.cast(value1)) : value1;
		withValue = withValue.getClass() == NEED_PARSING ? parse(String.class.cast(withValue)) : withValue;
	
		double number1 = Number.class.cast(value1).doubleValue();
		double number2 = Number.class.cast(withValue).doubleValue();
		
		return Double.compare(number1, number2);
	}
	
	/**
	 * Method uses BiFunction function to make calculation
	 * based on value1 and value2 and stores it to value property.
	 * @param value1 
	 * @param value2
	 * @param function
	 */
	private void storeResult(Object value1, Object value2, BiFunction<Number, Number, Number> function) {
		value1 = value1 == null ? Integer.valueOf(0) : this.value;
		value2 = value2 == null ? Integer.valueOf(0) : value2;
		testValidArithmeticClasses(value1, value2);
		
		value1 = value1.getClass() == NEED_PARSING ? parse(String.class.cast(value1)) : value1;
		value2 = value2.getClass() == NEED_PARSING ? parse(String.class.cast(value2)) : value2;
	
		Number number1 = Number.class.cast(value1);
		Number number2 = Number.class.cast(value2);
		
		Number result = function.apply(number1, number2);
		
		if(isDoubleOutput(value1, value2)) {
			this.value = Double.valueOf(result.doubleValue());
		} else {
			this.value = Integer.valueOf(result.intValue());
		}	
	}
	
	/**
	 * Check if class of value is valid for arithmetic 
	 * operations.
	 * @param value that is checked
	 * @return true if it is valid and false if it is not valid.
	 */
	private boolean isValidArithmeticsClass(Object value) {
		return arithClasses.contains(value.getClass());
	}
	
	/**
	 * Check if number should be stored as double object.
	 * @param value1 
	 * @param value2
	 * @return if either of value1 or value2 is Double return true,
	 * 			otherwise return false;
	 */
	private boolean isDoubleOutput(Object value1, Object value2) {
		if(value1.getClass() == TOP_PRIORITY) return true;
		if(value2.getClass() == TOP_PRIORITY) return true;
		return false;
	}
	
	/**
	 * Check if value1 and value2 are valid types for arithmetic
	 * for doing arithmetic calculations with them.
	 * @param value1
	 * @param value2
	 * @throws RuntimeException if value1 or value2 are not valid types
	 */
	private void testValidArithmeticClasses(Object value1, Object value2) {
		if(!isValidArithmeticsClass(value1))
			throw new RuntimeException("Current Value is not valid arithmetic class.");
		if(!isValidArithmeticsClass(value2))
			throw new RuntimeException("Given parameter is not valid arithmetic class.");
	}
	
	/**
	 * Parses expression into Integer or Double types
	 * @param expression that is parsed
	 * @return Double or Integer object
	 */
	private Object parse(String expression) {
		expression = expression.toLowerCase();
		if(expression.contains(".") || expression.contains("e")) {
			return parseDouble(expression);
		}
		
		return parseInteger(expression);
	}
	
	/**
	 * Parses expression into Double object
	 * @param expression that is parsed
	 * @return Double Object from expression
	 * @throws IllegalArgumentException if expression is not parsable
	 * 					as Double
	 */
	private Double parseDouble(String expression) {
		try {
			return Double.parseDouble(expression);
		} catch (NumberFormatException exception){
			throw new IllegalArgumentException("'" + expression +"' can't be parsed as double number.");
		}
	}
	
	/**
	 * Parses expression into Integer object
	 * @param expression that is parsed
	 * @return Integer Object from expression
	 * @throws IllegalArgumentException if expression is not parsable
	 * 					as Integer
	 */
	private Integer parseInteger(String expression) {
		try {
			return Integer.parseInt(expression);
		} catch (NumberFormatException exception){
			throw new IllegalArgumentException("'" + expression +"' can't be parsed as int number.");
		}
	}
	
}
