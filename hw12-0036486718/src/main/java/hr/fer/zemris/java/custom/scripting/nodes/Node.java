package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.collections.ArrayIndexedCollection;

/**
 * Class represents one node in syntax tree.
 * @author Luka Mijić
 *
 */
public class Node {

	/**
	 * Array of children nodes
	 */
	private ArrayIndexedCollection children;
	
	/**
	 * Method is used to return number of children.
	 * @return number of children.
	 */
	public int numberOfChildren() {
		if(children == null) return 0;
		
		return children.size();
	}
	
	/**
	 * Method is used to add new child node to the array 
	 * of children.
	 * @param child node that is being added
	 */
	public void addChild(Node child) {
		if(children == null) {
			children = new ArrayIndexedCollection();
		}
		
		children.add(child);
	}
	
	/**
	 * Method is used to get a child at given index from
	 * children array.
	 * @param index of child method gets
	 * @return child at given index
	 * @throws IllegalArgumentException if index is out of bounds. 
	 */
	public Node getChild(int index) {
		if(index < 0 || index >= numberOfChildren()) 
			throw new IllegalArgumentException("Can't get child that is out of index bounds.");
		
		return (Node) children.get(index);
	}
	
	/**
	 * Method that accepts visit for {@link INodeVisitor}.
	 * @param visitor that is visiting {@link Node}.
	 */
	public void accept(INodeVisitor visitor) {
		
	}
}
