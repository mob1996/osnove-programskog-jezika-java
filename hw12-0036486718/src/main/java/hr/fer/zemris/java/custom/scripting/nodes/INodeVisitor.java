package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * Interface with method that are used for creating
 * Visitor pattern that visits various implementations 
 * of {@link Node} class.
 * @author Luka Mijić
 *
 */
public interface INodeVisitor {
	
	/**
	 * Called when visiting {@link TextNode}.
	 * @param node that is being visited.
	 */
	public void visitTextNode(TextNode node);

	/**
	 * Called when visiting {@link ForLoopNode}.
	 * @param node that is being visited.
	 */
	public void visitForLoopNode(ForLoopNode node);

	/**
	 * Called when visiting {@link EchoNode}.
	 * @param node that is being visited.
	 */
	public void visitEchoNode(EchoNode node);

	/**
	 * Called when visiting {@link DocumentNode}.
	 * @param node that is being visited.
	 */
	public void visitDocumentNode(DocumentNode node);
}
