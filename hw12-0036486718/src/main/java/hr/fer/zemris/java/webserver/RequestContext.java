package hr.fer.zemris.java.webserver;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

/**
 * This class models context of request. 
 * It generates header based on properties such as
 * <code>encoding</code>, <code>statusCode</code>, <code>statusText</code>,
 * , <code>mimeType</code> and {@link List} of {@link RCCookie}s.
 * After header is generated and sent through given {@link OutputStream},
 * given array of bytes and given {@link String} can be also sent through {@link OutputStream}.
 * @author Luka Mijić
 *
 */
public class RequestContext {

	/**
	 * Inner static class of {@link RequestContext}.
	 * {@link RCCookie} stores informations such as cookie name, 
	 * cookie value, domain, path and max age. It is used to
	 * user can receive personalised experience.
	 * @author Luka Mijić
	 *
	 */
	public static class RCCookie {
		
		/**
		 * Name of cookie
		 */
		private String name;
		
		/**
		 * Value of cookie
		 */
		private String value;
		
		/**
		 * Cookie domain
		 */
		private String domain;
		
		/**
		 * Path to cookie
		 */
		private String path;
		
		/**
		 * Longevity of cookie.
		 */
		private Integer maxAge;
		
		/**
		 * Creates {@link RCCookie} from given parameters.
		 * @param name of cookie
		 * @param value of cookie
		 * @param maxAge longevity of cookie
		 * @param domain of cookie
		 * @param path of cookie
		 */
		public RCCookie(String name, String value, Integer maxAge, String domain, String path) {
			super();
			this.name = name;
			this.value = value;
			this.domain = domain;
			this.path = path;
			this.maxAge = maxAge;
		}

		/**
		 * @return cookie name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @return cookie value
		 */
		public String getValue() {
			return value;
		}

		/**
		 * @return cookie domain
		 */
		public String getDomain() {
			return domain;
		}

		
		/**
		 * @return cookie path
		 */
		public String getPath() {
			return path;
		}

		/**
		 * @return cookie longevity
		 */
		public Integer getMaxAge() {
			return maxAge;
		}
	}
	
	/**
	 * Used to separate lines in html header.
	 */
	private static final String DELIMITER = "\r\n";

	/**
	 * Header and given content is sent through this stream
	 */
	private OutputStream outputStream;
	
	/**
	 * Charset used for encoding given contents.
	 */
	private Charset charset;

	/**
	 * Encoding used for creating {@link Charset}.
	 * Default encoding is 'UTF-8'
	 */
	private String encoding = "UTF-8";
	
	/**
	 * Status code of request. Default value is 200.
	 */
	private int statusCode = 200;
	
	/**
	 * Status text of request. Default value is "OK"
	 */
	private String statusText = "OK";
	
	/**
	 * Mime type of request. Default value is "text/html"
	 */
	private String mimeType = "text/html";
	
	/**
	 * Length of content that is sent. Default value is null.
	 */
	private Long contentLen = null;

	/**
	 * Parameters of context.
	 */
	private Map<String, String> parameters;
	
	/**
	 * Temporary parameters of context.
	 */
	private Map<String, String> temporaryParameters;
	
	/**
	 * Persistent parameters of context.
	 */
	private Map<String, String> persistentParameters;
	
	/**
	 * List of {@link RCCookie} used for generating header
	 */
	private List<RCCookie> outputCookies;
	
	/**
	 * Dispatcher
	 */
	private IDispatcher dispatcher; 

	/**
	 * Flag that signals if header was generated.
	 */
	private boolean headerGenerated = false;

	/**
	 * Creates {@link RequestContext} from given parameters.
	 * Given {@link OutputStream} must not be <code>null</code> reference.
	 * If other parameters are null, they are treated as empty implementations.
	 * @param outputStream header and other contents are sent using this {@link OutputStream}
	 * @param parameters {@link Map} of parameters.
	 * @param persistentParameters {@link Map} of persistent parameters
	 * @param outputCookies {@link List} of outputCookies
	 */
	public RequestContext(OutputStream outputStream, Map<String, String> parameters,
			Map<String, String> persistentParameters, List<RCCookie> outputCookies) {
		this(outputStream, parameters, persistentParameters, outputCookies, null, null);
	}
	
	/**
	 * Creates {@link RequestContext} from given parameters.
	 * Given {@link OutputStream} must not be <code>null</code> reference.
	 * If other parameters are null, they are treated as empty implementations (except {@link IDispatcher} parameter).
	 * @param outputStream header and other contents are sent using this {@link OutputStream}
	 * @param parameters {@link Map} of parameters.
	 * @param persistentParameters {@link Map} of persistent parameters
	 * @param outputCookies {@link List} of outputCookies
	 * @param temporaryParameters {@link Map} of temporary parameters
	 * @param dispatcher instance of {@link IDispatcher}
	 */
	public RequestContext(OutputStream outputStream, Map<String, String> parameters,
			Map<String, String> persistentParameters, List<RCCookie> outputCookies, 
			Map<String, String> temporaryParameters, IDispatcher dispatcher) {
		this.outputStream = Objects.requireNonNull(outputStream, "'outputStream' argument must not be null reference.");
		this.parameters = parameters == null ? new HashMap<>() : parameters;
		this.persistentParameters = persistentParameters == null ? new HashMap<>() : persistentParameters;
		this.outputCookies = outputCookies == null ? new ArrayList<>() : outputCookies;
		this.temporaryParameters = temporaryParameters == null ? new HashMap<>() : temporaryParameters;
		this.dispatcher = dispatcher;
		
		charset = Charset.forName(encoding);
		
	}

	/**
	 * Set encoding to given encoding.
	 * @param encoding that is being set
	 * @throws RuntimeException if header was already generated
	 */
	public void setEncoding(String encoding) {
		if(headerGenerated) {
			throw new RuntimeException("Header was already generated.");
		}
		this.encoding = encoding;
		this.charset = Charset.forName(this.encoding);
	}

	/**
	 * Set status code to given status code.
	 * @param statusCode that is being set
	 * @throws RuntimeException if header was already generated
	 */
	public void setStatusCode(int statusCode) {
		if(headerGenerated) {
			throw new RuntimeException("Header was already generated.");
		}
		this.statusCode = statusCode;
	}

	/**
	 * Set status text to given status text.
	 * @param statusText that is being set
	 * @throws RuntimeException if header was already generated
	 */
	public void setStatusText(String statusText) {
		if(headerGenerated) {
			throw new RuntimeException("Header was already generated.");
		}
		this.statusText = statusText;
	}

	/**
	 * Set mime type to given mime type.
	 * @param statusType that is being set
	 * @throws RuntimeException if header was already generated
	 */
	public void setMimeType(String mimeType) {
		if(headerGenerated) {
			throw new RuntimeException("Header was already generated.");
		}
		this.mimeType = mimeType;
	}
	
	/**
	 * Set content length.
	 * @param contentLength that is being set
	 * @throws RuntimeException if header was already generated
	 */
	public void setContentLength(Long contentLength) {
		if(headerGenerated) {
			throw new RuntimeException("Header was already generated.");
		}
		this.contentLen = contentLength;
	}
	
	/**
	 * Add given {@link RCCookie} to {@link List}
	 * @param cookie that is added
	 * @throws RuntimeException if header was already generated
	 * @throws NullPointerException if given cookie is null reference
	 */
	public void addRCCookie(RCCookie cookie) {
		if(headerGenerated) {
			throw new RuntimeException("Header was already generated.");
		}
		outputCookies.add(Objects.requireNonNull(cookie, "Given RCCookie must not be null reference."));
	}

	/**
	 * @param name of parameter
	 * @return parameter value associated with given name. 
	 * 			If there is no such value return null.
	 */
	public String getParameter(String name) {
		return parameters == null ? null : parameters.get(name);
	}

	/**
	 * @return unmodifiable {@link Set} of parameter names.
	 */
	public Set<String> getParameterNames() {
		return Collections.unmodifiableSet(parameters == null ? new TreeSet<String>() : parameters.keySet());
	}

	/**
	 * @param name of persistent parameter
	 * @return persistent parameter value associated with given name.
	 * 			If there is no such value return null.
	 */
	public String getPersistentParameter(String name) {
		return persistentParameters == null ? null : persistentParameters.get(name);
	}

	/**
	 * @return unmodifiable {@link Set} of persistent parameter names.
	 */
	public Set<String> getPersistentParameterNames() {
		return Collections.unmodifiableSet(parameters == null ? new TreeSet<String>() : persistentParameters.keySet());
	}

	/**
	 * Adds given value associated with given name to {@link Map} of that 
	 * stores persistent parameters. If value with given name already exist,
	 * that value is overwritten.	
	 * @param name of persistent parameters
	 * @param value of persistent parameters
	 */
	public void setPersistentParameter(String name, String value) {
		persistentParameters.put(name, value);
	}
	
	/**
	 * Removes value associated with given name from {@link Map} that
	 * stores persistent parameters. If given name doesn't exist in {@link Map}
	 * nothing happens.
	 * @param name that is removed from map.
	 */
	public void removePersistentParameter(String name) {
		persistentParameters.remove(name);
	}
	
	/**
	 * @param name of temporary parameter
	 * @return value of temporary parameter associated with given name.
	 * 			If there is no such parameter, return null.
	 */
	public String getTemporaryParameter(String name) {
		return temporaryParameters.get(name);
	}
	
	/**
	 * @return unmodifiable {@link Set} of temporary parameter names.
	 */
	public Set<String> getTemporaryParameterNames(){
		return Collections.unmodifiableSet(temporaryParameters.keySet());
	}
	
	/**
	 * Adds given value associated with given name to {@link Map} that 
	 * stores temporary parameters. If value with given name already exist,
	 * that value is overwritten.	
	 * @param name of persistent parameters
	 * @param value of persistent parameters
	 */
	public void setTemporaryParameter(String name, String value) {
		temporaryParameters.put(name, value);
	}

	/**
	 * Removes value associated with given name from {@link Map} that
	 * stores temporary parameters. If given name doesn't exist in {@link Map}
	 * nothing happens.
	 * @param name that is removed from map.
	 */
	public void removeTemporaryParameter(String name) {
		temporaryParameters.remove(name);
	}
	
	/**
	 * @return {@link IDispatcher} property
	 */
	public IDispatcher getDispatcher() {
		return dispatcher;
	}

	/**
	 * Writes array of bytes to {@link OutputStream} property
	 * of {@link RequestContext}. If header wasn't generated yet,
	 * generate header first and write it on stream.
	 * @param data that is being written.
	 * @return this {@link RequestContext}
	 * @throws IOException if error while writing occurred.
	 */
	public RequestContext write(byte[] data) throws IOException {
		if(!headerGenerated) {
			outputStream.write(generateHeader().getBytes(StandardCharsets.ISO_8859_1));
		}
		
		outputStream.write(data);
		outputStream.flush();
		
		return this;
	}
	
	/**
	 * Writes array of bytes created from given
	 * {@link String} while using {@link RequestContext} 
	 * property <code>encoding</code> to {@link OutputStream} property
	 * of {@link RequestContext}. If header wasn't generated yet,
	 * generate header first and write it on stream.
	 * @param data that is being written.
	 * @return this {@link RequestContext}
	 * @throws IOException if error while writing occurred.
	 */
	public RequestContext write(String text) throws IOException {
		if(!headerGenerated) {
			outputStream.write(generateHeader().getBytes(StandardCharsets.ISO_8859_1));
		}
		outputStream.write(text.getBytes(charset));
		outputStream.flush();
		
		return this;
	}
	
	/**
	 * Writes <code>len</code> number of bytes starting
	 * from <code>offset</code> to {@link OutputStream} property
	 * of {@link RequestContext}. If header wasn't generated yet,
	 * generate header first and write it on stream. 
	 * @param data that is being written
	 * @param offset starting point of writing
	 * @param len number of bytes sent
	 * @return this {@link RequestContext}
	 * @throws IOException if error while writing occurred
	 */
	public RequestContext write(byte[] data, int offset, int len) throws IOException {
		if(!headerGenerated) {
			outputStream.write(generateHeader().getBytes(StandardCharsets.ISO_8859_1));
		}		

		outputStream.write(data, offset, len);
		outputStream.flush();
		return this;
	}
	
	/**
	 * Generates header of form:
	 * "HTTP/1.1 statusCode statusMessage\r\n
	 *  Content-Type:mimeType(;charset=encoding [mimeType starts with "text/")\r\n"
	 *  This is followed by:
	 *  Set-Cookie: name=value; Domain=domain; Path=path; Max-Age=maxAge
	 *  for each {@link RCCookie} stored in list.
	 *  If any of values are null than that information is omitted.
	 * @return generated header
	 */
	private String generateHeader() {
		StringBuilder headerBuilder = new StringBuilder();
		
		String contentType = "Content-Type: " + mimeType.trim();
		if(mimeType.trim().toLowerCase().startsWith("text/")) {
			contentType += "; charset=" + encoding;
		}
		
		headerBuilder.append("HTTP/1.1 " + statusCode + " " + statusText + DELIMITER)
					 .append(contentType + DELIMITER);
		
		if(contentLen != null) {
			headerBuilder.append("Content-Lenght: " + String.valueOf(this.contentLen) + DELIMITER);
		}
		
		for(RCCookie cookie : outputCookies) {
			headerBuilder.append("Set-Cookie: " + cookie.getName() + "=" + cookie.value);
			
			if(cookie.getDomain() != null) {
				headerBuilder.append(String.format("; Domain=%s", cookie.getDomain()));
			}
			
			if(cookie.getPath() != null) {
				headerBuilder.append(String.format("; Path=%s", cookie.getPath()));
			}
			
			if(cookie.getMaxAge() != null) {
				headerBuilder.append(String.format("; Max-Age=%d", cookie.getMaxAge().intValue()));
			}
			
			headerBuilder.append(DELIMITER);
		}
		
		if(outputCookies.size() == 0) {
			headerBuilder.append(DELIMITER);
		}
		headerGenerated = true;
		charset = Charset.forName(encoding);
		
		return headerBuilder.toString();
	}
	
}
