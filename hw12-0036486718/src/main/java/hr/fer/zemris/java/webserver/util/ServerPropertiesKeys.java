package hr.fer.zemris.java.webserver.util;

/**
 * Class stores static references to keys used for retrieving
 * information in server configuration.
 * @author Luka Mijić
 *
 */
public class ServerPropertiesKeys {
	
	/**
	 * Keys used
	 */
	public static final String ADRESS = "server.adress";
	public static final String DOMAIN_NAME = "server.domainName";
	public static final String PORT = "server.port";
	public static final String NUM_OF_WORKERS = "server.workersThreads";
	public static final String DOCUMENT_ROOT = "server.documentRoot";
	public static final String MIME_CONFIG_FILE = "server.mimeConfig";
	public static final String TIMEOUT = "session.timeout";
	public static final String WORKER_CONFIG_FILE = "server.worker";

}
