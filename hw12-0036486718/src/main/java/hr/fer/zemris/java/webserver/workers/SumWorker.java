package hr.fer.zemris.java.webserver.workers;

import java.util.Map;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * Class implements {@link IWebWorker}.
 * It takes 2 numbers from context and executes 
 * script for adding those two numbers.
 * @author Luka Mijić
 *
 */
public class SumWorker implements IWebWorker {

	/**
	 * It takes 2 values assigned to "a" and "b" from
	 * given context in parameters {@link Map}. Stores those two
	 * values to temporary parameters {@link Map} of given context.
	 * Also it sets "zbroJ":a+b on temporary parameters {@link Map}.
	 * Then dispatches request and calls for "/private/calc.smscr"
	 * script.
	 * @param context is used for dispatching information
	 */
	@Override
	public void processRequest(RequestContext context) throws Exception {
		String a = parse(context.getParameter("a"), 1);
		String b = parse(context.getParameter("b"), 2);
		
		context.setTemporaryParameter("a", a);
		context.setTemporaryParameter("b", b);
		context.setTemporaryParameter("zbroj", String.valueOf((Double.valueOf(a) + Double.valueOf(b))));
		
		context.getDispatcher().dispatchRequest("/private/calc.smscr");

	}
	
	/**
	 * @param value that is parsed
	 * @param defaultValue if parsing of {@link String} fails
	 * @return value if it can be parsed as {@link Double} otherwise
	 * 				return {@link String} representation of given defaultValue.
	 */
	private String parse(String value, double defaultValue) {		
		try {
			return String.valueOf(Double.valueOf(value));
		} catch(NumberFormatException | NullPointerException exc) {
			return String.valueOf(defaultValue);
		}
	}

}
