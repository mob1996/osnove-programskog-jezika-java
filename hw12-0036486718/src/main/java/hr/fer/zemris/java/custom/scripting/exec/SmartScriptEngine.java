package hr.fer.zemris.java.custom.scripting.exec;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantDouble;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantInteger;
import hr.fer.zemris.java.custom.scripting.elems.ElementString;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;
import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.INodeVisitor;
import hr.fer.zemris.java.custom.scripting.nodes.Node;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * This class executes programs written using Smart Script
 * scripting language. 
 * @author Luka Mijić
 *
 */
public class SmartScriptEngine {
	
	/**
	 * Starting point of written program
	 */
	private DocumentNode docNode;
	
	/**
	 * Request context of current script execution.
	 */
	private RequestContext reqContext;
	
	/**
	 * Multistack
	 */
	private ObjectMultistack multiStack = new ObjectMultistack();
	
	/**
	 * Defines scripting operations.
	 * @author Luka Mijić
	 */
	private static class OperatorConsumer implements Consumer<Stack<Object>> {
		/**
		 * Operator used for calculating result
		 */
		private BiConsumer<ValueWrapper, Object> operator;
		
		/**
		 * Creates {@link OperatorConsumer} from given parameters.
		 * @param operator used for calculation
		 */
		public OperatorConsumer(BiConsumer<ValueWrapper, Object> operator) {
			super();
			this.operator = operator;
		}

		/**
		 * Takes two operands from given stack and call
		 * {@link BiConsumer#accept(Object, Object)} on them.
		 * Second operand is first {@link Object} popped from {@link Stack}.
		 * First operand is second {@link Object} popped from {@link Stack};
		 * @param s used {@link Stack}
		 */
		@Override
		public void accept(Stack<Object> s) {
			Object second = s.pop();
			ValueWrapper first = new ValueWrapper(s.pop());
			operator.accept(first, second);
			s.push(first.getValue());
		}	
	}
	
	/**
	 * Implements {@link Consumer}.
	 * Consumer that pushes default value or value received from
	 * {@link Function} getter using key taken from top of {@link Stack}.
	 * @author Luka Mijić
	 */
	private static class GetConsumer implements Consumer<Stack<Object>>{

		/**
		 * Takes key and returns value
		 */
		private Function<String, Object> getter;
		
		/**
		 * Creates {@link GetConsumer}
		 * @param getter used for getting value
		 */
		public GetConsumer(Function<String, Object> getter) {
			super();
			this.getter = getter;
		}
		
		/**
		 * Pops key from top of {@link Stack}.
		 * Uses given key to retrieve value from {@link Function} getter.
		 * @param s used {@link Stack}
		 */
		@Override
		public void accept(Stack<Object> s) {
			Object defaultValue = s.pop();
			Object desiredValue = getter.apply((String) s.pop());
			if(desiredValue == null) {
				s.push(defaultValue);
			} else {
				s.push(desiredValue);
			}	
		}
	}
	
	/**
	 * Implements {@link Consumer}.
	 * Pops two values from stack, first one is key, second one
	 * is value. Using {@link BiConsumer} setter, given value is stored.
	 * @author Luka Mijić
	 */
	private static class SetConsumer implements Consumer<Stack<Object>> {
		/**
		 * Used for setting values
		 */
		private BiConsumer<String, String> setter;

		/**
		 * Creates {@link SetConsumer}
		 * @param setter
		 */
		public SetConsumer(BiConsumer<String, String> setter) {
			super();
			this.setter = setter;
		}

		/**
		 * First value popped is used for value key,
		 * second popped value is associated value.
		 * Using {@link BiConsumer} property value is set.
		 * @param s used {@link Stack}
		 */
		@Override
		public void accept(Stack<Object> s) {
			Object name = s.pop();
			Object value = s.pop();
			setter.accept((String) name, String.valueOf(value));
		}
	}
	
	/**
	 * Implements {@link Consumer}.
	 * Used for removing popped {@link String} using
	 * remover property.
	 * @author Luka Mijić
	 *
	 */
	private static class RemoveConsumer implements Consumer<Stack<Object>> {
		/**
		 * Used for removing value associated with given value
		 */
		private Consumer<String> remover;

		/**
		 * Creates {@link RemoveConsumer}
		 * @param remover
		 */
		public RemoveConsumer(Consumer<String> remover) {
			super();
			this.remover = remover;
		}

		/**
		 * Using popped {@link String} key to
		 * remove associated value.
		 * @param s used {@link Stack}
		 */
		@Override
		public void accept(Stack<Object> s) {
			remover.accept((String) s.pop()); 
		}
		
		
	}
	
	/**
	 * Map of available functions.
	 */
	public static Map<String, Consumer<Stack<Object>>> availableFunctions;
	
	/**
	 * Adding function, takes two values from value and pushes result.
	 */
	private final Consumer<Stack<Object>> ADD = new OperatorConsumer((f, s) -> f.add(s));
	
	/**
	 * Subtraction function, takes two values from value and pushes result.
	 */
	private final Consumer<Stack<Object>> SUB = new OperatorConsumer((f, s) -> f.subtract(s));
	
	/**
	 * Multiplying function, takes two values from value and pushes result.
	 */
	private final Consumer<Stack<Object>> MUL = new OperatorConsumer((f, s) -> f.multiply(s));
	
	/**
	 * Dividing function, takes two values from value and pushes result.
	 */
	private final Consumer<Stack<Object>> DIV = new OperatorConsumer((f, s) -> f.divide(s));
	
	/**
	 * Sinus function, takes one value and pushes sinus of that value.
	 */
	private final Consumer<Stack<Object>> SIN = new Consumer<>() {
		@Override
		public void accept(Stack<Object> s) {
			Double arg = Double.parseDouble(s.pop().toString());
			s.push(Math.sin(Math.toRadians(arg)));
		}
	};
	
	/**
	 * First popped value is used as {@link DecimalFormat} pattern,
	 * second popped value is {@link Double} that is formatted.
	 * Result of formating is pushed on stack.
	 */
	private static final Consumer<Stack<Object>> DECFMT = new Consumer<>() {
		@Override
		public void accept(Stack<Object> s) {
			String form = s.pop().toString();
			DecimalFormat decFormat = new DecimalFormat(form);
			Double x = Double.parseDouble(s.pop().toString());
			s.push(decFormat.format(x));
		}
	};
	
	/**
	 * Popped value is given to {@link RequestContext#setMimeType(String)}.
	 */
	private final Consumer<Stack<Object>> MIME_TYPE = s -> reqContext.setMimeType(s.pop().toString());
	
	/**
	 * Peeks value on top of stack and pushes it, making duplicate at top.
	 */
	private final Consumer<Stack<Object>> DUPLICATE = new Consumer<>() {
		@Override
		public void accept(Stack<Object> s) {
			Object x = s.peek();
			s.push(x);
		}
	};
	
	/**
	 * Swap first and second value at top of the {@link Stack}.
	 */
	private final Consumer<Stack<Object>> SWAP = new Consumer<>() {
		@Override
		public void accept(Stack<Object> s) {
			Object first = s.pop();
			Object second = s.pop();
			
			s.push(second);
			s.push(first);
		}
	};
	
	/**
	 * Get parameter using {@link RequestContext#getParameter(String)}
	 */
	private final Consumer<Stack<Object>> PARAM_GET = new GetConsumer((name) -> reqContext.getParameter(name));
	
	/**
	 * Get parameter using {@link RequestContext#getPersistentParameter(String)}
	 */
	private final Consumer<Stack<Object>> PPARAM_GET = new GetConsumer((name) -> reqContext.getPersistentParameter(name));
	
	/**
	 * Get parameter using {@link RequestContext#getTemporaryParameter(String)}
	 */
	private final Consumer<Stack<Object>> TPARAM_GET = new GetConsumer((name) -> reqContext.getTemporaryParameter(name));
	
	/**
	 * Set parameter using {@link RequestContext#setPersistentParameter(String, String)}
	 */
	private final Consumer<Stack<Object>> PPARAM_SET = new SetConsumer((name, value) -> reqContext.setPersistentParameter(name, value));
	
	/**
	 * Set parameter using {@link RequestContext#setTemporaryParameter(String, String)}
	 */
	private final Consumer<Stack<Object>> TPARAM_SET = new SetConsumer((name, value) -> reqContext.setTemporaryParameter(name, value));
	
	/**
	 * Removes parameter using {@link RequestContext#removePersistentParameter(String)}
	 */
	private final Consumer<Stack<Object>> PPARAM_DEL = new RemoveConsumer((name) -> reqContext.removePersistentParameter(name));
	
	/**
	 * Removes parameter using {@link RequestContext#removeTemporaryParameter(String)}
	 */
	private final Consumer<Stack<Object>> TPARAM_DEL = new RemoveConsumer((name) -> reqContext.removeTemporaryParameter(name));

	/**
	 * Classes that are interpreted as constants.
	 */
	private static ArrayList<Class<? extends Element>> constant_elements;
	
	/**
	 * {@link Map} that stores getter for different values stored in {@link Element} that
	 * can be interpreted as constants.
	 */
	private static Map<Class<? extends Element>, Function<Element, Object>> const_value_getters;
	static {
		constant_elements = new ArrayList<>();
		constant_elements.add(ElementConstantDouble.class);
		constant_elements.add(ElementConstantInteger.class);
		constant_elements.add(ElementString.class);
		
		const_value_getters = new HashMap<>();
		const_value_getters.put(ElementConstantDouble.class, new Function<Element, Object>() {

			@Override
			public Object apply(Element e) {
				return ((ElementConstantDouble)e).getValue();
			}
		});
		
		const_value_getters.put(ElementConstantInteger.class, new Function<Element, Object>() {

			@Override
			public Object apply(Element e) {
				return ((ElementConstantInteger)e).getValue();
			}
		});
		
		const_value_getters.put(ElementString.class, new Function<Element, Object>() {

			@Override
			public Object apply(Element e) {
				String str = ((ElementString)e).getValue();
				str = removeQuotation(str);
				return str;
			}
		});
	}

	/**
	 * Implementation of {@link INodeVisitor}.
	 */
	private INodeVisitor visitor = new INodeVisitor() {

		/**
		 * Called when visiting {@link TextNode}.
		 * It calls {@link RequestContext#write(String)} 
		 * on value stored in node that is being visited.
		 */
		@Override
		public void visitTextNode(TextNode node) {
			try {
				reqContext.write(node.toString());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		/**
		 * Called when visiting {@link ForLoopNode}.
		 * Visits children of {@link ForLoopNode} multiple times,
		 * depending on {@link ForLoopNode#getStartExpression()}, 
		 * {@link ForLoopNode#getEndExpression()} and {@link ForLoopNode#getStepExpression()}.
		 */
		@Override
		public void visitForLoopNode(ForLoopNode node) {
			ValueWrapper startValue = elemToWrapper(node.getStartExpression());
			ValueWrapper endValue = elemToWrapper(node.getEndExpression());
			ValueWrapper stepValue;
			if(node.getStepExpression() == null) {
				stepValue = new ValueWrapper(1);
			} else {
				stepValue = elemToWrapper(node.getStepExpression());
			}

			String variableName = node.getVariable().asText();
			multiStack.push(node.getVariable().asText(), startValue);
			while(startValue.numCompare(endValue.getValue()) <= 0) {
				visitChildren(node);
				
				multiStack.pop(variableName).add(stepValue.getValue());
				multiStack.push(variableName, startValue);
			}

		}

		/**
		 * Called on visiting {@link EchoNode}.
		 * {@link EchoNode} stores list of {@link Element}s.
		 * Iterate through all of those {@link Element}.
		 * If {@link Element} can be interpreted as constant,
		 * push it on temporary {@link Stack}.
		 * If {@link Element} can be interpreted as variable,
		 * value associated with variable is pushed on temporary {@link Stack}.
		 * Otherwise function is called.
		 */
		@Override
		public void visitEchoNode(EchoNode node) {
			Stack<Object> tempStack = new Stack<>();
	
			for(Element e : node.getElements()) {
				if(constant_elements.contains(e.getClass())) {
					tempStack.push(const_value_getters.get(e.getClass()).apply(e));
				} else if(e.getClass() == ElementVariable.class) {
					tempStack.push(multiStack.peek(e.asText()).getValue());
				} else {
					availableFunctions.get(e.asText()).accept(tempStack);
				}
			}
			
			tempStack.forEach(o -> {
				try {
					reqContext.write(o.toString());
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			});
		}

		/**
		 * Calls <code>visitChildren()</code> on given node.
		 */
		@Override
		public void visitDocumentNode(DocumentNode node) {
			visitChildren(node);
		}

		/**
		 * Transform {@link Element} into {@link ValueWrapper}.
		 * @param elem that is transformed
		 * @return created {@link ValueWrapper}
		 */
		private ValueWrapper elemToWrapper(Element elem) {
			if (constant_elements.contains(elem.getClass())) {
				return new ValueWrapper(removeQuotation(elem.asText()));
			} else {
				return multiStack.peek(elem.asText()); //variable
			}
		}
		
		/**
		 * Visiting all children of given node.
		 * @param node whose children are visited
		 */
		private void visitChildren(Node node) {
			for (int i = 0, size = node.numberOfChildren(); i < size; i++) {
				node.getChild(i).accept(visitor);
			}
		}
	};

	/**
	 * Creates {@link SmartScriptEngine} from given parameter.
	 * @param docNode starting visiting point.
	 * @param reqContext stores extra information
	 */
	public SmartScriptEngine(DocumentNode docNode, RequestContext reqContext) {
		this.docNode = docNode;
		this.reqContext = reqContext;
		
		initFunctions();
	}
	
	/**
	 * Adds all functions to {@link Map} of available functions.
	 */
	private void initFunctions() {
		availableFunctions = new HashMap<>();
		availableFunctions.put("+", ADD);
		availableFunctions.put("-", SUB);
		availableFunctions.put("*", MUL);
		availableFunctions.put("/", DIV);
		availableFunctions.put("@sin", SIN);
		availableFunctions.put("@decfmt", DECFMT);
		availableFunctions.put("@setMimeType", MIME_TYPE);
		availableFunctions.put("@dup", DUPLICATE);
		availableFunctions.put("@swap", SWAP);
		availableFunctions.put("@paramGet", PARAM_GET);
		availableFunctions.put("@pparamGet", PPARAM_GET);
		availableFunctions.put("@tparamGet", TPARAM_GET);
		availableFunctions.put("@pparamSet", PPARAM_SET);
		availableFunctions.put("@tparamSet", TPARAM_SET);
		availableFunctions.put("@pparamDel", PPARAM_DEL);
		availableFunctions.put("@tparamDel", TPARAM_DEL);
		
	}
	
	/**
	 * Removes quotations from {@link String}.
	 * @param value {@link String} from which quotations are removed.
	 * @return {@link String} with removed quotations.
	 */
	private static String removeQuotation(String value) {
		if(value.startsWith("\"") && value.endsWith("\"") && value.length() > 1) {
			value = value.substring(1, value.length() - 1);
		};
		return value;
	}

	/**
	 * Executes {@link SmartScriptEngine}.
	 * Calls accept on {@link DocumentNode} property.
	 */
	public void execute() {
		docNode.accept(visitor);
	}

	

}
