package hr.fer.zemris.java.webserver;

import java.io.IOException;

/**
 * Starting point of server program.
 * @author Luka Mijić
 *
 */
public class SmartHttpServerDemo {

	/**
	 * Creates and starts server
	 * @param args takes zero arguments.
	 */
	public static void main(String[] args) {
		SmartHttpServer server;
		try {
			server = new SmartHttpServer("server.properties");
			server.start();
		} catch (IOException e) {
			System.out.println("Error reading property file.");
		}
		
	}
}
