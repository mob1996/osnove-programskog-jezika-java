package hr.fer.zemris.java.webserver;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PushbackInputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import hr.fer.zemris.java.custom.scripting.exec.SmartScriptEngine;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.webserver.RequestContext.RCCookie;
import hr.fer.zemris.java.webserver.util.ServerUtil;

import static hr.fer.zemris.java.webserver.util.ServerPropertiesKeys.*;

/**
 * Class models server.
 * Server provides multiple services.
 * People can access files in document root (/private is not accessible).
 * People can call for server to execute scripts and receive 
 * results from executing them.
 * @author Luka Mijić
 *
 */
public class SmartHttpServer {

	/**
	 * Defines entry in {@link Map} that stores sessions.
	 * @author Luka Mijić
	 *
	 */
	private static class SessionMapEntry {

		/**
		 * Creates instance of {@link SessionMapEntry}.
		 * @param sid session id
		 * @param host information
		 * @param validUntil time when validity of entry expires
		 * @param map that stores information
		 */
		public SessionMapEntry(String sid, String host, long validUntil, Map<String, String> map) {
			super();
			this.sid = sid;
			this.host = host;
			this.validUntil = validUntil;
			this.map = map;
		}

		/**
		 * Session id
		 */
		String sid;
		
		/**
		 * Host name
		 */
		String host;
		
		/**
		 * Time until validity expires
		 */
		long validUntil;
		
		/**
		 * Map that stores persistent parameters
		 */
		Map<String, String> map;
	}

	/**
	 * Path to configuration directory
	 */
	private static final Path CONFIG_DIR = Paths.get("config");
	
	/**
	 * Default mime type
	 */
	private static final String DEFAULT_MIME_TYPE = "application/octet-stream";
	
	/**
	 * Extension of scripts
	 */
	private static final String SCRIPT_EXTENSION = ".smscr";
	
	/**
	 * Supported HTML versions
	 */
	private static List<String> supported_version;
	static {
		supported_version = new LinkedList<>();
		supported_version.add("HTTP/1.0");
		supported_version.add("HTTP/1.1");
	}
	
	/**
	 * Server address
	 */
	private String address;
	
	/**
	 * Server domain name
	 */
	private String domainName;
	
	/**
	 * Server port
	 */
	private int port;
	
	/**
	 * Number of serving threads
	 */
	private int workerThreads;
	
	/**
	 * How long is session valid (in seconds)
	 */
	private int sessionTimeout;
	
	/**
	 * {@link Map} that links extensions and mime types
	 */
	private Map<String, String> mimeTypes = new HashMap<String, String>();
	
	/**
	 * Thread that accepts and servers requests
	 */
	private ServerThread serverThread;
	
	/**
	 * Thread that periodically goes through all session
	 * and deletes all expired ones.
	 */
	private CleanSessionThread clearThread;
	
	/**
	 * Handles multi-threading serving.
	 */
	private ExecutorService threadPool;
	
	/**
	 * Path to document root
	 */
	private Path documentRoot;
	
	/**
	 * {@link Map} of {@link IWebWorker} that are
	 * named in workers config file.
	 */
	private Map<String, IWebWorker> workersMap;
	
	/**
	 * {@link Map} that stores session information.
	 */
	private Map<String, SessionMapEntry> sessions;
	
	/**
	 * Used for generating random session ID
	 */
	private Random sessionRandom;
	
	/**
	 * Creates and sets up server by reading configuration
	 * file whose location is given as parameter.
	 * @param fileName location of config file
	 * @throws IOException can be throw when reading config files
	 */
	public SmartHttpServer(String fileName) throws IOException {
		Properties properties = new Properties();
		properties.load(Files.newBufferedReader(CONFIG_DIR.resolve(fileName)));
		this.address = properties.getProperty(ADRESS);
		this.domainName = properties.getProperty(DOMAIN_NAME);
		this.port = Integer.valueOf(properties.getProperty(PORT));
		this.workerThreads = Integer.valueOf(properties.getProperty(NUM_OF_WORKERS, "10"));

		this.mimeTypes = new HashMap<>();
		Properties mimeProperties = new Properties();
		mimeProperties.load(Files.newBufferedReader(Paths.get(properties.getProperty(MIME_CONFIG_FILE))));
		mimeProperties.stringPropertyNames().forEach(name -> mimeTypes.put(name, mimeProperties.getProperty(name)));

		this.documentRoot = Paths.get(properties.getProperty(DOCUMENT_ROOT));
		this.serverThread = new ServerThread();
		this.clearThread = new CleanSessionThread(300 * 1000); //5min

		this.workersMap = new HashMap<>();
		Properties workerProperties = new Properties();
		workerProperties.load(Files.newBufferedReader(Paths.get(properties.getProperty(WORKER_CONFIG_FILE))));
		initWorkers(workerProperties);

		this.sessionTimeout = Integer.valueOf(properties.getProperty(TIMEOUT));
		this.sessions = new HashMap<>();
		this.sessionRandom = new Random();
	}

	/**
	 * Fills <code>workersMap</code> by creating instance
	 * of workers using information from given argument.
	 * @param workerProperties {@link Properties} that provides
	 * 				information for creating {@link IWebWorker}s.
	 */
	private void initWorkers(Properties workerProperties) {
		for (String key : workerProperties.stringPropertyNames()) {
			try {
				String path = key;
				String fqcn = workerProperties.getProperty(path);

				Class<?> referenceToClass = this.getClass().getClassLoader().loadClass(fqcn);
				@SuppressWarnings("deprecation")
				Object newObject = referenceToClass.newInstance();

				IWebWorker iww = (IWebWorker) newObject;
				workersMap.put(path, iww);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (InstantiationException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Starts server.
	 */
	protected synchronized void start() {
		if (this.serverThread.isAlive()) {
			return;
		}

		this.threadPool = Executors.newFixedThreadPool(this.workerThreads);
		this.serverThread.start();
		this.clearThread.start();

	}

	/**
	 * Shuts down server.
	 */
	protected synchronized void stop() {
		this.serverThread.end();
		this.clearThread.end();
		this.threadPool.shutdown();
	}

	/**
	 * Class extends {@link Thread}.
	 * Thread accepts request and delegates
	 * processing and responding of request
	 * to threads in {@link ExecutorService}.
	 * @author Luka Mijić
	 *
	 */
	protected class ServerThread extends Thread {

		/**
		 * Flag is set to true if thread should be shut down.
		 */
		private boolean end = true;
		
		/**
		 * Server socket (ip, port)
		 */
		private ServerSocket serverSocket = null;
		

		/**
		 * Method used for shutting down thread.
		 * https://docs.oracle.com/javase/8/docs/technotes/guides/concurrency/threadPrimitiveDeprecation.html
		 */
		public void end() {
			this.end = true;
			try {
				if (this.serverSocket != null) {
					this.serverSocket.close();
					this.serverSocket = null;
				}
			} catch (IOException e) {
				this.serverSocket = null;
				e.printStackTrace();
			}
		}

		/**
		 * Creates {@link ServerSocket} and binds it.
		 * While <code>end</code> flag is false,
		 * keep serving requests.
		 */
		@Override
		public void run() {
			this.end = false;
			try {
				this.serverSocket = new ServerSocket();
				this.serverSocket.bind(new InetSocketAddress(SmartHttpServer.this.address, SmartHttpServer.this.port));
				while (!end) {
					Socket client = serverSocket.accept();
					ClientWorker cw = new ClientWorker(client);
					SmartHttpServer.this.threadPool.submit(cw);
				}
			} catch (IOException e) {
				this.serverSocket = null;
				this.end = true;
				e.printStackTrace();
			}
		}
	}

	/**
	 * Class extends {@link Thread}.
	 * Thread periodically cleans expired {@link SessionMapEntry}
	 * from sessions {@link Map}.
	 * @author Luka Mijić
	 *
	 */
	private class CleanSessionThread extends Thread {
		
		/**
		 * Flag is set to true if thread should be shut down.
		 */
		private boolean end = true;
		
		/**
		 * Sleep time
		 */
		private long sleepTime;
		
		/**
		 * Creates {@link CleanSessionThread} and
		 * {@link #setDaemon(boolean)} to true.
		 * @param sleepTime sleeping time between cleaning
		 */
		private CleanSessionThread(long sleepTime) {
			setDaemon(true);
			this.sleepTime = sleepTime;
		}
		
		/**
		 * Sets up <code>end</code> flag to true,
		 * giving signal to {@link Thread} to shut down.
		 */
		private void end() {
			end = true;
		}
		
		/**
		 * Periodically cleans expired {@link SessionMapEntry}
		 * from sessions {@link Map}.
		 */
		@Override
		public void run() {
			end = false;
			try {
				while(true) {
					sleep(sleepTime);
					if(end) {
						return;
					}
					synchronized (serverThread) {
						for(String sid:sessions.keySet().toArray(new String[sessions.size()])) {
							if(System.currentTimeMillis() > sessions.get(sid).validUntil) {
								sessions.remove(sid);
							}
						}
					}
				}
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			} 
		}
	}
	
	/**
	 * Implements {@link Runnable} and {@link IDispatcher}.
	 * Thread that analyses client request and returns
	 * appropriate answer.
	 * @author Luka Mijić
	 *
	 */
	private class ClientWorker implements Runnable, IDispatcher {
		
		/**
		 * Socket
		 */
		private Socket csocket;
		
		/**
		 * Input stream
		 */
		private PushbackInputStream istream;
		
		/**
		 * Output stream
		 */
		private OutputStream ostream;
		
		/**
		 * HTTP version
		 */
		private String version;
		
		/**
		 * Request method
		 */
		private String method;
		
		/**
		 * host name
		 */
		private String host;
		
		/**
		 * {@link Map} of parameters
		 */
		private Map<String, String> params = new HashMap<String, String>();
		
		/**
		 * {@link Map} of temporary parameters
		 */
		@SuppressWarnings("unused")
		private Map<String, String> tempParams = new HashMap<String, String>();
		
		/**
		 * {@link Map} of persistent parameters
		 */
		private Map<String, String> permPrams = new HashMap<String, String>();
		
		/**
		 * {@link List} of {@link RCCookie}s.
		 */
		private List<RCCookie> outputCookies = new ArrayList<RequestContext.RCCookie>();
		
		/**
		 * {@link RequestContext} of answer
		 */
		private RequestContext reqCtx = null;
		
		/**
		 * Session ID
		 */
		private String SID;

		/**
		 * Initialises {@link ClientWorker}.
		 * @param csocket
		 */
		public ClientWorker(Socket csocket) {
			super();
			this.csocket = csocket;
		}

		/**
		 * Analyses request.
		 * If request is invalid send HTTP message
		 * with appropriate status code and status text.
		 * 
		 * Otherwise do more detailed analysis of request 
		 * and given appropriate response.
		 */
		@Override
		public void run() {
			try {
				istream = new PushbackInputStream(csocket.getInputStream());
				ostream = csocket.getOutputStream();

				byte[] byteRequest = ServerUtil.readRequest(istream);
				if (byteRequest == null) {
					ServerUtil.sendError(ostream, 400, "Bad request");
					return;
				}

				List<String> headers = ServerUtil.extractHeaders(new String(byteRequest, StandardCharsets.US_ASCII));
				String[] firstLine = headers.isEmpty() ? null : headers.get(0).split(" ");
				if (firstLine == null || firstLine.length != 3) {
					ServerUtil.sendError(ostream, 400, "Bad request");
					return;
				}

				method = firstLine[0].toUpperCase();
				if (!method.equals("GET")) {
					ServerUtil.sendError(ostream, 405, "Method Not Allowed");
					return;
				}

				version = firstLine[2].toUpperCase();
				if (!supported_version.contains(version)) {
					ServerUtil.sendError(ostream, 505, "HTTP Version Not Supported");
					return;
				}

				setHost(headers);
				checkSession(headers);

				String requestedPath = firstLine[1];
				String path;
				int splitLocation = requestedPath.indexOf('?');
				if (splitLocation == -1) {
					path = requestedPath;
				} else {
					path = requestedPath.substring(0, splitLocation);
					if (requestedPath.length() > splitLocation + 1) {
						parseParameters(requestedPath.substring(splitLocation + 1));
					}
				}

				internalDispatchRequest(path, true);

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					csocket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		@Override
		public void dispatchRequest(String urlPath) throws Exception {
			internalDispatchRequest(urlPath, false);
		}

		/**
		 * Dispatches request.
		 * If urlPath starts with '/ext/' try to create appropriate 
		 * {@link IWebWorker} and let him process request.
		 * If there is {@link IWebWorker} associated with urlPath
		 * retrieve it and let worker process request.
		 * 
		 * Check if urlPath requested is valid, otherwise send error.
		 * Valid urlPath exists, is in document root and is not forbidden.
		 * 
		 * If urlPath ends with '.smscr' {@link SmartScriptEngine}
		 * is used to execute script and result is sent to client.
		 * 
		 * Otherwise send file to client.
		 * @param urlPath of request
		 * @param directCall if directCalls is false, access to '/private/' is granted
		 * @throws Exception
		 */
		public void internalDispatchRequest(String urlPath, boolean directCall) throws Exception {
			if (directCall) {
				if (urlPath.startsWith("/private/") || urlPath.equals("/private")) {
					ServerUtil.sendError(ostream, 404, "File not found");
					return;
				}
			}

			if (urlPath.startsWith("/ext/")) {
				String tempPath = urlPath.substring(5);

				String fqcn = "hr.fer.zemris.java.webserver.workers." + tempPath;

				try {

					Class<?> referenceToClass = this.getClass().getClassLoader().loadClass(fqcn);
					@SuppressWarnings("deprecation")
					Object newObject = referenceToClass.newInstance();
					IWebWorker iww = (IWebWorker) newObject;
					if (reqCtx == null) {
						reqCtx = new RequestContext(ostream, params, permPrams, outputCookies, null, this);
					}
					iww.processRequest(reqCtx);
					return;
				} catch (Exception contProcessing) {
				}
			}

			IWebWorker worker = workersMap.get(urlPath);
			if (worker != null) {
				if (reqCtx == null) {
					reqCtx = new RequestContext(ostream, params, permPrams, outputCookies, null, this);
				}
				worker.processRequest(reqCtx);
				return;
			}

			Path requestedFile = documentRoot.resolve(urlPath.substring(1));
			String extension = extractExtension(requestedFile.getFileName().toString()).toLowerCase();
			if (!requestedFile.startsWith(documentRoot)) {
				ServerUtil.sendError(ostream, 403, "Forbidden");
				return;
			} else if (!Files.isReadable(requestedFile) || !Files.isRegularFile(requestedFile)) {
				ServerUtil.sendError(ostream, 404, "File not found");
				return;
			}

			if (("." + extension).endsWith(SCRIPT_EXTENSION)) {
				if (reqCtx == null) {
					reqCtx = new RequestContext(ostream, params, permPrams, outputCookies, null, this);
				}
				reqCtx.setMimeType(mimeTypes.get("txt"));
				SmartScriptParser p = new SmartScriptParser(
						new String(Files.readAllBytes(requestedFile), StandardCharsets.UTF_8));
				new SmartScriptEngine(p.getDocumentNode(), reqCtx).execute();
				return;
			}

			if (reqCtx == null) {
				reqCtx = new RequestContext(ostream, params, permPrams, outputCookies);
			}
			String mimeType = mimeTypes.get(extension);
			mimeType = mimeType == null ? DEFAULT_MIME_TYPE : mimeType;
			reqCtx.setMimeType(mimeType);
			reqCtx.setContentLength(Files.size(requestedFile));
			

			byte[] buf = new byte[4 * 1024];

			BufferedInputStream is = new BufferedInputStream(new FileInputStream(requestedFile.toFile()));
			while (true) {
				int r = is.read(buf);
				if (r < 1)
					break;
				reqCtx.write(buf, 0, r);
			}
			is.close();
		}

		/**
		 * Method checks if session with user already exists.
		 * If sessions is expired or it doesn't exists, start new session,
		 * otherwise retrieve existing session.
		 * @param headers might store SID
		 */
		private void checkSession(List<String> headers) {
			synchronized (serverThread) {
				String sidCandidate = null;
				for (String header : headers) {
					if (header.trim().toLowerCase().startsWith("cookie:")) {
						String[] cookies = header.trim().substring(7).split(";");
						for (String cookie : cookies) {
							String[] splitted = cookie.split("=");
							String name = splitted[0].trim();
							if (name.equals("sid")) {
								sidCandidate = splitted[1].trim();
								break;
							}
						}
						break;
					}
				}

				SessionMapEntry entry = null;
				if (sidCandidate != null) {
					entry = sessions.get(sidCandidate);
					if (entry == null) {
						sidCandidate = null;
					} else if (!entry.host.equals(params.get("host"))) {
						sidCandidate = null;
					} else if (entry.validUntil < System.currentTimeMillis()) {
						sessions.remove(sidCandidate);
						sidCandidate = null;
					}

					if (sidCandidate != null) {
						SID = entry.sid;
						entry.validUntil = ServerUtil.getFutureTimeMilis(sessionTimeout * 1000);
					}
				}

				if (sidCandidate == null) {
					SID = ServerUtil.generateSID(sessionRandom);
					entry = new SessionMapEntry(SID, params.get("host"),
							ServerUtil.getFutureTimeMilis(sessionTimeout * 1000), new ConcurrentHashMap<>());
					sessions.put(entry.sid, entry);
					outputCookies.add(new RCCookie("sid", entry.sid, null, entry.host, "/"));
				}

				
				permPrams = entry.map;
			}
		}

		/**
		 * From HTTP header find host information and
		 * set it's value to key 'host' in parameter {@link Map}.
		 * @param headers HTTP header
		 */
		private void setHost(List<String> headers) {
			for (String header : headers) {
				if (header.trim().toLowerCase().startsWith("host:")) {
					host = header.substring(5).trim();
					if (host.contains(":")) {
						host = host.split(":")[0];
					}
					params.put("host", host);
					return;
				}
			}
			params.put("host", SmartHttpServer.this.domainName);
		}

		/**
		 * Parses parameters from request.
		 * @param paramsStr contains parameter info
		 */
		private void parseParameters(String paramsStr) {
			for (String param : paramsStr.split("&")) {
				try {
					String[] splitted = param.split("=");
					params.put(splitted[0], splitted[1]);
				} catch (IndexOutOfBoundsException ignorable) {
				}
			}
		}

		/**
		 * Extracts extension from file name
		 * @param fileName 
		 * @return extension
		 */
		private String extractExtension(String fileName) {
			return fileName.substring(fileName.lastIndexOf('.') + 1);
		}

	}

}
