package hr.fer.zemris.java.webserver;

/**
 * Interface models objects that can dispatch information.
 * @author Luka Mijić
 *
 */
public interface IDispatcher {

	/**
	 * Dispatches information based on given path.
	 * @param urlPath is given path
	 * @throws Exception
	 */
	void dispatchRequest(String urlPath) throws Exception;
}
