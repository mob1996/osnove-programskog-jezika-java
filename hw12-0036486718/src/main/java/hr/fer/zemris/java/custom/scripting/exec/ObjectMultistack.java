package hr.fer.zemris.java.custom.scripting.exec;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Class represents Map which can store multiple
 * values to single key and those values organized in 
 * stack like structure. 
 * @author Luka Mijić
 *
 */
public class ObjectMultistack {
	
	/**
	 * Represents entry in the map
	 */
	public static class MultistackEntry {

		/**
		 * wrapper stored in MultistackEntry
		 */
		private ValueWrapper wrapper;
		
		/**
		 * Reference to next multistack entry
		 */
		private MultistackEntry next;

		/**
		 * Constructor for MultistackEntry
		 * @param wrapper
		 */
		public MultistackEntry(ValueWrapper wrapper) {
			this.wrapper = Objects.requireNonNull(wrapper, "wrapper can't be null reference.");
		}

		/**
		 * @return wrapper
		 */
		public ValueWrapper getWrapper() {
			return wrapper;
		}
	}
	
	/**
	 * Map that stores stacks to keys.
	 */
	private Map<String, MultistackEntry> multiStack;
	
	/**
	 * Creates new ObjectMultiStack
	 */
	public ObjectMultistack() {
		multiStack = new HashMap<>();
	}
	
	/**
	 * Method pushes value to the stack associated with the 
	 * key name.
	 * @param name
	 * @param valueWrapper
	 * @throws NullPointerException if either name or valueWrapper are null reference
	 */
	public void push(String name, ValueWrapper valueWrapper) {
		Objects.requireNonNull(name, "Key can't be null");
		Objects.requireNonNull(valueWrapper, "Value can't be null.");
		
		MultistackEntry entry = new MultistackEntry(valueWrapper);
		entry.next = multiStack.get(name);
		
		multiStack.put(name, entry);
	}
	
	/**
	 * Check if stack associated with name is empty or not.
	 * @param name string to which stack is associated to
	 * @return true if stack is empty, otherwise return false
	 * @throws NullPointerException if name is null reference
	 */
	public boolean isEmpty(String name) {
		Objects.requireNonNull(name, "Key can't be null");
		return multiStack.get(name) == null;
	}
	
	/**
	 * Return value at the top of the stack associated with 
	 * name without removing that value from top of the stack.  
	 * @param name string to which stack is associated to
	 * @return value at the top of the stack
	 * @throws EmptyStackException if stack associated to name is empty.
	 */
	public ValueWrapper peek(String name) {
		if(isEmpty(name))
			throw new EmptyStackException("Stack associated with '" + name + "' is empty.");
		
		return multiStack.get(name).getWrapper();
	}
	
	/**
	 * Return value at the top of the stack associated with 
	 * name. Top of the stack is removed.
	 * @param name string to which stack is associated to
	 * @return value at the top of the stack
	 * @throws EmptyStackException if stack associated to name is empty.
	 */
	public ValueWrapper pop(String name) {
		if(isEmpty(name))
			throw new EmptyStackException("Stack associated with '" + name + "' is empty.");
		
		return multiStack.replace(name, multiStack.get(name).next).getWrapper(); //replace returns previous value
	}
}
