package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * 
 * This enum class represents all types of Tokens
 * @author Luka Mijić
 * 
 */
public enum TokenType {
	/**
	 * This token type represents end of file.
	 * No more tokens can't be generated after this one
	 * is generated.
	 */
	EOF, 
	/**
	 * Only Generated in TEXT_MODE of lexer for
	 * value '{$'
	 */
	START_TAG,
	/**
	 * Generated for '='
	 */
	ECHO_TAG_NAME,
	/**
	 * Only generated in TAG_MODE of lexer for 
	 * value'$}'
	 */
	END_TAG,
	/**
	 * Only generated in TEXT_MODE of lexer for
	 * every value, including white spaces except
	 * '{$'
	 */
	TEXT,
	/**
	 * Only generated in TAG_MODE of lexer for
	 * variable values
	 */
	VARIABLE,
	/**
	 * Only generated in TAG_MODE of lexer for
	 * values that can be parsed as integer.
	 */
	INTEGER,
	/**
	 * Only generated in TAG_MODE of lexer for values
	 * that can be parsed as double
	 */
	DOUBLE,
	/**
	 * Only generated in TAG_MODE of lexer for values
	 * that start with '@' and are followed by letter. 
	 * They can be followed by letters, digits and '_'
	 */
	FUNCTION,
	/**
	 * Only generated in TAG_MODE of lexer for values
	 * '+', '-', '*', '/' and '^'
	 */
	OPERATOR,
	/**
	 * Only generated in TAG_MODE of lexer for values
	 * enclosed by "
	 */
	STRING
}
