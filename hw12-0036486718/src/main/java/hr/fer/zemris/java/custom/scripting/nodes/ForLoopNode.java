package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * Class represents one ForLoop node in syntax tree.
 * Class extends Node class.
 * @author Luka Mijić
 *
 */
import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;

public class ForLoopNode extends Node{

	/**
	 * Variable in ForLoop Node.
	 * Can't be null.
	 */
	private ElementVariable variable;
	
	/**
	 * Start expression in ForLoop Node.
	 * Can't be null.
	 */
	private Element startExpression;
	
	/**
	 * End expression in ForLoop Node.
	 * Can't be null.
	 */
	private Element endExpression;
	
	/**
	 * Step expression in ForLoop Node.
	 * Can be null.
	 */
	private Element stepExpression;

	/**
	 * Constructor for ForLoopNode
	 * @param variable of ForLoopNode
	 * @param startExpression of ForLoopNode
	 * @param endExpression of ForLoopNode
	 * @param stepExpression of ForLoopNode
	 * @throws NullPointerException if any argument except endExpression references null
	 */
	public ForLoopNode(ElementVariable variable, Element startExpression, Element endExpression,
			Element stepExpression) {
		super();
		if(variable == null || startExpression == null || endExpression == null)
			throw new NullPointerException("Only stepExpression argument in constructor can be null.");
		this.variable = variable;
		this.startExpression = startExpression;
		this.endExpression = endExpression;
		this.stepExpression = stepExpression;
	}

	public ElementVariable getVariable() {
		return variable;
	}

	public Element getStartExpression() {
		return startExpression;
	}

	public Element getEndExpression() {
		return endExpression;
	}

	public Element getStepExpression() {
		return stepExpression;
	}
	
	
	@Override
	public String toString() {
		StringBuilder forBuilder = new StringBuilder();
		
		forBuilder.append("{$FOR ");
		forBuilder.append(variable.asText() + " ");
		forBuilder.append(startExpression.asText() + " ");
		forBuilder.append(endExpression.asText());
		if(stepExpression != null) {
			forBuilder.append(" " + stepExpression.asText());
		}
		forBuilder.append("$}");
		
		return forBuilder.toString();
	}
	
	@Override
	public void accept(INodeVisitor visitor) {
		visitor.visitForLoopNode(this);
	}
	
}
