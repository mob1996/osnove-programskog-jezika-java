package hr.fer.zemris.java.hw07.shell.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;

/**
 * Class represents pwd command.
 * @author Luka Mijić
 *
 */
public class PwdCommand implements ShellCommand {

	/**
	 * Command name
	 */
	private static final String COMMAND_NAME = "pwd";
	
	/**
	 * Description of command
	 */
	private static List<String> description;
	static {
		description = new ArrayList<>();
		description.add("Command: pwd");
		description.add("\t- command writes current working directory");
		description.add("\t- command takes 0 parameters");
	}
	
	/**
	 * Method executes command pwd. It prints
	 * current working directory.
	 * @param env is Enviroment in which help is executed
	 * @param arguments
	 * @return ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> args = CommandUtil.toArguments(arguments);
		
		if(args.size() != 0) {
			env.writeln(COMMAND_NAME + " takes no arguments.");
			return ShellStatus.CONTINUE;
		}
		
		env.writeln("Current directory: " + env.getCurrentDirectory());
		
		return ShellStatus.CONTINUE;
	}

	/**
	 * @return command name
	 */
	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	/**
	 * @return command description
	 */
	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(description);
	}

}
