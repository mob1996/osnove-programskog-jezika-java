package hr.fer.zemris.java.hw07.crypto;

import static hr.fer.zemris.java.hw07.crypto.Util.*;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

public class Digest implements Runnable {

	/**
	 * Used to initialize MessageDigest
	 */
	private static final String SHA256 = "SHA-256";
	
	/**
	 * How many bytes one KB has
	 */
	private static final int KB = 1024;
	
	/**
	 * Path to file that is used for calculating digest
	 */
	private Path toFile;
	
	/**
	 * Creates new Digest class with Path to file whose contents will be
	 * digested.
	 * @param toFile
	 */
	public Digest(Path toFile) {
		this.toFile = Objects.requireNonNull(toFile, "Given file must not be null.");
	}

	/**
	 * Method is used to establish communication between
	 * user and program, prompting him to enter expected digest. 
	 * After user enters requiered values digesting process is called. 
	 * @throws IllegalArgumentException if toFile doesn't exist or if it isn't file
	 */
	@Override
	public void run(){
		if(!Files.isRegularFile(toFile))
			throw new IllegalArgumentException("Given path must lead to file.");
		
		try(BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			BufferedInputStream inputStream = new BufferedInputStream(
						Files.newInputStream(toFile, StandardOpenOption.READ))){
			
			MessageDigest shaDigest = MessageDigest.getInstance(SHA256);
		
			String expectedDigest = promptUser(in, "Please provide expected sha-256 digest for " 
					+ toFile.getFileName().toString() + "\n>");
			String realDigest = byteToHex(calculateDigest(inputStream, shaDigest));
			
			System.out.println(digestMessage(realDigest, expectedDigest, toFile.getFileName().toString()));
		} catch (IOException | NoSuchAlgorithmException e) {
			System.err.println(e.getClass().getSimpleName() + " happened.");
		}
	}
	
	/**
	 * Calculates digest from bytes retrieved using InputStream.
	 * @param fileStream retrieves bytes from file
	 * @param digest calculates digest
	 * @return byte array that represents calculated digest
	 * @throws IOException
	 */
	private byte[] calculateDigest(InputStream fileStream, MessageDigest digest) throws IOException {
		byte[] buff = new byte[4 * KB];
		int read = 0;
		while((read = fileStream.read(buff)) != -1) {
			digest.update(buff, 0, read);
		}
		
		return digest.digest();
	}
	
	/**
	 * Method is used to return appropriate message to
	 * user. Messages content depends on realDigest and
	 * expectedDigest. 
	 * @param realDigest 
	 * @param expectedDigest
	 * @param fileName
	 * @return message to user
	 */
	private String digestMessage(String realDigest, String expectedDigest, String fileName) {
		String message = null;
		if(expectedDigest.equals(realDigest)) {
			message = "Digesting completed. Digest of " 
						+ fileName + " matches expected digest.";
		} else {
			message = "Digesting completed. Digest of" 
					+ fileName + " does not match the expected digest."
					+ " Digest was: " + realDigest;
		}
		
		return message;
	}
	
	/**
	 * Method writes prompt message to user and reads
	 * user's input.
	 * @param in is used for reading user input
	 * @param message that is used to prompt user
	 * @return input string
	 * @throws IOException
	 */
	private String promptUser(BufferedReader in, String message) throws IOException {
		System.out.print(message);
		return in.readLine().toLowerCase();
	}
	
}
