package hr.fer.zemris.java.hw07.shell.commands.name_building;

/**
 * Interface represents name builder
 * @author Luka Mijić
 *
 */
public interface NameBuilder {

	/**
	 * Executes name builder functionality
	 * @param info of name builder
	 */
	void execute(NameBuilderInfo info);
}
