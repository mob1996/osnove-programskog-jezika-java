package hr.fer.zemris.java.hw07.shell.commands;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.lexer.ArgumentLexer;
import hr.fer.zemris.java.hw07.shell.parser.ArgumentParser;

/**
 * Class contains some usefull utilities for working with commands
 * 
 * @author Luka Mijić
 *
 */
public class CommandUtil {

	/**
	 * User needs to write this after prompt asking for permission
	 */
	public static final String YES = "yes";
	
	/**
	 * Key that is associated to stack containing 
	 */
	public static final String PATH_STACK_KEY = "cdstack";
	
	/**
	 * Generates list of String arguments from given string
	 * @param arguments
	 * @return list of arguments
	 */
	public static List<String> toArguments(String arguments) {
		return new ArgumentParser(arguments).getArguments();
	}

	/**
	 * Returns path that resolves relative path to absolute one
	 * @param pathStr
	 * @return absolute path
	 */
	public static Path getPathFromString(String pathStr, Environment env) {
		return env.getCurrentDirectory().resolve(Paths.get(pathStr))
				  .toAbsolutePath().normalize();
	}

	/**
	 * Method extracts command from given input
	 * @param input 
	 * @return string of command
	 */
	public static String extractCommand(String input) {
		return new ArgumentLexer(input).nextToken().getValue();
	}
	
	/**
	 * Method asks user for permission.
	 * @param env is used for communication with user
	 * @return true if user writes 'yes' otherwise return false
	 */
	public static boolean askForPermission(Environment env, String message) {
		env.write(message);
		return env.readLine().toLowerCase().equals(YES);
	}
	
}
