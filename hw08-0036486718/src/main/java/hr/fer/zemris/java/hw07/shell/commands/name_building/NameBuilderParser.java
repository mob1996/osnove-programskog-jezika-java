package hr.fer.zemris.java.hw07.shell.commands.name_building;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Class is used for creating NameBuilder used for
 * creating new names,
 * @author Luka
 *
 */
public class NameBuilderParser {

	/**
	 * Function is used as factory for creating
	 * SubstitutionNameBuilder
	 */
	private static final Function<String, NameBuilder> 
					SUBSTITUTE_BUILDER_FACTORY = expr -> new SubstitutionNameBuilder(expr);

	/**
	 * Function is used as factory for creating
	 * Constant NameBuilder
	 */				
	private static final Function<String, NameBuilder>
					CONSTANT_BUILDER_FACTORY = expr -> new ConstantNameBuilder(expr);
	
	/**
	 * NameBuilder created for generating names
	 */
	private NameBuilder nameBuilder;
	
	/**
	 * Created from given expression, used for
	 * generating NameBuilders
	 */
	private char[] data;
	
	/**
	 * Used for traversing char[] data
	 */
	private int currentIndex;
	
	/**
	 * Test if current character is valid for creating
	 * SubstituteNameBuilder
	 */
	private final Predicate<char[]> SUBSTITUTE_PREDICATE 
			= c -> !(currentIndex > c.length || (currentIndex > 1 && data[currentIndex-1] == '}'));
	
	/**
	 * Test if current character is valid for creating
     * ConstantNameBuilder
	 */
	private final Predicate<char[]> CONSTANT_PREDICATE 
			= c -> currentIndex < c.length && !isSubstitutionSequence();		
	
	/**
	 * Constructor is used for setting up class properties
	 * and creating NameBuilder
	 * @param expression
	 */
	public NameBuilderParser(String expression) {
		Objects.requireNonNull(expression, "Expression must not be null.");
		this.data = expression.toCharArray();
		this.nameBuilder  = new NameBuilders(createBuilders());
		this.currentIndex = 0;
	}
	
	/**
	 * @return nameBuilder
	 */
	public NameBuilder getNameBuilder() {
		return nameBuilder;
	}
	
	/**
	 * Creates NameBuilders used for creating main NameBuilder
	 * @return list of NameBuilders
	 */
	private List<NameBuilder> createBuilders(){
		List<NameBuilder> builders = new ArrayList<>();
	
		NameBuilder builder;
		while((builder = nextNameBuilder()) != null) {
			builders.add(builder);
		}
		
		return builders;
	}
	
	/**
	 * Creates nex NameBuilder. If next sequence of
	 * characters is '${' it creates SubstituteNameBuilder
	 * otherwise it creates ConstantNameBuilder
	 * @return created NameBuilder
	 */
	private NameBuilder nextNameBuilder() {
		if(currentIndex >= data.length) return null;
		
		return isSubstitutionSequence() ? 
				next(SUBSTITUTE_PREDICATE, SUBSTITUTE_BUILDER_FACTORY) : next(CONSTANT_PREDICATE, CONSTANT_BUILDER_FACTORY);
	}
	
	/**
	 * Method traverses data as long as given predicate is satisfied.
	 * It appends characters that satisfy predicate and those characters are
	 * transformed into String. That String is given to factory function that 
	 * creates needed NameBuilder.
	 * @param pred that tests if character satisfies conditions
	 * @param factory creates needed NameBuilder
	 * @return created NameBuilder
	 */
	private NameBuilder next(Predicate<char[]> pred, Function<String, NameBuilder> factory) {
		StringBuilder builder = new StringBuilder();
		
		while(pred.test(data)) {
			builder.append(data[currentIndex++]);
		}
		
		return factory.apply(builder.toString());
	}

	/**
	 * Check if next character sequence is '${'. 
	 * @return true if next character sequence is '${',
	 * 			otherwise return false.
	 */
	private boolean isSubstitutionSequence() {
		if(data[currentIndex] == '$') {
			if(currentIndex + 1 < data.length && data[currentIndex + 1] == '{') {
				return true;
			}
		}
		return false;
	}
}
