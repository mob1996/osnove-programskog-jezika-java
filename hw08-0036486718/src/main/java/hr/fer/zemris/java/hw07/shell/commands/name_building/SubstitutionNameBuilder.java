package hr.fer.zemris.java.hw07.shell.commands.name_building;

/**
 * This builder is used to append given group of matcher.
 * @author Luka Mijić
 *
 */
public class SubstitutionNameBuilder implements NameBuilder{

	/**
	 * Group that is appended
	 */
	private int group;
	
	/**
	 * If size of string that is lower than
	 * this value add (spaces - size) spaces.
	 */
	private int spaces;
	
	/**
	 * If spaces should be added
	 */
	private boolean hasSpaces;
	
	/**
	 * If spaces should be replaced by zeroes
	 */
	private boolean zeroes;

	/**
	 * Constructor takes expression that is used
	 * to set up properties of class
	 * @param expression
	 */
	public SubstitutionNameBuilder(String expression) {
		setup(expression);
	}
	
	/**
	 * Method using given expression sets up properties of class
	 * @param expression
	 * @throws IllegalArgumentException if given expression is invalid
	 */
	private void setup(String expression) {
		if(expression.startsWith("${") && expression.endsWith("}")) {
			expression = expression.substring(2, expression.length()-1).trim();
		}
		
		String[] args = expression.split(",");
		
		if(args.length == 1) {
			this.hasSpaces = false;
			this.zeroes = false;
		} else if(args.length == 2) {
			this.hasSpaces = true;
			String spacesArg = args[1].trim();
			this.zeroes = spacesArg.startsWith("0");
			this.spaces = Integer.parseInt(spacesArg);
		} else {
			throw new IllegalArgumentException("Invalid number of arguments in Substitution brackets.");
		}
		this.group = Integer.parseInt(args[0].trim());
	}

	/**
	 * Appends generated string to StringBuilder
	 * @param info providies StringBuilder
	 */
	@Override
	public void execute(NameBuilderInfo info) {
		String toAppend = createString(info);
		info.getStringBuilder().append(toAppend);
	}
	
	/**
	 * @param info provides way get values of group from
	 * 			matcher in info
	 * @return generated string
	 */
	private String createString(NameBuilderInfo info) {
		StringBuilder builder = new StringBuilder();
		
		String groupValue = info.getGroup(this.group);
		
		if(this.hasSpaces) {
			String padding = this.zeroes ? "0" : " ";
			for(int i = groupValue.length(); i < this.spaces; i++) {
				builder.append(padding);
			}
		}
		
		builder.append(groupValue);
		
		return builder.toString();
	}
	
}
