package hr.fer.zemris.java.hw07.shell.commands.name_building;

/**
 * Interface represents information of name builder
 * @author Luka Mijić
 *
 */
public interface NameBuilderInfo {

	/**
	 * @return StringBuilder
	 */
	StringBuilder getStringBuilder();
	
	/**
	 * @param index
	 * @return group at given index
	 */
	String getGroup(int index);
}
