package hr.fer.zemris.java.hw07.shell.commands.name_building;

import java.util.Objects;

/**
 * This builder is used to append given string constant on
 * StringBuilder given through NameBuilderInfo
 * @author Luka Mijić
 *
 */
public class ConstantNameBuilder implements NameBuilder {

	/**
	 * Constant that is appended
	 */
	private String constant;
	
	/**
	 * Constructor that takes constant
	 * @param constant
	 * @throws NullPointerException if constant is null reference
	 */
	public ConstantNameBuilder(String constant) {
		this.constant = Objects.requireNonNull(constant, "Given constant string can't be null.");
	}

	/**
	 * Method appends constant to StringBuilder
	 * prvided through info
	 * @param info provides StringBuillder
	 */
	@Override
	public void execute(NameBuilderInfo info) {
		info.getStringBuilder().append(this.constant);
	}

}
