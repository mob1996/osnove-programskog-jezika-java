package hr.fer.zemris.java.hw07.shell;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import hr.fer.zemris.java.hw07.shell.commands.CatCommand;
import hr.fer.zemris.java.hw07.shell.commands.CdCommand;
import hr.fer.zemris.java.hw07.shell.commands.CharsetsCommand;
import hr.fer.zemris.java.hw07.shell.commands.CopyCommand;
import hr.fer.zemris.java.hw07.shell.commands.CpTreeCommand;
import hr.fer.zemris.java.hw07.shell.commands.DropdCommand;
import hr.fer.zemris.java.hw07.shell.commands.ExitCommand;
import hr.fer.zemris.java.hw07.shell.commands.HelpCommand;
import hr.fer.zemris.java.hw07.shell.commands.HexDumpCommand;
import hr.fer.zemris.java.hw07.shell.commands.LSCommand;
import hr.fer.zemris.java.hw07.shell.commands.ListdCommand;
import hr.fer.zemris.java.hw07.shell.commands.MassRenameCommand;
import hr.fer.zemris.java.hw07.shell.commands.MkdirCommand;
import hr.fer.zemris.java.hw07.shell.commands.PopdCommand;
import hr.fer.zemris.java.hw07.shell.commands.PushdCommand;
import hr.fer.zemris.java.hw07.shell.commands.PwdCommand;
import hr.fer.zemris.java.hw07.shell.commands.RmTreeCommand;
import hr.fer.zemris.java.hw07.shell.commands.ShellCommand;
import hr.fer.zemris.java.hw07.shell.commands.SymbolCommand;
import hr.fer.zemris.java.hw07.shell.commands.TreeCommand;

/**
 * Class is implementation of working
 * Environment.
 * @author Luka Miijć
 *
 */
public class EnvironmentImpl implements Environment {

	/**
	 * Associates command names to commands.
	 */
	private static SortedMap<String, ShellCommand> commands;
	static {
		commands = new TreeMap<>();
		commands.put("help", new HelpCommand());
		commands.put("ls", new LSCommand());
		commands.put("charsets", new CharsetsCommand());
		commands.put("tree", new TreeCommand());
		commands.put("mkdir", new MkdirCommand());
		commands.put("copy", new CopyCommand());
		commands.put("cat", new CatCommand());
		commands.put("hexdump", new HexDumpCommand());
		commands.put("symbol", new SymbolCommand());
		commands.put("exit", new ExitCommand());
		commands.put("pwd", new PwdCommand());
		commands.put("cd", new CdCommand());
		commands.put("pushd", new PushdCommand());
		commands.put("popd", new PopdCommand());
		commands.put("listd", new ListdCommand());
		commands.put("dropd", new DropdCommand());
		commands.put("rmtree", new RmTreeCommand());
		commands.put("cptree", new CpTreeCommand());
		commands.put("massrename", new MassRenameCommand());
	}
	
	/**
	 * Input Stream
	 */
	private BufferedReader input;
	
	/**
	 * Outputstream
	 */
	private BufferedWriter output;
	
	/**
	 * Symbols for different functions
	 */
	private Character promptSymbol;
	private Character multiLineSymbol;
	private Character moreLinesSymbol;
	
	/**
	 * Current working directory of environment
	 */
	private Path currentPath;
	
	/**
	 * Data that is shared across commands
	 */
	private Map<String, Object> sharedData;
	
	/**
	 * Creates new EnvironmentImpl.
	 * Sets input stream to System.in and output
	 * to System.out
	 */
	public EnvironmentImpl() {
		this.input = new BufferedReader(new InputStreamReader(System.in));
		this.output = new BufferedWriter(new OutputStreamWriter(System.out));
		
		this.promptSymbol = '>';
		this.multiLineSymbol = '|';
		this.moreLinesSymbol = '\\';
		
		this.currentPath = Paths.get(".").toAbsolutePath().normalize();
		this.sharedData = new HashMap<>();
	}
	
	/**
	 * Reads line from input stream
	 */
	@Override
	public String readLine() throws ShellIOException {
		try {
			return input.readLine();
		} catch (IOException e) {
			throw new ShellIOException("Can't read line.");
		}
	}

	/**
	 * Writes text param to output stream
	 * @param text
	 */
	@Override
	public void write(String text) throws ShellIOException {
		try {
			output.write(text);
			output.flush();
		} catch (IOException e) {
			throw new ShellIOException("Can't write.");
		}

	}

	/**
	 * Writes to output stream and adds newline at the end of the
	 * text
	 * @param text
	 */
	@Override
	public void writeln(String text) throws ShellIOException {
			this.write(text + "\n");
	}

	/**
	 * @return unmodifiable sorted map that contains names of commands
	 * 			associated to commands
	 */
	@Override
	public SortedMap<String, ShellCommand> commands() {
		return Collections.unmodifiableSortedMap(commands);
	}

	/**
	 * @return multiline symbol
	 */
	@Override
	public Character getMultiLineSymbol() {
		return this.multiLineSymbol;
	}

	/**
	 * @param symbol sets multiline symbol to this param
	 */
	@Override
	public void setMultiLineSymbol(Character symbol) {
		this.multiLineSymbol = symbol;
	}

	/**
	 * @param prompty symbol
	 */
	@Override
	public Character getPromptSymbol() {
		return this.promptSymbol;
	}

	/**
	 * @param symbol sets prompt symbol to this param
	 */
	@Override
	public void setPromptSymbol(Character symbol) {
		this.promptSymbol = symbol;
	}

	/**
	 * @return morelines symbol
	 */
	@Override
	public Character getMoreLinesSymbol() {
		return this.moreLinesSymbol;
	}

	/**
	 * @param symbol sets morelines symbol to this param
	 */
	@Override
	public void setMoreLinesSymbol(Character symbol) {
		this.moreLinesSymbol = symbol;
	}

	/**
	 * @return current directory
	 */
	@Override
	public Path getCurrentDirectory() {
		return this.currentPath;
	}

	/**
	 * @param path sets current directory to given path
	 * @throws IllegalArgumentException if given path doens't lead to
	 * 			existing directory
	 */
	@Override
	public void setCurrentDirectory(Path path) {
		if(!Files.isDirectory(path))
			throw new IllegalArgumentException("Given path must lead toexisting directory.");
		this.currentPath = path.toAbsolutePath().normalize();
	}

	/**
	 * @param key of object
	 * @return value associated with key
	 */
	@Override
	public Object getSharedData(String key) {
		return sharedData.get(key);
	}

	/**
	 * Method adds to map value associated with key
	 * @param key of data
	 * @param value data
	 */
	@Override
	public void setSharedData(String key, Object value) {
		sharedData.put(key, value);
	}

}
