package hr.fer.zemris.java.hw07.shell.commands;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;

public class DropdCommand implements ShellCommand {

	/**
	 * Command name
	 */
	private static final String COMMAND_NAME = "dropd";
	
	/**
	 * Description of command
	 */
	private static List<String> description;
	static {
		description = new ArrayList<>();
		description.add("Command: dropd");
		description.add("\t- command takes zero arguments");
		description.add("\t- command removes path from stack");
	}
	
	/**
	 * Method executes command dropd. It pops path from stack.
	 * @param env is Enviroment in which help is executed
	 * @param arguments
	 * @return ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> args = CommandUtil.toArguments(arguments);
		
		if(args.size() != 0) {
			env.writeln(COMMAND_NAME + " takes no arguments.");
			return ShellStatus.CONTINUE;
		}
		
		@SuppressWarnings("unchecked")
		Stack<Path> stack = (Stack<Path>) env.getSharedData(CommandUtil.PATH_STACK_KEY);
		
		if(stack == null || stack.isEmpty()) {
			env.writeln("Zero paths are stored at stack.");
			return ShellStatus.CONTINUE;
		}
		
		Path newDir = stack.pop().toAbsolutePath().normalize();
		env.writeln(newDir.toString() + " removed from stack.");
		
		return ShellStatus.CONTINUE;
	}

	/**
	 * @return command name
	 */
	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	/**
	 * @return command description
	 */
	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(description);
	}

}
