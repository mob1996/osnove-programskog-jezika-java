package hr.fer.zemris.java.hw07.shell.commands;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;

/**
 * Class represents listd command.
 * @author Luka Mijić
 *
 */
public class ListdCommand implements ShellCommand {

	/**
	 * Command name
	 */
	private static final String COMMAND_NAME = "listd";
	
	/**
	 * Description of command
	 */
	private static List<String> description;
	static {
		description = new ArrayList<>();
		description.add("Command: pushd");
		description.add("\t- command takes zero arguments");
		description.add("\t- command lists all paths stored on stack");
	}
	
	/**
	 * Method executes listd command. It lists all
	 * path stored on stack. 
	 * @param env is Enviroment in which help is executed
	 * @param arguments
	 * @return ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> args = CommandUtil.toArguments(arguments);
		
		if(args.size() != 0) {
			env.writeln(COMMAND_NAME + " takes no arguments.");
			return ShellStatus.CONTINUE;
		}
		
		@SuppressWarnings("unchecked")
		Stack<Path> stack = (Stack<Path>) env.getSharedData(CommandUtil.PATH_STACK_KEY);
		
		if(stack != null) {
			env.writeln("Stack contains " + stack.size() + " entries.");
			for(int i = stack.size() - 1; i >= 0; i--) {
				env.writeln("\t" + stack.get(i));
			}
		} else {
			env.writeln("Stack doesn't exist.");
		}
		
		return ShellStatus.CONTINUE;
	}

	/**
	 * @return command name
	 */
	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	/**
	 * @return command description
	 */
	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(description);
	}

}
