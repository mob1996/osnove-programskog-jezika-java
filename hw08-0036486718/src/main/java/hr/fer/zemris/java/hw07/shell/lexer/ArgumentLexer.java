package hr.fer.zemris.java.hw07.shell.lexer;

import java.util.Objects;

/**
 * Class represents lexical analysator. 
 * @author Luka Mijić
 *
 */
public class ArgumentLexer {

	/**
	 * Input text is stored into char array
	 */
	private char[] data;
	
	/**
	 * Current token
	 */
	private Token token;
	
	/**
	 * Index of first index that is not used.
	 * Initial value is 0.
	 */
	private int currentIndex;
	
	/**
	 * Constructor takes String text and transforms it
	 * into char array. 
	 * @param text that is transformed
	 * @throws NullpointerException if given argument is null
	 */
	public ArgumentLexer(String text) {
		this.data = Objects.requireNonNull(text, 
				"Text argument in Lexer constructor cannot be null.").toCharArray();
	}
	
	 /**
     * @return token that was generated last
     */
	public Token getToken() {
		return token;
	}
	
	/**
	 * Method is used to generate next token from input string.
	 * @return next token from string
	 * @throws LexerException if EOF was reached
	 */
	public Token nextToken() {
		if(token != null && token.getType() == TokenType.EOF) throw new LexerException("No more tokens avaliable");
	
		skipWhiteSpace();
		if(currentIndex >= data.length) {
			token = new Token(TokenType.EOF, null);
			return token;
		}
		
		token = data[currentIndex] == '"' ? extractFromQuotes() : extract();
		
		return token;
	}
	
	/**
	 * Method extracts new token. Adds every character
	 * to string until blank space is reached.
	 * @return new token
	 */
	private Token extract() {
		StringBuilder builder = new StringBuilder();
		while(currentIndex < data.length) {
			if(Character.isWhitespace(data[currentIndex])) break;
			builder.append(data[currentIndex++]);
		}
		return new Token(TokenType.ARGUMENT, builder.toString());
	}

	/**
	 * Method extracts new token from enclosing quotes.
	 * @return new token
	 * @throws LexerException if there are no enclosing quotes
	 */
	private Token extractFromQuotes() {
		currentIndex++;
		StringBuilder builder = new StringBuilder();
		while(data[currentIndex] != '"') {
			builder.append(data[currentIndex++]);
			if(currentIndex >= data.length) {
				throw new LexerException("Arugment that has \" at start, must be enclosed with quotes.");
			}
		}
		currentIndex++;
		if(currentIndex < data.length && !Character.isWhitespace(data[currentIndex])) {
			throw new LexerException("Qouting must be last element of text or it must be followed by blank.");
		}
		return new Token(TokenType.ARGUMENT, builder.toString());
	}

	/**
	 * Method is used to skip all white space from current index 
	 * onwards. After calling this method index is at location of next
	 * non whitespace character or it is out of bounds. 
	 */
	private void skipWhiteSpace() {
		while(currentIndex < data.length) {
			if(!Character.isWhitespace(data[currentIndex])) break;
			currentIndex++;
		}
	}
}
