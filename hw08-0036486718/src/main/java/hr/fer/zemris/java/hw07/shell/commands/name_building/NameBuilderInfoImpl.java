package hr.fer.zemris.java.hw07.shell.commands.name_building;

import java.util.Objects;
import java.util.regex.Matcher;

/**
 * Implementation of NameBuilderInfo
 * @author Luka Mijić
 *
 */
public class NameBuilderInfoImpl implements NameBuilderInfo {

	private Matcher matcher;
	
	/**
	 * Number of groups in matcher, counts 0 group.
	 */
	private int numOfGroups;
	private StringBuilder strBuilder;
	
	/**
	 * Creates new NameBuilderInfo
	 * @param matcher 
	 */
	public NameBuilderInfoImpl(Matcher matcher) {
		this.matcher = Objects.requireNonNull(matcher, "Given matcher can't be null.");
		this.strBuilder = new StringBuilder();
		this.matcher.find();
		this.numOfGroups = this.matcher.groupCount() + 1;
	}
	
	/**
	 * @return strBuilder
	 */
	@Override
	public StringBuilder getStringBuilder() {
		return strBuilder;
	}

	/**
	 * @return String associated to given index
	 * 			in matcher
	 * @throws IndexOutOfBoundsException 
	 */
	@Override
	public String getGroup(int index) {
		if(index < 0 || index >= numOfGroups) {
			throw new IndexOutOfBoundsException("Index must be element of [0, " + numOfGroups + ">.");
		}
		return this.matcher.group(index);
	}

}
