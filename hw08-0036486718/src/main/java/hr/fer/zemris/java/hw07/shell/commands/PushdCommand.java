package hr.fer.zemris.java.hw07.shell.commands;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;

/**
 * Class represents pushd command.
 * @author Luka Mijić
 *
 */
public class PushdCommand implements ShellCommand {

	/**
	 * Command name
	 */
	private static final String COMMAND_NAME = "pushd";
	
	/**
	 * Description of command
	 */
	private static List<String> description;
	static {
		description = new ArrayList<>();
		description.add("Command: pushd");
		description.add("\t- command takes one argument, which is path to directory");
		description.add("\t- command pushes current directory on stack and uses given path as current one");
	}
	
	/**
	 * Method executes command pushd. It pushes current
	 * working directory on stack if given directory exists.
	 * Given directory becomes new working directory. 
	 * @param env is Enviroment in which help is executed
	 * @param arguments
	 * @return ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> args = CommandUtil.toArguments(arguments);
		
		if(args.size() != 1) {
			env.writeln(COMMAND_NAME + " takes only one argument.");
			return ShellStatus.CONTINUE;
		}
		
		Path newDir = CommandUtil.getPathFromString(args.get(0), env);
		
		if(!Files.isDirectory(newDir)) {
			env.writeln("Given path must lead to existing directory.");
			return ShellStatus.CONTINUE;
		}
		
		@SuppressWarnings("unchecked")
		Stack<Path> stack = (Stack<Path>) env.getSharedData(CommandUtil.PATH_STACK_KEY);
		
		if(stack == null) {
			stack = new Stack<>();
			env.setSharedData(CommandUtil.PATH_STACK_KEY, stack);
		}
		
		env.writeln("Pushing " + env.getCurrentDirectory() + " on the stack.\n" 
				+ "New working directory is " + newDir + ".");
		stack.push(env.getCurrentDirectory());
		env.setCurrentDirectory(newDir);
		
		return ShellStatus.CONTINUE;
	}

	/**
	 * @return command name
	 */
	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	/**
	 * @return command description
	 */
	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(description);
	}

}
