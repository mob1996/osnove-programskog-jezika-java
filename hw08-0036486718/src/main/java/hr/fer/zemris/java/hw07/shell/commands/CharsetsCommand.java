package hr.fer.zemris.java.hw07.shell.commands;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.SortedMap;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;
/**
 * Class represents charsets command.
 * @author Luka Mijić
 *
 */
public class CharsetsCommand implements ShellCommand {

	/**
	 * Command name
	 */
	private static final String COMMAND_NAME = "charsets";
	
	/**
	 * Map of all avaliable charsets
	 */
	private static final SortedMap<String, Charset> CHARSETS
											= Charset.availableCharsets();
	
	/**
	 * Description of command
	 */
	private static List<String> description;
	static {
		description = new ArrayList<>();
		description.add("Command: charsets");
		description.add("\t- command lists all avaliable charset on this platform");
	}
	
	/**
	 * Method executes command charsets. It every avaliable charsets
	 * @param env is Enviroment in which help is executed
	 * @param arguments
	 * @return ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if(!CommandUtil.toArguments(arguments).isEmpty()) {
			env.writeln(COMMAND_NAME + " takes no arguments.");
			return ShellStatus.CONTINUE;
		}
		
		env.writeln("Avaliable charsets: ");
		CHARSETS.forEach((k, v) -> env.writeln("\t- " + k));
		return ShellStatus.CONTINUE;
	}

	/**
	 * @return command name
	 */
	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	/**
	 * @return command description
	 */
	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(description);
	}

}
