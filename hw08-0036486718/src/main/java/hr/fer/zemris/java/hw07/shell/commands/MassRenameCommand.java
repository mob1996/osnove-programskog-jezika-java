package hr.fer.zemris.java.hw07.shell.commands;

import java.io.IOException;
import java.nio.file.DirectoryStream.Filter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;
import hr.fer.zemris.java.hw07.shell.commands.name_building.NameBuilder;
import hr.fer.zemris.java.hw07.shell.commands.name_building.NameBuilderInfo;
import hr.fer.zemris.java.hw07.shell.commands.name_building.NameBuilderInfoImpl;
import hr.fer.zemris.java.hw07.shell.commands.name_building.NameBuilderParser;

/**
 * Class represents massrename command.
 * @author Luka Mijić
 *
 */
public class MassRenameCommand implements ShellCommand {

	/**
	 * Class implements interface filter. It is used for filtering
	 * out paths that don't satisfy accept method.
	 * @author Luka Mijić
	 *
	 */
	private static class MassRenameFilter implements Filter<Path> {
		/**
		 * Pattern that path should satisfy
		 */
		private Pattern pattern;

		/**
		 * Creates filter 
		 * @param pattern that is used for filtering
		 */
		private MassRenameFilter(Pattern pattern) {
			this.pattern = pattern;
		}

		/**
		 * @return true if path is regular file and if path name
		 * 			matches pattern. Otherwise return false.
		 */
		@Override
		public boolean accept(Path p) throws IOException {
			return Files.isRegularFile(p) && pattern.matcher(p.getFileName().toString()).matches();
		}

	}

	/**
	 * Class is subcommand that implements various subcommand 
	 * of massrename
	 * @author Luka Mijić
	 *
	 */
	private static class SubCommand implements Runnable {

		private static final int PATTERN_FLAGS = Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE;
		private List<String> args;
		private Environment env;

		/**
		 * Creates SubCommand
		 * @param arguments list of arguments
		 * @param env working environment
		 */
		private SubCommand(List<String> arguments, Environment env) {
			this.args = arguments;
			this.env = env;
		}

		/**
		 * Method unwraps argument from list of arguments and
		 * calls appropriate method to execute subcommand.
		 */
		@Override
		public void run() {
			int argsNum = args.size();
			if (argsNum < 4 || argsNum > 5) {
				env.writeln("This method takes 4 or 5 arguments. Was " + argsNum + ".");
				return;
			}

			Path dir1 = CommandUtil.getPathFromString(args.get(0), env);
			Path dir2 = CommandUtil.getPathFromString(args.get(1), env);

			if (!Files.isDirectory(dir1) || !Files.isDirectory(dir2)) {
				env.writeln("First and second arguments must be existing directories.");
				return;
			}

			Pattern pattern;
			try {
				pattern = Pattern.compile(args.get(3), PATTERN_FLAGS);
				Filter<Path> filter = new MassRenameFilter(pattern);

				if (argsNum == 4) {
					fourArgumentCommand(pattern, filter, dir1, dir2);
				} else if (argsNum == 5) {
					fiveArgumentCommand(pattern, filter, dir1, dir2);
				}
			} catch (PatternSyntaxException e) {
				env.writeln("Pattern syntax is invalid.");
			} catch (IOException e) {
				env.writeln("IOException while using directory: " + dir1.toString());
			}
		}

		/**
		 * This method is called when there are 4 arguments.
		 * It implements 'filter' and 'group' subcommands.
		 * @param pattern that path should match
		 * @param filter used for filtering paths
		 * @param dir1 source directory
		 * @param dir2 destination directory
		 * @throws IOException 
		 */
		private void fourArgumentCommand(Pattern pattern, Filter<Path> filter, Path dir1, Path dir2) throws IOException {
			String commandName = this.args.get(2);
			Consumer<Path> consumer = null;

			if (commandName.equals("filter")) {
				consumer = p -> env.writeln(p.getFileName().toString());
			} else if (commandName.equals("groups")) {
				consumer = p -> {
					Matcher matcher = pattern.matcher(p.toString());
					env.write(p.getFileName().toString());
					if (matcher.find()) {
						for (int i = 0, groups = matcher.groupCount() + 1; i < groups; i++) {
							env.write(" " + i + ": " + matcher.group(i));
						}
					} else {
						env.write(" No groups found.");
					}
					env.writeln("");
				};
			} else {
				env.writeln("Invalid subcommand for four arguments.");
			}
			
			Files.newDirectoryStream(dir1, filter).forEach(consumer);
		}
		
		/**
		 * This method is called when there are 5 arguments.
		 * It implements 'show' and 'execute' subcommands.
		 * @param pattern that path should match
		 * @param filter used for filtering paths
		 * @param dir1 source directory
		 * @param dir2 destination directory
		 * @throws IOException 
		 */
		private void fiveArgumentCommand(Pattern pattern, Filter<Path> filter, Path dir1, Path dir2) throws IOException{
			String commandName = this.args.get(2);
			Consumer<Path> consumer = null;
			
			String expression = this.args.get(4);
			NameBuilderParser parser = new NameBuilderParser(expression);
			NameBuilder builder = parser.getNameBuilder();

			if (commandName.equals("show")) {
				consumer = p -> {
					Matcher matcher = pattern.matcher(p.toString());
					env.writeln(p.toAbsolutePath() + "=>" + dir2.resolve(newName(builder, matcher)).toAbsolutePath());
				};
			} else if (commandName.equals("execute")) {
				consumer = p -> {
					Matcher matcher = pattern.matcher(p.toString());
					Path dest = dir2.resolve(newName(builder, matcher));
					try {
						Files.move(p, dest);
					} catch (IOException e) {
						env.writeln("Move of " + p.toString() + " was not successful.");
					}
				};
			} else {
				env.writeln("Invalid subcommand for five arguments.");
			}
			
			Files.newDirectoryStream(dir1, filter).forEach(consumer);
		}
		
		private static String newName(NameBuilder builder, Matcher matcher) {
			NameBuilderInfo info = new NameBuilderInfoImpl(matcher);
			builder.execute(info);
			return info.getStringBuilder().toString();
		}

	}

	/**
	 * Command name
	 */
	private static final String COMMAND_NAME = "massrename";

	/**
	 * Description of command
	 */
	private static List<String> description;
	static {
		description = new ArrayList<>();
		description.add("Command: massrename");
		description.add("\t- takes four or five arguments");
		description.add("\t- first two arguments must be existing directories. "
				+ "First one is source, other is destination.");
		description.add("\t- third argument is subcommand (filter, groups, show, execute)");
		description.add("\t- fourth argument is regular expression that is used for filtering files in"
				+ " source directory");
		description.add("\t- fifth argument is used for renaming found files. ");
	}

	/**
	 * Method executes command massrename.
	 * @param env is Enviroment in which help is executed
	 * @param arguments
	 * @return ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> args = CommandUtil.toArguments(arguments);
		int argsNum = args.size();

		if (argsNum < 4 || argsNum > 5) {
			env.writeln("This method takes 4 or 5 arguments. Was " + argsNum + ".");
			return ShellStatus.CONTINUE;
		}
		new SubCommand(args, env).run();

		return ShellStatus.CONTINUE;
	}

	/**
	 * @return command name
	 */
	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	/**
	 * @return command description
	 */
	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(description);
	}

}
