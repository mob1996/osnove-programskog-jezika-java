package hr.fer.zemris.java.hw07.shell;

import java.nio.file.Path;
import java.util.SortedMap;

import hr.fer.zemris.java.hw07.shell.commands.ShellCommand;

/**
 * Interface represents enviroment that enables
 * user to communicate with program. 
 * @author Luka Mijić
 *
 */
public interface Environment {
	
	/**
	 * Reads line from source
	 * @return line that is read
	 * @throws ShellIOException
	 */
	String readLine() throws ShellIOException;
	
	/**
	 * Writes text to destination
	 * @param text that is written
	 * @throws ShellIOException
	 */
	void write(String text) throws ShellIOException;
	
	/**
	 * Writes text, adding new line character at the end.
	 * @param text text that is written
	 * @throws ShellIOException
	 */
	void writeln(String text) throws ShellIOException;
	
	/**
	 * @return map containing commands
	 */
	SortedMap<String, ShellCommand> commands();
	
	/**
	 * @return symbol that represents multi line
	 */
	Character getMultiLineSymbol();
	
	/**
	 * @param symbol becomes new multi line symbol
	 */
	void setMultiLineSymbol(Character symbol);
	
	/**
	 * @return symbol that represents prompt
	 */
	Character getPromptSymbol();
	
	/**
	 * @param symbol becomes new prompt symbol
	 */
	void setPromptSymbol(Character symbol);
	
	/**
	 * @return symbol thta represents more lines symbol
	 */
	Character getMoreLinesSymbol();
	
	/**
	 * @param symbol becomes new more lines symbol
	 */
	void setMoreLinesSymbol(Character symbol);
	
	/**
	 * @return path to current directory
	 */
	Path getCurrentDirectory();
	
	/**
	 * @param path becomes new current directory
	 */
	void setCurrentDirectory(Path path);
	
	/**
	 * @param key associated with data
	 * @return shared data
	 */
	Object getSharedData(String key);
	
	/**
	 * Set given value to given key.
	 * @param key of data
	 * @param value data
	 */
	void setSharedData(String key, Object value);
}
