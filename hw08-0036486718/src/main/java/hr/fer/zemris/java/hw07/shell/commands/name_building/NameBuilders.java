package hr.fer.zemris.java.hw07.shell.commands.name_building;

import java.util.List;
import java.util.Objects;

/**
 * This builder recieves list of NameBuilers.
 * @author Luka Mijić
 *
 */
public class NameBuilders implements NameBuilder {

	/**
	 * List of NameBuilders
	 */
	private List<NameBuilder> builders;
	
	/**
	 * Creates new NameBuilders from list of NameBuilders
	 * @param builders is list of NameBuilders
	 * @throws NullPointerException if given list is null reference
	 */
	public NameBuilders(List<NameBuilder> builders) {
		this.builders = Objects.requireNonNull(builders, "Given list of builders must not be null.");
	}
	
	/**
	 * Execute calls execute on all NameBuilders in list.
	 */
	@Override
	public void execute(NameBuilderInfo info) {
		this.builders.forEach(b -> b.execute(info));
	}

}
