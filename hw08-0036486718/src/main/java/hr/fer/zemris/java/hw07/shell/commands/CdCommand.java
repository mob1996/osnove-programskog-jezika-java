package hr.fer.zemris.java.hw07.shell.commands;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;

/**
 * Class represents cd command.
 * @author Luka Mijić
 *
 */
public class CdCommand implements ShellCommand{

	/**
	 * Command name
	 */
	private static final String COMMAND_NAME = "cd";
	
	/**
	 * Description of command
	 */
	private static List<String> description;
	static {
		description = new ArrayList<>();
		description.add("Command: cd");
		description.add("\t- command is used to change working directory");
		description.add("\t- command takes one argument");
		description.add("\t- if given path is relative it is resolved using current directory");
		description.add("\t- if given path is absolute, given path becomes new working directory");
	}
	
	/**
	 * Method executes command cd. It changes current working 
	 * directory. If given arguments can be interpreted as absolute
	 * path that exists then given path becomes new working directory.
	 * If given path is relative, it is resolved using current directory.
	 * @param env is Enviroment in which help is executed
	 * @param arguments
	 * @return ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> args = CommandUtil.toArguments(arguments);
		
		if(args.size() != 1) {
			env.writeln("Expected only one argument. Was " + args.size());
			return ShellStatus.CONTINUE;
		}
		
		Path newDir = CommandUtil.getPathFromString(args.get(0), env);
		if(Files.isDirectory(newDir)) {
			env.setCurrentDirectory(newDir);
			env.writeln("New working directory is: " + env.getCurrentDirectory().toString());
		} else {
			env.writeln("Given directory must exists. Was: " + newDir.toString());
		}
		return ShellStatus.CONTINUE;
	}

	/**
	 * @return command name
	 */
	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	/**
	 * @return command description
	 */
	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(description);
	}

}
