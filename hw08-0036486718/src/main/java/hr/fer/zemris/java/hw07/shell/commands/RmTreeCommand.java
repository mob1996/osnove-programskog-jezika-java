package hr.fer.zemris.java.hw07.shell.commands;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;

public class RmTreeCommand implements ShellCommand {

	/**
	 * Inner class of RmTreeCommand. Visitor is used for
	 * traversing current directory and all sub directories,
	 * files and removing them.
	 * @author Luka Mijić
	 *
	 */
	private static class RemoveVisitor implements FileVisitor<Path> {

		@Override
		public FileVisitResult postVisitDirectory(Path dir, IOException arg1) throws IOException {
			Files.delete(dir);
			return FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes arg1) throws IOException {
			return FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes arg1) throws IOException {
			Files.delete(file);
			return FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult visitFileFailed(Path file, IOException arg1) throws IOException {
			return FileVisitResult.TERMINATE;
		}
		
	}
	
	private static RemoveVisitor rmVisitor = new RemoveVisitor();
	
	/**
	 * Command name
	 */
	private static final String COMMAND_NAME = "rmtree";
	
	/**
	 * Description of command
	 */
	private static List<String> description;
	static {
		description = new ArrayList<>();
		description.add("Command: pushd");
		description.add("\t- command takes one argument, path that leads to existing directory");
		description.add("\t- method removes everything in given directory");
	}
	
	/**
	 * Method executes command rmtree. It removes all
	 * given directory, all subdirectories and files within.
	 * @param env is Enviroment in which help is executed
	 * @param arguments
	 * @return ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> args = CommandUtil.toArguments(arguments);
		
		if(args.size() != 1) {
			env.writeln("Expected only one argument. Was " + args.size());
			return ShellStatus.CONTINUE;
		}
		
		Path dirPath = CommandUtil.getPathFromString(args.get(0), env);
		if(!Files.isDirectory(dirPath)) {
			env.writeln("Given path must lead to directory.");
			return ShellStatus.CONTINUE;
		}
		
		String message = "Are you sure that you want to delete " + dirPath.toString()
			+ " and all subdirectories and files of it. Write 'yes' if you want to proceed "
			+ env.getPromptSymbol()+ " ";
		if(!CommandUtil.askForPermission(env, message)) {
			env.writeln("Permision denied.");
			return ShellStatus.CONTINUE;
		}
		
		try {
			Files.walkFileTree(dirPath, rmVisitor);
		} catch (IOException e) {
			env.writeln("IOException happened: " + e.getMessage());
		}
		
		
		return ShellStatus.CONTINUE;
	}

	/**
	 * @return command name
	 */
	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	/**
	 * @return command description
	 */
	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(description);
	}

}
