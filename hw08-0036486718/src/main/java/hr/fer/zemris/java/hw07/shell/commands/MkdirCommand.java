package hr.fer.zemris.java.hw07.shell.commands;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;

/**
 * Class represents mkdir command.
 * @author Luka Mijić
 *
 */
public class MkdirCommand implements ShellCommand {

	/**
	 * Command name
	 */
	private static final String COMMAND_NAME = "mkdir";

	/**
	 * Description of command
	 */
	private static List<String> description;
	static {
		description = new ArrayList<>();
		description.add("Command: mkdir");
		description.add("\t- command creates all directories in given path that don't exist");

	}

	/**
	 * Method executes command mkdir. It creates all directories
	 * in given path that already dont exist.
	 * @param env is Enviroment in which help is executed
	 * @param arguments
	 * @return ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> args = CommandUtil.toArguments(arguments);
		
		if(args.size() != 1) {
			env.writeln("Expected only one argument. Was " + args.size());
			return ShellStatus.CONTINUE;
		}

		Path dirPath = CommandUtil.getPathFromString(args.get(0), env);
		Path currentPath = dirPath.getRoot();

		if (!Files.isDirectory(currentPath)) {
			env.writeln("Given root doesn't exist. Error.");
			return ShellStatus.CONTINUE;
		}

		for (Path d : dirPath) {
			currentPath = currentPath.resolve(d);
			if (!Files.exists(currentPath)) {
				try {
					Files.createDirectory(currentPath);
				} catch (IOException e) {
					env.writeln("Can't create " + currentPath + ".");
					return ShellStatus.CONTINUE;
				}
			}
		}
		return ShellStatus.CONTINUE;
	}


	/**
	 * @return command name
	 */
	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	/**
	 * @return command description
	 */
	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(description);
	}

}
