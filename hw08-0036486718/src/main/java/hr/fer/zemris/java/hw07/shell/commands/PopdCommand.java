package hr.fer.zemris.java.hw07.shell.commands;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;

/**
 * Class represents popd command.
 * @author Luka Mijić
 *
 */
public class PopdCommand implements ShellCommand {

	/**
	 * Command name
	 */
	private static final String COMMAND_NAME = "popd";
	
	/**
	 * Description of command
	 */
	private static List<String> description;
	static {
		description = new ArrayList<>();
		description.add("Command: popd");
		description.add("\t- command takes zero arguments");
		description.add("\t- command takes path from stack and sets it as current directory");
		description.add("\t\t if stack isn't empty or taken directory exists.");
	}
	
	/**
	 * Method executes command popd. It pops path from stack
	 * and if given path leads to existing directory that path
	 * becomes new working directory. 
	 * @param env is Enviroment in which help is executed
	 * @param arguments
	 * @return ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> args = CommandUtil.toArguments(arguments);
		
		if(args.size() != 0) {
			env.writeln(COMMAND_NAME + " takes no arguments.");
			return ShellStatus.CONTINUE;
		}
		
		@SuppressWarnings("unchecked")
		Stack<Path> stack = (Stack<Path>) env.getSharedData(CommandUtil.PATH_STACK_KEY);
		
		if(stack == null || stack.isEmpty()) {
			env.writeln("Zero paths are stored at stack.");
			return ShellStatus.CONTINUE;
		}
		
		Path newDir = stack.pop().toAbsolutePath().normalize();
		
		if(!Files.isDirectory(newDir)) {
			env.writeln(newDir + " is not existing directory.");
			return ShellStatus.CONTINUE;
		}
		
		env.setCurrentDirectory(newDir);
		env.writeln("New working directory taken from stack: " + env.getCurrentDirectory() + ".");
		
		return ShellStatus.CONTINUE;
	}

	/**
	 * @return command name
	 */
	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	/**
	 * @return command description
	 */
	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(description);
	}

}
