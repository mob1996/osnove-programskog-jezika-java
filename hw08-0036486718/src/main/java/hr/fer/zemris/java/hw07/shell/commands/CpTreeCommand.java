package hr.fer.zemris.java.hw07.shell.commands;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;

/**
 * Class represents cptree command.
 * @author Luka Mijić
 *
 */
public class CpTreeCommand implements ShellCommand {

	/**
	 * Inner class of CpTreeCommand. Visitor is used for traversing current
	 * directory and copying it's content to another directory.
	 * 
	 * @author Luka Mijić
	 */
	private static class CopyVisitor implements FileVisitor<Path> {

		/**
		 * String used to go back in resolve
		 */
		private static final String GO_BACK = "..";

		/**
		 * Destination for copying
		 */
		private Path dest;

		/**
		 * Skip resolving for root of tree.
		 */
		private boolean skipFirst;

		private CopyVisitor(Path dest) {
			this.dest = dest;
			this.skipFirst = false;
		}

		@Override
		public FileVisitResult postVisitDirectory(Path dir, IOException arg1) throws IOException {
			this.dest = dest.resolve(GO_BACK);
			return FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes arg1) throws IOException {
			if (this.skipFirst != false) {
				this.dest = dest.resolve(dir.getFileName());
				Files.createDirectory(dest);
			}
			this.skipFirst = true;
			return FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes arg1) throws IOException {
			Path fileDest = dest.resolve(file.getFileName());

			BufferedInputStream input = new BufferedInputStream(Files.newInputStream(file, StandardOpenOption.READ));
			BufferedOutputStream output = new BufferedOutputStream(
					Files.newOutputStream(fileDest, StandardOpenOption.CREATE, StandardOpenOption.WRITE));

			byte[] buff = new byte[4 * KB];
			int read = 0;

			while ((read = input.read(buff)) != -1) {
				output.write(buff, 0, read);
			}

			input.close();
			output.close();
			return FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult visitFileFailed(Path file, IOException arg1) throws IOException {
			return FileVisitResult.SKIP_SUBTREE;
		}

	}

	/**
	 * Command name
	 */
	private static final String COMMAND_NAME = "cptree";

	/**
	 * How many bytes one KB has
	 */
	private static final int KB = 1024;

	/**
	 * Description of command
	 */
	private static List<String> description;
	static {
		description = new ArrayList<>();
		description.add("Command: cptree");
		description.add("\t- command takes two arguments");
		description.add("\t- first parameter takes path to existing directory");
		description.add("\t- second parameters sets destination");
		description.add("\t- command copies all files and directories from first parameter to destination");
	}

	/**
	 * Method executes command cptree. It copies all files
	 * and subdirectories to destination. 
	 * @param env is Enviroment in which help is executed
	 * @param arguments
	 * @return ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> args = CommandUtil.toArguments(arguments);
		if (args.size() != 2) {
			env.writeln("Invalid number of arguments for cptree command. Expected two.");
			return ShellStatus.CONTINUE;
		}
		Path src = CommandUtil.getPathFromString(args.get(0), env);
		Path dest = CommandUtil.getPathFromString(args.get(1), env);

		if (!Files.isDirectory(src)) {
			env.writeln("First argument must be existing directory.");
			return ShellStatus.CONTINUE;
		}

		try {
			if (!Files.isDirectory(dest)) {
				if (!Files.isDirectory(dest.getParent())) {
					env.writeln("Invalid second argument.");
				}
			} else {
				dest = dest.resolve(src.getFileName());
			}
			
			while(Files.exists(dest)) {
				String fileName = "c_" + dest.getFileName();
				dest = dest.getParent().resolve(fileName);
			}
			
			Files.createDirectory(dest);
				
			env.writeln("Started copying.");
			Files.walkFileTree(src, new CopyVisitor(dest));
			env.writeln("Copying was successfull.");
		} catch (IOException e) {
			env.writeln("Error while using cptree: " + e.getMessage());
		}

		return ShellStatus.CONTINUE;
	}

	/**
	 * @return command name
	 */
	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	/**
	 * @return command description
	 */
	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(description);
	}

}
