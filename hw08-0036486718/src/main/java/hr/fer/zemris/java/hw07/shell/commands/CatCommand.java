package hr.fer.zemris.java.hw07.shell.commands;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;

/**
 * Class represents cat command.
 * @author Luka Mijić
 *
 */
public class CatCommand implements ShellCommand {
	
	/**
	 * Command name
	 */
	private static final String COMMAND_NAME = "cat";
	
	/**
	 * Description of command
	 */
	private static List<String> description;
	static {
		description = new ArrayList<>();
		description.add("Command: cat");
		description.add("\t- command takes one or two arguments");
		description.add("\t- as first argument it always expects path to file");
		description.add("\t- if second argument is not given, command will use default charset");
		description.add("\t\t, if u enter second argument, some other valid charset is expected (check charsets command).");
	}
	
	/**
	 * Method executes command cat. It reads every line from
	 * file and writes it using env.
	 * @param env is Enviroment in which help is executed
	 * @param arguments
	 * @return ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> args = CommandUtil.toArguments(arguments);
		
		int numOfArgs = args.size();
		if(numOfArgs != 1 && numOfArgs != 2) {
			env.writeln("Invalid number of parameters for cat command. Expected 1 or 2 parameters.");
			return ShellStatus.CONTINUE;
		}
		Charset cs;
		try {
			cs = numOfArgs == 1 ? Charset.defaultCharset() : Charset.forName(args.get(1));
		} catch(UnsupportedCharsetException exc) {
			env.writeln("Given charset doesn't exist");
			return ShellStatus.CONTINUE;
		}
		Path src = CommandUtil.getPathFromString(args.get(0), env);
		
		if(!Files.isRegularFile(src)) {
			env.writeln("Path to existing file expected.");
			return ShellStatus.CONTINUE;
		}
		
		cat(src, cs, env);
		
		return ShellStatus.CONTINUE;
	}
	
	/**
	 * Method does actual reading of files..
	 * @param src of file
	 * @param cs charset used for reading
	 * @param env used for communication
	 */
	private void cat(Path src, Charset cs, Environment env) {
		try(BufferedReader fileReader = Files.newBufferedReader(src, cs)){
			fileReader.lines().forEach(line -> env.writeln(line));
		} catch(IOException e) {
			env.writeln("IOException happened.");
		} catch(RuntimeException e) {
			env.writeln(e.getMessage());
		}
	}
	
	/**
	 * @return command name
	 */
	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	/**
	 * @return command description
	 */
	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(description);
	}
	
}
