package hr.fer.zemris.java.hw07.shell.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.SortedMap;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;

/**
 * Class represents help command.
 * @author Luka Mijić
 */
public class HelpCommand implements ShellCommand {

	/**
	 * Command name
	 */
	private static final String COMMAND_NAME = "help";
	
	/**
	 * Description of command
	 */
	private static List<String> description;
	static {
		description = new ArrayList<>();
		description.add("Command: help");
		description.add("\t- if zero arguments are given, lists all supported commands");
		description.add("\t- if one argument is given and that argument is supported command");
		description.add("\t\tlist name and description of command.");
		description.add("\t- otherwise print description of help");
	}
	
	/**
	 * Method executes command help.
	 * @param env is Enviroment in which help is executed
	 * @param arguments If arguments String is emptry method lists all 
	 * 			supported commands. If not, checks if arguments is supported
	 * 			command. If it is print description of that command, otherwise
	 * 			print appropriate message
	 * @return ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> args = CommandUtil.toArguments(arguments);
		
		SortedMap<String, ShellCommand> commands = env.commands();
		if(args.isEmpty()) {
			env.writeln("All supported commands are: ");
			commands.forEach((k, v) -> env.writeln("\t- " + v.getCommandName()));
		} else if(args.size() == 1){
			ShellCommand command = commands.get(args.get(0).toLowerCase());
			
			if(command == null) {
				env.writeln("Invalid argument in help command.");
			} else {
				command.getCommandDescription()
					.forEach((line) -> env.writeln(line));
			}
		} else {
			env.writeln("Expected only one argument. Was " + args.size());
		}
		
		return ShellStatus.CONTINUE;
	}

	/**
	 * @return name of command
	 */
	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	/**
	 * @return description of command
	 */
	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(description);
	}

}
