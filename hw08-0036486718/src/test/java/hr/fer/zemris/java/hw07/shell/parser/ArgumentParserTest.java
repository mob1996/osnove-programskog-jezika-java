package hr.fer.zemris.java.hw07.shell.parser;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

public class ArgumentParserTest {

	public void areStringEqual(String[] expected, List<String> actual) {
		for(int i = 0; i < expected.length; i++) {
			assertEquals(expected[i], actual.get(i));
		}
	}
	
	@Test
	public void testZeroArguments() {
		ArgumentParser parser = new ArgumentParser("");
		
		assertEquals(0, parser.getArguments().size());
	}
	
	@Test
	public void testValidArgumentsWithoutQuotes() {
		ArgumentParser parser = new ArgumentParser("arg0\targ1 \narg2 arg3");
		
		String[] expected = {"arg0", "arg1", "arg2", "arg3"};
		List<String> actual = parser.getArguments();
		
		areStringEqual(expected, actual);
	}
	
	@Test
	public void testValidArgumentsWithQuotes() {
		String documentBody = "\"F:/Games/World of Warcraft\" \"arg0\t\n\"\t\"arg1  arg2 arg3\"";
		ArgumentParser parser = new ArgumentParser(documentBody);
		
		String[] expected = {"F:/Games/World of Warcraft", "arg0\t\n", "arg1  arg2 arg3"};
		List<String> actual = parser.getArguments();
		
		areStringEqual(expected, actual);
	}
	
	@Test (expected = ParserException.class)
	public void testInvalidArgumentWithQuotes() {
		String documentBody = "\"F:/Games/World of Warcraft\" \"arg0\t\n\"\t\"arg1  arg2 arg3";
		new ArgumentParser(documentBody);
	}
	
	@Test
	public void testMultipleVariousArguments() {
		String documentBody = "\"F:/Games/World of Warcraft\" arg0\t\n\t\"arg1  arg2 arg3\" arg4 arg5";
		ArgumentParser parser = new ArgumentParser(documentBody);
		
		String[] expected = {"F:/Games/World of Warcraft", "arg0", "arg1  arg2 arg3", "arg4", "arg5"};
		List<String> actual = parser.getArguments();
		
		areStringEqual(expected, actual);
	}
}
