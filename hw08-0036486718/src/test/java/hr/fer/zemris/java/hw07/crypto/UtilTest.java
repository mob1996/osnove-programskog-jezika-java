package hr.fer.zemris.java.hw07.crypto;
import static org.junit.Assert.*;

import org.junit.Test;

import hr.fer.zemris.java.hw07.crypto.Util;

public class UtilTest {

	@Test (expected = NullPointerException.class)
	public void testInvalidHexToByte0() {
		Util.hexToByte(null);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testInvalidHexToByte1() {
		Util.hexToByte("0ab");
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testInvalidHexToByte2() {
		Util.hexToByte("0abk");
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testInvalidHexToByte3() {
		Util.hexToByte("\t0abd");
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testInvalidHexToByte4() {
		Util.hexToByte("\t0abd");
	}
	
	@Test (expected = NullPointerException.class)
	public void testInvalidByteToHex0() {
		Util.byteToHex(null);
	}
	
	@Test
	public void testValidHexToByte() {
		byte[] bytes = {(byte) 0xFF, 0x12, 0x55, (byte) 0xA3, 0x3C, 0x2F, 0x4D, (byte) 0xE2, 0x00};
		byte[] hexBytes = Util.hexToByte("ff1255a33c2f4DE200");
		assertArrayEquals(bytes, hexBytes);
	}
	
	@Test
	public void testValidByteToHex() {
		String hex = "a32bcde4398adf";
		byte[] transformBytes = {(byte) 0xa3, 0x2b, (byte) 0xcd, (byte) 0xe4, 0x39, (byte) 0x8a, (byte) 0xdf};
		assertEquals(hex, Util.byteToHex(transformBytes));
	}
	
	@Test
	public void testMultiple() {
		String hex = "a32bcde4398adf";
		byte[] transformBytes = {(byte) 0xa3, 0x2b, (byte) 0xcd, (byte) 0xe4, 0x39, (byte) 0x8a, (byte) 0xdf};
		
		String newHex = Util.byteToHex(transformBytes);
		assertEquals(hex, newHex);
		assertArrayEquals(transformBytes, Util.hexToByte(newHex));
	}
}
