package hr.fer.zemris.java.hw06.demo4;

import static org.junit.Assert.*;

import org.junit.Test;

public class StudentRecordTest {

	private static double EPSILON = 10E-6;
	
	@Test 
	public void testConstructor() {
		StudentRecord student = new StudentRecord("0000000001", "Ivić", "Ivo", 27.5, 25.3, 10.2, 3);
		
		assertEquals("0000000001", student.getJmbag());
		assertEquals("Ivić", student.getLastName());
		assertEquals("Ivo", student.getFirstName());
		assertEquals(27.5, student.getMidTermScore(), EPSILON);
		assertEquals(25.3, student.getFinalScore(), EPSILON);
		assertEquals(10.2, student.getLabScore(), EPSILON);
		assertEquals(3, student.getGrade());
		
	}
	@Test (expected = NullPointerException.class)
	public void testInvalidConstructor0() {
		new StudentRecord(null, "Ivić", "Ivo", 27.5, 25.3, 10.2, 3);
	}
	
	@Test (expected = NullPointerException.class)
	public void testInvalidConstructor1() {
		new StudentRecord("0000000001", null, "Ivo", 27.5, 25.3, 10.2, 3);
	}
	
	@Test (expected = NullPointerException.class)
	public void testInvalidConstructor2() {
		new StudentRecord("0000000001", "Ivić", null, 27.5, 25.3, 10.2, 3);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testInvalidCostructor3() {
		new StudentRecord("00000000001", "Ivić", "Ivo", 27.5, 25.3, 10.2, 3);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testInvalidCostructor4() {
		new StudentRecord("000000000h1", "Ivić", "Ivo", 27.5, 25.3, 10.2, 3);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testInvalidCostructor5() {
		new StudentRecord("0000000001", "Iv?ić", "Ivo", 27.5, 25.3, 10.2, 3);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testInvalidCostructor6() {
		new StudentRecord("0000000001", "Iv	ić", "Ivo", 27.5, 25.3, 10.2, 3); //tab
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testInvalidCostructor7() {
		new StudentRecord("0000000001", "Ivić", "Iv*o", 27.5, 25.3, 10.2, 3);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testInvalidCostructor8() {
		new StudentRecord("0000000001", "Ivić", "Ivo", -5, 25.3, 10.2, 3);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testInvalidCostructor9() {
		new StudentRecord("0000000001", "Ivić", "Ivo", 27.5, -0.2, 10.2, 3);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testInvalidCostructor10() {
		new StudentRecord("0000000001", "Ivić", "Ivo", 27.5, 25.3, -152, 3);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testInvalidCostructor11() {
		new StudentRecord("0000000001", "Ivić", "Ivo", 27.5, 25.3, 10.2, 0);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testInvalidCostructor12() {
		new StudentRecord("0000000001", "Ivić", "Ivo", 27.5, 25.3, 10.2, -1);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testInvalidCostructor13() {
		new StudentRecord("0000000001", "Ivić", "Ivo", 27.5, 25.3, 10.2, 6);
	}
	
	@Test
	public void testValidParse() {
		StudentRecord student = StudentRecord.parse(" 0000000001\tIvić\tIvo Ivica\t 27.5"
				+ "\t 25.3\t\n 10.2\t 3");
		
		assertEquals("0000000001", student.getJmbag());
		assertEquals("Ivić", student.getLastName());
		assertEquals("Ivo Ivica", student.getFirstName());
		assertEquals(27.5, student.getMidTermScore(), EPSILON);
		assertEquals(25.3, student.getFinalScore(), EPSILON);
		assertEquals(10.2, student.getLabScore(), EPSILON);
		assertEquals(3, student.getGrade());
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testInvalidParse0() {
		StudentRecord.parse(" 0000000001   Ivić\tIvo Ivica\t 27.5"
				+ "\t 25.3\t\n 10.2\t 3");
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testInvalidParse1() {
		StudentRecord.parse(" 0000000001\tIvić\tIvo Ivica\t 27.5"
				+ "\t 25.3\t\n 10.s2\t 3");
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testInvalidParse2() {
		StudentRecord.parse(" 0000000001\tIvić\tIvo Ivica\t 27.5"
				+ "\t 25.3\t\n 10.2\t -3");
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testInvalidParse3() {
		StudentRecord.parse(" 0000000001\tIvić\tIvo Ivica\t -27.5"
				+ "\t 25.3\t\n 10.2\t 3");
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testInvalidParse4() {
		StudentRecord.parse(" 0000k00001\tIvić\tIvo Ivica\t 27.5"
				+ "\t 25.3\t\n 10.2\t 3");
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testInvalidParse5() {
		StudentRecord.parse(" 00020000001\tIvić\tIvo Ivica\t 27.5"
				+ "\t 25.3\t\n 10.2\t 3");
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testInvalidParse6() {
		StudentRecord.parse(" 0000000001\tIvić\tIvo Ivica\t 27.5"
				+ "\t 25.3\t\n 10.2\t 6");
	}
	
	@Test
	public void testToString() {
		StudentRecord s = new StudentRecord("0000000001", "Ivić", "Ivo", 27.5, 25.3, 10.2, 3);
		StudentRecord student = StudentRecord.parse(s.toString());
		
		assertEquals("0000000001", student.getJmbag());
		assertEquals("Ivić", student.getLastName());
		assertEquals("Ivo", student.getFirstName());
		assertEquals(27.5, student.getMidTermScore(), EPSILON);
		assertEquals(25.3, student.getFinalScore(), EPSILON);
		assertEquals(10.2, student.getLabScore(), EPSILON);
		assertEquals(3, student.getGrade());
	}
}
