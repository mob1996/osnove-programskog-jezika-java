package hr.fer.zemris.java.custom.scripting.exec;

import static org.junit.Assert.*;

import org.junit.Test;

import hr.fer.zemris.java.custom.scripting.exec.ValueWrapper;

public class ValueWrapperTest {
	
	private static final double EPSILON = 1E-6;
	
	@Test
	public void testAdd() {
		ValueWrapper wrapper = new ValueWrapper(3);
		
		wrapper.add(6);
		assertTrue(wrapper.getValue().getClass() == Integer.class);
		assertEquals(9, Integer.class.cast(wrapper.getValue()).intValue());
		
		wrapper.add("-11");
		assertTrue(wrapper.getValue().getClass() == Integer.class);
		assertEquals(-2,  Integer.class.cast(wrapper.getValue()).intValue());
		
		wrapper.add(57.5);
		assertTrue(wrapper.getValue().getClass() == Double.class);
		assertEquals(55.5, Double.class.cast(wrapper.getValue()).doubleValue(), EPSILON);
		
		wrapper.add(0.25);
		assertTrue(wrapper.getValue().getClass() == Double.class);
		assertEquals(55.75, Double.class.cast(wrapper.getValue()).doubleValue(), EPSILON);
		
		wrapper.add(0.25);
		assertTrue(wrapper.getValue().getClass() == Double.class);
		assertEquals(56, Double.class.cast(wrapper.getValue()).doubleValue(), EPSILON);
		
		wrapper.add("-5.55");
		assertTrue(wrapper.getValue().getClass() == Double.class);
		assertEquals(50.45, Double.class.cast(wrapper.getValue()).doubleValue(), EPSILON);
		
		wrapper.add(-1);
		assertTrue(wrapper.getValue().getClass() == Double.class);
		assertEquals(49.45, Double.class.cast(wrapper.getValue()).doubleValue(), EPSILON);
	}
	
	@Test
	public void testSub() {
		ValueWrapper wrapper = new ValueWrapper(null);
		
		wrapper.subtract(6);
		assertTrue(wrapper.getValue().getClass() == Integer.class);
		assertEquals(-6, Integer.class.cast(wrapper.getValue()).intValue());
		
		wrapper.add(null);
		assertTrue(wrapper.getValue().getClass() == Integer.class);
		assertEquals(-6,  Integer.class.cast(wrapper.getValue()).intValue());
		
		wrapper.subtract(57.5);
		assertTrue(wrapper.getValue().getClass() == Double.class);
		assertEquals(-63.5, Double.class.cast(wrapper.getValue()).doubleValue(), EPSILON);
		
		wrapper.subtract("-0.25");
		assertTrue(wrapper.getValue().getClass() == Double.class);
		assertEquals(-63.25, Double.class.cast(wrapper.getValue()).doubleValue(), EPSILON);
		
		wrapper.subtract("-0.25");
		assertTrue(wrapper.getValue().getClass() == Double.class);
		assertEquals(-63.0, Double.class.cast(wrapper.getValue()).doubleValue(), EPSILON);
		
		wrapper.subtract("-5.55");
		assertTrue(wrapper.getValue().getClass() == Double.class);
		assertEquals(-57.45, Double.class.cast(wrapper.getValue()).doubleValue(), EPSILON);
		
		wrapper.subtract("1");
		assertTrue(wrapper.getValue().getClass() == Double.class);
		assertEquals(-58.45, Double.class.cast(wrapper.getValue()).doubleValue(), EPSILON);
	}

	@Test
	public void testMul() {
		ValueWrapper wrapper = new ValueWrapper(2);
		
		wrapper.multiply(3);
		assertTrue(wrapper.getValue().getClass() == Integer.class);
		assertEquals(6,  Integer.class.cast(wrapper.getValue()).intValue());
		
		wrapper.multiply("-3");
		assertTrue(wrapper.getValue().getClass() == Integer.class);
		assertEquals(-18,  Integer.class.cast(wrapper.getValue()).intValue());
		
		wrapper.multiply("-1e3");
		assertTrue(wrapper.getValue().getClass() == Double.class);
		assertEquals(18000,  Double.class.cast(wrapper.getValue()).doubleValue(), EPSILON);
		
		wrapper.multiply("1e-4");
		assertTrue(wrapper.getValue().getClass() == Double.class);
		assertEquals(1.8,  Double.class.cast(wrapper.getValue()).doubleValue(), EPSILON);
		
		wrapper.multiply("2.3");
		assertTrue(wrapper.getValue().getClass() == Double.class);
		assertEquals(4.14,  Double.class.cast(wrapper.getValue()).doubleValue(), EPSILON);
		
		wrapper.multiply(null);
		assertTrue(wrapper.getValue().getClass() == Double.class);
		assertEquals(0,  Double.class.cast(wrapper.getValue()).doubleValue(), EPSILON);
		
		wrapper.multiply(500);
		assertTrue(wrapper.getValue().getClass() == Double.class);
		assertEquals(0,  Double.class.cast(wrapper.getValue()).doubleValue(), EPSILON);
	}
	
	@Test
	public void testDiv() {
		ValueWrapper wrapper = new ValueWrapper(5);
		
		wrapper.divide(2);
		assertTrue(wrapper.getValue().getClass() == Integer.class);
		assertEquals(2,  Integer.class.cast(wrapper.getValue()).intValue());
		
		wrapper.divide(2);
		assertTrue(wrapper.getValue().getClass() == Integer.class);
		assertEquals(1,  Integer.class.cast(wrapper.getValue()).intValue());
		
		wrapper.divide(1.0/25);
		assertTrue(wrapper.getValue().getClass() == Double.class);
		assertEquals(25,  Double.class.cast(wrapper.getValue()).doubleValue(), EPSILON);
		
		wrapper.divide(5);
		assertTrue(wrapper.getValue().getClass() == Double.class);
		assertEquals(5,  Double.class.cast(wrapper.getValue()).doubleValue(), EPSILON);
		
		wrapper.divide(2);
		assertTrue(wrapper.getValue().getClass() == Double.class);
		assertEquals(2.5,  Double.class.cast(wrapper.getValue()).doubleValue(), EPSILON);
		
		wrapper.divide(-5.0/33);
		assertTrue(wrapper.getValue().getClass() == Double.class);
		assertEquals(-16.5,  Double.class.cast(wrapper.getValue()).doubleValue(), EPSILON);
	}
	
	@Test
	public void testNumCompare() {
		ValueWrapper wrapper = new ValueWrapper(5);
		
		assertTrue(wrapper.numCompare(15) < 0);
		assertTrue(wrapper.numCompare(-10) > 0);
		assertTrue(wrapper.numCompare(5) == 0);
		assertTrue(wrapper.numCompare(5.0) == 0);
		
		wrapper = new ValueWrapper("-2.0");
		assertTrue(wrapper.numCompare("12") < 0);
		assertTrue(wrapper.numCompare("-8.25") > 0);
		assertTrue(wrapper.numCompare(-2) == 0);
		assertTrue(wrapper.numCompare(-2.0) == 0);
		
		wrapper = new ValueWrapper(null);
		assertTrue(wrapper.numCompare("12") < 0);
		assertTrue(wrapper.numCompare("-8.25") > 0);
		assertTrue(wrapper.numCompare(0) == 0);
		assertTrue(wrapper.numCompare(0.0) == 0);
		assertTrue(wrapper.numCompare(null) == 0);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testInvalidDiv0() {
		ValueWrapper wrapper = new ValueWrapper(1);
		wrapper.divide(null);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testInvalidDiv1() {
		ValueWrapper wrapper = new ValueWrapper(1);
		wrapper.divide(0);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testInvalidDiv2() {
		ValueWrapper wrapper = new ValueWrapper(1);
		wrapper.divide(0.0);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testInvalidParse0() {
		ValueWrapper wrapper = new ValueWrapper("one");
		
		wrapper.add(2);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testInvalidParse1() {
		ValueWrapper wrapper = new ValueWrapper(2);
		
		wrapper.add("one");
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testInvalidParse2() {
		ValueWrapper wrapper = new ValueWrapper("one");
		
		wrapper.add("two");
	}
	
	@Test (expected = RuntimeException.class)
	public void testIsArithmeticType0() {
		ValueWrapper wrapper = new ValueWrapper(new Object());
		
		wrapper.add(5);
	}
	
	@Test (expected = RuntimeException.class)
	public void testIsArithmeticType1() {
		ValueWrapper wrapper = new ValueWrapper(3);
		
		wrapper.add(new Object());
	}
	
	@Test (expected = RuntimeException.class)
	public void testIsArithmeticType2() {
		ValueWrapper wrapper = new ValueWrapper(new Object());
		
		wrapper.add(new Object());
	}
	
}
