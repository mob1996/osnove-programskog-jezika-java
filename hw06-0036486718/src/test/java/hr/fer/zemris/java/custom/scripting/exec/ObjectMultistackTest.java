package hr.fer.zemris.java.custom.scripting.exec;

import static org.junit.Assert.*;

import org.junit.Test;

import hr.fer.zemris.java.custom.scripting.exec.EmptyStackException;
import hr.fer.zemris.java.custom.scripting.exec.ObjectMultistack;
import hr.fer.zemris.java.custom.scripting.exec.ValueWrapper;

public class ObjectMultistackTest {

	@Test (expected = NullPointerException.class)
	public void testInvalidKey0() {
		ObjectMultistack multiStack = new ObjectMultistack();
		multiStack.push(null, new ValueWrapper(3));
	}
	
	@Test (expected = NullPointerException.class)
	public void testInvalidKey1() {
		ObjectMultistack multiStack = new ObjectMultistack();
		multiStack.peek(null);
	}
	
	@Test (expected = NullPointerException.class)
	public void testInvalidKey2() {
		ObjectMultistack multiStack = new ObjectMultistack();
		multiStack.pop(null);
	}
	
	@Test (expected = NullPointerException.class)
	public void testInvalidKey3() {
		ObjectMultistack multiStack = new ObjectMultistack();
		multiStack.isEmpty(null);
	}
	
	@Test (expected = NullPointerException.class)
	public void testInvalidPush() {
		ObjectMultistack multiStack = new ObjectMultistack();
		multiStack.push("0", null);
	}
	
	@Test (expected = EmptyStackException.class)
	public void testEmptyPop() {
		ObjectMultistack multiStack = new ObjectMultistack();
		multiStack.pop("0");
	}
	
	@Test (expected = EmptyStackException.class)
	public void testEmptyPeek() {
		ObjectMultistack multiStack = new ObjectMultistack();
		multiStack.peek("0");
	}
		
	@Test
	public void testIsEmpty() {
		ObjectMultistack multiStack = new ObjectMultistack();
		
		assertTrue(multiStack.isEmpty("0"));
		assertTrue(multiStack.isEmpty("1"));
		assertTrue(multiStack.isEmpty("2"));
		
		int i = 0;
		multiStack.push("0", new ValueWrapper(i++));
		multiStack.push("0", new ValueWrapper(i++));
		multiStack.push("0", new ValueWrapper(i++));
		
		multiStack.push("1", new ValueWrapper(i++));
		multiStack.push("1", new ValueWrapper(i++));
		
		multiStack.push("2", new ValueWrapper(i++));
		
		assertFalse(multiStack.isEmpty("0"));
		multiStack.pop("0");
		assertFalse(multiStack.isEmpty("0"));
		multiStack.pop("0");
		assertFalse(multiStack.isEmpty("0"));
		multiStack.pop("0");
		
		assertTrue(multiStack.isEmpty("0"));
		
		assertFalse(multiStack.isEmpty("1"));
		multiStack.pop("1");
		assertFalse(multiStack.isEmpty("1"));
		multiStack.pop("1");
		
		assertTrue(multiStack.isEmpty("1"));
		
		assertFalse(multiStack.isEmpty("2"));
		multiStack.pop("2");
		
		assertTrue(multiStack.isEmpty("2"));
	}
	
	@Test
	public void testPeek() {
		ObjectMultistack multiStack = new ObjectMultistack();
		
		int i = 0;
		multiStack.push("0", new ValueWrapper(i++));
		multiStack.push("0", new ValueWrapper(i++));
		multiStack.push("0", new ValueWrapper(i++));
		
		multiStack.push("1", new ValueWrapper(i++));
		multiStack.push("1", new ValueWrapper(i++));
		
		multiStack.push("2", new ValueWrapper(i++));
		
		assertEquals(2, multiStack.peek("0").getValue());
		multiStack.pop("0");
		assertEquals(1, multiStack.peek("0").getValue());
		multiStack.pop("0");
		assertEquals(0, multiStack.peek("0").getValue());
		
		assertEquals(4, multiStack.peek("1").getValue());
		multiStack.pop("1");
		assertEquals(3, multiStack.peek("1").getValue());
		
		
		assertEquals(5, multiStack.peek("2").getValue());
	}
	
	@Test
	public void testPop() {
ObjectMultistack multiStack = new ObjectMultistack();
		
		int i = 0;
		multiStack.push("0", new ValueWrapper(i++));
		multiStack.push("0", new ValueWrapper(i++));
		multiStack.push("0", new ValueWrapper(i++));
		
		multiStack.push("1", new ValueWrapper(i++));
		multiStack.push("1", new ValueWrapper(i++));
		
		multiStack.push("2", new ValueWrapper(i++));
		
		assertEquals(2, multiStack.pop("0").getValue());
		assertEquals(1, multiStack.pop("0").getValue());
		assertEquals(0, multiStack.pop("0").getValue());
		
		assertEquals(4, multiStack.pop("1").getValue());
		assertEquals(3, multiStack.pop("1").getValue());
		
		assertEquals(5, multiStack.pop("2").getValue());
		
		assertTrue(multiStack.isEmpty("0"));
		assertTrue(multiStack.isEmpty("1"));
		assertTrue(multiStack.isEmpty("2"));
	}
}
