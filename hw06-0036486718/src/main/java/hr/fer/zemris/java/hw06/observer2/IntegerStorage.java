package hr.fer.zemris.java.hw06.observer2;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * Class is used as a storage for int value. Class also provides option to
 * monitor changes in state through various Observers.
 * 
 * @author Luka Mijić
 *
 */
public class IntegerStorage {

	/**
	 * Value that is stored
	 */
	private int value;

	/**
	 * List of observers
	 */
	private List<IntegerStorageObserver> observers;

	/**
	 * Constructor that creates new IntegerStorage
	 * 
	 * @param initialValue
	 *            is used as initial value of value property
	 */
	public IntegerStorage(int initialValue) {
		this.value = initialValue;
	}

	/**
	 * Method is used to add observer to list of observers.
	 * 
	 * @param observer
	 *            that is added to list
	 */
	public void addObserver(IntegerStorageObserver observer) {
		Objects.requireNonNull(observer, "Observer parameter can't be null reference.");

		if (observers == null) {
			observers = new ArrayList<>();
		}

		observers.add(observer);
	}

	/**
	 * Method removes observer from list of observers if it is stored in list.
	 * 
	 * @param observer
	 *            that is being removed
	 */
	public void removeObserver(IntegerStorageObserver observer) {
		observers.remove(observer);
	}

	/**
	 * Method cleares observer list from all observers.
	 */
	public void clearObservers() {
		observers.clear();
	}

	/**
	 * @return value of value property
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Method sets value property to new value and also
	 * notifies observers of this change.
	 * @param value that value property is being set to. 
	 */
	public void setValue(int newValue) {
		if (this.value != newValue) {
			IntegerStorageChange change = new IntegerStorageChange(this, this.value, newValue);
			this.value = newValue;
			if (observers != null) {
				IntegerStorageObserver[] observersArray = observers.toArray(new IntegerStorageObserver[observers.size()]);
				for (IntegerStorageObserver observer : observersArray) {
					observer.valueChanged(change);
				}
			}
		}
	}
}
