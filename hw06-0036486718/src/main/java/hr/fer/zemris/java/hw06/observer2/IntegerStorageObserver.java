package hr.fer.zemris.java.hw06.observer2;

/**
 * Interface represents Obeserver of IntergerStorage.
 * @author Luka Mijić
 *
 */
public interface IntegerStorageObserver {

	/**
	 * Method is called when value is changed
	 * @param change
	 */
	void valueChanged(IntegerStorageChange istorage);
}
