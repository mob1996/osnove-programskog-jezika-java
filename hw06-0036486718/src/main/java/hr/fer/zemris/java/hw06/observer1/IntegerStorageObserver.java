package hr.fer.zemris.java.hw06.observer1;

/**
 * Interface represents Obeserver of IntergerStorage.
 * @author Luka Mijić
 *
 */
public interface IntegerStorageObserver {

	/**
	 * Method is called when value is changed
	 * @param istorage
	 */
	void valueChanged(IntegerStorage istorage);
}
