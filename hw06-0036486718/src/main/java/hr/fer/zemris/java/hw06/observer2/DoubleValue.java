package hr.fer.zemris.java.hw06.observer2;

/**
 * Class implements IntegerStorageObserver and
 * doubles value of IntegerStorage (without modifying
 * value property of given IntegerStorage)
 * @author Luka Mijić
 *
 */
public class DoubleValue implements IntegerStorageObserver {

	/**
	 * Used to double value
	 */
	private static int DOUBLE = 2;
	
	/**
	 * Keeps count how many observations
	 * will be observed
	 */
	private int remainingObservations;
	
	/**
	 * Creates new DoubleValue object. 
	 * @param initialObservations sets how many times
	 * 			changes will be observed
	 */
	public DoubleValue(int initialObservations) {
		this.remainingObservations = initialObservations;
	}
	
	/**
	 * Method prints doubled value of IntegerStorage
	 * @param change
	 */
	@Override
	public void valueChanged(IntegerStorageChange change) {
		System.out.println("Double value: " + (DOUBLE * change.getNewValue()));
		this.remainingObservations--;
		
		if(remainingObservations <= 0 ) {
			change.getStorage().removeObserver(this);
		}
		
	}

}
