package hr.fer.zemris.java.hw06.observer2;

/**
 * Class implements IntegerStorageObserver and
 * counts how many times the value of IntegerStorage
 * was changed since adding this observer to observers
 * of IntegerStorage
 * @author Luka Mijić
 *
 */
public class ChangeCounter implements IntegerStorageObserver {

	/**
	 * Variable that stores how many times was value changed
	 */
	private int count = 0;
	
	/**
	 * Method that prints how many times was value changed. 
	 * @param change
	 */
	@Override
	public void valueChanged(IntegerStorageChange change) {
		this.count++;
		System.out.println("Number of values changes since tracking: " + count);
	}

}
