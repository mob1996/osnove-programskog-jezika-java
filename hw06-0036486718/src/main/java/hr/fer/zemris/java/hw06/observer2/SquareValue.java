package hr.fer.zemris.java.hw06.observer2;

/**
 * Class implements IntegerStorageObserver and
 * squares value of IntegerStorage (without modifying
 * value property of given IntegerStorage)
 * @author Luka Mijić
 *
 */
public class SquareValue implements IntegerStorageObserver {

	/**
	 * Method prints squared value of IntegerStorage
	 * @param change
	 */
	@Override
	public void valueChanged(IntegerStorageChange change) {
		int value = change.getNewValue();
		System.out.println("Provided new value: " + value + ", square is " + value * value);
	}

}
