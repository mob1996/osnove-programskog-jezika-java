package hr.fer.zemris.java.hw06.demo2;

import java.util.Iterator;
import java.util.NoSuchElementException;

import static java.lang.Math.sqrt;

/**
 * Collection of prime integer numbers.
 * @author Luka Mijić
 *
 */
public class PrimesCollection implements Iterable<Integer> {

	/**
	 * Number of prime numbers
	 */
	private int numberOfPrimes;
	
	/**
	 * Creates PrimeCollections
	 * @param numberOfPrimes
	 */
	public PrimesCollection(int numberOfPrimes) {
		if(numberOfPrimes < 0) {
			throw new IllegalArgumentException("Number of primes in PrimesCollection must be higher than zero.");
		}
		this.numberOfPrimes = numberOfPrimes;
	}

	/**
	 * @return return implementation of iterator
	 */
	@Override
	public Iterator<Integer> iterator() {
		return new PrimesIterator(numberOfPrimes);
	}
	
	/**
	 * Iterator implementation for PrimesCollection
	 */
	private class PrimesIterator implements Iterator<Integer> {

		/**
		 * How much numbers can be generated
		 */
		private int numbersLeft;
		
		/**
		 * Current number
		 */
		private int current = 1;
		
		/**
		 * Creates new PrimesIterator
		 * @param numbersLeft
		 */
		private PrimesIterator(int numbersLeft) {
			this.numbersLeft = numbersLeft;
		}
		
		/**
		 * Checks if there are prime numbers avaliable
		 */
		@Override
		public boolean hasNext() {
			return numbersLeft > 0;
		}

		/**
		 * Return next prime number
		 */
		@Override
		public Integer next() {
			if(numbersLeft <= 0) throw new NoSuchElementException("No more primes avaliable.");
			while(!isPrime(++current));
			numbersLeft--;
			return current;
		}
		
		/**
		 * Check if number is prime number
		 * @param number that is being checked
		 * @return true if number is prime, otherwise
		 * 			return false
		 */
		private boolean isPrime(int number) {
			if(number == 2) return true;
			
			double lastCheck = sqrt(number);
			for(int i = 2; i <= lastCheck; i++) {
				if(number % i == 0) return false;
			}
			
			return true;
		}
	}
}
