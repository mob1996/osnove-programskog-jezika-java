package hr.fer.zemris.java.hw06.demo4;

import java.util.Objects;
import java.util.function.Predicate;

/**
 * Class represents one record of Student
 * @author Luka Mijić
 *
 */
public class StudentRecord {

	/**
	 * Number of properties StudentRecord stores
	 */
	private static final int PROPERTY_NUMBER = 7;
	
	/**
	 * Valid length of jmbag property
	 */
	private static final int JMBAG_LENGTH = 10;
	
	/**
	 * Predicate that checkis if jmbag is valid
	 */
	private static final Predicate<String> VALID_JMBAG;
	
	/**
	 * Predicate that check if name is valid
	 */
	private static final Predicate<String> VALID_NAME;
	
	/**
	 * Predicate that checks if score is valid
	 */
	private static final Predicate<Double> VALID_SCORE;
	
	/**
	 * Predicate that check is grade is valid
	 */
	private static final Predicate<Integer> VALID_GRADE;
	
	/**
	 * Predicate that checks if given number of parameters for 
	 * parsing student record is valid
	 */
	private static final Predicate<Integer> VALID_PARAMETERS_NUMBER;
	
	static {
		VALID_JMBAG = new Predicate<>() {

			@Override
			public boolean test(String jmbag) {
				if(jmbag.length() != JMBAG_LENGTH) return false;
				
				for(Character c:jmbag.toCharArray()) {
					if(!Character.isDigit(c)) return false;
				}
				
				return true;
			}
		};
		
		VALID_NAME = new Predicate<>() {
			@Override
			public boolean test(String name) {
				 for(Character c:name.toCharArray()) {
						if(!Character.isLetter(c) && c != ' ' && !Character.isDigit(c)) return false;
					}
				return true;
			}
		};
		
		VALID_SCORE = (score) -> score >= 0;
		
		VALID_GRADE  = (grade) -> grade >= 1 && grade <=5;
		
		VALID_PARAMETERS_NUMBER = (number) -> PROPERTY_NUMBER == number;
	}
	
	/**
	 * Unique Student identifier
	 */
	private String jmbag;
	
	/**
	 * Last name of Student
	 */
	private String lastName;
	
	/**
	 * First name of student
	 */
	private String firstName;
	
	/**
	 * Score of mid term exam
	 */
	private double midTermScore;
	
	/**
	 * Score of final exam
	 */
	private double finalScore;
	
	/**
	 * Score from laboratory experiments
	 */
	private double labScore;
	
	/**
	 * Grade
	 */
	private int grade;

	/**
	 * Creates new student record
	 * @param jmbag of student
	 * @param lastName of student 
	 * @param firstName of student
	 * @param midTermScore of student
	 * @param finalScore of student
	 * @param labScore of student
	 * @param grade of student
	 * @throws NullPointerException if jmbag, lastName or firstName are null references
	 * @throws IllegalArgumentException if midTermScore, finalScore and labScore are not positive values
	 * 					or if grade is not element of [1, 5] or if jmbag is not 10 chars long and if it contains
	 * 					anything except numbers or if last name and first name contain any nonletter except ' '
	 */
	public StudentRecord(String jmbag, String lastName, String firstName, double midTermScore, double finalScore,
			double labScore, int grade) {
		this.jmbag = Objects.requireNonNull(jmbag, "jmbag can't be null reference.");
		this.lastName = Objects.requireNonNull(lastName, "Last name can't be null reference.");
		this.firstName = Objects.requireNonNull(firstName, "First name can't be null reference");
		
		isValidArgument(jmbag, VALID_JMBAG, "jmbag must be 10 characters long with only digits.");
		isValidArgument(lastName, VALID_NAME, "Last name must have only letters and blank space.");
		isValidArgument(firstName, VALID_NAME, "First name must have only letters and blank space.");
		isValidArgument(midTermScore, VALID_SCORE, "Midterm score must be positive number.");
		isValidArgument(finalScore, VALID_SCORE, "Score from final exam must be positive number.");
		isValidArgument(labScore, VALID_SCORE, "Score from laboratory experiments must be positive number.");
		isValidArgument(grade, VALID_GRADE, "Grade must be in set [1, 5].");
		
		this.midTermScore = midTermScore;
		this.finalScore = finalScore;
		this.labScore = labScore;
		this.grade = grade;
	}

	/**
	 * @return student's jmbag
	 */
	public String getJmbag() {
		return jmbag;
	}

	/**
	 * @return student's last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return student's first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return student's midterm score
	 */
	public double getMidTermScore() {
		return midTermScore;
	}

	/**
	 * @return student's score from final exam
	 */
	public double getFinalScore() {
		return finalScore;
	}

	/**
	 * @return student's score from laboratory experiments
	 */
	public double getLabScore() {
		return labScore;
	}

	/**
	 * @return student's grade
	 */
	public int getGrade() {
		return grade;
	}
	
	/**
	 * Method is used to check if arg satisfies
	 * given predicate.
	 * @param arg that is checked
	 * @param predicate used for testing arg
	 * @param message that is used if exception is thrown
	 * @throws IllegalArgumentException if argument is not valid
	 */
	private static <T> void isValidArgument(T arg, Predicate<T> predicate, String message) {
		if(!predicate.test(arg)) throw new IllegalArgumentException(message);
 	}
	
	/**
	 * @return total score combining mid term score,
	 * 		score from final exam and lab score
	 */
	public double getTotalScore() {
		return midTermScore + finalScore + labScore;
	}
	
	/**
	 * Parses StudentRecord from string where properties
	 * are divided by \t
	 * @param record that is parsed
	 * @return new StudentRecord
	 * @throws IllegalArgumentException if there are not enough parameters
	 * 				or if those parameters are not valid
	 */
	public static StudentRecord parse(String record) {
		String[] values = record.trim().split("\\t+");
		
		isValidArgument(values.length, VALID_PARAMETERS_NUMBER
				, "Need " + PROPERTY_NUMBER + " values to parse student (properties are divided by tab)."
						+ " Was " + values.length + ".\nValue was " + record.trim() + ".");
		
		return new StudentRecord(
				values[0].trim(), values[1].trim()
				, values[2].trim(), parseDouble(values[3].trim(), "Must be parsable as double. Was" + values[3].trim())
				, parseDouble(values[4].trim(), "Must be parsable as double. Was" + values[4].trim())
				, parseDouble(values[5].trim(), "Must be parsable as double. Was" + values[5].trim())
				, parseInt(values[6].trim(), "Must be parsable as int. Was" + values[6].trim()));
	}
	
	/**
	 * Method is used for parsing double from string
	 * @param parse is string that is parsed
	 * @param message that is used if exception happens
	 * @return parsed double
	 * @throws IllegalArgumentException if parse can't be parsed as double
	 */
	private static double parseDouble(String parse, String message) {
		try {
			return Double.valueOf(parse);
		} catch (NumberFormatException exc) {
			throw new IllegalArgumentException(message);
		}
	}
	
	/**
	 * Method is used for parsing int from string
	 * @param parse is string that is parsed
	 * @param message that is used if exception happens
	 * @return parsed int
	 * @throws IllegalArgumentException if parse can't be parsed as int
	 */
	private static int parseInt(String parse, String message) {
		try {
			return Integer.valueOf(parse);
		} catch (NumberFormatException exc) {
			throw new IllegalArgumentException(message);
		}
	}
	
	/**
	 * @return String representation of StudentRecord that can
	 * 			be parsed again into StudentRecord
	 */
	@Override
	public String toString() {
		return jmbag + "\t" + lastName + "\t" + firstName + "\t" + midTermScore
				+ "\t" + finalScore + "\t" + labScore + "\t" + grade;
	}
	
}
