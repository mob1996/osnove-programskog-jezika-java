package hr.fer.zemris.java.hw06.observer1;

/**
 * Class implements IntegerStorageObserver and
 * squares value of IntegerStorage (without modifying
 * value property of given IntegerStorage)
 * @author Luka Mijić
 *
 */
public class SquareValue implements IntegerStorageObserver {

	/**
	 * Method prints squared value of IntegerStorage
	 */
	@Override
	public void valueChanged(IntegerStorage istorage) {
		int value = istorage.getValue();
		System.out.println("Provided new value: " + value + ", square is " + value * value);
	}

}
