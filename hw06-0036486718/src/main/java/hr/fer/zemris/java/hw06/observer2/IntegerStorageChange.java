package hr.fer.zemris.java.hw06.observer2;

/**
 * Class represents change that has happened in
 * IntegerStorage class
 * @author Luka Mijić
 *
 */
public class IntegerStorageChange {

	/**
	 * Reference to IntegerStorage
	 */
	private IntegerStorage storage;

	/**
	 * Old value 
	 */
	private int oldValue;
	
	/**
	 * New value
	 */
	private int newValue;
	
	/**
	 * Creates new IntegerStorageChange
	 * @param storage 
	 * @param oldValue
	 * @param newValue
	 */
	public IntegerStorageChange(IntegerStorage storage, int oldValue, int newValue) {
		super();
		this.storage = storage;
		this.oldValue = oldValue;
		this.newValue = newValue;
	}


	/**
	 * @return IntegerStorage
	 */
	public IntegerStorage getStorage() {
		return storage;
	}


	/**
	 * @return old value
	 */
	public int getOldValue() {
		return oldValue;
	}

	/**
	 * @return new value
	 */
	public int getNewValue() {
		return newValue;
	}
	
}
