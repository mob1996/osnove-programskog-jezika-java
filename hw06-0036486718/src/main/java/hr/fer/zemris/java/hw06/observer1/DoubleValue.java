package hr.fer.zemris.java.hw06.observer1;

/**
 * Class implements IntegerStorageObserver and
 * doubles value of IntegerStorage (without modifying
 * value property of given IntegerStorage)
 * @author Luka Mijić
 *
 */
public class DoubleValue implements IntegerStorageObserver {

	/**
	 * Used to double value
	 */
	private static int DOUBLE = 2;
	
	/**
	 * Keeps count how many observations
	 * will be observed
	 */
	private int remainingObservations;
	
	/**
	 * Creates new DoubleValue object. 
	 * @param initialObservations sets how many times
	 * 			changes will be observed
	 */
	public DoubleValue(int initialObservations) {
		this.remainingObservations = initialObservations;
	}
	
	/**
	 * Method prints doubled value of IntegerStorage
	 */
	@Override
	public void valueChanged(IntegerStorage istorage) {
		System.out.println("Double value: " + (DOUBLE * istorage.getValue()));
		this.remainingObservations--;
		
		if(remainingObservations <= 0) {
			istorage.removeObserver(this);
		}
		
	}

}
