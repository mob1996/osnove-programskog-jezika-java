package hr.fer.zemris.java.hw06.demo4;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Class is demonstration that StudentRecord works and
 * that I posses certain knowledge of streams. 
 * @author Luka Mijić
 *
 */
public class StudentDemo {
	
	private static String RELATIVE_PATH = "src/main/resources/studenti.txt";
	
	public static void main(String[] args) {

		try {
			List<StudentRecord> records = convert(Files.readAllLines(Paths.get(RELATIVE_PATH), StandardCharsets.UTF_8));
			
			System.out.println("Broj studenata koji su dobili više od 25 bodova u sumi MI+ZI+LAB: " + vratiBodovaViseOd25(records));
			System.out.println("Broj odlikaša: " + vratiBrojOdlikaša(records));
			printStudentRecords(vratiListuOdlikasa(records));
			System.out.println("+---------------------------------------------------------------------------+");
			printStudentRecords(vratiSortiranuListuOdlikasa(records));
			System.out.println("+---------------------------------------------------------------------------+");
			System.out.println("Broj nepolozenih: " + vratiPopisNepolozenih(records).size());
			System.out.println("+---------------------------------------------------------------------------+");
			razvrstajStudentePoOcjenama(records).forEach((key, list) -> System.out.println(key + ": " + list.size()));
			System.out.println("+---------------------------------------------------------------------------+");
			vratiBrojStudenataPoOcjenama(records).forEach((key, num) -> System.out.println(key + ": " + num));
			System.out.println("+---------------------------------------------------------------------------+");
			razvrstajProlazPad(records).forEach((key, members) -> System.out.println(key + ": " + members.size()));
		}  catch(FileNotFoundException exc) {
			System.err.println("Given file doesnt exist.");
		} catch(IOException exc) {
			System.err.println("IOException happened.");
			exc.printStackTrace();
		}
	}
	
	/**
	 * @param records
	 * @return number of students whose total score is over 25
	 */
	public static long vratiBodovaViseOd25(List<StudentRecord> records) {
		return records.stream()
				.filter((r) -> r.getTotalScore() > 25)
				.count();
	}
	
	/**
	 * @param records
	 * @return number of people that passed subject with grade 5
	 */
	public static long vratiBrojOdlikaša(List<StudentRecord> records) {
		return records.stream()
				.filter((r) -> r.getGrade() == 5)
				.count();
	}
	
	/**
	 * @param records
	 * @return return unsorted list of students that recieved grade 5.
	 */
	public static List<StudentRecord> vratiListuOdlikasa(List<StudentRecord> records){
		return records.stream()
				.filter((r) -> r.getGrade() == 5)
				.collect(Collectors.toList());
	}
	
	/**
	 * @param records
	 * @return sorted list of students who passed subject with grade 5. Student with highest score is at top. 
	 */
	public static List<StudentRecord> vratiSortiranuListuOdlikasa(List<StudentRecord> records) {
		return records.stream()
				.filter((r) -> r.getGrade() == 5)
				.sorted((r1, r2) -> Double.valueOf(r2.getTotalScore()).compareTo(Double.valueOf(r1.getTotalScore())))
				.collect(Collectors.toList());
	}
	
	/**
	 * @param records
	 * @return list of students who failed subject
	 */
	public static List<StudentRecord> vratiPopisNepolozenih(List<StudentRecord> records){
		return records.stream()
				.filter((r) -> r.getGrade() == 1)
				.sorted((r1, r2) -> r1.getJmbag().compareTo(r2.getJmbag()))
				.collect(Collectors.toList());
	}
	
	/**
	 * Method creates map that splits students into list according to which
	 * grade they got.
	 * @param records
	 * @return map that uses grades and keys and to each key there is list of students
	 * 			that recieved that grade associated to it. 
	 */
	public static Map<Integer, List<StudentRecord>> razvrstajStudentePoOcjenama(List<StudentRecord> records){
		return records.stream()
				.collect(Collectors.groupingBy(StudentRecord::getGrade));
	}
	
	/**
	 * Method creates map that stores how many people recieved each grade.
	 * @param records
	 * @return Map that uses grades as key and to each key is attributed how many
	 * 			studens recieved that grade
	 */
	public static Map<Integer, Integer> vratiBrojStudenataPoOcjenama(List<StudentRecord> records){
		Function<StudentRecord, Integer> keyMapper = StudentRecord::getGrade;
		Function<StudentRecord, Integer> valueMapper = (r) -> Integer.valueOf(1); //if key is seen for first time, map record to 1
		BinaryOperator<Integer> mergeFunction = (v1, v2) -> v1.intValue() + v2.intValue(); //if key is already mapped take current value and
		return records.stream()																				//add 1 to it
				.collect(Collectors.toMap(keyMapper, valueMapper, mergeFunction));
	}
	
	/**
	 * @param records
	 * @return Map that contains list of students 
	 * 			who passed subject associated to key true, and list of
	 * 			students who failed associated to key false
	 */
	public static Map<Boolean, List<StudentRecord>> razvrstajProlazPad(List<StudentRecord> records){
		return records.stream()
				.collect(Collectors.partitioningBy(r -> r.getGrade() != 1));
	}
	
	/**
	 * Method converts list of String lines and converts it
	 * into list of student records
	 * @param lines
	 * @return list of student records
	 */
	public static List<StudentRecord> convert(List<String> lines){
		List<StudentRecord> records = new LinkedList<>();
		
		for(String strRecord:lines) {
			records.add(StudentRecord.parse(strRecord));
		}
		
		return records;
	}
	
	/**
	 * Method takes list of Student records and prints it
	 * @param records
	 */
	public static void printStudentRecords(List<StudentRecord> records) {
		for(StudentRecord record:records) {
			System.out.println(record);
		}
	}
	
	
	
}
