package hr.fer.zemris.math.lexer;
import static org.junit.Assert.*;

import org.junit.Test;

public class ComplexLexerTest {

	private static final double EPSILON = 1E-4;
	
	@Test (expected = LexerException.class)
	public void testNextTokenAfterEof() {
		ComplexLexer l = new ComplexLexer("");
		ComplexToken token = l.nextToken();
		
		assertEquals(ComplexTokenType.EOF, token.getType());
		assertNull(token.getValue());
		
		l.nextToken();
	}
	
	@Test
	public void testValidOneRealsPositiveWithoutPlus() {
		ComplexLexer l = new ComplexLexer("2.45");
		l.nextToken();
		
		assertEquals(ComplexTokenType.REAL, l.getToken().getType());
		assertEquals(2.45, l.getToken().getValue(), EPSILON);
		
		l.nextToken();
		
		assertEquals(ComplexTokenType.EOF,l.getToken().getType());
		assertNull(l.getToken().getValue());
	}
	
	@Test
	public void testValidOneRealsWithPlus() {
		ComplexLexer l = new ComplexLexer("+   2.45");
		l.nextToken();
		
		assertEquals(ComplexTokenType.REAL, l.getToken().getType());
		assertEquals(2.45, l.getToken().getValue(), EPSILON);
		
		l.nextToken();
		
		assertEquals(ComplexTokenType.EOF,l.getToken().getType());
		assertNull(l.getToken().getValue());
	}
	
	@Test
	public void testValidOneRealsWithMinus() {
		ComplexLexer l = new ComplexLexer("-   2.45");
		l.nextToken();
		
		assertEquals(ComplexTokenType.REAL, l.getToken().getType());
		assertEquals(-2.45, l.getToken().getValue(), EPSILON);
		
		l.nextToken();
		
		assertEquals(ComplexTokenType.EOF,l.getToken().getType());
		assertNull(l.getToken().getValue());
	}
	
	@Test
	public void testValidOneImaginaryPositiveWithoutPlus() {
		ComplexLexer l = new ComplexLexer("i2.45");
		l.nextToken();
		
		assertEquals(ComplexTokenType.IMAGINARY, l.getToken().getType());
		assertEquals(2.45, l.getToken().getValue(), EPSILON);
		
		l.nextToken();
		
		assertEquals(ComplexTokenType.EOF,l.getToken().getType());
		assertNull(l.getToken().getValue());		
	}
	
	@Test
	public void testValidOneImaginaryWithPlus() {
		ComplexLexer l = new ComplexLexer("+   i2.45");
		l.nextToken();
		
		assertEquals(ComplexTokenType.IMAGINARY, l.getToken().getType());
		assertEquals(2.45, l.getToken().getValue(), EPSILON);
		
		l.nextToken();
		
		assertEquals(ComplexTokenType.EOF,l.getToken().getType());
		assertNull(l.getToken().getValue());			
	}
	
	@Test
	public void testValidOneImaginaryWithMinus() {
		ComplexLexer l = new ComplexLexer("-   i2.45");
		l.nextToken();
		
		assertEquals(ComplexTokenType.IMAGINARY, l.getToken().getType());
		assertEquals(-2.45, l.getToken().getValue(), EPSILON);
		
		l.nextToken();
		
		assertEquals(ComplexTokenType.EOF,l.getToken().getType());
		assertNull(l.getToken().getValue());			
	}
	
	@Test
	public void testValidOneImaginaryWithOnlyI() {
		ComplexLexer l = new ComplexLexer("   i   ");
		l.nextToken();
		
		assertEquals(ComplexTokenType.IMAGINARY, l.getToken().getType());
		assertEquals(1, l.getToken().getValue(), EPSILON);
		
		l.nextToken();
		
		assertEquals(ComplexTokenType.EOF,l.getToken().getType());
		assertNull(l.getToken().getValue());		
	}
	
	@Test
	public void testValidOneImaginaryWithOnlyMinusI() {
		ComplexLexer l = new ComplexLexer(" -  i   ");
		l.nextToken();
		
		assertEquals(ComplexTokenType.IMAGINARY, l.getToken().getType());
		assertEquals(-1, l.getToken().getValue(), EPSILON);
		
		l.nextToken();
		
		assertEquals(ComplexTokenType.EOF,l.getToken().getType());
		assertNull(l.getToken().getValue());		
	}
	
	@Test
	public void testValidOneImaginaryWithOnlyPlusI() {
		ComplexLexer l = new ComplexLexer(" +  i   ");
		l.nextToken();
		
		assertEquals(ComplexTokenType.IMAGINARY, l.getToken().getType());
		assertEquals(1, l.getToken().getValue(), EPSILON);
		
		l.nextToken();
		
		assertEquals(ComplexTokenType.EOF,l.getToken().getType());
		assertNull(l.getToken().getValue());		
	}
	
	@Test
	public void testComplexInput() {
		String expression = "2\t-i0 + 4.25 - i25+i7.5 + 2 - i";
		ComplexLexer l = new ComplexLexer(expression);
		
		l.nextToken();
		
		assertEquals(ComplexTokenType.REAL, l.getToken().getType());
		assertEquals(2, l.getToken().getValue(), EPSILON);
		
		l.nextToken();
		
		assertEquals(ComplexTokenType.IMAGINARY, l.getToken().getType());
		assertEquals(0, l.getToken().getValue(), EPSILON);
		
		l.nextToken();
		
		assertEquals(ComplexTokenType.REAL, l.getToken().getType());
		assertEquals(4.25, l.getToken().getValue(), EPSILON);
		
		l.nextToken();
		
		assertEquals(ComplexTokenType.IMAGINARY, l.getToken().getType());
		assertEquals(-25, l.getToken().getValue(), EPSILON);
		
		l.nextToken();
		
		assertEquals(ComplexTokenType.IMAGINARY, l.getToken().getType());
		assertEquals(7.5, l.getToken().getValue(), EPSILON);
		
		l.nextToken();
		
		assertEquals(ComplexTokenType.REAL, l.getToken().getType());
		assertEquals(2, l.getToken().getValue(), EPSILON);
		
		l.nextToken();
		
		assertEquals(ComplexTokenType.IMAGINARY, l.getToken().getType());
		assertEquals(-1, l.getToken().getValue(), EPSILON);
		
		l.nextToken();
		
		assertEquals(ComplexTokenType.EOF,l.getToken().getType());
		assertNull(l.getToken().getValue());
	}
	
	@Test (expected = LexerException.class)
	public void testInvalid0() {
		ComplexLexer l = new ComplexLexer("-");
		l.nextToken();
	}
	
	@Test (expected = LexerException.class)
	public void testInvalid1() {
		ComplexLexer l = new ComplexLexer("-2 45");
		l.nextToken();
		l.nextToken();
	}
	
	@Test (expected = LexerException.class)
	public void testInvalid2() {
		ComplexLexer l = new ComplexLexer("-2 i");
		l.nextToken();
		l.nextToken();
	}
	
	@Test (expected = LexerException.class)
	public void testInvalid3() {
		ComplexLexer l = new ComplexLexer("fds");
		l.nextToken();
	}
	
	@Test (expected = LexerException.class)
	public void testInvalid4() {
		ComplexLexer l = new ComplexLexer("29+i+fds");
		l.nextToken();
		l.nextToken();
		l.nextToken();
	}
}
