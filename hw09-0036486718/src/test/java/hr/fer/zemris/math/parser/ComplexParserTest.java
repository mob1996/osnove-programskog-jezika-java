package hr.fer.zemris.math.parser;

import static org.junit.Assert.*;

import org.junit.Test;

public class ComplexParserTest {

	private static final double EPSILON = 1E-4;
	
	@Test (expected = IllegalArgumentException.class)
	public void testEmptyInput0() {
		new ComplexParser("");
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testEmptyInput1() {
		new ComplexParser("    ");
	}
	
	@Test (expected = ParserException.class)
	public void testInvalidInput0() {
		new ComplexParser("-fa");
	}
	
	@Test (expected = ParserException.class)
	public void testInvalidInput1() {
		new ComplexParser("-2 - ");
	}
	
	@Test
	public void testValidInput() {
		ComplexParser p = new ComplexParser("3+i1.5+    4 - i5");
		assertEquals(7, p.getParsedComplex().getReal(), EPSILON);
		assertEquals(-3.5, p.getParsedComplex().getImaginary(), EPSILON);
	}
	
	
}
