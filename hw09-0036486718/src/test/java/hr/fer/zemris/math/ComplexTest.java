package hr.fer.zemris.math;

import static org.junit.Assert.*;

import java.util.List;

import static java.lang.Math.sqrt;

import org.junit.Assert;
import org.junit.Test;

public class ComplexTest {

	private static double THRESHOLD = 10E-4;
	
	@Test
	public void testEmptyConstructor() {
		Complex c = new Complex();
		assertEquals(0, c.getReal(), THRESHOLD);
		assertEquals(0, c.getImaginary(), THRESHOLD);
	}
	
	@Test
	public void testConstructor() {
		Complex c = new Complex(5, -2);
		assertEquals(5, c.getReal(), THRESHOLD);
		assertEquals(-2, c.getImaginary(), THRESHOLD);
		
		c = new Complex(-10.2153, 0.325421);
		assertEquals(-10.2153, c.getReal(), THRESHOLD);
		assertEquals(0.325421, c.getImaginary(), THRESHOLD);
	}
	
	@Test
	public void testModule() {
		Complex c = new Complex(4, -2);
		assertEquals(sqrt(20), c.module(), THRESHOLD);
		
		c = new Complex(2.24, -0.25);
		assertEquals(sqrt(2.24 * 2.24  + 0.25 * 0.25), c.module(), THRESHOLD);
	}
	
	@Test (expected = NullPointerException.class)
	public void testAddNull(){
		Complex.ZERO.add(null);
	}
	
	@Test
	public void testAdd() {
		Complex c = Complex.ZERO.add(new Complex(5, 3));
		assertEquals(5, c.getReal(), THRESHOLD);
		assertEquals(3, c.getImaginary(), THRESHOLD);
		
		c = new Complex(-2.25, 20.32).add(new Complex(-0.5, 4.18));
		assertEquals(-2.75, c.getReal(), THRESHOLD);
		assertEquals(24.5, c.getImaginary(), THRESHOLD);
	}
	
	@Test (expected = NullPointerException.class)
	public void testSubNull(){
		Complex.ZERO.sub(null);
	}
	
	@Test
	public void testSub() {
		Complex c = Complex.ZERO.sub(new Complex(5, 3));
		assertEquals(-5, c.getReal(), THRESHOLD);
		assertEquals(-3, c.getImaginary(), THRESHOLD);
		
		c = new Complex(-2.25, 20.32).sub(new Complex(-0.5, 4.18));
		assertEquals(-1.75, c.getReal(), THRESHOLD);
		assertEquals(16.14, c.getImaginary(), THRESHOLD);
	}
	
	@Test (expected = NullPointerException.class)
	public void testMulNull(){
		Complex.ZERO.mul(null);
	}
	
	@Test
	public void testMul() {
		Complex c = Complex.ZERO.mul(new Complex(2, 2));
		assertEquals(0, c.getReal(), THRESHOLD);
		assertEquals(0, c.getImaginary(), THRESHOLD);
		
		c = new Complex(-3, 2.5).mul(new Complex(0.5, 0));
		assertEquals(-1.5, c.getReal(), THRESHOLD);
		assertEquals(1.25, c.getImaginary(), THRESHOLD);
		
		c = new Complex(-3, 2.5).mul(new Complex(0, -0.5));
		assertEquals(1.25, c.getReal(), THRESHOLD);
		assertEquals(1.5, c.getImaginary(), THRESHOLD);
		
		c = new Complex(2.40, -4.50).mul(new Complex(-5.5, 3.25));
		assertEquals(1.425, c.getReal(), THRESHOLD);
		assertEquals(32.55, c.getImaginary(), THRESHOLD);
	}
	
	@Test (expected = NullPointerException.class)
	public void testDivNull(){
		Complex.ZERO.div(null);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testDivByZero(){
		Complex.ONE.div(Complex.ZERO);
	}
	
	@Test
	public void testDiv() {
		Complex c = Complex.ZERO.div(new Complex(2, 5));
		assertEquals(0, c.getReal(), THRESHOLD);
		assertEquals(0, c.getImaginary(), THRESHOLD);
		
		c = new Complex(4.25, -2.5).div(new Complex(0.25, 2.5));
		assertEquals(-0.8217821782, c.getReal(), THRESHOLD);
		assertEquals(-1.782178218, c.getImaginary(), THRESHOLD);
		
		c = new Complex(-5.25, -0.2).div(new Complex(-0.234, 10.42));
		assertEquals(-0.00787527291, c.getReal(), THRESHOLD);
		assertEquals(0.5040156251, c.getImaginary(), THRESHOLD);
	}
	
	@Test
	public void testNegate() {
		Complex c = new Complex(2, -5).negate();
		assertEquals(-2, c.getReal(), THRESHOLD);
		assertEquals(5, c.getImaginary(), THRESHOLD);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testNegativePower() {
		new Complex(2, 4).power(-1);
	}
	
	@Test
	public void testPowerZero(){
		Complex c = new Complex(2, 4).power(0);
		assertEquals(1, c.getReal(), THRESHOLD);
		assertEquals(0, c.getImaginary(), THRESHOLD);
	}
	
	@Test
	public void testPower() {
		Complex c = new Complex(2, 4).power(3);
		assertEquals(-88, c.getReal(), THRESHOLD);
		assertEquals(-16, c.getImaginary(), THRESHOLD);
		
		c = new Complex(5.25, 10.47).power(10);
		assertEquals(3129571026.97, c.getReal(), 10E-2);
		assertEquals(-48484728907.2, c.getImaginary(), 10E-1);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testRootsForNegative() {
		new Complex(2, 4).root(-1);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testRootsForZero() {
		new Complex(2, 4).root(0);
	}
	
	@Test
	public void testRoots() {
		List<Complex> roots = new Complex(2, -3).root(3);
		
		Assert.assertEquals(1.4519, roots.get(0).getReal(), THRESHOLD);
		Assert.assertEquals(-0.4934, roots.get(0).getImaginary(), THRESHOLD);
		
		Assert.assertEquals(-0.298628, roots.get(1).getReal(), THRESHOLD);
		Assert.assertEquals(1.504046, roots.get(1).getImaginary(), THRESHOLD);
		
		Assert.assertEquals(-1.1532, roots.get(2).getReal(), THRESHOLD);
		Assert.assertEquals(-1.0106, roots.get(2).getImaginary(), THRESHOLD);
		
	}
		
}
