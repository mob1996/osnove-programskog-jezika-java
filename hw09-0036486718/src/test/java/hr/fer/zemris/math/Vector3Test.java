package hr.fer.zemris.math;

import static org.junit.Assert.*;

import org.junit.Test;

public class Vector3Test {

	private static double THRESHOLD = 10E-4;
	
	@Test
	public void testCreation() {
		Vector3 v1 = new Vector3(2, -3, 0);
		assertEquals(2, v1.getX(), THRESHOLD);
		assertEquals(-3, v1.getY(), THRESHOLD);
		assertEquals(0, v1.getZ(), THRESHOLD);
		
		Vector3 v2 = new Vector3(5, -1000, 2465);
		assertEquals(5, v2.getX(), THRESHOLD);
		assertEquals(-1000, v2.getY(), THRESHOLD);
		assertEquals(2465, v2.getZ(), THRESHOLD);
	}
	
	@Test
	public void testNorm() {
		Vector3 v1 = new Vector3(2, -3, 0);
		assertEquals(3.6055512, v1.norm(), THRESHOLD);
		
		Vector3 v2 = new Vector3(5, -1000, 2465);
		assertEquals(2660.122178, v2.norm(), THRESHOLD);
	}
	
	@Test
	public void testNormalized() {
		Vector3 v1 = new Vector3(5.2, -4, 3.5);
		Vector3 v1Normalized = v1.normalized();
		double v1Norm = v1.norm();
		
		assertEquals(v1.getX()/v1Norm, v1Normalized.getX(), THRESHOLD);
		assertEquals(v1.getY()/v1Norm, v1Normalized.getY(), THRESHOLD);
		assertEquals(v1.getZ()/v1Norm, v1Normalized.getZ(), THRESHOLD);
		
		Vector3 v2 = new Vector3(-12.4, 32.5, 456);
		Vector3 v2Normalized = v2.normalized();
		double v2Norm = v2.norm();
		
		assertEquals(v2.getX()/v2Norm, v2Normalized.getX(), THRESHOLD);
		assertEquals(v2.getY()/v2Norm, v2Normalized.getY(), THRESHOLD);
		assertEquals(v2.getZ()/v2Norm, v2Normalized.getZ(), THRESHOLD);
	}
	
	@Test (expected = NullPointerException.class)
	public void testAddNull() {
		new Vector3(2, 4, 3).add(null);
	}
	
	@Test
	public void testAdd() {
		Vector3 v1 = new Vector3(3, 4, -8);
		Vector3 v2 = new Vector3(-2, 0, 5);
		Vector3 v3 = v1.add(v2);
		
		assertEquals(1, v3.getX(), THRESHOLD);
		assertEquals(4, v3.getY(), THRESHOLD);
		assertEquals(-3, v3.getZ(), THRESHOLD);
		
		v1 = new Vector3(-4, 3, -10);
		v2 = new Vector3(-7, -5, 12);
		v3 = v1.add(v2);
		
		assertEquals(-11, v3.getX(), THRESHOLD);
		assertEquals(-2, v3.getY(), THRESHOLD);
		assertEquals(2, v3.getZ(), THRESHOLD);
	}
	
	@Test (expected = NullPointerException.class)
	public void testSubNull() {
		new Vector3(2, 4, 3).sub(null);
	}
	
	@Test
	public void testSub() {
		Vector3 v1 = new Vector3(3, 4, -8);
		Vector3 v2 = new Vector3(-2, 0, 5);
		Vector3 v3 = v1.sub(v2);
		
		assertEquals(5, v3.getX(), THRESHOLD);
		assertEquals(4, v3.getY(), THRESHOLD);
		assertEquals(-13, v3.getZ(), THRESHOLD);
		
		v1 = new Vector3(-4, 3, -10);
		v2 = new Vector3(-7, -5, 12);
		v3 = v1.sub(v2);
		
		assertEquals(3, v3.getX(), THRESHOLD);
		assertEquals(8, v3.getY(), THRESHOLD);
		assertEquals(-22, v3.getZ(), THRESHOLD);
	}
	
	@Test (expected = NullPointerException.class)
	public void testDotNull() {
		new Vector3(2, 4, 3).dot(null);
	}
	
	@Test
	public void testDot() {
		Vector3 v1 = new Vector3(3, 4, -8);
		Vector3 v2 = new Vector3(-2, 0, 5);
		
		assertEquals(-46, v1.dot(v2), THRESHOLD);
		
		v1 = new Vector3(2.2, 4.6432, -8.543);
		v2 = new Vector3(-5.23, 10.321, 5.345);
		
		assertEquals(-9.2458678, v1.dot(v2), THRESHOLD);
	}
	
	@Test (expected = NullPointerException.class)
	public void testCrossNull() {
		new Vector3(2, 4, 3).cross(null);
	}
	
	@Test
	public void testCross() {
		Vector3 v1 = new Vector3(3, 4, -8);
		Vector3 v2 = new Vector3(-2, 0, 5);
		Vector3 v3 =  v1.cross(v2);
		assertEquals(20, v3.getX(), THRESHOLD);
		assertEquals(1, v3.getY(), THRESHOLD);
		assertEquals(8, v3.getZ(), THRESHOLD);
		
		v1 = new Vector3(2.2, 4.6432, -8.543);
		v2 = new Vector3(-5.23, 10.321, 5.345);
		v3 =  v1.cross(v2);
		assertEquals(112.990207, v3.getX(), THRESHOLD);
		assertEquals(32.92089, v3.getY(), THRESHOLD);
		assertEquals(46.990136, v3.getZ(), THRESHOLD);
	}
	
	@Test
	public void testScale() {
		Vector3 v1 = new Vector3(5, 2, 1);
		Vector3 v2 = v1.scale(0.5);
		assertEquals(2.5, v2.getX(), THRESHOLD);
		assertEquals(1, v2.getY(), THRESHOLD);
		assertEquals(0.5, v2.getZ(), THRESHOLD);
		
		v1 = new Vector3(5, 2, -1);
		v2 = v1.scale(-2);
		assertEquals(-10, v2.getX(), THRESHOLD);
		assertEquals(-4, v2.getY(), THRESHOLD);
		assertEquals(2, v2.getZ(), THRESHOLD);
	}
	
	@Test (expected = NullPointerException.class)
	public void testCosAngleNull() {
		new Vector3(2, 4, 3).cosAngle(null);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testCosAngleOtherNormZero() {
		new Vector3(2, 4, 3).cosAngle(new Vector3(0, 0, 0));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testCosAngleFirstNormZero() {
		new Vector3(0, 0, 0).cosAngle(new Vector3(5, 0, 2));
	}
	
	@Test
	public void testCosAngle() {
		Vector3 v1 = new Vector3(2, 4, 5);
		Vector3 v2 = new Vector3(-3, 4, 0);
		double cosAngle = v1.cosAngle(v2);
		
		assertEquals(0.29814239699997197, cosAngle, THRESHOLD);
		
		v1 = new Vector3(2.5, -3.2, 10.25);
		v2 = new Vector3(-25.5, 20.3, 5.5);
		cosAngle = v1.cosAngle(v2);
		
		assertEquals(-0.1984896780912384, cosAngle, THRESHOLD);
	}
	
	@Test
	public void testToArray() {
		Vector3 v1 = new Vector3(5, -10, 4);
		double[] expectedArray = {v1.getX(), v1.getY(), v1.getZ()};
		assertArrayEquals(expectedArray, v1.toArray(), THRESHOLD);
		
		Vector3 v2 = new Vector3(3.5231, -42.432432, 5.53215);
		double[] expectedArray2 = {v2.getX(), v2.getY(), v2.getZ()};
		assertArrayEquals(expectedArray2, v2.toArray(), THRESHOLD);
	}
}
