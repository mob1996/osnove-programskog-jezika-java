package hr.fer.zemris.java.raytracer.model;

import java.util.Objects;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

/**
 * Class extedns {@link GraphicalObject} and models sphere.
 * @author Mob
 *
 */
public class Sphere extends GraphicalObject {
	
	/**
	 * Center of sphere
	 */
	private Point3D center;
	
	/**
	 * Radius of sphere
	 */
	private double radius;
	
	/**
	 * Diffuse factor for red color.
	 */
	private double kdr;
	
	/**
	 * Diffuse factor for green color.
	 */
	private double kdg;
	
	/**
	 * Diffuse factor for blue color.
	 */
	private double kdb;
	
	/**
	 * Reflective factor for red color. 
	 */
	private double krr;
	
	/**
	 * Reflective factor for green color. 
	 */
	private double krg;
	
	/**
	 * Reflective factor for blue color. 
	 */
	private double krb;
	
	/**
	 * Shininess constant. For larger value, object seems
	 * more smooth and more mirror like.
	 */
	private double krn;
	
	
	/**
	 * Creates new {@link Sphere} object.
	 * @param center of sphere
	 * @param radius of spehere
	 * @param kdr diffuse factor for red color
	 * @param kdg diffuse factor for green color
	 * @param kdb diffuse factor for blue color
	 * @param krr reflective factor for red color. 
	 * @param krg reflective factor for green color. 
	 * @param krb reflective factor for blue color. 
	 * @param krn shininess constant
	 * @throws NullPointerException if <code>center</code> is <code>null</code> reference.
	 */
	public Sphere(Point3D center, double radius, double kdr, double kdg, double kdb, double krr, double krg,
			double krb, double krn) {
		this.center = Objects.requireNonNull(center, "Center of Sphere must not be null.");
		this.radius = radius;
		this.kdr = kdr;
		this.kdg = kdg;
		this.kdb = kdb;
		this.krr = krr;
		this.krg = krg;
		this.krb = krb;
		this.krn = krn;
	}

	/**
	 * Method is used for calculating the closest {@link RayIntersection}
	 * between {@link Ray} and {@link Sphere}. That {@link RayIntersection}
	 * must be in direction of given {@link Ray}, not behind it.
	 * @return closest {@link RayIntersection}. If intersection doesn't exist
	 * 				or if intersection is not in ray direction return <code>null</code> 
	 */
	@Override
	public RayIntersection findClosestRayIntersection(Ray ray) {
		double a = 1;
		double b = ray.direction.scalarMultiply(2).scalarProduct(ray.start.sub(center));
		double c = pow(ray.start.sub(center).norm(), 2) - pow(this.radius, 2);
		
		double[] quadraticResults = solveQuadratic(a, b, c);
		if(quadraticResults == null) {
			return null;
		}
		if(quadraticResults[0] < 0 && quadraticResults[1] < 0) {
			return null;
		}
		double factor = calculateFactor(quadraticResults);
		if(factor < 0) {
			return null;
		}
		
		Point3D intersectionPoint = ray.start.add(ray.direction.scalarMultiply(factor));
		double distance = intersectionPoint.sub(ray.start).norm();
		boolean isOuter = ray.start.sub(center).norm() > radius;
		return new SphereRayInterception(intersectionPoint, distance, isOuter);
	}

	/**
	 * Method solves quadratix equation.
	 * Form: f(x) = ax^2 + bx + c
	 * @param a factor
	 * @param b factor
	 * @param c factor
	 * @return double array with one value if delta (b^2 - 4*a*c) equals zero,
	 * 			array with two values if delta is higher than zero and null if
	 * 			delta is lower than zero. 
	 */
	private double[] solveQuadratic(double a, double b, double c) {
		double delta = b * b - 4 * a * c;
		double[] results = null;
		if(delta == 0) {
			results = new double[1];
			results[0] = -b / (2 * a);
		} else if(delta > 0) {
			results = new double[2];
			results[0] = (-b + sqrt(delta)) / (2 * a);
			results[1] = (-b - sqrt(delta)) / (2 * a);
		}
		
		return results;
	}
	
	/**
	 * Often vector notation is:
	 * V(d) = START_POINT + d * DIRECTION_VECTOR.
	 * This method calculates valid d for finding {@link Sphere} and
	 * {@link Ray} {@link RayIntersection}. Valid factor must be non negative
	 * value, and also the one closes to zero.
	 * @param factors array of factors
	 * @return return factor with lowest value that is also larger than zero,
	 * 			if method returns negative value that means there is no valid factor.
	 */
	private double calculateFactor(double[] factors) {
		double factor = -1;
		for(int i = 0; i < factors.length; i++) {
			if(factor < 0) {
				factor = factors[i];
			}
			
			if(factors[i] > 0  && factors[i] < factor) {
				factor = factors[i];
			}
		}
		return factor;
	}
	
	/**
	 * Class extends {@link RayIntersection}. It models intersection between
	 * {@link Ray} and {@link Sphere}.
	 * @see RayIntersection for more information
	 * @author Luka Mijić
	 *
	 */
	private class SphereRayInterception extends RayIntersection {

		protected SphereRayInterception(Point3D point, double distance, boolean outer) {
			super(point, distance, outer);
		}

		@Override
		public Point3D getNormal() {
			return this.getPoint().sub(Sphere.this.center).normalize();
		}

		@Override
		public double getKdr() {
			return Sphere.this.kdr;
		}

		@Override
		public double getKdg() {
			return Sphere.this.kdg;
		}

		@Override
		public double getKdb() {
			return Sphere.this.kdb;
		}

		@Override
		public double getKrr() {
			return Sphere.this.krr;
		}

		@Override
		public double getKrg() {
			return Sphere.this.krg;
		}

		@Override
		public double getKrb() {
			return Sphere.this.krb;
		}

		@Override
		public double getKrn() {
			return Sphere.this.krn;
		}
		
	}
}
