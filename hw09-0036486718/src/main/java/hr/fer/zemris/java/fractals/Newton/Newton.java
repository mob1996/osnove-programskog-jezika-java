package hr.fer.zemris.java.fractals.Newton;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import hr.fer.zemris.java.fractals.viewer.FractalViewer;
import hr.fer.zemris.math.Complex;
import hr.fer.zemris.math.ComplexRootedPolynomial;

/**
 * Class provides interface for user to communicate with
 * system.
 * @author Luka Mijić
 *
 */
public class Newton {

	/**
	 * Minimum of roots required.
	 */
	private static int MIN_ROOTS = 2;
	
	/**
	 * Method prompts user to give input.
	 * It reads that input and creates fractal image based
	 * on given input. 
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Welcome to Newton-Raphson iteration-based fractal viewer.");
		System.out.println("Please enter at least two roots, one root per line. Enter 'done' when done.");

		List<Complex> roots = null;
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));) {
			roots = readInput(reader);
		} catch (IOException e) {
			System.out.println("IOException, can't recover. Bye.");
			System.exit(0);
		}

		if(roots.size() < MIN_ROOTS) {
			System.out.println("Need atleast two root inputs.");
			System.exit(0);
		}
		
		ComplexRootedPolynomial rootsPoly = new ComplexRootedPolynomial(roots.toArray(new Complex[roots.size()]));
		FractalViewer.show(new NewtonProducer(rootsPoly, 16*16*16));
	}

	/**
	 * Method is used for reading inputs from users.
	 * Given input is parsed into {@link Complex}.
	 * @param reader that reads from source
	 * @return {@link List} of {@link Complex} roots
	 * @throws IOException
	 */
	private static List<Complex> readInput(BufferedReader reader) throws IOException {
		List<Complex> roots = new ArrayList<>();
		int i = 1;
		while (true) {
			System.out.print("Roots " + i + "> ");
			String line = reader.readLine();
			if (line.trim().equals("done")) {
				break;
			}
			try {
				roots.add(Complex.parse(line));
				i++;
			} catch (IllegalArgumentException exc) {
				System.out.println("Invalid input, try again. " + exc.getMessage());
			}
		}
		return roots;
	}
}
