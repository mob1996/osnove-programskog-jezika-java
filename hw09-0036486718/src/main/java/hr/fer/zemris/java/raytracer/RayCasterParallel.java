package hr.fer.zemris.java.raytracer;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

import hr.fer.zemris.java.raytracer.model.IRayTracerProducer;
import hr.fer.zemris.java.raytracer.model.IRayTracerResultObserver;
import hr.fer.zemris.java.raytracer.model.Point3D;
import hr.fer.zemris.java.raytracer.model.Ray;
import hr.fer.zemris.java.raytracer.model.Scene;
import hr.fer.zemris.java.raytracer.viewer.RayTracerViewer;

/**
 * Class models RayCaster. It casts {@link Ray} from eye to screen.
 * Then it calculates RGB component for each pixel. It uses {@link ForkJoinPool}
 * to paralelize whole process.
 * @author Luka Mijič
 *
 */
public class RayCasterParallel {

	public static void main(String[] args) {
		RayTracerViewer.show(new RayCasterProducer(), new Point3D(10, 0, 0), new Point3D(0, 0, 0),
				new Point3D(0, 0, 10), 20, 20);
	}
	
	/**
	 * @see IRayTracerProducer
	 * @author Luka Mijić
	 *
	 */
	public static class RayCasterProducer implements IRayTracerProducer {

		@Override
		public void produce(Point3D eye, Point3D view, Point3D viewUp, double horizontal, double vertical, int width,
				int height, long requestNo, IRayTracerResultObserver observer) {
			System.out.println("Započinjem izračune...");
			short[] red = new short[width * height];
			short[] green = new short[width * height];
			short[] blue = new short[width * height];
			
			Point3D eyeView = view.sub(eye).normalize();
			Point3D viewUpNormalized = viewUp.normalize();

			Point3D yAxis = viewUpNormalized.sub(eyeView.scalarMultiply(eyeView.scalarProduct(viewUpNormalized)))
					.normalize();
			Point3D xAxis = eyeView.vectorProduct(yAxis).normalize();
			@SuppressWarnings("unused")
			Point3D zAxis = xAxis.vectorProduct(yAxis).normalize();
			
			Point3D xCorner = xAxis.scalarMultiply(horizontal / 2.0).negate();
			Point3D yCorner = yAxis.scalarMultiply(vertical / 2.0);
			Point3D screenCorner = view.add(xCorner.add(yCorner));

			Scene scene = RayTracerViewer.createPredefinedScene();
			
			ForkJoinPool pool = new ForkJoinPool();
			pool.invoke(new RayCasterJob(scene, eye, screenCorner, 
					xAxis, yAxis, width, height, horizontal, vertical, 0, height-1, red, green, blue));
			pool.shutdown();
			System.out.println("Izračuni gotovi...");
			observer.acceptResult(red, green, blue, requestNo);
			System.out.println("Dojava gotova...");
		}
	}
	
	/**
	 * Class extends {@link RecursiveAction} and it models {@link Thread}s task.
	 * @author Luka Mijić
	 *
	 */
	public static class RayCasterJob extends RecursiveAction {

		
		private static final long serialVersionUID = 1L;
		
		/**
		 * Maximum number of rows that can be processed in one task
		 */
		private static final int THRESHOLD = 16;
		
		/**
		 * Scene that models environment
		 */
		private Scene scene;
		
		/**
		 * Location from which ray was cast
		 */
		private Point3D eye;
		
		/**
		 * Corner of the screen
		 */
		private Point3D corner;
		
		/**
		 * i vector
		 */
		private Point3D xAxis;
		
		/**
		 * j vector
		 */
		private Point3D yAxis;
		
		/**
		 * Number of pixels in a row
		 */
		private int width;
		
		/**
		 * Number of pixel rows.
		 */
		private int height;
		
		/**
		 * x Axis maps pixels to [-horizontal, +horizontal]
		 */
		private double horizontal;
		
		/**
		 * y axis maps pixels to [-vertical, +vertical]
		 */
		private double vertical;
		
		/**
		 * Starting row for task
		 */
		private int yMin;
		
		/**
		 * Ending row for task
		 */
		private int yMax;
		
		/**
		 * Stores red values for each pixel
		 */
		private short[] red;
		
		/**
		 * Stores green values for each pixel
		 */
		private short[] green;
		
		/**
		 * Stores blue values for each pixel
		 */
		private short[] blue;
		
		/**
		 * Creates new {@link RayCasterJob}
		 * @param scene models environment
		 * @param eye location from which ray was cast
		 * @param corner of the screen
		 * @param xAxis value of i vector
		 * @param yAxis value of j vector
		 * @param width number of pixels in each row
		 * @param height number of pixel rows
		 * @param horizontal x axis is mapped to [-horizontal, +horizontal]
		 * @param vertical y axis is mapped to [-vertical, +vertical]
		 * @param yMin starting row for task
		 * @param yMax ending row for task
		 * @param red values of each pixel
		 * @param green values of each pixel 
		 * @param blue values of each pixel
		 */
		public RayCasterJob(Scene scene, Point3D eye, Point3D corner, Point3D xAxis, Point3D yAxis, int width, int height, double horizontal,
				double vertical, int yMin, int yMax, short[] red, short[] green, short[] blue) {
			this.scene = scene;
			this.eye = eye;
			this.corner = corner;
			this.xAxis = xAxis;
			this.yAxis = yAxis;
			this.width = width;
			this.height = height;
			this.horizontal = horizontal;
			this.vertical = vertical;
			this.yMin = yMin;
			this.yMax = yMax;
			this.red = red;
			this.green = green;
			this.blue = blue;
		}

		@Override
		protected void compute() {
			if(yMax-yMin+1 <= THRESHOLD) {
				computeDirect();
				return;
			}
			invokeAll(
			new RayCasterJob(scene, eye, corner, xAxis, yAxis, width, height, horizontal, vertical, yMin, yMin+(yMax-yMin)/2, red, green, blue),
			new RayCasterJob(scene, eye, corner, xAxis, yAxis, width, height, horizontal, vertical, yMin+(yMax-yMin)/2 + 1, yMax, red, green, blue)
			);
		}
		
		/**
		 * Method computes values for <code>red</code>,
		 * <code>green</code>, <code>blue</code> array for
		 * indexes [yMin*width, yMax * width -1]
		 */
		private void computeDirect() {
			int offset = yMin * width;
			short rgb[] = new short[3];
			for (int y = yMin; y <= yMax; y++) {
				for (int x = 0; x < width; x++) {
					double screenX = (double) x / (width - 1) * horizontal;
					double screenY = (double) y / (height - 1) * vertical;
					Point3D xPoint = xAxis.scalarMultiply(screenX);
					Point3D yPoint = yAxis.scalarMultiply(screenY).negate();
					Point3D screenPoint = corner.add(xPoint.add(yPoint));
					Ray ray = Ray.fromPoints(eye, screenPoint);
		
					RayUtil.tracer(scene, ray, rgb);
					red[offset] = rgb[0] > 255 ? 255 : rgb[0];
					green[offset] = rgb[1] > 255 ? 255 : rgb[1];
					blue[offset] = rgb[2] > 255 ? 255 : rgb[2];
					offset++;
				}
			}
		}	
	}
}
