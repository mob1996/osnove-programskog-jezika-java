package hr.fer.zemris.java.fractals.Newton;

import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;

import hr.fer.zemris.math.Complex;
import hr.fer.zemris.math.ComplexPolynomial;
import hr.fer.zemris.math.ComplexRootedPolynomial;

/**
 * Class models a job that each {@link Thread} in
 * {@link ExecutorService} has to complete.
 * @author Mob
 *
 */
public class NewtonJob implements Callable<Void> {

	/**
	 * Treshold for modules of {@link Complex}
	 */
	private static final double MODULE_TRESHOLD = 1E-3;
	
	/**
	 * Treshold for {@link Complex} roots.
	 */
	private static final double ROOT_TRESHOLD = 1E-3;

	/**
	 * Minimun value of real component
	 */
	private double reMin;
	
	/**
	 * Maximum value of real component
	 */
	private double reMax;
	
	/**
	 * Minimum value of imaginary component
	 */
	private double imMin;
	
	/**
	 * Maximum value of imaginary component
	 */
	private double imMax;
	
	/**
	 * Width of window
	 */
	private int width;
	
	/**
	 * Height of window
	 */
	private int height;
	
	/**
	 * Starting row for this job
	 */
	private int yMin;
	
	/**
	 * Ending row for this job
	 */
	private int yMax;
	
	/**
	 * Number of iterations
	 */
	private int m;
	
	/**
	 * Data where calculations are stored. 
	 * Offset for each task is width * yMin
	 */
	private short[] data;
	
	/**
	 * {@link Complex} roots
	 */
	private ComplexRootedPolynomial roots;
	
	/**
	 * <code>poly</code> is created from <code>roots</code>
	 * @see {@link ComplexRootedPolynomial#toComplexPolynomial()}
	 */
	private ComplexPolynomial poly;
	
	/**
	 * <code>derived</code> is created by deriving <code>poly</code>
	 * @see {@link ComplexPolynomial#derive()}
	 */
	private ComplexPolynomial derived;

	/**
	 * Initialization of job.
	 * @param reMin is minimum value of real component
	 * @param reMax is maximum value of real component
	 * @param imMin is minimum value of imaginary component
	 * @param imMax is maximum value of imaginary component
	 * @param width of window
	 * @param height of window
	 * @param yMin starting row for job
	 * @param yMax ending row for job
	 * @param m number of iterations
	 * @param data where calculations are stored
	 * @param roots of polynom
	 * @throws NullPointerException if <code>roots</code> or <code>data</code> is null reference.
	 */
	public NewtonJob(double reMin, double reMax, double imMin, double imMax, int width, int height, int yMin, int yMax,
			int m, short[] data, ComplexRootedPolynomial roots) {
		Objects.requireNonNull(data, "Reference to data must not be null reference.");
		Objects.requireNonNull(roots, "'roots' must not be null reference.");
		this.reMin = reMin;
		this.reMax = reMax;
		this.imMin = imMin;
		this.imMax = imMax;
		this.width = width;
		this.height = height;
		this.yMin = yMin;
		this.yMax = yMax;
		this.m = m;
		this.data = data;
		this.roots = roots;
		this.poly = roots.toComplexPolynomial();
		this.derived = poly.derive();
	}

	/**
	 * Method that stores calculations in appropriate
	 * place in <code>data</code>.
	 * @return null
	 */
	@Override
	public Void call() throws Exception {
		int offset = yMin * width;
		for (int y = yMin; y <= yMax; y++) {
			for (int x = 0; x < width; x++) {
				Complex zn = mapToComplex(x, y);
				data[offset++] = calculate(zn);
			}
		}
		return null;
	}

	/**
	 * Method maps given window coordinates
	 * to {@link Complex} value.
	 * @param x coordinate
	 * @param y coordinate
	 * @return {@link Complex}
	 */
	private Complex mapToComplex(int x, int y) {
		double re = (double) x / (width - 1) * (reMax - reMin) + reMin;
		double im =  -((double) y / (height - 1) * (imMax - imMin)) + imMax;
		return new Complex(re, im);
	}

	/**
	 * Calculates int value from {@link Complex}.
	 * @param zn is {@link Complex} for which int is calculated
	 * @return calculated int
	 */
	private short calculate(Complex zn) {
		int i = 0;
		Complex zn1 = null;
		while (i < m) {
			zn1 = zn.sub(poly.apply(zn).div(derived.apply(zn)));
			i++;
			if(zn1.sub(zn).module() <= MODULE_TRESHOLD) break;
			zn = zn1;
		}
		short result = (short) roots.indexOfClosestRootFor(zn1, ROOT_TRESHOLD);
		result = result == -1 ? 0 : result;
		return result;
	}
}
