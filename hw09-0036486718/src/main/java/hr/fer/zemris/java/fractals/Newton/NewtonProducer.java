package hr.fer.zemris.java.fractals.Newton;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;

import hr.fer.zemris.java.fractals.viewer.IFractalProducer;
import hr.fer.zemris.java.fractals.viewer.IFractalResultObserver;
import hr.fer.zemris.math.ComplexRootedPolynomial;

/**
 * Class models {@link IFractalProducer}. It creates {@link ExecutorService}
 * and creates {@link NewtonJob}s for service to compute.
 * @author Luka Mijić
 *
 */
public class NewtonProducer implements IFractalProducer {

	/**
	 * Factory that creates DAEMON {@link Thread}s.
	 */
	private static final ThreadFactory DAEMON_FACTORY = r -> {
		Thread t = new Thread(r);
		t.setDaemon(true);
		return t;
	};
	
	/**
	 * Complex roots for which fractal is created.
	 */
	private ComplexRootedPolynomial roots;
	
	/**
	 * Number of iteration for calculations
	 */
	private int iterationNumber;
	
	/**
	 * Number of available processor cores
	 */
	private int cores;
	
	/**
	 * Service that computes given tasks to it. 
	 */
	private ExecutorService pool;
	
	/**
	 * Initialization of {@link NewtonProducer}.
	 * @param roots for which fractal is created.
	 * @param iterationNumber number of iterations for calculations
	 */
	public NewtonProducer(ComplexRootedPolynomial roots, int iterationNumber) {
		this.roots = roots;
		this.iterationNumber = iterationNumber;
		this.cores = Runtime.getRuntime().availableProcessors();
		this.pool = Executors.newFixedThreadPool(cores, DAEMON_FACTORY);
	}

	@Override
	public void produce(double reMin, double reMax, double imMin, double imMax, int width, int height, long requestNo,
			IFractalResultObserver observer) {
		short[] data = new short[width * height];
		int tracks = 8 * cores;
		int yRange = height / tracks;
		
		List<Future<Void>> results = new ArrayList<>();
		
		for(int i = 0; i < tracks; i++) {
			int yMin = i * yRange;
			int yMax = (i + 1) * yRange - 1;
			if(i == tracks - 1) {
				yMax = height - 1;
			}
			NewtonJob job = new NewtonJob(reMin, reMax, imMin, imMax, 
					width, height, yMin, yMax, iterationNumber, data, roots);
			results.add(pool.submit(job));
		}
		
		for(Future<Void> result:results) {
			try {
				result.get();
			} catch (InterruptedException | ExecutionException e) {
			}
		}
		
		observer.acceptResult(data, (short) (roots.toComplexPolynomial().order() + 1), requestNo);
	}

	

}
