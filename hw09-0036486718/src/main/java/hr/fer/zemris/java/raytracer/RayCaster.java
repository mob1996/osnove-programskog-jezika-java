package hr.fer.zemris.java.raytracer;

import hr.fer.zemris.java.raytracer.model.IRayTracerProducer;
import hr.fer.zemris.java.raytracer.model.IRayTracerResultObserver;
import hr.fer.zemris.java.raytracer.model.Point3D;
import hr.fer.zemris.java.raytracer.model.Ray;
import hr.fer.zemris.java.raytracer.model.Scene;
import hr.fer.zemris.java.raytracer.viewer.RayTracerViewer;

/**
 * Class models RayCaster. It casts {@link Ray} from eye to screen.
 * Then it calculates RGB component for each pixel.
 * @author Luka Mijič
 *
 */
public class RayCaster {

	public static void main(String[] args) {
		RayTracerViewer.show(getIRayTracerProducer(), new Point3D(10, 0, 0), new Point3D(0, 0, 0),
				new Point3D(0, 0, 10), 20, 20);
	}

	private static IRayTracerProducer getIRayTracerProducer() {
		return new IRayTracerProducer() {

			/**
			 * @see IRayTracerProducer
			 */
			@Override
			public void produce(Point3D eye, Point3D view, Point3D viewUp, double horizontal, double vertical,
					int width, int height, long requestNo, IRayTracerResultObserver observer) {
				System.out.println("Započinjem izračune...");
				short[] red = new short[width * height];
				short[] green = new short[width * height];
				short[] blue = new short[width * height];

				Point3D eyeView = view.sub(eye).normalize();
				Point3D viewUpNormalized = viewUp.normalize();

				Point3D yAxis = viewUpNormalized.sub(eyeView.scalarMultiply(eyeView.scalarProduct(viewUpNormalized)))
						.normalize();
				Point3D xAxis = eyeView.vectorProduct(yAxis).normalize();
				@SuppressWarnings("unused")
				Point3D zAxis = xAxis.vectorProduct(yAxis).normalize();

				Point3D xCorner = xAxis.scalarMultiply(horizontal / 2.0).negate();
				Point3D yCorner = yAxis.scalarMultiply(vertical / 2.0);
				Point3D screenCorner = view.add(xCorner.add(yCorner));

				Scene scene = RayTracerViewer.createPredefinedScene();

				short[] rgb = new short[3];
				int offset = 0;
				for (int y = 0; y < height; y++) {
					for (int x = 0; x < width; x++) {
						double screenX = (double) x / (width - 1) * horizontal;
						double screenY = (double) y / (height - 1) * vertical;
						Point3D xPoint = xAxis.scalarMultiply(screenX);
						Point3D yPoint = yAxis.scalarMultiply(screenY).negate();
						Point3D screenPoint = screenCorner.add(xPoint.add(yPoint));
						Ray ray = Ray.fromPoints(eye, screenPoint);
						
						RayUtil.tracer(scene, ray, rgb);
						red[offset] = rgb[0] > 255 ? 255 : rgb[0];
						green[offset] = rgb[1] > 255 ? 255 : rgb[1];
						blue[offset] = rgb[2] > 255 ? 255 : rgb[2];
						offset++;
					}
				}

				System.out.println("Izračuni gotovi...");
				observer.acceptResult(red, green, blue, requestNo);
				System.out.println("Dojava gotova...");
			}
		};
	}

	

}
