package hr.fer.zemris.java.raytracer;


import hr.fer.zemris.java.raytracer.model.GraphicalObject;
import hr.fer.zemris.java.raytracer.model.LightSource;
import hr.fer.zemris.java.raytracer.model.Point3D;
import hr.fer.zemris.java.raytracer.model.Ray;
import hr.fer.zemris.java.raytracer.model.RayIntersection;
import hr.fer.zemris.java.raytracer.model.Scene;

/**
 * Class provides utilites for working with scenes and rays.
 * For example finding closest intersection, calculating diffuse and 
 * reflextive components of pixel color.
 * @author Luka Mijić
 *
 */
public class RayUtil {

	/**
	 * RGB uses three components
	 */
	private static final int COMPOMENTS = 3;
	
	/**
	 * Interface is used to access factor from
	 * {@link RayIntersection}
	 * @author Luka Mijić
	 *
	 */
	private interface FactorGetter {
		double getFactor(RayIntersection i);
	}
	
	/**
	 * Interface is used to access intensities from
	 * {@link LightSource}.
	 * @author Luka Mijić
	 *
	 */
	private interface IntensityGetter {
		int getIntensity(LightSource ls);
	}
	
	/**
	 * Stores RGB diffuse factor getters
	 */
	private static FactorGetter[] dFactors;
	
	/**
	 * Stores RGB reflective factor getters.
	 */
	private static FactorGetter[] rFactors;
	
	/**
	 * Stores shininess factor getter
	 */
	private static FactorGetter krn;
	
	/**
	 * Stores RGB intensity getters.
	 */
	private static IntensityGetter[] intensities;
	static {
		dFactors = new FactorGetter[COMPOMENTS];
		rFactors = new FactorGetter[COMPOMENTS];
		intensities = new IntensityGetter[COMPOMENTS];
		
		dFactors[0] = i -> i.getKdr();
		dFactors[1] = i -> i.getKdg();
		dFactors[2] = i -> i.getKdb();
		
		rFactors[0] = i -> i.getKrr();
		rFactors[1] = i -> i.getKrg();
		rFactors[2] = i -> i.getKrb();
		
		krn = i -> i.getKrn();
		
		intensities[0] = ls -> ls.getR();
		intensities[1] = ls -> ls.getG();
		intensities[2] = ls -> ls.getB();
	}
	
	
	/**
	 * Method finds closest intersection for {@link Ray} and
	 * {@link GraphicalObject} in {@link Scene} and then it calculates 
	 * RGB colors.
	 * @param scene models environment that stores {@link LightSource} and {@link GraphicalObject}
	 * @param ray that is cast from 'eye'
	 * @param rgb results are stored here
	 */
	public static void tracer(Scene scene, Ray ray, short[] rgb) {
		rgb[0] = 15;
		rgb[1] = 15;
		rgb[2] = 15;
		RayIntersection closest = RayUtil.findClosestIntersection(scene, ray);
		if (closest == null) {
			rgb[0] = 0;
			rgb[1] = 0;
			rgb[2] = 0;
			return;
		}
		
		short[] calculated = RayUtil.calculateDAndR(scene, ray, closest);
		rgb[0] += calculated[0];
		rgb[1] += calculated[1];
		rgb[2] += calculated[2];
	}
	
	/**
	 * Method is used for finding closest intersection between given ray and 
	 * {@link GraphicalObject}s in {@link Scene}.
	 * @param scene stores {@link LightSource}s and {@link GraphicalObject}s.
	 * @param ray that intersects with zero or more {@link GraphicalObject}s.
	 * @return null if ray doesn't have intersections otherwise return closes one.
	 */
	public static RayIntersection findClosestIntersection(Scene scene, Ray ray) {
		RayIntersection intersection = null;

		for (GraphicalObject obj : scene.getObjects()) {
			RayIntersection currentIntersection = obj.findClosestRayIntersection(ray);
			if (intersection == null) {
				intersection = currentIntersection;
			} else if (currentIntersection != null && intersection != null) {
				if (intersection.getDistance() > currentIntersection.getDistance()) {
					intersection = currentIntersection;
				}
			}
		}
		return intersection;
	}
	
	/**
	 * Method calculates diffuse and reflective RGB components for intersection.
	 * @param scene models environment.
	 * @param ray 
	 * @param i intersection
	 * @return array of short values with calculated RGB for diffuse and reflective components.
	 */
	public static short[] calculateDAndR(Scene scene, Ray ray, RayIntersection i) {
		short[] rgb = new short[COMPOMENTS];
		short[] dComponent = new short[COMPOMENTS];
		short[] rComponent = new short[COMPOMENTS];
		Point3D normal = i.getNormal().normalize();
		for(LightSource ls:scene.getLights()) {
			Point3D lp = ls.getPoint();
			Ray rayI = Ray.fromPoints(lp, i.getPoint());
			RayIntersection rI = findClosestIntersection(scene, rayI);
			if(!shouldCalculateColor(i, rI, lp)) {
				continue;
			}
			
			Point3D l = lp.sub(i.getPoint()).normalize();
			double cosL = l.scalarProduct(normal);
			if(cosL > 0) {
				calculateComponents(dComponent, intensities, dFactors, ls, i, cosL);
			}
			
			Point3D r = normal.scalarMultiply(2 * l.scalarProduct(normal)).sub(l);
			Point3D v = ray.start.sub(i.getPoint()).normalize();
			double multiplier = Math.pow(r.scalarProduct(v), krn.getFactor(i));
			if(multiplier > 0) {
				calculateComponents(rComponent, intensities, rFactors, ls, i, multiplier);
			}
			
		}
		
		rgb[0] = (short) (dComponent[0] + rComponent[0]);
		rgb[1] = (short) (dComponent[1] + rComponent[1]);
		rgb[2] = (short) (dComponent[2] + rComponent[2]);
		return rgb;
	}
	
	/**
	 * Method checks if calculations should be done for given rays.
	 * @param i1 ray from eye to intersection
	 * @param i2 ray from eye {@link LightSource} to intersection.
	 * @param lp location of {@link LightSource}.
	 * @return
	 */
	private static boolean shouldCalculateColor(RayIntersection i1, RayIntersection i2, Point3D lp) {
		if(i2 == null) return false;
		double distance1 = i1.getPoint().sub(lp).norm();
		double distance2 = i2.getPoint().sub(lp).norm();
		if(distance2 + 1e-6 < distance1) return false;
		
		return true;
	}

	/**
	 * Method calculates new value of toAdd array.
	 * @param toAdd array that is modifies
	 * @param intesities array of {@link IntensityGetter}
	 * @param getters array of {@link FactorGetter}
	 * @param ls light source
	 * @param i intersection
	 * @param multiplier
	 */
	private static void calculateComponents(short[] toAdd, IntensityGetter[] intesities, 
							FactorGetter[] getters, LightSource ls, RayIntersection i, double multiplier) {
		for(int k = 0; k < COMPOMENTS; k++) {
			toAdd[k] += (multiplier * getters[k].getFactor(i) * intesities[k].getIntensity(ls));
		}
	}
}
