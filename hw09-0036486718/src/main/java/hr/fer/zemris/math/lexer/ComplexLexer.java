package hr.fer.zemris.math.lexer;

import java.util.Objects;

/**
 * Class represents lexical analysator used for
 * generating tokens that store real or imaginary value.
 * @author Luka Mijić
 *
 */
public class ComplexLexer {

	/**
	 * Input text is stored into char array
	 */
	private char[] data;
	
	/**
	 * Current token
	 */
	private ComplexToken token;
	
	/**
	 * Index of first index that is not used.
	 * Initial value is 0.
	 */
	private int currentIndex;
	
	
	/**
	 * Constructor takes String text and transforms it
	 * into char array. 
	 * @param text that is transformed
	 * @throws NullpointerException if given argument is null
	 */
	public ComplexLexer(String expression) {
		Objects.requireNonNull(expression, "Given expression must not be null reference.");
		expression = expression.trim();
		if(!expression.startsWith("+") && !expression.startsWith("-")) {
			expression = "+" + expression;
		}
		data = expression.toCharArray();
	}

	/**
	 * @return {@link ComplexToken} that was generated last.
	 * 			Returns <code>null</code> if no {@link ComplexToken} was generated.
	 */
	public ComplexToken getToken() {
		return token;
	}
	
	/**
	 * Method generates new {@link ComplexToken}.
	 * @return new {@link ComplexToken}.
	 */
	public ComplexToken nextToken() {
		if(token != null && token.getType() == ComplexTokenType.EOF) {
			throw new LexerException("No more tokens avaliable.");
		}
		
		skipWhiteSpace();
		
		token = currentIndex >= data.length ? new ComplexToken(ComplexTokenType.EOF, null) : next();
		return token;
	}
	
	/**
	 * Implementation of generation of {@link ComplexToken}.
	 * @return generated {@link ComplexToken}
	 */
	private ComplexToken next() {
		boolean positive = false;
		ComplexTokenType type = ComplexTokenType.REAL;
		if(data[currentIndex] == '+') {
			positive = true;
		} else if(data[currentIndex] != '-') {
			throw new LexerException("Expected '+' or '-' sign at index " + currentIndex + ".");
		}
		
		if(++currentIndex >= data.length) {
			throw new LexerException("Invalid expression");
		}
		skipWhiteSpace();
		if(data[currentIndex] == 'i') {
			type = ComplexTokenType.IMAGINARY;
			currentIndex++;
		}
		
		StringBuilder numberBuilder = new StringBuilder();
		while(currentIndex < data.length) {
			char currentChar = data[currentIndex];
			if(!Character.isDigit(currentChar) && !(currentChar == '.')) {
				break;
			}
			numberBuilder.append(currentChar);
			currentIndex++;
		}
		
		String strNumber = numberBuilder.toString();
		int multiplier = positive ? 1 : -1;
		if(type == ComplexTokenType.IMAGINARY && strNumber.isEmpty()) {
			return new ComplexToken(type, 1.0 * multiplier);
		}
		
		try {
			Double number = Double.valueOf(strNumber) * multiplier;
			return new ComplexToken(type, number);
		} catch (NumberFormatException exc) {
			throw new LexerException("'" + strNumber + "' is not parsable to double.");
		}
	}
	
	/**
	 * Method is used to skip all white space from current index 
	 * onwards. After calling this method index is at location of next
	 * non whitespace character or it is out of bounds. 
	 */
	private void skipWhiteSpace() {
		while(currentIndex < data.length) {
			if(!Character.isWhitespace(data[currentIndex])) break;
			currentIndex++;
		}
	}
}
