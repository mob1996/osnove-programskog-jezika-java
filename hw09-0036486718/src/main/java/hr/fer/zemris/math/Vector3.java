package hr.fer.zemris.math;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;
import java.util.Objects;

/**
 * Class represents vector that contains 3 components.
 * Those components are x, y and z. Angle between all components is 90 degrees.
 * {@link Vector3} is unmodifiable, and all method return completly new {@link Vector3}.
 * @author Luka Mijić
 *
 */
public class Vector3 {
	
	/**
	 * Component x
	 */
	private double x;
	
	/**
	 * Component y
	 */
	private double y;
	
	/**
	 * Component z
	 */
	private double z;

	/**
	 * Default constructor of {@link Vector3}.
	 * Creates new instance of {@link Vector3}.
	 * @param x value of x component
	 * @param y value of y component
	 * @param z value of z component
	 */
	public Vector3(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	/**
	 * @return value of x component
	 */
	public double getX() {
		return x;
	}

	/**
	 * @return value of y component
	 */
	public double getY() {
		return y;
	}

	/**
	 * @return value of z component
	 */
	public double getZ() {
		return z;
	}
	
	/**
	 * Method calculates and returns {@link Vector3} norm.
	 * @return vector norm
	 */
	public double norm() {
		double norm = pow(this.x, 2) + pow(this.y, 2) + pow(this.z, 2);
		norm = sqrt(norm);
		return norm;
	}
	
	/**
	 * Method creates new instance of {@link Vector3} that created by
	 * normalization of current {@link Vector3}.
	 * Method doesn't modify current {@link Vector3}.
	 * @return created {@link Vector3}
	 */
	public Vector3 normalized() {
		double norm  = this.norm();
		return new Vector3(this.x / norm, this.y / norm, this.z / norm);
	}
	
	/**
	 * Method adds two {@link Vector3} without modifying 
	 * any of them. Result is creation of new {@link Vector3}.
	 * @param other {@link Vector3} that is added.
	 * @return new {@link Vector3}
	 * @throws NullPointerException if other is {@value null}
	 */
	public Vector3 add(Vector3 other) {
		Objects.requireNonNull(other, "Parameter 'other' can't be null reference.");
		return new Vector3(this.x + other.x, this.y + other.y, this.z + other.z);
	}
	
	/**
	 * Method subtracts other {@link Vector3} from current one without modifying 
	 * any of them. Result is creation of new {@link Vector3}.
	 * @param other {@link Vector3} that is subtracted.
	 * @return new {@link Vector3}
	 * @throws NullPointerException if other is {@value null}
	 */
	public Vector3 sub(Vector3 other) {
		Objects.requireNonNull(other, "Parameter 'other' can't be null reference.");
		return new Vector3(this.x - other.x, this.y - other.y, this.z - other.z);
	}
	
	/**
	 * Method calculates dot product from two {@link Vector3} without modifying 
	 * any of them.
	 * @param other {@link Vector3} used in calculation
	 * @return dot product
	 * @throws NullPointerException if other is {@value null}
	 */
	public double dot(Vector3 other) {
		Objects.requireNonNull(other, "Parameter 'other' can't be null reference.");
		double dot = this.x * other.x + this.y * other.y + this.z * other.z;
		return dot;
	}
	
	/**
	 * Method calculates cross product from two {@link Vector3} without modifying 
	 * any of them.
	 * @param other {@link Vector3} used in calculation
	 * @return cross product
	 * @throws NullPointerException if other is {@value null}
	 */
	public Vector3 cross(Vector3 other) {
		Objects.requireNonNull(other, "Parameter 'other' can't be null reference.");
		double newX = this.y * other.z - this.z * other.y;
		double newY = this.z * other.x - this.x * other.z;
		double newZ = this.x * other.y - this.y * other.x;
		return new Vector3(newX, newY, newZ);
	}
	
	/**
	 * Method calculates {@link Vector3} by scaling current 
	 * {@link Vector3} without modifying it.
	 * @param s scaler
	 * @return new {@link Vector3}
	 */
	public Vector3 scale(double s) {
		return new Vector3(this.x * s, this.y * s, this.z * s);
	}
	
	/**
	 * Method calculated cos(angle) between two {@link Vector3}.
	 * @param other
	 * @return double value of cos(angle)
	 * @throws NullPointerException if other is {@value null}
	 * @throws IllegalArgumentException if divison by zero is tried. 
	 * 					Division by zero happens if norm of any {@link Vector3} is zero.
	 */
	public double cosAngle(Vector3 other) {
		Objects.requireNonNull(other, "Parameter 'other' can't be null reference.");
		double dot = this.dot(other);
		double divisor = this.norm() * other.norm();
		if(divisor == 0) {
			throw new IllegalArgumentException("Divisor is 0.");
		}
		
		return dot / divisor;
	}
	
	/**
	 * Method creates double array which stores components
	 * like [x, y, z].
	 * @return return created array
	 */
	public double[] toArray() {
		double[] array = {this.x, this.y, this.z};
		return array;
	}
	
	/**
	 * Method creates {@link String} representation of
	 * {@link Vector3}.
	 * @return created {@link String} representation.
	 */
	@Override
	public String toString() {
		return String.format("(%.3f, %.3f, %.3f)", this.x, this.y, this.z);
	}
}
