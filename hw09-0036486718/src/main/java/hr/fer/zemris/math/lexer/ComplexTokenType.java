package hr.fer.zemris.math.lexer;

/**
 * Enum class models types of {@link ComplexToken}.
 * @author Luka Mijić
 *
 */
public enum ComplexTokenType {

	/**
	 * Real number
	 */
	REAL,
	
	/**
	 * Complex number
	 */
	IMAGINARY,
	
	/**
	 * End of file
	 */
	EOF
}
