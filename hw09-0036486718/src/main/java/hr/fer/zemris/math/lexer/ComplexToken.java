package hr.fer.zemris.math.lexer;

import java.util.Objects;


/**
 * Class {@link ComplexToken} models token 
 * generated using lexical analysis. 
 * @author Luka Mijić
 *
 */
public class ComplexToken {

	/**
	 * Type of token
	 */
	private ComplexTokenType type;
	
	/**
	 * Value of token
	 */
	private Double value;
	
	/**
	 * Creates {@link ComplexToken} using given type and value.
	 * @param type of token
	 * @param value of token
	 * @throws NullPointerException if <code>type</code> is <code>null</code> reference.
	 */
	public ComplexToken(ComplexTokenType type, Double value) {
		this.type = Objects.requireNonNull(type, "Type must not be null reference.");;
		this.value = value;
	}
	
	/**
	 * @return type of token
	 */
	public ComplexTokenType getType() {
		return type;
	}

	/**
	 * @return value of token
	 */
	public Double getValue() {
		return value;
	}
}
