package hr.fer.zemris.math;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Class models complex rooted polynomial.
 * f(z) = (z-z1) * (z-z2) * .... * (z-zn).
 * z1...zn are given through constructor.
 * z is given through {@link ComplexRootedPolynomial#apply(Complex)}.
 * Single {@link ComplexRootedPolynomial} can be used for calculating multiple z's.
 * @author Luka Mijić
 *
 */
public class ComplexRootedPolynomial {

	/**
	 * Stores given roots
	 */
	private List<Complex> roots;
	
	/**
	 * Creates instance of {@link ComplexRootedPolynomial}.
	 * @param roots array of roots
	 * @throws IllegalArgumentException if <code>roots</code> is empty.
	 * @throws NullPointerException if <code>roots</code> is <code>null</code> or if any element
	 * 				inside <code>roots</code> is <code>null</code>
	 */
	public ComplexRootedPolynomial(Complex ...roots) {
		Objects.requireNonNull(roots, "Parameter 'roots' must not be null reference.");
		if(roots.length <= 0) {
			throw new IllegalArgumentException("ComplexRootedPolynomial needs atleast one root.");
		}
		
		this.roots = new ArrayList<>();
		for(Complex root:roots) {
			Objects.requireNonNull(root, "All elements of 'roots' must be non null reference to Complex.");
			this.roots.add(root);
		}
	}
	
	/**
	 * Calculates complex rooted polynomial for <code>z</code>.
	 * @param z is {@link Complex} for which polynomial is calculated
	 * @return result of calculations
	 * @throws NullPointerException if <code>z</code> is <code>null</code> reference.
	 */
	public Complex apply(Complex z) {
		Objects.requireNonNull(z, "Given complex number must not be null reference.");
		Complex result = Complex.ONE;
		for(Complex zn:this.roots) {
			result = result.mul(z.sub(zn));
		}
		return result;
	}
	
	/**
	 * Calculates {@link ComplexPolynomial} from this {@link ComplexRootedPolynomial}.
	 * @return new {@link ComplexPolynomial}
	 * @see ComplexPolynomial
	 */
	public ComplexPolynomial toComplexPolynomial() {
		ComplexPolynomial result = new ComplexPolynomial(this.roots.get(0).negate(), Complex.ONE);
		for(int i = 1, size = this.roots.size(); i < size; i++) {
			result = result.multiply(new ComplexPolynomial(this.roots.get(i).negate(), Complex.ONE));
		}
		return result;
	}
	
	/**
	 * Calculates which root is closest to <code>z</code>.
	 * For root to be closest, it needs to have the lowest distance to
	 * <code>z</code> and that distance must be lower than <code>threshold</code>.
	 * @param z
	 * @param threshold
	 * @return index of {@link Complex} that is closest to <code>z</code> in <code>roots</code>
	 * @throws NullPointerException if <code>z</code> is <code>null</code> reference.
	 * @throws IllegalArgumentException if <code>z</code> is less than zero.
	 */
	public int indexOfClosestRootFor(Complex z, double threshold) {
		Objects.requireNonNull(z, "Given complex number must not be null reference.");
		if(threshold < 0) throw new IllegalArgumentException("Threshold must be non negative value.");
		
		int index = -1;
		double distance = Double.MAX_VALUE;

		for(int i = 0, size = roots.size(); i < size; i++) {
			double newDistance = z.sub(roots.get(i)).module();
			if(newDistance < threshold && newDistance < distance) {
				distance = newDistance;
				index = i + 1;
			}
		}
		
		return index;
	}
	
	/**
	 * Method creates {@link String} representation of {@link ComplexRootedPolynomial}.
	 * @return {@link String} representation.
	 */
	@Override
	public String toString() {
		StringBuilder rootsStrBuilder = new StringBuilder().append("( ");
		for(int i = 0, size = roots.size(); i < size; i++) {
			rootsStrBuilder.append(roots.get(i));
			rootsStrBuilder = i == size - 1 ? 
					rootsStrBuilder.append(" )") : rootsStrBuilder.append(", ");  
		}
		return rootsStrBuilder.toString();
	}
}
