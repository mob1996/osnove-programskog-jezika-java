package hr.fer.zemris.math.parser;

import java.util.Objects;

import hr.fer.zemris.math.Complex;
import hr.fer.zemris.math.lexer.ComplexLexer;
import hr.fer.zemris.math.lexer.ComplexTokenType;
import hr.fer.zemris.math.lexer.LexerException;

/**
 * Class models parser that takes String input and creates in
 * {@link Complex} number.
 * @author Luka Mijić
 *
 */
public class ComplexParser {

	private ComplexLexer lexer;
	private Complex complex;
	
	public ComplexParser(String expression) {
		Objects.requireNonNull(expression, "'expression' must not be null reference.");
		if(expression.trim().isEmpty()) {
			throw new IllegalArgumentException("'expression' must not be empty String.");
		}
		lexer = new ComplexLexer(expression);
		parse();
	}
	
	/**
	 * Method parses input into {@link Complex}.
	 */
	private void parse() {
		double re = 0;
		double im = 0;
		
		try {
			while (true) {
				lexer.nextToken();
				if(isTokenOfType(ComplexTokenType.EOF)) {
					break;
				} else if(isTokenOfType(ComplexTokenType.REAL)) {
					re = re + lexer.getToken().getValue();
				} else {
					im = im + lexer.getToken().getValue();
				}
			}
		} catch (LexerException e) {
			throw new ParserException(e.getMessage());
		}
		complex = new Complex(re, im);
	}
	
	/**
	 * @return parsed {@link Complex}
	 */
	public Complex getParsedComplex() {
		return complex;
	}
	
	/**
	 * Helper method that is used to check if current token is same type as given
	 * type
	 * 
	 * @param type
	 *            that we are checking
	 * @return true if current token's type is same as type
	 */
	private boolean isTokenOfType(ComplexTokenType type) {
		return lexer.getToken().getType() == type;
	}
}
