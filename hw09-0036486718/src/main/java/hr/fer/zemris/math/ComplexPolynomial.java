package hr.fer.zemris.math;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Class models complex polynomial.
 * f(z) = z0 + z1 * z + z2 * z^2 + ... + zn * zn^n
 * z0...zn are given through constructor.
 * z is given through {@link ComplexPolynomial#apply(Complex)}.
 * Single {@link ComplexPolynomial} can be used for calculating multiple z's.
 * @author Luka Mijić
 *
 */
public class ComplexPolynomial {

	/**
	 * Stores given factors
	 */
	private List<Complex> factors;
	
	/**
	 * Creates instance of {@link ComplexPolynomial}.
	 * @param factors array of factors
	 * @throws IllegalArgumentException if <code>factors</code> is empty.
	 * @throws NullPointerException if <code>factors</code> is <code>null</code> or if any element
	 * 				inside <code>factors</code> is <code>null</code>
	 */
	public ComplexPolynomial(Complex ...factors) {
		Objects.requireNonNull(factors, "Parameter 'roots' must not be null reference.");
		if(factors.length <= 0) {
			throw new IllegalArgumentException("ComplexRootedPolynomial needs atleast one root.");
		}
		
		this.factors = new ArrayList<>();
		for(Complex factor:factors) {
			Objects.requireNonNull(factor, "All elements of 'roots' must be non null reference to Complex.");
			this.factors.add(factor);
		}
	}
	
	/**
	 * @return order of {@link ComplexPolynomial}
	 */
	public short order() {
		return (short) (factors.size() - 1);
	}
	
	/**
	 * Method creates new {@link ComplexPolynomial} by multiplying 
	 * two {@link ComplexPolynomial}.
	 * @param other
	 * @return new {@link ComplexPolynomial} by multiplying two {@link ComplexPolynomial}
	 * @throws NullPointerException if <code>other</code> is <code>null</code> reference
	 */
	public ComplexPolynomial multiply(ComplexPolynomial other) {
		Objects.requireNonNull(other, "ComplexPolynomial 'other' must not be null reference.");
		int thisOrder = this.order();
		int otherOrder = other.order();
		Complex[] newFactors = new Complex[thisOrder + otherOrder + 1]; 
		
		for(int i = 0; i <= thisOrder; i++) {
			for(int j = 0; j <= otherOrder; j++) {
				int order = i + j;
				Complex mul = this.factors.get(i).mul(other.factors.get(j));
				if(newFactors[order] == null) {
					newFactors[order] = mul;
				} else {
					newFactors[order] = newFactors[order].add(mul);
				}
			}
		}
		
		return new ComplexPolynomial(newFactors);
	}
	
	/**
	 * Method creates new {@link ComplexPolynomial} by deriving
	 * current {@link ComplexPolynomial}.
	 * @return new {@link ComplexRootedPolynomial}
	 */
	public ComplexPolynomial derive() {
		Complex[] newFactors = new Complex[this.order()];
		for(int i = 1, size = this.factors.size(); i < size; i++) {
			newFactors[i-1] = this.factors.get(i).mul(new Complex(i, 0));
		}
		
		return new ComplexPolynomial(newFactors);
	}
	
	/**
	 * Calculates complex rooted polynomial for <code>z</code>.
	 * @param z is {@link Complex} for which polynomial is calculated
	 * @return result of calculations
	 * @throws NullPointerException if <code>z</code> is <code>null</code> reference.
	 */
	public Complex apply(Complex z) {
		Objects.requireNonNull(z, "Given complex number must not be null reference.");
		Complex result = Complex.ZERO;
		for(int i = 0, size = this.factors.size(); i < size; i++) {
			Complex zn = this.factors.get(i);
			result = result.add(zn.mul(z.power(i)));
		}
		return result;
	}
	
	/**
	 * Method creates {@link String} representation of {@link ComplexPolynomial}.
	 * @return {@link String} representation.
	 */
	@Override
	public String toString() {
		StringBuilder factorsStrBuilder = new StringBuilder().append("( ");
		for(int i = 0, size = factors.size(); i < size; i++) {
			factorsStrBuilder.append(factors.get(i));
			factorsStrBuilder = i == size - 1 ? 
					factorsStrBuilder.append(" )") : factorsStrBuilder.append(", ");  
		}
		return factorsStrBuilder.toString();
	}
}
