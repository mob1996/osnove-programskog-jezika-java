package hr.fer.zemris.math;

import static java.lang.Math.sqrt;
import static java.lang.Math.PI;
import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.pow;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import hr.fer.zemris.math.parser.ComplexParser;
import hr.fer.zemris.math.parser.ParserException;
/**
 * Class models complex number. Complex number has two components,
 * real and imaginary component. Class also provides methods for handling
 * complex numbers. Instance of {@link Complex} are unmodifiable after construction
 * and every method returns new {@link Complex}.
 * @author Luka Mijić
 *
 */
public class Complex {

	
	/**
	 * Instance of {@link Complex} where real and imaginary
	 * components are equal to zero. 
	 */
	public static final Complex ZERO = new Complex(0, 0);
	
	/**
	 * Instance of {@link Complex} where real component is
	 * equal to one and imaginary component is zero. 
	 */
	public static final Complex ONE = new Complex(1, 0);
	
	/**
	 * Instance of {@link Complex} where real component is
	 * equal to negative one and imaginary component is zero. 
	 */
	public static final Complex ONE_NEG = new Complex(-1, 0);
	
	/**
	 * Instance of {@link Complex} where real component is equal to
	 * zero and imaginary component is one. 
	 */
	public static final Complex IM = new Complex(0, 1);
	
	/**
	 * Instance of {@link Complex} where real component is equal to
	 * zero and imaginary component is negative one. 
	 */
	public static final Complex IM_NEG = new Complex(0, -1);
	
	/**
	 * Real part of complex number
	 */
	private double re;
	
	/**
	 * Imaginary part of complex number
	 */
	private double im;

	/**
	 * Creates new {@link Complex} with real and 
	 * imaginary components set to zero. 
	 */
	public Complex() {
		this(0, 0);
	}
	
	/**
	 * Constructor that creates new {@link Complex} using
	 * given arguments.
	 * @param re real component 
	 * @param im imaginary component
	 */
	public Complex(double real, double imaginary) {
		this.re = real;
		this.im = imaginary;
	}
	
	/**
	 * Method calculates complex number from given module and angle.
	 * @param module of complex number
	 * @param angle of complex number
	 * @return new {@link Complex} from module and angle
	 */
	public static Complex fromModuleAndAngle(double module, double angle) {
		double re = module * cos(angle);
		double im = module * sin(angle);
		return new Complex(re, im);
	}
	
	/**
	 * Factory method creates new {@link Complex} from given {@link String}
	 * @param expression that is parsed
	 * @return created {@link Complex}
	 */
	public static Complex parse(String expression) {
		try {
			return new ComplexParser(expression).getParsedComplex();
		} catch(ParserException exc) {
			throw new IllegalArgumentException("'" + expression + "' can't be parsed into complex.");
		}
	}
	
	/**
	 * @return value of real component
	 */
	public double getReal() {
		return re;
	}

	/**
	 * @return value of imaginary component
	 */
	public double getImaginary() {
		return im;
	}

	/**
	 * Method calculates module of complex number stored in {@link Complex}.
	 * Algorithm: module = sqrt(re^2 + im^2)
	 * @return module of complex number
	 */
	public double module() {
		return sqrt(this.re * this.re + this.im * this.im);
	}
	
	/**
	 * Method creates complex number by adding two complex numbers.
	 * @param other {@link Complex} that is added
	 * @return {@link Complex} created by adding other {@link Complex} to
	 * 			current one. 
	 * @throws NullPointerException if other is <code>null</code> reference
	 */
	public Complex add(Complex other) {
		Objects.requireNonNull(other, "Parameter 'other' can't be null reference.");
		return new Complex(this.re + other.re, this.im + other.im);
	}
	
	/**
	 * Method creates complex number by subtracting given on from
	 * one that called this method. 
	 * @param other {@link Complex} that is subtracted
	 * @return {@link Complex} created by subtracting
	 * @throws NullPointerException if other is <code>null</code> reference
	 */
	public Complex sub(Complex other) {
		Objects.requireNonNull(other, "Parameter 'other' can't be null reference.");
		return new Complex(this.re - other.re, this.im - other.im);
	}
	
	/**
	 * Method creates complex number by multiplying two complex numbers.
	 * @param other second complex number in multiplying
	 * @return {@link Complex} created by multiplying two complex numbers.
	 * @throws NullPointerException if other is <code>null</code> reference
	 */
	public Complex mul(Complex other) {
		Objects.requireNonNull(other, "Parameter 'other' can't be null reference.");
		double newRe = this.re * other.re - this.im * other.im;
		double newIm = this.re * other.im + this.im * other.re;
		return new Complex(newRe, newIm);
	}
	
	/**
	 * Method creates complex number by dividing two complex numbers.
	 * @param other divisor
	 * @return {@link Complex} created by division of two complex numbers.
	 * @throws NullPointerException if other is <code>null</code> reference
	 * @throws IllegalArgumentException if module of other is {@value 0}
	 */
	public Complex div(Complex other) {
		Objects.requireNonNull(other, "Parameter 'other' can't be null reference.");
		double otherModule = other.module();
		if(otherModule == 0) {
			throw new IllegalArgumentException("Can't divide by zero.");
		}
		double divisor = 1 / (otherModule * otherModule);
		double newRe = (this.re * other.re + this.im * other.im) * divisor;
		double newIm = (-this.re * other.im + this.im * other.re) * divisor;
		
		return new Complex(newRe, newIm);
	}
	
	/**
	 * Method creates negated complex number.
	 * x + y * j => - x - y * j
	 * @return negated {@link Complex}
	 */
	public Complex negate() {
		return new Complex(-this.re, -this.im);
	}
	
	/**
	 * Method creates complex number by using power of n on current complex
	 * number.
	 * @param n power
	 * @return new {@link Complex}
	 * @throws IllegalArgumentException if n is negative integer.
	 */
	public Complex power(int n) {
		if(n < 0) {
			throw new IllegalArgumentException("Parameter 'n' must be non negative integer.");
		}
		double newAngle = n * atan2(this.im, this.re);
		double newModule = pow(this.module(), n);
		
		return fromModuleAndAngle(newModule, newAngle);
	}
	
	/**
	 * Method calculates roots of complex number. 
	 * @param n root
	 * @return {@link List} of {@link Complex} that contains all roots.
	 * @throws IllegalArgumentException if n is not positive integer
	 */
	public List<Complex> root(int n){
		if(n <= 0) {
			throw new IllegalArgumentException("Parameter 'n' must be positive integer.");
		}
		List<Complex> roots = new ArrayList<>();
		double newModule = pow(this.module(), 1.0/n);
		double currentAngle = atan2(this.im, this.re);
		
		for(int i = 0; i < n; i++) {
			double newAngle = (currentAngle + 2 * i * PI)/n;
			roots.add(fromModuleAndAngle(newModule, newAngle));
		}
		return roots;
	}
	
	/**
	 * Method creates String representation of complex number.
	 * @return created representation
	 */
	@Override
	public String toString() {
		return String.format("%.3f%+.3fi", this.re, this.im);
	}
	
}
