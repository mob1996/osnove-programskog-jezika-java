package hr.fer.zemris.java.gui.layouts;

import static org.junit.Assert.*;

import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.junit.Test;

public class CalcLayoutTest {

	@Test (expected = CalcLayoutException.class)
	public void testComponentInvalidPosition0() {
		JPanel p = new JPanel(new CalcLayout());
		p.add(new JLabel("x"), new RCPosition(-1, 4));
	}

	@Test (expected = CalcLayoutException.class)
	public void testComponentInvalidPosition1() {
		JPanel p = new JPanel(new CalcLayout());
		p.add(new JLabel("x"), new RCPosition(6, 4));
	}
	
	@Test (expected = CalcLayoutException.class)
	public void testComponentInvalidPosition2() {
		JPanel p = new JPanel(new CalcLayout());
		p.add(new JLabel("x"), new RCPosition(3, -1));
	}
	
	@Test (expected = CalcLayoutException.class)
	public void testComponentInvalidPosition3() {
		JPanel p = new JPanel(new CalcLayout());
		p.add(new JLabel("x"), new RCPosition(3, 8));	
	}
	
	@Test (expected = CalcLayoutException.class)
	public void testComponentInvalidPosition4() {
		JPanel p = new JPanel(new CalcLayout());
		p.add(new JLabel("x"), new RCPosition(0, 2));
	}
	
	@Test (expected = CalcLayoutException.class)
	public void testComponentInvalidPosition5() {
		JPanel p = new JPanel(new CalcLayout());
		p.add(new JLabel("x"), new RCPosition(1, 2));
	}
	
	@Test (expected = CalcLayoutException.class)
	public void testComponentInvalidPosition6() {
		JPanel p = new JPanel(new CalcLayout());
		p.add(new JLabel("x"), new RCPosition(1, 4));
	}
	
	@Test (expected = CalcLayoutException.class)
	public void testAddComponentToExistingPosition() {
		JPanel p = new JPanel(new CalcLayout());
		p.add(new JLabel("x"), new RCPosition(2, 2));
		p.add(new JButton("y"), "2, 2");
	}
	
	@Test
	public void testPrefSize0() {
		JPanel p = new JPanel(new CalcLayout(2));
		JLabel l1 = new JLabel(""); l1.setPreferredSize(new Dimension(10,30));
		JLabel l2 = new JLabel(""); l2.setPreferredSize(new Dimension(20,15));
		p.add(l1, new RCPosition(2,2));
		p.add(l2, new RCPosition(3,3));
		Dimension dim = p.getPreferredSize();
		
		assertEquals(152, dim.getWidth(), 0);
		assertEquals(158, dim.getHeight(), 0);
	}
	
	@Test
	public void testPrefSize1() {
		JPanel p = new JPanel(new CalcLayout(2));
		JLabel l1 = new JLabel(""); l1.setPreferredSize(new Dimension(108,15));
		JLabel l2 = new JLabel(""); l2.setPreferredSize(new Dimension(16,30));
		p.add(l1, new RCPosition(1,1));
		p.add(l2, new RCPosition(3,3));
		Dimension dim = p.getPreferredSize();
		
		System.out.println(dim.getWidth());
		assertEquals(152, dim.getWidth(), 0);
		assertEquals(158, dim.getHeight(), 0);
	}
}
