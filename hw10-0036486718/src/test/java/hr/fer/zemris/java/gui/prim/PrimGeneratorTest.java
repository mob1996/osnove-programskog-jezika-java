package hr.fer.zemris.java.gui.prim;

import static org.junit.Assert.*;

import org.junit.Test;

public class PrimGeneratorTest {

	@Test
	public void testGenerator() {
		PrimGenerator gen = new PrimGenerator();
		assertEquals(1, gen.next());
		assertEquals(2, gen.next());
		assertEquals(3, gen.next());
		
		for(int i = 0; i < 5; i++, gen.next());
		
		assertEquals(19, gen.next());
		
		gen.reset();
		
		assertEquals(1, gen.next());
		assertEquals(2, gen.next());
		assertEquals(3, gen.next());
		
		for(int i = 0; i < 5; i++, gen.next());
		
		assertEquals(19, gen.next());
		
	}
}
