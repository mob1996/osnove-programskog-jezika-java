package hr.fer.zemris.java.gui.prim;

import static org.junit.Assert.*;

import org.junit.Test;

public class PrimListModelTest {

	@Test (expected = IndexOutOfBoundsException.class)
	public void testInvalidGet() {
		PrimListModel model = new PrimListModel();
		model.getElementAt(5);
	}
	
	@Test (expected = IndexOutOfBoundsException.class)
	public void testInvalidGet1() {
		PrimListModel model = new PrimListModel();
		model.getElementAt(-2);
	}
	
	
	@Test
	public void testModel() {
		PrimListModel m = new PrimListModel();
		
		for(int i = 0; i < 99; i++, m.next());
		
		assertEquals(100, m.getSize());
		
		assertEquals(Integer.valueOf(1), m.getElementAt(0));
		assertEquals(Integer.valueOf(523), m.getElementAt(99));
	}
}
