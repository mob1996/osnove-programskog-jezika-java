package hr.fer.zemris.java.gui.calc;


import java.util.function.Consumer;

import javax.swing.JButton;

/**
 * Class extends {@link JButton} and implements {@link Inversable}.
 * It models inversable button. 
 * @author Luka Mijić
 *
 */
public class InverseButton extends JButton implements Inversable {

	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Current consumer
	 */
	private Consumer<CalcModel> currentConsumer;
	
	/**
	 * Original consumer
	 */
	private Consumer<CalcModel> originalConsumer;
	
	/**
	 * Inversed consumer
	 */
	private Consumer<CalcModel> inverseConsumer;
	
	/**
	 * Creates new {@link InverseButton}. 
	 * @param text of button
	 * @param originalConsumer original functionality
	 * @param inverseConsumer inverse functionality
	 */
	public InverseButton(String text, Consumer<CalcModel> originalConsumer,
			Consumer<CalcModel> inverseConsumer) {
		this.setText(text);
		
		this.originalConsumer = originalConsumer;
		this.inverseConsumer = inverseConsumer;
		this.currentConsumer = originalConsumer;
	}

	/**
	 * Inverses state of {@link InverseButton}
	 */
	@Override
	public void inverse() {
		if(currentConsumer == originalConsumer) {
			currentConsumer = inverseConsumer;
		} else {
			currentConsumer = originalConsumer;
		}
	}

	/**
	 * @return <code>current consumer</code>
	 */
	public Consumer<CalcModel> getCurrentConsumer(){
		return this.currentConsumer;
	}

}
