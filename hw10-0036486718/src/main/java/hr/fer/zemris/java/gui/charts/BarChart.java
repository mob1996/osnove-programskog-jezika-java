package hr.fer.zemris.java.gui.charts;

import java.util.List;

/**
 * Class models chart.
 * It stores {@link List} of {@link XYValue}s, 
 * name of x and y labels. Minimum y, maximum y and step.
 * @author Luka Mijić
 *
 */
public class BarChart {

	/**
	 * List of {@link XYValue}s
	 */
	private List<XYValue> xyValues;
	
	/**
	 * Name of x axis
	 */
	private String xLabel;
	
	/**
	 * Name of y axis
	 */
	private String yLabel;
	
	/**
	 * Minimum y
	 */
	private int yMin;
	
	/**
	 * Maximum y
	 */
	private int yMax;
	
	/**
	 * Step
	 */
	private int step;
	
	/**
	 * Creates new {@link BarChart} chart model
	 * @param xyValues {@link List} of {@link XYValue} values.
	 * @param xLabel name of x axis
	 * @param yLabel name of y axis
	 * @param yMin minimum y value
	 * @param yMax maximum y value
	 * @param step 
	 */
	public BarChart(List<XYValue> xyValues, String xLabel, String yLabel, int yMin, int yMax, int step) {
		this.xyValues = xyValues;
		this.xyValues.sort((f, s) -> Integer.valueOf(f.getX()).compareTo(s.getX()));
		this.xLabel = xLabel;
		this.yLabel = yLabel;
		this.yMin = yMin;
		this.yMax = yMax;
		this.step = step;
	}

	/**
	 * @return <code>xyValues</code>
	 */
	public List<XYValue> getXyValues() {
		return xyValues;
	}

	/**
	 * @return <code>xLabel</code>
	 */
	public String getxLabel() {
		return xLabel;
	}

	/**
	 * @return <code>yLabel</code>
	 */
	public String getyLabel() {
		return yLabel;
	}

	/**
	 * @return <code>yMin</code>
	 */
	public int getyMin() {
		return yMin;
	}

	/**
	 * @return <code>yMax</code>
	 */
	public int getyMax() {
		return yMax;
	}

	/**
	 * @return <code>step</code>
	 */
	public int getStep() {
		return step;
	}
	
}
