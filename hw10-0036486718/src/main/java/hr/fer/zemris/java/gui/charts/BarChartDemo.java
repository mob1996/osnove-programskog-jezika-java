package hr.fer.zemris.java.gui.charts;

import java.awt.BorderLayout;
import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * Program that reads chart informations from file and
 * creates {@link BarChart}. Program creates visible {@link JFrame}
 * and puts {@link BarChartComponent} created from {@link BarChart} into 
 * {@link JFrame}.
 * @author Luka Mijić
 *
 */
public class BarChartDemo extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Component that draws chart
	 */
	private BarChartComponent chart;
	
	private Path p;
	
	/**
	 * Creates frame
	 * @param p path to file that models chart
	 */
	public BarChartDemo(Path p) {
		chart = new BarChartComponent(parseFromFile(p));
		this.p = p;
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.setSize(800, 600);
		setTitle("Table");
		setLocation(20, 20);
		initGUI();
	}
	
	/**
	 * Add {@link BarChartComponent} to frame.
	 */
	private void initGUI() {
		JLabel label = new JLabel(p.toAbsolutePath().toString(), SwingUtilities.CENTER);
		getContentPane().setLayout(new BorderLayout());
		
		JPanel p = new JPanel();
		p.add(chart);
		
		getContentPane().add(label, BorderLayout.NORTH);
		getContentPane().add(chart, BorderLayout.CENTER);
	}
	
	/**
	 * Starting point of program.
	 * Program takes one argument, path to file that models
	 * {@link BarChartComponent}. If file doesn't exist or if
	 * it is not formated right program closes.
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length != 1) {
			System.out.println("Expected one argument from command line.");
			System.exit(0);
		}
		
		Path p = Paths.get(args[0]);
		
		if(!Files.isRegularFile(p)) {
			System.out.println("Given argument must lead to existing file.");
			System.exit(0);
		}
		
		SwingUtilities.invokeLater(() -> new BarChartDemo(p).setVisible(true));
	}
	
	/**
	 * Parses {@link BarChart} from given {@link Path} that leads to file on disc.
	 * @param p that leads to file
	 * @return parsed {@link BarChart}
	 */
	private static BarChart parseFromFile(Path p) {
		BarChart chart = null;
		try(BufferedReader r = new BufferedReader(new FileReader(p.toFile()))){
			String xLabel = r.readLine();
			String yLabel = r.readLine();
			List<XYValue> values = parseValues(r.readLine().trim());
			int yMin = Integer.valueOf(r.readLine());
			int yMax = Integer.valueOf(r.readLine());
			int step = Integer.valueOf(r.readLine());
			
			return new BarChart(values, xLabel, yLabel, yMin, yMax, step);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.exit(0);
		} 
		
		return chart;
	}
	
	/**
	 * Parses {@link List} of {@link XYValue} from given {@link String}.
	 * @param line {@link String} that is parsed.
	 * @return parsed {@link List}
	 */
	private static List<XYValue> parseValues(String line) {
		List<XYValue> values = new ArrayList<>();
		String[] inputs = line.split("\\s+");
		for(String i:inputs) {
			values.add(XYValue.parse(i));
		}
		return values;
	}
	
}
