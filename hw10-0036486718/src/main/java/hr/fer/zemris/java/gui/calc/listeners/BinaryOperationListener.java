package hr.fer.zemris.java.gui.calc.listeners;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.function.DoubleBinaryOperator;

import javax.swing.JOptionPane;

import hr.fer.zemris.java.gui.calc.CalcModel;

/**
 * {@link ActionListener} that {@link CalcModel#setPendingBinaryOperation(DoubleBinaryOperator)} 
 * to given {@link CalcModel}.
 * @author Luka Mijić
 *
 */
public class BinaryOperationListener implements ActionListener {

	/**
	 * {@link DoubleBinaryOperator} that is being set
	 */
	private DoubleBinaryOperator op;
	
	/**
	 * Model
	 */
	private CalcModel model;
	
	/**
	 * Parent to {@link JOptionPane} 
	 */
	private Component parent;
	
	/**
	 * Creates new {@link BinaryOperationListener}
	 * @param op {@link DoubleBinaryOperator} that is being set to model.
	 * @param model to which {@link DoubleBinaryOperator} is being set
	 * @param parent of {@link JOptionPane} that shows if error happens.
	 */
	public BinaryOperationListener(DoubleBinaryOperator op, CalcModel model, Component parent) {
		super();
		this.op = op;
		this.model = model;
		this.parent = parent;
	}

	/**
	 * Uses {@link CalcModel#setPendingBinaryOperation(DoubleBinaryOperator)} on given model.
	 * @param e is not used.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			model.setPendingBinaryOperation(op);
		} catch (Exception exc) {
			JOptionPane.showMessageDialog(parent, exc.getMessage(), "WARNING", JOptionPane.WARNING_MESSAGE);
		}
		
	}

}
