package hr.fer.zemris.java.gui.prim;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * Program that show two lists of prime numbers.
 * Next prime number is added by clickin "NEXT" button.
 * @author Luka Mijić
 *
 */
public class PrimFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Creates frame with default attributes.
	 */
	public PrimFrame() {
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.setSize(300,300);
		setTitle("Primes");
		setLocation(20, 20);
		initGUI();
	}
	
	/**
	 * Initializes GUI of frame.
	 * It adds two {@link JScrollPane} that share
	 * same {@link PrimListModel}. When {@link JButton} "NEXT" is clicked,
	 * new prime number is added to model.
	 */
	private void initGUI() {
		PrimListModel model = new PrimListModel();
		getContentPane().setLayout(new BorderLayout());
		
		JList<Integer> list1 = new JList<>(model);
		JList<Integer> list2 = new JList<>(model);
		
		JButton next = new JButton("NEXT");
		next.addActionListener(e -> model.next());
		
		
		JPanel central = new JPanel(new GridLayout(1, 0));
		central.add(new JScrollPane(list1));
		central.add(new JScrollPane(list2));
		
		getContentPane().add(next, BorderLayout.SOUTH);
		getContentPane().add(central, BorderLayout.CENTER);
		
	}
	
	/**
	 * Starts program.
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new PrimFrame().setVisible(true));

	}

}
