package hr.fer.zemris.java.gui.calc;

import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

/**
 * Class models calculator display.
 * It extends {@link JLabel}. It also implements {@link CalcValueListener}.
 * Because of that class can listen to changes in {@link CalcModel}.
 * @author Luka Mijić
 *
 */
public class CalcDisplay extends JLabel implements CalcValueListener {

	private static final long serialVersionUID = 1L;

	/**
	 * Style of font. 
	 */
	private static final int STYLE = Font.BOLD;
	
	/**
	 * Font size
	 */
	private static final int FONT_SIZE = 24;
	
	/**
	 * Border size
	 */
	private static final int BORDER_SIZE = 4;
	
	/**
	 * Creates {@link CalcDisplay}.
	 */
	public CalcDisplay() {
		this.setHorizontalAlignment(JLabel.RIGHT);
		Font currFont = this.getFont();
		this.setFont(new Font(currFont.getFontName(), STYLE, FONT_SIZE));
		this.setBackground(Color.YELLOW);
		this.setOpaque(true);
		this.setBorder(BorderFactory.createLineBorder(Color.BLACK, BORDER_SIZE));
	}
	
	/**
	 * Method is called usually from {@link CalcModel} when
	 * change happens in that models.
	 * @param model whose state changed
	 */
	@Override
	public void valueChanged(CalcModel model) {
		this.setText(model.toString());
	}

}
