package hr.fer.zemris.java.gui.calc;

/**
 * Interface that provides one method, {@link #inverse()}.
 * @author Luka Mijić
 *
 */
public interface Inversable {
	/**
	 * Inverses state of object.
	 */
	void inverse();
}
