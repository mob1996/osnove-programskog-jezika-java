package hr.fer.zemris.java.gui.calc.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import hr.fer.zemris.java.gui.calc.CalcModel;

/**
 * {@link ActionListener} that inserts digit to given {@link CalcModel}.
 * @author Luka Mijić
 *
 */
public class DigitListener implements ActionListener{

	/**
	 * Model
	 */
	private CalcModel model;
	
	/**
	 * Digit that is being inserted
	 */
	private int digit;
	
	/**
	 * Creates new {@link DigitListener}.
	 * @param model that gets inserted digit
	 * @param digit that is inserted
	 */
	public DigitListener(CalcModel model, int digit) {
		this.model = model;
		this.digit = digit;
	}
	
	/**
	 * Inserts <code>digit</code> to given <code>model</code>.
	 * @param e is not used
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		model.insertDigit(this.digit);
	}
	
	
}
