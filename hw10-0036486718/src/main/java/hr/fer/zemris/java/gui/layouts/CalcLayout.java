package hr.fer.zemris.java.gui.layouts;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager2;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Class implements {@link LayoutManager2} and models
 * layout for calculator. Layout has 31 slots for components.
 * It contains 7 columns and 5 rows. First row has 1 component that takes
 * 5 slots and is supposed to be used as a calculator display. Only available 
 * positions in first row are (1,1), (1,6), (1,7).
 * @author Luka Mijić
 *
 */
public class CalcLayout implements LayoutManager2 {
	
	/**
	 * Gets minimum {@link Dimension} from given component.
	 */
	private static Function<Component, Dimension> MIN_DIM_GETTER;
	
	/**
	 * Gets preffered {@link Dimension} from given component.
	 */
	private static Function<Component, Dimension> PREF_DIM_GETTER;
	static {
		MIN_DIM_GETTER = c -> c.getMinimumSize();
		PREF_DIM_GETTER = c -> c.getPreferredSize();
	}
	
	/**
	 * Number of rows
	 */
	private static final int CALC_ROWS = 5;
	/**
	 * Number of columns.
	 */
	private static final int CALC_COLUMNS = 7;
	
	/**
	 * This message is given to exception that is thrown when invalid {@link RCPosition} is given.
	 */
	private static final String POSITION_EXCEPTION_MESSAGE 
			= "Valid rows are [1, " + CALC_ROWS + "]. \nValid columns are [1, " + CALC_COLUMNS + "].\n"
				+ "If row is 1, then only valid column values are {1, 6, 7}.";
	
	/**
	 * Gap between {@link Component}s
	 */
	private int gap = 0;	
	
	/**
	 * Map takes {@link Component} as a key and {@link RCPosition}
	 * as value.
	 */
	private Map<Component, RCPosition> constraintsMap;
	
	/**
	 * Creates {@link CalcLayout} with zero gap.
	 */
	public CalcLayout() {
		this(0);
	}
	
	/**
	 * Creates {@link CalcLayout} with given gap.
	 * @param gap between components
	 */
	public CalcLayout(int gap) {
		this.gap = gap;
		this.constraintsMap = new HashMap<>();
	}
	
	/**
	 * Method is called to give appropriate 
	 * positions and dimension to {@link Container}s 
	 * {@link Component}s. 
	 * @param target {@link Container} that need it's subcompoments
	 * 					organized
	 */
	@Override
	public void layoutContainer(Container target) {
		int height = target.getHeight();
		int width = target.getWidth();
		Insets ins = target.getInsets();
        
        int compHeight = (height - ins.top - ins.bottom - (CALC_ROWS - 1) * gap) / CALC_ROWS;
        int compWidth = (width - ins.left - ins.right - (CALC_COLUMNS - 1) * gap) / CALC_COLUMNS;
        
        int compNum = target.getComponentCount();
        for(int i = 0; i < compNum; i++) {
        	Component c = target.getComponent(i);
        	RCPosition pos = constraintsMap.get(c);
        	int cWidth = 0;
        	
        	if(pos.getRow() == 1 && pos.getColumn() == 1) {
        		cWidth = 5 * compWidth + 4 * this.gap;
        	} else {
        		cWidth = compWidth;
        	}
        	int xPos = ins.left + (pos.getColumn() - 1) * (compWidth + gap);
        	int yPos = ins.top + (pos.getRow() - 1) * (compHeight + gap);
        	
        	c.setSize(cWidth, compHeight);
        	c.setBounds(xPos, yPos, cWidth, compHeight);
        }
	}

	/**
	 * Calculates minimum size that layout needs.
	 * @param target container
	 * @return calculated {@link Dimension}
	 */
	@Override
	public Dimension minimumLayoutSize(Container target) {
		Dimension dim = new Dimension(0, 0);
		Insets insets = target.getInsets();
		dim.width += (insets.left + insets.right);
		dim.height += (insets.top + insets.bottom);
		
		Dimension minComponentDim = componentsSize(MIN_DIM_GETTER);
		dim.width += (CALC_COLUMNS * minComponentDim.width + (CALC_COLUMNS - 1) * this.gap);
		dim.height += (CALC_ROWS * minComponentDim.height + (CALC_ROWS - 1) * this.gap);
		
		return dim;
	}

	/**
	 * Calculates preffered size that layout needs.
	 * @param target container
	 * @return calculated {@link Dimension}
	 */
	@Override
	public Dimension preferredLayoutSize(Container target) {
		Dimension dim = new Dimension(0, 0);
		Insets insets = target.getInsets();
		dim.width += (insets.left + insets.right);
		dim.height += (insets.top + insets.bottom);
		
		Dimension prefComponentDim = componentsSize(PREF_DIM_GETTER);
		dim.width += (CALC_COLUMNS * prefComponentDim.width + (CALC_COLUMNS - 1) * this.gap);
		dim.height += (CALC_ROWS * prefComponentDim.height + (CALC_ROWS - 1) * this.gap);
		return dim;
	}

	
	/**
	 * Adds component with given constraits.
	 * @param comp is component that is added
	 * @param constraints of component
	 * @throws CalcLayoutException if <code>constraits</code> is not {@link String}
	 * 								that can be parsed in {@link RCPosition} or if it isn't
	 * 								already {@link RCPosition}
	 */
	@Override
	public void addLayoutComponent(Component comp, Object constraints) {
		RCPosition position = null;
		if(constraints instanceof RCPosition) {
			position = (RCPosition) constraints;
		} else if (constraints instanceof String) {
			position = RCPosition.parse((String) constraints);
		} else {
			throw new CalcLayoutException("Constraints of invalid type.");
		}
		
		if(!isValidPosition(position)) {
			throw new CalcLayoutException(POSITION_EXCEPTION_MESSAGE + " Was: (" + position.toString() + ")");
		}
		
		if(this.constraintsMap.containsValue(position)) {
			throw new CalcLayoutException("Position (" + position.getRow() + ", " 
										+ position.getColumn() + ") is alread taken.");
		}
		
		this.constraintsMap.put(comp, position);
	}

	/**
	 * @param target container
	 * @return x alignment
	 */
	@Override
	public float getLayoutAlignmentX(Container target) {
		return 0.5f;
	}

	/**
	 * @param target container
	 * @return y alignment
	 */
	@Override
	public float getLayoutAlignmentY(Container target) {
		return 0.5f;
	}

	/**
	 * Does nothing, empty implementation.
	 */
	@Override
	public void invalidateLayout(Container target) {	
	}

	/**
	 * @param target container
	 * @return maximum layout {@link Dimension}
	 */
	@Override
	public Dimension maximumLayoutSize(Container target) {
		return new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE);
	}

	/**
	 * Does nothing, empty implementation.
	 */
	@Override
	public void addLayoutComponent(String name, Component comp) {
	}

	/**
	 * Removec {@link Component} from layout.
	 * @param comp {@link Component} that is removed
	 */
	@Override
	public void removeLayoutComponent(Component comp) {
		constraintsMap.remove(comp);
	}
	
	/**
	 * Method calculates wanted dimensions for one component.
	 * @param getter takes {@link Component} and returns {@link Dimension}.
	 * @see Component#getMinimumSize()
	 * @see Component#getPreferredSize()
	 * @return calculated {@link Dimension}
	 */
	private Dimension componentsSize(Function<Component, Dimension> getter) {
		int width = 0;
		int height = 0;
		
		RCPosition firstPos = new RCPosition(1, 1);
		for(Component comp:constraintsMap.keySet()) {
			Dimension size = getter.apply(comp);
			if(constraintsMap.get(comp).equals(firstPos)) {
				width = Math.max(width, (size.width - 4 * this.gap) / 5);
			} else {
				width = Math.max(width, size.width);
			}
			height = Math.max(height, size.height);
		}
		
		return new Dimension(width, height);
	}
	
	/**
	 * Check if given position is valid.
	 * @param position that is checked
	 * @return false if position is out of bounds or if position is already taken.
	 */
	private static boolean isValidPosition(RCPosition position) {
		if(position.getRow() <=0  || position.getRow() > CALC_ROWS) {
			return false;
		} else if(position.getColumn() <= 0 || position.getColumn() > CALC_COLUMNS) {
			return false;
		} else if(position.getRow() == 1) {
			if(position.getColumn() < 6 && position.getColumn() != 1) {
				return false;
			}
		}
		return true;
	}
}
