package hr.fer.zemris.java.gui.layouts;

/**
 * Class models exception that is supposed to be throw when
 * error occures in {@link CalcLayout}
 * @author Luka Mijić
 *
 */
public class CalcLayoutException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor
	 */
	public CalcLayoutException() {
		super();
	}

	/**
	 * Constructor that takes message argument.
	 * @param message is used to give better feedback
	 */
	public CalcLayoutException(String message) {
		super(message);
	}
}
