package hr.fer.zemris.java.gui.calc;

import static java.lang.Math.*;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.function.Consumer;
import java.util.function.DoubleBinaryOperator;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import hr.fer.zemris.java.gui.calc.listeners.BinaryOperationListener;
import hr.fer.zemris.java.gui.calc.listeners.DigitListener;
import hr.fer.zemris.java.gui.layouts.CalcLayout;

/**
 * Class represents functioning calculator program that provides some
 * extended functionalites. 
 * @author Luka Mijić
 *
 */
public class Calculator extends JFrame {

	private static final long serialVersionUID = 1L;

	/**
	 * Array of positions where digits are stored.
	 */
	private static final String[] NUMBER_POSITIONS = {"5,3", "4,3", "4,4", "4,5",
															 "3,3", "3,4", "3,5",
															 "2,3", "2,4", "2,5" };
	
	/**
	 * Stack to which calculator can push values or take them.
	 */
	private Stack<Double> stack = new Stack<>();
	
	/**
	 * List of all {@link Inversable} objects created.
	 */
	private List<Inversable> inversableBtns = new ArrayList<>();
	
	/**
	 * Calculator model.
	 */
	private CalcModel model = new CalcModelImpl();
	
	/**
	 * Creates new {@link Calculator}.
	 * Calls {@link #initGUI()} that sets up all components.
	 */
	public Calculator() {
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Calculator");
		setLocation(20, 20);
		initGUI();
		this.pack();
	}
	
	/**
	 * Sets up all components of {@link Calculator}.
	 */
	private void initGUI() {
		JPanel panel = new JPanel(new CalcLayout(4));
		CalcDisplay display = new CalcDisplay();
		getContentPane().add(panel);
		display.valueChanged(model);
		panel.add(display, "1,1");
		model.addCalcValueListener(display);
		
		initNumberButtons(panel);
		initSimpleOperationButtons(panel);
		initInversableButtons(panel);
		
		JCheckBox invBox = new JCheckBox("Inv", false);
		invBox.addActionListener(e -> inversableBtns.forEach(i -> i.inverse()));
		panel.add(invBox, "5,7");
		createAndAddButton(panel, "clr", e -> model.clear(), "1,7");
		createAndAddButton(panel, "res", e -> {model.clearAll();
												stack.clear();}, "2,7");
		
		
		createAndAddButton(panel, "push", e -> stack.push(model.getValue()), "3,7");
		createAndAddButton(panel, "pop", e -> {
			if(stack.isEmpty()) {
				JOptionPane.showMessageDialog(panel, "Nothing is stored on stack");
				return;
			} else {
				model.setValue(stack.pop());
			}
		}, "4,7");

	}
	
	/**
	 * Initializes all digit buttons and adds them to given {@link Container}.
	 * @param parent given {@link Container}
	 */
	private void initNumberButtons(Container parent) {
		for(int i = 0; i < NUMBER_POSITIONS.length; i++) {
			createAndAddButton(parent, String.valueOf(i), new DigitListener(model, i), NUMBER_POSITIONS[i]);
		}
	}
	
	/**
	 * Initializes all simple operations buttons, like +,-,*,/, 1/x, ., +/-, =.
	 * It adds all buttons to given {@link Container}
	 * @param parent given {@link Container}
	 */
	private void initSimpleOperationButtons(Container parent) {
		createAndAddButton(parent, "+/-", e -> model.swapSign(), "5,4");
		createAndAddButton(parent, ".", e -> model.insertDecimalPoint(), "5,5");
		
		DoubleBinaryOperator ADD = (d1, d2) -> d1 + d2;
		DoubleBinaryOperator SUB = (d1, d2) -> d1 - d2;
		DoubleBinaryOperator MUL = (d1, d2) -> d1 * d2;
		DoubleBinaryOperator DIV = (d1, d2) -> {
			if(d2 == 0) {
				throw new IllegalArgumentException("Can't divide by zero.");
			}
			return d1 / d2;
		};
		createAndAddButton(parent, "+", new BinaryOperationListener(ADD, model, parent), "5, 6");
		createAndAddButton(parent, "-", new BinaryOperationListener(SUB, model, parent), "4, 6");
		createAndAddButton(parent, "*", new BinaryOperationListener(MUL, model, parent), "3, 6");
		createAndAddButton(parent, "/", new BinaryOperationListener(DIV, model, parent), "2, 6");
		
		ActionListener equalAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					model.setPendingBinaryOperation(null);
				} catch (Exception exc) {
					JOptionPane.showMessageDialog(parent, exc.getMessage(), "WARNING", JOptionPane.WARNING_MESSAGE);
				}
			}
		};
		createAndAddButton(parent, "=",  equalAction, "1, 6");
		
		createAndAddButton(parent, "1/x", e -> model.setValue(1.0 / model.getValue()), "2,1");
	}
	
	/**
	 * Initializes all inversable buttons and adds them to given {@link Container}.
	 * @param parent given {@link Container}
	 */
	private void initInversableButtons(Container parent) {
		createInversableButton(parent, "sin", 
				m -> m.setValue(sin(m.getValue())), m -> m.setValue(asin(m.getValue())), "2,2");
		createInversableButton(parent, "cos", 
				m -> m.setValue(cos(m.getValue())), m -> m.setValue(acos(m.getValue())), "3,2");
		createInversableButton(parent, "tan", 
				m -> m.setValue(tan(m.getValue())), m -> m.setValue(atan(m.getValue())), "4,2");
		createInversableButton(parent, "ctg", 
				m -> m.setValue(1.0/tan(m.getValue())), m -> m.setValue(1.0 / atan(m.getValue())), "5,2");
		
		createInversableButton(parent, "log", 
				m -> m.setValue(log10(m.getValue())), m -> m.setValue(pow(10, m.getValue())), "3,1");
		createInversableButton(parent, "ln", 
				m -> m.setValue(log(m.getValue())), m -> m.setValue(pow(E, m.getValue())), "4,1");
		
		DoubleBinaryOperator pow = (d1, d2) -> pow(d1, d2);
		DoubleBinaryOperator inverse_pow = (d1, d2) -> pow(d1, 1.0/d2);
		createInversableButton(parent, "x^n", 
				m -> m.setPendingBinaryOperation(pow), m -> m.setPendingBinaryOperation(inverse_pow), "5,1");
	}
	
	/**
	 * Factory method that creates {@link InverseButton} and adds it to given {@link Container}, to given
	 * constrait.
	 * @param parent given {@link Container}
	 * @param text button text
	 * @param consumer1 models original functionality of button
	 * @param consumer2 models inverse functionality of button
	 * @param constraits position of button
	 */
	private void createInversableButton(Container parent, String text, Consumer<CalcModel> consumer1,
			Consumer<CalcModel> consumer2, Object constraits) {
		InverseButton btn = new InverseButton(text, consumer1, consumer2);
		btn.addActionListener(e -> {
			try {
				btn.getCurrentConsumer().accept(model);
			} catch(Exception exc) {
				JOptionPane.showMessageDialog(parent, exc.getMessage(), "WARNING", JOptionPane.WARNING_MESSAGE);
			}
		});
		parent.add(btn, constraits);
		inversableBtns.add(btn);
	}
	
	/**
	 * Factory method that creates {@link JButton} and adds it to given {@link Container}, to given
	 * constrait.
	 * @param parent given {@link Container}
	 * @param text button text
	 * @param l listener that is added to button
	 * @param constraits position of button
	 */
	private void createAndAddButton(Container parent, String text, ActionListener l, Object constraits) {
		JButton btn = new JButton(text);
		btn.addActionListener(l);
		parent.add(btn, constraits);
	}
	
	/**
	 * Starts program.
	 * @param args zero arguments needed.
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new Calculator().setVisible(true));
	}
}
