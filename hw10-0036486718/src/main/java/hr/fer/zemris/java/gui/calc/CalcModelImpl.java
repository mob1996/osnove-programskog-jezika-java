package hr.fer.zemris.java.gui.calc;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.function.DoubleBinaryOperator;

/**
 * Implementation of {@link CalcModel}.
 * @author Mob
 *
 */
public class CalcModelImpl implements CalcModel {

	/**
	 * Decimal point
	 */
	private static final String DOT = ".";
	
	/**
	 * Minus sign
	 */
	private static final String MINUS = "-";

	private static final int MAX_DOUBLE_LEN = 308;
	
	/**
	 * Current value of model
	 */
	private String value = null;
	
	/**
	 * {@link List} of {@link CalcValueListener}s
	 */
	private List<CalcValueListener> listeners = new ArrayList<>();
	
	/**
	 * {@link Stack} that stores {@link Double} operands.
	 */
	Stack<Double> operands = new Stack<>();
	
	/**
	 * Pending {@link DoubleBinaryOperator}
	 */
	private DoubleBinaryOperator pendingOperation = null;
	
	/**
	 * Flag that says if value can be overwritteb
	 */
	private boolean overwrite = false;
	
	/**
	 * Flag that says if '=' was called before.
	 */
	private boolean multiple_equals = false;

	/**
	 * Adds {@link CalcValueListener} to {@link List} of listeners.
	 * @param l {@link CalcValueListener} that is being added.
	 */
	@Override
	public void addCalcValueListener(CalcValueListener l) {
		listeners.add(l);

	}

	/**
	 * Removes {@link CalcValueListener} from {@link List} of listeners.
	 * @param l {@link CalcValueListener} that is being removed.
	 */
	@Override
	public void removeCalcValueListener(CalcValueListener l) {
		listeners.remove(l);

	}

	/**
	 * @return {@link #toString()} parsed to double
	 */
	@Override
	public double getValue() {
		try {
			return Double.valueOf(toString());
		} catch (NumberFormatException exc) {
			throw new IllegalArgumentException("'" + toString() + "' can't be parsed into number.");
		}
	}

	/**
	 * Sets <code>value</code> property to given value and notifies listeners.
	 * @param value is new value of <code>value</code> property
	 */
	@Override
	public void setValue(double value) {
		if(Double.isNaN(value) || Double.isInfinite(value)) {
			throw new IllegalArgumentException("Value mustn't be NaN nor infinite");
		}
		this.value = String.valueOf(value);
		notifyListeners();
	}

	/**
	 * Clears <code>value</code> and notifies listeners.
 	 */
	@Override
	public void clear() {
		value = null;
		overwrite = false;
		notifyListeners();
	}

	/**
	 * Calls {@link #clear()} {@link #clearActiveOperand()} and sets <code>pendingOperation</code>
	 * to null.
	 * @see #clear()
	 * @see #clearActiveOperand()
	 */
	@Override
	public void clearAll() {
		clear();
		clearActiveOperand();
		pendingOperation = null;
	}

	/**
	 * Swaps sign of value and notifies listeners. 
	 */
	@Override
	public void swapSign() {
		if(value == null) return;
		value = toString().startsWith(MINUS) ? toString().substring(1) : MINUS + toString();
		notifyListeners();
	}

	/**
	 * Inserts decimal point into <code>value</code> if it doesn't already contain dot.
	 */
	@Override
	public void insertDecimalPoint() {
		if (overwrite == true) {
			overwrite = false;
			this.value = null;
		}

		if (!toString().contains(DOT)) {
			value = toString() + DOT;
			notifyListeners();
		}
	}

	/**
	 * Inserts given digit into value.
	 * @param digit value that is being inserted
	 */
	@Override
	public void insertDigit(int digit) {
		if(toString().length() >= MAX_DOUBLE_LEN) return;
		
		value = value == null || overwrite ? String.valueOf(digit) : value + String.valueOf(digit);
		overwrite = false;
		notifyListeners();
	}

	/**
	 * @return true if <code>activeOperand</code> is set. 
	 */
	@Override
	public boolean isActiveOperandSet() {
		return !operands.isEmpty();
	}

	/**
	 * @return value of <code>activeOperand</code>
	 * @throws IllegalStateException if {@link #isActiveOperandSet()} returns false.
	 */
	@Override
	public double getActiveOperand() {
		if (!isActiveOperandSet()) {
			throw new IllegalStateException("No active operand was set.");
		}
		return operands.peek();
	}

	/**
	 * Sets active operand.
	 * @param activeOperand that is set.
	 */
	@Override
	public void setActiveOperand(double activeOperand) {
		operands.push(activeOperand);
	}

	/**
	 * Cleares active operand.
	 */
	@Override
	public void clearActiveOperand() {
		operands.clear();

	}

	/**
	 * @return <code>pendingBinaryOperation</code>
	 */
	@Override
	public DoubleBinaryOperator getPendingBinaryOperation() {
		return pendingOperation;
	}

	/**
	 * Sets <code>pendingBinaryOperation</code> to given one.
	 * If given argument is null it is seen as calling "=". It used previous pendingBinaryOperation.
	 * @param op given {@link DoubleBinaryOperator}
	 */
	@Override
	public void setPendingBinaryOperation(DoubleBinaryOperator op) {
		if(op == null) {
			if(operands.size() < 2) {
				if(operands.size() == 1) {
					operands.push(Double.valueOf(toString()));
				} else {
					return;
				}
			}
			double right;
			if(this.multiple_equals) {
				right = operands.pop();
			} else {
				operands.pop();
				right = Double.valueOf(toString());
			}
			double left = operands.pop();
			double newValue = pendingOperation.applyAsDouble(left, right);
			setActiveOperand(newValue);
			setValue(newValue);
			setActiveOperand(right);
			this.multiple_equals = true;
			overwrite = true;
			return;
		}
		
		if(multiple_equals) {
			operands.clear();
		}
		
		if(isActiveOperandSet()) {
			if(operands.size() >= 2) {
				operands.pop();
			}
			double left = operands.pop();
			double right = Double.valueOf(toString());
		
			double newValue = pendingOperation.applyAsDouble(left, right);
			setActiveOperand(newValue);
			setValue(newValue);
			setActiveOperand(right);
		} else {
			setActiveOperand(Double.valueOf(toString()));
		}
		
		this.pendingOperation = op;
		this.multiple_equals = false;
		overwrite = true;
	}

	/**
	 * Notifies listeners of change in model state.
	 */
	private void notifyListeners() {
		clearLeadingZeroes();
		listeners.forEach(l -> l.valueChanged(this));
	}

	/**
	 * @return {@link String} <code>value</code> property
	 */
	@Override
	public String toString() {
		return value == null ? "0" : value;
	}
	
	private void clearLeadingZeroes() {
		if(value != null) {
			while(value.startsWith("0") && !value.startsWith("0.") && !value.equals("0")) {
				value = value.substring(1);
			}
		}
	}
	
}
