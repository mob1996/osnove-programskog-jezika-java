package hr.fer.zemris.java.gui.prim;

import static java.lang.Math.sqrt;

/**
 * Class is prime numbers generator.
 * First prim number is one. Next one can be aquired by calling method next().
 * @author Luka Mijić
 *
 */
public class PrimGenerator {
	
	/**
	 * Starting prime number.
	 */
	private int currentNumber = 1;
	
	/**
	 * @return next prime number
	 */
	public int next() {
		if(currentNumber == 1 || currentNumber == 2) {
			return currentNumber++;
		}
	
		while(!isPrime(currentNumber)) {
			currentNumber = currentNumber + 2;
		}
		int prime = currentNumber;
		currentNumber = currentNumber + 2;
		
		return prime;
	}
	
	/**
	 * Check if number is prime.
	 * @param number that is checked
	 * @return true if number is prime number, otherwise return false.
	 */
	private boolean isPrime(int number) {
		if(number == 2) return true;
		
		double lastCheck = sqrt(number);
		for(int i = 3; i <= lastCheck; i += 2) {
			if(number % i == 0) return false;
		}
		
		return true;
	}
	
	/**
	 * Resets generator to start.
	 * Next call on method {@link #next()} will return 1.
	 */
	public void reset() {
		currentNumber = 1;
	}
}
