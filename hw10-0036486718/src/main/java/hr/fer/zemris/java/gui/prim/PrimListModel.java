package hr.fer.zemris.java.gui.prim;

import java.util.ArrayList;
import java.util.List;

import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 * Class implements {@link ListModel} and models
 * list that contains prime numbers.
 * @author Luka Mijić
 *
 */
public class PrimListModel implements ListModel<Integer> {
	
	/**
	 * Stored prime numbers
	 */
	private List<Integer> elems = new ArrayList<>();
	
	/**
	 * Stored listeners 
	 */
	private List<ListDataListener> listeners = new ArrayList<>();
	
	/**
	 * Generator of primes number
	 */
	private PrimGenerator generator = new PrimGenerator();
	
	/**
	 * Creates {@link PrimListModel} that initialy stores
	 * one prim number.
	 */
	public PrimListModel() {
		next();
	}
	
	/**
	 * Adds {@link ListDataListener} to this model.
	 * @param l is {@link ListDataListener} that is added.
	 */
	@Override
	public void addListDataListener(ListDataListener l) {
		listeners.add(l);
	}

	/**
	 * @param i is index of element
	 * @return element at given index
	 */
	@Override
	public Integer getElementAt(int i) {
		return elems.get(i);
	}

	/**
	 * @return number of elements in the list
	 */
	@Override
	public int getSize() {
		return elems.size();
	}

	/**
	 * Remove given {@link ListDataListener} from listeners list.
	 * @param l is {@link ListDataListener} that is removed
	 */
	@Override
	public void removeListDataListener(ListDataListener l) {
		listeners.remove(l);
	}
	
	/**
	 * Adds next prime number to list.
	 */
	public void next() {
		int pos = elems.size();
		elems.add(generator.next());
		ListDataEvent event = new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, pos, pos);
		for(ListDataListener l : listeners) {
			l.intervalAdded(event);
		}
	}

}
