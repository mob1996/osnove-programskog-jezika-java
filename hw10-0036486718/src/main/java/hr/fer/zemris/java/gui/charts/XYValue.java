package hr.fer.zemris.java.gui.charts;

/**
 * Class models (x,y) value on the charts.
 * @author Luka Mijić
 *
 */
public class XYValue {

	/**
	 * x value
	 */
	private int x;
	
	/**
	 * y value
	 */
	private int y;
	
	/**
	 * Creates new {@link XYValue} with given properties.
	 * @param x value
	 * @param y value
	 */
	public XYValue(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	/**
	 * @return <code>x</code> value
	 */
	public int getX() {
		return x;
	}

	/**
	 * @return <code>y</code> value
	 */
	public int getY() {
		return y;
	}
	
	/**
	 * Parses new {@link XYValue} from given expression.
	 * Expression must be formed like: x,y
	 * @param expression that is parsed
	 * @return created {@link XYValue}
	 * @throws IllegalArgumentException if expression can't be parsed.
	 */
	public static XYValue parse(String expression) {
		try {
			String[] split = expression.split(",");
			return new XYValue(Integer.valueOf(split[0]), Integer.valueOf(split[1]));
		} catch (Exception exc) {
			throw new IllegalArgumentException("'" + expression + "' can't be parsed into XYvalue.");
		}
	}
}
