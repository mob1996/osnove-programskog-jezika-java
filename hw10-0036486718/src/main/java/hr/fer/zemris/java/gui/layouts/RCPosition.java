package hr.fer.zemris.java.gui.layouts;

/**
 * Class models position. 
 * It stores two read only int properties.
 * First one represents row number, other represents column number.
 * @author Luka Mijić
 *
 */
public class RCPosition {
	
	/**
	 * Row number
	 */
	private int row;
	
	/**
	 * Column number
	 */
	private int column;
	
	/**
	 * Creates {@link RCPosition}. 
	 * @param row 
	 * @param column
	 */
	public RCPosition(int row, int column) {
		super();
		this.row = row;
		this.column = column;
	}

	/**
	 * @return <code>row</code> property
	 */
	public int getRow() {
		return row;
	}

	/**
	 * @return <code>column</code> property
	 */
	public int getColumn() {
		return column;
	}

	/**
	 * Parses given string into {@link RCPosition}.
	 * String is expected to be formed as following. r,c
	 * @param expression that is parsed
	 * @return created {@link RCPosition}
	 */
	public static RCPosition parse(String expression) {
		String[] values = expression.split(",");
		if(values.length != 2) {
			throw new IllegalArgumentException("'" + expression + "' can't be parsed into RcPosition.");
		}
		
		String num = values[0].trim();
		try {
			int rows = Integer.valueOf(num);
			num = values[1].trim();
			int columns = Integer.valueOf(num);
			return new RCPosition(rows, columns);
		} catch(NumberFormatException exc) {
			throw new IllegalArgumentException("'" + num + "' is not parsable as int.");
		}
	}

	/**
	 * @return {@link String} representation of {@link RCPosition}.
	 * 			It can be parsed back into {@link RCPosition}.
	 */
	@Override
	public String toString() {
		return row + ", " + column;
	}
	
	/**
	 * @return hashcode of this instance of {@link RCPosition}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + column;
		result = prime * result + row;
		return result;
	}

	/**
	 * @return true if this and <code>obj</code> model same {@link RCPosition}.
	 * 			Same {@link RCPosition} has equal <code>row</code> and <code>column</code>
	 * 			properties.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RCPosition other = (RCPosition) obj;
		if (column != other.column)
			return false;
		if (row != other.row)
			return false;
		return true;
	}
	
	
	 
}
