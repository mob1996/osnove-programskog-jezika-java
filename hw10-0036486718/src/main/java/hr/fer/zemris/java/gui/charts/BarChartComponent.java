package hr.fer.zemris.java.gui.charts;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;

import javax.swing.JComponent;

/**
 * Class extends {@link JComponent}.
 * It draws chart from {@link BarChart}.
 * @author Luka Mijić
 *
 */

public class BarChartComponent extends JComponent {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Dim gray color
	 */
	private static final Color DIM_GRAY = Color.decode("#696969");
	
	/**
	 * Color used for drawing grids
	 */
	private static final Color GRID_COLOR = Color.decode("#F98866");
	
	/**
	 * Color used for fill column values on chart
	 */
	private static final Color BAR_COLOR = Color.decode("#1e88ed");
	
	/**
	 * Color used for background
	 */
	private static final Color BACKGROUND = Color.WHITE;
	
	/**
	 * Gap
	 */
	private int gap = 0;
	
	/**
	 * Factor that is used for determining font size
	 */
	private static final double FONT_FACTOR = 0.015;
	
	/**
	 * Factor that is used for determining gap
	 */
	private static final double GAP_FACTOR = 0.012;
	
	/**
	 * Factor that is used to determine stroke size.
	 */
	private static final double STROKE_FACTOR = 0.004;
	
	/**
	 * Insets where all properties are zero
	 */
	private static final Insets CHANGE_NOTHING = new Insets(0, 0, 0, 0);
	
	/**
	 * Chart model that is drawn
	 */
	private BarChart barChart;
	
	/**
	 * Minimum y value
	 */
	private int yMin;
	
	/**
	 * Number of vertical grids
	 */
	private int verticalGrids;
	
	/**
	 * Number of horizontal grids
	 */
	private int horizontalGrids;

	/**
	 * Format used for drawing y values on graph
	 */
	private String yFormat;

	/**
	 * Creates {@link BarChartComponent}.
	 * @param barChart is model that is used for drawing
	 * @param p path to file that stores {@link BarChart}
	 */
	public BarChartComponent(BarChart barChart) {
		super();
		this.barChart = barChart;
		setUp();
	}
	
	/**
	 * Sets up some properties.
	 */
	private void setUp() {
		yMin = barChart.getyMin();
		
		while((barChart.getyMax() - yMin) % barChart.getStep() != 0) {
			yMin++;
		}
		
		int maxYLenght = 0;
		for(XYValue v:barChart.getXyValues()) {
			maxYLenght = Math.max(maxYLenght, String.valueOf(v.getY()).length());
		}
		
		yFormat = "%" + maxYLenght + "d";
		
		verticalGrids = (barChart.getyMax() - yMin) / barChart.getStep();
		horizontalGrids = barChart.getXyValues().size();
	}
	
	/**
	 * Draws component using instance of {@link Graphics}.
	 * @param g {@link Graphics}
	 */
	@Override
	protected void paintComponent(Graphics g) {
		Insets insets = getInsets();
		Rectangle wS = getBounds();
		
		Graphics2D g2d = initGraphics2D(g, wS);
		g2d.setColor(BACKGROUND);
		g2d.fillRect(0, 0, wS.width, wS.height);
		g2d.setColor(DIM_GRAY);
		wS = rectangleFromRectangleAndInsets(wS, insets);
		
		wS = drawAxisLabels(g2d, wS);
		wS = drawArrows(g2d, wS);
		wS = drawHorizontalGrids(g2d, wS);
		wS = drawBars(g2d, wS);
		wS = drawVerticalGrids(g2d, wS);
		wS = drawAxis(g2d, wS);
		
		wS = drawNumberLabels(g2d, wS);
		
	}
	
	/**
	 * Turns {@link Graphics} to {@link Graphics2D} and sets
	 * sets stroke, font size and gap.
	 * @param g {@link Graphics} that is casted into {@link Graphics2D}
	 * @param r {@link Rectangle} used for setting up {@link Graphics2D}
	 * @return {@link Graphics2D}
	 */
	private Graphics2D initGraphics2D(Graphics g, Rectangle r) {
		Graphics2D g2d = (Graphics2D) g;
		g2d.setStroke(new BasicStroke((float) (STROKE_FACTOR * (r.height + r.width))));
		int fontSize = (int) (FONT_FACTOR * (r.width + r.height));
		g2d.setFont(g2d.getFont().deriveFont(Font.BOLD, fontSize));
		gap = (int) (GAP_FACTOR * (r.width + r.height));
		
		return g2d;
	}
	
	/**
	 * Draws axis labels. 
	 * @param g2d is used for drawing
	 * @param r provides available {@link Rectangle} for drawing.
	 * @return new available space for drawing
	 */
	private Rectangle drawAxisLabels(Graphics2D g2d, Rectangle r) {
		FontMetrics fm = g2d.getFontMetrics();
		
		int w = fm.stringWidth(barChart.getxLabel());
		int h = fm.getDescent();
		g2d.drawString(barChart.getxLabel(), (r.x + r.width - w) / 2, r.y  + r.height - gap - h);
		
		w = fm.stringWidth(barChart.getyLabel());
		h = fm.getAscent();
		int x = r.x + gap + h;
		int y = (r.height - w) / 2 + w;
		AffineTransform at = new AffineTransform();
		at.rotate(-Math.PI / 2);
		g2d.setTransform(at);
		g2d.drawString(barChart.getyLabel(), -y, x);
		at.rotate(Math.PI/2);
		g2d.setTransform(at);
		
		Insets newInsets = new Insets(gap, 4 * gap + fm.getHeight(), 4 * gap + fm.getHeight(), gap);
		return rectangleFromRectangleAndInsets(r, newInsets);
	}
	
	/**
	 * Draws axis arrows.
	 * @param g2d is used for drawing
	 * @param r provides available {@link Rectangle} for drawing.
	 * @return new available space for drawing
	 */
	private Rectangle drawArrows(Graphics2D g2d, Rectangle r) {
		
		int[] xPointsTop = {r.x, r.x - gap / 2, r.x + gap / 2};
		int[] yPointsTop = {r.y - gap, r.y, r.y};
		g2d.fillPolygon(xPointsTop, yPointsTop, xPointsTop.length);
		
		int[] xPointsBot = {r.x + r.width, r.x + r.width - gap, r.x + r.width - gap};
		int[] yPointsBot = {r.y + r.height, r.y + r.height + gap / 2, r.y + r.height - gap / 2};
		g2d.fillPolygon(xPointsBot, yPointsBot, xPointsBot.length);
		
		Insets i = new Insets(0, 0, 0, gap);
		return rectangleFromRectangleAndInsets(r, i);
	}
	
	/**
	 * Draws x and y axis. 
	 * @param g2d is used for drawing
	 * @param r provides available {@link Rectangle} for drawing.
	 * @return new available space for drawing
	 */
	private Rectangle drawAxis(Graphics2D g2d, Rectangle r) {
		
		g2d.drawLine(r.x, r.y, r.x, r.y + r.height + gap / 2);
		g2d.drawLine(r.x - gap / 2, r.y + r.height, r.x + r.width, r.y + r.height);
		return rectangleFromRectangleAndInsets(r, CHANGE_NOTHING);
	}
	
	/**
	 * Draws horizontal grids.
	 * @param g2d is used for drawing
	 * @param r provides available {@link Rectangle} for drawing.
	 * @return new available space for drawing
	 */
	private Rectangle drawHorizontalGrids(Graphics2D g2d, Rectangle r) {
		g2d.setColor(GRID_COLOR);
		g2d.setStroke(new BasicStroke((float) (STROKE_FACTOR / 2 * (r.height + r.width))));
		int rowHeight = (int) (r.getHeight() / verticalGrids);
		for(int i = 1; i <= verticalGrids; i++) {
			int y = r.y + r.height - i * rowHeight;
			g2d.drawLine(r.x - gap / 2 , y, r.x + r.width + gap, y);
		}
	
		return rectangleFromRectangleAndInsets(r, CHANGE_NOTHING);
	}
	
	/**
	 * Draws vertical grids.
	 * @param g2d is used for drawing
	 * @param r provides available {@link Rectangle} for drawing.
	 * @return new available space for drawing
	 */
	private Rectangle drawVerticalGrids(Graphics2D g2d, Rectangle r) {
		g2d.setColor(GRID_COLOR);
		g2d.setStroke(new BasicStroke((float) (STROKE_FACTOR / 2 * (r.height + r.width))));
		
		int columnWidth = (int) (r.getWidth() / horizontalGrids);
		for(int i = 1; i <= horizontalGrids; i++) {
			int x = r.x + i * columnWidth;
			g2d.drawLine(x, r.y, x, r.y + r.height + gap / 2);
		}
		
		g2d.setColor(DIM_GRAY);
		g2d.setStroke(new BasicStroke((float) (STROKE_FACTOR * (r.height + r.width))));
		return rectangleFromRectangleAndInsets(r, CHANGE_NOTHING);
	}
	
	/**
	 * Draws numbers that shows weight of each horizontal grid.
	 * @param g2d is used for drawing
	 * @param r provides available {@link Rectangle} for drawing.
	 * @return new available space for drawing
	 */
	private Rectangle drawNumberLabels(Graphics2D g2d, Rectangle r) {
		Font oldFont = g2d.getFont();
		int fontSize = (int) (FONT_FACTOR * (r.width + r.height));
		g2d.setFont(g2d.getFont().deriveFont(Font.BOLD, fontSize));
		
		int rowHeight = (int) (r.getHeight() / verticalGrids);
		int fontHeight = g2d.getFont().getSize();
		int yCurr = yMin;
		for(int i = 0; i <= verticalGrids; i++) {
			int y = r.y + r.height - i * rowHeight + fontHeight / 2;
			g2d.drawString(String.format(yFormat, yCurr), r.x - 2 * gap, y);
			yCurr += barChart.getStep();
		}
		
		int columnWidth = (int) (r.getWidth() / horizontalGrids);
		FontMetrics fm = g2d.getFontMetrics();
		for(int i = 0; i < horizontalGrids; i++) {
			String xLabel = String.valueOf(barChart.getXyValues().get(i).getX());
			int numWidth = fm.stringWidth(String.valueOf(xLabel));
			
			int x = r.x + i * columnWidth + (columnWidth - numWidth) / 2;
			g2d.drawString(xLabel, x, r.y + r.height + 2 * gap);
		}
		g2d.setFont(oldFont);
		return rectangleFromRectangleAndInsets(r, CHANGE_NOTHING);
	}
	
	/**
	 * Draws chart values. 
	 * @param g2d is used for drawing
	 * @param r provides available {@link Rectangle} for drawing.
	 * @return new available space for drawing
	 */
	private Rectangle drawBars(Graphics g2d, Rectangle r) {
		g2d.setColor(BAR_COLOR);
		
		int rowHeight = (int) (r.getHeight() / verticalGrids);
		int columnWidth = (int) (r.getWidth() / horizontalGrids);
		for(int i = 0; i < horizontalGrids; i++) {
			double y = (barChart.getXyValues().get(i).getY() - yMin) / (double)barChart.getStep();
			g2d.fillRect(r.x + i * columnWidth, (int)(r.y + r.height - y * rowHeight), columnWidth, (int) (y * rowHeight));
		}
		g2d.setColor(DIM_GRAY);
		return rectangleFromRectangleAndInsets(r, CHANGE_NOTHING);
	}
	
	/**
	 * Creates new {@link Rectangle} from existing {@link Rectangle} and {@link Insets}.
	 * @param r starting {@link Rectangle}
	 * @param i {@link Insets} used for creating new {@link Rectangle}
	 * @return new {@link Rectangle}
	 */
	private static Rectangle rectangleFromRectangleAndInsets(Rectangle r, Insets i) {
		return new Rectangle(r.x + i.left, r.y + i.top, r.width - i.left - i.right, r.height - i.top - i.bottom);
	}
}
