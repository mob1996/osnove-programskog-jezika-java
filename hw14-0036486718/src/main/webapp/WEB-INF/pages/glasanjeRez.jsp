<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="contextPath" value="${pageContext.request.contextPath }"/>
<html>
	<head>
		<style type="text/css">
			table {
    			font-family: arial, sans-serif;
    			border-collapse: collapse;
   				width: 100%;
			}

			td, th {
   		 		border: 1px solid #dddddd;
    			text-align: left;
    			padding: 8px;
			}

			tr:nth-child(even) {
    			background-color: #dddddd;
			}
		</style>
	</head>

	<body>
	
		<h1>Rezultati glasanja</h1>
		<p>Ovo su rezultati glasanja.</p>
	
		<table border="1" class="rez" style="width:30%">
			<thead><tr><th>Kandidati</th><th>Broj glasova</th></tr></thead>
			<tbody>
				<c:forEach var="option" items="${options }">
					<tr><td>${option.optionTitle }</td><td>${option.votesCount }</td></tr>		
				</c:forEach>
			</tbody>
		</table>
	
		<h2>Grafički prikaz rezultata</h2>
		<img alt="Pie-chart" src="${contextPath }/servleti/glasanje-grafika?pollID=${pollID }" width="600" height="600"/>
	
		<h2>Rezultati u XLS formatu</h2>
		<p>Rezultati u XLS formatu dostupni su <a href="${contextPath }/servleti/glasanje-xls?pollID=${pollID }"> ovdje.</a></p>
	
	
		<h2>Razno</h2>
		<p>Pobjednici:</p>
		<ul>
			<c:forEach var="winner" items="${winningOptions }">
					<li><a href="${winner.optionLink }" target="_blank"> ${winner.optionTitle }</a></li>
			</c:forEach>
		</ul>
		
		<a href="${contextPath }/index.html">INDEX</a>	
	</body>
</html>