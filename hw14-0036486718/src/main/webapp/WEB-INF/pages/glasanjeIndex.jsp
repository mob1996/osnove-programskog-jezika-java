<%@ page contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="contextPath" value="${pageContext.request.contextPath }"/>
<html>
	<body>
		<h1><font size="15"> ${poll.title }</font></h1>
		<p>${poll.message }</p>
		
		<ol>
			<c:forEach var="option" items="${pollOptions }">
				<li><a href="glasanje-glasaj?id=${option.id }&pollID=${poll.id }">${option.optionTitle }</a></li>
			</c:forEach>
		</ol>
		
		<a href="${contextPath }/index.html">INDEX</a>
	</body>
</html>
