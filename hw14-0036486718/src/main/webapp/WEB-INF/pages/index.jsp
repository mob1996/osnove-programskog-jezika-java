<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="contextPath" value="${pageContext.request.contextPath }"/>

<html>
		<h1><font size="15"> Aktivne ankete</font></h1>
		<p>Odaberite anketu u kojoj želite sudjelovati.</p>
		
		<ol>
			<c:forEach var="poll" items="${polls }">
				<li><a href="${contextPath }/servleti/glasanje?pollID=${poll.id }">${poll.title }</a></li>
			</c:forEach>
		</ol>
	</body>
</html>