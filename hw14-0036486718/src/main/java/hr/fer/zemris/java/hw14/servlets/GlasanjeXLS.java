package hr.fer.zemris.java.hw14.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import hr.fer.zemris.java.hw14.dao.DAOProvider;
import hr.fer.zemris.java.hw14.model.PollOption;

/**
 * Class extends {@link HttpServlet}.
 * It overrides only one method {@link HttpServlet#doGet}.
 * Loads poll contest information and turns it
 * into .xls format. Call {@link HSSFWorkbook#write(java.io.OutputStream)}
 * and give {@link HttpServletResponse#getOutputStream()} as argument.
 * 
 * @author Luka Mijić
 *
 */
public class GlasanjeXLS extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		long pollID;
		try {
			pollID = Long.valueOf(req.getParameter("pollID"));
		} catch (NumberFormatException | NullPointerException exc) {
			resp.sendError(400, "No valid pollID parameter given.");
			return;
		}
		
		if(DAOProvider.getDao().getPoll(pollID) == null) {
			resp.sendError(400, "No poll associated with given id.");
			return;
		}
		
		List<PollOption> options = DAOProvider.getDao().getPollOptions(pollID);
		
		HSSFWorkbook hwb = new HSSFWorkbook();
		HSSFSheet sheet = hwb.createSheet("Voting results");
		
		for(int i = 0; i < options.size(); i++) {
			HSSFRow row = sheet.createRow(i);
			row.createCell(0).setCellValue(options.get(i).getOptionTitle());
			row.createCell(1).setCellValue(options.get(i).getVotesCount());
		}
		
		req.getSession().removeAttribute("bandInfo");
		resp.setContentType("application/vnd.ms-excel");
		resp.setHeader("Content-Disposition", "attachment; filename=\"tablica.xls\"");
	
		hwb.write(resp.getOutputStream());
		hwb.close();
	}

}
