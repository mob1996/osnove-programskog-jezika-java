package hr.fer.zemris.java.hw14.util.database;

/**
 * This class stores names of columns in 
 * "Polls" table.
 * @author Luka Mijić
 *
 */
public class PollColumns {

	public static final String TABLE_NAME = "Polls";
	public static final String ID = "id";
	public static final String TITLE = "title";
	public static final String MESSAGE = "message";
}
