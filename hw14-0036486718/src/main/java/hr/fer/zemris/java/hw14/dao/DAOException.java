package hr.fer.zemris.java.hw14.dao;

/**
 * Custom exception that extends {@link RuntimeException}.
 * This exception is thrown when error occurs while working with
 * data databases.
 * @author Luka Mijić
 *
 */
public class DAOException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * @see RuntimeException
	 */
	public DAOException() {
	}

	/**
	 * @see RuntimeException
	 */
	public DAOException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @see RuntimeException
	 */
	public DAOException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @see RuntimeException
	 */
	public DAOException(String message) {
		super(message);
	}

	/**
	 * @see RuntimeException
	 */
	public DAOException(Throwable cause) {
		super(cause);
	}
}