package hr.fer.zemris.java.hw14;

import java.beans.PropertyVetoException;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.sql.ConnectionPoolDataSource;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mchange.v2.c3p0.DataSources;

import hr.fer.zemris.java.hw14.dao.DAOProvider;
import hr.fer.zemris.java.hw14.util.PropertyFilesNames;

/**
 * Extends {@link ServletContextListener}.
 * Method {@link ServletContextListener#contextInitialized(ServletContextEvent)}
 * is called at the start and {@link ServletContextListener#contextDestroyed(ServletContextEvent)}
 * is called at the end of web application life cycle. 
 * 
 * In start method configuration files are read, 
 * pool of {@link Connection}s to database is created and
 * if data isn't already stored new data is created from
 * property file.
 * 
 * At the end of life cycle {@link ConnectionPoolDataSource} is
 * destroyed. 
 * @author Luka Mijić
 *
 */
public class ServerSetup implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		Path dbPropertiesPath = Paths.get(
			sce.getServletContext().getRealPath("WEB-INF/" + PropertyFilesNames.DB_SETTINGS)
		);
		
		if(!Files.exists(dbPropertiesPath)) {
			throw new RuntimeException("Database properties file doesn't exist.");
		}
		
		try {
			Properties dbProperties = new Properties();
			ComboPooledDataSource cpds = new ComboPooledDataSource();
			BufferedReader dbPropStream = Files.newBufferedReader(dbPropertiesPath, StandardCharsets.UTF_8);
			dbProperties.load(dbPropStream);
			dbPropStream.close();
			
			String dbHost = dbProperties.getProperty("host");
			String dbPort = dbProperties.getProperty("port");
			String dbName = dbProperties.getProperty("name");
			String dbUser = dbProperties.getProperty("user");
			String dbPw = dbProperties.getProperty("password");
			
			List<String> tableNames = parseTableNames(dbProperties.getProperty("table.names"));
			
			String connectionURL = String.format("jdbc:derby://%s:%s/%s;user=%s;password=%s"
												, dbHost
												, dbPort
												, dbName
												, dbUser
												, dbPw
									);
			cpds.setDriverClass("org.apache.derby.jdbc.ClientDriver");
			
			cpds.setMinPoolSize(5);
			cpds.setMaxPoolSize(20);
			cpds.setAcquireIncrement(5);
			cpds.setInitialPoolSize(5);
			
			cpds.setJdbcUrl(connectionURL);
			sce.getServletContext().setAttribute("hr.fer.zemris.dbpool", cpds);
			
			Connection dbCon = cpds.getConnection();
			populateDatabase(sce, dbCon, tableNames);
			dbCon.close();
			
		} catch (IOException e) {
			throw new RuntimeException("Error reading properties file.", e);
		} catch (NullPointerException e) {
			throw new RuntimeException("Needed property missing.", e);
		} catch (PropertyVetoException e) {
			throw new RuntimeException("Error starting connection pool", e);
		} catch (SQLException e) {
			throw new RuntimeException("Can't connect to database during startup.", e);
		}
	}

	/**
	 * Method reads property files used for creating database tables
	 * and entries. 
	 * Checks if tables are already created if they aren't using
	 * given {@link Connection} send appropriate SQL queries stored
	 * in property files.
	 * @param sce {@link ServletContextEvent} used for getting {@link ServletContext#getRealPath(String)}
	 * @param dbCon {@link Connection} to database
	 * @param tableNames names of tables that are created
	 * @throws IOException
	 * @throws SQLException
	 */
	private void populateDatabase(ServletContextEvent sce, Connection dbCon, List<String> tableNames) throws IOException, SQLException {
		Path dbCreatePath = Paths.get(
				sce.getServletContext().getRealPath("WEB-INF/" + PropertyFilesNames.DB_CREATION_COMMANDS)
				);
		Path dbPopulatePath = Paths.get(
				sce.getServletContext().getRealPath("WEB-INF/" + PropertyFilesNames.DB_POPULATE_TABLES)
				);
		
		Properties createProperties = new Properties();
		Properties populateProperties = new Properties();
		
		BufferedReader createPropReader = Files.newBufferedReader(dbCreatePath, StandardCharsets.UTF_8);
		BufferedReader populatePropReader = Files.newBufferedReader(dbPopulatePath, StandardCharsets.UTF_8);
		
		createProperties.load(createPropReader);
		populateProperties.load(populatePropReader);
		
		createPropReader.close();
		populatePropReader.close();
		
		Set<String> existingTables = DAOProvider.getDao().getTableNames(dbCon);
		
		for(String tableName:tableNames) {
			if(existingTables.contains(tableName.toLowerCase())) {
				continue;
			}
			
			String sqlCreate = createProperties.getProperty(tableName);
			String sqlPopulate = populateProperties.getProperty(tableName);
			
			PreparedStatement createStatement = dbCon.prepareStatement(sqlCreate);
			createStatement.executeUpdate();
			
			PreparedStatement populateStatement = dbCon.prepareStatement(sqlPopulate);
			populateStatement.executeUpdate();
			
			createStatement.close();
			populateStatement.close();
		}
		
	}
	
	/**
	 * Parses {@link String} that separates table names using ','.
	 * @param tables {@link String} that is parsed
	 * @return {@link List} of table names
	 */
	private List<String> parseTableNames(String tables){
		List<String> tableNames = new ArrayList<>();
		for(String name:tables.split(",")) {
			tableNames.add(name.trim());
		}
		return tableNames;
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		ComboPooledDataSource cpds = (ComboPooledDataSource)sce.getServletContext().getAttribute("hr.fer.zemris.dbpool");
		if(cpds!=null) {
			try {
				DataSources.destroy(cpds);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	

}
