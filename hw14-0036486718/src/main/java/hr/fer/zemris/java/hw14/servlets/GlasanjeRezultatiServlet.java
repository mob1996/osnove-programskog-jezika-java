package hr.fer.zemris.java.hw14.servlets;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw14.dao.DAOProvider;
import hr.fer.zemris.java.hw14.model.PollOption;

/**
 * Class extends {@link HttpServlet}.
 * It overrides only one method {@link HttpServlet#doGet}.
 * Loads option contest information and stores it into
 * {@link HttpServletRequest#setAttribute(String, Object)} and
 * dispatches to appropriate .jsp.
 * @author Luka Mijić
 *
 */
public class GlasanjeRezultatiServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		long pollID;
		try {
			pollID = Long.valueOf(req.getParameter("pollID"));
		} catch (NumberFormatException | NullPointerException exc) {
			resp.sendError(400, "No valid pollID parameter given.");
			return;
		}
		
		if(DAOProvider.getDao().getPoll(pollID) == null) {
			resp.sendError(400, "No poll associated with given id.");
			return;
		}
		
		List<PollOption> options = DAOProvider.getDao().getPollOptions(pollID);
		options.sort((o1, o2) -> Long.compare(o2.getVotesCount(), o1.getVotesCount()));
		
		long maxVotes = options.stream()
				   			   .mapToLong(b -> b.getVotesCount())
				   			   .max().orElse(0);
		List<PollOption> winningOptions = options.stream()
												   .filter(b -> b.getVotesCount() == maxVotes)
												   .collect(Collectors.toList());
		
		req.setAttribute("options", options);
		req.setAttribute("winningOptions", winningOptions);
		req.setAttribute("pollID", pollID);
		
		req.getRequestDispatcher("/WEB-INF/pages/glasanjeRez.jsp")
								.forward(req, resp);
	}
}
