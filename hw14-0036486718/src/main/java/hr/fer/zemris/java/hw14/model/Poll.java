package hr.fer.zemris.java.hw14.model;

/**
 * Class models poll. 
 * It contains properties such as id, title and message.
 * @author Luka Mijić
 *
 */
public class Poll {

	/**
	 * id of pool
	 */
	private long id;
	
	/**
	 * Title of poll
	 */
	private String title;
	
	/**
	 * Message of poll.
	 */
	private String message;
	
	/**
	 * Default {@link Poll} constructor.
	 */
	public Poll() {
		
	}
	
	/**
	 * Creates {@link Poll} instance,
	 * @param id of poll
	 * @param title of poll
	 * @param message of poll
	 */
	public Poll(long id, String title, String message) {
		super();
		this.id = id;
		this.title = title;
		this.message = message;
	}

	/**
	 * @return id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id is new poll id
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return poll title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title is new poll title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return message of title
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message is new message of title
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
}
