package hr.fer.zemris.java.hw14.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw14.dao.DAOProvider;

/**
 * Class extends {@link HttpServlet}.
 * It overrides only one method {@link HttpServlet#doGet}.
 * Loads option information, store it to {@link HttpServletRequest#setAttribute(String, Object)}
 * and dispatches it to appropriate .jsp file.
 * @author Luka Mijić
 *
 */
public class GlasanjeServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		long pollID;
		try {
			pollID = Long.valueOf(req.getParameter("pollID"));
		} catch (NumberFormatException | NullPointerException exc) {
			resp.sendError(400, "No valid pollID parameter given.");
			return;
		}
		
		if(DAOProvider.getDao().getPoll(pollID) == null) {
			resp.sendError(400, "No poll associated with given id.");
			return;
		}
		
		req.setAttribute("poll", DAOProvider.getDao().getPoll(pollID));
		req.setAttribute("pollOptions", DAOProvider.getDao().getPollOptions(pollID));
		
		req.getRequestDispatcher("/WEB-INF/pages/glasanjeIndex.jsp")
								.forward(req, resp);
	}

}
