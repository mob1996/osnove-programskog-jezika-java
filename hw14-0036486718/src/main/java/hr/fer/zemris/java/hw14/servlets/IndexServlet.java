package hr.fer.zemris.java.hw14.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw14.dao.DAOProvider;
import hr.fer.zemris.java.hw14.model.Poll;

/**
 * {@link HttpServlet} that loads information
 * about active {@link Poll}s into {@link List} and stores it
 * into {@link HttpServletRequest#setAttribute(String, Object)}.
 * After that dispatches request to appropriate .jsp file
 * @author Luka Mijić
 *
 */
public class IndexServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setAttribute("polls", DAOProvider.getDao().getPolls());
		
		req.getRequestDispatcher("/WEB-INF/pages/index.jsp")
								.forward(req, resp);
	}
}
