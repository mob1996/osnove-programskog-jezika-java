package hr.fer.zemris.java.hw14.util;

/**
 * This utility class stores names of property files
 * used for server configuration;
 * @author Luka Mijić
 *
 */
public class PropertyFilesNames {

	public static final String DB_SETTINGS = "dbSettings.properties";
	public static final String DB_CREATION_COMMANDS = "dbTableCreationCommands.properties";
	public static final String DB_POPULATE_TABLES = "dbPopulateTables.properties";
}
