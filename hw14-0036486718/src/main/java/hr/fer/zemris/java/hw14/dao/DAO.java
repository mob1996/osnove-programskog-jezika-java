package hr.fer.zemris.java.hw14.dao;

import java.sql.Connection;
import java.util.List;
import java.util.Set;

import hr.fer.zemris.java.hw14.model.Poll;
import hr.fer.zemris.java.hw14.model.PollOption;

/**
 * Sučelje prema podsustavu za perzistenciju podataka.
 * 
 * @author marcupic
 *
 */
public interface DAO {

	/**
	 * @return {@link List} of ids stored in table 'Polls'
	 * @throws DAOException
	 */
	List<Long> getPollIds() throws DAOException;
	
	/**
	 * @param pollId is id of wanted poll
	 * @return {@link Poll} if poll associated to given id exists,
	 * 			otherwise return null
	 * @throws DAOException
	 */
	Poll getPoll(long pollId) throws DAOException;
	
	/**
	 * @return {@link List} of all active {@link Poll}s.
	 * @throws DAOException
	 */
	List<Poll> getPolls() throws DAOException;
	
	/**
	 * @return {@link List} of ids stored in table 'PollOptions'
	 * @throws DAOException
	 */
	List<Long> getPollOptionsIds() throws DAOException;
	
	/**
	 * @param optionId is id of wanted option 
	 * @return {@link PollOption} if option associated with given id exits,
	 * 			otherwise return null
	 * @throws DAOException
	 */
	PollOption getPollOption(long optionId) throws DAOException;
	
	/**
	 * @param pollId is id that associates {@link PollOption} to {@link Poll}
	 * @return {@link List} of all {@link PollOption} associated with
	 * 			that is associated with given id.
	 * @throws DAOException
	 */
	List<PollOption> getPollOptions(long pollId) throws DAOException;
	
	/**
	 * @param con for accessing database
	 * @return {@link Set} of table names
	 * @throws DAOException
	 */
	Set<String> getTableNames(Connection con) throws DAOException;
	
	/**
	 * Increases value of column votesCount by 1 in row whose
	 * id column is same as given id.
	 * @param optionId id of option
	 * @throws DAOException
	 */
	void castVote(long optionId) throws DAOException;
}