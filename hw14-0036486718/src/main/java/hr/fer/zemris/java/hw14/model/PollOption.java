package hr.fer.zemris.java.hw14.model;

/**
 * Class models poll options.. 
 * It contains properties such as id, optionTitle etc.
 * @author Luka Mijić
 *
 */
public class PollOption {

	/**
	 * id of option
	 */
	private long id;
	
	/**
	 * Option title
	 */
	private String optionTitle;
	
	/**
	 * Option link
	 */
	private String optionLink;
	
	/**
	 * Poll id
	 */
	private long pollId;
	
	/**
	 * Number of votes for given option
	 */
	private long votesCount;
	
	/**
	 * Default constructor.
	 */
	public PollOption() {
		
	}

	/**
	 * Creates new instance of {@link PollOption}
	 * @param id of option
	 * @param optionTitle title of option
	 * @param optionLink link for option
	 * @param pollId is id of {@link Poll} to which option is associated to
	 * @param votesCount number of votes option received
	 */
	public PollOption(long id, String optionTitle, String optionLink, long pollId, long votesCount) {
		super();
		this.id = id;
		this.optionTitle = optionTitle;
		this.optionLink = optionLink;
		this.pollId = pollId;
		this.votesCount = votesCount;
	}

	/**
	 * @return id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id is new id of option
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return title of option
	 */
	public String getOptionTitle() {
		return optionTitle;
	}

	/**
	 * @param optionTitle is new title of option
	 */
	public void setOptionTitle(String optionTitle) {
		this.optionTitle = optionTitle;
	}

	/**
	 * @return link of option
	 */
	public String getOptionLink() {
		return optionLink;
	}

	/**
	 * @param optionLink is new link of option
	 */
	public void setOptionLink(String optionLink) {
		this.optionLink = optionLink;
	}

	/**
	 * @return id of {@link Poll} to which option is associated to.
	 */
	public long getPollId() {
		return pollId;
	}

	/**
	 * @param pollId is new id of {@link Poll} to which option is associated to.
	 */
	public void setPollId(long pollId) {
		this.pollId = pollId;
	}

	/**
	 * @return number of votes received 
	 */
	public long getVotesCount() {
		return votesCount;
	}

	/**
	 * @param votesCount is new value of votesCount parameter
	 */
	public void setVotesCount(long votesCount) {
		this.votesCount = votesCount;
	}
}
