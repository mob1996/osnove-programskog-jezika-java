package hr.fer.zemris.java.hw14.dao.sql;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import hr.fer.zemris.java.hw14.dao.DAO;
import hr.fer.zemris.java.hw14.dao.DAOException;
import hr.fer.zemris.java.hw14.model.Poll;
import hr.fer.zemris.java.hw14.model.PollOption;
import hr.fer.zemris.java.hw14.util.database.PollColumns;
import hr.fer.zemris.java.hw14.util.database.PollOptionColumns;

/**
 * Ovo je implementacija podsustava DAO uporabom tehnologije SQL. Ova
 * konkretna implementacija očekuje da joj veza stoji na raspolaganju
 * preko {@link SQLConnectionProvider} razreda, što znači da bi netko
 * prije no što izvođenje dođe do ove točke to trebao tamo postaviti.
 * U web-aplikacijama tipično rješenje je konfigurirati jedan filter 
 * koji će presresti pozive servleta i prije toga ovdje ubaciti jednu
 * vezu iz connection-poola, a po zavrsetku obrade je maknuti.
 *  
 * @author marcupic
 */
public class SQLDAO implements DAO {

	@Override
	public List<Long> getPollIds() throws DAOException {
		List<Long> idList = new ArrayList<>();
		Connection con = SQLConnectionProvider.getConnection();
		
		String sqlQuery = "select id from Polls order by id";
		
		try(PreparedStatement pst = con.prepareStatement(sqlQuery)) {
			try (ResultSet rset = pst.executeQuery()){
				while(rset != null && rset.next()) {
					idList.add(rset.getLong(PollColumns.ID));
				}
			} 
		} catch (Exception e) {
			throw new DAOException("Error getting polls ids", e);
		}
		
		return idList;
	}

	@Override
	public Poll getPoll(long pollId) throws DAOException {
		Connection con = SQLConnectionProvider.getConnection();
		String sqlQuery = "select * from Polls where id=?";
		
		try(PreparedStatement pst = con.prepareStatement(sqlQuery)) {
			pst.setLong(1, pollId);
			try (ResultSet rset = pst.executeQuery()){
				if(rset !=null && rset.next()) {
					return createPoll(rset);
				}
			} 
		} catch (Exception e) {
			throw new DAOException("Error getting requested poll.", e);
		}
		return null;
	}

	@Override
	public List<Poll> getPolls() throws DAOException {
		List<Poll> polls = new ArrayList<>();
		Connection con = SQLConnectionProvider.getConnection();
		String sqlQuery = "select * from Polls";
		try(PreparedStatement pst = con.prepareStatement(sqlQuery)) {
			try (ResultSet rset = pst.executeQuery()){
				if(rset != null) {
					while(rset.next()) {
						polls.add(createPoll(rset));
					}
				}
			} 
		} catch (Exception e) {
			throw new DAOException("Error getting polls.", e);
		}
		return polls;
	}

	@Override
	public List<Long> getPollOptionsIds() throws DAOException {
		List<Long> idList = new ArrayList<>();
		Connection con = SQLConnectionProvider.getConnection();
		
		String sqlQuery = "select id from PollOptions order by id";
		
		try(PreparedStatement pst = con.prepareStatement(sqlQuery)) {
			try (ResultSet rset = pst.executeQuery()){
				while(rset != null && rset.next()) {
					idList.add(rset.getLong(PollOptionColumns.ID));
				}
			} 
		} catch (Exception e) {
			throw new DAOException("Error getting poll options ids", e);
		}
		
		return idList;
	}

	@Override
	public PollOption getPollOption(long optionId) throws DAOException {
		Connection con = SQLConnectionProvider.getConnection();
		String sqlQuery = "select * from PollOptions where id=?";
		
		try(PreparedStatement pst = con.prepareStatement(sqlQuery)) {
			pst.setLong(1, optionId);
			try (ResultSet rset = pst.executeQuery()){
				if(rset !=null && rset.next()) {
					return createPollOption(rset);
				}
			} 
		} catch (Exception e) {
			throw new DAOException("Error getting requested poll option", e);
		}
		return null;
	}

	@Override
	public List<PollOption> getPollOptions(long pollId) throws DAOException {
		List<PollOption> pollOptions = new ArrayList<>();
		Connection con = SQLConnectionProvider.getConnection();
		String sqlQuery = "select * from PollOptions where " + PollOptionColumns.POLL_ID + "=?";
		try(PreparedStatement pst = con.prepareStatement(sqlQuery)) {
			pst.setLong(1, pollId);
			try (ResultSet rset = pst.executeQuery()){
				if(rset !=null) {
					while(rset.next()) {
						pollOptions.add(createPollOption(rset));
					}
				}
			} 
		} catch (Exception e) {
			throw new DAOException("Error getting polls.", e);
		}
		
		return pollOptions;
	}

	@Override
	public void castVote(long optionId) throws DAOException{
		Connection con = SQLConnectionProvider.getConnection();
		String sqlQueryFormat = "UPDATE %s SET %s = %s + 1 where %s = ?";
		String sqlQuery = String.format(sqlQueryFormat
										, PollOptionColumns.TABLE_NAME
										, PollOptionColumns.VOTES_COUNT
										, PollOptionColumns.VOTES_COUNT
										, PollOptionColumns.ID);
		try(PreparedStatement pst = con.prepareStatement(sqlQuery)) {
			pst.setLong(1, optionId);
			
			pst.executeUpdate();
		} catch (Exception e) {
			throw new DAOException("Error casting vote", e);
		}
	}
	
	private Poll createPoll(ResultSet rset) throws SQLException {
		Poll poll = new Poll();
		poll.setId(rset.getLong(PollColumns.ID));
		poll.setTitle(rset.getString(PollColumns.TITLE));
		poll.setMessage(rset.getString(PollColumns.MESSAGE));
		return poll;
	}
	
	private PollOption createPollOption(ResultSet rset) throws SQLException {
		PollOption pollOption = new PollOption();
		pollOption.setId(rset.getLong(PollOptionColumns.ID));
		pollOption.setOptionTitle(rset.getString(PollOptionColumns.OPTION_TITLE));
		pollOption.setOptionLink(rset.getString(PollOptionColumns.OPTION_LINK));
		pollOption.setPollId(rset.getLong(PollOptionColumns.POLL_ID));
		pollOption.setVotesCount(rset.getLong(PollOptionColumns.VOTES_COUNT));
		
		return pollOption;
	}

	@Override
	public Set<String> getTableNames(Connection con) throws DAOException {
		try {
		    Set<String> set = new HashSet<String>();
		    DatabaseMetaData dbmeta = con.getMetaData();
		    readDBTable(set, dbmeta, "TABLE", null);
		    readDBTable(set, dbmeta, "VIEW", null);
		    return set;
		} catch (SQLException e) {
			throw new DAOException("Error retrieving tables.", e);
		}
		
	}
	
	/**
	 * Calls {@link DatabaseMetaData#getTables(String, String, String, String[])}
	 * using given parameters and adds tables to given {@link Set}.
	 * @param set that stores table names
	 * @param dbmeta stores meta-data of database
	 * @param searchCriteria 
	 * @param schema
	 * @throws SQLException
	 */
	private void readDBTable(Set<String> set, DatabaseMetaData dbmeta, String searchCriteria, String schema)
		      throws SQLException {
		ResultSet rs = dbmeta.getTables(null, schema, null, new String[] { searchCriteria });
		while (rs.next()) {
			set.add(rs.getString("TABLE_NAME").toLowerCase());
		}
	}
	

}