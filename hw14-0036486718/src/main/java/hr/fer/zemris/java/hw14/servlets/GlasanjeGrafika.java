package hr.fer.zemris.java.hw14.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.util.Rotation;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

import hr.fer.zemris.java.hw14.dao.DAOProvider;
import hr.fer.zemris.java.hw14.model.PollOption;

/**
 * Class extends {@link HttpServlet}.
 * It overrides only one method {@link HttpServlet#doGet}.
 * Loads voting contest results and creates .png pie chart
 * that represents results.
 * @author Luka Mijić
 *
 */
public class GlasanjeGrafika extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		long pollID;
		try {
			pollID = Long.valueOf(req.getParameter("pollID"));
		} catch (NumberFormatException | NullPointerException exc) {
			resp.sendError(400, "No valid pollID parameter given.");
			return;
		}
		
		if(DAOProvider.getDao().getPoll(pollID) == null) {
			resp.sendError(400, "No poll associated with given id.");
			return;
		}
		
		resp.setContentType("image/png");
		
		List<PollOption> options = DAOProvider.getDao().getPollOptions(pollID);
		
		JFreeChart chart = createChart(createDataset(options), "Rezultati glasanja");
		ChartUtils.writeChartAsPNG(resp.getOutputStream(), chart, 600, 600);
		
	}
	
	/**
	 * Creates {@link PieDataset} from given data.
	 * @param voting given data
	 * @return created {@link PieDataset}
	 */
	private PieDataset createDataset(List<PollOption> voting) {
		DefaultPieDataset dataset = new DefaultPieDataset();
		voting.forEach(b -> dataset.setValue(b.getOptionTitle(), b.getVotesCount()));
		return dataset;
	}
	
	/**
	 * Creates {@link JFreeChart} from given {@link PieDataset}
	 * and given title.
	 * @param dataset given data
	 * @param title given title
	 * @return created {@link JFreeChart}.
	 */
	private JFreeChart createChart(PieDataset dataset, String title) {
		JFreeChart chart = ChartFactory.createPieChart(title, dataset, true, true, false);
		PiePlot plot = (PiePlot) chart.getPlot();
		plot.setStartAngle(290);
		plot.setDirection(Rotation.CLOCKWISE);
		plot.setForegroundAlpha(0.5f);
		return chart;
	}
}
