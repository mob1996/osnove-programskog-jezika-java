package hr.fer.zemris.java.hw14.util.database;

/**
 * This class stores names of columns in 
 * "PollOptions" table.
 * @author Luka Mijić
 *
 */
public class PollOptionColumns {

	public static final String TABLE_NAME = "PollOptions";
	public static final String ID = "id";
	public static final String OPTION_TITLE = "optionTitle";
	public static final String OPTION_LINK = "optionLink";
	public static final String POLL_ID = "pollID";
	public static final String VOTES_COUNT = "votesCount";
}
