package hr.fer.zemris.java.hw14.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw14.dao.DAOProvider;

/**
 * Class extends {@link HttpServlet}.
 * It overrides only one method, {@link HttpServlet#doGet}.
 * Takes from {@link HttpServletRequest}  one parameter "id".
 * Update voting contest with given "id" if there is option
 * associated to given id.
 * @author Luka Mijić
 *
 */
public class GlasanjeGlasajServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		long id;
		long pollID;
		try {
			pollID = Long.valueOf(req.getParameter("pollID"));
			id = Long.valueOf(req.getParameter("id"));
		} catch(NumberFormatException | NullPointerException exc) {
			resp.sendError(400, "No valid poll option id given.");
			return;
		}
		
		if(DAOProvider.getDao().getPollOption(id) == null) {
			resp.sendError(400, "No poll option associated with given id.");
			return;
		}
		
		DAOProvider.getDao().castVote(id);
		
		req.setAttribute("pollID", pollID);
		resp.sendRedirect(req.getContextPath() + "/servleti/glasanje-rezultati?pollID=" + pollID);
	}
	
}
