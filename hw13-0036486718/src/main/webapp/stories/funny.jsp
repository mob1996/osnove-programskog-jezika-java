<%@ page import="hr.fer.zemris.java.hw13.util.Util" session="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<% request.setAttribute("font_color", "#" + Util.randomColorGenerator()); %>
<html>
	<body bgcolor=#<c:out default = "FFFFFF" value = "${bgcolor}"/>>
	
		<h1><font color="${font_color }" size = "10">Story</font></h1>
	 	<p><font color="${font_color }" size="6">Just before the final exam in  my college finance class, 
	 		a less-than-stellar student approached me.<br>
			“Can you tell me what grade  I would need to get on the exam  to pass the course?” he asked.
			I gave him the bad news. <br>“The exam is worth 100 points. You would need 113 points to earn a D.”
			“OK,” he said. “And how many points would I need to get a C?” </font></p>
			
		<a href="/webapp2/index.jsp">INDEX</a>
	</body>
</html>

