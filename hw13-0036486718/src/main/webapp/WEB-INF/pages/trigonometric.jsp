<%@ page session="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="contextPath" value="${pageContext.request.contextPath }"/>
<html>
	<style>
		table {
    		font-family: arial, sans-serif;
    		border-collapse: collapse;
   			width: 100%;
		}

		td, th {
   		 	border: 1px solid #dddddd;
    		text-align: left;
    		padding: 8px;
		}

		tr:nth-child(even) {
    		background-color: #dddddd;
		}
	</style>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Trigonometric</title>
	</head>	
	<body bgcolor=#<c:out default = "FFFFFF" value = "${bgcolor}"/>>
		 <table style="width:50%">
		 	<tr>
		 		<th>Argument x</th>
		 		<th>sin(x)</th>
		 		<th>cos(x)</th>
		 	</tr>
		 	<c:forEach var="zapis" items="${results }">
		 		<tr>
		 			<td>x = ${zapis.degreeAngle }</td>
					<td>${zapis.sin }</td>
					<td>${zapis.cos }</td>
		 		</tr>
		 	</c:forEach>
		</table> 
		
		<a href="${contextPath }/index.jsp">INDEX</a>

	</body>
</html>