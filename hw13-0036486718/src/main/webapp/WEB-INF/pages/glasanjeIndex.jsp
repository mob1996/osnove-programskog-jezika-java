<%@ page session="true" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="contextPath" value="${pageContext.request.contextPath }"/>
<html>
	<body bgcolor=#<c:out default = "FFFFFF" value = "${bgcolor}"/>>
	
		<h1><font size="15"> Glasanje za omiljeni band</font></h1>
		<p>Od sljedećih bendova, koji Vam je bend najdraži? Kliknite na link kako biste glasali!</p>
		
		<ol>
			<c:forEach var="band" items="${bands }">
				<li><a href="glasanje-glasaj?id=${band.id }">${band.name }</a></li>
			</c:forEach>
		</ol>
		
		<a href="${contextPath }/index.jsp">INDEX</a>
	</body>
</html>
