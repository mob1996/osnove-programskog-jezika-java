<%@ page session="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="contextPath" value="${pageContext.request.contextPath }"/>
<html>
	<head>
		<style type="text/css">
			table {
    			font-family: arial, sans-serif;
    			border-collapse: collapse;
   				width: 100%;
			}

			td, th {
   		 		border: 1px solid #dddddd;
    			text-align: left;
    			padding: 8px;
			}

			tr:nth-child(even) {
    			background-color: #dddddd;
			}
		</style>
	</head>

	<body bgcolor=#<c:out default = "FFFFFF" value = "${bgcolor}"/>>
	
		<h1>Rezultati glasanja</h1>
		<p>Ovo su rezultati glasanja.</p>
	
		<table border="1" class="rez" style="width:30%">
			<thead><tr><th>Bend</th><th>Broj glasova</th></tr></thead>
			<tbody>
				<c:forEach var="band" items="${bandInfo }">
					<tr><td>${band.name }</td><td>${band.voteCount }</td></tr>		
				</c:forEach>
			</tbody>
		</table>
	
		<h2>Grafički prikaz rezultata</h2>
		<img alt="Pie-chart" src="${contextPath }/glasanje-grafika" width="600"height="600"/>
	
		<h2>Rezultati u XLS formatu</h2>
		<p>Rezultati u XLS formatu dostupni su <a href="${contextPath }/glasanje-xls"> ovdje.</a></p>
	
	
		<h2>Razno</h2>
		<p>Primjeri pjesama pobjedničkih bendova:</p>
		<ul>
			<c:forEach var="band" items="${winners }">
					<li><a href="${band.link }" target="_blank"> ${band.name }</a></li>
			</c:forEach>
		</ul>
		
		<a href="${contextPath }/index.jsp">INDEX</a>	
	</body>
</html>