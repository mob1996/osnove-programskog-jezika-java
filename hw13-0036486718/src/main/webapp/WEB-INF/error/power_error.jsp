<%@ page session="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="contextPath" value="${pageContext.request.contextPath }"/>
<html>
	<body bgcolor=#<c:out default = "FFFFFF" value = "${bgcolor}"/>>
		<h1>Invalid parameters given.</h1>
		<p>"a" and "b" parameters must be element of [-100, 100].<br>
			"n" parameter must be element of [1, 5]</p>
		
		<p>a = ${a }</p>
		<p>b = ${b }</p>
		<p>n = ${n }</p>
		
		<a href="${contextPath }/index.jsp">INDEX</a>
	</body>
</html>