<%@ page session="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="contextPath" value="${pageContext.request.contextPath }"/>
<html>
	<body bgcolor=#<c:out default = "FFFFFF" value = "${bgcolor}"/>>
		<h1>Color picker</h1>
	 	<a href="${contextPath }/colors.jsp">Background color chooser</a> 
	 	
	 	<form action="trigonometric" method="GET">
  			Početni kut:<br><input type="number" name="a" min="0" max="360" step="1" value="0"><br>
  			Završni kut:<br><input type="number" name="b" min="0" max="360" step="1" value="360"><br>
  		<input type="submit" value="Tabeliraj"><input type="reset" value="Reset">
  		</form>
  		<p><a href="${contextPath }/trigonometric?a=0&b=90">Table for a=0 and b=90 degrees.</a></p>
  		

  		<p><a href="${contextPath }/stories/funny.jsp">Funny Story</a></p>
  		<p><a href="${contextPath }/report.jsp">OS Report</a></p>
  		
  		<form action="powers" method="GET">
  			Start number:<br><input type="number" name="a" min="-100" max="100" step="1" value="-100"><br>
  			End number:<br><input type="number" name="b" min="-100" max="100" step="1" value="100"><br>
  			Pages number:<br><input type="number" name="n" min="1" max="5" step="1" value="1"><br>
  		<input type="submit" value="To xls"><input type="reset" value="Reset">
  		</form>
  		
  		
  		<p><a href="${contextPath }/powers?a=1&b=100&n=3">Powers XLS</a></p>
  		<p><a href="${contextPath }/appinfo.jsp">Elapsed Time</a></p>
  		<p><a href="${contextPath }/glasanje">Glasanje</a></p>

	</body>
</html>
