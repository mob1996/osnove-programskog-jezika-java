<%@ page session="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
   
<c:set var="contextPath" value="${pageContext.request.contextPath }"/>
<html>
	<style>
		table, th, td {
    	border: 1px solid black;
	}
	</style>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Insert title here</title>
	</head>
	<body bgcolor=#<c:out default = "FFFFFF" value = "${bgcolor}"/>>
		 <table style="width:20%">
		 	<tr><th>Colors</th></tr>
  			<tr><td><a href="${contextPath }/setcolor?bgcolor=FFFFFF">WHITE</a></td></tr>
  			<tr><td><a href="${contextPath }/setcolor?bgcolor=FF0000">RED</a></td></tr>
  			<tr><td><a href="${contextPath }/setcolor?bgcolor=00FF00">GREEN</a></td></tr>
  			<tr><td><a href="${contextPath }/setcolor?bgcolor=00FFFF">CYAN</a></td></tr>

		</table> 

		<a href="${contextPath }/index.jsp">INDEX</a>
	</body>
</html>