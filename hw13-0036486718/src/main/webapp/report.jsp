<%@ page session="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="contextPath" value="${pageContext.request.contextPath }"/>
<html>
	<body bgcolor=#<c:out default = "FFFFFF" value = "${bgcolor}"/>>
		<h1>OS usage</h1>
		<p>Here are results of OS usage in survey that we completed.</p>

		<img src = "${contextPath }/reportImage">
		
		<p><a href="${contextPath }/index.jsp">INDEX</a></p>
	</body>
</html>