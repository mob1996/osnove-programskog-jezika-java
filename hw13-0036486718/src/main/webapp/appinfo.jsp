<%@ page import="hr.fer.zemris.java.hw13.util.Util" session="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="contextPath" value="${pageContext.request.contextPath }"/>
<% Long startTime = (Long) request.getServletContext().getAttribute("startTime");
   request.setAttribute("elapsedTime", Util.elapsedFormattedTime(System.currentTimeMillis() - startTime)); %>
   
<html>
	<body bgcolor=#<c:out default = "FFFFFF" value = "${bgcolor}"/>>
	
	<h1><font size="15"> ${elapsedTime } </font></h1>
	
	<a href="${contextPath }/index.jsp">INDEX</a>
	</body>
</html>