package hr.fer.zemris.java.hw13.models;

/**
 * Model stores degree angle, sinus and cosinus of that angle.
 * @author Luka Mijić
 *
 */
public class TrigonometricModel {

	/**
	 * Angle in degrees
	 */
	private double degreeAngle;
	
	/**
	 * Sinus of angle
	 */
	private double sin;
	
	/**
	 * Cosinus of angle
	 */
	private double cos;
	
	/**
	 * Create new {@link TrigonometricModel} using given angle.
	 * @param degreeAngle
	 */
	public TrigonometricModel(double degreeAngle) {
		this.degreeAngle = degreeAngle;
		this.sin = Math.sin(Math.toRadians(degreeAngle));
		this.cos = Math.cos(Math.toRadians(degreeAngle));
	}

	/**
	 * @return angle
	 */
	public double getDegreeAngle() {
		return degreeAngle;
	}

	/**
	 * @return sinus of angle
	 */
	public double getSin() {
		return sin;
	}
	
	/**
	 * @return cosinus of angle
	 */
	public double getCos() {
		return cos;
	}
	
	
}
