package hr.fer.zemris.java.hw13.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.util.Rotation;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

/**
 * Class extends {@link HttpServlet}.
 * It overrides only one method {@link HttpServlet#doGet}.
 * Creates {@link JFreeChart} and calls {@link ChartUtils#writeChartAsPNG(java.io.OutputStream, JFreeChart, int, int)}
 * and gives {@link HttpServletResponse#getOutputStream()} as argument.
 * @author Luka Mijić
 *
 */
public class ReportImage extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("image/png");
		PieDataset dataset = createDataset();
		
		JFreeChart chart = createChart(dataset, "OS USAGE");
		ChartUtils.writeChartAsPNG(resp.getOutputStream(), chart, 600, 600);
	}

	/**
	 * @return created {@link PieDataset}
	 */
	private PieDataset createDataset() {
		DefaultPieDataset dataset = new DefaultPieDataset();
		dataset.setValue("Linux", 1047);
		dataset.setValue("Mac", 522);
		dataset.setValue("Windows", 6323);
		return dataset;
	}

	/**
	 * Creates {@link JFreeChart} from given
	 * {@link PieDataset} and title.
	 * @param dataset used for creating chart
	 * @param title of chart
	 * @return created {@link JFreeChart}
	 */
	private JFreeChart createChart(PieDataset dataset, String title) {
		JFreeChart chart = ChartFactory.createPieChart(title, dataset, true, true, false);
		PiePlot plot = (PiePlot) chart.getPlot();
		plot.setStartAngle(290);
		plot.setDirection(Rotation.CLOCKWISE);
		plot.setForegroundAlpha(0.5f);
		return chart;
	}
}
