package hr.fer.zemris.java.hw13.models;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Models one vote entry in contest.
 * @author Luka Mijić
 *
 */
public class VoteEntry {

	/**
	 * id of entry
	 */
	private int id;
	
	/**
	 * Vote count
	 */
	private int voteCount;
	
	/**
	 * Creates new {@link VoteEntry} from parameters.
	 * @param id of entry
	 * @param voteCount of entry
	 */
	public VoteEntry(int id, int voteCount) {
		this.id = id;
		this.voteCount = voteCount;
	}

	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return vote count
	 */
	public int getVoteCount() {
		return voteCount;
	}
	
	/**
	 * Increase vote count by one.
	 */
	public void castVote() {
		voteCount++;
	}
	
	/**
	 * Parse vote entry from given {@link String}.
	 * Expected form of {@link String} is "id\tvoteCount"
	 * @param line that is parsed
	 * @return parse {@link VoteEntry}
	 */
	public static VoteEntry parse(String line) {
		String[] params = line.trim().split("\t");
		return new VoteEntry(Integer.valueOf(params[0]), Integer.valueOf(params[1]));
	}
	
	/**
	 * Loads {@link List} of {@link VoteEntry} from lines
	 * in given {@link Path}.
	 * @param filePath that stores vote entries
	 * @return {@link List} of {@link VoteEntry}
	 * @throws IOException
	 */
	public static synchronized List<VoteEntry> loadVotes(Path filePath) throws IOException {
		List<VoteEntry> entries = new ArrayList<>();
		Files.readAllLines(filePath, StandardCharsets.UTF_8).forEach(line -> entries.add(VoteEntry.parse(line)));
		return entries;
	}
	
	/**
	 * Write {@link List} of {@link VoteEntry} to given {@link Path}.
	 * It overwrites currently stored information.
	 * @param entries
	 * @param destPath
	 * @throws IOException
	 */
	public static synchronized void write(List<VoteEntry> entries, Path destPath) throws IOException {
		BufferedWriter writer = Files.newBufferedWriter(destPath, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
		for(int i = 0; i < entries.size(); i++) {
			String end = (i == entries.size() - 1) ? "" : "\n";
			writer.write(entries.get(i) + end);
		}
		writer.close();
	}
	
	/**
	 * Creates {@link Map} from {@link List} of {@link VoteEntry}.
	 * To {@link VoteEntry#getId()} associate {@link VoteEntry#getVoteCount()}.
	 * @param bands that is turned into {@link Map}
	 * @return created {@link Map}
	 */
	public static Map<Integer, Integer> toMap(List<VoteEntry> votes){
		Map<Integer, Integer> map = new HashMap<>();
		votes.forEach(v -> map.put(v.getId(), v.getVoteCount()));
		return map;
	}
	
	@Override
	public String toString() {
		return id + "\t" + voteCount;
	}
}
