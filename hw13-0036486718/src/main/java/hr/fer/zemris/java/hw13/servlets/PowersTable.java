package hr.fer.zemris.java.hw13.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * Class extends {@link HttpServlet}.
 * It overrides only one method {@link HttpServlet#doGet}.
 * Takes three arguments from {@link HttpServletRequest}.
 * Argument "a" is start number, "b" is end number, "n" is pages number.
 * For each i that is element of [1, n] it creates xls page.
 * Each page contains in 0th column numbers in [a, b] and
 * in 1st column is that number to power of i.
 * @author Luka Mijić
 *
 */
public class PowersTable extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int a = parseInt(req.getParameter("a"), 1);
		int b = parseInt(req.getParameter("b"), 10);
		int n = parseInt(req.getParameter("n"), 2);
		
		if(a > b) {
			int pom = a;
			a = b;
			b = pom;
		}
		
		req.setAttribute("a", a);
		req.setAttribute("b", b);
		req.setAttribute("n", n);
		
		if(!inBounds(a, -100, 100) || !inBounds(b, -100, 100) || !inBounds(n, 1, 5)) {
			req.getRequestDispatcher("/WEB-INF/error/power_error.jsp")
							.forward(req, resp);
		}
		
		HSSFWorkbook hwb = new HSSFWorkbook();
		
		for(int i = 1; i <= n; i++) {
			HSSFSheet sheet = hwb.createSheet(String.format("Power of %d", i));
			
			HSSFRow header = sheet.createRow(0);
			header.createCell(0).setCellValue("Value");
			header.createCell(1).setCellValue(String.format("%d-th power", i));
			
			for(int j = a; j <=b; j++) {
				int rowIndex = j - a + 1;
				
				HSSFRow row = sheet.createRow(rowIndex);
				row.createCell(0).setCellValue(String.valueOf(j));
				row.createCell(1).setCellValue(String.valueOf(Math.pow(j, i)));
			}
		}
		
		resp.setContentType("application/vnd.ms-excel");
		resp.setHeader("Content-Disposition", "attachment; filename=\"tablica.xls\"");
	
		hwb.write(resp.getOutputStream());
		hwb.close();
	}
	
	/**
	 * Check if given number is in given bounds.
	 * Both bounds are inclusive.
	 * @param value that is checked
	 * @param lowerBound 
	 * @param upperBound
	 * @return true if value is element of [lowerBound, upperBound]
	 */
	private boolean inBounds(int value, int lowerBound, int upperBound) {
		return value >= lowerBound && value <= upperBound;
	}
	
	/**
	 * Parses {@link String} into {@link Integer}.
	 * If parsing failed return given default value.
	 * @param value that is parsed
	 * @param defValue default value
	 * @return parsed value or if that failed default value
	 */
	private int parseInt(String value, int defValue) {
		try {
			return Integer.valueOf(value);
		} catch (NumberFormatException | NullPointerException exc) {
			return defValue;
		}
	}
}
