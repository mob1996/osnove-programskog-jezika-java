package hr.fer.zemris.java.hw13.models;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Model represents basic information of music band.
 * @author Luka Mijić
 *
 */
public class BandInfo {

	/**
	 * Band id
	 */
	private int id;
	
	/**
	 * Band name
	 */
	private String name;
	
	/**
	 * Song link
	 */
	private String link;
	
	/**
	 * Create new {@link BandInfo} from given parameters
	 * @param id of band
	 * @param name of band
	 * @param link of band
	 */
	public BandInfo(int id, String name, String link) {
		super();
		this.id = id;
		this.name = name;
		this.link = link;
	}

	/**
	 * @return band id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return band name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return song link
	 */
	public String getLink() {
		return link;
	}
	
	/**
	 * Parse {@link BandInfo} from {@link String}.
	 * {@link String} must be of form "id\tname\tlink".
	 * @param line that is parsed
	 * @return parsed {@link BandInfo}
	 */
	public static BandInfo parseBandInfo(String line) {
		String[] params = line.trim().split("\t");
		return new BandInfo(Integer.valueOf(params[0]), params[1], params[2]);
	}
	
	/**
	 * Loads {@link List} of {@link BandInfo} from lines
	 * in given {@link Path}.
	 * @param filePath that stores bands information
	 * @return {@link List} of {@link BandInfo}
	 * @throws IOException
	 */
	public static synchronized List<BandInfo> loadBands(Path filePath) throws IOException{
		List<BandInfo> bands = new ArrayList<>();
		Files.readAllLines(filePath, StandardCharsets.UTF_8).forEach(line -> bands.add(BandInfo.parseBandInfo(line)));
		bands.sort((b1, b2) -> Integer.compare(b1.getId(), b2.getId()));
		return bands;
	}
	
	/**
	 * Creates {@link Map} from {@link List} of {@link BandInfo}.
	 * To {@link BandInfo#id} associate {@link BandInfo}.
	 * @param bands that is turned into {@link Map}
	 * @return created {@link Map}
	 */
	public static Map<Integer, BandInfo> toMap(List<BandInfo> bands){
		Map<Integer, BandInfo> map = new HashMap<>();
		bands.forEach(i -> map.put(i.getId(), i));
		return map;
	}
	
	@Override
	public String toString() {
		return id + "\t" + name + "\t" + link; 
	}
}
