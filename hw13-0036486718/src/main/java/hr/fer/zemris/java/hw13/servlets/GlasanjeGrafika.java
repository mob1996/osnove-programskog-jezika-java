package hr.fer.zemris.java.hw13.servlets;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.util.Rotation;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

import hr.fer.zemris.java.hw13.models.BandAndVoting;
import hr.fer.zemris.java.hw13.models.BandInfo;
import hr.fer.zemris.java.hw13.models.VoteEntry;

/**
 * Class extends {@link HttpServlet}.
 * It overrides only one method {@link HttpServlet#doGet}.
 * Loads voting contest results and creates .png pie chart
 * that represents results.
 * @author Luka Mijić
 *
 */
public class GlasanjeGrafika extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Path resultsPath = Paths.get(req.getServletContext().getRealPath("/WEB-INF/glasanje-rezultati.txt"));
		Path bandsPath = Paths.get(req.getServletContext().getRealPath("/WEB-INF/glasanje-definicija.txt"));
		
		resp.setContentType("image/png");
		Map<Integer, Integer> votes = VoteEntry.toMap(VoteEntry.loadVotes(resultsPath));
		Map<Integer, BandInfo> bands = BandInfo.toMap(BandInfo.loadBands(bandsPath)); 
		
		List<BandAndVoting> merged = BandAndVoting.merge(bands, votes);
		
		JFreeChart chart = createChart(createDataset(merged), "Rezultati glasanja");
		ChartUtils.writeChartAsPNG(resp.getOutputStream(), chart, 600, 600);
		
	}
	
	/**
	 * Creates {@link PieDataset} from given data.
	 * @param voting given data
	 * @return created {@link PieDataset}
	 */
	private PieDataset createDataset(List<BandAndVoting> voting) {
		DefaultPieDataset dataset = new DefaultPieDataset();
		voting.forEach(b -> dataset.setValue(b.getName(), b.getVoteCount()));
		return dataset;
	}
	
	/**
	 * Creates {@link JFreeChart} from given {@link PieDataset}
	 * and given title.
	 * @param dataset given data
	 * @param title given title
	 * @return created {@link JFreeChart}.
	 */
	private JFreeChart createChart(PieDataset dataset, String title) {
		JFreeChart chart = ChartFactory.createPieChart(title, dataset, true, true, false);
		PiePlot plot = (PiePlot) chart.getPlot();
		plot.setStartAngle(290);
		plot.setDirection(Rotation.CLOCKWISE);
		plot.setForegroundAlpha(0.5f);
		return chart;
	}
}
