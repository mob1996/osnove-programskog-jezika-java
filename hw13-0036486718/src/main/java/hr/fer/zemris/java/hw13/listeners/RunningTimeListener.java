package hr.fer.zemris.java.hw13.listeners;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * This listener stores server start time 
 * on {@link ServletContext}.
 * @author Luka Mijić
 *
 */
public class RunningTimeListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		sce.getServletContext().setAttribute("startTime", System.currentTimeMillis());

	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {

	}

}
