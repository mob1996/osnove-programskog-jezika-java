package hr.fer.zemris.java.hw13.servlets;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import hr.fer.zemris.java.hw13.models.BandAndVoting;
import hr.fer.zemris.java.hw13.models.BandInfo;
import hr.fer.zemris.java.hw13.models.VoteEntry;

/**
 * Class extends {@link HttpServlet}.
 * It overrides only one method {@link HttpServlet#doGet}.
 * Loads band contest information and turns it
 * into .xls format. Call {@link HSSFWorkbook#write(java.io.OutputStream)}
 * and give {@link HttpServletResponse#getOutputStream()} as argument.
 * 
 * @author Luka Mijić
 *
 */
public class GlasanjeXLS extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Path resultsPath = Paths.get(req.getServletContext().getRealPath("/WEB-INF/glasanje-rezultati.txt"));
		Path bandsPath = Paths.get(req.getServletContext().getRealPath("/WEB-INF/glasanje-definicija.txt"));
		
		Map<Integer, Integer> votes = VoteEntry.toMap(VoteEntry.loadVotes(resultsPath));
		Map<Integer, BandInfo> bands = BandInfo.toMap(BandInfo.loadBands(bandsPath)); 
		
		List<BandAndVoting> merged = BandAndVoting.merge(bands, votes);
		
		HSSFWorkbook hwb = new HSSFWorkbook();
		HSSFSheet sheet = hwb.createSheet("Voting results");
		
		for(int i = 0; i < merged.size(); i++) {
			HSSFRow row = sheet.createRow(i);
			row.createCell(0).setCellValue(merged.get(i).getName());
			row.createCell(1).setCellValue(merged.get(i).getVoteCount());
		}
		
		req.getSession().removeAttribute("bandInfo");
		resp.setContentType("application/vnd.ms-excel");
		resp.setHeader("Content-Disposition", "attachment; filename=\"tablica.xls\"");
	
		hwb.write(resp.getOutputStream());
		hwb.close();
	}

}
