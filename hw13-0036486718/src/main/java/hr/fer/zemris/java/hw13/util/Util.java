package hr.fer.zemris.java.hw13.util;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Utility class with some useful utility methods.
 * @author Luka Mijić
 *
 */
public class Util {
	
	/**
	 * Valid characters in HEX colour format.
	 */
	private static final char[] legalColorValues
			= {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
	
	/**
	 * Number of characters in HEX colour format.
	 */
	private static final short HEX_COLOR_LENGTH = 6;
	
	/**
	 * Creates random {@link String} representation of
	 * HEX colour.
	 * @return
	 */
	public static String randomColorGenerator() {
		char[] newColor = new char[HEX_COLOR_LENGTH];
		
		Random rand = new Random();
		for(int i = 0; i < newColor.length; i++) {
			newColor[i] = legalColorValues[rand.nextInt(legalColorValues.length)];
		}
		
		return new String(newColor);
	}
	
	/**
	 * Calculate elapsed time from given milliseconds in 
	 * day:hour:minute:seconds.milliseconds format.
	 * @param elapsedMillis 
	 * @return formated elapsed time.
	 */
	public static String elapsedFormattedTime(long elapsedMillis) {
		long days = TimeUnit.MILLISECONDS.toDays(elapsedMillis);
		long hr = TimeUnit.MILLISECONDS.toHours(elapsedMillis - TimeUnit.DAYS.toMillis(days));
		long min = TimeUnit.MILLISECONDS.toMinutes(elapsedMillis - TimeUnit.DAYS.toMillis(days) 
																 - TimeUnit.HOURS.toMillis(hr));
		long sec = TimeUnit.MILLISECONDS.toSeconds(elapsedMillis - TimeUnit.DAYS.toMillis(days) 
																 - TimeUnit.HOURS.toMillis(hr) - TimeUnit.MINUTES.toMillis(min));
		long ms = TimeUnit.MILLISECONDS.toMillis(elapsedMillis - TimeUnit.DAYS.toMillis(days) 
															   - TimeUnit.HOURS.toMillis(hr)
															   - TimeUnit.MINUTES.toMillis(min) 
															   - TimeUnit.SECONDS.toMillis(sec));
		
		return String.format("%02d:%02d:%02d:%02d.%03d", days, hr, min, sec, ms);
	}
	
}
