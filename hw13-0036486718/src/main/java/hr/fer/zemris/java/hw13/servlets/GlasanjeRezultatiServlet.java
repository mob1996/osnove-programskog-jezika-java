package hr.fer.zemris.java.hw13.servlets;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw13.models.BandAndVoting;
import hr.fer.zemris.java.hw13.models.BandInfo;
import hr.fer.zemris.java.hw13.models.VoteEntry;

/**
 * Class extends {@link HttpServlet}.
 * It overrides only one method {@link HttpServlet#doGet}.
 * Loads band contest information and stores it into
 * {@link HttpServletRequest#setAttribute(String, Object)} and
 * dispatches to appropriate .jsp.
 * @author Luka Mijić
 *
 */
public class GlasanjeRezultatiServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Path resultsPath = Paths.get(req.getServletContext().getRealPath("/WEB-INF/glasanje-rezultati.txt"));
		Path bandsPath = Paths.get(req.getServletContext().getRealPath("/WEB-INF/glasanje-definicija.txt"));
		
		if(!Files.exists(resultsPath)) {
			Files.createFile(resultsPath);
		}
		
		Map<Integer, Integer> votes = VoteEntry.toMap(VoteEntry.loadVotes(resultsPath));
		Map<Integer, BandInfo> bands = BandInfo.toMap(BandInfo.loadBands(bandsPath)); 
		
		List<BandAndVoting> combined = BandAndVoting.merge(bands, votes);
		
		int maxVotes = combined.stream()
							   .mapToInt(b -> b.getVoteCount())
							   .max().orElse(0);
		List<BandAndVoting> winningBands = combined.stream()
												   .filter(b -> b.getVoteCount() == maxVotes)
												   .collect(Collectors.toList());
		
		req.setAttribute("bandInfo", combined);
		req.setAttribute("winners", winningBands);
		
		req.getRequestDispatcher("/WEB-INF/pages/glasanjeRez.jsp")
								.forward(req, resp);
	}
}
