package hr.fer.zemris.java.hw13.servlets;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw13.models.BandInfo;
import hr.fer.zemris.java.hw13.models.VoteEntry;

/**
 * Class extends {@link HttpServlet}.
 * It overrides only one method, {@link HttpServlet#doGet}.
 * Takes from {@link HttpServletRequest}  one parameter "id".
 * Update voting contest with given "id" if band is registered to
 * given id.
 * @author Luka Mijić
 *
 */
public class GlasanjeGlasajServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		update(req);
		
		resp.sendRedirect(req.getContextPath() + "/glasanje-rezultati");
	}
	
	/**
	 * Process given request, use it's parameter to
	 * update voting contest and update .txt database.
	 * @param req {@link HttpServletRequest}.
	 * @throws IOException
	 */
	private void update(HttpServletRequest req) throws IOException {
		int id;
		try {
			id = Integer.valueOf(req.getParameter("id"));
		} catch(NumberFormatException | NullPointerException exc) {
			return;
		}
		
		String resultsFileName = req.getServletContext().getRealPath("/WEB-INF/glasanje-rezultati.txt");
		String bandsFileName = req.getServletContext().getRealPath("/WEB-INF/glasanje-definicija.txt");
		
		Path resultsPath = Paths.get(resultsFileName);
		Path bandsPath = Paths.get(bandsFileName);
		
		List<Integer> validIds = BandInfo.loadBands(bandsPath)
										 .stream().map(b -> b.getId()).collect(Collectors.toList());
		
		if(!validIds.contains(id)) {
			return;
		}
	
		if(!Files.exists(resultsPath)) {
			Files.createFile(resultsPath);
		}
		List<VoteEntry> entries = VoteEntry.loadVotes(resultsPath);
		
		boolean update = false;
		for(VoteEntry entry:entries) {
			if(entry.getId() == id) {
				update = true;
				entry.castVote();
			}
		}
		
		if(!update) {
			entries.add(new VoteEntry(id, 1));
		}
		
		VoteEntry.write(entries, resultsPath);
		
	}

}
