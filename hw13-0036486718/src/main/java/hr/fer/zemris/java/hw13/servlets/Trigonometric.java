package hr.fer.zemris.java.hw13.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw13.models.TrigonometricModel;

/**
 * Class extends {@link HttpServlet}.
 * It overrides only one method {@link HttpServlet#doGet}.
 * It takes two arguments from given {@link HttpServletRequest},
 * calculates cos and sin for them. Stores results as {@link List}
 * and dispatches it to .jsp file that generates table with results.
 * @author Luka Mijić
 *
 */
public class Trigonometric extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int a = (int) parseDouble(req.getParameter("a"), 0);
		int b = (int) parseDouble(req.getParameter("b"), 360);
		
		if(a > b) {
			int pom = a;
			a = b;
			b = pom;
		}
		
		if(b > a + 720) {
			b = a + 720;
		}
		
		List<TrigonometricModel> results = new ArrayList<>();
		
		for(int i = a; i <=b; i++) {
			results.add(new TrigonometricModel(i));
		}
		req.setAttribute("results", results);
		
		req.getRequestDispatcher("/WEB-INF/pages/trigonometric.jsp")
								.forward(req, resp);
		
	}
	
	/**
	 * Parses {@link String} into {@link Double}.
	 * If parsing failed return given default value.
	 * @param value that is parsed
	 * @param defValue default value
	 * @return parsed value or if that failed default value
	 */
	private double parseDouble(String value, double defValue) {
		try {
			return Double.valueOf(value);
		} catch (NumberFormatException | NullPointerException exc) {
			return defValue;
		}
	}
}
