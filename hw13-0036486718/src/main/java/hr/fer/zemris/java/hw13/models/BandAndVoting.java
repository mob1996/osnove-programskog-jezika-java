package hr.fer.zemris.java.hw13.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * This model models basic band information
 * like their name and link to their song and their vote
 * count in popularity contest.
 * @author Luka Mijić
 *
 */
public class BandAndVoting {
	
	/**
	 * Band name
	 */
	private String name;
	
	/**
	 * Song link
	 */
	private String link;
	
	/**
	 * Vote count
	 */
	private int voteCount;

	/**
	 * Creates new BandAndVoting 
	 * @param name of band
	 * @param link of song
	 * @param voteCount in contest
	 */
	public BandAndVoting(String name, String link, int voteCount) {
		this.name = name;
		this.link = link;
		this.voteCount = voteCount;
	}

	/**
	 * @return band name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return song link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @return vote count
	 */
	public int getVoteCount() {
		return voteCount;
	}
	
	/**
	 * Merges {@link BandInfo} and {@link VoteEntry} map.
	 * For each {@link BandInfo} entry add to list new {@link BandAndVoting}.
	 * {@link BandAndVoting} is created from {@link BandInfo#getName()},
	 * {@link BandInfo#getLink()} and vote count from votes.
	 * @param bands {@link Map} of {@link Integer} and {@link BandInfo}
	 * @param votes {@link Map} of {@link Integer} and {@link Integer}.
	 * @return merged list
	 */
	public static List<BandAndVoting> merge(Map<Integer, BandInfo> bands, Map<Integer, Integer> votes){
		List<BandAndVoting> merged = new ArrayList<>();
		
		bands.forEach((id, info) -> {
			int voteCount = votes.getOrDefault(id, 0);
			merged.add(new BandAndVoting(info.getName(), info.getLink(), voteCount));
		});
		
		merged.sort((b1, b2) -> Integer.compare(b2.getVoteCount(), b1.getVoteCount()));
		return merged;
	}

}