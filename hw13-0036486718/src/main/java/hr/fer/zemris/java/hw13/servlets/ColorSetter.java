package hr.fer.zemris.java.hw13.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class extends {@link HttpServlet}
 * and overrides only {@link HttpServlet#doGet} method.
 * Method takes one parameter "bgcolor" from given {@link HttpServletRequest}
 * and store it into session parameters.
 * @author Luka Mijić
 *
 */
public class ColorSetter extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Valid letters in HEX colours format.
	 */
	private static final char[] validLetters = {'A', 'B', 'C', 'D', 'E', 'F'};
	
	private static final String WHITE = "FFFFFF";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String colorPicked = req.getParameter("bgcolor") == null ? WHITE : (String) req.getParameter("bgcolor");
		
		if(colorPicked == null) {
			colorPicked = "FFFFFF";
		} else {
			if(!decodable(colorPicked)) {
				colorPicked = "FFFFFF";
			}
		}
		
		req.getSession().setAttribute("bgcolor", colorPicked);
		
		req.getRequestDispatcher("/WEB-INF/pages/color_message.jsp")
						.forward(req, resp);
	}
	
	/**
	 * Check if given {@link String} can be decoded
	 * as colour in HEX format.
	 * @param colorStr {@link String} that is checked
	 * @return true if it can be decoded, otherwise return false
	 */
	private boolean decodable(String colorStr) {
		if(colorStr.length() != 6) return false;
		
		for(int i = 0; i < colorStr.length(); i++) {
			if(Character.isDigit(colorStr.charAt(i))) {
				continue;
			}
			
			char upperChar = Character.toUpperCase(colorStr.charAt(i));
			boolean validLetter = false;
			for(int j = 0; j < validLetters.length; j++) {
				if(upperChar == colorStr.charAt(i)) {
					validLetter = true;
					break;
				}
			}
			
			if(validLetter) continue;
			return false;
		}
		
		return true;
	}
}
