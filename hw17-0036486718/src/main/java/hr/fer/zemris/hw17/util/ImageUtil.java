package hr.fer.zemris.hw17.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Utility class that holds some utility methods.
 * @author Luka Mijić
 *
 */
public class ImageUtil {

	/**
	 * Writes image from {@link InputStream} to given {@link OutputStream}.
	 * @param is {@link InputStream}
	 * @param os {@link OutputStream}
	 * @throws IOException if errors while reading or writing occurs
	 */
	public static void writeImage(InputStream is, OutputStream os) throws IOException {
		int r;
		byte[] data = new byte[512];
		while((r = is.read(data)) != -1) {
			os.write(data, 0, r);
		}
	}
	
	/**
	 * Finds extension of file.
	 * @param fileName name of file
	 * @return null if extension doesn't exists, otherwise return extension
	 */
	public static String getExtension(String fileName) {
		int lastDot = fileName.lastIndexOf('.');
		if(lastDot == fileName.length() - 1 || lastDot == -1 || lastDot == 0) {
			return null;
		}
		
		return fileName.substring(lastDot + 1); 
	}
}
