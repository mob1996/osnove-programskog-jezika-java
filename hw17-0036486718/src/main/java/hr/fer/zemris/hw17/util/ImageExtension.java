package hr.fer.zemris.hw17.util;

import java.util.Set;
import java.util.TreeSet;

/**
 * Class holds valid image extensions.
 * @author Luka Mijić
 *
 */
public class ImageExtension {

	/**
	 * {@link Set} of valid images extensions.
	 */
	private static Set<String> extension;
	
	static {
		extension = new TreeSet<>();
		extension.add("jpeg");
		extension.add("jpg");
		extension.add("png");
		extension.add("svg");
	}
	
	/**
	 * Check if given extension is valid.
	 * @param ext is extension that is checked
	 * @return true if extension is valid, otherwise false
	 */
	public static boolean isValid(String ext) {
		return extension.contains(ext.toLowerCase());
	}
}
