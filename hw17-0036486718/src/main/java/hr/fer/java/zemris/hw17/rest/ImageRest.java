package hr.fer.java.zemris.hw17.rest;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;

import hr.fer.java.zemris.hw17.database.ImageDB;
import hr.fer.java.zemris.hw17.model.Image;

/**
 * Class that implements architectural style known as REST.
 * REST is known for treating everything as resource.
 * This implementation creates JSON resources. 
 * @author Luka Mijić
 *
 */
@Path("/image")
public class ImageRest {

	/**
	 * Creates {@link JSONObject} that puts
	 * number of images in database to key "numberOfImages"
	 * @return {@link Response}
	 */
	@GET
	@Produces("application/json")
	public Response getNumberOfImages() {
		int n = 0;
		try {
			n = ImageDB.getNumberOfImages();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
		
		JSONObject result = new JSONObject();
		result.put("numberOfImages", n);
		
		return Response.status(Status.OK).entity(result.toString()).build();
	}
	
	/**
	 * Create {@link JSONObject} that holds single {@link JSONArray}
	 * that is filled with all tag values.
	 * @return {@link Response}
	 */
	@Path("/tags/get")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTags() {
		JSONObject result = new JSONObject();
		JSONArray tags = new JSONArray();
		
		try {
			Set<String> tagsStr = ImageDB.allTags();
			for(String t:tagsStr) {
				tags.put(t);
			}
		} catch(IOException exc) {
			exc.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
		
		result.put("tags", tags);
		
		return Response.status(Status.OK).entity(result.toString()).build();
	}
	
	/**
	 * Return {@link JSONObject} that hold single {@link JSONArray}
	 * that is filled with all {@link Image#getName()}s associated
	 * with given tag.
	 * @param tag {@link Image} tags
	 * @return {@link Response}
	 */
	@Path("/tags/{tag}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getImagesForTag(@PathParam("tag") String tag) {
		JSONObject result = new JSONObject();
		JSONArray imageNames = new JSONArray();
	
		try {
			List<Image> imgs = ImageDB.forTags(tag);
			for(Image i:imgs) {
				imageNames.put(i.getName());
			}
		} catch(IOException exc) {
			exc.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
		
		result.put("images", imageNames);
		return Response.status(Status.OK).entity(result.toString()).build();
	}
	
	/**
	 * Create {@link JSONObject} that has key name, desc and
	 * {@link JSONArray} of {@link Image} tags.
	 * @param name of wanted {@link Image}
	 * @return {@link Response}
	 */
	@Path("{imageName}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getImageForName(@PathParam("imageName") String name) {
		JSONObject result = new JSONObject();
		
		Image img;
		try {
			img = ImageDB.getImage(name);
		} catch(IOException exc) {
			exc.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
		JSONArray imgTags = new JSONArray();
		for(String t:img.getTags()) {
			imgTags.put(t);
		}
		
		result.put("name", img.getName());
		result.put("desc", img.getDesc());
		result.put("tags", imgTags);
		
		return Response.status(Status.OK).entity(result.toString()).build();
	}
	
}
