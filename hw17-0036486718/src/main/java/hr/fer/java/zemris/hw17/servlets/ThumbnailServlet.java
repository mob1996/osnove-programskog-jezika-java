package hr.fer.java.zemris.hw17.servlets;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.java.zemris.hw17.database.PathProvider;
import hr.fer.zemris.hw17.util.ImageExtension;
import hr.fer.zemris.hw17.util.ImageUtil;

/**
 * Servlet is mapped to "/thumbnail/*".
 * If * part is empty or invalid default thumbnail is sent back.
 * @author Luka Mijić
 *
 */
@WebServlet("/thumbnail/*")
public class ThumbnailServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Method handles GET requests.
	 * In url everything after "/thumbnail/" is treated as
	 * parameters separated by "/". If there is more than one parameter
	 * or if there are no parameters or parameter doesn't have valid name
	 * servlet returns default thumbnail that is basically image of question mark.
	 * 
	 * If name is valid but thumbnail doesn't exist create it from from
	 * original image. Send thumbnail back.
	 * @param req {@link HttpServletRequest}
	 * @param resp {@link HttpServletResponse}
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if(req.getPathInfo() == null || req.getPathInfo().length() == 1) {
			defaultThumbnail(resp);
			return;
		}
		
		String[] params = req.getPathInfo().substring(1).split("/");
		if(params.length != 1) {
			defaultThumbnail(resp);
			return;
		}
		
		String imgName = params[0];
		String extension = ImageUtil.getExtension(imgName);
		if(extension == null || !ImageExtension.isValid(extension)) {
			defaultThumbnail(resp);
			return;
		}
		
		Path imagePath = PathProvider.getImageDirectory().resolve(imgName);
		if(!Files.exists(imagePath)) {
			defaultThumbnail(resp);
			return;
		}
		
		Path thumbnailPath = PathProvider.getThumbnailDirectory().resolve(imgName);
		if(!Files.exists(thumbnailPath)) {
			createThumbnail(imagePath);
		}
		
		resp.setContentType("image/" + extension);
		InputStream is = Files.newInputStream(thumbnailPath);
		ImageUtil.writeImage(is, resp.getOutputStream());
		is.close();
	}
	
	/**
	 * Method creates thumbnail by image at given location.
	 * @param src location of original image
	 * @throws IOException if errors reading or writing occurs
	 */
	private void createThumbnail(Path src) throws IOException {
		BufferedImage img = new BufferedImage(150, 150, BufferedImage.TYPE_INT_RGB);
		img.createGraphics()
		   .drawImage(ImageIO.read(src.toFile())
		   .getScaledInstance(150, 150, Image.SCALE_SMOOTH),0,0,null);
		
		Path thumbnailPath = PathProvider.getThumbnailDirectory().resolve(src.getFileName());
		ImageIO.write(img
					  , ImageUtil.getExtension(src.getFileName().toString())
					  , thumbnailPath.toFile()
		);
	}
	
	/**
	 * Send default thumbnail (question mark) to {@link OutputStream}
	 * provided by given {@link HttpServletResponse}.
	 * @param resp {@link HttpServletResponse}.
	 * @throws IOException
	 */
	private void defaultThumbnail(HttpServletResponse resp) throws IOException {
		resp.setContentType("image/png");
		Path thumbnailPath = PathProvider.getThumbnailDirectory().resolve("unknown.png");
		InputStream is = Files.newInputStream(thumbnailPath);
		ImageUtil.writeImage(is , resp.getOutputStream());
		is.close();
	}
}
