package hr.fer.java.zemris.hw17.database;

import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * This class provides {@link Path} to various files
 * and directories on disk. 
 * Class also implements {@link ServletContextListener} because
 * location of those files is relative and it is not know before
 * server deployment.
 * @author Luka Mijić
 */
@WebListener
public class PathProvider implements ServletContextListener {

	/**
	 * {@link Path} to thumbnails directory
	 */
	private static Path thumbnailDirectory;
	
	/**
	 * {@link Path} to image directory
	 */
	private static Path imageDirectory;
	
	/**
	 * {@link Path} to database file
	 */
	private static Path databaseFile;
	
	/**
	 * @return {@link Path} to thumbnails directory
	 */
	public static Path getThumbnailDirectory() {
		return thumbnailDirectory;
	}

	/**
	 * @return {@link Path} to image directory
	 */
	public static Path getImageDirectory() {
		return imageDirectory;
	}

	/**
	 * @return {@link Path} to database file
	 */
	public static Path getDatabaseFile() {
		return databaseFile;
	}

	/**
	 * Method is called when server is started.
	 * It initialises all paths.
	 * @param sce {@link ServletContextEvent}
	 */
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		imageDirectory = Paths.get(sce.getServletContext().getRealPath("/WEB-INF/gallery"));
		thumbnailDirectory = Paths.get(sce.getServletContext().getRealPath("/WEB-INF/thumbnail"));
		databaseFile = Paths.get(sce.getServletContext().getRealPath("/WEB-INF/opisnik.txt"));
		
	}

	/**
	 * Does nothing
	 */
	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		
	}

}
