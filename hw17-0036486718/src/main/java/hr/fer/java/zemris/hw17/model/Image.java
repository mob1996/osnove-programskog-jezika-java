package hr.fer.java.zemris.hw17.model;

import java.util.Set;

/**
 * Model of images stored in database.
 * @author Luka Mijić
 *
 */
public class Image {

	/**
	 * Image name
	 */
	private String name;
	
	/**
	 * Image description
	 */
	private String desc;
	
	/**
	 * Image tags
	 */
	private Set<String> tags;
	
	/**
	 * Creates new {@link Image} from given parameters.
	 * @param name of image
	 * @param desc is description of image
	 * @param tags all tags of image
	 */
	public Image(String name, String desc, Set<String> tags) {
		this.name = name;
		this.desc = desc;
		this.tags = tags;
	}
	
	/**
	 * @return image name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return description of image
	 */
	public String getDesc() {
		return desc;
	}
	
	/**
	 * @return image tags
	 */
	public Set<String> getTags() {
		return tags;
	}
	
	
}
