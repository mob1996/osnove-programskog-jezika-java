package hr.fer.java.zemris.hw17.servlets;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.java.zemris.hw17.database.PathProvider;
import hr.fer.zemris.hw17.util.ImageExtension;
import hr.fer.zemris.hw17.util.ImageUtil;

/**
 * Servlet is mapped to "/image/*".
 * If * part is empty or invalid default image is sent back.
 * @author Luka Mijić
 *
 */
@WebServlet("/image/*")
public class ImageServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * Method handles GET requests.
	 * In url everything after "/image/" is treated as
	 * parameters separated by "/". If there is more than one parameter
	 * or if there are no parameters or parameter doesn't have valid name
	 * servlet returns default image that is basically image of question mark.
	 * 
	 * Otherwise send valid image.
	 * @param req {@link HttpServletRequest}
	 * @param resp {@link HttpServletResponse}
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if(req.getPathInfo() == null || req.getPathInfo().length() == 1) {
			defaultImage(resp);
			return;
		}
		String[] params = req.getPathInfo().substring(1).split("/");
	
		if(params.length != 1) {
			defaultImage(resp);
			return;
		}
		String imgName = params[0];
		String extension = ImageUtil.getExtension(imgName);
		if(extension == null || !ImageExtension.isValid(extension)) {
			defaultImage(resp);
			return;
		}
		
		Path imagePath = PathProvider.getImageDirectory().resolve(imgName);
		if(!Files.exists(imagePath)) {
			defaultImage(resp);
			return;
		}
		
		resp.setContentType("image/" + extension);
		InputStream is = Files.newInputStream(imagePath);
		ImageUtil.writeImage(is, resp.getOutputStream());
		is.close();
	}
	
	/**
	 * Send default image (question mark image) to {@link OutputStream}
	 * provided by given {@link HttpServletResponse}.
	 * @param req {@link HttpServletRequest}
	 * @throws IOException
	 */
	private void defaultImage(HttpServletResponse resp) throws IOException {
		resp.setContentType("image/png");
		Path defaultPath = PathProvider.getImageDirectory().resolve("unknown.jpg");
		InputStream is = Files.newInputStream(defaultPath);
		ImageUtil.writeImage(is , resp.getOutputStream());
		is.close();
	}
	
	
}
