package hr.fer.java.zemris.hw17.database;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import hr.fer.java.zemris.hw17.model.Image;

/**
 * Class that has methods to access data
 * stored in database.
 * @author Luka Mijić
 *
 */
public class ImageDB {

	/**
	 * @return number of database entries in 
	 * @throws IOException if error reading file occurs
	 */
	public static int getNumberOfImages() throws IOException {
		return Files.readAllLines(PathProvider.getDatabaseFile(), StandardCharsets.UTF_8).size() / 3;
	}
	
	/**
	 * @param name of image
	 * @return {@link Image} associated to given name
	 * @throws IOException if error reading file occurs
	 */
	public static Image getImage(String name) throws IOException {
		List<String> lines = Files.readAllLines(PathProvider.getDatabaseFile(), StandardCharsets.UTF_8);
		for(int i = 0; i < lines.size(); i = i + 3) {
			if(lines.get(i).equals(name)) {
				return fromLines(lines.get(i), lines.get(i + 1), lines.get(i + 2));
			}
		}
		return null;
	}
	
	/**
	 * Create {@link List} of {@link Image}s that stores
	 * all {@link Image} whose {@link Image#getTags()} {@link Set}
	 * contains at least one of given tags.
	 * @param tags that are searched for
	 * @return {@link List} of {@link Image}s associated with given tags
	 * @throws IOException if error reading file occurs
	 */
	public static List<Image> forTags(String ... tags) throws IOException{
		List<Image> images = new LinkedList<>();
		List<String> lines = Files.readAllLines(PathProvider.getDatabaseFile(), StandardCharsets.UTF_8);
		for(int i = 2; i < lines.size(); i = i + 3) {
			Set<String> imgTags = tags(lines.get(i));
			
			for(String t:tags) {
				if(imgTags.contains(t.toLowerCase())) {
					images.add(new Image(lines.get(i - 2), lines.get(i - 1), imgTags));
					break;
				}
			}
		}
		
		return images;
	}
	
	/**
	 * @return {@link Set} that contains all tags in {@link Image}s.
	 * @throws IOException if error reading file occurs
	 */
	public static Set<String> allTags() throws IOException{
		Set<String> tags = new TreeSet<>();
		List<String> lines = Files.readAllLines(PathProvider.getDatabaseFile(), StandardCharsets.UTF_8);
		for(int i = 2; i < lines.size(); i = i + 3) {
			tags.addAll(tags(lines.get(i)));
		}
		
		return tags;
	}
	
	/**
	 * Creates {@link Image} object from given String.
	 * @param name of image
	 * @param desc is description of image
	 * @param tags contains all image tags
	 * @return
	 */
	private static Image fromLines(String name, String desc, String tags) {
		return new Image(name, desc, tags(tags));
	}
	
	/**
	 * Method parses {@link Set} of tags from
	 * given {@link String} text. Tags are separated by
	 * ',' character.
	 * @param tagsStr {@link String} of tags
	 * @return {@link Set} containing tags
	 */
	private static Set<String> tags(String tagsStr){
		Set<String> tags = new TreeSet<>();
		for(String tag:tagsStr.split(",")) {
			tags.add(tag.toLowerCase().trim());
		}
		return tags;
	}

}
