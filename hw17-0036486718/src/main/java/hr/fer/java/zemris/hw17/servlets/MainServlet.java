package hr.fer.java.zemris.hw17.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Main {@link HttpServlet}.
 * It leads to the main page of web application.
 * @author Luka Mijić
 *
 */
@WebServlet(urlPatterns= {"", "/index", "/index.html"})
public class MainServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * Servlet gets {@link RequestDispatcher} from {@link HttpServletRequest}
	 * and dispatches it to "/WEB-INF/index.html"
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("/WEB-INF/index.html").forward(req, resp);
	}
}
