package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * * 
 * Class used to model exceptions that can occur while
 * working with lexical analyser. 
 * @author Luka Mijić
 */
public class LexerException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor
	 */
	public LexerException() {
		super();
	}

	/**
	 * Constructor that takes message argument.
	 * @param message is used to give better feedback
	 */
	public LexerException(String message) {
		super(message);
	}
}
