package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Class extends Element class and represents
 * function element.
 * @author Luka Mijić
 *
 */
public class ElementFunction extends Element{
	
	/**
	 * String value that stores name of function
	 */
	private String name;
	
	/**
	 * Constructor that sets value to argument 
	 * value. 
	 * @param value
	 */
	public ElementFunction(String name) {
		if(name == null) throw new IllegalArgumentException("Argument name cannot be null.");
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	/**
	 * @return String name
	 */
	@Override
	public String asText() {
		return name;
	}
}
