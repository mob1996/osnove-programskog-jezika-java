package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Class extends Element class and represents
 * constant integer elements. 
 * @author Luka Mijić
 *
 */
public class ElementConstantInteger extends Element{
	
	/**
	 * Integer value that class stores
	 */
	private int value;
	
	/**
	 * Constructor that sets value to argument 
	 * value. 
	 * @param value
	 */
	public ElementConstantInteger(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
	
	/**
	 * @return String representation of 
	 * 			int value
	 */
	@Override
	public String asText() {
		return String.valueOf(value);
	}
}
