package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Class extends Element class and represents
 * operator element. 
 * @author Mob
 *
 */
public class ElementOperator extends Element{
	
	/**
	 * String value that stores symbol of operator
	 */
	private String symbol;
	
	/**
	 * Constructor that sets value to argument 
	 * value. 
	 * @param value
	 */
	public ElementOperator(String symbol) {
		if(symbol == null) throw new IllegalArgumentException("Argument symbol cannot be null.");
		this.symbol = symbol;
	}
	
	public String getSymbol() {
		return symbol;
	}
	
	/**
	 * @return Symbol
	 */
	@Override
	public String asText() {
		return symbol;
	}
}
