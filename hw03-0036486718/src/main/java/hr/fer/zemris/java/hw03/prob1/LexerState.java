package hr.fer.zemris.java.hw03.prob1;

/**
 * This enum class represents valid working states
 * of lexical analyser.
 * @author Luka Mijić
 */
public enum LexerState {
	/**
	 * Basic mode of Lexer. When Lexer works
	 * in this mode it can generate EOF, WORD,
	 * NUMBER and SYMBOL. 
	 */
	BASIC, 
	/**
	 * Extended mode of Lexer. Cannot generate numbers and
	 * only symbol token it can generate is '#'.
	 */
	EXTENDED;
}
