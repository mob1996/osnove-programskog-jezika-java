package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Class that extends Element, 
 * represents variable element.
 * @author Luka Mijić
 *
 */
public class ElementVariable extends Element{
	
	/**
	 * Name of variable
	 */
	private String name;
	
	/**
	 * Constructor of ElementVariable.
	 * Sets name to argument name.
	 * @param name of the variable 
	 */
	public ElementVariable(String name) {
		if(name == null) throw new IllegalArgumentException("Argument name cannot be null.");
		
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	/**.
	 * @return String name
	 */
	@Override
	public String asText() {
		return name;
	}
}
