package hr.fer.zemris.java.custom.collections;

/**
 * 
 * @author Luka Mijić
 * Class Processor is template
 * for creating local classes that 
 * implement method process.
 *
 */
public class Processor {

	/**
	 * Method used for processing given value.
	 * @param value value that is processed
	 */
	public void process(Object value) {
		
	}
}
