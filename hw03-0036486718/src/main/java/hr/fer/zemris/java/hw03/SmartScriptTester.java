package hr.fer.zemris.java.hw03;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.Node;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParserException;

public class SmartScriptTester {

	public static void main(String[] args) {
		if(args.length != 1) {
			System.err.println("Invalid numbers of arguments");
			return;
		}
		
		String filepath = args[0];
		String docBody;

		SmartScriptParser parser = null;
		try {
			docBody = new String(Files.readAllBytes(Paths.get(filepath)), StandardCharsets.UTF_8);
			parser = new SmartScriptParser(docBody);
			DocumentNode document = parser.getDocumentNode();
			String originalDocumentBody = createOriginalDocumentBody(document);
			SmartScriptParser parser2 = new SmartScriptParser(originalDocumentBody);
			String originalDocumentBody2 = createOriginalDocumentBody(parser2.getDocumentNode());
			System.out.println(originalDocumentBody); 
			System.out.println(originalDocumentBody.equals(originalDocumentBody2));
		} catch (SmartScriptParserException e) {
			System.out.println("Unable to parse document!");
			System.exit(-1);
		}catch (IOException e) {
			System.out.println("Unable to open filepath");
			System.exit(-1);
		} catch (Exception e) {
			System.out.println("If this line ever executes, you have failed this class!");
			System.exit(-1);
		}
		
	}

	private static String createOriginalDocumentBody(DocumentNode document) {
		StringBuilder docBuilder = new StringBuilder();

		appendNode(document, docBuilder, true);
		
		return docBuilder.toString();
	}
	
	private static void appendNode(Node node, StringBuilder sb, boolean root) {
		sb.append(node.toString());
		int numberOfChildren = node.numberOfChildren();
		if(numberOfChildren == 0) return;
		
		for(int i = 0; i < numberOfChildren; i++) {
			appendNode(node.getChild(i), sb, false);
		}
		
		if(!root) {
			sb.append("{$END$}");
		}
	}
}
