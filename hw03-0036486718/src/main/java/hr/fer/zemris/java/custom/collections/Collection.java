package hr.fer.zemris.java.custom.collections;

/**
 * Class that represents models collection of values
 * @author Luka Mijic
 * @version 1.0
 */
public class Collection {

	/**
	 * Default constructor
	 */
	protected Collection() {
		
	}
	
	/**
	 * Public method that checks if collections is empty.
	 * @return true if collection is empty,
	 * 		   otherwise it returns false 
	 */
	public boolean isEmpty() {
		return size() == 0 ? true : false;
	}
	
	/**
	 * Public method that returns current size of collection
	 * @return size of the collection
	 */
	public int size() {
		return 0;
	}
	
	/**
	 * Public method that adds new element to the collection
	 * @param value object to be added
	 */
	public void add(Object value) {
		
	}
	
	/**
	 * Public method that checks if collection has given value stored in it.
	 * @param value value that we look for
	 * @return true if collection contains value,
	 *         otherwise return false
	 */
	public boolean contains(Object value) {
		return false;
	}
	
	/**
	 * Public method that removes value from collection if given value
	 * is already stored.
	 * @param value value that we want to remove from collection
	 * @return true if removal is successful, otherwise return false
	 */
	public boolean remove(Object value) {
		return false;
	}
	
	/**
	 * Public method that allocates new array with size
	 * equal to the size of the collection. Fill new array with collections
	 * content.
	 * @return the new array
	 * @throws UnsupportedOperationException always.
	 */
	public Object[] toArray() {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * 
	 * @param processor
	 */
	public void forEach(Processor processor) {
		
	}
	
	/**
	 * Public method that adds all elements from given collection
	 * into current collection. Given collection is not changed.
	 * @param other elements from this collections are added to current collection
	 */
	public void addAll(Collection other) {
		class AddProcessor extends Processor{
			
			@Override
			public void process(Object value) {
				add(value);
			}
		}
		
		other.forEach(new AddProcessor());
	}
	
	/**
	 * Public method that removes all elements from collection
	 */
	public void clear() {
		
	}
	
	
}
