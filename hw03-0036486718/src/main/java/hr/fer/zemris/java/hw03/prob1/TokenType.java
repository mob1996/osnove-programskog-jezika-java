package hr.fer.zemris.java.hw03.prob1;

/**
 * 
 * This enum class represents all types of Tokens
 * @author Luka Mijić
 * 
 */
public enum TokenType {
	/**
	 * This token type represents end of file.
	 * No more tokens can't be generated after this one
	 * is generated.
	 */
	EOF, 
	
	/**
	 * This token type represents word generated from
	 * letters. In context of Lexer working in BASIC mode 
	 * letters are 'a'-'z' and 'A'-'Z', numbers and '\' that 
	 * have symbol '\' before it. In EXTENDED mode
	 * everything except white spaces and '#' is considered letter.
	 */
	WORD,
	/**
	 * This token type represents numbers generated from Lexer.
	 * Numbers must be able to stored in variable of type LONG.
	 */
	NUMBER, 
	/**
	 * Everything else.
	 */
	SYMBOL;
}
