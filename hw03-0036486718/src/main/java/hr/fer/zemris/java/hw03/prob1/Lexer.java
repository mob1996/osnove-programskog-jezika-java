package hr.fer.zemris.java.hw03.prob1;

import java.math.BigInteger;

/**
 * This class represents functional lexical 
 * analyser that creates new Tokens from input String.
 * @author Luka Mijić
 */
public class Lexer {

	/**
	 * Input text is stored into char array
	 */
	private char[] data;
	
	/**
	 * Current token
	 */
	private Token token;
	
	/**
	 * Index of first index that is not used.
	 * Initial value is 0.
	 */
	private int currentIndex;
	
	/**
	 * Current state of lexical analyser.
	 */
	private LexerState state;	
	
	/**
	 * Constructor takes String text and transforms it
	 * into char array. 
	 * @param text that is transformed
	 * @throws IllegalArgumentException if given argument is null
	 */
	public Lexer(String text) {
		if(text == null) throw new NullPointerException("Text argument in Lexer constructor cannot be null.");
	
		data = text.toCharArray();
		state = LexerState.BASIC;
	}

    /**
     * 
     * @return token that was generated last
     */
	public Token getToken() {
		return token;
	}
	
	/**
	 * Sets parameter state to argument state.
	 * @param state is new state of lexer
	 */
	public void setState(LexerState state) {
		if(state == null) throw new NullPointerException("Given state cannot be null");
		this.state = state;
	}
	
	/**
	 * Method is used to generate next token from input string.
	 * @return next token from string
	 * @throws LexerException if EOF was reached
	 */
	public Token nextToken() {
		if(token != null && token.getType() == TokenType.EOF) throw new LexerException("No more tokens avaliable");
		
		skipWhiteSpace();
		
		if(currentIndex >= data.length) {
			token = new Token(TokenType.EOF, null);
			return token;
		}
		
		token = state == LexerState.BASIC ? nextTokenBasic() : nextTokenExtended();
		
		return token;
	}

	/**
	 * This method is called by nextToken() if Lexer is working in BASIC state.
	 * @return token generated by lexer in BASIC state
	 */
	private Token nextTokenBasic() {
		char currentChar = data[currentIndex];
		
		if(Character.isLetter(currentChar) || currentChar == '\\') {
			return nextWordBasic();
		} else if(Character.isDigit(currentChar)) {
			return nextNumberBasic();
		} else {
			currentIndex++;
			return new Token(TokenType.SYMBOL, Character.valueOf(currentChar));
		}
	}
	
	/**
	 * This method generates new Token of type 
	 * WORD while lexer works in BASIC state.
	 * @return new WORD type Token
	 */
	private Token nextWordBasic() {
		StringBuilder wordBuilder = new StringBuilder();
		
		while(currentIndex < data.length) {
			char currentChar = data[currentIndex];
			
			if(Character.isLetter(currentChar)) {
				wordBuilder.append(currentChar);
				currentIndex++;
			} else if(currentChar == '\\') {
				currentIndex++;
				
				if(currentIndex >= data.length) {
					throw new LexerException("'\\' cannot be last element of input string.");
				}
				
				currentChar = data[currentIndex++];
				
				if(!Character.isDigit(currentChar) && currentChar != '\\') {
					throw new LexerException("'\\' is not followed by a number or '\\'");
				}
				
				wordBuilder.append(currentChar);
			} else {
				break;
			}
		}
		
		return new Token(TokenType.WORD, wordBuilder.toString());
	}
	
	/**
	 * This method generates new Token of type 
	 * WORD while lexer works in BASIC state.
	 * @return new Token of type NUMBER
	 */
	private Token nextNumberBasic() {
		StringBuilder numberBuilder = new StringBuilder();
		
		while(currentIndex < data.length) {
			char currentChar = data[currentIndex];
			
			if(!Character.isDigit(currentChar)) break;
			
			numberBuilder.append(currentChar);
			currentIndex++;
			
			BigInteger bigInt = new BigInteger(numberBuilder.toString());
			BigInteger maxLong = BigInteger.valueOf(Long.MAX_VALUE);
			
			if(bigInt.compareTo(maxLong) > 0) throw new LexerException("Number is too big to be parsed as LONG");		
				
		}
		
		return new Token(TokenType.NUMBER, Long.valueOf(numberBuilder.toString()));
	}
	
	/**
	 * This method is called by nextToken() if Lexer is working in EXTENDED state.
	 * @return token generated by lexer in EXTENDED state
	 */
	private Token nextTokenExtended() {
		if(data[currentIndex] == '#') {
			currentIndex++;
			return new Token(TokenType.SYMBOL, Character.valueOf('#'));
		} else {
			StringBuilder wordBuilder = new StringBuilder();
			while(currentIndex < data.length) {
				if(data[currentIndex] == '#' || Character.isWhitespace(data[currentIndex])) break;
				
				wordBuilder.append(data[currentIndex++]);
			}
			return new Token(TokenType.WORD, wordBuilder.toString());
		}
	}
	
	/**
	 * Method is used to skip all white space from current index 
	 * onwards. After calling this method index is at location of next
	 * non whitespace character or it is out of bounds. 
	 */
	private void skipWhiteSpace() {
		while(currentIndex < data.length) {
			if(!Character.isWhitespace(data[currentIndex])) break;
			currentIndex++;
		}
	}

}
