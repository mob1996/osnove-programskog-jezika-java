package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.scripting.elems.Element;

/**
 * Class represents one EchoNode node in syntax tree.
 * Class extends Node class.
 * @author Luka Mijić
 *
 */
public class EchoNode extends Node {

	/**
	 * Elements inside echo tags
	 */
	private Element[] elements;
	
	/**
	 * Constructor for EchoNode that takes
	 * argument elements as elementy propery
	 * @param elements 
	 */
	public EchoNode(Element[] elements) {
		if(elements == null)
			throw new NullPointerException("Argument elements in Echo node constructor can't be null.");
		this.elements = elements;
	}

	
	public Element[] getElements() {
		return elements;
	}
	
	@Override
	public String toString() {
		StringBuilder echoBuilder = new StringBuilder();
		echoBuilder.append("{$=");
		for(int i = 0; i < elements.length; i++) {
			echoBuilder.append(elements[i].asText());
			if(i < elements.length-1) {
				echoBuilder.append(" ");
			}
		}
		
		echoBuilder.append("$}");
		return echoBuilder.toString();
	}
}
