package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * Class represents one Document node in syntax tree.
 * DocumentNode is starting point in sytax tree.
 * Class extends Node class.
 * @author Mob
 *
 */
public class DocumentNode extends Node {


	@Override
	public String toString() {
		return "";
	}
}
