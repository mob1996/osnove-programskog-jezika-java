package hr.fer.zemris.java.hw03.prob1;
/**
 * 
 * Class Token represents combination of
 * TokenType and value created by lexical analyser.
 * @author Luka Mijić
 * 
 */
public class Token {

	private TokenType type;
	private Object value;
	
	/**
	 * Constructor that takes two arguments for 
	 * creation of new Token. 
	 * @param type of created Token
	 * @param value of created Token
	 */
	public Token(TokenType type, Object value) {
		if(type == null) throw new NullPointerException("Type cannot be null.");
		
		this.type = type;
		this.value = value;
	}

	public TokenType getType() {
		return type;
	}

	public Object getValue() {
		return value;
	}
	
	
}
