package hr.fer.zemris.java.custom.scripting.lexer;

public enum LexerState {
	/**
	 * Mode that looks for TEXT and START_TAG TokenType
	 */
	TEXT_MODE,
	/**
	 * Mode that should be entered after reading START_TAG
	 * and should be changed after reading END_TAG.
	 */
	TAG_MODE
}
