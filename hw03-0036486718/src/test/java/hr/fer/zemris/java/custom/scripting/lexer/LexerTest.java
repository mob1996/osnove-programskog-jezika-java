package hr.fer.zemris.java.custom.scripting.lexer;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

public class LexerTest {

	// Helper method for checking if lexer generates the same stream of tokens
	// as the given stream.
	private void checkTokenStream(Lexer lexer, Token[] correctData) {
		int counter = 0;
		for (Token expected : correctData) {
			Token actual = lexer.nextToken();
			String msg = "Checking token " + counter + ":";
			assertEquals(msg, expected.getType(), actual.getType());
			assertEquals(msg, expected.getValue(), actual.getValue());
			counter++;
		}
	}

	@Test
	public void testNotNull() {
		Lexer lexer = new Lexer("");
		
		assertNotNull("Token was expected but null was returned.", lexer.nextToken());
	}
	
	
	@Test(expected=NullPointerException.class)
	public void testNullInputInConstructor() {
		// must throw!
		new Lexer(null);
	}
	
	@Test
	public void testIfGetReturnsSame() {
		Lexer lexer = new Lexer("");
		
		Token token = lexer.nextToken();
		assertEquals("getToken returned different token than nextToken.", token, lexer.getToken());
		assertEquals("getToken returned different token than nextToken.", token, lexer.getToken());
	}
	
	@Test (expected=LexerException.class)
	public void testNextAfterEOF() {
		Lexer lexer = new Lexer("");
		
		lexer.nextToken();
		lexer.nextToken();
	}
	
	@Test (expected=LexerException.class)
	public void testWithInvalidCurlyBracket() {
		Lexer lexer = new Lexer("  Cars can be{ very fast ");
		lexer.nextToken();
		lexer.nextToken();
	}
	
	@Test (expected=LexerException.class)
	public void testWithInvalidEscape() {
		Lexer lexer = new Lexer("  Cars can be\\ very fast ");
		lexer.nextToken();
	}
	
	@Test
	public void testWithValidCurlyBracketAndEscape() {
		Lexer lexer = new Lexer(" Cars can be \\{ very fast\\\\very slow}.\n\t");
		
		Token token = lexer.nextToken();
		
		assertEquals(" Cars can be { very fast\\very slow}.\n\t", (String) token.getValue());
		assertEquals(TokenType.EOF, lexer.nextToken().getType());
	}
	
	@Test
	public void testStartTag() {
		Lexer lexer = new Lexer("{$");
		
		assertEquals("{$", lexer.nextToken().getValue());
		assertEquals(TokenType.START_TAG, lexer.getToken().getType());
		
		lexer = new Lexer("   {$ cars ");
		
		assertEquals("   ", lexer.nextToken().getValue());
		assertEquals(TokenType.TEXT, lexer.getToken().getType());
		
		assertEquals("{$", lexer.nextToken().getValue());
		assertEquals(TokenType.START_TAG, lexer.getToken().getType());
		
		assertEquals(" cars ", lexer.nextToken().getValue());
		assertEquals(TokenType.TEXT, lexer.getToken().getType());
	}
	
	
	@Test
	public void testTextModeComplicated() {
		Lexer lexer = new Lexer(" The Last of Us should win {$\\{Game of the Year\\\\ Game of the Decade}\n"
				+ " award. It was truly great experience. {$. \\\\Joel and Ellie are both great\n"
				+ "characters.");
		
		Token correctData[] = {
				new Token(TokenType.TEXT, " The Last of Us should win "),
				new Token(TokenType.START_TAG, "{$"),
				new Token(TokenType.TEXT, "{Game of the Year\\ Game of the Decade}\n"
				+ " award. It was truly great experience. "),
				new Token(TokenType.START_TAG, "{$"),
				new Token(TokenType.TEXT, ". \\Joel and Ellie are both great\n"
				+ "characters.")
		};
		
		checkTokenStream(lexer, correctData);
	
	}
	
	
	@Test (expected = LexerException.class)
	public void testInvalidString() {
		Lexer lexer = new Lexer(" Cars {$ \"9k");
		
		Token token = lexer.nextToken();
		assertEquals(TokenType.TEXT, token.getType());
		assertEquals(" Cars ", token.getValue());
		
		token = lexer.nextToken();
		assertEquals(TokenType.START_TAG, token.getType());
		assertEquals("{$", token.getValue());
		
		lexer.setState(LexerState.TAG_MODE);
		
		token = lexer.nextToken();
	}
	
	@Test 
	public void testValidString() {
		Lexer lexer = new Lexer(" Cars {$ \"9k\"");
		
		Token token = lexer.nextToken();
		assertEquals(TokenType.TEXT, token.getType());
		assertEquals(" Cars ", token.getValue());
		
		token = lexer.nextToken();
		assertEquals(TokenType.START_TAG, token.getType());
		assertEquals("{$", token.getValue());
		
		lexer.setState(LexerState.TAG_MODE);
		
		token = lexer.nextToken();
		assertEquals(TokenType.STRING, token.getType());
		assertEquals("\"9k\"", token.getValue());
	}
	
	@Test (expected = LexerException.class)
	public void testInvalidVariable() {
		Lexer lexer = new Lexer("{$ _current2_Index22");
		
		Token token = lexer.nextToken();
		assertEquals(TokenType.START_TAG, token.getType());
		assertEquals("{$", token.getValue());
		
		lexer.setState(LexerState.TAG_MODE);
		
		token = lexer.nextToken();
		
	}
	
	@Test 
	public void testValidVariable() {
		Lexer lexer = new Lexer("{$ current2_Index2");
		
		Token token = lexer.nextToken();
		assertEquals(TokenType.START_TAG, token.getType());
		assertEquals("{$", token.getValue());
		
		lexer.setState(LexerState.TAG_MODE);
		
		token = lexer.nextToken();
		assertEquals(TokenType.VARIABLE, token.getType());
		assertEquals("current2_Index2", token.getValue());
	}
	
	@Test(expected = LexerException.class)
	public void testInvalidFunction() {
		Lexer lexer = new Lexer("{$ @");
		
		Token token = lexer.nextToken();
		assertEquals(TokenType.START_TAG, token.getType());
		assertEquals("{$", token.getValue());
		
		lexer.setState(LexerState.TAG_MODE);
		
		lexer.nextToken();
	}
	
	@Test(expected = LexerException.class)
	public void testInvalidFunction2() {
		Lexer lexer = new Lexer("{$ @2");
		
		Token token = lexer.nextToken();
		assertEquals(TokenType.START_TAG, token.getType());
		assertEquals("{$", token.getValue());
		
		lexer.setState(LexerState.TAG_MODE);
		
		lexer.nextToken();
	}
	
	@Test
	public void testValidFunction() {
		Lexer lexer = new Lexer("{$ @sin_squared1");
		
		Token token = lexer.nextToken();
		assertEquals(TokenType.START_TAG, token.getType());
		assertEquals("{$", token.getValue());
		
		lexer.setState(LexerState.TAG_MODE);
		
		token = lexer.nextToken();
		
		assertEquals(TokenType.FUNCTION, token.getType());
		assertEquals("@sin_squared1", token.getValue());
	}
	
	@Test 
	public void testIntegerFollowedByVariable(){
		Lexer lexer = new Lexer("{$ 20k3");
		
		Token token = lexer.nextToken();
		assertEquals(TokenType.START_TAG, token.getType());
		assertEquals("{$", token.getValue());
		
		lexer.setState(LexerState.TAG_MODE);
		
		lexer.nextToken();
		assertEquals(TokenType.INTEGER, lexer.getToken().getType());
		assertEquals(Integer.valueOf(20), lexer.getToken().getValue());
		
		lexer.nextToken();
		assertEquals(TokenType.VARIABLE, lexer.getToken().getType());
		assertEquals("k3", lexer.getToken().getValue());
		
		lexer.nextToken();
		assertEquals(TokenType.EOF, lexer.getToken().getType());
	}
	
	@Test 
	public void testDoubleInteger(){
		Lexer lexer = new Lexer("{$ 2.0k3");
		
		Token token = lexer.nextToken();
		assertEquals(TokenType.START_TAG, token.getType());
		assertEquals("{$", token.getValue());
		
		lexer.setState(LexerState.TAG_MODE);
		
		lexer.nextToken();
		assertEquals(TokenType.DOUBLE, lexer.getToken().getType());
		assertEquals(Double.valueOf(2.0), lexer.getToken().getValue());
		
		lexer.nextToken();
		assertEquals(TokenType.VARIABLE, lexer.getToken().getType());
		assertEquals("k3", lexer.getToken().getValue());
		
		lexer.nextToken();
		assertEquals(TokenType.EOF, lexer.getToken().getType());
	}
	
	@Test 
	public void testNegativeIntegerFollowedByVariable(){
		Lexer lexer = new Lexer("{$ -20k3");
		
		Token token = lexer.nextToken();
		assertEquals(TokenType.START_TAG, token.getType());
		assertEquals("{$", token.getValue());
		
		lexer.setState(LexerState.TAG_MODE);
		
		lexer.nextToken();
		assertEquals(TokenType.INTEGER, lexer.getToken().getType());
		assertEquals(Integer.valueOf(-20), lexer.getToken().getValue());
		
		lexer.nextToken();
		assertEquals(TokenType.VARIABLE, lexer.getToken().getType());
		assertEquals("k3", lexer.getToken().getValue());
		
		lexer.nextToken();
		assertEquals(TokenType.EOF, lexer.getToken().getType());

	}
	
	@Test 
	public void testNegativeDoubleFollowedByVariable(){
		Lexer lexer = new Lexer("{$ -2.0k3");
		
		Token token = lexer.nextToken();
		assertEquals(TokenType.START_TAG, token.getType());
		assertEquals("{$", token.getValue());
		
		lexer.setState(LexerState.TAG_MODE);
		
		lexer.nextToken();
		assertEquals(TokenType.DOUBLE, lexer.getToken().getType());
		assertEquals(Double.valueOf(-2.0), lexer.getToken().getValue());
		
		lexer.nextToken();
		assertEquals(TokenType.VARIABLE, lexer.getToken().getType());
		assertEquals("k3", lexer.getToken().getValue());
		
		lexer.nextToken();
		assertEquals(TokenType.EOF, lexer.getToken().getType());
	}
	
	@Test 
	public void testValidIntegersAndDoubles(){
		Lexer lexer = new Lexer("{$ 4342   -234 2.314 -2.314");
		
		Token token = lexer.nextToken();
		assertEquals(TokenType.START_TAG, token.getType());
		assertEquals("{$", token.getValue());
		
		lexer.setState(LexerState.TAG_MODE);
		
		token = lexer.nextToken();
		assertEquals(TokenType.INTEGER, token.getType());
		assertEquals(Integer.valueOf("4342"), token.getValue());
		
		token = lexer.nextToken();
		assertEquals(TokenType.INTEGER, token.getType());
		assertEquals(Integer.valueOf("-234"), token.getValue());
		
		token = lexer.nextToken();
		assertEquals(TokenType.DOUBLE, token.getType());
		assertEquals(Double.valueOf("2.314"), token.getValue());
		
		token = lexer.nextToken();
		assertEquals(TokenType.DOUBLE, token.getType());
		assertEquals(Double.valueOf("-2.314"), token.getValue());
		
		token = lexer.nextToken();
		assertEquals(TokenType.EOF, token.getType());
		
		
	}
	
	@Test 
	public void testValidOperators(){
		Lexer lexer = new Lexer("{$ +-*/^");
		
		Token token = lexer.nextToken();
		assertEquals(TokenType.START_TAG, token.getType());
		assertEquals("{$", token.getValue());
		
		lexer.setState(LexerState.TAG_MODE);
		
		Token[] correctData = {
				new Token(TokenType.OPERATOR, Character.valueOf('+')),
				new Token(TokenType.OPERATOR, Character.valueOf('-')),
				new Token(TokenType.OPERATOR, Character.valueOf('*')),
				new Token(TokenType.OPERATOR, Character.valueOf('/')),
				new Token(TokenType.OPERATOR, Character.valueOf('^')),
				new Token(TokenType.EOF, null)
		};
		
		checkTokenStream(lexer, correctData);
	}
	
	@Test
	public void testValidMultiple() {
		Lexer lexer = new Lexer("{$ current2_Index2+\"variable value is 25\" "
				+ "*@sin_Function1-curInd_22$} This was a tag. "
				+ "{$= variable2 2.3^-2 -3.2 \"-3\" + 3.2-2 @k$} ja sam kalkulator\n");
		
		Token token = lexer.nextToken();
		assertEquals(TokenType.START_TAG, token.getType());
		assertEquals("{$", token.getValue());
		
		lexer.setState(LexerState.TAG_MODE);
		 
		Token[] correctData = {
			new Token(TokenType.VARIABLE, "current2_Index2"),
			new Token(TokenType.OPERATOR, Character.valueOf('+')),
			new Token(TokenType.STRING, "\"variable value is 25\""),
			new Token(TokenType.OPERATOR, Character.valueOf('*')),
			new Token(TokenType.FUNCTION, "@sin_Function1"),
			new Token(TokenType.OPERATOR, Character.valueOf('-')),
			new Token(TokenType.VARIABLE, "curInd_22"),
			new Token(TokenType.END_TAG, "$}")
		};
		
		checkTokenStream(lexer, correctData);
		
		lexer.setState(LexerState.TEXT_MODE);
		
		token = lexer.nextToken();
			
		assertEquals(TokenType.TEXT, token.getType());
		assertEquals(" This was a tag. ", token.getValue());
		
		token = lexer.nextToken();
	
		assertEquals(TokenType.START_TAG, token.getType());
		assertEquals("{$", token.getValue());
		
		lexer.setState(LexerState.TAG_MODE);
		
		Token[] correctData2 = {
			new Token(TokenType.ECHO_TAG_NAME, Character.valueOf('=')),
			new Token(TokenType.VARIABLE, "variable2"),
			new Token(TokenType.DOUBLE, Double.valueOf("2.3")),
			new Token(TokenType.OPERATOR, Character.valueOf('^')),
			new Token(TokenType.INTEGER, Integer.valueOf("-2")),
			new Token(TokenType.DOUBLE, Double.valueOf("-3.2")),
			new Token(TokenType.STRING, "\"-3\""),
			new Token(TokenType.OPERATOR, Character.valueOf('+')),
			new Token(TokenType.DOUBLE, Double.valueOf("3.2")),
			new Token(TokenType.INTEGER, Integer.valueOf("-2")),
			new Token(TokenType.FUNCTION, "@k"),
			new Token(TokenType.END_TAG, "$}")
		};
		
		checkTokenStream(lexer, correctData2);
		
		lexer.setState(LexerState.TEXT_MODE);
		
		token = lexer.nextToken();
			
		assertEquals(TokenType.TEXT, token.getType());
		assertEquals(" ja sam kalkulator\n", token.getValue());
		
		token = lexer.nextToken();
		
		assertEquals(TokenType.EOF, token.getType());
	}
	
	
}
