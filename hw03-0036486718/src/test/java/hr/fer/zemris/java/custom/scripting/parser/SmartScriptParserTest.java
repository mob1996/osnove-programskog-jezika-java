package hr.fer.zemris.java.custom.scripting.parser;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.junit.Test;

public class SmartScriptParserTest {

	/**
	 * Method is used to load file into string.
	 * @param filename name of the file
	 * @return return string
	 */
	private String loader(String filename) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try (InputStream is = this.getClass().getClassLoader().getResourceAsStream(filename)) {
			byte[] buffer = new byte[1024];
			while (true) {
				int read = is.read(buffer);
				if (read < 1)
					break;
				bos.write(buffer, 0, read);
			}
			return new String(bos.toByteArray(), StandardCharsets.UTF_8);
		} catch (IOException ex) {
			return null;
		}
	}
	
	@Test (expected = SmartScriptParserException.class)
	public void testInvalidTagName() {
		String document = loader("invalidTagName.txt");
		
		SmartScriptParser parser = new SmartScriptParser(document);
	}
	
	@Test (expected = SmartScriptParserException.class)
	public void testNotAllowedTagName() {
		String document = loader("notAllowedTagName.txt");
		
		SmartScriptParser parser = new SmartScriptParser(document);
	}
	
	@Test (expected = SmartScriptParserException.class)
	public void testTagNotClosed() {
		String document = loader("tagNotClosed.txt");
		
		SmartScriptParser parser = new SmartScriptParser(document);
	}
	
	@Test (expected = SmartScriptParserException.class)
	public void testInvalidEndTag() {
		String document = loader("invalidEndTag.txt");
		
		SmartScriptParser parser = new SmartScriptParser(document);
	}
	
	@Test (expected = SmartScriptParserException.class)
	public void testInvalidForTag1() {
		String document = loader("invalidForTag1.txt");
		
		SmartScriptParser parser = new SmartScriptParser(document);
	}
	
	@Test (expected = SmartScriptParserException.class)
	public void testInvalidForTag2() {
		String document = loader("invalidForTag2.txt");
		
		SmartScriptParser parser = new SmartScriptParser(document);
	}
	
	@Test (expected = SmartScriptParserException.class)
	public void testInvalidForTag3() {
		String document = loader("invalidForTag3.txt");
		
		SmartScriptParser parser = new SmartScriptParser(document);
	}
	
	@Test 
	public void testValidEndTags() {
		String document = loader("validEndTags.txt");
		
		SmartScriptParser parser = new SmartScriptParser(document);
	}
	
	@Test 
	public void testValidForTags() {
		String document = loader("validForTags.txt");
		
		SmartScriptParser parser = new SmartScriptParser(document);
	}
	
	@Test 
	public void testValidEchoTags() {
		String document = loader("validEchoTags.txt");
		
		SmartScriptParser parser = new SmartScriptParser(document);
	}
	
	@Test 
	public void testParsableProgram() {
		String document = loader("validProgram.txt");
		
		SmartScriptParser parser = new SmartScriptParser(document);
	}
	
}
