package hr.fer.zemris.java.hw15.dao.jpa;

import javax.persistence.EntityManagerFactory;

/**
 * This class stores {@link EntityManagerFactory}.
 * Main functionality of this class is to give access
 * to same {@link EntityManagerFactory} throughout the project. 
 * @author Luka Mijič
 *
 */
public class JPAEMFProvider {

	/**
	 * Stored {@link EntityManagerFactory}.
	 */
	public static EntityManagerFactory emf;
	
	/**
	 * @return stored {@link EntityManagerFactory}
	 */
	public static EntityManagerFactory getEmf() {
		return emf;
	}
	
	/**
	 * @param emf is {@link EntityManagerFactory} that is stored.
	 */
	public static void setEmf(EntityManagerFactory emf) {
		JPAEMFProvider.emf = emf;
	}
}