package hr.fer.zemris.java.hw15.dao.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import hr.fer.zemris.java.hw15.dao.DAOException;

/**
 * {@link JPAEMProvider} is class that
 * provides {@link EntityManager}. Each {@link Thread} has
 * access to it's own {@link EntityManager} and references to
 * different {@link EntityManager} are stored in {@link ThreadLocal}.
 * @author Luka Mijić
 *
 */
public class JPAEMProvider {

	/**
	 * Storage of {@link EntityManager}s.
	 * {@link ThreadLocal} is basically a map that uses {@link Thread#getId()}
	 * as key value.
	 */
	private static ThreadLocal<EntityManager> locals = new ThreadLocal<>();

	/**
	 * If there is already {@link EntityManager} associated to current
	 * {@link Thread#getId()} return that {@link EntityManager}.
	 * Otherwise using {@link JPAEMFProvider} create new {@link EntityManager}
	 * and call {@link EntityManager#getTransaction()} and on received call 
	 * {@link EntityTransaction#begin()}. 
	 * @return {@link EntityManager}
	 */
	public static EntityManager getEntityManager() {
		EntityManager em = locals.get();
		if(em==null) {
			em = JPAEMFProvider.getEmf().createEntityManager();
			em.getTransaction().begin();
			locals.set(em);
		}
		return em;
	}

	/**
	 * Check {@link ThreadLocal} property map and check
	 * if any {@link EntityManager} is associated to current {@link Thread}.
	 * If there is no such object return null.
	 * 
	 * Otherwise get {@link EntityTransaction} by calling 
	 * {@link EntityManager#getTransaction()} on retrieved manager.
	 * 
	 * On call {@link EntityTransaction#commit()} and {@link EntityManager#close()}.
	 * Removed {@link EntityManager} from {@link ThreadLocal}.
	 * @throws DAOException if errors with closing occurs
	 */
	public static void close() throws DAOException {
		EntityManager em = locals.get();
		if(em==null) {
			return;
		}
		DAOException dex = null;
		try {
			em.getTransaction().commit();
		} catch(Exception ex) {
			dex = new DAOException("Unable to commit transaction.", ex);
		}
		try {
			em.close();
		} catch(Exception ex) {
			if(dex!=null) {
				dex = new DAOException("Unable to close entity manager.", ex);
			}
		}
		locals.remove();
		if(dex!=null) throw dex;
	}
	
}