package hr.fer.zemris.java.hw15.model.forms;

import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;

import hr.fer.zemris.java.hw15.crypto.PasswordEncrypt;
import hr.fer.zemris.java.hw15.dao.DAOProvider;
import hr.fer.zemris.java.hw15.model.BlogUser;


/**
 * Class extends {@link AbstractForm}.
 * This form is used for {@link BlogUser} login into system.
 * @author Luka Mijić
 *
 */
public class LoginForm extends AbstractForm {

	/**
	 * Nick of user that wants to login
	 */
	private String nick;
	
	/**
	 * Hash of user's password
	 */
	private String passwordHash;
	
	@Override
	public void fillFromHttpRequest(HttpServletRequest req) {
		this.nick = prepareInputData(req.getParameter("nick"));
		processPassword(req.getParameter("password"));
	}
	
	@Override
	public void validate() {
		clearErrors();
		if(nick.isEmpty()) {
			putError("nick", "Nick filed required.");
		}
		if(this.passwordHash == null) {
			putError("password", "Password required.");
		}
		
		if(hasErrors()) {
			return;
		}
		
		BlogUser user = DAOProvider.getDAO().getBlogUserFromNick(nick);
		
		if(user == null) {
			putError("nick", "User with given nick doesn't exist.");
		} else if(!user.getPasswordHash().equals(this.passwordHash)) {
			putError("password", "Invalid password.");
		}
	}
	
	/**
	 * This method takes password values, it doesn't store it
	 * but it calculates it's hash and stores calculated hash into form.
	 * @param password whose hash is stored
	 */
	private void processPassword(String password){
		password = prepareInputData(password);
		this.passwordHash = null;
		
		if(password.isEmpty()) {
			return;
		}

		try {
			this.passwordHash = PasswordEncrypt.encryptPassword(password);
		} catch (NoSuchAlgorithmException e) {
			
		}
	}

	/**
	 * @return form's nick property
	 */
	public String getNick() {
		return nick;
	}

	/**
	 * @param nick is new value of form's nick property
	 */
	public void setNick(String nick) {
		this.nick = nick;
	}

	/**
	 * @return form's password hash
	 */
	public String getPasswordHash() {
		return passwordHash;
	}

	/**
	 * @param passwordHash is new value of form's password hash property
	 */
	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}
	
	
}
