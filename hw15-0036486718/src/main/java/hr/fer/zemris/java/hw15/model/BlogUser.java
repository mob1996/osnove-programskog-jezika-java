package hr.fer.zemris.java.hw15.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

/**
 * Class models all properties of users.
 * This objects uses JPA annotations and can be stored
 * in database. 
 * @author Luka Mijić
 *
 */
@NamedQueries({
	@NamedQuery(name="getAuthors", query="select auth from BlogUser as auth"),
	@NamedQuery(name="byEmail", query="select user from BlogUser as user where user.email=:checkMail"),
	@NamedQuery(name="byNick", query="select user from BlogUser as user where user.nick=:checkNick"),
})
@Entity
@Table(name="blog_users")
public class BlogUser {

	/**
	 * User id
	 */
	private Long id;
	
	/**
	 * First name of user
	 */
	private String firstName;
	
	/**
	 * Last name of user
	 */
	private String lastName;
	
	/**
	 * User's nick
	 */
	private String nick;
	
	/**
	 * User's email
	 */
	private String email;
	
	/**
	 * Hash of user's password
	 */
	private String passwordHash;
	
	/**
	 * {@link List} of {@link BlogEntry} that user published.
	 */
	private List<BlogEntry> publishedBlogs;
	
	/**
	 * @return user's id
	 */
	@Id @GeneratedValue
	public Long getId() {
		return id;
	}
	
	/**
	 * @param id is new id of user
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return user's first name
	 */
	@Column(nullable=false)
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName is new first name of user
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return users last name
	 */
	@Column(nullable=false)
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName is new last name of user
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return user's nick
	 */
	@Column(unique=true, nullable=false)
	public String getNick() {
		return nick;
	}

	/**
	 * @param nick is new value of users nick
	 */
	public void setNick(String nick) {
		this.nick = nick;
	}

	/**
	 * @return user's email address
	 */
	@Column(unique=true, nullable=false)
	public String getEmail() {
		return email;
	}

	/**
	 * @param email is new email of user
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return hash of user's password
	 */
	@Column(nullable=false)
	public String getPasswordHash() {
		return passwordHash;
	}

	/**
	 * @param passwordHash
	 */
	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	/**
	 * @return {@link List} of all {@link BlogEntry} published by this user
	 */
	@OneToMany(mappedBy="author", fetch=FetchType.LAZY, cascade=CascadeType.PERSIST, orphanRemoval=true)
	@OrderBy("lastModifiedAt")
	public List<BlogEntry> getPublishedBlogs() {
		return publishedBlogs;
	}

	/**
	 * @param publishedBlogs is new {@link List} of published blogs
	 */
	public void setPublishedBlogs(List<BlogEntry> publishedBlogs) {
		this.publishedBlogs = publishedBlogs;
	}

	/**
	 * @return calculated hash code
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * @return true if two {@link BlogUser} are same, otherwise return false.
	 * 			Two {@link BlogUser}'s are same if they have same id.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BlogUser other = (BlogUser) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
