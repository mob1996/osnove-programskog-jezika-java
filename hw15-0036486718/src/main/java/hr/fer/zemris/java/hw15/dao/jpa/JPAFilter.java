package hr.fer.zemris.java.hw15.dao.jpa;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

/**
 * Class implements {@link Filter}.
 * This filter is called on request that try to get
 * to "/servleti/*" URL.
 * @author Luka Mijić
 *
 */
@WebFilter("/servleti/*")
public class JPAFilter implements Filter {

	/**
	 * Empty implementation.
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	/**
	 * Doesn't do anything on request arrival but
	 * calls {@link JPAEMProvider#close()} after processing of request.
	 * 
	 * @param request is {@link ServletRequest}
	 * @param response is {@link ServletResponse}
	 * @param chain is {@link FilterChain} objects
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		try {
			chain.doFilter(request, response);
		} finally {
			JPAEMProvider.close();
		}
	}

	/**
	 * Empty implementation
	 */
	@Override
	public void destroy() {
	}
	
}