package hr.fer.zemris.java.hw15.dao;

import hr.fer.zemris.java.hw15.dao.jpa.JPADAOImpl;

/**
 * Singleton class that provides access
 * to one implementation of {@link DAO} object.
 * @author Luka Mijić
 *
 */
public class DAOProvider {

	/**
	 * Chosen implementation of {@link DAO}.
	 */
	private static DAO dao = new JPADAOImpl();
	
	/**
	 * @return implementation of {@link DAO}
	 */
	public static DAO getDAO() {
		return dao;
	}
	
}