package hr.fer.zemris.java.hw15.model.forms;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import hr.fer.zemris.java.hw15.model.BlogEntry;

/**
 * Class extends {@link AbstractForm}.
 * This form is used for creation/editing of
 * {@link BlogEntry}.
 * @author Luka Mijić
 *
 */
public class BlogForm extends AbstractForm {

	/**
	 * Title of form
	 */
	private String title;
	
	/**
	 * Form text
	 */
	private String text;
	
	@Override
	public void fillFromHttpRequest(HttpServletRequest req) {
		this.title = prepareInputData(req.getParameter("title"));
		this.text = prepareInputData(req.getParameter("text"));
	}

	@Override
	public void validate() {
		clearErrors();
		if(title.isEmpty()) {
			putError("title", "Title field required.");
		} else if(title.length() > 200) {
			putError("title", "Title is too long. Max 200 characters.");
		}
		if(text.isEmpty()) {
			putError("text", "Text field required.");
		} else if(text.length() > 4096) {
			putError("text", "Text is too long. Max 4096 characters.");
		}
	}

	/**
	 * This method takes {@link BlogEntry} and sets
	 * its title and text properties to form values.
	 * Also it sets creation time if {@link BlogEntry#getCreatedAt()}
	 * return <code>null</code> reference and it sets current time
	 * as last modified time.
	 * @param entry that is filled with values
	 */
	public void fillFromBlogForm(BlogEntry entry) {
		entry.setText(text);
		entry.setTitle(title);
		
		if(entry.getCreatedAt() == null) {
			entry.setCreatedAt(new Date());
		}
		
		entry.setLastModifiedAt(new Date());
	}
	
	/**
	 * Fills form with text and title values from
	 * {@link BlogEntry} object.
	 * @param entry that is used for filling form
	 */
	public void fillFormFromBlog(BlogEntry entry) {
		this.title = entry.getTitle();
		this.text = entry.getText();
	}
	
	/**
	 * @return title of form.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title is new title of form
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return form's text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text is new text of form.
	 */
	public void setText(String text) {
		this.text = text;
	}

}
