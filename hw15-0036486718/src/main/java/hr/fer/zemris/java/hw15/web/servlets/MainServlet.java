package hr.fer.zemris.java.hw15.web.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw15.dao.DAOProvider;
import hr.fer.zemris.java.hw15.model.BlogUser;

/**
 * Class extends {@link HttpServlet}.
 * This Servlet handels request aimed at main screen of web application
 * It overrides only doGet method.
 * @author Luka Mijić
 *
 */
@WebServlet("/servleti/main")
public class MainServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
	
	/**
	 * It reads all {@link BlogUser} and stores {@link List} into temporary
	 * parameters and dispatches it to login.jsp.
	 * @param req {@link HttpServletRequest}
	 * @param resp {@link HttpServletResponse}
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		
		List<BlogUser> authors = DAOProvider.getDAO().getAuthors();
		req.setAttribute("authors", authors);
		
		req.getRequestDispatcher("/WEB-INF/pages/login.jsp").forward(req, resp);
	}
}
