package hr.fer.zemris.java.hw15.model.forms;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import hr.fer.zemris.java.hw15.model.BlogComment;

/**
 * Class extends {@link AbstractForm}.
 * This form is used for creation of {@link BlogComment}.
 * @author Luka Mijić
 *
 */
public class CommentForm extends AbstractForm {

	/**
	 * Email of comment's author
	 */
	private String email;
	
	/**
	 * Comment's contents
	 */
	private String comment;
	
	@Override
	public void fillFromHttpRequest(HttpServletRequest req) {
		this.email = prepareInputData(req.getParameter("email"));
		this.comment = prepareInputData(req.getParameter("comment"));
	}

	@Override
	public void validate() {
		clearErrors();
		
		if(this.email.isEmpty()) {
			putError("email", "Email is required.");
		} else {
			int l = this.email.length();
			int p = this.email.indexOf('@');
			if(l<3 || p==-1 || p==0 || p==l-1) {
				putError("email", "Email format is not valid.");
			}
		}
		
		if(this.comment.isEmpty()) {
			putError("comment", "Comment is required.");
		} else if(comment.length() > 4096) {
			putError("comment", "Comment is too big. Max. characters = 4096.");
		}
	}

	/**
	 * Method takes {@link BlogComment} and uses
	 * form's properties to fill {@link BlogComment}'s properties.
	 * It also sets postedOn property to current time.
	 * @param comment that is being filled
	 */
	public void fillFromCommentForm(BlogComment comment) {
		comment.setUsersEMail(this.email);
		comment.setMessage(this.comment);
		comment.setPostedOn(new Date());
	}
	
	/**
	 * @return form's email property
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email is new value of form's email property
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return form's comments contents property
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment is new content of form's comment property
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	
}
