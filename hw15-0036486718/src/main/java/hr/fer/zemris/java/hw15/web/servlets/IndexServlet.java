package hr.fer.zemris.java.hw15.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet maps "/" and "index.jsp" URL's.
 * This implementation of {@link HttpServlet} overrides
 * only doGet method.
 * @author Luka Mijić
 *
 */
@WebServlet(urlPatterns= {"/", "/index.jsp"})
public class IndexServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * Redirects to "servleti/main".
	 * @param req {@link HttpServletRequest}
	 * @param resp {@link HttpServletResponse}
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.sendRedirect("servleti/main");
	}
}
