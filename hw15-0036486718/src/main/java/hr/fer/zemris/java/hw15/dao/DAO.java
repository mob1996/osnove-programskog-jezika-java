package hr.fer.zemris.java.hw15.dao;

import java.util.List;

import hr.fer.zemris.java.hw15.model.BlogComment;
import hr.fer.zemris.java.hw15.model.BlogEntry;
import hr.fer.zemris.java.hw15.model.BlogUser;

/**
 * DAO - Data Access Object
 * 
 * This interface models an object used for
 * retrieving data. 
 * @author Luka Mijić
 *
 */
public interface DAO {

	/**
	 * Retrieve {@link BlogEntry} using it's id.
	 * If no {@link BlogEntry} is associated with given
	 * id return <code>null</code>.
	 * @param id of wanted {@link BlogEntry}
	 * @return wanted {@link BlogEntry} or null
	 * @throws DAOException if error occurs while fetching data
	 */
	public BlogEntry getBlogEntry(Long id) throws DAOException;
	
	/**
	 * Retrieve {@link BlogUser} whose nick property is equal
	 * to given nick parameter. If such object doesn't exist return null.
	 * @param nick of wanted {@link BlogUser}
	 * @return wanted {@link BlogUser} or null
	 * @throws DAOException if errors occurs while fetching data
	 */
	public BlogUser getBlogUserFromNick(String nick) throws DAOException;
	
	/**
	 * Retrieve {@link BlogUser} whose email property is equal
	 * to given email parameter. If such object doesn't exist return null.
	 * @param email of wanted {@link BlogUser}
	 * @return wanted {@link BlogUser} or null
	 * @throws DAOException if errors occurs while fetching data
	 */
	public BlogUser getBlogUserFromEmail(String email) throws DAOException;
	
	/**
	 * Retrieve {@link BlogUser} whose id property is equal
	 * to given id parameter. If such object doesn't exist return null.
	 * @param id of wanted {@link BlogUser}
	 * @return wanted {@link BlogUser} or null
	 * @throws DAOException if errors occurs while fetching data
	 */
	public BlogUser getBlogUser(Long id) throws DAOException;
	
	/**
	 * @return {@link List} of all {@link BlogUser}s
	 * 			registered in database.
	 * @throws DAOException if errors occurs while fetching data
	 */
	public List<BlogUser> getAuthors() throws DAOException;
	
	/**
	 * Adds given {@link BlogUser} to database.
	 * @param user that is added to database
	 * @throws DAOException if errors occurs while storing data
	 */
	public void addBlogUser(BlogUser user) throws DAOException;
	
	/**
	 * Adds {@link BlogEntry} to {@link BlogUser} whose nick property
	 * is equal to given nick parameter.
	 * @param nick of {@link BlogUser}
	 * @param blog added {@link BlogEntry}
	 * @throws DAOException if errors occurs while storing data
	 */
	public void addBlogToUser(String nick, BlogEntry blog) throws DAOException;
	
	/**
	 * Adds {@link BlogComment} to {@link BlogEntry} whose id property
	 * is equal to given blogId parameter.
	 * @param blogId id of {@link BlogEntry}
	 * @param comment is {@link BlogComment} added to {@link BlogEntry}
	 * @throws DAOException if errors occurs while storing data
	 */
	public void addCommentToBlog(Long blogId, BlogComment comment) throws DAOException;
	
	/**
	 * Updates {@link BlogEntry}'s title and text properties
	 * using given title and text parameter.
	 * @param blogId id of updated {@link BlogEntry}
	 * @param title is new title of {@link BlogEntry}
	 * @param text is new text of {@link BlogEntry}
	 * @throws DAOException if errors occures while storing data
	 */
	public void editBlog(Long blogId, String title, String text) throws DAOException;
	
}