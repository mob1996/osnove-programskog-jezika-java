package hr.fer.zemris.java.hw15.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import hr.fer.zemris.java.hw15.dao.DAOProvider;
import hr.fer.zemris.java.hw15.model.BlogUser;
import hr.fer.zemris.java.hw15.model.forms.LoginForm;
import hr.fer.zemris.java.hw15.web.util.MessageSender;
import hr.fer.zemris.java.hw15.web.util.SessionAttributes;

/**
 * Class extends {@link HttpServlet}.
 * This servlet handles login and log out requests.
 * It implements only doPost method due to sensitive nature
 * of logins. 
 * @author Luka Mijić
 *
 */
@WebServlet("/servleti/login")
public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	private static final String LOGIN_METHOD = "Login";
	private static final String LOG_OUT_METHOD = "Log out";

	/**
	 * If user requested logout operation:
	 * 		1. first check if user is logged in
	 * 				- if he is not send him to error page, otherwise continue
	 * 		2. Invalidate current session
	 * 
	 * If user requested login:
	 * 		1. Check if user is already logged in
	 * 				- if he is send him to error page, otherwise continue
	 * 		2. Create {@link LoginForm} and fill it from {@link HttpServletRequest}.
	 * 		3. Validate {@link LoginForm}
	 * 				- if form contains error dispatch to login screen and show appropriate messages
	 * 		4. Store user's info into current session
	 * @param req {@link HttpServletRequest}
	 * @param resp {@link HttpServletResponse}
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if(LOG_OUT_METHOD.equals(req.getParameter("method"))) {
			if(req.getSession().getAttribute(SessionAttributes.ID) == null) {
				MessageSender.sendError(req, resp, "Can't log out when you are not signed in.", 400);
				return;
			}
			
			req.getSession().invalidate();
		} else if(LOGIN_METHOD.equals(req.getParameter("method"))){
			if(req.getSession().getAttribute(SessionAttributes.ID) != null) {
				MessageSender.sendError(req, resp, "Log out before signing in.", 400);
				return;
			}
			
			LoginForm form = new LoginForm();
			form.fillFromHttpRequest(req);
			form.validate();
			
			if(form.hasErrors()) {
				req.setAttribute("form", form);
				req.getRequestDispatcher("/WEB-INF/pages/login.jsp").forward(req, resp);
				return;
			}
			
			BlogUser currUser = DAOProvider.getDAO().getBlogUserFromNick(form.getNick());
			HttpSession currSession = req.getSession();
			currSession.setAttribute(SessionAttributes.ID, currUser.getId());
			currSession.setAttribute(SessionAttributes.FIRST_NAME, currUser.getFirstName());
			currSession.setAttribute(SessionAttributes.LAST_NAME, currUser.getLastName());
			currSession.setAttribute(SessionAttributes.NICK, currUser.getNick());
			currSession.setAttribute(SessionAttributes.EMAIL, currUser.getEmail());
			currSession.setMaxInactiveInterval(600);
		} else {
			MessageSender.sendError(req, resp, "Invalid method.", 405);
			return;
		}
		
		resp.sendRedirect(req.getServletContext().getContextPath() + "/index.jsp");
		
	}
}
