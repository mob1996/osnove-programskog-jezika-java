package hr.fer.zemris.java.hw15.model.forms;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/**
 * This class is abstract class.
 * It models basic functionalities of forms
 * used for client input. 
 * @author Luka Mijić
 *
 */
public abstract class AbstractForm {

	/**
	 * {@link Map} that stores all errors after validation.
	 */
	private Map<String, String> errors = new HashMap<>();
	
	/**
	 * Retrieves error associated to given errName.
	 * @param errName key to error
	 * @return error associated to given parameter, otherwise return null
	 */
	public String getError(String errName) {
		return errors.get(errName);
	}
	
	/**
	 * Stores error. Name is key that is associated to error,
	 * and desc is it's description.
	 * @param name of error
	 * @param desc of description
	 */
	public void putError(String name, String desc) {
		errors.put(name, desc);
	}
	
	/**
	 * Removes memory of all errors.
	 */
	public void clearErrors() {
		errors.clear();
	}

	/**
	 * @return true if at least one error is stored,
	 * 			otherwise return false
	 */
	public boolean hasErrors() {
		return !errors.isEmpty();
	}
	
	/**
	 * Check if there is error associated to given parameter.
	 * @param errName
	 * @return
	 */
	public boolean hasError(String errName) {
		return errors.containsKey(errName);
	}
	
	/**
	 * Takes input {@link String} and prepares it for
	 * form.
	 * @param inputData that is processed
	 * @return If given parameter is null
	 * 			reference return empty {@link String} otherwise
	 * 			call {@link String#trim()} on given parameter and return
	 * 			retrieved value.
	 */
	protected String prepareInputData(String inputData) {
		return inputData == null ? "" : inputData.trim();
	}
	
	/**
	 * Fills form with values given through {@link HttpServletRequest}.
	 * @param req given request.
	 */
	public abstract void fillFromHttpRequest(HttpServletRequest req);
	
	/**
	 * Method should always be called before using filled form for
	 * any operation. It checks for any errors in form and stores them
	 * into error {@link Map}. Usually error {@link Map} is cleared at
	 * the start of this method. 
	 */
	public abstract void validate();
}
