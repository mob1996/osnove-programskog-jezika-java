package hr.fer.zemris.java.hw15.web.util;

/**
 * Class stores key for getting user information
 * from current session.
 * @author Luka Mijić
 *
 */
public class SessionAttributes {

	/**
	 * Id key
	 */
	public static final String ID = "current_user_id";
	
	/**
	 * First name key
	 */
	public static final String FIRST_NAME = "current_user_fn";
	
	/**
	 * Last name key
	 */
	public static final String LAST_NAME = "current_user_ln";
	
	/**
	 * Nick key
	 */
	public static final String NICK = "current_user_nick";
	
	/**
	 * Email key
	 */
	public static final String EMAIL = "current_user_email";
}
