package hr.fer.zemris.java.hw15.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Class models all properties of single blog entry.
 * This objects uses JPA annotations and can be stored
 * in database. 
 * @author Luka Mijić
 *
 */
@NamedQueries({
	@NamedQuery(name="BlogEntry.upit1",query="select b from BlogComment as b where b.blogEntry=:be and b.postedOn>:when")
})
@Entity
@Table(name="blog_entries")
@Cacheable(true)
public class BlogEntry {

	/**
	 * Blog id
	 */
	private Long id;
	
	/**
	 * Author of blog
	 */
	private BlogUser author;
	
	/**
	 * Blog's comments
	 */
	private List<BlogComment> comments = new ArrayList<>();
	
	/**
	 * Time of creation of blog
	 */
	private Date createdAt;
	
	/**
	 * Time when blog was last modified
	 */
	private Date lastModifiedAt;
	
	/**
	 * Blog's title
	 */
	private String title;
	
	/**
	 * Contents of blog
	 */
	private String text;
	
	/**
	 * @return id of blog
	 */
	@Id @GeneratedValue
	public Long getId() {
		return id;
	}
	
	/**
	 * @param id is new id of blog
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return author that wrote this blog
	 */
	@ManyToOne
	@JoinColumn(nullable=false)
	public BlogUser getAuthor() {
		return author;
	}

	/**
	 * @param author is new author of blog
	 */
	public void setAuthor(BlogUser author) {
		this.author = author;
	}

	/**
	 * @return {@link List} of all {@link BlogComment} posted on this blog
	 */
	@OneToMany(mappedBy="blogEntry",fetch=FetchType.LAZY, cascade=CascadeType.PERSIST, orphanRemoval=true)
	@OrderBy("postedOn")
	public List<BlogComment> getComments() {
		return comments;
	}
	
	/**
	 * @param comments is new {@link List} of comments
	 */
	public void setComments(List<BlogComment> comments) {
		this.comments = comments;
	}

	/**
	 * @return creation time
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt is new time of creation
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return time of last modification of blog contents
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=true)
	public Date getLastModifiedAt() {
		return lastModifiedAt;
	}

	/**
	 * @param lastModifiedAt is new time of last modification
	 */
	public void setLastModifiedAt(Date lastModifiedAt) {
		this.lastModifiedAt = lastModifiedAt;
	}

	/**
	 * @return title of blog
	 */
	@Column(length=200,nullable=false)
	public String getTitle() {
		return title;
	}

	/**
	 * @param title is new blog's title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return contents of blog
	 */
	@Column(length=4096,nullable=false)
	public String getText() {
		return text;
	}

	/**
	 * @param text is new blog content
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return calculated hash code
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * @return true if two {@link BlogEntry} are equal, otherwise return false.
	 * 			Two {@link BlogEntry} objects are equal if they have same id.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BlogEntry other = (BlogEntry) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}