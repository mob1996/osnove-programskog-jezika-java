package hr.fer.zemris.java.hw15.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw15.dao.DAO;
import hr.fer.zemris.java.hw15.dao.DAOProvider;
import hr.fer.zemris.java.hw15.model.BlogComment;
import hr.fer.zemris.java.hw15.model.BlogEntry;
import hr.fer.zemris.java.hw15.model.BlogUser;
import hr.fer.zemris.java.hw15.model.forms.BlogForm;
import hr.fer.zemris.java.hw15.model.forms.CommentForm;
import hr.fer.zemris.java.hw15.web.util.MessageSender;
import hr.fer.zemris.java.hw15.web.util.SessionAttributes;

/**
 * Servlet extends {@link HttpServlet}.
 * This servlet handles all requests aimed at 
 * "servleti/author/*" URL. It handles multiple options
 * like viewing blogs, creating new blogs, editing blogs.
 * @author Luka Mijić
 *
 */
@WebServlet("/servleti/author/*")
public class AuthorServlet extends HttpServlet {

	/**
	 * Results of login validation. 
	 */
	private static enum LoginValidation {
		OK, NOT_SIGNED_IN, FORBIDDEN;
	}
	
	private static final long serialVersionUID = 1L;

	/**
	 * Calls {@link AuthorServlet#process(HttpServletRequest, HttpServletResponse)}.
	 * @param req {@link HttpServletRequest}
	 * @param resp {@link HttpServletResponse}
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		process(req, resp);
	}
	
	/**
	 * Calls {@link AuthorServlet#process(HttpServletRequest, HttpServletResponse)}.
	 * @param req {@link HttpServletRequest}
	 * @param resp {@link HttpServletResponse}
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		process(req, resp);
	}
	
	/**
	 * First retrieve "*" part from "servleti/author/*" URL.
	 * Split it using "/" as separator.
	 * 
	 * a) Only one value received by splitting:
	 * 		1. Try to get {@link BlogUser} with using that value as
	 * 			nick parameter in {@link DAO#getBlogUser(Long)}
	 * 				- if user doesn't exists return message that client chose invalid path
	 * 		2. Dispatch client to authors profile page and lists all of his {@link BlogEntry}.
	 * 
	 * b) Two parts received by splitting:
	 * 		1. Try to get {@link BlogUser} with using that value as
	 * 			nick parameter in {@link DAO#getBlogUser(Long)}
	 * 				- if user doesn't exists return message that client chose invalid path
	 * 			
	 * 			i) if second parameter is "new"
	 * 				1. If user is not logged in or tries to access other users
	 * 					new operation send him error message
	 * 				2. Call {@link AuthorServlet#processNew(HttpServletRequest, HttpServletResponse, String)}
	 * 			
	 * 			ii) if second parameter is "edit" 
	 * 				1. Check if client's nick is same as URL nick part, if it's not send him message
	 * 				2. Call {@link AuthorServlet#processEdit(HttpServletRequest, HttpServletResponse, Long)}
	 * 			
	 * 		   iii) 1. Check if second parameter is number value if it's not send error message
	 * 				2. Call {@link AuthorServlet#showBlog(HttpServletRequest, HttpServletResponse, Long, String)} 
	 * 
	 * @param req {@link HttpServletRequest}
	 * @param resp {@link HttpServletResponse}
	 * @throws ServletException
	 * @throws IOException
	 */
	private void process(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String pathInfo = req.getPathInfo();
		if(pathInfo.startsWith("/")) {
			pathInfo = pathInfo.replaceFirst("/", "");
		}
		
		String[] pathParameters = pathInfo.split("/");
		
		if(pathParameters.length < 1 || pathParameters.length > 2) {
			MessageSender.sendError(req, resp, "Invalid path.", 404);
			return;
		}
		
		String nick = pathParameters[0];
		BlogUser user = DAOProvider.getDAO().getBlogUserFromNick(nick);
		if(user == null) {
			MessageSender.sendError(req, resp, "Invalid path. User doesn't exist.", 404);
			return;
		}
		
		if(req.getParameter("method") != null && "Cancel".equals(req.getParameter("method"))) {
			resp.sendRedirect(req.getServletContext().getContextPath() + "/servleti/author/" + nick);
			return;
		}
		
		if(pathParameters.length == 1) {
			req.setAttribute("user", user);
			req.getRequestDispatcher("/WEB-INF/pages/author.jsp").forward(req, resp);
		} else if(pathParameters.length == 2) {
			if(pathParameters[1].equals("new")) {
				LoginValidation validation = validLogin(req, nick);
				
				if(validation != LoginValidation.OK) {
					String msg = validation == LoginValidation.FORBIDDEN ? "Forbidden access" : "You need to sign in to create blog.";
					int errorCode = validation == LoginValidation.FORBIDDEN ? 403 : 405;
					MessageSender.sendError(req, resp, msg, errorCode);
					return;
				}
				
				processNew(req, resp, nick);
			} else if(pathParameters[1].startsWith("edit?") || pathParameters[1].equals("edit")) {
				if(validLogin(req, nick) != LoginValidation.OK) {
					MessageSender.sendError(req, resp, "You don't have access to this blog.", 403);
					return;
				}
				
				Long blogId = null;
				try {
					blogId = Long.valueOf(req.getParameter("blogId"));
				} catch(NumberFormatException exc) {
					MessageSender.sendError(req, resp, "Blog id is not valid.", 404);
					return;
				}
				
				processEdit(req, resp, blogId);
				return;
			} else {
				Long blogId = null;
				try {
					blogId = Long.valueOf(pathParameters[1]);
					showBlog(req, resp, blogId, nick);
				} catch (NumberFormatException exc) {
					MessageSender.sendError(req, resp, "Invalid blog entry ID", 404);
				}
			}
		} 
	}
	
	/**
	 * Method check if user has access to wanted resources.
	 * @param req {@link HttpServletRequest} is used to retrieve session information
	 * @param nick in "/servleti/author/NICK/*" path
	 * @return if client is not logged in return {@link LoginValidation#NOT_SIGNED_IN }, if
	 * 			user is logged in but given nick is not equal to their nick return
	 * 			{@link LoginValidation#FORBIDDEN}, otherwise return {@link LoginValidation#OK}
	 */
	private LoginValidation validLogin(HttpServletRequest req, String nick) {
		if(req.getSession().getAttribute(SessionAttributes.ID) == null) {
			return LoginValidation.NOT_SIGNED_IN;
		}
		String sessionNick = (String) req.getSession().getAttribute(SessionAttributes.NICK);
		return sessionNick.equals(nick) ? LoginValidation.OK : LoginValidation.FORBIDDEN;
	}
	
	/**
	 * Process:
	 * 	1. Store nick and "new" in temporary parameters
	 *  2. If value "Save" is not marked to key "method" dispatch
	 *  	to blogForm.jsp
	 *  3. Create {@link BlogForm} and fill it with {@link HttpServletRequest}
	 *  4. Validate form
	 *  5. If it has errors dispatch to blogForm.jsp and show error text to client
	 *  6. Add {@link BlogEntry}
	 * @param req {@link HttpServletRequest}
	 * @param resp {@link HttpServletResponse}
	 * @param nick in "/servleti/author/NICK/*" path
	 * @throws IOException
	 * @throws ServletException
	 */
	private void processNew(HttpServletRequest req, HttpServletResponse resp, String nick) throws IOException, ServletException {
		req.setAttribute("author", nick);
		req.setAttribute("operation", "new");
		
		if(req.getParameter("method") == null || !"Save".equals(req.getParameter("method"))) {
			req.getRequestDispatcher("/WEB-INF/pages/blogForm.jsp").forward(req, resp);
			return;
		} 
		
		BlogForm form = new BlogForm();
		form.fillFromHttpRequest(req);
		form.validate();
		if(form.hasErrors()) {
			req.setAttribute("form", form);
			req.getRequestDispatcher("/WEB-INF/pages/blogForm.jsp").forward(req, resp);
			return;
		}
		
		BlogEntry blog = new BlogEntry();
		form.fillFromBlogForm(blog);
		DAOProvider.getDAO().addBlogToUser(nick, blog);
		resp.sendRedirect(req.getServletContext().getContextPath() + "/servleti/author/" + nick);
	}
	
	/**
	 * Process:
	 * 	1. Check if {@link BlogEntry} with given blogId exist
	 * 			- if it doesn't send error
	 *  2. Check if current user has same id as blog author
	 *  		- if he doesn't send error
	 *  3. Store nick and "edit" to temporary parameters.
	 *  4. If value "Save" is not marked to key "method" dispatch
	 *  	to blogForm.jsp
	 *  5. Create {@link BlogForm} and fill it with {@link HttpServletRequest}
	 *  6. Validate form
	 *  7. If it has errors dispatch to blogForm.jsp and show error text to client
	 *  8. Update {@link BlogEntry}
	 * @param req {@link HttpServletRequest}
	 * @param resp {@link HttpServletResponse}
	 * @param blogId id of blog that is edited
	 * @throws ServletException
	 * @throws IOException
	 */
	private void processEdit(HttpServletRequest req, HttpServletResponse resp, Long blogId) throws ServletException, IOException {
		BlogEntry blog = DAOProvider.getDAO().getBlogEntry(blogId);
		if(blog == null) {
			MessageSender.sendError(req, resp, "Blog doesn't exist.", 404);
			return;
		} else if(!blog.getAuthor().getId().equals(req.getSession().getAttribute(SessionAttributes.ID))) {
			MessageSender.sendError(req, resp, "Forbidden access to blog.", 403);
			return;
		}
		
		req.setAttribute("author", blog.getAuthor().getNick());
		req.setAttribute("operation", "edit");
	
		BlogForm form = new BlogForm();
		if(req.getParameter("method") == null || !"Save".equals(req.getParameter("method"))) {
			form.fillFormFromBlog(blog);
			req.setAttribute("form", form);
			req.getRequestDispatcher("/WEB-INF/pages/blogForm.jsp").forward(req, resp);
			return;
		} 
		
		form.fillFromHttpRequest(req);
		form.validate();
		if(form.hasErrors()) {
			req.setAttribute("form", form);
			req.getRequestDispatcher("/WEB-INF/pages/blogForm.jsp").forward(req, resp);
			return;
		}
		
		DAOProvider.getDAO().editBlog(blogId, form.getTitle(), form.getText());
		resp.sendRedirect(req.getServletContext().getContextPath() + "/servleti/author/" + blog.getAuthor().getNick() + "/" + blog.getId());
	}

	/**
	 * Process:
	 * 	1. Check if blog exists if it doesn't send error
	 * 	2. If value "Comment" is not marked to key "method" dispatch
	 *  	to showBorm.jsp
	 *  3. Create {@link CommentForm} and fill it with {@link HttpServletRequest}
	 *  4. Validate form and if it has errors dispatch to showBlog.jsp and show error messages
	 *  5. Add {@link BlogComment}
	 * @param req {@link HttpServletRequest}
	 * @param resp {@link HttpServletResponse}
	 * @param blogId id id of viewed blog
	 * @param nick of blogs author
	 * @throws ServletException
	 * @throws IOException
	 */
	private void showBlog(HttpServletRequest req, HttpServletResponse resp, Long blogId, String nick) throws ServletException, IOException {
		BlogEntry blog = DAOProvider.getDAO().getBlogEntry(blogId);
		if(blog == null) {
			MessageSender.sendError(req, resp, "Blog doesn't exits.", 404);
			return;
		}
		
		if(req.getParameter("method") == null || !"Comment".equals(req.getParameter("method"))) {
			req.setAttribute("blog", blog);
			req.getRequestDispatcher("/WEB-INF/pages/showBlog.jsp").forward(req, resp);
			return;
		} 
		
		CommentForm commForm = new CommentForm();
		commForm.fillFromHttpRequest(req);
		commForm.validate();
		if(commForm.hasErrors()) {
			req.setAttribute("blog", blog);
			req.setAttribute("form", commForm);
			req.getRequestDispatcher("/WEB-INF/pages/showBlog.jsp").forward(req, resp);
			return;
		}
	
		BlogComment comm = new BlogComment();
		commForm.fillFromCommentForm(comm);
		DAOProvider.getDAO().addCommentToBlog(blogId, comm);
		resp.sendRedirect(req.getServletContext().getContextPath() + "/servleti/author/" + nick + "/" + blogId);
	}
	
}
