 package hr.fer.zemris.java.hw15.dao.jpa;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import hr.fer.zemris.java.hw15.dao.DAO;
import hr.fer.zemris.java.hw15.dao.DAOException;
import hr.fer.zemris.java.hw15.model.BlogComment;
import hr.fer.zemris.java.hw15.model.BlogEntry;
import hr.fer.zemris.java.hw15.model.BlogUser;

/**
 * Implementation of {@link DAO} interface.
 * This implementation uses JPA API for 
 * writing and accessing data from database.
 * @see DAO for method documentation.
 * @author Luka Mijić
 *
 */
public class JPADAOImpl implements DAO {

	/**
	 * @see DAO#getBlogEntry(Long)
	 */
	@Override
	public BlogEntry getBlogEntry(Long id) throws DAOException {
		return JPAEMProvider.getEntityManager().find(BlogEntry.class, id);
	}

	/**
	 * @see DAO#getBlogUser(Long)
	 */
	@Override
	public BlogUser getBlogUser(Long id) throws DAOException {
		return JPAEMProvider.getEntityManager().find(BlogUser.class, id);
	}

	/**
	 * @see DAO#getBlogUserFromNick(String)
	 */
	@Override
	public BlogUser getBlogUserFromNick(String nick) throws DAOException {
		EntityManager em = JPAEMProvider.getEntityManager();
		BlogUser user = null;
		try {
			user =  em.createNamedQuery("byNick", BlogUser.class)
						.setParameter("checkNick", nick)
						.getSingleResult();	
		} catch (NoResultException ignorable){
			
		}
		return user;
	}

	/**
	 * @see DAO#getBlogUserFromEmail(String)
	 */
	@Override
	public BlogUser getBlogUserFromEmail(String email) throws DAOException {
		EntityManager em = JPAEMProvider.getEntityManager();
		BlogUser user = null;
		try {
			user =  em.createNamedQuery("byEmail", BlogUser.class)
					.setParameter("checkMail", email)
					.getSingleResult();	
		} catch (NoResultException ignorable){
			
		}
		return user;
	}
	
	/**
	 * @see DAO#getAuthors()
	 */
	@Override
	public List<BlogUser> getAuthors() throws DAOException {
		EntityManager em = JPAEMProvider.getEntityManager();
		return em.createNamedQuery("getAuthors", BlogUser.class).getResultList();
	}

	/**
	 * @see DAO#addBlogUser(BlogUser)
	 */
	@Override
	public void addBlogUser(BlogUser user) throws DAOException {
		JPAEMProvider.getEntityManager().persist(user);
	}

	/**
	 * @see DAO#addBlogToUser(String, BlogEntry)
	 */
	@Override
	public void addBlogToUser(String nick, BlogEntry blog) throws DAOException {
		BlogUser user = getBlogUserFromNick(nick);
		
		if(user == null) {
			throw new DAOException("User doesn't exist");
		}
		blog.setAuthor(user);
		user.getPublishedBlogs().add(blog);
	}

	/**
	 * @see DAO#addCommentToBlog(Long, BlogComment)
	 */
	@Override
	public void addCommentToBlog(Long blogId, BlogComment comment) throws DAOException {
		BlogEntry blog = getBlogEntry(blogId);
		
		if(blog == null) {
			throw new DAOException("Blog doesn't exist");
		}
		
		comment.setBlogEntry(blog);
		blog.getComments().add(comment);
		
	}

	/**
	 * @see DAO#editBlog(Long, String, String)
	 */
	@Override
	public void editBlog(Long blogId, String title, String text) throws DAOException {
		BlogEntry blog = getBlogEntry(blogId);
		blog.setTitle(title);
		blog.setText(text);
		blog.setLastModifiedAt(new Date());
		
	}

}