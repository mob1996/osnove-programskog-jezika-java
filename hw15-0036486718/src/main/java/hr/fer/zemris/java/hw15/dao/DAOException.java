package hr.fer.zemris.java.hw15.dao;

/**
 * Extends {@link RuntimeException}.
 * It models exceptions that occur while working with 
 * DAO (Data Access Object).
 * @author Luka Mijić
 *
 */
public class DAOException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * Creates new {@link DAOException}
	 * @param message of exception
	 * @param cause of exception
	 */
	public DAOException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Creates new {@link DAOException}.
	 * @param message of exception
	 */
	public DAOException(String message) {
		super(message);
	}
}