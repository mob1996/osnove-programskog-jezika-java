package hr.fer.zemris.java.hw15.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw15.dao.DAOProvider;
import hr.fer.zemris.java.hw15.model.BlogUser;
import hr.fer.zemris.java.hw15.model.forms.UserRegistrationForm;
import hr.fer.zemris.java.hw15.web.util.MessageSender;
import hr.fer.zemris.java.hw15.web.util.SessionAttributes;

/**
 * Class extends {@link HttpServlet}.
 * This servlet handles registration process.
 * It implements doGet and doPost method due to sensitive nature
 * of registration process. 
 * @author Luka Mijić
 *
 */
@WebServlet("/servleti/register")
public class RegisterServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * If user is already logged in send them error message.
	 * Otherwise dispatch them to registration screen. 
	 * @param req {@link HttpServletRequest}
	 * @param resp {@link HttpServletResponse}
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if(req.getSession().getAttribute(SessionAttributes.ID) != null) {
			MessageSender.sendInfo(req, resp, "You are already registered.");
			return;
		}
		
		req.getRequestDispatcher("/WEB-INF/pages/register.jsp").forward(req, resp);
	}
	
	/**
	 * Registration process:
	 * 	1. Check if user is already logged in and if he is send them info message
	 * 	2. Parameter mapped to method must be "Register". If it isn't redirect user to "index.jsp".
	 * 	3. Create {@link UserRegistrationForm} and fill it from request.
	 *  4. Validate {@link UserRegistrationForm}
	 *  5. If form contains error dispatch user to register form and give appropriate messages
	 *  6. Create user
	 * @param req {@link HttpServletRequest}
	 * @param resp {@link HttpServletResponse}
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if(req.getSession().getAttribute(SessionAttributes.ID) != null) {
			MessageSender.sendInfo(req, resp, "You are already registered.");
			return;
		}
		
		req.setCharacterEncoding("UTF-8");
		
		String method = req.getParameter("method");
		if(!"Register".equals(method)) {
			resp.sendRedirect(req.getServletContext().getContextPath() + "/index.jsp");
			return;
		}
		
		UserRegistrationForm regForm = new UserRegistrationForm();
		regForm.fillFromHttpRequest(req);
		regForm.validate();
		
		if(regForm.hasErrors()) {
			req.setAttribute("form", regForm);
			req.getRequestDispatcher("/WEB-INF/pages/register.jsp").forward(req, resp);
			return;
		}
		
		BlogUser user = new BlogUser();
		regForm.fillFromRegistrationForm(user);
		DAOProvider.getDAO().addBlogUser(user);
		
		MessageSender.sendInfo(req, resp, "Registration complete.");
	}
}
