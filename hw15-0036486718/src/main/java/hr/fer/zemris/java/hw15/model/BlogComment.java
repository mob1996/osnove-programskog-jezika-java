package hr.fer.zemris.java.hw15.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Class models all properties of blog comments.
 * This objects uses JPA annotations and can be stored
 * in database. 
 * @author Luka Mijić
 *
 */
@Entity
@Table(name="blog_comments")
public class BlogComment {

	/**
	 * Id of comment
	 */
	private Long id;
	
	/**
	 * {@link BlogEntry} that is commented
	 */
	private BlogEntry blogEntry;
	
	/**
	 * {@link BlogUser} that wrote comment.
	 */
	private String usersEMail;
	
	/**
	 * Contents of comment
	 */
	private String message;
	
	/**
	 * Time when comment was posted
	 */
	private Date postedOn;
	
	/**
	 * @return id of comment
	 */
	@Id @GeneratedValue
	public Long getId() {
		return id;
	}
	
	/**
	 * @param id is new id of comment
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return {@link BlogEntry} that is commented
	 */
	@ManyToOne
	@JoinColumn(nullable=false)
	public BlogEntry getBlogEntry() {
		return blogEntry;
	}
	
	/**
	 * @param blogEntry is new {@link BlogEntry} that is commented
	 */
	public void setBlogEntry(BlogEntry blogEntry) {
		this.blogEntry = blogEntry;
	}

	/**
	 * @return email of comment's author
	 */
	@Column(length=100,nullable=false)
	public String getUsersEMail() {
		return usersEMail;
	}

	/**
	 * @param usersEMail is new email of comments author
	 */
	public void setUsersEMail(String usersEMail) {
		this.usersEMail = usersEMail;
	}

	/**
	 * @return content of comment
	 */
	@Column(length=4096,nullable=false)
	public String getMessage() {
		return message;
	}

	/**
	 * @param message is new content of comment
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return moment when comment was posted
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	public Date getPostedOn() {
		return postedOn;
	}

	/**
	 * @param postedOn is new comment post time
	 */
	public void setPostedOn(Date postedOn) {
		this.postedOn = postedOn;
	}

	/**
	 * @return calculated hash code of {@link BlogComment}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * @return true if two {@link BlogComment}s are equal, otherwise return false.
	 * 			Two {@link BlogComment}s are equal when their ID's are same.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BlogComment other = (BlogComment) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}