package hr.fer.zemris.java.hw15.model.forms;

import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;

import hr.fer.zemris.java.hw15.crypto.PasswordEncrypt;
import hr.fer.zemris.java.hw15.dao.DAOProvider;
import hr.fer.zemris.java.hw15.model.BlogUser;

/**
 * Class extends {@link AbstractForm}.
 * This form is used for registration of {@link BlogUser}.
 * @author Luka Mijić
 *
 */
public class UserRegistrationForm  extends AbstractForm{

	/**
	 * User's first name
	 */
	private String firstName;
	
	/**
	 * User's last name
	 */
	private String lastName;
	
	/**
	 * User's email
	 */
	private String email;
	
	/**
	 * User's nick
	 */
	private String nick;
	
	/**
	 * User's password hash
	 */
	private String passwordHash;
	
	@Override
	public void fillFromHttpRequest(HttpServletRequest req) {
		this.firstName = prepareInputData(req.getParameter("fName"));
		this.lastName = prepareInputData(req.getParameter("lName"));
		this.email = prepareInputData(req.getParameter("email"));
		this.nick = prepareInputData(req.getParameter("nick"));
		processPassword(req.getParameter("password"));
	}
	
	/**
	 * This method takes {@link BlogUser} and
	 * uses it's properties to fill form properties.
	 * @param user that is used to filling up form
	 */
	public void fillFromUser(BlogUser user) {
		this.firstName = user.getFirstName();
		this.lastName = user.getLastName();
		this.nick = user.getNick();
		this.email = user.getEmail();
		this.passwordHash = user.getPasswordHash();
	}
	
	/**
	 * This method takes {@link BlogUser} and
	 * fills it's properties using form properties
	 * @param user whose properties are filled
	 */
	public void fillFromRegistrationForm(BlogUser user) {
		user.setFirstName(this.firstName);
		user.setLastName(this.lastName);
		user.setNick(this.nick);
		user.setEmail(this.email);
		user.setPasswordHash(this.passwordHash);
		
	}
	
	@Override
	public void validate() {
		clearErrors();
		
		if(this.firstName.isEmpty()) {
			putError("fName", "First name is required");
		}
		
		if(this.lastName.isEmpty()) {
			putError("lName", "Last name is required");
		}
		
		if(this.email.isEmpty()) {
			putError("email", "Email is required.");
		} else {
			int l = this.email.length();
			int p = this.email.indexOf('@');
			if(l<3 || p==-1 || p==0 || p==l-1) {
				putError("email", "Email format is not valid.");
			} else if(DAOProvider.getDAO().getBlogUserFromEmail(email) != null){
				putError("email", "EMail already in use.");
			}
		}
		
		if(this.nick.isEmpty()) {
			putError("nick", "Nick is required");
		} else if(DAOProvider.getDAO().getBlogUserFromNick(nick) != null) {
			putError("nick", "Nick is already in use.");
		}
		
		if(this.passwordHash == null || this.passwordHash.isEmpty()) {
			putError("password", "Invalid password.");
		} 
	}
	
	/**
	 * This method takes password values, it doesn't store it
	 * but it calculates it's hash and stores calculated hash into form.
	 * @param password whose hash is stored
	 */
	private void processPassword(String password){
		password = prepareInputData(password);
		this.passwordHash = null;
		
		if(password.isEmpty()) {
			return;
		}

		try {
			this.passwordHash = PasswordEncrypt.encryptPassword(password);
		} catch (NoSuchAlgorithmException e) {
			
		}
	}

	/**
	 * @return form's first name property
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName is new first name of form property
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return form's last name property
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName is new last name of form property
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return form's email address property
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email is new email address of form property
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return form's nick property
	 */
	public String getNick() {
		return nick;
	}

	/**
	 * @param nick is new nick form property
	 */
	public void setNick(String nick) {
		this.nick = nick;
	}

	/**
	 * @return passwords hash of form property
	 */
	public String getPasswordHash() {
		return passwordHash;
	}

	/**
	 * @param passwordHash is new password's hash of form property
	 */
	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}
	
}
