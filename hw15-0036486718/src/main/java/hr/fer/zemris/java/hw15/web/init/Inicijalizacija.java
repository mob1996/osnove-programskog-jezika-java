package hr.fer.zemris.java.hw15.web.init;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import hr.fer.zemris.java.hw15.dao.jpa.JPAEMFProvider;

/**
 * Class implements {@link ServletContextListener}.
 * {@link ServletContextListener#contextInitialized(ServletContextEvent)}
 * is called when application starts. 
 * {@link ServletContextListener#contextDestroyed(ServletContextEvent)} is
 * called on application shutdown.
 * @author Luka Mijić
 *
 */
@WebListener
public class Inicijalizacija implements ServletContextListener {

	/**
	 * Creates {@link EntityManagerFactory} and stores it using
	 * {@link JPAEMFProvider#setEmf(EntityManagerFactory)}.
	 * @param sce
	 */
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("baza.podataka.za.blog");  
		sce.getServletContext().setAttribute("my.application.emf", emf);
		JPAEMFProvider.setEmf(emf);
	}

	/**
	 * Closes {@link EntityManagerFactory} created on initialization of application.
	 */
	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		JPAEMFProvider.setEmf(null);
		EntityManagerFactory emf = (EntityManagerFactory)sce.getServletContext().getAttribute("my.application.emf");
		if(emf!=null) {
			emf.close();
		}
	}
}