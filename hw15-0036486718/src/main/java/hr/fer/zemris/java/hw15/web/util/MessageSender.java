package hr.fer.zemris.java.hw15.web.util;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class contains several methods
 * used for dispatching request to give client some feedback.
 * @author Luka Mijić
 *
 */
public class MessageSender {
	
	/**
	 * Dispatches request to message.jsp that show given message.
	 * @param req {@link HttpServletRequest}
	 * @param resp {@link HttpServletResponse}
	 * @param message is message being set as temporary parameter
	 * @throws ServletException
	 * @throws IOException
	 */
	public static void sendInfo(HttpServletRequest req, HttpServletResponse resp, String message) throws ServletException, IOException {
		req.setAttribute("message", message);
		req.getRequestDispatcher("/WEB-INF/pages/info/message.jsp").forward(req, resp);
	}
	
	/**
	 * Dispatches request to message.jsp that show given error message and error code.
	 * @param req {@link HttpServletRequest}
	 * @param resp {@link HttpServletResponse}
	 * @param message is error message being set as temporary parameter
	 * @param code is error code being set as temporary parameter
	 * @throws ServletException
	 * @throws IOException
	 */
	public static void sendError(HttpServletRequest req, HttpServletResponse resp, String message, int code) throws ServletException, IOException {
		req.setAttribute("message", code + ":  " +  message);
		req.getRequestDispatcher("/WEB-INF/pages/info/message.jsp").forward(req, resp);
	}
}
