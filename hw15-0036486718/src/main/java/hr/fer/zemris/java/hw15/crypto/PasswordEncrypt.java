package hr.fer.zemris.java.hw15.crypto;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

/**
 * Class has method used for encrypting passwords.
 * @author Luka Mijić
 *
 */
public class PasswordEncrypt {
	/**
	 * Each hex character can be represented as 4
	 * zeroes or ones
	 */
	private static int HEX_2_BYTE_LENGHT = 4;
	
	/**
	 * All valid hex values
	 */
	private static final String HEXES = "0123456789abcdef";
	
	/**
	 * 00001111 mask
	 */
	private static int LAST_FOUR_MASK = 0x0F;
	
	/**
	 * 10000000 mask
	 */
	private static int FIRST_FOUR_MASK = 0xF0;
	
	private static final String SHA1 = "SHA-1";
	
	/**
	 * Method takes passwords and calculates it's 
	 * hash using "SHA-1". 
	 * @param password that is encrypted
	 * @return "SHA-1" hash of password
	 * @throws NoSuchAlgorithmException should not be thrown
	 */
	public static String encryptPassword(String password) throws NoSuchAlgorithmException {
		MessageDigest crypt = MessageDigest.getInstance(SHA1);
		crypt.reset();
		crypt.update(password.getBytes(StandardCharsets.UTF_8));
		return byteToHex(crypt.digest());
	}
	
	/**
	 * Method transforms bytes array into hex String
	 * @param bytes
	 * @return bytes transformed into valid hex String
	 * @throws NullPointerException if bytes are null reference
	 */
	public static String byteToHex(byte[] bytes) {
		Objects.requireNonNull(bytes, "Given bytes array can't be null reference.");
		StringBuilder hex = new StringBuilder();
		
		for(Byte b:bytes) {
			int index1 = (int) ((b & FIRST_FOUR_MASK) >> HEX_2_BYTE_LENGHT);
			int index2 = ((int) b) & LAST_FOUR_MASK;
			hex.append(HEXES.charAt(index1));
			hex.append(HEXES.charAt(index2));
		}
		
		return hex.toString();
	}
}
