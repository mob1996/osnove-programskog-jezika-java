<%@ page session="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="contextPath" value="${pageContext.request.contextPath }"/>

<html>
 	<meta charset="UTF-8"> 
	<head>
		<style type="text/css">
		.error {
		   font-family: fantasy;
		   font-weight: bold;
		   font-size: 0.9em;
		   color: #FF0000;
		   padding-left: 110px;
		}
		.formLabel {
		   display: inline-block;
		   width: 100px;
                   font-weight: bold;
		   text-align: right;
                   padding-right: 10px;
		}
		.formControls {
		  margin-top: 10px;
		}
		</style>
	</head>
	
	<body>
		<c:choose>
			<c:when test="${operation == 'edit' }">
				<h1>Edit your blog.</h1>
			</c:when>
			<c:when test="${operation == 'new' }">
				<h1>Create new blog.</h1>
			</c:when>
		</c:choose>
		
		<form action="" method="post">
			<c:if test="${operation == 'edit' }">
				<input type="text" name="blogId" hidden="true" value="<c:out value="${blogId}"/>">
			</c:if>
			
			<div>
				<div>
				 	<span class="formLabel">Title</span>
				 	<input type="text" name="title" value="<c:out value="${form.title }"/>" size="20">
				</div>
					<c:if test="${form.hasError('title')}">
		 				<div class="error"><c:out value="${form.getError('title')}"/></div>
		 			</c:if>
			</div>
			
			<div>
				<div>
				 	<span class="formLabel">Blog</span>
				 	<textarea name="text" rows="10" cols="70" placeholder="Start writing blog here...">${form.text }</textarea>
				</div>
					<c:if test="${form.hasError('text')}">
		 				<div class="error"><c:out value="${form.getError('text')}"/></div>
		 			</c:if>
			</div>
			
			<div class="formControls">
		  		<span class="formLabel">&nbsp;</span>
		  		<input type="submit" name="method" value="Save">
		  		<input type="submit" name="method" value="Cancel">
			</div>
		</form>
	</body>
</html>