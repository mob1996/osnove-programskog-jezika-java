<%@ page session="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="contextPath" value="${pageContext.request.contextPath }"/>

<html>
	<meta charset="UTF-8"> 
	<head>
		<title><c:out value="${author }"/></title>
		
		<style type="text/css">
		.error {
		   font-family: fantasy;
		   font-weight: bold;
		   font-size: 0.9em;
		   color: #FF0000;
		   padding-left: 110px;
		}
		.formLabel {
		   display: inline-block;
		   width: 100px;
                   font-weight: bold;
		   text-align: right;
                   padding-right: 10px;
		}
		.formControls {
		  margin-top: 10px;
		}
		
		.btn-link{
  			border:none;
  			outline:none;
 		 	background:none;
  			cursor:pointer;
  			color:#0000EE;
  			padding:0;
  			text-decoration:underline;
  			font-family:inherit;
  			font-size:inherit;
		}
		
		</style>
	</head>
	
	<body>
		<p><a href="${contextPath }/index.html">HOME</a></p>
		
		<h1>${blog.title }</h1>
		<textarea readonly cols="60" rows="30">${blog.text }</textarea>
		<c:if test="${blog.author.id == current_user_id }">
			<p><a href="${contextPath }/servleti/author/${current_user_nick }/edit?blogId=${blog.id }">Edit</a></p>
		</c:if>
		
		<form action="" method="post">
			<c:if test="${operation == 'edit' }">
				<input type="text" name="blogId" hidden="true" value="<c:out value="${blogId}"/>">
			</c:if>
			
			<div>
				<div>
					<span class="formLabel">Email</span>
					<c:choose>
						<c:when test="${not empty current_user_id }">
							<input type="text" name="email" value="<c:out value="${current_user_email }"/>" size="30" readonly >
						</c:when>
						<c:otherwise>
							<input type="text" name="email" value="<c:out value="${form.email }"/>" size="30">
						</c:otherwise>
					</c:choose>
				</div>
					<c:if test="${form.hasError('email')}">
		 				<div class="error"><c:out value="${form.getError('email')}"/></div>
		 			</c:if>
			</div>
			
			<div>
				<div>
				 	<span class="formLabel">Comment</span>
				 	<textarea name="comment" rows="10" cols="30" placeholder="Start writing comment here...">${form.comment }</textarea>
				</div>
					<c:if test="${form.hasError('comment')}">
		 				<div class="error"><c:out value="${form.getError('comment')}"/></div>
		 			</c:if>
			</div>
			
			<div class="formControls">
		  		<span class="formLabel">&nbsp;</span>
		  		<input type="submit" name="method" value="Comment">
			</div>
		</form>
		
		<% int index = 1; %>
		<c:forEach var="comment" items="${blog.comments }">
			<p><% out.write(String.valueOf(index)); %>. Comment</p>
			<p><textarea name="text" rows="10" cols="30" readonly>${comment.message }</textarea></p>
			<p><input type="text" name="email" value="<c:out value="${comment.usersEMail }"/>" size="30" readonly></p>
			<% index++; %>
		</c:forEach>
		
	</body>
</html>