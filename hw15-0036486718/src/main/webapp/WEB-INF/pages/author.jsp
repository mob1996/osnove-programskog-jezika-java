<%@ page session="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="contextPath" value="${pageContext.request.contextPath }"/>

<html>
 	<meta charset="UTF-8"> 

	<head>
		<title><c:out value="${author }"/></title>
		
		<style type="text/css">
		.error {
		   font-family: fantasy;
		   font-weight: bold;
		   font-size: 0.9em;
		   color: #FF0000;
		   padding-left: 110px;
		}
		.formLabel {
		   display: inline-block;
		   width: 100px;
                   font-weight: bold;
		   text-align: right;
                   padding-right: 10px;
		}
		.formControls {
		  margin-top: 10px;
		}
		
		.btn-link{
  			border:none;
  			outline:none;
 		 	background:none;
  			cursor:pointer;
  			color:#0000EE;
  			padding:0;
  			text-decoration:underline;
  			font-family:inherit;
  			font-size:inherit;
		}
		</style>
	</head>
	
	<body>
		<p><a href="${contextPath }/index.html">HOME</a></p>
		<c:if test="${not empty current_user_id }">
			<p><a href="${contextPath }/servleti/author/${current_user_nick}/new">Create your own blog.</a></p>
		</c:if>	
		<h1><c:out value="${user.nick }"/> blogs: </h1>
		<p>Number of ${user.nick }'s blogs is ${fn:length(user.publishedBlogs) }</p>
		<c:forEach var="blog" items="${user.publishedBlogs }">
			<p><a href="${contextPath }/servleti/author/${user.nick }/${blog.id }">${blog.title }</a></p>
		</c:forEach>
	</body>
</html>