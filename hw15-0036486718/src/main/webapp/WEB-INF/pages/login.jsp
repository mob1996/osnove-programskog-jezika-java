<%@ page session="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="contextPath" value="${pageContext.request.contextPath }"/>

<html>
	 <meta charset="UTF-8"> 
	<head>
		<title>LOGIN</title>
		
		<style type="text/css">
		.error {
		   font-family: fantasy;
		   font-weight: bold;
		   font-size: 0.9em;
		   color: #FF0000;
		   padding-left: 110px;
		}
		.formLabel {
		   display: inline-block;
		   width: 100px;
                   font-weight: bold;
		   text-align: right;
                   padding-right: 10px;
		}
		.formControls {
		  margin-top: 10px;
		}
		
		.btn-link{
  			border:none;
  			outline:none;
 		 	background:none;
  			cursor:pointer;
  			color:#0000EE;
  			padding:0;
  			text-decoration:underline;
  			font-family:inherit;
  			font-size:inherit;
		}
		</style>
	</head>
	
	<body>
		<c:choose>
			<c:when test="${empty current_user_id }">
				<form action="login" method="post">
					<div>
						<div>
				 			<span class="formLabel">Nickname</span>
				 			<input type="text" name="nick" value="<c:out value="${form.nick}"/>" size="20">
						</div>
						<c:if test="${form.hasError('nick')}">
		 					<div class="error"><c:out value="${form.getError('nick')}"/></div>
		 				</c:if>
					</div>
					<div>
						<div>
				 			<span class="formLabel">Password</span>
				 			<input type="password" name="password" value="" size="20">
						</div>
						<c:if test="${form.hasError('password')}">
		 					<div class="error"><c:out value="${form.getError('password')}"/></div>
		 				</c:if>
					</div>
			
					<div class="formControls">
		  				<span class="formLabel">&nbsp;</span>
		  				<input type="submit" name="method" value="Login">
					</div>
				</form>
		
				<a href="${contextPath }/servleti/register">Register</a>
			</c:when>
			<c:otherwise>
				<form action="login" method="post">
  					<button type="submit" name="method" value="Log out" class="btn-link">Log out</button>
				</form>
				
				<a href="${contextPath }/servleti/author/${current_user_nick }">My Page</a>
			</c:otherwise>
		</c:choose>	
		<p><c:out value="${current.user.id }"/></p>
		<h1>Authors</h1>
		<p>Number of authors: <c:out value="${fn:length(authors) }"/></p>
		<c:forEach var="author" items="${authors }">
				<p><a href="${contextPath }/servleti/author/${author.nick}"><c:out value="${author.nick }"/></a></p>		
		</c:forEach>
	</body>
</html>