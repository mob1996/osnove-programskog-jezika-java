<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="contextPath" value="${pageContext.request.contextPath }"/>

<html>
	<body>
		<h1>${message }</h1>
		
		
		<a href="${contextPath }/index.jsp">HOME</a>	
	</body>
</html>