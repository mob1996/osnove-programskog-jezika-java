<%@ page session="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="contextPath" value="${pageContext.request.contextPath }"/>

<html>
	 <meta charset="UTF-8"> 
	<head>
		<title>REGISTER</title>
		
		<style type="text/css">
		.error {
		   font-family: fantasy;
		   font-weight: bold;
		   font-size: 0.9em;
		   color: #FF0000;
		   padding-left: 110px;
		}
		.formLabel {
		   display: inline-block;
		   width: 100px;
                   font-weight: bold;
		   text-align: right;
                   padding-right: 10px;
		}
		.formControls {
		  margin-top: 10px;
		}
		</style>
	</head>
	
	<body>
		<form action="register" method="post">
			<div>
				<div>
				 <span class="formLabel">First Name</span><input type="text" name="fName" value="<c:out value="${form.firstName}"/>" size="20">
				</div>
				<c:if test="${form.hasError('fName')}">
		 			<div class="error"><c:out value="${form.getError('fName')}"/></div>
		 		</c:if>
			</div>
			
			<div>
				<div>
				 <span class="formLabel">Last Name</span><input type="text" name="lName" value="<c:out value="${form.lastName}"/>" size="20">
				</div>
				<c:if test="${form.hasError('lName')}">
		 			<div class="error"><c:out value="${form.getError('lName')}"/></div>
		 		</c:if>
			</div>
			
			<div>
				<div>
				 <span class="formLabel">Email</span><input type="text" name="email" value="<c:out value="${form.email}"/>" size="20">
				</div>
				<c:if test="${form.hasError('email')}">
		 			<div class="error"><c:out value="${form.getError('email')}"/></div>
		 		</c:if>
			</div>
		
			<div>
				<div>
				 <span class="formLabel">Nickname</span><input type="text" name="nick" value="<c:out value="${form.nick}"/>" size="20">
				</div>
				<c:if test="${form.hasError('nick')}">
		 			<div class="error"><c:out value="${form.getError('nick')}"/></div>
		 		</c:if>
			</div>
			
			<div>
				<div>
				 <span class="formLabel">Password</span><input type="password" name="password" value="" size="20">
				</div>
				<c:if test="${form.hasError('password')}">
		 			<div class="error"><c:out value="${form.getError('password')}"/></div>
		 		</c:if>
			</div>
			
			<div class="formControls">
		  		<span class="formLabel">&nbsp;</span>
		  		<input type="submit" name="method" value="Register">
		  		<input type="submit" name="method" value="Cancel">
			</div>
		</form>
	</body>
</html>